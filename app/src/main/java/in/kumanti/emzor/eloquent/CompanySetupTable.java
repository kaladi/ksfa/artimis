package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CompanySetup;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_COMPANY_SETUP;

public class CompanySetupTable extends SQLiteOpenHelper {

    // Columns used for Company Setup
    private static final String COLUMN_CID = "Id";
    private static final String COLUMN_COMPANY_NAME = "company_name";
    private static final String COLUMN_SHORT_NAME = "short_name";
    private static final String COLUMN_CONTACT_PERSON = "contact_person";
    private static final String COLUMN_EMAIL_ID = "email_id";
    private static final String COLUMN_MOBILE_NUMBER = "mobile_number";
    private static final String COLUMN_SUPPORT_NUMBER1 = "support_number1";
    private static final String COLUMN_SUPPORT_NUMBER2 = "support_number2";
    private static final String COLUMN_SUPPORT_EMAIL = "support_email";
    private final static String COLUMN_COMPANY_LOGO = "company_logo";
    //public static final String COLUMN_COMPANY_CREATED_AT = "created_at";
    //public static final String COLUMN_COMPANY_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public CompanySetupTable(@Nullable Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_LEDGER);

        // Create Company Setup Table
        String company_setup = "CREATE TABLE IF NOT EXISTS " + TABLE_COMPANY_SETUP + "(" +
                COLUMN_CID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_COMPANY_NAME + " TEXT ," +
                COLUMN_SHORT_NAME + " TEXT, " +
                COLUMN_CONTACT_PERSON + " INTEGER, " +
                COLUMN_EMAIL_ID + " TEXT, " +
                COLUMN_MOBILE_NUMBER + " TEXT ," +
                COLUMN_SUPPORT_NUMBER1 + " TEXT, " +
                COLUMN_SUPPORT_NUMBER2 + " TEXT, " +
                COLUMN_SUPPORT_EMAIL + " TEXT, " +
                COLUMN_COMPANY_LOGO + " BLOB NOT NULL " +
                ");";
        sqlWrite.execSQL(company_setup);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPANY_SETUP);

    }

    public long create(CompanySetup companySetup) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_SUPPORT_EMAIL, companySetup.support_email);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_COMPANY_SETUP, null, values);
        db.close();
        return id;

    }

    //Get CompanyDetails
    /*public ArrayList<CompanySetup> getCompanySetup() {

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select * from "+TABLE_COMPANY_SETUP, null);

        Log.d("QueryData cursor ",data.toString());

        ArrayList<CompanySetup> companySetupArrayList = new ArrayList<>();
        while(data.moveToNext())
        {
            CompanySetup cs = new CompanySetup();
            cs.setId(data.getInt(0));
            cs.setCompany_name(data.getString(1));
            cs.setShort_name(data.getString(2));
            cs.setContact_person(data.getString(3));
            cs.setEmail_id(data.getString(4));
            cs.setMobile_number(data.getString(5));
            cs.setSupport_number1(data.getString(6));
            cs.setSupport_number2(data.getString(7));
            cs.setSupport_email(data.getString(8));
            cs.setCompany_logo(data.getBlob(9));
            companySetupArrayList.add(cs);

            Log.d("Email ",data.getString(4));
        }
        return companySetupArrayList;
    }*/

}
