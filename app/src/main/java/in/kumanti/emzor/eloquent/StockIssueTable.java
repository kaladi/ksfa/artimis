package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.StockIssue;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_ISSUE;

public class StockIssueTable extends SQLiteOpenHelper {

    //Columns used for Stock Return Information details for a selected ordered product
    private static final String COLUMN_CUS_STOCK_ISSUE_ID = "stockIssueId";
    private static final String COLUMN_CUS_STOCK_ISSUE_NUMBER = "stockIssueNum";
    private static final String COLUMN_CUS_STOCK_ISSUE_DATE = "stockIssueDate";
    private static final String COLUMN_CUS_STOCK_ISSUE_BDE_CODE = "bdeCode";
    private static final String COLUMN_CUS_STOCK_ISSUE_PRODUCTS_COUNT = "issuedCount";
    private static final String COLUMN_CUS_STOCK_ISSUE_SIGNATURE = "signature";
    private final static String COLUMN_CUS_STOCK_ISSUE_RANDOM_NUM = "randomNumber";
    private final static String COLUMN_CUS_STOCK_ISSUE_EMAIL_SENT = "emailSent";
    private final static String COLUMN_CUS_STOCK_ISSUE_SMS_SENT = "smsSent";
    private final static String COLUMN_CUS_STOCK_ISSUE_PRINT_DONE = "printDone";
    private final static String COLUMN_CUS_STOCK_ISSUE_VISIT_SEQUENCE_NO = "visitSeqNo";
    private final static String COLUMN_CUS_STOCK_ISSUE_IS_SYNC = "isSync";
    private final static String COLUMN_CUS_STOCK_ISSUE_IS_EXPORTED = "isExported";
    private static final String COLUMN_CUS_STOCK_ISSUE_CREATED_AT = "createdAt";
    private static final String COLUMN_CUS_STOCK_ISSUE_UPDATED_AT = "updatedAt";
    private static final String COLUMN_CUS_STOCK_ISSUE_SIGNEE_NAME = "signeeName";

    public Context context;
    String selectQuery;
    Cursor cursor;
    private SQLiteDatabase sqlWrite;

    public StockIssueTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCK_ISSUE);

        String stockReturnTable = "CREATE TABLE IF NOT EXISTS " + TABLE_STOCK_ISSUE + "(" +
                COLUMN_CUS_STOCK_ISSUE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUS_STOCK_ISSUE_NUMBER + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_DATE + " TEXT, " +

                COLUMN_CUS_STOCK_ISSUE_BDE_CODE + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_SIGNATURE + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_PRODUCTS_COUNT + " INTEGER, " +

                COLUMN_CUS_STOCK_ISSUE_RANDOM_NUM + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_EMAIL_SENT + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_SMS_SENT + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_PRINT_DONE + " TEXT, " +

                COLUMN_CUS_STOCK_ISSUE_VISIT_SEQUENCE_NO + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_IS_SYNC + " TEXT, " +
                COLUMN_CUS_STOCK_ISSUE_IS_EXPORTED + " TEXT, " +

                COLUMN_CUS_STOCK_ISSUE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CUS_STOCK_ISSUE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CUS_STOCK_ISSUE_SIGNEE_NAME + " TEXT " +
                ");";
        sqlWrite.execSQL(stockReturnTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(StockIssue stockIssue) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUS_STOCK_ISSUE_NUMBER, stockIssue.getStockIssueNumber());
        values.put(COLUMN_CUS_STOCK_ISSUE_DATE, stockIssue.getStockIssueDate());

        values.put(COLUMN_CUS_STOCK_ISSUE_BDE_CODE, stockIssue.getBdeCode());
        values.put(COLUMN_CUS_STOCK_ISSUE_SIGNATURE, stockIssue.getBdeSignature());
        values.put(COLUMN_CUS_STOCK_ISSUE_PRODUCTS_COUNT, stockIssue.getIssueProductsCount());

        values.put(COLUMN_CUS_STOCK_ISSUE_RANDOM_NUM, stockIssue.getRandomNum());
        values.put(COLUMN_CUS_STOCK_ISSUE_IS_SYNC, stockIssue.isSync());
        values.put(COLUMN_CUS_STOCK_ISSUE_IS_EXPORTED, stockIssue.getIsExported());

        values.put(COLUMN_CUS_STOCK_ISSUE_EMAIL_SENT, stockIssue.getEmailSent());
        values.put(COLUMN_CUS_STOCK_ISSUE_SMS_SENT, stockIssue.getSmsSent());
        values.put(COLUMN_CUS_STOCK_ISSUE_PRINT_DONE, stockIssue.getPrintDone());

        values.put(COLUMN_CUS_STOCK_ISSUE_VISIT_SEQUENCE_NO, stockIssue.getVisitSeqNumber());

        values.put(COLUMN_CUS_STOCK_ISSUE_SIGNEE_NAME, stockIssue.getSigneeName());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_STOCK_ISSUE, null, values);
        db.close();
        return id;
    }

    public int getCustomerReturnNumber() {
        int customerReturnId = 0;
        String selectQuery = "select randomNumber from xxmsales_stock_issue where stockIssueDate=date('now') order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            customerReturnId = cursor.getInt(0);
            cursor.close();
        }
        return customerReturnId;
    }

    public ArrayList<StockIssue> getStockIssueDetailsForSync() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_STOCK_ISSUE;

        ArrayList<StockIssue> customerReturnArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {
            StockIssue stockIssue = new StockIssue();
            stockIssue.setTabCode(Constants.TAB_CODE);
            stockIssue.setCompanyCode(Constants.COMPANY_CODE);
            stockIssue.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            stockIssue.setStockIssueId(id);
            stockIssue.setStockIssueNumber(cursor.getString(1));
            stockIssue.setStockIssueDate(cursor.getString(2));
            stockIssue.setBdeCode(cursor.getString(3));
            stockIssue.setBdeSignature(cursor.getString(4));
            stockIssue.setVisitSeqNumber(cursor.getString(10));
            stockIssue.setCreatedAt(cursor.getString(13));
            stockIssue.setUpdatedAt(cursor.getString(14));

            customerReturnArrayList.add(stockIssue);
        }
        cursor.close();
        sqlWrite.close();
        return customerReturnArrayList;
    }


}
