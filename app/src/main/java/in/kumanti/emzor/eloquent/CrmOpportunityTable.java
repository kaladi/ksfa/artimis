package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.CrmOpportunity;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_OPPORTUNITY;

public class CrmOpportunityTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_OPPORTUNITY_ID = "opportunityId";
    private static final String COLUMN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_COLD_CALL_ID = "coldCallId";
    private static final String COLUMN_OPPORTUNITY_NUMBER = "opportunityNo";
    private static final String COLUMN_OPPORTUNITY_DATE = "opportunityDate";
    private static final String COLUMN_OPPORTUNITY_STATUS = "opportunityStatus";
    private static final String COLUMN_OPPORTUNITY_IS_SYNC = "isSync";
    private static final String COLUMN_OPPORTUNITY_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_OPPORTUNITY_RANDOM_NUMBER = "randomNumber";
    private static final String COLUMN_OPPORTUNITY_CREATED_AT = "createdAt";
    private static final String COLUMN_OPPORTUNITY_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmOpportunityTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_OPPORTUNITY);
        String opportunityTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_OPPORTUNITY + "(" +
                COLUMN_OPPORTUNITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_COLD_CALL_ID + " INTEGER, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_OPPORTUNITY_NUMBER + " TEXT, " +
                COLUMN_OPPORTUNITY_DATE + " TEXT, " +
                COLUMN_OPPORTUNITY_STATUS + " REAL, " +
                COLUMN_OPPORTUNITY_IS_SYNC + " TEXT, " +
                COLUMN_OPPORTUNITY_RANDOM_NUMBER + " TEXT, " +
                COLUMN_OPPORTUNITY_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_OPPORTUNITY_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_OPPORTUNITY_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(opportunityTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmOpportunity opportunity) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_COLD_CALL_ID, opportunity.getColdCallId());
        values.put(COLUMN_CUSTOMER_CODE, opportunity.getCustomerCode());
        values.put(COLUMN_OPPORTUNITY_NUMBER, opportunity.getOpportunityNo());
        values.put(COLUMN_OPPORTUNITY_DATE, opportunity.getOpportunityDate());
        values.put(COLUMN_OPPORTUNITY_STATUS, opportunity.getOpportunityStatus());
        values.put(COLUMN_OPPORTUNITY_IS_SYNC, opportunity.isSync());
        values.put(COLUMN_OPPORTUNITY_LATEST_SYNC_DATE, opportunity.getLatestSyncDate());
        values.put(COLUMN_OPPORTUNITY_RANDOM_NUMBER, opportunity.getRandomNumber());
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_OPPORTUNITY, null, values);
        db.close();
        return id;
    }



   /* public CrmOpportunity getLastOpportunity() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_OPPORTUNITY +" ORDER BY "+COLUMN_OPPORTUNITY_ID+" DESC LIMIT 1";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );
        int i = 0;
        CrmOpportunity op = null;
        while (cursor.moveToNext()) {
            op = new CrmOpportunity();
            op.setOpportunityId(cursor.getInt(0));
            op.setOpportunityNo(cursor.getString(3));
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return op;
    }*/


    public ArrayList<CrmOpportunity> getCrmOpportunity() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_OPPORTUNITY + " order by opportunityId desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        ArrayList<CrmOpportunity> crmOpportunityArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            CrmOpportunity opportunity = new CrmOpportunity();
            opportunity.setOpportunityId(cursor.getInt(0));
            opportunity.setColdCallId(cursor.getInt(1));
            opportunity.setCustomerCode(cursor.getString(2));
            opportunity.setOpportunityNo(cursor.getString(3));
            opportunity.setOpportunityDate(cursor.getString(4));
            opportunity.setOpportunityStatus(cursor.getString(5));

            crmOpportunityArrayList.add(i, opportunity);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return crmOpportunityArrayList;
    }


    public int getOpportunityNumber() {
        int opportunityId = 0;
        String selectQuery = "select randomNumber from xxmsales_crm_opportunity where opportunityDate=date('now') order by randomNumber desc LIMIT 1";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            opportunityId = cursor.getInt(0);
            cursor.close();
        }

        return opportunityId;
    }

}
