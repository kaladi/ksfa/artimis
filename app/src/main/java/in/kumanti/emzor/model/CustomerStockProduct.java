package in.kumanti.emzor.model;

import java.util.ArrayList;

public class CustomerStockProduct {

    public int id;
    public String tabCode;
    public String srCode;
    public String companyCode;
    public String customerCode;
    public String customerName;
    public String transactionType;
    public String transactionNumber;
    public String transactionDate;
    public String productCode;
    public String stockProductName;
    public boolean stockAvailability;
    public int stockVolume;
    public ArrayList<CompetitorStockProduct> competitorStockProductArrayList;
    public String notes;
    public String date;
    public String createdAt;
    public String updatedAt;
    public String visitSeqNumber;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTab_id() {
        return tabCode;
    }

    public void setTab_id(String tab_id) {
        this.tabCode = tab_id;
    }


    public ArrayList<CompetitorStockProduct> getCompetitorStockProductArrayList() {
        return competitorStockProductArrayList;
    }

    public void setCompetitorStockProductArrayList(ArrayList<CompetitorStockProduct> competitorStockProductArrayList) {
        this.competitorStockProductArrayList = competitorStockProductArrayList;
    }


    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public String getCustomer_id() {
        return customerCode;
    }

    public void setCustomer_id(String customer_id) {
        this.customerCode = customer_id;
    }

    public String getCustomer_name() {
        return customerName;
    }

    public void setCustomer_name(String customer_name) {
        this.customerName = customer_name;
    }

    public String getSales_rep_id() {
        return srCode;
    }

    public void setSales_rep_id(String sales_rep_id) {
        this.srCode = sales_rep_id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getProduct_id() {
        return productCode;
    }

    public void setProduct_id(String product_id) {
        this.productCode = product_id;
    }

    public String getStock_product_name() {
        return stockProductName;
    }

    public void setStock_product_name(String stock_product_name) {
        this.stockProductName = stock_product_name;
    }

    public boolean isStock_availability() {
        return stockAvailability;
    }

    public void setStock_availability(boolean stock_availability) {
        this.stockAvailability = stock_availability;
    }


    public Integer getStock_volume() {
        return stockVolume;
    }


    public void setStock_volume(int stock_volume) {
        this.stockVolume = stock_volume;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getVisitSeqNumber() {
        return visitSeqNumber;
    }

    public void setVisitSeqNumber(String visitSeqNumber) {
        this.visitSeqNumber = visitSeqNumber;
    }
}
