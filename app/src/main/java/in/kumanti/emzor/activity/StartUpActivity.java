package in.kumanti.emzor.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.Globals;

import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;

public class StartUpActivity extends AppCompatActivity {

    public static final int OVERLAY_PERMISSION_REQ_CODE = 4545;
    private static final int PERMISSION_REQUEST_CODE = 200;
    protected CustomViewGroup blockingView = null;
    ImageView info;
    Button SignIN;
    MyDBHandler dbHandler;
    Globals globals;
    TextView actionbarTitle, marqueeText;
    ImageView actionbarBackButton, deviceInfo, customerImage,centerCustomerImage,centerImage,headerImage,footerLogo;
    LinearLayout view;
    View decorView;
    String user_role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user_role = bundle.getString("user_role");
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "Please give my app this permission!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            } else {
                disableStatusBar();
            }
        } else {
            disableStatusBar();
        }

        setContentView(R.layout.activity_login_signup);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        File dbfile = new File(Constants.DATABASE_NAME);
        if (dbfile.exists() && !dbfile.isDirectory()) {

//            MyDBHandler dbHandler1 = new MyDBHandler(this, null, null, DATABASE_VERSION);
//            System.out.println("DATABASE_VERSION"+DATABASE_VERSION);
            Constants.COMPANY_NAME = "select company_name from xxmsales_company_setup order by Id desc limit 1";
        }

        initializeViews();

        System.out.println("Constants.COMPANY_CODE = " + Constants.COMPANY_CODE);

        customerType();
        populateHeaderDetails();
        setFooterDateTime();
        marqueeText.setSelected(true);

        if (!checkPermission()) {

            requestPermission();

        } else {


        }

        try {

            File filepath = Environment.getExternalStorageDirectory();
            File dir = new File(filepath.getAbsolutePath() + "/KSFA/csv/");
            dir.mkdirs();

            String fname = "OpenStock";
            File file = new File(dir, fname);
            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);

                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {

        }

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


        SignIN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!checkPermission()) {
                    requestPermission();
                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("login_type", "User");
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }


    private void populateHeaderDetails() {

    }

    private void initializeViews() {
        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        SignIN = findViewById(R.id.signin);
        centerCustomerImage  =  findViewById(R.id.centerCustomerImage);
        centerImage  =  findViewById(R.id.centerImage);
        headerImage  =  findViewById(R.id.headerImage);
        footerLogo = findViewById(R.id.footerLogo);
        view = findViewById(R.id.view);
        globals = ((Globals) getApplicationContext());


    }

    private void customerType(){
        artimis();
       /* if (Constants.COMPANY_NAME==null){
            artimis();
        }
        else {
            if (Constants.COMPANY_NAME.equals("VISTA INTERNATIONAL LTD")){
                vista();
            }else {
                artimis();
            }
        }*/

    }

    private void  artimis(){

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.gps_progress_on));
        }
        view.setBackgroundColor(this.getResources().getColor(R.color.green_vista));
        Resources res = getResources();
        centerCustomerImage.setImageDrawable(res.getDrawable(R.drawable.artemis_logo));
        centerImage.setImageDrawable(res.getDrawable(R.drawable.artemis_banner));
        headerImage.setImageDrawable(res.getDrawable(R.drawable.artemis_logo));
        footerLogo.setImageDrawable(res.getDrawable(R.drawable.artemis_logo));
        SignIN.setBackgroundResource(R.drawable.btn_primary_green);
    }
    private void  vista(){

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.blue_vista));
        }
        view.setBackgroundColor(this.getResources().getColor(R.color.blue_vista));
        Resources res = getResources();
        centerCustomerImage.setImageDrawable(res.getDrawable(R.drawable.vista_logo));
        centerImage.setImageDrawable(res.getDrawable(R.drawable.vista_banner));
        headerImage.setImageDrawable(res.getDrawable(R.drawable.vista_header));
       /* headerImage.getLayoutParams().height = 150;
        headerImage.getLayoutParams().width = 150;*/
        footerLogo.setImageDrawable(res.getDrawable(R.drawable.vista_footer));
        SignIN.setBackgroundResource(R.drawable.btn_primary_blue);
    }


    private void hideSystemUIView() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }


    protected void disableStatusBar() {

        WindowManager manager = ((WindowManager) StartUpActivity.this.getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            localLayoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY ;
        }

        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to receive touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (40 * getResources().getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        blockingView = new CustomViewGroup(this);
        manager.addView(blockingView, localLayoutParams);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, "User can access system settings without this permission!", Toast.LENGTH_SHORT).show();
                } else {
                    disableStatusBar();
                }
            }
        }
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();

        if (blockingView != null) {
            WindowManager manager = ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
            manager.removeView(blockingView);
        }
    }

    private void setFooterDateTime() {

        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(StartUpActivity.this, android.Manifest.permission.CAMERA);
        int result1 = ContextCompat.checkSelfPermission(StartUpActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(StartUpActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(StartUpActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(StartUpActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int result5 = ContextCompat.checkSelfPermission(StartUpActivity.this, Manifest.permission.INTERNET);
        int result6 = ContextCompat.checkSelfPermission(StartUpActivity.this, Manifest.permission.READ_PHONE_STATE);
        int result7 = ContextCompat.checkSelfPermission(StartUpActivity.this, Manifest.permission.SEND_SMS);
        int result8 = ContextCompat.checkSelfPermission(StartUpActivity.this, Manifest.permission.READ_SMS);
        int result9 = ContextCompat.checkSelfPermission(StartUpActivity.this, Manifest.permission.CALL_PHONE);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED && result4 == PackageManager.PERMISSION_GRANTED && result5 == PackageManager.PERMISSION_GRANTED
                && result6 == PackageManager.PERMISSION_GRANTED && result7 == PackageManager.PERMISSION_GRANTED && result9 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {

        ActivityCompat.requestPermissions(StartUpActivity.this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET, Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS,
                Manifest.permission.CALL_PHONE, Manifest.permission.BLUETOOTH}, PERMISSION_REQUEST_CODE);

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {


    }

}

