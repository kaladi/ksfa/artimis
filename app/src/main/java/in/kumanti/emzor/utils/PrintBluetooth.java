package in.kumanti.emzor.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Set;
import java.util.UUID;

public class PrintBluetooth implements Runnable {
    protected static final String TAG = "TAG";
    private static final int REQUEST_CONNECT_DEVICE = 21;
    private static final int REQUEST_ENABLE_BT = 22;
    String printerName = "MP80-17031810239";
    Activity activity;
    Context mContext;
    String printMsgContent;
    Bitmap bitmapImage;
    Print print;


    BluetoothAdapter mBluetoothAdapter;
    BluetoothDevice mBluetoothDevice;
    TextView stat;
    int printstat;
    LinearLayout layout;
    AlertDialog.Builder builder;
    private UUID applicationUUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ProgressDialog mBluetoothConnectProgressDialog;
    private BluetoothSocket mBluetoothSocket;
    private ProgressDialog loading;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mBluetoothConnectProgressDialog.dismiss();

            // Snackbar snackbar = Snackbar.make(layout, "Bluetooth Printer is Connected!", Snackbar.LENGTH_LONG);
            // snackbar.show();
            /*stat.setText("");
            stat.setText("Connected");
            stat.setTextColor(Color.rgb(97, 170, 74));*/
//            mPrint.setEnabled(true);
//            mScan.setText("Disconnect");
            //mDisc.setEnabled(true);
            //mDisc.setBackgroundColor(Color.rgb(0, 0, 0));
            //mScan.setEnabled(false);
            //mScan.setBackgroundColor(Color.rgb(161, 161, 161));

        }
    };


    public PrintBluetooth(Activity ac) {
        activity = ac;
    }

    public static byte intToByteArray(int value) {
        byte[] b = ByteBuffer.allocate(4).putInt(value).array();

        for (int k = 0; k < b.length; k++) {
            System.out.println("Selva  [" + k + "] = " + "0x"
                    + UnicodeFormatter.byteToHex(b[k]));
        }

        return b[3];
    }

    public void printContent(String printMsg, Bitmap image) {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            print = new Print(activity.getApplicationContext());
            if (mBluetoothAdapter == null) {
                Toast.makeText(activity.getApplicationContext(), "Message1", Toast.LENGTH_SHORT).show();
            } else {
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(
                            BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    activity.startActivityForResult(enableBtIntent,
                            REQUEST_ENABLE_BT);
                } else {
                    ListPairedDevices();
                    Intent connectIntent = new Intent(activity.getApplicationContext(),
                            DeviceListActivity.class);
                    activity.startActivityForResult(connectIntent,
                            REQUEST_CONNECT_DEVICE);
                    /*String mDeviceAddress = "00:12:6F:CC:AA:5B";
                    mBluetoothDevice = mBluetoothAdapter
                            .getRemoteDevice(mDeviceAddress);
                    *//*mBluetoothConnectProgressDialog = ProgressDialog.show(activity.getApplicationContext(),
                            "Connecting...", mBluetoothDevice.getName() + " : "
                                    + mBluetoothDevice.getAddress(), true, false);*//*
                    Thread mBlutoothConnectThread = new Thread(this);
                    mBlutoothConnectThread.start();*/


                }
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void p1() {

        Thread t = new Thread() {
            public void run() {
                try {
                    OutputStream os = mBluetoothSocket
                            .getOutputStream();
                    String printMsg;

                    String productName = "AZITHROMYCIN";
                    String qty = "56";
                    String rate = "661.73";
                    String amount = "37,056.88";
                    String totalAmount = "37,056.88";
                    int totalSize = 48;
                    String ESC_NEW_LINE = "\n";
                    String horizontalLine = "-----------------------------------------------";
                    printMsg = "";
                    printMsg += print.centerString(totalSize, "EMZOR");
                    printMsg += print.centerString(totalSize, "Plot 3C Block A Aswani Market Road,");
                    printMsg += print.centerString(totalSize, "Oshodi/Isolo Expressway,");
                    printMsg += print.centerString(totalSize, "Lagos State");
                    printMsg += print.centerString(totalSize, "Phone: +234-7080606000");

                    printMsg += ESC_NEW_LINE + ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + "Receipt Number:" + "A0020R07051906";
                    printMsg += ESC_NEW_LINE + print.padRight("Date:" + "May 7 19", 24) + print.padLeft("TIME:" + "12:00", 24);
                    printMsg += ESC_NEW_LINE + "BDE Name:" + "BLESSING USUWAH";
                    printMsg += ESC_NEW_LINE + "Customer Name:" + "NEW GATE HOSPITAL LTD";
                    printMsg += ESC_NEW_LINE + "Address:";
                    printMsg += print.centerString(totalSize, "LAGOS");
                    printMsg += print.centerString(totalSize, "LAYOUT ONITSHA");
                    //  printMsg += print.centerString( totalSize,customerAddress3);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += print.centerString(15, "Product Name") + "  " + " QTY " + print.padLeft("PRICE", 10) + "  " + print.padLeft("AMOUNT", 14);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += print.padLeft("1.", 3) + print.padRight(productName, 12) + "  " + print.padLeft(qty, 4) + " " + print.padLeft(rate, 10) + "  " + print.padLeft(amount, 14);
                    printMsg += print.padLeft("2.", 3) + print.padRight(productName, 12) + "  " + print.padLeft(qty, 4) + " " + print.padLeft(rate, 10) + "  " + print.padLeft(amount, 14);
                    printMsg += print.padLeft("3.", 3) + print.padRight(productName, 12) + "  " + print.padLeft(qty, 4) + " " + print.padLeft(rate, 10) + "  " + print.padLeft(amount, 14);
                    printMsg += print.padLeft("4.", 3) + print.padRight(productName, 12) + "  " + print.padLeft(qty, 4) + " " + print.padLeft(rate, 10) + "  " + print.padLeft(amount, 14);
                    printMsg += print.padLeft("5.", 3) + print.padRight(productName, 12) + "  " + print.padLeft(qty, 4) + " " + print.padLeft(rate, 10) + "  " + print.padLeft(amount, 14);

                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.centerString(34, "Payment Type:") + print.padLeft("", 14);
                    printMsg += print.centerString(34, "VAT:") + print.padLeft("", 14);
                    printMsg += print.centerString(34, "Discounts:") + print.padLeft("", 14);
                    printMsg += print.centerString(35, "Net Value:") + print.padLeft("", 13);


                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + print.padRight("Cash Paid:" + "250", 24) + print.padLeft("Balance:" + "11:00", 24);

                    printMsg += ESC_NEW_LINE + "Change Required:";
                    printMsg += ESC_NEW_LINE + "On Account Adjusted:" + "";

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + print.padRight("Customer Sign", 14);
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.centerString(48, "**** Thank You ****");
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    // print.printContent(printMsg);


                    os.write(printMsgContent.getBytes());


                    //This is printer specific code you can comment ==== > Start

                    // Setting height
                   /* int gs = 29;
                    os.write(intToByteArray(gs));
                    int h = 104;
                    os.write(intToByteArray(h));
                    int n = 162;
                    os.write(intToByteArray(n));

                    // Setting Width
                    int gs_width = 29;
                    os.write(intToByteArray(gs_width));
                    int w = 119;
                    os.write(intToByteArray(w));
                    int n_width = 2;
                    os.write(intToByteArray(n_width));
*/
                    try {
                        closeSocket(mBluetoothSocket);

                    } catch (Exception e) {
                        Log.e("Tag", "Exe ", e);
                    }
                } catch (Exception e) {
                    Log.e("PrintActivity", "Exe ", e);
                }
            }
        };
        t.start();
    }

    public void onActivityResult(int mRequestCode, int mResultCode,
                                 Intent mDataIntent, String printMsg) {
        //super.onActivityResult(mRequestCode, mResultCode, mDataIntent);

        switch (mRequestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (mResultCode == Activity.RESULT_OK) {
                    Bundle mExtra = mDataIntent.getExtras();
                    String mDeviceAddress = mExtra.getString("DeviceAddress");
                    Log.v(TAG, "Coming incoming address " + mDeviceAddress);
                    printMsgContent = printMsg;
                    mBluetoothDevice = mBluetoothAdapter
                            .getRemoteDevice(mDeviceAddress);
                    mBluetoothConnectProgressDialog = ProgressDialog.show(activity,
                            "Connecting...", mBluetoothDevice.getName() + " : "
                                    + mBluetoothDevice.getAddress(), true, false);
                    // Thread mBlutoothConnectThread = new Thread(this);
                    // mBlutoothConnectThread.start();
                    // pairToDevice(mBluetoothDevice); This method is replaced by
                    // progress dialog with thread
                }
                break;

            case REQUEST_ENABLE_BT:
                if (mResultCode == Activity.RESULT_OK) {
                    ListPairedDevices();
                    Intent connectIntent = new Intent(activity.getApplicationContext(),
                            DeviceListActivity.class);
                    activity.startActivityForResult(connectIntent, REQUEST_CONNECT_DEVICE);
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Message", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void ListPairedDevices() {
        Set<BluetoothDevice> mPairedDevices = mBluetoothAdapter
                .getBondedDevices();
        if (mPairedDevices.size() > 0) {
            for (BluetoothDevice mDevice : mPairedDevices) {
                Log.v(TAG, "PairedDevices: " + mDevice.getName() + "  "
                        + mDevice.getAddress());
            }
        }
    }

    public void run() {
        try {
            /*mBluetoothSocket = mBluetoothDevice
                    .createRfcommSocketToServiceRecord(applicationUUID);*/
//            mBluetoothSocket =(BluetoothSocket) mBluetoothDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(mBluetoothDevice,1);
//
//            mBluetoothAdapter.cancelDiscovery();
//            mBluetoothSocket.connect();
            mHandler.sendEmptyMessage(0);
            print = new Print(activity.getApplicationContext());
            print.printContent(printMsgContent);
            // printContent(printMsgContent, bitmapImage);
        } catch (Exception eConnectException) {
            Log.d(TAG, "CouldNotConnectToSocket", eConnectException);
            closeSocket(mBluetoothSocket);
            return;
        }
    }

    private void closeSocket(BluetoothSocket nOpenSocket) {
        try {
            nOpenSocket.close();
            Log.d(TAG, "SocketClosed");
        } catch (IOException ex) {
            Log.d(TAG, "CouldNotCloseSocket");
        }
    }

    public byte[] sel(int val) {
        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.putInt(val);
        buffer.flip();
        return buffer.array();
    }
}

class UnicodeFormatter {

    static public String byteToHex(byte b) {
        // Returns hex String representation of byte b
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        char[] array = {hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f]};
        return new String(array);
    }

    static public String charToHex(char c) {
        // Returns hex String representation of char c
        byte hi = (byte) (c >>> 8);
        byte lo = (byte) (c & 0xff);
        return byteToHex(hi) + byteToHex(lo);
    }

}