package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;
import java.util.ArrayList;

import in.kumanti.emzor.model.CompetitorStockProduct;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_COMPETITOR_PRODUCT;
import static in.kumanti.emzor.utils.Constants.TABLE_COMPETITOR_STOCK_PRODUCT;

public class CompetitorStockProductTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    public static final String COLUMN_CI_ID = "id";
    public static final String COLUMN_CI_PRODUCT_ID = "product_id";
    public static final String COLUMN_CI_COMPETITOR_STOCK_ID = "competitor_stock_id";
    public static final String COLUMN_CI_COMPETITOR_PRODUCT_ID = "competitor_product_id";
    public static final String COLUMN_CI_AVAILABILITY = "competitor_availability";
    public static final String COLUMN_CI_VOLUME = "competitor_volume";
    public static final String COLUMN_CI_PRICE = "competitor_price";
    public static final String COLUMN_CI_PROMOTION = "competitor_promotion";
    public static final String COLUMN_CI_CREATED_AT = "created_at";
    public static final String COLUMN_CI_UPDATED_AT = "updated_at";
    public static final String COLUMN_CI_IS_SYNC = "is_sync";


    public Context context;
    private SQLiteDatabase sqlWrite;
    String company_code="";
    String tab_code="";
    String sr_code="";
    Constants constants;

    public CompetitorStockProductTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        constants=new Constants();
        company_code= Constants.COMPANY_CODE;
        tab_code= Constants.TAB_CODE;
        sr_code= Constants.SR_CODE;
       // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPETITOR_STOCK_PRODUCT);
        String competitorInfoDetailTable = "CREATE TABLE IF NOT EXISTS " + TABLE_COMPETITOR_STOCK_PRODUCT + "(" +
                COLUMN_CI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CI_PRODUCT_ID + " TEXT, " +
                COLUMN_CI_COMPETITOR_STOCK_ID+" TEXT, "+
                COLUMN_CI_COMPETITOR_PRODUCT_ID + " TEXT, " +
                COLUMN_CI_AVAILABILITY + " BOOLEAN, " +
                COLUMN_CI_VOLUME + " REAL, " +
                COLUMN_CI_PRICE + " REAL, " +
                COLUMN_CI_PROMOTION + " TEXT, " +
                COLUMN_CI_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CI_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CI_IS_SYNC + " TEXT " +

                ");";

          Log.d("CompetitorInfo","Database Created"+DATABASE_NAME);

        sqlWrite.execSQL(competitorInfoDetailTable);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CompetitorStockProduct competitor){
        ContentValues values = new ContentValues();
        values.put(COLUMN_CI_PRODUCT_ID, competitor.getProduct_id());
        values.put(COLUMN_CI_COMPETITOR_STOCK_ID, competitor.getCompetitor_stock_id());
        values.put(COLUMN_CI_COMPETITOR_PRODUCT_ID, competitor.getCompetitor_product_id());
        values.put(COLUMN_CI_AVAILABILITY, competitor.getCompetitor_product_availability());
        values.put(COLUMN_CI_VOLUME, competitor.getCompetitor_product_volume());
        values.put(COLUMN_CI_PRICE, competitor.getCompetitor_product_price());
        values.put(COLUMN_CI_PROMOTION, competitor.getCompetitor_product_promotion());
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_COMPETITOR_STOCK_PRODUCT, null, values);
        db.close();
        return id;
    }

    public ArrayList<CompetitorStockProduct> getCompetitorProductsById(String productId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_COMPETITOR_STOCK_PRODUCT + " WHERE "+COLUMN_CI_PRODUCT_ID+"=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {productId} );
        Log.d("Data","getCompetitorDetailsByProductId  " + productId);
        ArrayList<CompetitorStockProduct> competitorStockProductArrayList = new ArrayList<CompetitorStockProduct>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            CompetitorStockProduct cd = new CompetitorStockProduct();
            cd.setId(cursor.getInt(0));
            cd.setProduct_id(cursor.getString(1));
            cd.setCompetitor_stock_id(cursor.getString(3));
            cd.setCompetitor_product_id(cursor.getString(3));
            cd.setCompetitor_product_availability(cursor.getInt(4) > 0);
            cd.setCompetitor_product_volume(cursor.getInt(5));
            cd.setCompetitor_product_price(cursor.getDouble(6));
            cd.setCompetitor_product_promotion(cursor.getString(7));
            competitorStockProductArrayList.add(i,cd);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return competitorStockProductArrayList;
    }

    public ArrayList<CompetitorStockProduct> getCompetitorProductsByStockId(String competitorstockId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT tcsp.id,tcsp.product_id,tcsp.competitor_stock_id,tcsp.competitor_product_id,tcsp.competitor_availability,tcsp.competitor_volume,tcsp.competitor_price,tcsp.competitor_promotion,tcp.productName FROM " + TABLE_COMPETITOR_STOCK_PRODUCT + " as tcsp LEFT JOIN "+TABLE_COMPETITOR_PRODUCT+" as tcp ON tcsp.competitor_product_id = tcp.productCode WHERE "+COLUMN_CI_COMPETITOR_STOCK_ID+"=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {competitorstockId} );
        Log.d("Data","getCompetitorDetailsByProductId  " + competitorstockId);
        ArrayList<CompetitorStockProduct> competitorStockProductArrayList = new ArrayList<CompetitorStockProduct>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            CompetitorStockProduct cd = new CompetitorStockProduct();
            cd.setId(cursor.getInt(0));
            cd.setProduct_id(cursor.getString(1));
            cd.setCompetitor_stock_id(cursor.getString(3));
            cd.setCompetitor_product_id(cursor.getString(3));
            cd.setCompetitor_product_availability(cursor.getInt(4) > 0);
            cd.setCompetitor_product_volume(cursor.getInt(5));
            cd.setCompetitor_product_price(cursor.getDouble(6));
            cd.setCompetitor_product_promotion(cursor.getString(7));
            cd.setProductName(cursor.getString(8));
            competitorStockProductArrayList.add(i,cd);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return competitorStockProductArrayList;
    }

    public ArrayList<CompetitorStockProduct> getAllCompetitorStockProductsForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_COMPETITOR_STOCK_PRODUCT ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );
        Log.d("Data","getCompetitorDetailsByProductId  "  );
        ArrayList<CompetitorStockProduct> competitorStockProductArrayList = new ArrayList<CompetitorStockProduct>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            CompetitorStockProduct cd = new CompetitorStockProduct();
            cd.setTabCode(tab_code);
            cd.setSrCode(sr_code);
            cd.setCompanyCode(company_code);
            cd.setId(cursor.getInt(0));
            cd.setProduct_id(cursor.getString(1));
            cd.setCompetitor_stock_id(cursor.getString(2));
            cd.setCompetitor_product_id(cursor.getString(3));
            cd.setCompetitor_product_availability(cursor.getInt(4) > 0);
            cd.setCompetitor_product_volume(cursor.getInt(5));
            cd.setCompetitor_product_price(cursor.getDouble(6));
            cd.setCreatedAt(cursor.getString(8));
            cd.setUpdatedAt(cursor.getString(9));

            competitorStockProductArrayList.add(i,cd);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return competitorStockProductArrayList;
    }

    public void deleteByProduct(String competitorStockId){
        SQLiteDatabase db = this.getWritableDatabase();

        // Deleting all records from database table
        db.delete(TABLE_COMPETITOR_STOCK_PRODUCT, COLUMN_CI_COMPETITOR_STOCK_ID+ "='" + competitorStockId+"'", null);
        db.close();
    }
}
