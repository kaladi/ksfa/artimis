package in.kumanti.emzor.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.StockAdjustBatchAdapter;
import in.kumanti.emzor.adapter.StockIssueBatchAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.StockIssueBatch;
import in.kumanti.emzor.model.StockReceiptBatch;


public class StockAdjustBatchActivity extends MainActivity {

    public RecyclerView batchRecyclerView;
    MyDBHandler myDBHandler;
    String login_id = "", product_id, product_name, marqueeTextString = "";
    int position = 0;
    Integer returnQuantity;
    ImageView actionbarBackButton, deviceInfo;
    ArrayList<StockIssueBatch> stockIssueBatchArrayList = new ArrayList<StockIssueBatch>();
    TextView productNameTv, productQuantityTv;
    ImageButton saveIssueBatchImageButton,addBatchRowButton;
    private TextView marqueeText, actionbarTitle;
    private RecyclerView.Adapter batchAdapter;
    private RecyclerView.LayoutManager batchLayoutManager;

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_stock_adjust_batch);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Batch");
        marqueeText.setSelected(true);

        marqueeText.setText(marqueeTextString);

        //Populate the Quotation Product details using the Recycler view
        batchRecyclerView.setHasFixedSize(true);
        batchLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        batchRecyclerView.setLayoutManager(batchLayoutManager);

        try {

            batchAdapter = new StockAdjustBatchAdapter(stockIssueBatchArrayList, null);
            batchRecyclerView.setAdapter(batchAdapter);

        } catch (Exception e) {
            Log.d("QueryDataResponse", "" + e.getMessage());
        }


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent output = new Intent();
                output.putExtra("product_id", product_id);
                output.putExtra("product_batch_details", stockIssueBatchArrayList);
                setResult(Activity.RESULT_OK, output);
                finish();
            }
        });

        addBatchRowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBatchRow();
            }
        });


        saveIssueBatchImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideSoftKeyboard(getApplicationContext(), v);
                boolean resetflag = false;
              /*  View current = getCurrentFocus();
                current.clearFocus();*/

                InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);

                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }

                if (validateInputs(0)) {
                    Log.d("InputError", "Invalid Inputs");
                    batchAdapter.notifyDataSetChanged();
                } else {
                    Intent output = new Intent();
                    output.putExtra("product_id", product_id);
                    output.putExtra("product_batch_details", stockIssueBatchArrayList);
                    setResult(Activity.RESULT_OK, output);
                    finish();
                }
            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);
            }
        });


    }

    private void addBatchRow() {
        if (validateInputs(1)) //Invalid Inputs
        {
            Log.d("InputError", "Invalid Inputs");
        } else {
            StockIssueBatch srbd = new StockIssueBatch();
            stockIssueBatchArrayList.add(srbd);
        }
        batchAdapter.notifyDataSetChanged();
    }


    private boolean validateInputs(int totalCountCheckFlag) {
        boolean resetflag = false;
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(
                Context.INPUT_METHOD_SERVICE);

        View focusedView = getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        for (StockIssueBatch crb : stockIssueBatchArrayList) {
            String batchQuantity ="0";
            if((crb.getBatchQuantity())==null || crb.getBatchQuantity().equals(""))
            {
                batchQuantity ="0";
            }else{
                batchQuantity = crb.getBatchQuantity();
            }

//            if (!TextUtils.isEmpty(crb.getIssueQuantity()) ) {
//                crb.setErrorIssueQuantity("Please enter return quantity less than product quantity");
//                resetflag = true;
//            } else
//                crb.setErrorIssueQuantity("");

        }

        Integer currentTotalQuanitity = 0;

        for (StockIssueBatch crb : stockIssueBatchArrayList) {
            currentTotalQuanitity += Integer.parseInt(TextUtils.isEmpty(crb.getIssueQuantity()) ? "0" : crb.getIssueQuantity());
        }

        if (totalCountCheckFlag == 1 && currentTotalQuanitity == returnQuantity) {
            Toast.makeText(getApplicationContext(), "Total Quantity already Equal", Toast.LENGTH_SHORT).show();
            resetflag = true;
        }
        if (totalCountCheckFlag == 0 && currentTotalQuanitity < returnQuantity) {
            Toast.makeText(getApplicationContext(), "Sum of Batch Quantities Cannot be Less than Product Quantity", Toast.LENGTH_SHORT).show();
            resetflag = true;
        }
        if (currentTotalQuanitity > returnQuantity) {
            Toast.makeText(getApplicationContext(), "Sum of Batch Quantities Cannot be Greater than Product Quantity", Toast.LENGTH_SHORT).show();
            resetflag = true;
        }
        return resetflag;
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        batchRecyclerView = findViewById(R.id.stockIssueBatchRecyclerView);
        addBatchRowButton = findViewById(R.id.addBatchRowImageButton);
        saveIssueBatchImageButton = findViewById(R.id.stockIssueBatchImageButton);

        productNameTv = findViewById(R.id.stockIssueBatchProductNameTv);
        productQuantityTv = findViewById(R.id.stockIssueBatchProductQtyTv);

        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

    }


    public void populateHeaderDetails() {
        marqueeTextString = sharedPreferences.getString("MarqueeText", "");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            login_id = bundle.getString("login_id");
            //Log.d("LoginId",login_id);
            product_id = (String) bundle.get("product_id");
            product_name = (String) bundle.get("product_name");
            returnQuantity = Integer.parseInt((String) bundle.get("product_quantity"));
            stockIssueBatchArrayList = (ArrayList<StockIssueBatch>) bundle.get("product_batch_details");
            productNameTv.setText(product_name);
            productQuantityTv.setText(returnQuantity.toString());
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

}
