package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.OldOpportunity;
import in.kumanti.emzor.model.OpportunityProductDetails;

public class OpportunityPastTransactionsAdapter extends RecyclerView.Adapter<OpportunityPastTransactionsAdapter.ViewHolder> {

    public ArrayList<OldOpportunity> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;

    public OpportunityPastTransactionsAdapter(ArrayList<OldOpportunity> myDataset) {
        mDataset = myDataset;
    }

    public void add(OldOpportunity item1, int position) {
        mDataset.add(position, item1);
        Log.d("Recycle", "Data set Add" + position + "--" + mDataset.size());
        notifyItemInserted(position);
    }

    public void remove(OpportunityProductDetails item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public OpportunityPastTransactionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_opportunity_past_transactions, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        viewHolder.opportunityNotesList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size()) {
                    mDataset.remove(position);
                    notifyDataSetChanged();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView opportunityNumber, enquiryNumber, enquiryDate, quotationNumber, quotationDate, productValue, opportunityStatus;
        public ImageView opportunityNotesList;

        public ViewHolder(View v) {

            super(v);
            opportunityNumber = v.findViewById(R.id.pastOpportunityNumberTextView);
            enquiryNumber = v.findViewById(R.id.opportunityEnquiryNumberTextView);
            enquiryDate = v.findViewById(R.id.opportunityEnquiryDateTextView);
            quotationNumber = v.findViewById(R.id.opportunityQuotationNumberTextView);
            quotationDate = v.findViewById(R.id.opportunityQuotationDateTextView);
            productValue = v.findViewById(R.id.opportunityTotalValueTextView);
            opportunityStatus = v.findViewById(R.id.opportunityStatusTextView);
            opportunityNotesList = v.findViewById(R.id.opportunityNotesListImageView);
        }
    }


}