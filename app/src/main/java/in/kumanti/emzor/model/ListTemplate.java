package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class ListTemplate {
    private String salesorder_id;
    private String template_name;


    public ListTemplate(String salesorder_id1, String template_name1) {
        salesorder_id = salesorder_id1;
        template_name = template_name1;

    }


    public String getSalesorder_id() {
        return salesorder_id;
    }

    public void setSalesorder_id(String salesorder_id1) {
        salesorder_id = salesorder_id1;
    }

    public String getTemplate_name() {
        return template_name;
    }

    public void setTemplate_name(String template_name1) {
        template_name = template_name1;
    }

}

