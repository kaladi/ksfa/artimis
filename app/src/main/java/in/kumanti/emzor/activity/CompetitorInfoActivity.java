package in.kumanti.emzor.activity;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.CompetitorProductTable;
import in.kumanti.emzor.eloquent.CompetitorStockProductTable;
import in.kumanti.emzor.eloquent.CustomerStockProductTable;
import in.kumanti.emzor.model.CompetitorStockProduct;
import in.kumanti.emzor.model.CustomerStockProduct;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import in.kumanti.emzor.model.CompetitorProduct;
import in.kumanti.emzor.utils.InputFilterMinMax;
import in.kumanti.emzor.adapter.CompetitorStockProductAdapter;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class CompetitorInfoActivity extends MainActivity {

    public String productName, productCode, productUom;
    public Double productPrice;
    public TextView productNameTextView, productCodeTextView, productPriceTextView, productUomTextView,customerName,sr_name,checkInTimeText;
    public EditText stockVolume,createCompetitorInfoNotes;
    public CheckBox stockAvailability;
    public ImageButton saveCompetitorInfoButton;
    public Button notesCompetitorButton;
    public TextView accountNo, city, actionBarTitle;
    public ImageButton competitor_details_add_button;
    public ImageView sr_image,customerImage, deviceInfo, actionBarBack;
    public RecyclerView CompetitorProductRecyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;
    CompetitorStockProductTable ctTable ;
    CustomerStockProductTable CustomerStockProductTable;
    ArrayList<CompetitorStockProduct> competitorStockProductArrayList =   new ArrayList<CompetitorStockProduct>();
    public ImageButton saveNotesButton, cancelNotesButton;
    CustomSearchableSpinner competitor_name_spinner;
    public String competitorInfoNotesText;
    ArrayList<CompetitorProduct> competitorListArrayProduct;

    String customer_id = "";
    String  ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    MyDBHandler dbHandler;
    String checkin_time = "";
    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;
    String login_id = "";
    String order_id = "";
    String invoice_id = "" , transactionType = "", transactionDate = "", transactionNumber = "";
    private TextView marqueeText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_competitor_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText("Competitor Info");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText","");
        marqueeText.setText(marquee_txt);
        //Populate CompetitorInfo Product details in header section
        productNameTextView.setText(productName);
        productCodeTextView.setText(productCode);
        productPriceTextView.setText(String.valueOf(productPrice));
        productUomTextView.setText(productUom);

        stockVolume.setFilters(new InputFilter[]{ new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
        //Populate Competitor Stock Details
        CustomerStockProduct competStockDetails = CustomerStockProductTable.getStockProductsByTransactionType(transactionType, productCode, customer_id);
        if(competStockDetails!=null) {
            stockAvailability.setChecked(competStockDetails.isStock_availability());
            if(competStockDetails.isStock_availability())
                stockVolume.setEnabled(true);
            stockVolume.setText((competStockDetails.getStock_volume().toString()));
            competitorInfoNotesText = competStockDetails.notes;

            competitorStockProductArrayList = ctTable.getCompetitorProductsByStockId(String.valueOf(competStockDetails.getId()));
        }

        CompetitorProductTable pclt = new CompetitorProductTable(getApplicationContext());
        //Populate Competitor Details
        competitorListArrayProduct = pclt.getCompetitorProductsListByProductCode(productCode);
        CompetitorProduct cp = new CompetitorProduct();
        cp.setProductCode("-1");
        cp.setProductName("Select Product");

        competitor_name_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                competitor_name_spinner.isSpinnerDialogOpen = false;

                CompetitorProduct p = (CompetitorProduct) adapterView.getItemAtPosition(pos);
                if(p.getProductCode().equals("-1")) {
                    return;
                }
                Log.d("DataView", "Details");
                if(competitorStockProductArrayList.size() >= competitorListArrayProduct.size()) {
                    Toast.makeText(getApplicationContext(), "Competitor Products is not available", Toast.LENGTH_SHORT).show();
                    competitor_details_add_button.setEnabled(true);
                    return;
                }
                CompetitorStockProduct cd = new CompetitorStockProduct();
                Log.d("DataView", "Details" + cd.toString());
                cd.setProduct_id(productCode);
                cd.setCompetitor_product_id(p.getProductCode());
                cd.setProductName(p.getProductName());
                cd.setCompetitor_product_volume(cd.getCompetitor_product_volume());
                cd.setCompetitor_product_price(cd.getCompetitor_product_price());
                cd.setCompetitor_product_availability(cd.getCompetitor_product_availability());
                competitorStockProductArrayList.add(cd);
                recyclerViewAdapter = new CompetitorStockProductAdapter(competitorStockProductArrayList, competitorListArrayProduct);
                CompetitorProductRecyclerView.setAdapter(recyclerViewAdapter);

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                competitor_name_spinner.isSpinnerDialogOpen = false;

            }
        });
        recyclerViewAdapter = new CompetitorStockProductAdapter(competitorStockProductArrayList, competitorListArrayProduct);
        CompetitorProductRecyclerView.setAdapter(recyclerViewAdapter);


        stockAvailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stockAvailability.isChecked())
                    stockVolume.setEnabled(true);
                else {
                    stockVolume.setText(null);
                    stockVolume.setEnabled(false);
                }
            }
        });


        competitor_details_add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                competitor_details_add_button.setEnabled(false);
                try {
                    if(competitorStockProductArrayList.size() >= competitorListArrayProduct.size()) {
                        Toast.makeText(getApplicationContext(), "Competitor Products is not available", Toast.LENGTH_SHORT).show();
                        competitor_details_add_button.setEnabled(true);
                        return;
                    }
                    ArrayList<CompetitorProduct> productdisplayList = new ArrayList<CompetitorProduct>();
                    for(CompetitorProduct pcl: competitorListArrayProduct)
                    {
                        boolean found = false;
                        int i =0;
                        for(CompetitorStockProduct pc: competitorStockProductArrayList){
                            if(pc.getCompetitor_product_id()!=null && pcl.getProductCode().equals(pc.getCompetitor_product_id()))
                            {
                                found = true;
                            }
                            i++;
                        }
                        if(!found) {
                            productdisplayList.add(pcl);
                        }
                    }
                    CompetitorProduct cp = new CompetitorProduct();
                    cp.setProductName("Select Competitor Product");
                    cp.setProductCode("-1");
                    productdisplayList.add(0,cp);
                    ArrayAdapter<CompetitorProduct> spinnerArrayAdapter = new ArrayAdapter<CompetitorProduct>(getApplicationContext(),android.R.layout.simple_spinner_item, productdisplayList);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                    competitor_name_spinner.setAdapter(spinnerArrayAdapter);
                    MotionEvent motionEvent = MotionEvent.obtain( 0, 0, MotionEvent.ACTION_UP, 0, 0, 0 );
                    if(!competitor_name_spinner.isFocused())
                        competitor_name_spinner.dispatchTouchEvent(motionEvent);
                }
                catch (Exception e)
                {

                }

                competitor_details_add_button.setEnabled(true);
                /*
                if(competitorStockProductArrayList.size() >= competitorListArrayProduct.size()) {
                    Toast.makeText(getApplicationContext(), "All the items added", Toast.LENGTH_SHORT).show();
                    competitor_details_add_button.setEnabled(true);
                    return;
                }
                CompetitorStockProduct cd = new CompetitorStockProduct();
                Log.d("Data Competitor ", "Details" + cd.toString());
                cd.setProduct_id(productCode);
               // cd.setCompetitor_product_id(spinnerMap.get(competitorProduct.getSelectedItemPosition()));
                cd.setCompetitor_product_volume(cd.getCompetitor_product_volume());
                cd.setCompetitor_product_price(cd.getCompetitor_product_price());
                cd.setCompetitor_product_availability(cd.getCompetitor_product_availability());
                competitorStockProductArrayList.add(cd);
                recyclerViewAdapter = new CompetitorStockProductAdapter(competitorStockProductArrayList, competitorListArrayProduct);
                CompetitorProductRecyclerView.setAdapter(recyclerViewAdapter);
                */
            }
        });

        saveCompetitorInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCompetitorInfoButton.setEnabled(false);
                hideSoftKeyboard(getApplicationContext(),v);
                boolean resetflag = false;
                View current = getCurrentFocus();
                current.clearFocus();
                if(stockAvailability.isChecked()&& TextUtils.isEmpty(stockVolume.getText()))
                {
                    stockVolume.setError("Please enter the stock volume");
                    resetflag = true;
                }
                else
                {
                    stockVolume.setError(null);
                }
                for(CompetitorStockProduct cd: competitorStockProductArrayList){
                    if(cd.getCompetitor_product_availability())
                    {
                        Log.d("CDSTOCK",cd.getCompetitor_product_id()+" "+cd.getCompetitor_product_price()+" "+ cd.getCompetitor_product_volume()+" "+cd.getCompetitor_product_promotion());
                        if(cd.getCompetitor_product_price()==null || cd.getCompetitor_product_price().equals("") || cd.getCompetitor_product_price()==0.0){
                            cd.setError_competitor_product_price("Please enter value");
                            resetflag = true;
                        }
                        else
                            cd.setError_competitor_product_price("");
                        if(cd.getCompetitor_product_volume()==null || cd.getCompetitor_product_volume().equals("")|| cd.getCompetitor_product_volume()==0){
                            cd.setError_competitor_product_volume("Please enter value");
                            resetflag = true;
                        }
                        else
                            cd.setError_competitor_product_volume("");
                        /*if(cd.getCompetitor_product_promotion()==null || cd.getCompetitor_product_promotion().equals("")){
                            cd.setError_competitor_product_promotion("Please enter value");
                            resetflag = true;
                        }
                        else
                            cd.setError_competitor_product_promotion("");*/
                    }
                    else
                    {
                        cd.setError_competitor_product_price("");
                        cd.setError_competitor_product_volume("");
                        cd.setError_competitor_product_promotion("");
                    }
                }
                if(resetflag)
                {
                    saveCompetitorInfoButton.setEnabled(true);
                    recyclerViewAdapter = new CompetitorStockProductAdapter(competitorStockProductArrayList, competitorListArrayProduct);
                    CompetitorProductRecyclerView.setAdapter(recyclerViewAdapter);
                }
                else
                {
                    long competitor_stock_id = -1L;
                    CustomerStockProduct csd = CustomerStockProductTable.getStockProductsByTransactionType(transactionNumber, productCode, customer_id);
                    if(csd!=null)
                    {
                        csd.setStock_availability(stockAvailability.isChecked());
                        if(stockAvailability.isChecked())
                            csd.setStock_volume(Integer.parseInt(stockVolume.getText().toString()));
                        csd.setNotes(competitorInfoNotesText);
                        CustomerStockProductTable.update(String.valueOf(csd.id),csd);
                        competitor_stock_id = csd.id;
                    }
                    else
                    {
                        CustomerStockProduct customerStockProduct = new CustomerStockProduct();
                        customerStockProduct.setSales_rep_id(login_id);
                        customerStockProduct.setTab_id(tab_code);
                        customerStockProduct.setTransactionType(transactionType);
                        customerStockProduct.setTransactionDate(transactionDate);
                        customerStockProduct.setTransactionNumber(transactionNumber);

                        customerStockProduct.setProduct_id(productCode);
                        customerStockProduct.setCustomer_id(customer_id);
                        customerStockProduct.setStock_availability(stockAvailability.isChecked());
                        customerStockProduct.setNotes(competitorInfoNotesText);
                        if("".equals(stockVolume.getText().toString())) {

                        } else{
                            customerStockProduct.setStock_volume(Integer.parseInt(stockVolume.getText().toString()));

                        }
                        competitor_stock_id = CustomerStockProductTable.create(customerStockProduct);
                    }

                    ctTable.deleteByProduct(String.valueOf(competitor_stock_id));
                    for(CompetitorStockProduct cd: competitorStockProductArrayList){
                        cd.setCompetitor_stock_id(String.valueOf(competitor_stock_id));
                        ctTable.create(cd);
                    }
                    finish();
                }

            }
        });


        notesCompetitorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCompetitorInfoNotesEditor();
            }

        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        actionBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }

        });

    }


    public  void initializeViews() {

        actionBarTitle =  findViewById(R.id.toolbar_title);
        actionBarBack = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTimeText = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);




        productNameTextView = findViewById(R.id.product_name_textView);
        productCodeTextView = findViewById(R.id.product_code_textView);
        productPriceTextView = findViewById(R.id.product_price_textView);
        productUomTextView = findViewById(R.id.product_uom_textView);
        stockAvailability = findViewById(R.id.stock_availability_checkbox);
        stockVolume = findViewById(R.id.stock_volume_editText);
        saveCompetitorInfoButton = findViewById(R.id.competitor_save_button);
        notesCompetitorButton = findViewById(R.id.competitor_notes_button);
        CompetitorProductRecyclerView = findViewById(R.id.competitor_details_list);
        competitor_details_add_button = findViewById(R.id.competitor_details_add_button);
        competitor_name_spinner = findViewById(R.id.competitor_name_spinner);
        CompetitorProductRecyclerView.setHasFixedSize(true);
        recyclerViewLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        CompetitorProductRecyclerView.setLayoutManager(recyclerViewLayoutManager);
        marqueeText =  findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        ctTable = new CompetitorStockProductTable(getApplicationContext());
        CustomerStockProductTable = new CustomerStockProductTable(getApplicationContext());

        constants = new Constants();
        tab_code = Constants.TAB_CODE;


    }

    private void showCompetitorInfoNotesEditor() {
        View editingNotesToolView = getLayoutInflater().inflate(R.layout.notes_layout, null, false);

        createCompetitorInfoNotes = editingNotesToolView.findViewById(R.id.notesEditText);
        saveNotesButton =  editingNotesToolView.findViewById(R.id.saveNotesButton);
        cancelNotesButton = editingNotesToolView.findViewById(R.id.cancelNotesButton);

        createCompetitorInfoNotes.setText(competitorInfoNotesText);
        Log.d("Notes", "ShowNotesEditor: " );
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setView(editingNotesToolView);
        final android.app.AlertDialog alert = builder.create();
        Log.d("Notes", "ShowNotesEditor Alert: " + alert.toString() );
        alert.show();

        saveNotesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(getApplicationContext(),v);
                competitorInfoNotesText = createCompetitorInfoNotes.getText().toString();
                alert.dismiss();

            }
        });


        cancelNotesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(getApplicationContext(),v);
                alert.cancel();

            }
        });



    }
    public static void hideSoftKeyboard (Context context, View view)
    {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }


    /*private void update_order_status(String status) {
        dbHandler.update_order_header_status(Integer.toString(Integer.parseInt(order_id)), null, null, status,null,0,null,null);
        dbHandler.update_order_detail_status(Integer.toString(Integer.parseInt(order_id)),status);

        Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);

        //Create the bundle
        Bundle bundle = new Bundle();
        //Add your data to bundle
        bundle.putString("checkin_time", checkin_time);
        bundle.putString("customer_id", customer_id);
        bundle.putString("sales_rep_id", login_id);

        //Add the bundle to the intent
        myintent.putExtras(bundle);

        //Fire that second activity
        startActivity(myintent);
    } */

    private void populateHeaderDetails() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();
        checkInTimeText.setText(checkin_time);

        Bundle competitorBundleData = getIntent().getExtras();
        productName = competitorBundleData.getString("product_name");
        productCode = competitorBundleData.getString("product_id");
        productPrice = competitorBundleData.getDouble("price");
        productUom = competitorBundleData.getString("product_uom");
        customer_id = competitorBundleData.getString("customer_id");
        checkin_time = competitorBundleData.getString("checkin_time");
        order_id = competitorBundleData.getString("order_id");

        transactionType = competitorBundleData.getString("transactionType");
        transactionDate = competitorBundleData.getString("transactionDate");
        transactionNumber = competitorBundleData.getString("transactionNumber");


        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            order_id = bundle.getString("order_id");
            invoice_id = bundle.getString("invoice_id");

            transactionType = bundle.getString("transactionType");
            transactionDate = bundle.getString("transactionDate");
            transactionNumber = bundle.getString("transactionNumber");
            productName = bundle.getString("product_name");


            Log.d("InfoDetails","Order Id "+order_id+" Invoice Id"+invoice_id);
        }

        checkInTimeText.setText(checkin_time);


        //Populate Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new  File(String.valueOf(dir));

                if(imgFile.exists()){

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }

    }



    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

}
