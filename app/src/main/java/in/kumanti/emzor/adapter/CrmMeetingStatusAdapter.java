package in.kumanti.emzor.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CrmMeeting;

public class CrmMeetingStatusAdapter extends RecyclerView.Adapter<CrmMeetingStatusAdapter.ViewHolder> {

    public ArrayList<CrmMeeting> mDataset;
    public Context context;

    public CrmMeetingStatusAdapter(ArrayList<CrmMeeting> myDataset) {
        mDataset = myDataset;
    }

    public void add(CrmMeeting item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmMeeting item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public CrmMeetingStatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_crm_meeting_status, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder viewHolder, final int position) {
        boolean isEditable = true;
        if(mDataset.get(position).getMeetingId()!=0) {
            viewHolder.tvMeetingDate.setEnabled(false);
            viewHolder.tvMeetingDate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            viewHolder.etMeeting.setEnabled(false);
            viewHolder.etStatus.setEnabled(false);
            viewHolder.etRemarks.setEnabled(false);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getError_MeetingDate())) {
            viewHolder.tvMeetingDate.setError(mDataset.get(position).getError_MeetingDate());
        } else {
            viewHolder.tvMeetingDate.setError(null);
        }

        viewHolder.tvMeetingDate.setOnClickListener(v -> getInspectionDatePickerView(viewHolder.tvMeetingDate, mDataset.get(position)));

        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getMeetingDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.tvMeetingDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }

        viewHolder.etMeeting.setText(mDataset.get(position).getMeetingType());

        if (!TextUtils.isEmpty(mDataset.get(position).getError_MeetingType())) {
            viewHolder.etMeeting.setError(mDataset.get(position).getError_MeetingType());
        } else {
            viewHolder.etMeeting.setError(null);
        }

      /*  viewHolder.etMeeting.setOnFocusChangeListener((v, hasFocus) -> {
            if (position < mDataset.size()) {
                final EditText caption = (EditText) v;
                mDataset.get(position).setMeetingType(caption.getText().toString());
                caption.setError(null);
            }
        });*/

        viewHolder.etMeeting.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) viewHolder.etMeeting;
                    mDataset.get(position).setMeetingType(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });

        viewHolder.etStatus.setText(mDataset.get(position).getMeetingStatus());

        if (!TextUtils.isEmpty(mDataset.get(position).getError_MeetingStatus())) {
            viewHolder.etStatus.setError(mDataset.get(position).getError_MeetingStatus());
        } else {
            viewHolder.etStatus.setError(null);
        }

        viewHolder.etStatus.setOnFocusChangeListener((v, hasFocus) -> {
            if (position < mDataset.size()) {
                final EditText caption = (EditText) v;
                mDataset.get(position).setMeetingStatus(caption.getText().toString());
                caption.setError(null);
            }
        });

        viewHolder.etRemarks.setText(mDataset.get(position).getRemarks());

        if (!TextUtils.isEmpty(mDataset.get(position).getError_Remarks())) {
            viewHolder.etRemarks.setError(mDataset.get(position).getError_Remarks());
        } else {
            viewHolder.etRemarks.setError(null);
        }

        viewHolder.etRemarks.setOnFocusChangeListener((v, hasFocus) -> {
            if (position < mDataset.size()) {
                final EditText caption = (EditText) v;
                mDataset.get(position).setRemarks(caption.getText().toString());
                caption.setError(null);
            }
        });

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView etMeeting, etStatus,etRemarks;
        TextView tvMeetingDate;

        public ViewHolder(View v) {
            super(v);
            tvMeetingDate = v.findViewById(R.id.btDateButton);
            etMeeting = v.findViewById(R.id.etMeeting);
            etStatus = v.findViewById(R.id.etStatus);
            etRemarks = v.findViewById(R.id.etRemarks);
        }
    }

    private void getInspectionDatePickerView(TextView meetingDateTv, CrmMeeting crmMeeting) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {
                    try {

                        String viewDateFormat = "dd MMM YY";
                        String storeDbFormat = "yyyy-MM-dd";

                        SimpleDateFormat viewFormat = new SimpleDateFormat();
                        Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        viewFormat.applyPattern(viewDateFormat);
                        viewFormat.format(d);

                        SimpleDateFormat storeFormat = new SimpleDateFormat();
                        Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        storeFormat.applyPattern(storeDbFormat);
                        storeFormat.format(d1);

                        meetingDateTv.setText(viewFormat.format(d1));
                        crmMeeting.setMeetingDate(storeFormat.format(d1));

                        Log.d("TransferTest:", " Meeting storeFormat--" + storeFormat.format(d1));
                        Log.d("TransferTest:", " Meeting viewFormat--" + viewFormat.format(d1));

                        meetingDateTv.setError(null);
                    } catch (Exception e) {

                    }
                }, mDay, mMonth, mYear);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}