package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.PurchaseHistoryAdapter;
import in.kumanti.emzor.adapter.ViewStockOrderAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.InvoiceBatch;
import in.kumanti.emzor.model.OrderProductSpinner;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.PurchaseHistoryItems;
import in.kumanti.emzor.model.SpinnerProductList;
import in.kumanti.emzor.model.StockViewInvoiceList;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.InputFilterMinMax;
import in.kumanti.emzor.utils.SharedPreferenceManager;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;


public class InvoiceActivity extends MainActivity implements AdapterView.OnItemSelectedListener {

    public int seconds = 60;
    public int minutes = 60;
    CustomSearchableSpinner Product;
    ArrayList<InvoiceBatch> productbatchLists;
    ArrayAdapter<OrderProductSpinner> orderProductSpinnerArrayAdapter;
    InvoiceBatch batchlist;
    Spinner Customer_Name, productType;
    String productType_val;
    Button add_batch, competitor_info, stock_view, add_btn, view_btn, scheme_detail;
    ImageView customerImage, sr_image, delivery_addrs, back, info;
    ArrayList<PurchaseHistoryItems> purchaseHistoryItems;
    PurchaseHistoryItems purchaseHistoryLists1;
    PurchaseHistoryAdapter adapter;
    MyDBHandler dbHandler;
    String company_code, tab_code, sr_code, tab_prefix;
    Globals globals;
    String login_id = "";
    String customer_id = "", transactionType = "Invoice", transactionDate = "", transactionNumber = "";
    String checkin_time = "";
    String order_type = "";
    int invoice_id = 0;
    int random_num = 0;
    int order_qty = 0;
    int vat = 0;
    TextView salesrep_name, manager, region, gps, warehouse, prod_batch, uom_batch, qty_batch;
    TextView customerName, city, checkintime1, sr_name, txt_stock;
    TextView purchase_history, prod_name, uom, value, order_lines, order_value, invoice_num, invoice_date, ytd_qty,
            ytd_value,
            ytd_price,
            ytd_invoices,
            txt_product_uom,
            txt_product_code,
            txt_product_name,
            notification_value;
    LinearLayout notification;
    String product_name, ordervalue_formatted, product_id, product_uom, sr_name1;
    String sales_rep_id, salesinvoice_id, salesinvoice_number, customer_type, salesinvoice_date, salesinvoice_status, salesinvoice_value, salesinvoice_lines;
    EditText qty, discount, price;
    String salesorder_line_id, order_quantity, dis_perc;
    List<Integer> x = new ArrayList<Integer>();

    int a[] = new int[5];
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1, save_salesinvoice_date;
    String sr_name_details, sr_rm_name, sr_region, sr_warehouse;
    Chronometer chronometer;
    long stopTime = 0;
    ListView batch_list;
    DecimalFormat formatter, formatter1;
    SharedPreferenceManager sharedPreferenceManager;
    double price_unformatted = 0.00;
    String price_formatted;
    double order_price = 0.00;
    double order_values = 0.00;
    String batch_inserted = "";
    String first_val = "";
    int remaining_qty = 0;
    int allocate_qty = 0;
    int quan = 0;
    double discount_value = 0.00;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng;
    int invoice_line_id = 0;
    ArrayList<SpinnerProductList> spinnerProductLists;
    ArrayList<OrderProductSpinner> orderProductSpinnerArrayList;
    int numRows1 = 0;
    SpinnerProductList spinnerProductList1;
    int numbRows = 0;
    int batch_value = 0;
    ListView stock_list;
    ArrayList<StockViewInvoiceList> stockList;
    StockViewInvoiceList stocks;
    ViewStockOrderAdapter adapter1;
    Constants constants;
    LinearLayout ytd_section;
    String batch_controlled;
    Button notification_bell;
    ImageView actionbarBackButton, deviceInfo;
    String invoiceType;
    private TextView accountNo;
    private TextView marqueeText, actionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_invoice);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText("Invoice");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        // notification.setVisibility(View.VISIBLE);

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_invoice_batch, null);
        discount.setEnabled(false);

        prod_batch = alertLayout.findViewById(R.id.txt_product);
        uom_batch = alertLayout.findViewById(R.id.txt_uom);
        qty_batch = alertLayout.findViewById(R.id.txt_quantity);
        batch_list = alertLayout.findViewById(R.id.batch_list);

        final ImageView Yes = alertLayout.findViewById(R.id.Yes);
        final ImageView No = alertLayout.findViewById(R.id.No);

        getlocation();

        long date = System.currentTimeMillis();
        SimpleDateFormat sdfd = new SimpleDateFormat("dd MMM yy");
        SimpleDateFormat sdft = new SimpleDateFormat("hh:mm");
        salesinvoice_date = sdfd.format(date);
        invoice_date.setText(salesinvoice_date);
        checkintime1.setText(checkin_time);

        int line_count = dbHandler.get_count_invoice_lines(Integer.toString(invoice_id));
        double order_val = dbHandler.get_invoice_value(Integer.toString(invoice_id));

        if (line_count == 0) {
            order_lines.setText("");
        } else {
            order_lines.setText(Integer.toString(line_count));

        }

        if (order_val == 0) {
            order_value.setText("");
        } else {
            String format_order_value = formatter.format(order_val);
            order_value.setText(format_order_value);
        }


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        competitor_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("Select Product".equals(product_name)) {
                    Toast.makeText(InvoiceActivity.this, "Select Product to View Competitor Info", Toast.LENGTH_SHORT).show();

                } else {

                    Intent myintent = new Intent(getApplicationContext(), CompetitorInfoActivity.class);

                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    //chronometer.stop();

                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putDouble("price", price_unformatted);
                    bundle.putString("login_id", login_id);
                    bundle.putString("product_name", product_name);
                    bundle.putString("product_id", product_id);
                    bundle.putString("product_uom", product_uom);
                    bundle.putString("invoice_id", String.valueOf(invoice_id));
                    bundle.putString("transactionType", transactionType);
//                    bundle.putString("transactionDate", invoice_date.getText().toString());
                    bundle.putString("transactionDate", save_salesinvoice_date);
                    bundle.putString("transactionNumber", invoice_num.getText().toString());


                    myintent.putExtras(bundle);
                    startActivity(myintent);
                }

            }

        });

        productType.setOnItemSelectedListener(this);
        loadSpinnerType();


        sr_image.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                showsrdetails();
                return true;
            }
        });

        customerImage.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                showcustomerLocation();
                return true;
            }
        });

        view_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int inv_count = dbHandler.get_invoice_count(invoice_id);

                if (inv_count == 0) {
                    Toast.makeText(getApplicationContext(), "Add Product to view the Invoices", Toast.LENGTH_LONG).show();

                } else {
                    Intent myintent = new Intent(getApplicationContext(), InvoiceProductActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    //chronometer.stop();
                    //Create the bundle
                    Bundle bundle = new Bundle();

                    //Add your data to bundle
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putString("invoice_id", Integer.toString(invoice_id));
                    bundle.putString("login_id", login_id);
                    bundle.putString("invoice_num", invoice_num.getText().toString());
                    bundle.putString("customer_type", customer_type);
                    bundle.putDouble("price", price_unformatted);
                    bundle.putString("invoice_type", invoiceType);

                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivity(myintent);
                }

            }
        });

        add_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("Invoice".equals(invoiceType)) {

                    if ("Yes".equals(batch_controlled)) {

                        String qty1 = qty.getText().toString();

                        if (qty.getText().toString().equals("")) {

                        } else {

                            quan = Integer.parseInt(qty.getText().toString());

                        }

                        int product_count1 = dbHandler.get_count_invoiceproduct_lines(invoice_id, product_id);
                        // int batch_count1 = dbHandler.get_count_batchlines(invoice_id, product_id);
                        int batch_value = dbHandler.get_productbatch_quantity(product_id);


                        if (TextUtils.isEmpty(qty1)) {
                            qty.setError(getString(R.string.error_field_required));

                        } else if ("0".equals(qty1)) {
                            qty.setError(getString(R.string.Zero_Validation));
                        } else if (product_count1 == 1) {
                            Toast.makeText(getApplicationContext(), "Product Already Exist", Toast.LENGTH_LONG).show();
                        } else if (quan > batch_value) {
                            qty.setError(getString(R.string.BatchQuantity));
                        } else {

                            dbHandler.deletebatch(product_id);
                            loadproductbatchlist(invoice_id, quan);

                            Intent myintent = new Intent(getApplicationContext(), InvoiceBatchActivity.class);

                            //Create the bundle
                            Bundle bundle = new Bundle();
                            //Add your data to bundle
                            bundle.putString("checkin_time", checkin_time);
                            bundle.putString("customer_id", customer_id);
                            bundle.putString("product", product_name);
                            bundle.putString("uom", product_uom);
                            bundle.putInt("quantity", quan);
                            bundle.putString("login_id", login_id);
                            bundle.putString("product_id", product_id);
                            bundle.putInt("invoice_id", invoice_id);

                            //Add the bundle to the intent
                            myintent.putExtras(bundle);

                            //Fire that second activity
                            startActivity(myintent);
                            //addbatch();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Product is not batch controlled", Toast.LENGTH_LONG).show();

                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Cannot View Batch on Secondary Sales Order", Toast.LENGTH_LONG).show();

                }
            }

        });

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog alertbox = new Dialog(v.getRootView().getContext());

                LayoutInflater mInflater = (LayoutInflater)
                        getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                final View alertLayout = mInflater.inflate(R.layout.activity_exit_invoice_popup, null);

                alertbox.setCancelable(false);
                //alertbox.setView(alertLayout);
                alertbox.setContentView(alertLayout);

                final Button Yes = alertLayout.findViewById(R.id.Yes);
                final Button No = alertLayout.findViewById(R.id.No);

                Yes.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {


                        if (invoice_id == 0) {
                            if ("Doctor".equals(customer_type)) {
                                finish();
                            } else {
                                Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                                //chronometer.stop();
                                Bundle bundle = new Bundle();
                                bundle.putString("checkin_time", checkin_time);
                                bundle.putString("sales_rep_id", login_id);
                                bundle.putString("customer_id", customer_id);
                                bundle.putString("nav_type", "Home");

                                myintent.putExtras(bundle);
                                startActivity(myintent);
                            }

                        } else {

                            update_invoice_status("Cancelled");
                            Toast.makeText(getApplicationContext(), "Invoice Cancelled", Toast.LENGTH_LONG).show();

                            // dbHandler.deletebatch(product_id);

                        }

                    }

                });

                No.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        alertbox.dismiss();
                    }

                });

                alertbox.show();

            }
        });

      /*  stock_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent myintent = new Intent(getApplicationContext(), CreateViewInvoiceActivity.class);

                //Create the bundle
                Bundle bundle = new Bundle();

                //Add your data to bundle
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customer_id);
                bundle.putInt("invoice_id", invoice_id);
                bundle.putString("login_id", login_id);
                bundle.putString("salesinvoice_date", salesinvoice_date);
                bundle.putString("product_name", product_name);
                bundle.putString("product_id", product_id);
                bundle.putString("product_uom", product_uom);


                //Add the bundle to the intent
                myintent.putExtras(bundle);

                //Fire that second activity
                startActivity(myintent);
            }

        }); */

        stock_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("Select Product".equals(product_name)) {
                    Toast.makeText(InvoiceActivity.this, "Select Product to View Stock", Toast.LENGTH_SHORT).show();

                } else {
                    showstockview();
                   /* Intent myintent = new Intent(getApplicationContext(), CreateViewOrderActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putInt   ("order_id", order_id);
                    bundle.putString("login_id", login_id);
                    bundle.putString("salesorder_date", salesorder_date);
                    bundle.putString("product_name", product_name);
                    bundle.putString("product_id", product_id);
                    bundle.putString("product_uom", product_uom);
                    myintent.putExtras(bundle);
                    startActivity(myintent); */
                }

            }

        });

        scheme_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Select Product".equals(product_name)) {
                    Toast.makeText(InvoiceActivity.this, "Select Product to View Scheme", Toast.LENGTH_SHORT).show();

                } else {
                    showscheme();

                }

            }

        });

        notification_bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                alertbox.setTitle("You have Focus Products to Add");

                alertbox.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

                alertbox.show();

            }

        });

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                price.setEnabled(false);
                int batch_count = dbHandler.get_count_batch(Integer.toString(invoice_id), product_id);
                int product_count1 = dbHandler.get_count_invoiceproduct_lines(invoice_id, product_id);
                int stock_batch_value = dbHandler.get_stockreceipt_quantity(product_id);
                String qty1 = "";
                int qty2 = 0;
                if ("".equals(qty.getText().toString())) {
                    qty.setError(getString(R.string.error_field_required));

                } else {
                    qty1 = qty.getText().toString();
                    qty2 = Integer.parseInt(qty1);
                    batch_value = dbHandler.get_productbatch_quantity(product_id);
                }

                if (TextUtils.isEmpty(qty1)) {
                    qty.setError(getString(R.string.error_field_required));

                } else if ("0".equals(qty1)) {
                    qty.setError(getString(R.string.Zero_Validation));
                } else if (product_count1 == 1) {
                    Toast.makeText(getApplicationContext(), "Product Already Exist", Toast.LENGTH_LONG).show();
                } else if ("Invoice".equals(invoiceType)) {
                    if (qty2 > stock_batch_value) {
                        qty.setError(getString(R.string.BatchQuantity));
                    } else if ("Yes".equals(batch_controlled)) {
                        if (qty2 > batch_value) {
                            qty.setError(getString(R.string.BatchQuantity));
                        } else {
                            add_invoice_lines();
                        }
                    } else {
                        add_invoice_lines();
                    }
                } else {
                    add_invoice_lines();

                }
            }

        });

        Random rnd = new Random();
        int ran_num = 100000 + rnd.nextInt(900000);
        salesinvoice_number = Integer.toString(ran_num);

        discount.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100")});

        qty.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                order_values = 0;
                discount.setText("");
                if (qty.getText().toString().equals("")) {

                    value.setText("");
                    discount.setEnabled(false);

                } else {

                    discount.setEnabled(false);
                    GeneralSettings gs = generalSettingsTable.getSettingByKey("secCustDiscount");
                    if (gs != null && gs.getValue().equals("Yes")) {
                        discount.setEnabled(true);
                    }


                    int quan = Integer.parseInt(qty.getText().toString());
                    Double amnt = Double.parseDouble(price.getText().toString());
                    order_values = Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                    if (order_values == 0) {
                        ordervalue_formatted = "0";
                    } else {
                        ordervalue_formatted = formatter.format(order_values);
                    }


                    value.setText(ordervalue_formatted);
                    order_quantity = Integer.toString(quan);
                    order_price = amnt;

                }
            }
        });

        price.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                order_values = 0;
                discount.setText("");

                if (price.getText().toString().equals("")) {
                    value.setText("");
                    price_unformatted = 0;
                    price.setText("0");

                } else {

                    if (qty.getText().toString().equals("")) {

                        value.setText("");
                        discount.setEnabled(false);
                        //price_unformatted=Double.parseDouble(price.getText().toString());

                    } else {
                        int quan = Integer.parseInt(qty.getText().toString());
                        double amnt = Double.parseDouble(price.getText().toString());
                        order_values = Double.valueOf(quan * amnt) + (vat * Double.valueOf(quan * amnt)) / 100;
                        String orderval_formatted = formatter.format(order_values);
                        value.setText(orderval_formatted);
                        price_unformatted = Double.parseDouble(price.getText().toString());
                    }

                }
            }
        });

        discount.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (discount.getText().toString().equals("")) {
                    Integer quan = 0;
                    Double amnt = 0.00;
                    discount_value = 0;
                    dis_perc = "";
                    if ((qty.getText().toString().equals("")) || (price.getText().toString().equals(""))) {
                    } else {
                        quan = Integer.parseInt(qty.getText().toString());
                        amnt = Double.parseDouble(price.getText().toString());
                    }

                    order_values = Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                    String ordervalue_formatted = formatter.format(order_values);
                    value.setText(ordervalue_formatted);
                } else {

                    Integer disc = Integer.parseInt(discount.getText().toString());
                    Integer quan = Integer.parseInt(qty.getText().toString());
                    Double amnt = Double.parseDouble(price.getText().toString());

                    order_values = Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100
                            - Integer.valueOf(quan) * amnt
                            * Integer.valueOf(disc) / 100;
                    String ordervalue_formatted = formatter.format(order_values);
                    discount_value = ((Integer.valueOf(quan) * amnt) + (vat * Integer.valueOf(quan) * amnt) / 100) - order_values;
                    value.setText(ordervalue_formatted);
                    order_quantity = Integer.toString(quan);
                    order_price = amnt;
                    dis_perc = Integer.toString(disc);

                }
            }
        });

    }

    private void populateHeaderDetails() {
        //Populate Header Details

        chronometer.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        chronometer.start();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            invoice_id = bundle.getInt("invoice_id", invoice_id);
            salesinvoice_number = bundle.getString("invoice_num");
            customer_type = bundle.getString("customer_type");
            invoiceType = bundle.getString("invoice_type");

            if (invoice_id == 0) {

                random_num = dbHandler.get_sale_invoice_id();
                invoice_id = dbHandler.get_invoice_sequence_value();

                // invoice_id = dbHandler.get_invoice_id();
                invoice_id = invoice_id + 1;
                dbHandler.update_invoice_sequence(invoice_id);

                random_num = random_num + 1;

                SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("ddMMyy");
                Date myDate1 = new Date();
                String invoice_seq_date = timeStampFormat1.format(myDate1);

                if (random_num <= 9) {
                    invoice_num.setText(tab_code + "IN"  +  invoice_seq_date + "00" + random_num);

                } else if (random_num > 10 & random_num < 99) {
                    invoice_num.setText(tab_code + "IN"  +   invoice_seq_date + "0" + random_num);

                } else {
                    invoice_num.setText(tab_code + "IN"  +  invoice_seq_date + random_num);
                }
            } else {
                order_type = bundle.getString("order_type");
                invoice_num.setText(salesinvoice_number);
            }

        }


        sr_name.setText(sr_name1);
        checkintime1.setText(checkin_time);


        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }
            }
        }


        if ("Doctor".equals(customer_type)) {
            purchase_history.setText("FOC HISTORY");
            ytd_section.setVisibility(View.GONE);
        }


        price.setEnabled(false);


    }

    public void showstockview() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_create_view_invoice, null);
        //final TextView txt_product = alertLayout.findViewById(R.id.txt_product);
        //final TextView txt_description = alertLayout.findViewById(R.id.txt_description);
        //final ImageView close = alertLayout.findViewById(R.id.close);
        stock_list = alertLayout.findViewById(R.id.stock_list);
        txt_stock = alertLayout.findViewById(R.id.txt_stock);
        txt_product_uom = alertLayout.findViewById(R.id.txt_product_uom);
        txt_product_code = alertLayout.findViewById(R.id.txt_product_code);
        txt_product_name = alertLayout.findViewById(R.id.txt_product_name);


        //txt_product.setText("Product  " + product_name);
//txt_description.setText("");
        getstock_list();
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
// this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
// disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void showscheme() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.scheme_product, null);
        final TextView txt_product = alertLayout.findViewById(R.id.schemeProductNameTv);
        final TextView txt_description = alertLayout.findViewById(R.id.schemeProductDescriptionTv);

        txt_product.setText("Product  " + product_name);
//txt_description.setText("");

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
// this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
// disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void showschemedetail() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_scheme_allocation, null);
        //final TextView txt_product = alertLayout.findViewById(R.id.txt_product);
        //final TextView txt_description = alertLayout.findViewById(R.id.txt_description);
        //final ImageView close = alertLayout.findViewById(R.id.close);


        //txt_product.setText("Product  " + product_name);
        //txt_description.setText("");

        //getstock_list();
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void getstock_list() {

        int stock = dbHandler.getstockonhand(product_id);
        String stockonhand = Integer.toString(stock);
        txt_stock.setText(formatter1.format(Double.parseDouble(stockonhand)));
        txt_product_uom.setText(product_uom);
        txt_product_code.setText(product_id);
        txt_product_name.setText(product_name);

        //Working with List to Show Data's
        stockList = new ArrayList<StockViewInvoiceList>();
        Cursor data = dbHandler.getstock_viewinvoice(product_id);
        numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {
                String onhand_qty = data.getString(2);

                stocks = new StockViewInvoiceList(
                        data.getString(0),
                        data.getString(1),
                        formatter1.format(Double.parseDouble(onhand_qty)));
                stockList.add(i, stocks);
                //System.out.println(userList.get(i).getFirstName());
                i++;


            }

            adapter1 = new ViewStockOrderAdapter(getApplicationContext(), R.layout.list_view_stock_invoice, stockList);
            //stock_list = (ListView) findViewById(R.id.stock_list);
            stock_list.setAdapter(adapter1);
        }
    }

    private void getlocation() {


        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            String lat = Double.toString(latitude);
            String lng = Double.toString(longitude);
            lat_lng = lat + "," + lng;

        } else {

            gpsTracker.showSettingsAlert();

        }
    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public void addbatch() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_invoice_batch, null);
        //final PopupWindow popupWindow=new PopupWindow(alertLayout);
        // popupWindow.setFocusable(true);
        //spopupWindow.update();
        final TextView prod_batch = alertLayout.findViewById(R.id.txt_product);
        final TextView uom_batch = alertLayout.findViewById(R.id.txt_uom);
        final TextView qty_batch = alertLayout.findViewById(R.id.txt_quantity);
        batch_list = alertLayout.findViewById(R.id.batch_list);

        final ImageView Yes = alertLayout.findViewById(R.id.Yes);
        final ImageView No = alertLayout.findViewById(R.id.No);


        prod_batch.setText(product_name);
        uom_batch.setText(product_uom);
        qty_batch.setText(order_quantity);

        //loadproductbatchlist();
        final Dialog alert = new Dialog(this);
        // this is set the view from XML inside AlertDialog
        alert.setContentView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.show();

        Yes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                alert.dismiss();
            }

        });

        No.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                alert.dismiss();
            }

        });


    }

   /* private void loadproductbatchlist() {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails(product_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
            batch_list.setAdapter(null);
        } else {
            int i = 0;
            while (data.moveToNext()) {
                batchlist = new InvoiceBatch(
                        data.getString(1),
                        data.getString(0),
                        data.getString(2),
                        data.getString(3));
                productbatchLists.add(i, batchlist);
                long id2 = dbHandler.insert_product_batch_temp
                        (product_id,
                                data.getString(0),
                                data.getString(1),
                                data.getString(2),
                                data.getString(3)

                        );
                i++;
            }

            InvoiceBatchAdapter adapter = new InvoiceBatchAdapter(getApplicationContext(), R.layout.list_invoice_batch, productbatchLists);
            batch_list.setAdapter(adapter);


        }
    }
*/

  /*  public void loadSpinnerProduct() {

        // Spinner Drop down elements
        List<String> lables = dbHandler.getInvoiceProducts(invoice_id);
        lables.add(0, "Select Product");
        Product.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                lables));


        // Creating adapter for spinner
       /* ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getApplicationContext(),
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        Product.setAdapter(dataAdapter); */
    //} */

    public void loadSpinnerType() {

        String[] labels = {"All", "Focus", "Scheme"};

        productType.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                labels));

        productType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productType_val = adapterView.getItemAtPosition(pos).toString();

// Load Products
                loadSpinnerProduct(productType_val);
                //Toast.makeText(getApplicationContext(), productType_val, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

    }

    public void loadSpinnerProduct(String productType_val) {

        // Spinner Drop down elements
        /*List<String> lables = dbHandler.getOrderProducts(order_id);
        lables.add(0, "Select Product");
        Product.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                lables)); */

        /*spinnerProductLists = new ArrayList<SpinnerProductList>();
        Cursor data = dbHandler.getProductSpinnerInvoice(invoice_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
        } else {
            int i = 0;
            while (data.moveToNext()) {

                spinnerProductList1 = new SpinnerProductList(data.getString(2), data.getString(6), data.getString(1));
                spinnerProductLists.add(i, spinnerProductList1);
                i++;
            }
            final SpinnerProductAdapter spinnneradapter = new SpinnerProductAdapter(getApplicationContext(), R.layout.custom_spinner, R.id.product, spinnerProductLists);
            Product.setAdapter(spinnneradapter);
        } */
        ArrayList<OrderProductSpinner> orderProductSpinnerArrayList = new ArrayList<OrderProductSpinner>();

        if ("Doctor".equals(customer_type)) {
            ArrayList<Product> productArrayList = dbHandler.getStockReceiptProductsByType(0, invoice_id);
            for (Product p : productArrayList) {
                OrderProductSpinner cs = new OrderProductSpinner();
                cs.setProduct_id(p.getProduct_id());
                cs.setProduct(p.getProduct_name());
                cs.setProd_type(p.getProduct_type());

                orderProductSpinnerArrayList.add(cs);
            }
        } else {
            orderProductSpinnerArrayList = dbHandler.getInvoiceProductSpinner(customer_id, invoice_id, productType_val.toLowerCase(), invoiceType);
        }
        OrderProductSpinner cs = new OrderProductSpinner();
        cs.setProduct("Select Product");
        cs.setProd_type("-1");
        cs.setProduct_id("-1");
        orderProductSpinnerArrayList.add(0, cs);
        Product.setTitle("Select Product");
        Product.setPositiveButton("Close");

        /*orderProductSpinnerArrayAdapter = new ArrayAdapter<OrderProductSpinner>(getApplicationContext(), android.R.layout.simple_spinner_item, orderProductSpinnerArrayList);
        orderProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        Product.setAdapter(orderProductSpinnerArrayAdapter);*/


        //orderProductSpinnerArrayList = dbHandler.getInvoiceProductSpinner(customer_id, invoice_id);
        //OrderProductSpinner cs = new OrderProductSpinner();
//        cs.setProduct("Select Product");
//        cs.setProd_type("-1");
//        cs.setProduct_id("-1");

        //orderProductSpinnerArrayList.add(0, cs);
        final SpinnerProductAdapter spinnneradapter = new SpinnerProductAdapter(getApplicationContext(), R.layout.custom_spinner, R.id.product, orderProductSpinnerArrayList);
        Product.setAdapter(spinnneradapter);
        Product.setOnItemSelectedListener(this);
    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // product_name = ((TextView) view.findViewById(R.id.product)).getText().toString();
//        Product.isSpinnerDialogOpen = false;

        product_name = parent.getItemAtPosition(position).toString();
        OrderProductSpinner cs1 = (OrderProductSpinner) parent.getItemAtPosition(position);
        String product_id1 = cs1.getProduct_id();
        System.out.println("product_id1inv = " + product_id1);

        if ("Select Product".equals(product_name)) {
            resetfields();

            price.setEnabled(false);

        } else {
            if ("Doctor".equals(customer_type)) {
                price.setEnabled(false);


            } else {
                price.setEnabled(false);
                GeneralSettings gs = generalSettingsTable.getSettingByKey("editSecSalesPrice");
                if (gs != null && gs.getValue().equals("Yes")) {
                    price.setEnabled(true);
                }

            }

            dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
//            Cursor productDetails = dbHandler.getProductDetails(product_name);
            Cursor productDetails = dbHandler.getProductDetails(product_id1);
            numbRows = productDetails.getCount();
            if (numbRows == 0) {
                // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
            } else {
                //int i = 0;
                while (productDetails.moveToNext()) {

                    if ("Doctor".equals(customer_type)) {
                        price_unformatted = 0.00;
                        price_formatted = "0";

                    } else {
                        price_unformatted = productDetails.getDouble(5);
                        //price_formatted = formatter.format(price_unformatted);

                    }

                    batch_controlled = productDetails.getString(8);
                    prod_name.setText(productDetails.getString(2));
                    uom.setText(productDetails.getString(4));
                    vat = productDetails.getInt(11);

                    qty.setText("");
                    value.setText("");

                    price.setText(String.valueOf(price_unformatted));
                    product_id = productDetails.getString(1);
                    System.out.println("product_iddb = " + product_id);
                    product_uom = productDetails.getString(4);

                    Cursor data = dbHandler.getInvoicePurchaseHistory(product_id, customer_id);
                    purchaseHistoryItems = new ArrayList<PurchaseHistoryItems>();

                    int numRows1 = data.getCount();
                    if (numRows1 == 0) {
                        int j = 0;
                        while (data.moveToNext()) {
                            String price_val = data.getString(2);
                            double price_val1 = Double.parseDouble(price_val);
                            String formatted_price = formatter.format(price_val1);
                            purchaseHistoryLists1 = new PurchaseHistoryItems(data.getString(0), data.getString(1), formatted_price, data.getString(3), data.getString(3));
                            purchaseHistoryItems.add(j, purchaseHistoryLists1);
                            //System.out.println(userList.get(i).getFirstName());
                            j++;
                        }

                        adapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItems, customer_type);
                        ListView top_product_list = findViewById(R.id.top_product_list);
                        top_product_list.setAdapter(adapter);

                    } else {
                        //Toast.makeText(getApplicationContext(), "history", Toast.LENGTH_LONG).show();

                        int j = 0;
                        while (data.moveToNext()) {
                            String price_val = data.getString(2);
                            double price_val1 = Double.parseDouble(price_val);
                            String formatted_price = formatter.format(price_val1);
                            String inputDateStr = data.getString(0);

                            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                            DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                            Date date = null;

                            try {
                                date = inputFormat.parse(inputDateStr);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String outputDateStr = outputFormat.format(date);

                            purchaseHistoryLists1 = new PurchaseHistoryItems(outputDateStr, data.getString(1), formatted_price, data.getString(3), data.getString(3));
                            purchaseHistoryItems.add(j, purchaseHistoryLists1);
                            //System.out.println(userList.get(i).getFirstName());
                            j++;
                        }

                        adapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItems, customer_type);
                        ListView top_product_list = findViewById(R.id.top_product_list);
                        top_product_list.setAdapter(adapter);
                    }

                }

                get_ytd_values();
                qty.setEnabled(true);

            }
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
//        Product.isSpinnerDialogOpen = false;

    }

    private void initializeViews() {

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);
        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);


        batch_list = findViewById(R.id.batch_list);

        prod_name = findViewById(R.id.prod_name);
        uom = findViewById(R.id.uom);
        value = findViewById(R.id.value);
        qty = findViewById(R.id.quantity);
        order_lines = findViewById(R.id.order_line_val);
        order_value = findViewById(R.id.order_value);
        invoice_num = findViewById(R.id.invoice_num);
        invoice_date = findViewById(R.id.invoice_date);
        sr_image = findViewById(R.id.sr_image);
        productType = findViewById(R.id.productType);

        price = findViewById(R.id.secSalesPriceEditText);
        discount = findViewById(R.id.secSalesDiscountEditText);


        sr_name = findViewById(R.id.sr_name);
        //delivery_addrs = (ImageView) findViewById(R.id.delivery_addrs);
        chronometer = findViewById(R.id.txt_duration);
        add_batch = findViewById(R.id.add_batch);
        stock_view = findViewById(R.id.stock_view);
        ytd_qty = findViewById(R.id.ytd_qty);
        ytd_value = findViewById(R.id.ytd_value);
        ytd_price = findViewById(R.id.ytd_price);
        ytd_invoices = findViewById(R.id.ytd_invoices);
        checkintime1 = findViewById(R.id.checkintime1);
        salesrep_name = findViewById(R.id.sales_rep_name);
        //  manager = findViewById(R.id.manager);
        //  region = findViewById(R.id.region);
        gps = findViewById(R.id.gps);
        view_btn = findViewById(R.id.view_btn);
        add_btn = findViewById(R.id.add_btn);
        competitor_info = findViewById(R.id.competitor_info);
        scheme_detail = findViewById(R.id.scheme_detail);
        Product = findViewById(R.id.Product);
        purchase_history = findViewById(R.id.purchase_history);
        ytd_section = findViewById(R.id.ytd_section);

        qty.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
        constants = new Constants();

        notification = findViewById(R.id.notificationContainer);
        notification_bell = findViewById(R.id.notificationBellButton);
        notification_value = findViewById(R.id.focusedProdCountTv);

        globals = ((Globals) getApplicationContext());
        login_id = globals.getLogin_id();
        sr_name1 = globals.getSr_name();
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        company_code = Constants.COMPANY_CODE;
        tab_code = Constants.TAB_CODE;
        sr_code = Constants.SR_CODE;
        tab_prefix = Constants.TAB_PREFIX;

        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("#,###");

    }

   /* public void empty_list(){
        Cursor productDetails = dbHandler.getEmptyRows();
        int numbRows = productDetails.getCount();
        if (numbRows == 0) {
            Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            //int i = 0;
            while (productDetails.moveToNext()) {


                Cursor data = dbHandler.getPurchaseHistory(product_id, customer_id);
                purchaseHistoryItems = new ArrayList<PurchaseHistoryItems>();

                int numRows1 = data.getCount();
                if (numRows1 == 0) {
                    int j = 0;
                    while (data.moveToNext()) {
                        String price_val = data.getString(14);
                        double price_val1 = Double.parseDouble(price_val);
                        String formatted_price = formatter.format(price_val1);
                        purchaseHistoryLists1 = new PurchaseHistoryItems(data.getString(5), data.getString(13), formatted_price, data.getString(3), data.getString(4));
                        purchaseHistoryItems.add(j, purchaseHistoryLists1);
                        //System.out.println(userList.get(i).getFirstName());
                        j++;
                    }

                    adapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItems);
                    ListView top_product_list = (ListView) findViewById(R.id.top_product_list);
                    top_product_list.setAdapter(adapter);
                    Toast.makeText(getApplicationContext(), "no history", Toast.LENGTH_LONG).show();

                } else {
                    //Toast.makeText(getApplicationContext(), "history", Toast.LENGTH_LONG).show();

                    int j = 0;
                    while (data.moveToNext()) {
                        String price_val = data.getString(14);
                        double price_val1 = Double.parseDouble(price_val);
                        String formatted_price = formatter.format(price_val1);
                        purchaseHistoryLists1 = new PurchaseHistoryItems(data.getString(5), data.getString(13), formatted_price, data.getString(3), data.getString(4));
                        purchaseHistoryItems.add(j, purchaseHistoryLists1);
                        //System.out.println(userList.get(i).getFirstName());
                        j++;
                    }

                    adapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItems);
                    ListView top_product_list = (ListView) findViewById(R.id.top_product_list);
                    top_product_list.setAdapter(adapter);
                    top_product_list.setEmptyView(findViewById(R.id.empty));
                }

            }

        }
    } */

    private void loadproductbatchlist(int invoice_id, int quantity) {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails(product_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {

        } else {
            int i = 0;
            while (data.moveToNext()) {


                long id2 = dbHandler.insert_product_batch
                        (data.getString(0),
                                data.getString(1),
                                data.getString(2),
                                data.getString(3),
                                null,
                                null,
                                String.valueOf(invoice_id),
                                "Draft"
                        );

            }

            i++;


        }

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

        /*String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date + "\n" + time); */
    }

    private void update_invoice_status(String status) {
        dbHandler.update_invoice_header_status(String.valueOf(invoice_id), status, 0.00, 0.00, null, 0.00, null);
        dbHandler.update_invoice_detail_status(String.valueOf(invoice_id), status);
        dbHandler.updatebatchstatus(Integer.valueOf(invoice_id), status);

        if ("Doctor".equals(customer_type)) {

            Intent myintent = new Intent(getApplicationContext(), DoctorVisitActivity.class);
            //Create the bundle
            Bundle bundle = new Bundle();
            //Add your data to bundle
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        } else {

            Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);

            //Create the bundle
            Bundle bundle = new Bundle();
            //Add your data to bundle
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("sales_rep_id", login_id);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        }

    }

    private void get_ytd_values() {

        int ytd_qti = dbHandler.get_ytd_quantity_sec(customer_id, product_id);
        double ytd_val = dbHandler.get_ytd_value_sec(customer_id, product_id);
        int ytd_inv = dbHandler.get_ytd_invoices_sec(customer_id);
        double ytd_price1 = dbHandler.get_ytd_price_sec(customer_id, product_id);

        String ytf_val_formatted = formatter.format(ytd_val);
        String ytf_price_formatted = formatter.format(ytd_price1);
        String ytf_qty_formatted = formatter1.format(ytd_qti);


        if (ytd_val == 0) {
            ytd_value.setText("0");
        } else {
            ytd_value.setText(ytf_val_formatted);

        }

        if (ytd_price1 == 0) {
            ytd_price.setText("0");
        } else {
            ytd_price.setText(ytf_price_formatted);

        }

        ytd_qty.setText(ytf_qty_formatted);
        ytd_price.setText(ytf_price_formatted);
        ytd_invoices.setText(Integer.toString(ytd_inv));

    }

    public void showdeliveryaddressdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_delivery_details, null);


        final TextView ship_add1 = alertLayout.findViewById(R.id.ship_add1);
        final TextView ship_add2 = alertLayout.findViewById(R.id.ship_add2);
        final TextView ship_add3 = alertLayout.findViewById(R.id.ship_add3);
        final TextView ship_city = alertLayout.findViewById(R.id.ship_city);
        final TextView ship_state = alertLayout.findViewById(R.id.ship_state);
        final TextView ship_country = alertLayout.findViewById(R.id.ship_country);

        ship_add1.setText(ship_address1);
        ship_add2.setText(ship_address2);
        ship_add3.setText(ship_address3);
        ship_city.setText(ship_city1);
        ship_state.setText(ship_state1);
        ship_country.setText(ship_country1);


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void showsrdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_sr_details, null);
        final TextView sr_name = alertLayout.findViewById(R.id.sr_name_details);
        final TextView sr_rm = alertLayout.findViewById(R.id.sr_rm_name);
        final TextView sr_rgn = alertLayout.findViewById(R.id.sr_region);
        final TextView sr_whouse = alertLayout.findViewById(R.id.sr_warehouse);
        final TextView sr_gps = alertLayout.findViewById(R.id.sr_gps);

        sr_name.setText(sr_name_details);
        sr_rm.setText(sr_rm_name);
        sr_rgn.setText(sr_region);
        sr_whouse.setText(sr_warehouse);
        sr_gps.setText(lat_lng);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void loadproductbatchlist_batch() {

        Cursor data = dbHandler.getProductBatchDetails2(product_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        order_qty = Integer.parseInt(order_quantity);
        if (numRows1 == 0) {

        } else {
            int i = 0;
            allocate_qty = order_qty;
            while (data.moveToNext()) {

                int onhand_qty = data.getInt(2);
                if (onhand_qty >= allocate_qty) {
                    // remaining_qty=onhand_qty-allocate_qty;

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(allocate_qty), data.getString(0), String.valueOf(invoice_id));
                    //dbHandler.updatebatchqty(String.valueOf(invoice_id), product_id, data.getString(1));

                    break;

                } else {

                    remaining_qty = allocate_qty - onhand_qty;
                    Log.i("remaining_qty", String.valueOf(remaining_qty));

                    int test = allocate_qty;
                    if (allocate_qty <= onhand_qty) {
                        test = allocate_qty;
                    } else {
                        test = onhand_qty;
                    }

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(test), data.getString(0), String.valueOf(invoice_id));
                    allocate_qty = remaining_qty;
                }
                i++;
            }

        }
    }

    private void resetfields() {
        prod_name.setText("");
        uom.setText("");
        price.setText("");
        value.setText("");
        qty.setText("");
        ytd_price.setText("0");
        ytd_qty.setText("0");
        ytd_invoices.setText("0");
        ytd_value.setText("0");
        if (numbRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {

            purchaseHistoryItems.clear();
            adapter.notifyDataSetChanged();
        }

        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_grey_scheme);
        prod_name.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        //loadSpinnerProduct();


    }

    private void add_invoice_lines() {
        salesinvoice_status = "Draft";
        long date = System.currentTimeMillis();
        SimpleDateFormat sdfd = new SimpleDateFormat("ddMMMyyyy");
        SimpleDateFormat sdft = new SimpleDateFormat("hh:mm");
        salesinvoice_date = sdfd.format(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        save_salesinvoice_date = timeStampFormat.format(myDate);

        if (order_type.equals("")) {

            String visitSequence = sharedPreferences.getString("VisitSequence", "");

            long id1 = dbHandler.insert_invoice_header
                    (login_id,
                            customer_id,
                            Integer.toString(invoice_id),
                            invoice_num.getText().toString(),
                            save_salesinvoice_date,
                            salesinvoice_status,
                            salesinvoice_value,
                            salesinvoice_value,
                            String.valueOf(random_num),
                            visitSequence);

        }

        if ((order_type.equals("")) || (order_type.equals("Lines"))) {

            invoice_line_id = dbHandler.get_sale_invoice_line_id(invoice_id);
            invoice_line_id = invoice_line_id + 1;

            int quan = Integer.parseInt(qty.getText().toString());
            double amnt = Double.parseDouble(price.getText().toString());

            long id2 = dbHandler.insert_invoice_details
                    (sales_rep_id,
                            customer_id,
                            Integer.toString(invoice_id),
                            invoice_line_id,
                            invoice_num.getText().toString(),
                            save_salesinvoice_date,
                            salesinvoice_status,
                            product_id,
                            product_name,
                            product_uom,
                            order_quantity,
                            Double.parseDouble(price.getText().toString()),
                            order_values,
                            dis_perc,
                            vat,
                            Double.valueOf(quan * amnt) + (vat * Double.valueOf(quan * amnt)) / 100,
                            0.00,
                            0.00,
                            discount_value
                    );
            //loadproductbatchlist_insert();
            //loadproductbatchlist_update();


            if (id2 <= 0) {
                Toast.makeText(getApplicationContext(), "Invoice Creation Failed", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "Product is added to cart", Toast.LENGTH_LONG).show();
            }

            order_type = "Lines";
            if ("Invoice".equals(invoiceType)) {
                if ("Yes".equals(batch_controlled)) {
                    dbHandler.deletebatch(product_id);
                    loadproductbatchlist(invoice_id, quan);
                    loadproductbatchlist_batch();
                    loadproductbatchlist1();
                    dbHandler.updatebatchstatus(invoice_id, "Inprocess");
                } else {

                }
            } else {

            }

            //dbHandler.update_invoice_sequence(Integer.valueOf(invoice_id));

        }

        int line_count = dbHandler.get_count_invoice_lines(Integer.toString(invoice_id));
        double order_val = dbHandler.get_invoice_value(Integer.toString(invoice_id));
        String format_order_value = formatter.format(order_val);
        qty.setEnabled(false);
        order_lines.setText(Integer.toString(line_count));
        prod_name.setText("");
        uom.setText("");
        price.setText("");
        value.setText("");
        qty.setText("");
        ytd_price.setText("0");
        ytd_qty.setText("0");
        ytd_invoices.setText("0");
        ytd_value.setText("0");
        purchaseHistoryItems.clear();
        adapter.notifyDataSetChanged();
        discount.setText("");
        order_value.setText(format_order_value);
        //invoice_num.setText(login_id + "18" + Integer.toString(invoice_id));
        invoice_date.setText(salesinvoice_date);
        loadSpinnerType();
        dbHandler.update_invoice_header(Integer.toString(invoice_id), Double.toString(order_val), Integer.toString(line_count));
    }

    private void loadproductbatchlist1() {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails2(product_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            Toast.makeText(getApplicationContext(), "No Records Found.", Toast.LENGTH_LONG).show();
            batch_list.setAdapter(null);
        } else {
            int i = 0;
            while (data.moveToNext()) {

                batchlist = new InvoiceBatch(
                        data.getString(1),
                        data.getString(0),
                        data.getInt(2),
                        data.getInt(3));
                productbatchLists.add(i, batchlist);
                i++;
            }

            InvoiceBatchAdapter adapter = new InvoiceBatchAdapter(getApplicationContext(), R.layout.list_invoice_batch, productbatchLists);
            batch_list.setAdapter(adapter);

        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    public class SpinnerProductAdapter extends ArrayAdapter<OrderProductSpinner> {

        Activity mActivity = null;
        Globals globals;
        MyDBHandler dbHandler;
        private LayoutInflater mInflater;
        private ArrayList<OrderProductSpinner> users;
        private int mViewResourceId;
        private Context c;

        public SpinnerProductAdapter(Context context, int textViewResourceId, int id, ArrayList<OrderProductSpinner> users) {

            super(context, id, users);
            this.users = users;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            dbHandler = new MyDBHandler(c, null, null, 1);

        }


        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            convertView = mInflater.inflate(mViewResourceId, null);

            final OrderProductSpinner user = users.get(position);
            if (user != null) {

                viewHolder = new ViewHolder();
                viewHolder.product = convertView.findViewById(R.id.product);
                viewHolder.item_type = convertView.findViewById(R.id.item_type);

                if (position == 0) {
                    viewHolder.product.setText("Select Product");

                } else {

                    if (viewHolder.product != null) {
                        viewHolder.product.setText(user.getProduct());
                    }

                    if (viewHolder.item_type != null) {

                        String item_type_val = user.getProd_type();
                        if ("Focused".equals(item_type_val)) {
                            viewHolder.item_type.setImageResource(R.drawable.product_green_scheme);

                        } else {
                            viewHolder.item_type.setImageResource(R.drawable.product_yellow_scheme);

                        }

                    }

                }
            }

            return convertView;
        }

        public View getDropDownView(int position, View convertView, ViewGroup
                parent) {
            return getView(position, convertView, parent);

        }

        public class ViewHolder {
            TextView product;
            ImageView item_type;
        }

    }

    public class InvoiceBatchAdapter extends ArrayAdapter<InvoiceBatch> {

        Activity mActivity = null;
        AlertDialog alertDialog1;
        Globals globals;
        MyDBHandler dbHandler;
        ViewHolder viewHolder = null;
        private LayoutInflater mInflater;
        private ArrayList<InvoiceBatch> productbatchLists;
        private int mViewResourceId;
        private Context c;


        public InvoiceBatchAdapter(Context context, int textViewResourceId, ArrayList<InvoiceBatch> productbatchLists) {
            super(context, textViewResourceId, productbatchLists);
            this.productbatchLists = productbatchLists;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            dbHandler = new MyDBHandler(c, null, null, 1);
            viewHolder = null;

        }

        public int getCount() {
            return productbatchLists.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public View getView(final int position, View convertView, ViewGroup parent) {


            convertView = mInflater.inflate(mViewResourceId, null);
            final InvoiceBatch user = productbatchLists.get(position);
            final int pos = position;

            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.expirydate = convertView.findViewById(R.id.batch_expirydate);
                viewHolder.batchnumber = convertView.findViewById(R.id.batch_batchnumber);
                viewHolder.quantity = convertView.findViewById(R.id.batch_quantity);
                viewHolder.invoices = convertView.findViewById(R.id.batch_invoices);


                if (viewHolder.expirydate != null) {
                    viewHolder.expirydate.setText(user.getBatchexpirydate());
                }
                if (viewHolder.batchnumber != null) {
                    viewHolder.batchnumber.setText((user.getBatchnumber()));
                }

                if (viewHolder.quantity != null) {
                    viewHolder.quantity.setText((String.valueOf(user.getBatchquantity())));
                } else {
                    viewHolder.quantity.setText("0");
                }
                if (viewHolder.invoices != null) {
                    viewHolder.invoices.setText((String.valueOf(user.getBatchinvoice())));
                } else {
                    viewHolder.invoices.setText("Anbu");
                }

                //   viewHolder.invoices.setText(productbatchLists.get(position).getBatchinvoice());
                // viewHolder.invoices.setId(position);

                //we need to update adapter once we finish with editing


             /*  viewHolder.invoices.addTextChangedListener(new TextWatcher() {

                    public void afterTextChanged(Editable s) {
                    }

                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {

                        final int position = v.getId();
                        final EditText Caption = (EditText) v;
                        String batchnumber=user.getBatchnumber();

                        dbHandler.updateInvoiceBatch(product_id,Caption.getText().toString(),batchnumber,invoice_id);
                    }

                        //dbHandler.updateInvoiceBatch(product_id,"9999");
                      /*  if (viewHolder.invoices.getText().toString().equals("")) {

                            Toast.makeText(getApplicationContext(), "Inside Null", Toast.LENGTH_LONG).show();

                        } else {

                            Integer quan = Integer.parseInt(viewHolder.invoices.getText().toString());
                            String invoice_val=String.valueOf(Integer.valueOf(quan));
                            Toast.makeText(getApplicationContext(), quan, Toast.LENGTH_LONG).show();



                            //dbHandler.updateInvoiceBatch(product_id,invoice_val);
                        }
                    }
                }); */


            }

            return convertView;
        }


        public class ViewHolder {
            TextView expirydate, batchnumber, quantity;
            EditText invoices, dummy;

        }

        class ListItem {
            String caption;
        }


    }
}
