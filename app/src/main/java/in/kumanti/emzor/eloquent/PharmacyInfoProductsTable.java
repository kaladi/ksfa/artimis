package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.PharmacyInfoProducts;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_PHARMACY_INFO_PRODUCTS;

public class PharmacyInfoProductsTable extends SQLiteOpenHelper {

    private static final String COLUMN_PHARMACY_INFO_PRODUCT_ID = "id";
    private static final String COLUMN_PHARMACY_INFO_ID = "pharmacy_info_id";
    private static final String COLUMN_PHARMACY_INFO_PRODUCT_CODE = "product_code";
    private static final String COLUMN_PHARMACY_INFO_PRODUCT_NAME = "product_name";
    private static final String COLUMN_PHARMACY_INFO_PRODUCT_TYPE = "product_type";
    private static final String COLUMN_PHARMACY_INFO_PRODUCT_PRESCRIPTIONS_COUNT = "prescriptions_count";
    private static final String COLUMN_PHARMACY_INFO_PRODUCT_CREATED_AT = "created_at";
    private static final String COLUMN_PHARMACY_INFO_PRODUCT_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public PharmacyInfoProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_PHARMACY_INFO_PRODUCTS);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_PHARMACY_INFO_PRODUCTS + "(" +
                COLUMN_PHARMACY_INFO_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PHARMACY_INFO_ID + " TEXT, " +
                COLUMN_PHARMACY_INFO_PRODUCT_CODE + " TEXT, " +
                COLUMN_PHARMACY_INFO_PRODUCT_NAME + " TEXT, " +
                COLUMN_PHARMACY_INFO_PRODUCT_TYPE + " TEXT, " +
                COLUMN_PHARMACY_INFO_PRODUCT_PRESCRIPTIONS_COUNT + " TEXT, " +
                COLUMN_PHARMACY_INFO_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_PHARMACY_INFO_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(PharmacyInfoProducts pharmacyInfoProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_PHARMACY_INFO_ID, pharmacyInfoProducts.getPharmacyInfoId());
        values.put(COLUMN_PHARMACY_INFO_PRODUCT_CODE, pharmacyInfoProducts.getProductCode());
        values.put(COLUMN_PHARMACY_INFO_PRODUCT_NAME, pharmacyInfoProducts.getProductName());
        values.put(COLUMN_PHARMACY_INFO_PRODUCT_TYPE, pharmacyInfoProducts.getProductType());
        values.put(COLUMN_PHARMACY_INFO_PRODUCT_PRESCRIPTIONS_COUNT, pharmacyInfoProducts.getPrescriptionsCount());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PHARMACY_INFO_PRODUCTS, null, values);
        db.close();
        return id;
    }

    public ArrayList<PharmacyInfoProducts> getPharmacyInfoProducts(String pharmacyId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PHARMACY_INFO_PRODUCTS + " WHERE " + COLUMN_PHARMACY_INFO_ID + "=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{pharmacyId});
        Log.d("Data", "Get Products  ");

        ArrayList<PharmacyInfoProducts> pharmacyInfoProductsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            PharmacyInfoProducts product = new PharmacyInfoProducts();
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            product.setId(cursor.getInt(0));
            product.setPharmacyInfoId(cursor.getString(1));
            product.setProductCode(cursor.getString(2));
            product.setProductName(cursor.getString(3));
            product.setProductType(cursor.getString(4));
            product.setPrescriptionsCount(cursor.getString(5));
            pharmacyInfoProductsArrayList.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return pharmacyInfoProductsArrayList;
    }

    public ArrayList<PharmacyInfoProducts> getPharmacyInfoProducts() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_PHARMACY_INFO_PRODUCTS;

        ArrayList<PharmacyInfoProducts> pharmacyInfoArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            PharmacyInfoProducts pharmacyInfoProducts = new PharmacyInfoProducts();
            pharmacyInfoProducts.setTabCode(Constants.TAB_CODE);
            pharmacyInfoProducts.setCompanyCode(Constants.COMPANY_CODE);
            pharmacyInfoProducts.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            pharmacyInfoProducts.setId(id);
            pharmacyInfoProducts.setPharmacyInfoId(cursor.getString(1));
            pharmacyInfoProducts.setProductCode(cursor.getString(2));
            pharmacyInfoProducts.setProductName(cursor.getString(3));
            pharmacyInfoProducts.setProductType(cursor.getString(4));
            pharmacyInfoProducts.setPrescriptionsCount(cursor.getString(5));
            pharmacyInfoProducts.setCreatedAt(cursor.getString(6));
            pharmacyInfoProducts.setUpdatedAt(cursor.getString(7));

            pharmacyInfoArrayList.add(pharmacyInfoProducts);
        }
        cursor.close();
        sqlWrite.close();
        return pharmacyInfoArrayList;
    }

    public void deletePrescriptionProduct(String pharmacyId) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_PHARMACY_INFO_PRODUCTS, COLUMN_PHARMACY_INFO_ID + "=?", new String[]{pharmacyId});
            db.close();
        } catch (Exception e) {
            Log.d("DeleteProduct", "Error" + e.getMessage());
        }

    }

}
