package in.kumanti.emzor.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.StockReceiptBatchActivity;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.StockReceiptDistributorAdapter;
import in.kumanti.emzor.eloquent.DistributorsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.model.Distributors;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;

import static android.content.Context.MODE_PRIVATE;
import static in.kumanti.emzor.activity.MainActivity.PREF_NAME;


public class StockReceiptDistributorFragment extends Fragment {

    public RecyclerView stockReceiptDistributorProdRecyclerView;
    CustomSearchableSpinner distributorNameSpinner, productSpinner;
    TextView receipt_num, receipt_date;
    ArrayAdapter<Product> productSpinnerArrayAdapter;
    ArrayAdapter<Distributors> distributorSpinnerArrayAdapter;
    ArrayList<Distributors> distributorArrayList;
    ArrayList<Product> productArrayList;
    ArrayList<Product> newProductList;
    ProductTable productTable;
    ArrayList<StockReceipt> stockReceiptDisProductArrayList = new ArrayList<StockReceipt>();
    Context mContext;
    MyDBHandler dbHandler;
    DistributorsTable distributorsTable;
    String login_id = "", warehouse_id = null, invoice_id = null, product_id = null, str_receipt_id = null, str_receip_date = null, distributor_id = null, distributor_name = "";
    int receipt_id;
    Button addRow;
    RecyclerViewItemListener listener, listener1;
    ImageButton saveStockReceiptButton;
    int random_num = 0;
    String company_code, tab_code, sr_code, tab_prefix;
    Constants constants;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private RecyclerView.Adapter stockReceiptDistributorProdAdapter;
    private RecyclerView.LayoutManager stockReceiptDistributorProdLayoutManager;


    public StockReceiptDistributorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_stock_receipt_distributor, container, false);
        mContext = rootView.getContext();


        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        dbHandler = new MyDBHandler(mContext, null, null, 1);
        distributorsTable = new DistributorsTable(mContext);

        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
        }

        productTable = new ProductTable(mContext);

        distributorNameSpinner = rootView.findViewById(R.id.stockReceiptDisSpinner);
        productSpinner = rootView.findViewById(R.id.stockReceiptDisProdSpinner);
        stockReceiptDistributorProdRecyclerView = rootView.findViewById(R.id.stockReceiptDisRecyclerView);
        addRow = rootView.findViewById(R.id.addRowButton);
        saveStockReceiptButton = rootView.findViewById(R.id.saveDisStockReceiptImageButton);
        receipt_num = rootView.findViewById(R.id.receipt_num);
        receipt_date = rootView.findViewById(R.id.receipt_date);

        constants = new Constants();
        tab_prefix = Constants.TAB_PREFIX;
        company_code = Constants.COMPANY_CODE;
        tab_code = Constants.TAB_CODE;
        sr_code = Constants.SR_CODE;

        //To Generate the Recepit number
        receipt_id = dbHandler.get_reciept_id();
        //int rec_num = receipt_id + 1;
        //str_receipt_id = Integer.toString(rec_num);
        //receipt_num.setText(str_receipt_id);
        generateStockReceiptNumber();

        setRecieptDateTime();

        //Distributor Spinnser Initialization
        //To Set the Values to the Product Spinner
        distributorArrayList = distributorsTable.getDistributors();
        Distributors cd = new Distributors();
        cd.setDistributorCode("-1");
        cd.setDistributorName("Select");
        distributorArrayList.add(0, cd);
        distributorNameSpinner.setTitle("Select a Distributor");
        distributorNameSpinner.setPositiveButton("OK");
        distributorSpinnerArrayAdapter = new ArrayAdapter<Distributors>(mContext, android.R.layout.simple_spinner_item, distributorArrayList);
        distributorSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        distributorNameSpinner.setAdapter(distributorSpinnerArrayAdapter);
        distributorNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                distributorNameSpinner.isSpinnerDialogOpen = false;

                Distributors cd = (Distributors) adapterView.getItemAtPosition(pos);
                distributor_name = cd.getDistributorName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                distributorNameSpinner.isSpinnerDialogOpen = false;
            }
        });

        //Product Spinner Initialization
        updateProductSpinner();
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;

                Product p = (Product) adapterView.getItemAtPosition(pos);
                product_id = p.product_id;


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });


        stockReceiptDistributorProdRecyclerView.setHasFixedSize(true);
        stockReceiptDistributorProdLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        stockReceiptDistributorProdRecyclerView.setLayoutManager(stockReceiptDistributorProdLayoutManager);

        listener = (view, position) -> {
            updateProductSpinner();
        };
        listener1 = (view, position) -> {

            Intent intent = new Intent(getContext(), StockReceiptBatchActivity.class);
            intent.putExtra("product_id", stockReceiptDisProductArrayList.get(position).getProductId());
            intent.putExtra("product_name", stockReceiptDisProductArrayList.get(position).getProductName());
            intent.putExtra("product_quantity", stockReceiptDisProductArrayList.get(position).getProductQuantity());
            intent.putExtra("product_batch_details", stockReceiptDisProductArrayList.get(position).getBatchDetailArrayList());
            getActivity().startActivityForResult(intent, 2);
        };
        stockReceiptDistributorProdAdapter = new StockReceiptDistributorAdapter(stockReceiptDisProductArrayList, listener, listener1);
        stockReceiptDistributorProdRecyclerView.setAdapter(stockReceiptDistributorProdAdapter);

        //Generate new row or item in opportunity details Recycler view
        addRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addStockReceiptDetail();
            }
        });


        saveStockReceiptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stockReceiptDisProductArrayList.size() != 0) {
                    boolean validCheck = true;
                    for (StockReceipt srdp : stockReceiptDisProductArrayList) {
                        if (srdp.getBatchControlled().equals("Yes") && srdp.getBatchDetailArrayList().size() == 0) {
                            srdp.setError_productName("Please add Batch Details");
                            validCheck = false;
                        } else {
                            srdp.setError_productName(null);
                        }

                        if (TextUtils.isEmpty(srdp.getProductQuantity())) {
                            srdp.setError_productQuantity("Please enter quantity");
                            validCheck = false;
                        } else {
                            srdp.setError_productQuantity(null);
                            if (srdp.getBatchControlled().equals("Yes") && srdp.getBatchDetailArrayList().size() != 0 && srdp.getBatchDetailArrayListTotalCount() != Integer.parseInt(srdp.getProductQuantity())) {
                                srdp.setError_productName("Batch total quantity doesnt match with quantity");
                                validCheck = false;
                            } else if (srdp.getBatchControlled().equals("Yes") && srdp.getBatchDetailArrayList().size() != 0) {
                                srdp.setError_productName(null);
                            }
                        }

                        if (TextUtils.isEmpty(srdp.getProductPrice()) || Double.parseDouble(srdp.getProductPrice()) == 0) {
                            srdp.setError_productPrice("Please enter valid price");
                            validCheck = false;
                        } else {
                            srdp.setError_productPrice(null);
                        }
                    }
                    if (!validCheck) {
                        stockReceiptDistributorProdAdapter = new StockReceiptDistributorAdapter(stockReceiptDisProductArrayList, listener, listener1);
                        stockReceiptDistributorProdRecyclerView.setAdapter(stockReceiptDistributorProdAdapter);
                        //stockReceiptDistributorProdAdapter.notifyDataSetChanged();
                        //Toast.makeText(mContext, "Please add Batch Details", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Distributors cd = (Distributors) distributorNameSpinner.getSelectedItem();

                    if (cd.getDistributorCode().equals("-1")) {
                        Toast.makeText(getContext(), "Please select the distributor Name", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.stock_receipt_save_confirmation, null);
                    Button saveStockReceiptNo = alertLayout.findViewById(R.id.stockReceiptNoButton),
                            saveStockReceiptYes = alertLayout.findViewById(R.id.stockReceiptYesButton);

                    AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(false);

                    AlertDialog dialog = alert.create();

                    saveStockReceiptYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (StockReceipt srwp : stockReceiptDisProductArrayList) {
                                try {
                                    String visitSequence = sharedPreferences.getString("VisitSequence", "");
                                    long id = dbHandler.createstockreciept
                                            (str_receipt_id,
                                                    str_receip_date,
                                                    "NGN",
                                                    login_id,
                                                    distributor_name,
                                                    "Stock Receipt",
                                                    "distributor",
                                                    srwp.getProductId(),
                                                    srwp.getProductUom(),
                                                    srwp.getProductQuantity(),
                                                    srwp.getProductPrice(),
                                                    srwp.getProductTotalPrice(),
                                                    "",
                                                    "",
                                                    "Completed",
                                                    random_num,
                                                    visitSequence,
                                                    receipt_num.getText().toString(),
                                                    "");
                                    for (StockReceiptBatch srbd : srwp.getBatchDetailArrayList()) {
                                        dbHandler.insertingProductBatch(srwp.getProductId(), srbd.getBatchNumber(), srbd.getExpiryDate(), srbd.getBatchQuantity(), invoice_id, str_receipt_id, invoice_id, "Completed", "StockReceipt");
                                    }
                                    Log.d("QueryResponse", srwp.getProductName() + " id:" + id);
                                } catch (Exception e) {
                                    Log.d("QueryResponse", e.getMessage());
                                }
                            }
                            Toast.makeText(getContext(), "Stock Receipt Created Successfully", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            getActivity().finish();
                        }
                    });

                    saveStockReceiptNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                        }
                    });


                    dialog.show();

                } else {
                    Toast.makeText(getContext(), "There is no products added", Toast.LENGTH_LONG).show();
                }


            }
        });


        return rootView;
    }

    private void generateStockReceiptNumber() {

        random_num = dbHandler.get_receipt_random_num();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        if (random_num <= 9) {
            receipt_num.setText(tab_prefix + "SR" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            receipt_num.setText(tab_prefix + "SR" + rec_seq_date + "0" + random_num);

        } else {
            receipt_num.setText(tab_prefix + "SR" + rec_seq_date + random_num);
        }

        str_receipt_id = receipt_num.getText().toString();
    }


    //Saving the Distributor Stock Receipt
    private void saveStockReceipt() {


    }

    /**
     * Update the Product Spinner, based upon the addition and deletion of the products in Opportunity Product Details RecyclerView List
     */
    private void updateProductSpinner() {
        //To Set the Values to the Product Spinner
       String sec_price_list =  dbHandler.get_sec_price_list();
       if(!sec_price_list.isEmpty())
           productArrayList = productTable.getDistributorProducts(sec_price_list);
        else
           productArrayList = productTable.getDistributorProducts1();
        //Remove the already selected product
        int k = 0;
        newProductList = new ArrayList<Product>();
        for (Product product : productArrayList) {
            boolean flag = true;
            for (StockReceipt srd : stockReceiptDisProductArrayList) {
                if (product.product_id.equals(srd.getProductId()))
                    flag = false;
            }
            if (flag)
                newProductList.add(product);
            k++;
        }
        Product p = new Product();
        p.setProduct_id("-1");
        p.setProduct_name("Select");
        newProductList.add(0, p);

        productSpinner.setTitle("Select a Product");
        productSpinner.setPositiveButton("OK");
        productSpinnerArrayAdapter = new ArrayAdapter<Product>(mContext, android.R.layout.simple_spinner_item, newProductList);
        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(productSpinnerArrayAdapter);
    }


    /**
     * Adding of Products Details in Opportunity Recycler View List
     */
    private void addStockReceiptDetail() {
        if (productSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(mContext, "Please choose a product before adding to the Product List", Toast.LENGTH_SHORT).show();
            return;
        }

        Product selectedProduct = newProductList.get(productSpinner.getSelectedItemPosition());
        StockReceipt srd = new StockReceipt();
        srd.setProductName(selectedProduct.product_name);
        srd.setProductUom(selectedProduct.product_uom);
        srd.setProductId(selectedProduct.product_id);

        System.out.println("TTT::selectedProduct.product_price = " + selectedProduct.product_price);
        if(selectedProduct.product_price == null){
            srd.setProductPrice("0");
        }else
        srd.setProductPrice(selectedProduct.product_price);

        srd.setBatchControlled(selectedProduct.batch_controlled);
        stockReceiptDisProductArrayList.add(srd);
        stockReceiptDistributorProdAdapter.notifyDataSetChanged();
        Log.d("Recycle", "Data Added: " + selectedProduct.product_name);
        updateProductSpinner();
    }

    private void setRecieptDateTime() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        receipt_date.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        str_receip_date = timeStampFormat.format(myDate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("DataCheck", "Entered Child Fragment" + requestCode);
        //super.onActivityResult(requestCode, resultCode, data); //comment this unless you want to pass your result to the activity.
        if (requestCode == 2) {
            Log.d("DataCheck", "Entered in to Check");
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String product_id = (String) bundle.get("product_id");
                ArrayList<StockReceiptBatch> product_batch_details = (ArrayList<StockReceiptBatch>) bundle.get("product_batch_details");

                for (StockReceipt srwp : stockReceiptDisProductArrayList) {
                    Log.d("DataCheck", "Product Ids " + product_id + " " + srwp.getProductId());
                    Log.d("DataCheck", "Quantity" + srwp.getProductQuantity());
                    if (product_id.equals(srwp.getProductId())) {
                        Log.d("DataCheck", "Value Assigned in product");
                        srwp.setBatchDetailArrayList(product_batch_details);
                        if (product_batch_details.size() != 0)
                            srwp.setError_productName(null);
                        Log.d("DataCheck", "Values" + srwp.getBatchDetailArrayList().size());
                    }
                }
                stockReceiptDistributorProdAdapter = new StockReceiptDistributorAdapter(stockReceiptDisProductArrayList, listener, listener1);
                stockReceiptDistributorProdRecyclerView.setAdapter(stockReceiptDistributorProdAdapter);
                //stockReceiptDistributorProdAdapter.notifyDataSetChanged();
            }
        }
    }
}






