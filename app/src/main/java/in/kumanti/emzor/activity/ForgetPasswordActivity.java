package in.kumanti.emzor.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;

public class ForgetPasswordActivity extends AppCompatActivity {
    String txt_password, user_password;
    EditText forget_password;
    View focusView = null;
    boolean cancel = false;
    String email;
    TextView sent_password;
    Button send, login;
    MyDBHandler dbHandler;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        forget_password = findViewById(R.id.forget_password);
        send = findViewById(R.id.send);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            email = (String) bd.get("email");
        }
        forget_password.setText(email);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
        }

        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                txt_password = forget_password.getText().toString();
                forget_password.setError(null);
                if (TextUtils.isEmpty(txt_password)) {
                    forget_password.setError(getString(R.string.error_field_required));
                    focusView = forget_password;
                    cancel = true;
                } else if (!txt_password.matches(emailPattern)) {

                    forget_password.setError("Invalid Email Address");

                } else {
                    getPassword();
                }
            }
        });
    }

    private void getPassword() {

        user_password = dbHandler.getUserPassword(email);
        Log.i("user_password", user_password);
        if (user_password == "0") {
            Toast.makeText(this.getApplicationContext(), "No Records", Toast.LENGTH_LONG).show();

        }
    }


    private void sendemail() {

        String to = email;
        String subject = "Your Current Password";
        String message = "Your Current Password is  " + user_password + " Kindly do not share your Password  ";

        // We call  action Send in order to send the Email.
        try {
            if (message.equals("")) {
                Toast.makeText(getApplicationContext(), "Empty Message, Do you want to Continue", Toast.LENGTH_LONG).show();
            } else {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                // we need setType to prompts only email clients.
                email.setType("message/rfc822");

                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Mail not Sent, Please try again..!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void showpopup() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.messsent_popup, null);
        login = alertLayout.findViewById(R.id.login);
        sent_password = alertLayout.findViewById(R.id.sent_password);
        sent_password.setText("Password Sent to your Mail Id");
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mapIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(mapIntent);
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }
}
