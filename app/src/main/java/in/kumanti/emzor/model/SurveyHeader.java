package in.kumanti.emzor.model;


public class SurveyHeader {

    public String survey_id;
    public String survey_name;
    public String survey_code;
    public String survey_description;
    public String survey_type;
    public String effective_start_date;
    public String effective_end_date;

    public String getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(String survey_id) {
        this.survey_id = survey_id;
    }

    public String getSurvey_name() {
        return survey_name;
    }

    public void setSurvey_name(String survey_name) {
        this.survey_name = survey_name;
    }

    public String getSurvey_code() {
        return survey_code;
    }

    public void setSurvey_code(String survey_code) {
        this.survey_code = survey_code;
    }

    public String getSurvey_description() {
        return survey_description;
    }

    public void setSurvey_description(String survey_description) {
        this.survey_description = survey_description;
    }

    public String getSurvey_type() {
        return survey_type;
    }

    public void setSurvey_type(String survey_type) {
        this.survey_type = survey_type;
    }

    public String getEffective_start_date() {
        return effective_start_date;
    }

    public void setEffective_start_date(String effective_start_date) {
        this.effective_start_date = effective_start_date;
    }

    public String getEffective_end_date() {
        return effective_end_date;
    }

    public void setEffective_end_date(String effective_end_date) {
        this.effective_end_date = effective_end_date;
    }
}
