package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import in.kumanti.emzor.R;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LedgerTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.Invoices;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.Ledger;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.InputFilterMinMax;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;


public class CollectionActivity extends MainActivity implements AdapterView.OnItemSelectedListener {

    private static final int CAPTURE_PHOTO1 = 2;
    TextView actionbarTitle;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView customerName, customerCode, accountNo, city, checkInTimeText;
    String login_id, customer_type, customer_id = "", customerNameString, customerEmailAddress, customerAddress1, customerAddress2, customerAddress3;
    MyDBHandler dbHandler;
    Chronometer duration;
    TextView collectionNo, collectionDate, currency, invoiceAmount, paidAmount, dueAmount, chequeDate, transferDate;
    RadioButton onAccountRb, againstInvoiceRb, cashPaymentRb, chequePaymentRb, transferPaymentRb;
    Spinner invoiceNumbSpinner, orderNumbSpinner;
    EditText paymentAmount, chequeNo, chequeBankBranch, transferAcNo, transferBankBranch, transferTellerNo;
    ImageView collectionSaveButton, createSignature, viewSignature, captureLpoImage, showLpoImagesGallery;
    LinearLayout amountContainer, paymentContainer, invoiceNumberContainer, orderNumberContainer;
    LinearLayout chequeHeaderContainer, chequeValueContainer, bankTransferHeaderContainer, bankTransferValueContainer;
    int image_id = 0;
    LpoImagesTable lpoImagesTable;
    LedgerTable ledgerTable;
    Bitmap selectedImage = null;
    String save_collection_date, orderNumber, save_check_date, save_transfer_date;
    String checkin_time = "";
    int collection_id = 0;
    String sr_name1, ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1, sr_name_details;
    int a[] = new int[5];
    long stopTime = 0;
    String collection_type = "Against Invoice";
    String againstType, invoiceDate = "", orderDate = "", primaryInvoiceDate = "", secInvoiceDate = "";
    String paymentMode = "Cash";

    double pay_amount, inv_amount, paid_amt, due_amt = 0.00;
    String invoice_id;
    DecimalFormat decimalFormatter;
    String formatInvoiceAmount, formatPaidAmount, formatDueAmount, formatPaymentAmount;
    int currentCheckedRadio, currentCheckedRadio1;
    String tab_prefix;
    int random_num = 0;
    EditText signatureName;
    List<String> lables;
    Cursor invoiceSectionDetails;
    Float quan;
    Float amnt;
    Globals globals;
    String companyName,companyCode1, compAdd1, compAdd2, compAdd3, compMblNo,tab_code;
    String collectionStatus = "";
    String invoice_date = "";
    String collectionNoString, collectionTime;
    //Variables declaration for print, email and sms
    Button smsButton, printButton, emailButton;
    PrintBluetooth printBluetooth;
    String customerPhoneNumber = "";
    String smsMessageText = "";
    Print print;
    GeneralSettingsTable generalSettingsTable;
    // android built in classes for bluetooth operations
    String printerName = "MP80-17031810239";
    String printMsg;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    GeneralSettings gs;
    private TextView marqueeText;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private int mYear, mMonth, mDay;
    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";
    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_collection);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Collection");
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        tab_code = Constants.TAB_CODE;

        setCollectionDate();
        generateCollectionNumber();

        onAccountRb.setOnCheckedChangeListener(new Radio_check());
        againstInvoiceRb.setOnCheckedChangeListener(new Radio_check());

        // Load Invoice Number
        invoiceNumbSpinner.setOnItemSelectedListener(this);
        loadInvoiceNumbers();

        paymentAmount.setEnabled(false);
        // Load Order Number
        orderNumbSpinner.setOnItemSelectedListener(this);
        loadOrderSpinnerProduct();


        paymentAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if ("onAccount".equals(collection_type)) {

                } else {
                    if (("".equals(paymentAmount.getText().toString())) || "".equals(due_amt)) {

                    } else {
                        quan = Float.parseFloat(paymentAmount.getText().toString());
                        amnt = Float.parseFloat(String.valueOf(due_amt));
                        if (quan > amnt) {
                            paymentAmount.setError("Payment Amount is greater than Due Amount");
                        } else {

                        }
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cashPaymentRb.setOnCheckedChangeListener(new Radio_check1());
        chequePaymentRb.setOnCheckedChangeListener(new Radio_check1());
        transferPaymentRb.setOnCheckedChangeListener(new Radio_check1());

        createSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopupsignature();
            }
        });


        viewSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedImage != null) {
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.image_preview, null);
                    ImageView capturedImage, closeButton;
                    capturedImage = alertLayout.findViewById(R.id.capturedImage);
                    closeButton = alertLayout.findViewById(R.id.closeImageView);
                    capturedImage.setImageBitmap(selectedImage);
                    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    // this is set the view from XML inside AlertDialog

                    alert.setView(alertLayout);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(true);

                    android.app.AlertDialog dialog = alert.create();
                    dialog.show();


                }

            }
        });


        collectionSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gs = generalSettingsTable.getSettingByKey("collectionSignature");
                if (gs != null && gs.getValue().equals("Yes")) {
                    if (TextUtils.isEmpty(signatureName.getText())) {
                        Toast.makeText(getApplicationContext(), "Please Enter Signee's Name", Toast.LENGTH_LONG).show();
                        return;
                    } else if (selectedImage == null) {
                        Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                        return;
                    }

                }


                if ("onAccount".equals(collection_type)) {
                    saveCollectionData(v);
                } else {
                    if ("".equals(paymentAmount.getText().toString())) {
                        paymentAmount.setError("Enter Payment Amount");

                    } else {
                        quan = Float.parseFloat(paymentAmount.getText().toString());
                        amnt = Float.parseFloat(String.valueOf(due_amt));
                        if (quan > amnt) {
                            paymentAmount.setError("Payment Amount is greater than Due Amount");
                        } else {
                            saveCollectionData(v);
                        }
                    }

                }

            }
        });

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (collectionStatus.equals("Completed")) {
                    Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                    long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    duration.stop();

                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("sales_rep_id", login_id);
                    bundle.putString("customer_id", customer_id);
                    bundle.putString("nav_type", "Home");

                    myintent.putExtras(bundle);
                    startActivity(myintent);
                    return;
                }
                final Dialog alertbox = new Dialog(v.getRootView().getContext());

                LayoutInflater mInflater = (LayoutInflater)
                        getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                final View alertLayout = mInflater.inflate(R.layout.activity_exit_collection_popup, null);

                alertbox.setCancelable(false);
                //alertbox.setView(alertLayout);
                alertbox.setContentView(alertLayout);

                final Button Yes = alertLayout.findViewById(R.id.Yes);
                final Button No = alertLayout.findViewById(R.id.No);

                Yes.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {
                        lpoImagesTable.deleteCancelledImages(collectionNo.getText().toString());
                        //if (collection_id == 0) {
                        Log.d("Test****","CancellingYes--1");

                        Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                        long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                        SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                        duration.stop();

                        Bundle bundle = new Bundle();
                        bundle.putString("checkin_time", checkin_time);
                        bundle.putString("sales_rep_id", login_id);
                        bundle.putString("customer_id", customer_id);
                        bundle.putString("nav_type", "Home");

                        myintent.putExtras(bundle);
                        startActivity(myintent);
//                        } else {
//                            Log.d("Test****","CancellingYes--2");
//                            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
//                            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
//                            updateCollectionStatus("Cancelled");
//                            Toast.makeText(getApplicationContext(), "Collection Cancelled", Toast.LENGTH_LONG).show();
//                        }
                    }


                });

                No.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        Log.d("****Test****","CancellingNo");


                        alertbox.dismiss();
                    }

                });

                alertbox.show();

            }

        });


        /**
         * Sending SMS Manager API using default SMS manager
         */


        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!collectionStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save collection to send sms", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(customerPhoneNumber)) {
                    Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                    return;
                }
                checkPaymentType();


                smsMessageText ="Payment Receipt\n" +
                        "\n" +
                        "Thanks for trusting " + Constants.COMPANY_NAME + " Products." +
                        "\n" +
                        "Please refer your"+
                        "\n" +
                        "Receipt No: #" + collectionNo.getText().toString() + "\n" +
                        "Receipt Date: " + collectionDate.getText() + "\n" +
                        "Aganist: " + againstType + "\n";
                System.out.println("smsMessageTextC = " + smsMessageText);
                if ("Primary".equals(customer_type)) {
                    if (againstInvoiceRb.isChecked()) {
                        smsMessageText += "Invoice No: " + invoice_id + "\n";
                    } else {
                        smsMessageText += "Order No: " + orderNumber + "\n";
                    }
                } else {
                    if (againstInvoiceRb.isChecked()) {
                        smsMessageText += "Invoice No: " + invoice_id + "\n";
                    }
                }

                smsMessageText += "Payment Mode: " + paymentMode + "\n";

                if (chequePaymentRb.isChecked())
                    smsMessageText += "Cheque No: " + chequeNo.getText().toString() + "\n" + "Bank & Branch: " + chequeBankBranch.getText().toString() + "\n";
                else if (transferPaymentRb.isChecked())
                    smsMessageText += "Transfer No: " + transferTellerNo.getText().toString() + "\n" + "Bank & Branch: " + transferBankBranch.getText().toString() + "\n";
                smsMessageText += "Receipt Amount: " + "NGN " +paymentAmount.getText().toString() + "\n" +
                        "care.nigeria@artemislife.com"+ "\n";

                System.out.println("smsMessageText = " + smsMessageText);

                Gson gson = new GsonBuilder().setLenient().create();
                JsonObject jp = new JsonObject();

                if ("Primary".equals(customer_type)) {
                    jp.addProperty("smsType", "collectionOrder");

                } else {
                    jp.addProperty("smsType", "collectionInvoice");

                }
                jp.addProperty("smsMessageText", smsMessageText);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);

                jp.addProperty("collectionNumber", collectionNo.getText().toString());
                jp.addProperty("paymentMode", againstType);
                jp.addProperty("date", collectionDate.getText().toString());
                jp.addProperty("time", collectionTime);

                jp.addProperty("bdeName", sr_name_details);
                jp.addProperty("customerName", customerNameString);
                jp.addProperty("customerPhoneNumber", customerPhoneNumber);

                jp.addProperty("address", customerAddress1 + customerAddress2);

                jp.addProperty("paymentType", paymentMode);
                jp.addProperty("receiptAmount", paymentAmount.getText().toString());
                if (chequePaymentRb.isChecked()) {
                    jp.addProperty("chequeNo", chequeNo.getText().toString());
                    jp.addProperty("chequeDate", chequeDate.getText().toString());
                    jp.addProperty("checkBankBranch", chequeBankBranch.getText().toString());

                } else if (transferPaymentRb.isChecked()) {
                    jp.addProperty("tellerNo", transferTellerNo.getText().toString());
                    jp.addProperty("transferDate", transferDate.getText().toString());
                    jp.addProperty("transferBankBranch", transferBankBranch.getText().toString());
                }
                if ("Primary".equals(customer_type)) {
                    if (againstInvoiceRb.isChecked()) {
                        jp.addProperty("invoiceNo", invoice_id);
                        jp.addProperty("invoiceDate", invoiceDate);
                    } else {
                        jp.addProperty("orderNo", orderNumber);
                        jp.addProperty("orderDate", orderDate);
                    }
                } else {
                    if (againstInvoiceRb.isChecked()) {
                        jp.addProperty("invoiceNo", invoice_id);
                        jp.addProperty("invoiceDate", invoiceDate);
                    }
                }

                jp.addProperty("receiptAmount", paymentAmount.getText().toString());

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.SMS_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingCollectionSMSData(jp);
                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }
                });


             /*   try {
                    smsMessageText = "Payment Receipt\n" +
                            "\n" +
                            "Receipt No: #" + collectionNo.getText().toString() + "\n" +
                            "Receipt Date: " + collectionDate.getText() + "\n" +
                            "Receipt Time: " + collectionTime + "\n" +
                            "Aganist: " + againstType + "\n";

                    if ("Primary".equals(customer_type)) {
                        if (againstInvoiceRb.isChecked()) {
                            smsMessageText += "Invoice No: " + invoice_id + "\n";
                        } else {
                            smsMessageText += "Order No: " + orderNumber + "\n";
                        }
                    } else {
                        if (againstInvoiceRb.isChecked()) {
                            smsMessageText += "Invoice No: " + invoice_id + "\n";
                        }
                    }

                    smsMessageText += "Payment Mode: " + paymentMode + "\n";

                    if (chequePaymentRb.isChecked())
                        smsMessageText += "Cheque No: " + chequeNo.getText().toString() + "\n" + "Bank & Branch: " + chequeBankBranch.getText().toString() + "\n";
                    else if (transferPaymentRb.isChecked())
                        smsMessageText += "Transfer No: " + transferTellerNo.getText().toString() + "\n" + "Bank & Branch: " + transferBankBranch.getText().toString() + "\n";
                    smsMessageText += "Receipt Amount: " + paymentAmount.getText().toString() + "\n";

                    SmsManager smsManager = SmsManager.getDefault();
                    ArrayList<String> parts = smsManager.divideMessage(smsMessageText);
                    //smsManager.sendTextMessage(phoneNumber, null, message, null, null);
                    smsManager.sendMultipartTextMessage(customerPhoneNumber, null, parts,
                            null, null);
                    smsManager.sendTextMessage(customerPhoneNumber, null, smsMessageText, null, null);
                    Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.d("Sms", "Exception" + e);
                }
*/

            }
        });



        /*
         * Print the order
         */


        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!collectionStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save collection to print", Toast.LENGTH_LONG).show();
                    return;
                }


                try {
                    checkPaymentType();
                    int totalSize = 48;
                    String ESC_NEW_LINE = "\n";
                    String horizontalLine = "-----------------------------------------------";
                    printMsg = "";
                    printMsg += print.centerString(totalSize, companyName);
                    printMsg += print.centerString(totalSize, compAdd1);
                    printMsg += print.centerString(totalSize, compAdd2);
                    printMsg += print.centerString(totalSize, compAdd3);
                    printMsg += print.centerString(totalSize, compMblNo);

                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + "Receipt Number:" + collectionNo.getText().toString();
                    printMsg += ESC_NEW_LINE + "Payment Mode:" + againstType;
                    printMsg += ESC_NEW_LINE + print.padRight("Date:" + collectionDate.getText(), 24) + print.padLeft("TIME:" + collectionTime, 24);
                    printMsg += "BDE Name:" + sr_name_details;
                    printMsg += ESC_NEW_LINE + "Customer Name:" + customerNameString;
                    printMsg += ESC_NEW_LINE + "Address:";
                    printMsg += print.padRight(customerAddress1, totalSize);
                    printMsg += print.padRight(customerAddress2, totalSize);
                    printMsg += print.padRight(customerAddress3, totalSize);

                    printMsg += ESC_NEW_LINE + horizontalLine;


                    printMsg += ESC_NEW_LINE + "Payment Type:" + paymentMode + ESC_NEW_LINE;

                    if (chequePaymentRb.isChecked()) {
                        printMsg += print.padRight("ChequeNo:" + chequeNo.getText(), 24) + print.padLeft("Date:" + chequeDate.getText(), 20);
                        printMsg += ESC_NEW_LINE + "Bank&Branch Name:" + chequeBankBranch.getText().toString();
                    } else if (transferPaymentRb.isChecked()) {
                        printMsg += print.padRight("Teller No:" + transferTellerNo.getText().toString(), 24) + print.padLeft("Date:" + transferDate.getText().toString(), 20);
                        printMsg += ESC_NEW_LINE + "Bank&Branch Name:" + transferBankBranch.getText().toString();
                    }
                    if ("Primary".equals(customer_type)) {
                        if (againstInvoiceRb.isChecked()) {
                            printMsg += ESC_NEW_LINE + "Receipt Number:" + collectionNo.getText().toString();
                            printMsg += ESC_NEW_LINE + "Payment Mode:" + againstType;


                            printMsg += ESC_NEW_LINE + "Invoice No:" + invoice_id;
                            Log.d("Collection", "Invoice Number:----" + invoice_id + "Invoice Date:--" + primaryInvoiceDate);
                            printMsg += ESC_NEW_LINE + "Date:" + primaryInvoiceDate;
                        } else {
                            printMsg += ESC_NEW_LINE + "Order No:" + orderNumber;
                            printMsg += ESC_NEW_LINE + "Date:" + orderDate;
                        }
                    } else {
                        if (againstInvoiceRb.isChecked()) {
                            printMsg += ESC_NEW_LINE + "Invoice No:" + invoice_id;
                            printMsg += ESC_NEW_LINE + "Date:" + invoiceDate;
                        }
                    }
                    printMsg += ESC_NEW_LINE + "Receipt Amount:" + paymentAmount.getText();

                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.padLeft("Customer Sign", totalSize);
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    print.printContent(printMsg);
                    // printBluetooth.printContent(printMsg, selectedImage);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        });


        /*
         * Sending email using webService
         */

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!collectionStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save collection to send email", Toast.LENGTH_LONG).show();
                    return;
                }
                checkPaymentType();

                Gson gson = new GsonBuilder().setLenient().create();
                JsonObject jp = new JsonObject();

                if ("Primary".equals(customer_type)) {
                    jp.addProperty("emailType", "collectionOrder");

                } else {
                    jp.addProperty("emailType", "collectionInvoice");

                }

                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);

                jp.addProperty("collectionNumber", collectionNo.getText().toString());
                jp.addProperty("paymentMode", againstType);
                jp.addProperty("date", collectionDate.getText().toString());
                jp.addProperty("time", collectionTime);

                jp.addProperty("bdeName", sr_name_details);
                jp.addProperty("customerName", customerNameString);
                jp.addProperty("customerEmailAddress", customerEmailAddress);

                jp.addProperty("address", customerAddress1 + customerAddress2);

                jp.addProperty("paymentType", paymentMode);
                jp.addProperty("receiptAmount", paymentAmount.getText().toString());
                if (chequePaymentRb.isChecked()) {
                    jp.addProperty("chequeNo", chequeNo.getText().toString());
                    jp.addProperty("chequeDate", chequeDate.getText().toString());
                    jp.addProperty("checkBankBranch", chequeBankBranch.getText().toString());

                } else if (transferPaymentRb.isChecked()) {
                    jp.addProperty("tellerNo", transferTellerNo.getText().toString());
                    jp.addProperty("transferDate", transferDate.getText().toString());
                    jp.addProperty("transferBankBranch", transferBankBranch.getText().toString());
                }
                if ("Primary".equals(customer_type)) {
                    if (againstInvoiceRb.isChecked()) {
                        jp.addProperty("invoiceNo", invoice_id);
                        jp.addProperty("invoiceDate", invoiceDate);
                    } else {
                        jp.addProperty("orderNo", orderNumber);
                        jp.addProperty("orderDate", orderDate);
                    }
                } else {
                    if (againstInvoiceRb.isChecked()) {
                        jp.addProperty("invoiceNo", invoice_id);
                        jp.addProperty("invoiceDate", invoiceDate);
                    }
                }

                jp.addProperty("receiptAmount", paymentAmount.getText().toString());

                if (selectedImage != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                    jp.addProperty("customerSignature", encodedImage);
                }
                Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingEmailData(jp);

                Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }
                });
            }
        });


        chequeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                // subtract 2 Months from now
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(CollectionActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {
                                    String PATTERN = "dd MMM YY";
                                    String PATTERN1 = "yyyy-MM-dd";

                                    SimpleDateFormat dateFormat = new SimpleDateFormat();
                                    Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    dateFormat.applyPattern(PATTERN);
                                    dateFormat.format(d);
                                    //txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    chequeDate.setText(dateFormat.format(d));

                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat();
                                    Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    dateFormat1.applyPattern(PATTERN1);
                                    dateFormat1.format(d1);

                                    save_check_date = dateFormat1.format(d1);


                                } catch (Exception e) {

                                }


                            }
                        }, mDay, mMonth, mYear);

                c.add(Calendar.MONTH, -1);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.getDatePicker().updateDate(mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


        transferDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(CollectionActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                try {
                                    String PATTERN = "dd MMM YY";
                                    String PATTERN1 = "yyyy-MM-dd";

                                    SimpleDateFormat dateFormat = new SimpleDateFormat();
                                    Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    dateFormat.applyPattern(PATTERN);
                                    dateFormat.format(d);
                                    //txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    transferDate.setText(dateFormat.format(d));

                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat();
                                    Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    dateFormat1.applyPattern(PATTERN1);
                                    dateFormat1.format(d1);

                                    save_transfer_date = dateFormat1.format(d1);


                                } catch (Exception e) {

                                }


                            }
                        }, mDay, mMonth, mYear);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        captureLpoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent1, CAPTURE_PHOTO1);

            }
        });

        showLpoImagesGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int lpo_image_count = lpoImagesTable.get_lpo_order_image_count(collectionNo.getText().toString());

                if (lpo_image_count == 0) {
                    Toast.makeText(CollectionActivity.this, "No Images,Capture and Check again.", Toast.LENGTH_SHORT).show();
                } else {
                    Intent myintent = new Intent(getApplicationContext(), OrderGalleryActivity.class);

                    //Create the bundle
                    Bundle bundle = new Bundle();

                    //Add your data to bundle

                    bundle.putString("customer_id", customer_id);
                    bundle.putString("login_id", login_id);
                    bundle.putString("order_number", collectionNo.getText().toString());

                    myintent.putExtras(bundle);
                    startActivity(myintent);
                }


            }
        });
    }

    private void checkPaymentType() {
        againstType = "On Account";
        if (againstInvoiceRb.isChecked())
            againstType = "Against Invoice";
        paymentMode = "Cash";
        if (chequePaymentRb.isChecked())
            paymentMode = "Cheque";
        else if (transferPaymentRb.isChecked())
            paymentMode = "Bank Transfer";
        if ("Primary".equals(customer_type)) {
            if (againstInvoiceRb.isChecked()) {
                Cursor primaryInvoiceDetails = dbHandler.getPrimaryinvoiceSectionDetails(invoice_id);
                //invoiceDate = "12 Apr 19";


                String PATTERN = "dd MMM YY";
                try {
                    primaryInvoiceDate = primaryInvoiceDetails.getString(3);
                    SimpleDateFormat dateFormat = new SimpleDateFormat();
                    Date d = new SimpleDateFormat("MM/dd/yyyy").parse(primaryInvoiceDate);
                    dateFormat.applyPattern(PATTERN);
                    dateFormat.format(d);
                    primaryInvoiceDate = dateFormat.format(d);
                    Log.d("Collection", "Invoice Number:----" + invoice_id + "Invoice Date:--" + primaryInvoiceDate);
                    Log.d("Collections:", "Invoice Date:--" + primaryInvoiceDate);

                } catch (Exception e) {
                    // ledger.setInvoiceDate(cursor.getString(3));
                }
            } else {
                Cursor orderHeaderInfo = dbHandler.getorderHeaderInfo(invoice_id);
                int count2 = orderHeaderInfo.getCount();
                if (count2 == 0) {
                    //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
                } else {
                    int i = 0;
                    while (orderHeaderInfo.moveToNext()) {

                        Log.d("Collections:", "Order Date:--" + orderDate);
                        String PATTERN = "dd MMM YY";
                        try {
                            orderDate = orderHeaderInfo.getString(5);

                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(orderDate);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            orderDate = dateFormat.format(d);
                        } catch (Exception e) {
                            // ledger.setInvoiceDate(cursor.getString(3));
                        }
                    }
                }

            }
        } else {
            if (againstInvoiceRb.isChecked()) {
                Invoices invoice = dbHandler.getinvoiceHeader(invoice_id);
                // invoiceDate = "21 May 19";

                String PATTERN = "dd MMM YY";
                try {
                    invoiceDate = invoice.getDate();
                    SimpleDateFormat dateFormat = new SimpleDateFormat();
                    Date d = new SimpleDateFormat("yyyy-MM-dd").parse(invoiceDate);
                    dateFormat.applyPattern(PATTERN);
                    dateFormat.format(d);
                    invoiceDate = dateFormat.format(d);
                } catch (Exception e) {
                    // ledger.setInvoiceDate(cursor.getString(3));
                }
            }
        }

    }

    private void generateCollectionNumber() {
        random_num = dbHandler.get_collection_random_num(customer_type);
        collection_id = dbHandler.get_collection_sequence_value(customer_type);

        collection_id = collection_id + 1;
        random_num = random_num + 1;

        SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("ddMMyy");
        Date myDate1 = new Date();
        String collection_seq_date = timeStampFormat1.format(myDate1);

        if ("Secondary".equals(customer_type)) {

            if (random_num <= 9) {
                collectionNo.setText(tab_code + "SC"  + collection_seq_date + "00" + random_num);

            } else if (random_num > 10 & random_num < 99) {
                collectionNo.setText(tab_code + "SC"  + collection_seq_date + "0" + random_num);

            } else {
                collectionNo.setText(tab_code + "SC"  + collection_seq_date + random_num);
            }

        } else {
            if (random_num <= 9) {
                collectionNo.setText(tab_code + "PC"  + collection_seq_date + "00" + random_num);

            } else if (random_num > 10 & random_num < 99) {
                collectionNo.setText(tab_code + "PC"  + collection_seq_date + "0" + random_num);

            } else {
                collectionNo.setText(tab_code + "PC"  + collection_seq_date + random_num);
            }
        }

        collectionNoString = (collectionNo.getText().toString());

    }

    private void setCollectionDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        collectionDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        save_collection_date = timeStampFormat.format(myDate);

    }

    public void saveCollectionData(View v) {
        if ("Select".equals(invoice_id) && "Against Invoice".equals(collection_type)) {
            Toast.makeText(getApplicationContext(), "Please select invoice number", Toast.LENGTH_LONG).show();

        } else if (("".equals(paymentAmount.getText().toString()))) {
            paymentAmount.setError("Enter Payment Amount");

        } else {
            GeneralSettings gs = generalSettingsTable.getSettingByKey("signature");
            if (gs != null && gs.getValue().equals("1") && selectedImage == null) {
                Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                return;
            }
            android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
            alertbox.setTitle("Save Transaction ?");
            alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {

                    Log.d("CollectionScreen", "Inside Collection Save");

                    try {

                        if ("".equals(paymentAmount.getText().toString())) {

                        } else {
                            // pay_amount = Double.parseDouble(payment_amount.getText().toString());

                            Number number = decimalFormatter.parse(paymentAmount.getText().toString());
                            pay_amount = number.doubleValue();
                        }

                        if ("".equals(invoiceAmount.getText().toString())) {

                        } else {

                            Number number = decimalFormatter.parse(invoiceAmount.getText().toString());
                            inv_amount = number.doubleValue();
                        }

                        if ("".equals(paidAmount.getText().toString())) {

                        } else {
                            Number number = decimalFormatter.parse(paidAmount.getText().toString());
                            paid_amt = number.doubleValue();

                        }

                        if ("".equals(due_amt)) {

                        } else {
                            Number number = decimalFormatter.parse(String.valueOf(due_amt));
                            due_amt = number.doubleValue();
                        }
                    } catch (Exception e) {
                        Log.d("CollectionScreen", "Inside Collection Save Exception");

                    }
                    if ("onAccount".equals(collection_type)) {
                        if ("Cheque".equals(paymentMode)) {
                            if ("".equals(chequeNo.getText().toString())) {
                                chequeNo.setError("Enter Check Number");
                            }
                            if ("".equals(chequeDate.getText().toString())) {
                                chequeDate.setError("Enter Check Date");
                            }
                            if ("".equals(chequeBankBranch.getText().toString())) {
                                chequeBankBranch.setError("Enter Bank and Branch");
                            } else {
                                if ("Secondary".equals(customer_type)) {

                                    insertingCollectionData();

                                } else {

                                    insertingPrimaryCollectionData();
                                }
                            }
                        } else if ("Transfer".equals(paymentMode)) {
                            if ("".equals(transferAcNo.getText().toString())) {
                                transferAcNo.setError("Enter Account Number");
                            }
                            if ("".equals(transferDate.getText().toString())) {
                                transferDate.setError("Enter Transfer Date");
                            }
                            if ("".equals(transferBankBranch.getText().toString())) {
                                transferBankBranch.setError("Enter Bank and Branch");
                            } else {
                                if ("Secondary".equals(customer_type)) {

                                    insertingCollectionData();

                                } else {

                                    insertingPrimaryCollectionData();
                                }
                            }
                        } else {
                            if ("Secondary".equals(customer_type)) {

                                insertingCollectionData();

                            } else {
                                insertingPrimaryCollectionData();
                            }
                        }


                    } else {
                        Log.d("CollectionScreen", "Inside Against Invoice Save");

                        if (pay_amount > due_amt) {
                            paymentAmount.setError("Payment Amount is greater than Due Amount");
                        }
                        if (pay_amount == 0) {
                            paymentAmount.setError("Payment Amount Cannot be Zero");
                        } else {
                            if ("Cheque".equals(paymentMode)) {

                                if ("".equals(chequeNo.getText().toString())) {
                                    chequeNo.setError("Enter Check Number");
                                }
                                if ("".equals(chequeDate.getText().toString())) {
                                    chequeDate.setError("Enter Check Date");
                                }
                                if ("".equals(chequeBankBranch.getText().toString())) {
                                    chequeBankBranch.setError("Enter Bank and Branch");
                                } else {
                                    if ("Secondary".equals(customer_type)) {
                                        //Toast.makeText(getApplicationContext(), "Inside Secondary", Toast.LENGTH_LONG).show();

                                        insertingCollectionData();

                                    } else {
                                        //Toast.makeText(getApplicationContext(), "Inside Primary", Toast.LENGTH_LONG).show();

                                        insertingPrimaryCollectionData();
                                    }
                                }

                            } else {
                                Log.d("CollectionScreen", "Inside Secondary Cash");

                                //int check_count = dbHandler.get_count_check_number(check_num.getText().toString(), customer_type);
                                // Toast.makeText(getApplicationContext(), check_count, Toast.LENGTH_LONG).show();

                                //if (check_count > 1) {
                                //   check_num.setError("Duplicate Check Number");
                                //} else {
                                if ("Secondary".equals(customer_type)) {

                                    Log.d("CollectionScreen", "Inside Secondary Save");

                                    insertingCollectionData();

                                } else {

                                    insertingPrimaryCollectionData();
                                }
                                // }

                            }

                        }
                    }

                    System.out.println("TTT::customer_type = " + customer_type);
                    if ("Secondary".equals(customer_type))
                   { if (selectedImage != null) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        String fileName = "signature_" + collectionNoString + "_" + customer_id + ".png";
                       System.out.println("TTT::PrifileName = " + fileName);
                        dbHandler.updateSecCollectionSignatureimage(collectionNoString, collectionSignaturesFolderPath + File.separator + fileName, fileName);
                    }
                   }else{
                        if (selectedImage != null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            selectedImage.compress(Bitmap.CompressFormat.PNG, 100, baos);
                            String fileName = "signature_" + collectionNoString + "_" + customer_id + ".png";
                            System.out.println("TTT::SecfileName = " + fileName);
                            dbHandler.updatePrimCollectionSignatureimage(collectionNoString, collectionSignaturesFolderPath + File.separator + fileName, fileName);
                        }
                    }


                    Toast.makeText(getApplicationContext(), "Collection saved successfully", Toast.LENGTH_LONG).show();

                    collectionSaveButton.setEnabled(false);
                    collectionSaveButton.setVisibility(View.GONE);
                    signatureName.setEnabled(false);
                    paymentAmount.setEnabled(false);
                    captureLpoImage.setVisibility(View.GONE);

                }


            })

                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            collectionSaveButton.setEnabled(true);

                        }
                    });

            alertbox.show();

        }
    }

    public void showChangesConfirmDialog(View v, int position) {
        if (position == 1) {
            clearOnAccountViews();
            collection_type = "onAccount";
            cashPaymentRb.setChecked(true);
            paymentContainer.setVisibility(View.VISIBLE);
            chequeHeaderContainer.setVisibility(View.GONE);
            chequeValueContainer.setVisibility(View.GONE);
            bankTransferHeaderContainer.setVisibility(View.GONE);
            bankTransferValueContainer.setVisibility(View.GONE);
            amountContainer.setVisibility(View.GONE);
            invoiceNumberContainer.setVisibility(View.GONE);
            if ("Primary".equals(customer_type)) {
                orderNumberContainer.setVisibility(View.VISIBLE);
            }
            paymentAmount.clearFocus();
            currentCheckedRadio = R.id.collectionOnAccountRB;
            paymentAmount.setText("");
            paymentAmount.setEnabled(true);

        } else {
            clearAgainstInvoiceViews();
            collection_type = "Against Invoice";
            cashPaymentRb.setChecked(true);
            amountContainer.setVisibility(View.VISIBLE);
            paymentContainer.setVisibility(View.VISIBLE);
            bankTransferHeaderContainer.setVisibility(View.GONE);
            bankTransferValueContainer.setVisibility(View.GONE);
            //check_section.setVisibility(View.VISIBLE);
            //check_section1.setVisibility(View.VISIBLE);
            invoiceNumberContainer.setVisibility(View.VISIBLE);
            paymentAmount.clearFocus();
            currentCheckedRadio = R.id.collectionAgainstInvoiceRB;
            //payment_amount.setText(String.valueOf(0));
            paymentAmount.setText("");
            paymentAmount.setEnabled(false);
            orderNumberContainer.setVisibility(View.GONE);

        }
    }

    private void updateCollectionStatus(String status) {

        // dbHandler.update_order_header_status(Integer.toString(collection_id), null, null, status, null, 0);
        //dbHandler.update_order_detail_status(Integer.toString(collection_id), status);

        Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);

        //Create the bundle
        Bundle bundle = new Bundle();
        //Add your data to bundle
        bundle.putString("checkin_time", checkin_time);
        bundle.putString("customer_id", customer_id);
        bundle.putString("sales_rep_id", login_id);

        //Add the bundle to the intent
        myintent.putExtras(bundle);

        //Fire that second activity
        startActivity(myintent);
    }

    public void insertingCollectionData() {

        //collection_id = dbHandler.get_collection_id();
        //collection_id = collection_id + 1;
        //collection_number = String.valueOf(collection_id);

        String invoice_number;

        if ("onAccount".equals(collection_type)) {
            invoice_number = "";

        } else {
            invoice_number = invoice_id;

        }


        String visitSequence = sharedPreferences.getString("VisitSequence", "");

        long id1 = dbHandler.insert_collection_details
                (
                        login_id,
                        customer_id,
                        collection_id,
                        collectionNo.getText().toString(),
                        save_collection_date,
                        inv_amount,
                        paid_amt,
                        due_amt,
                        collection_type,
                        pay_amount,
                        null,
                        paymentMode,
                        chequeNo.getText().toString(),
                        save_check_date,
                        chequeBankBranch.getText().toString(),
                        invoice_number,
                        invoice_number,
                        String.valueOf(random_num),
                        transferAcNo.getText().toString(),
                        save_transfer_date,
                        transferBankBranch.getText().toString(),
                        transferTellerNo.getText().toString(),
                        invoice_date,
                        visitSequence,
                        "Collection",
                        signatureName.getText().toString()

                );

        if (id1 <= 0) {
            Toast.makeText(getApplicationContext(), "Collection Entry Failed to Save", Toast.LENGTH_LONG).show();
        } else {

            if ("Against Invoice".equals(collection_type)) {

                double inv_bal_amt = due_amt - pay_amount;
                dbHandler.update_invoice_balance(invoice_id, inv_bal_amt);

                //finish();
            } else {

            }

            dbHandler.update_collection_sequence(collection_id);
            collectionStatus = "Completed";
            againstInvoiceRb.setEnabled(false);
            onAccountRb.setEnabled(false);
            invoiceNumbSpinner.setEnabled(false);
            cashPaymentRb.setEnabled(false);
            chequePaymentRb.setEnabled(false);
            transferPaymentRb.setEnabled(false);
            chequeDate.setEnabled(false);
            chequeNo.setEnabled(false);
            chequeBankBranch.setEnabled(false);
            transferAcNo.setEnabled(false);
            transferDate.setEnabled(false);
            transferBankBranch.setEnabled(false);
            transferTellerNo.setEnabled(false);
            createSignature.setEnabled(false);
            collectionSaveButton.setEnabled(false);
            updateCollectionTime();
            /*Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
            long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            chronometer.stop();
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("sales_rep_id", login_id);
            bundle.putString("customer_id", customer_id);
            bundle.putString("nav_type", "Home");

            myintent.putExtras(bundle);
            startActivity(myintent);*/
            //collectionSaveButton.setVisibility(View.GONE);
        }
    }

    public void updateCollectionTime() {
        //Populate SalesRep Details
        Cursor collectionTimeDetails = dbHandler.getCollectionTime(String.valueOf(collection_id));
        int count1 = collectionTimeDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            Log.d("CollectionTime", "TimeZone " + TimeZone.getDefault().getDisplayName());
            int i = 0;
            while (collectionTimeDetails.moveToNext()) {
                String dateTime = collectionTimeDetails.getString(0);
                Log.d("CollectionTime", "TimeZone " + TimeZone.getDefault().getDisplayName());

                SimpleDateFormat dateFormat = new SimpleDateFormat();
                dateFormat.applyPattern("HH:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                Date myDate = new java.util.Date(Long.parseLong(dateTime) * 1000L);
                collectionTime = dateFormat.format(myDate);
            }
        }


    }

    public void insertingPrimaryCollectionData() {

        //collection_id = dbHandler.get_primary_collection_id();
        //collection_id = collection_id + 1;
        //collection_number = collection_num.getText().toString();
        String invoice_number;

        if ("onAccount".equals(collection_type)) {
            invoice_number = "";

        } else {
            invoice_number = invoice_id;

        }

        if ("Select".equals(orderNumber)) {
            orderNumber = "";

        }


        String visitSequence = sharedPreferences.getString("VisitSequence", "");

        long id1 = dbHandler.insert_primary_collection_details
                (login_id,
                        customer_id,
                        collection_id,
                        collectionNo.getText().toString(),
                        save_collection_date,
                        inv_amount,
                        paid_amt,
                        due_amt,
                        collection_type,
                        pay_amount,
                        null,
                        paymentMode,
                        chequeNo.getText().toString(),
                        save_check_date,
                        chequeBankBranch.getText().toString(),
                        invoice_number,
                        invoice_number,
                        String.valueOf(random_num),
                        transferAcNo.getText().toString(),
                        save_transfer_date,
                        transferBankBranch.getText().toString(),
                        transferTellerNo.getText().toString(),
                        orderNumber,
                        visitSequence,
                        signatureName.getText().toString()

                );

        if (id1 <= 0) {
            Toast.makeText(getApplicationContext(), "Collection Entry Failed to Save", Toast.LENGTH_LONG).show();
        } else {
            dbHandler.update_primary_collection_sequence(collection_id);
            collectionStatus = "Completed";
            againstInvoiceRb.setEnabled(false);
            onAccountRb.setEnabled(false);
            invoiceNumbSpinner.setEnabled(false);
            cashPaymentRb.setEnabled(false);
            chequePaymentRb.setEnabled(false);
            transferPaymentRb.setEnabled(false);
            chequeDate.setEnabled(false);
            chequeNo.setEnabled(false);
            chequeBankBranch.setEnabled(false);
            transferAcNo.setEnabled(false);
            transferDate.setEnabled(false);
            transferBankBranch.setEnabled(false);
            transferTellerNo.setEnabled(false);
            createSignature.setEnabled(false);
            collectionSaveButton.setEnabled(false);
            View current = getCurrentFocus();
            current.clearFocus();
            paymentAmount.setEnabled(false);
            updateCollectionTime();
            /*Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
            long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            chronometer.stop();
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("sales_rep_id", login_id);
            bundle.putString("customer_id", customer_id);
            bundle.putString("nav_type", "Home");

            myintent.putExtras(bundle);
            startActivity(myintent);*/
            //collectionSaveButton.setVisibility(View.GONE);
        }
    }

    public void loadInvoiceNumbers() {
        if ("Primary".equals(customer_type)) {
            lables = dbHandler.getPrimaryInvoicenumbers(customer_id);
            lables.add(0, "Select");

        } else {
            //lables = dbHandler.getInvoicenumbers(customer_id);
            ArrayList<Ledger> ledgerArrayList = ledgerTable.getLedgerArrayList(customer_id, customer_type);
            double outstandingTotal = 0;
            int count = 0;
            List<String> invoiceNumbers = new ArrayList<String>();
            ArrayList<String> invoiceNumberList = new ArrayList<>();
            for (Ledger l : ledgerArrayList) {
                if(!invoiceNumberList.contains(l.getInvoiceNumber()))
                    invoiceNumberList.add(l.getInvoiceNumber());
            }

            for (Ledger l : ledgerArrayList) {
                if(l.getTransactionType() != null) {
                    if (l.getTransactionType().equals("Return")) {
                        if (l.getOutstandingAmount() != null)
                            outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                    }
                }
            }
            for(String invoiceNumber:invoiceNumberList){
                //String invoiceLastBalance = "";
                double invoiceLastBalance = 0;
                String transactionType = "";
                for (Ledger l : ledgerArrayList) {
                    if(invoiceNumber.equals(l.getInvoiceNumber())) {
                        if(l.getOutstandingAmount() != null)
                            invoiceLastBalance += Double.parseDouble(l.getOutstandingAmount());
                        transactionType = l.getTransactionType();
                        System.out.println("TT**invoiceNumber = " + invoiceNumber);
                    }
                }
                Log.d("InvoiceVal",""+invoiceLastBalance);
                double payment_amount = dbHandler.getCollectionDueAmout(invoiceNumber);
                double invoice_amount = dbHandler.getInvoiceAmout(invoiceNumber);
                System.out.println("invoice_amount = " + invoice_amount);
                System.out.println("payment_amount = " + payment_amount);

                if(invoiceLastBalance!=0 && invoice_amount !=payment_amount && !invoiceNumber.equals("") && invoiceNumber!=null) {
                    invoiceNumbers.add(invoiceNumber);
                }
            }
            lables = invoiceNumbers;
        }

        invoiceNumbSpinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                lables));

       /* // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getApplicationContext(),
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        Product.setAdapter(dataAdapter);*/
    }

    public void loadOrderSpinnerProduct() {

        if ("Primary".equals(customer_type)) {
            lables = dbHandler.getPrimaryOrdernumbers(customer_id);

        }

        lables.add(0, "Select");
        orderNumbSpinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                lables));

        orderNumbSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                orderNumber = adapterView.getItemAtPosition(pos).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

       /* // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getApplicationContext(),
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        Product.setAdapter(dataAdapter);*/
    }

    private void populateHeaderDetails() {
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            checkin_time = bundle.getString("checkin_time");
            customer_type = bundle.getString("customer_type");

        }

        checkInTimeText.setText(checkin_time);

        paymentAmount.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});

        //Populate Header Details

        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                customerNameString = customerDetails.getString(2);
                customerEmailAddress = customerDetails.getString(33);


                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                customerAddress1 = customerDetails.getString(6);
                customerAddress2 = customerDetails.getString(7);
                customerAddress3 = customerDetails.getString(8);
                customerPhoneNumber = customerDetails.getString(11);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }
            }
        }

        //Populate SalesRep Details
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                sr_name_details = salesRepDetails.getString(2);
            }
        }

        getCompanyInfo();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_PHOTO1) {
            if (resultCode == RESULT_OK) {

                image_id = lpoImagesTable.get_lpo_gallery_image_id();
                image_id = image_id + 1;
                String imagename = "customer_image" + image_id;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                String fileName = "lpo_" + collectionNo.getText().toString() + "_" + image_id + ".png";
                fileUtils.storeImage(photo, fileName, orderGalleryImageFolderPath);

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                LPOImages lpoImages = new LPOImages();
                lpoImages.setOrderCode(collectionNo.getText().toString());
                lpoImages.setLpoImageName(fileName);
                lpoImages.setLpoImagePath(orderGalleryImageFolderPath + File.separator + fileName);
                lpoImages.setVisitSeqNumber(visitSequence);
                lpoImagesTable.create(lpoImages);

            }
        } else

            printBluetooth.onActivityResult(requestCode, resultCode, data, printMsg);
    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);


            }
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        invoice_id = parent.getItemAtPosition(position).toString();

        if ("Select".equals(invoice_id)) {
            paymentAmount.setEnabled(false);

        } else {
            paymentAmount.setEnabled(true);
            if ("Primary".equals(customer_type)) {
                invoiceSectionDetails = dbHandler.getPrimaryinvoiceSectionDetails(invoice_id);
            } else {
                invoiceSectionDetails = dbHandler.getinvoiceSectionDetails(invoice_id);
            }

            int count1 = invoiceSectionDetails.getCount();
            if (count1 == 0) {

            } else {
                int i = 0;
                while (invoiceSectionDetails.moveToNext()) {

                    inv_amount = Double.parseDouble(invoiceSectionDetails.getString(0));
                    formatInvoiceAmount = decimalFormatter.format(inv_amount);
                    invoiceAmount.setText(formatInvoiceAmount);

                    invoice_date = invoiceSectionDetails.getString(3);
                    double outstandingTotal = 0;
                    double paidTotal = 0;
                    if (!"Primary".equals(customer_type)) {
                        ArrayList<Ledger> ledgerArrayList = ledgerTable.getSecondaryInvoiceLedgerArrayList(customer_id,invoice_id);

                        ArrayList<String> invoiceNumberList = new ArrayList<>();
                        for (Ledger l : ledgerArrayList) {
                            if (!invoiceNumberList.contains(l.getInvoiceNumber()) )
                            {
                                   /*for (String invoiceNumber : invoiceNumberList) {
                                       if (!invoiceNumber.equals("") && invoiceNumber != null)
                                         {  */
                                invoiceNumberList.add(l.getInvoiceNumber());
                                System.out.println("T***invoiceNumber = " + l.getInvoiceNumber());
                                System.out.println("T***invoiceNumberList = " + invoiceNumberList);
//                               }
//                                   }
                            }
                        }

                        for (Ledger l : ledgerArrayList) {
                            if(l.getTransactionType() != null) {
                                if (l.getTransactionType().equals("Return")) {
                                    if (l.getOutstandingAmount() != null)
                                        outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                                }
                            }
                        }
                        for (String invoiceNumber : invoiceNumberList) {
                            String invoiceLastBalance = "";
                            double payment_amount = 0;
                            double invoice_amount = 0;
                            for (Ledger l : ledgerArrayList) {
                                if(l.getTransactionType() != null) {
                                    if (invoiceNumber.equals(l.getInvoiceNumber()) && !l.getTransactionType().equals("Return") && !invoiceNumber.equals("") && invoiceNumber != null) {
                                        if (l.getOutstandingAmount() != null)
//                                            invoiceLastBalance = l.getOutstandingAmount();
                                        {
                                            System.out.println("T***@invoiceNumber = " + invoiceNumber);
                                            payment_amount = dbHandler.getCollectionDueAmout(invoiceNumber);
                                            invoice_amount = dbHandler.getInvoiceAmout(invoiceNumber);
                                            paidTotal = payment_amount;
                                            System.out.println("TT**invoice_amount1 = " + invoice_amount);
                                            System.out.println("TT**payment_amount1 = " + payment_amount);
                                            outstandingTotal = (invoice_amount - payment_amount) ;
                                            System.out.println("TT**outstandingTotal = " + outstandingTotal);
                                        }
                                    }
                                }
                            }


                           /* outstandingTotal = (invoice_amount - payment_amount) ;
                            System.out.println("TT**outstandingTotal = " + outstandingTotal);*/
                            // Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount()+" "+ outstandingTotal);
                        }
                    }
                    if ("Primary".equals(customer_type)) {
                        due_amt = Double.parseDouble(invoiceSectionDetails.getString(2));
                        paid_amt = Double.parseDouble(invoiceSectionDetails.getString(1));
                    }
                    else{


                        due_amt = outstandingTotal;
                        paid_amt = paidTotal;
                    }
                    formatDueAmount = decimalFormatter.format(due_amt);
                    dueAmount.setText(formatDueAmount);

                    formatPaidAmount = decimalFormatter.format(paid_amt);
                    paidAmount.setText(formatPaidAmount);
                }
            }

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void clearAgainstInvoiceViews() {
        invoiceNumbSpinner.setSelection(0);
        invoiceAmount.setText("");
        paidAmount.setText("");
        dueAmount.setText("");
        chequeNo.setText("");
        chequeBankBranch.setText("");
        transferTellerNo.setText("");
        transferDate.setText("");
        transferAcNo.setText("");
        transferBankBranch.setText("");
    }

    public void clearOnAccountViews() {
        chequeNo.setText("");
        chequeDate.setText("");
        chequeBankBranch.setText("");
        transferDate.setText("");
        transferAcNo.setText("");
        transferBankBranch.setText("");
        orderNumbSpinner.setSelection(0);

    }

    private void showpopupsignature() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);
        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(view -> mSignaturePad.clear());

        mSaveButton.setOnClickListener(view -> {
            Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
            selectedImage = signatureBitmap;
            viewSignature.setImageBitmap(signatureBitmap);
            dialog.dismiss();

            String fileName = "signature_" + collectionNoString + "_" + customer_id + ".png";
            fileUtils.storeImage(selectedImage, fileName, collectionSignaturesFolderPath);

            paymentAmount.clearFocus();

        });




    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    private void initializeViews() {

        captureLpoImage = findViewById(R.id.captureLpoImageView);
        showLpoImagesGallery = findViewById(R.id.showLpoImagesGallery);

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);


        checkInTimeText = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);


        collectionNo = findViewById(R.id.collectionNumberTv);
        currency = findViewById(R.id.collectionCurrencyTv);
        collectionDate = findViewById(R.id.collectionDateTv);
        onAccountRb = findViewById(R.id.collectionOnAccountRB);
        againstInvoiceRb = findViewById(R.id.collectionAgainstInvoiceRB);

        invoiceNumberContainer = findViewById(R.id.collectionInvoiceNumbSection);
        invoiceNumbSpinner = findViewById(R.id.collectionInvoiceNumbSpinner);

        orderNumberContainer = findViewById(R.id.collectionOrderNumbSection);
        orderNumbSpinner = findViewById(R.id.collectionOrderNumbSpinner);

        amountContainer = findViewById(R.id.collectionAmountSection);
        invoiceAmount = findViewById(R.id.collectionInvoiceAmntTv);
        paidAmount = findViewById(R.id.collectionPaidAmntTv);
        dueAmount = findViewById(R.id.collectionDueAmntTv);

        paymentContainer = findViewById(R.id.collectionPaymentSection);
        paymentAmount = findViewById(R.id.collectionPaymentAmntEt);
        chequePaymentRb = findViewById(R.id.collectionChequeRB);
        cashPaymentRb = findViewById(R.id.collectionCashRB);
        transferPaymentRb = findViewById(R.id.collectionBankTransferRB);

        signatureName = findViewById(R.id.signCustomerNameEditText);
        createSignature = findViewById(R.id.CreateSignImageView);
        viewSignature = findViewById(R.id.ViewSignatureImageView);

        chequeNo = findViewById(R.id.collectionCheckNoEt);
        chequeDate = findViewById(R.id.collectionCheckDateTv);
        chequeBankBranch = findViewById(R.id.collectionBranchEt);


        chequeHeaderContainer = findViewById(R.id.collectionChequeTitleSection);
        chequeValueContainer = findViewById(R.id.collectionChequeDataSection);

        bankTransferHeaderContainer = findViewById(R.id.collectionBankTransferHeaderSection);
        bankTransferValueContainer = findViewById(R.id.collectionBankTransferValueSection);

        transferAcNo = findViewById(R.id.collectionBankAcNoEt);
        transferDate = findViewById(R.id.collectionTransferDateTv);
        transferBankBranch = findViewById(R.id.collectionBankBranchEt);
        transferTellerNo = findViewById(R.id.collectionTellerNoEt);


        currentCheckedRadio = R.id.collectionAgainstInvoiceRB;
        currentCheckedRadio1 = R.id.collectionCashRB;


        collectionSaveButton = findViewById(R.id.collectionSaveButton);
        printButton = findViewById(R.id.collectionPrintButton);
        emailButton = findViewById(R.id.collectionEmailButton);
        smsButton = findViewById(R.id.collectionSmsButton);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        decimalFormatter = new DecimalFormat("#,###.00");

        //Get Values
        globals = ((Globals) getApplicationContext());
        login_id = globals.getLogin_id();
        sr_name1 = globals.getSr_name();

        tab_prefix = Constants.TAB_PREFIX;


        print = new Print(this);
        printBluetooth = new PrintBluetooth(this);

        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());
        ledgerTable = new LedgerTable(getApplicationContext());
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        lpoImagesTable = new LpoImagesTable(getApplicationContext());

        gs = generalSettingsTable.getSettingByKey("enablePrint");
        if (gs != null && gs.getValue().equals("Yes")) {
            printButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableSms");
        if (gs != null && gs.getValue().equals("Yes")) {
            smsButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableEmail");
        if (gs != null && gs.getValue().equals("Yes")) {
            emailButton.setVisibility(View.VISIBLE);
        }

        signatureName.setFilters(new InputFilter[]{filter});


    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    class Radio_check implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (onAccountRb.isChecked()) {
                if (currentCheckedRadio == onAccountRb.getId())
                    return;
                showChangesConfirmDialog(buttonView, 1);

            } else if (againstInvoiceRb.isChecked()) {
                if (currentCheckedRadio == againstInvoiceRb.getId())
                    return;
                showChangesConfirmDialog(buttonView, 0);


            }

        }
    }

    class Radio_check1 implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            if (cashPaymentRb.isChecked()) {
                paymentMode = "Cash";
                //inv_section.setVisibility(View.VISIBLE);
                chequeHeaderContainer.setVisibility(View.GONE);
                chequeValueContainer.setVisibility(View.GONE);
                bankTransferHeaderContainer.setVisibility(View.GONE);
                bankTransferValueContainer.setVisibility(View.GONE);
                chequeNo.setText("");
                chequeBankBranch.setText("");
                chequeDate.setText("");
                //Toast.makeText(getApplicationContext(), radiotext1, Toast.LENGTH_SHORT).show();

            } else if (chequePaymentRb.isChecked()) {
                paymentMode = "Cheque";
                // inv_section.setVisibility(View.VISIBLE);
                paymentContainer.setVisibility(View.VISIBLE);
                chequeHeaderContainer.setVisibility(View.VISIBLE);
                chequeValueContainer.setVisibility(View.VISIBLE);
                bankTransferHeaderContainer.setVisibility(View.GONE);
                bankTransferValueContainer.setVisibility(View.GONE);

            } else if (transferPaymentRb.isChecked()) {
                paymentMode = "Transfer";
                // inv_section.setVisibility(View.VISIBLE);
                paymentContainer.setVisibility(View.VISIBLE);
                chequeHeaderContainer.setVisibility(View.GONE);
                chequeValueContainer.setVisibility(View.GONE);
                bankTransferHeaderContainer.setVisibility(View.VISIBLE);
                bankTransferValueContainer.setVisibility(View.VISIBLE);

            }

        }
    }
}
