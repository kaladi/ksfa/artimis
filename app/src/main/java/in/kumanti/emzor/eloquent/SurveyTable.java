package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.Survey;
import in.kumanti.emzor.model.SurveyHeader;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY_RESPONSE;

public class SurveyTable extends SQLiteOpenHelper {

    private static final String COLUMN_SURVEY_ID = "id";
    private static final String COLUMN_COMPANY_ID = "company_id";
    private static final String COLUMN_TAB_ID = "tab_id";
    private static final String COLUMN_SALES_REP_ID = "sales_rep_id";
    private static final String COLUMN_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_SURVEY_NAME = "survey_name";
    private static final String COLUMN_SURVEY_CODE = "survey_code";
    private static final String COLUMN_SURVEY_TYPE = "survey_type";
    private static final String COLUMN_SURVEY_DESCRIPTION = "survey_description";
    private static final String COLUMN_SURVEY_DATE = "survey_date";
    private static final String COLUMN_SURVEY_CREATED_AT = "created_at";
    private static final String COLUMN_SURVEY_UPDATED_AT = "updated_at";
    private static final String COLUMN_SURVEY_IS_SYNC = "is_sync";

    public Context context;
    private SQLiteDatabase sqlWrite;
    private SurveyQuestionsTable surveyQuestionsTable;

    public SurveyTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY);

        String surveyTable = "CREATE TABLE IF NOT EXISTS " + TABLE_SURVEY + "(" +
                COLUMN_SURVEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_ID + " TEXT, " +
                COLUMN_COMPANY_ID + " TEXT, " +
                COLUMN_TAB_ID + " TEXT, " +
                COLUMN_SALES_REP_ID + " TEXT, " +
                COLUMN_SURVEY_NAME + " TEXT, " +
                COLUMN_SURVEY_DATE + " TEXT, " +
                COLUMN_SURVEY_CODE + " TEXT, " +
                COLUMN_SURVEY_TYPE + " TEXT, " +
                COLUMN_SURVEY_DESCRIPTION + " TEXT, " +
                COLUMN_SURVEY_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SURVEY_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SURVEY_IS_SYNC + " TEXT " +
                ");";

        sqlWrite.execSQL(surveyTable);
        surveyQuestionsTable = new SurveyQuestionsTable(context);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Survey getSurveyById(String surveyCode) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_SURVEY + " WHERE " + COLUMN_SURVEY_CODE + "= ?";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{surveyCode});
        Survey survey = null;
        while (cursor.moveToNext()) {
            survey = new Survey();
            int id = cursor.getInt(0);
            survey.setId(id);
            survey.setCompany_id(cursor.getString(2));
            survey.setSurvey_code(cursor.getString(7));
            survey.setSurvey_name(cursor.getString(5));
            survey.setSurvey_description(cursor.getString(9));
            survey.setSurvey_date(cursor.getString(6));
            survey.setSurvey_questions(surveyQuestionsTable.getQuestionsBySurveyId(String.valueOf(cursor.getString(7))));
        }
        cursor.close();
        sqlWrite.close();
        return survey;
    }

    public ArrayList<Survey> getSurveyList(String customerId) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_SURVEY + " WHERE survey_code NOT IN ( select survey_id FROM " + TABLE_SURVEY_RESPONSE + " where customer_id = ? AND status = 'completed')";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerId});

        ArrayList<Survey> surveyArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Survey survey = new Survey();
            int id = cursor.getInt(0);
            survey.setId(id);
            survey.setCompany_id(cursor.getString(2));
            survey.setSurvey_name(cursor.getString(5));
            survey.setSurvey_code(cursor.getString(7));
            survey.setSurvey_date(cursor.getString(6));
            //survey.setSurvey_questions(surveyQuestionsTable.getQuestionsBySurveyId(String.valueOf(id)));
            surveyArrayList.add(survey);
        }
        cursor.close();
        sqlWrite.close();
        return surveyArrayList;
    }

    public long create(SurveyHeader survey) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SURVEY_NAME, survey.getSurvey_name());
        values.put(COLUMN_SURVEY_CODE, survey.getSurvey_code());
        values.put(COLUMN_SURVEY_TYPE, survey.getSurvey_type());
        values.put(COLUMN_SURVEY_DESCRIPTION, survey.getSurvey_description());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SURVEY, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Survey Headers List details from the Master via Web Service
     */
    public void insertingSurveysMaster(ArrayList<SurveyHeader> surveyArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (surveyArrayList != null) {
                for (int i = 0; i < surveyArrayList.size(); i++) {
                    values.put(COLUMN_SURVEY_NAME, surveyArrayList.get(i).getSurvey_name());
                    values.put(COLUMN_SURVEY_CODE, surveyArrayList.get(i).getSurvey_code());
                    values.put(COLUMN_SURVEY_TYPE, surveyArrayList.get(i).getSurvey_type());
                    values.put(COLUMN_SURVEY_DESCRIPTION, surveyArrayList.get(i).getSurvey_description());
                    db.insert(TABLE_SURVEY, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }

    }

    public void deleteSurveyHeader() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SURVEY, null, null);
        db.close();
    }


}
