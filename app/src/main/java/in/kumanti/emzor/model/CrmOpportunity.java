package in.kumanti.emzor.model;


import java.io.Serializable;

public class CrmOpportunity implements Serializable {

    public int opportunityId;
    public int coldCallId;
    public String customerCode;
    public String opportunityNo;
    public String opportunityDate;
    public String opportunityStatus;
    public String randomNumber;
    public boolean isSync;
    public String latestSyncDate;
    public String createdAt;
    public String updatedAt;

    public int getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(int opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public int getColdCallId() {
        return coldCallId;
    }

    public void setColdCallId(int coldCallId) {
        this.coldCallId = coldCallId;
    }

    public String getOpportunityNo() {
        return opportunityNo;
    }

    public void setOpportunityNo(String opportunityNo) {
        this.opportunityNo = opportunityNo;
    }

    public String getOpportunityDate() {
        return opportunityDate;
    }

    public void setOpportunityDate(String opportunityDate) {
        this.opportunityDate = opportunityDate;
    }

    public String getOpportunityStatus() {
        return opportunityStatus;
    }

    public void setOpportunityStatus(String opportunityStatus) {
        this.opportunityStatus = opportunityStatus;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getLatestSyncDate() {
        return latestSyncDate;
    }

    public void setLatestSyncDate(String latestSyncDate) {
        this.latestSyncDate = latestSyncDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(String randomNumber) {
        this.randomNumber = randomNumber;
    }

    @Override
    public String toString() {
        return this.opportunityNo;
    }


}
