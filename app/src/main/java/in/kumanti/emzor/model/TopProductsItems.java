package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class TopProductsItems {
    private String product;
    private String quantity;
    private String price;
    private String value;
    private String transaction_count;
    private String product_id;

    public TopProductsItems(String product1, String quantity1, String price1, String value1, String transaction_count1, String product_id1) {
        product = product1;
        quantity = quantity1;
        price = price1;
        value = value1;
        transaction_count = transaction_count1;
        product_id = product_id1;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product1) {
        product = product1;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity1) {
        quantity = quantity1;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price1) {
        price = price1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value1) {
        value = value1;
    }

    public String getTransaction_count() {
        return transaction_count;
    }

    public void setTransaction_count(String transaction_count1) {
        transaction_count = transaction_count1;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id1) {
        product_id = product_id1;
    }

}

