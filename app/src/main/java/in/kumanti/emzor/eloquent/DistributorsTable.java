package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.Distributors;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_DISTRIBUTORS;

public class DistributorsTable extends SQLiteOpenHelper {

    private static final String COLUMN_DISTRIBUTOR_ID = "distributorId";
    private static final String COLUMN_DISTRIBUTOR_NAME = "distributorName";
    private static final String COLUMN_DISTRIBUTOR_CODE = "distributorCode";
    private static final String COLUMN_DISTRIBUTOR_CREATED_AT = "createdAt";
    private static final String COLUMN_DISTRIBUTOR_UPDATED_AT = "updatedAt";

    public Context context;
    String selectQuery;
    Cursor cursor;

    public DistributorsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_DISTRIBUTORS);

        String distributorsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_DISTRIBUTORS + "(" +
                COLUMN_DISTRIBUTOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DISTRIBUTOR_CODE + " TEXT, " +
                COLUMN_DISTRIBUTOR_NAME + " TEXT, " +
                COLUMN_DISTRIBUTOR_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_DISTRIBUTOR_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(distributorsTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(Distributors distributors) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_DISTRIBUTOR_CODE, distributors.distributorCode);
        values.put(COLUMN_DISTRIBUTOR_NAME, distributors.distributorName);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_DISTRIBUTORS, null, values);
        db.close();
        return id;
    }

    /*
     * Fetching the Distributors Name List details from the Master via Web Service
     */
    public void insertingDistributorsMaster(ArrayList<Distributors> distributorsArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (distributorsArrayList != null) {
                for (int i = 0; i < distributorsArrayList.size(); i++) {
                    values.put(COLUMN_DISTRIBUTOR_CODE, distributorsArrayList.get(i).getDistributorCode());
                    values.put(COLUMN_DISTRIBUTOR_NAME, distributorsArrayList.get(i).getDistributorName());
                    db.insert(TABLE_DISTRIBUTORS, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }
    }

    public ArrayList<Distributors> getDistributors() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DISTRIBUTORS + " WHERE distributorName NOT LIKE '%_MDS%' ORDER BY distributorName ASC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("ViewStock--", "Get Products  ");
        ArrayList<Distributors> distributorsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Distributors distributors = new Distributors();
            Log.d("View stock MYDATA--", "Data  id" + cursor.getInt(0));
            distributors.setDistributorId(cursor.getInt(0));
            distributors.setDistributorCode(cursor.getString(1));
            distributors.setDistributorName(cursor.getString(2));

            distributorsArrayList.add(distributors);
        }
        cursor.close();
        sqlWrite.close();
        return distributorsArrayList;
    }

    public void deleteDistributors() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DISTRIBUTORS, null, null);
        db.close();
    }

}
