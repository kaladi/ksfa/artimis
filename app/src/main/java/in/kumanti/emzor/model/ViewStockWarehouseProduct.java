package in.kumanti.emzor.model;

import java.util.ArrayList;

public class ViewStockWarehouseProduct {

    private String warehouseCode;
    private String warehouseName;
    private String productCode;
    private String productName;
    private String productUom;
    private String productQuantity;
    private String productPrice;
    private String productTotalPrice;
    private String primaryOrderCount = "";
    private String batchControlled;
    ArrayList<SrInvoiceBatch> batchDetailArrayList = new ArrayList<SrInvoiceBatch>();


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }


    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }


    public String getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(String productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }


    public String getPrimaryOrderCount() {
        return primaryOrderCount;
    }

    public void setPrimaryOrderCount(String primaryOrderCount) {
        this.primaryOrderCount = primaryOrderCount;
    }


    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public ArrayList<SrInvoiceBatch> getBatchDetailArrayList() {
        return batchDetailArrayList;
    }

    public void setBatchDetailArrayList(ArrayList<SrInvoiceBatch> batchDetailArrayList) {
        this.batchDetailArrayList = batchDetailArrayList;
    }

    public int getBatchDetailArrayListTotalCount() {
        int count = 0;
        for (SrInvoiceBatch srb : batchDetailArrayList) {
            count += Integer.parseInt(srb.getBatchQuantity());
        }
        return count;
    }

    public String getBatchControlled() {
        return batchControlled;
    }

    public void setBatchControlled(String batchControlled) {
        this.batchControlled = batchControlled;
    }

}
