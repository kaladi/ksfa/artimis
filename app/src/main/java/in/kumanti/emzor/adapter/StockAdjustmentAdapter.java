package in.kumanti.emzor.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CustomerReturnProducts;
import in.kumanti.emzor.model.StockIssueProducts;
import in.kumanti.emzor.utils.DecimalDigitsInputFilter;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_ZERO;

public class StockAdjustmentAdapter extends RecyclerView.Adapter<StockAdjustmentAdapter.ViewHolder> {

    public ArrayList<StockIssueProducts> mDataset;
    public Context context;
    public String quantity;
    DecimalFormat decimalFormat;
    ArrayList<CustomerReturnProducts> customerReturnProductsArrayList;
    String formatPrice, totalReturnValue;
    private RecyclerViewItemListener mListener, batchListener,mListener1;
    private boolean isStockReturnSaved;
    Double priceDouble;



    public StockAdjustmentAdapter(ArrayList<StockIssueProducts> myDataset, RecyclerViewItemListener listener, RecyclerViewItemListener blistener) {
        mDataset = myDataset;
        mListener = listener;
        batchListener = blistener;
        // isStockReturnSaved = stockReturnSaved;
    }

    public void add(StockIssueProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockIssueProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockAdjustmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stock_adjust, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener, new CustomQtyChangeListener(), batchListener, new CustomPriceChangeListener(),new  CustomRemarkChangeListener());
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.uom.setText(mDataset.get(position).getProductUom());
        viewHolder.quantity.setText(mDataset.get(position).getStockQuantity());
        viewHolder.etRemarks.setText(mDataset.get(position).getRemarks());


        System.out.println("TTT::position = " + position);
        System.out.println("TTT::mDataset.get(position).getUnitPrice() = " + mDataset.get(position).getUnitPrice());
        if(mDataset.get(position).getUnitPrice() == null )
        {
            viewHolder.price.setText("");
        }else
        {
            viewHolder.price.setText(String.valueOf(mDataset.get(position).getUnitPrice()));
        }



//        viewHolder.valueTV.setText(mDataset.get(position).getIssueValue());
        viewHolder.customQtyChangeListener.updatePosition(position, viewHolder);

        viewHolder.CustomPriceChangeListener.updatePosition(position, viewHolder);

//        viewHolder.CustomRemarkChangeListener.updatePosition(position, viewHolder);


//        viewHolder.returnQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER,MAX_NUMBER)});

        if(mDataset.get(position).getIssueQuantity() == null )
        {
            viewHolder.returnQuantity.setText("");
        }else
        {
            viewHolder.returnQuantity.setText(String.valueOf(mDataset.get(position).getIssueQuantity()));
        }


        if (!TextUtils.isEmpty(mDataset.get(position).getErrorStockIssueQty())) {
            viewHolder.returnQuantity.setError(mDataset.get(position).getErrorStockIssueQty());
        } else {
            viewHolder.returnQuantity.setError(null);
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getErrorProduct())) {
            viewHolder.productName.setError(mDataset.get(position).getErrorProduct());
        } else {
            viewHolder.productName.setError(null);
        }

        viewHolder.price.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setUnitPrice(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });

        viewHolder.returnQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setIssueQuantity(caption.getText().toString());
                    caption.setError(null);
                }

            }
        });
        viewHolder.etRemarks.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setRemarks(caption.getText().toString());
                    caption.setError(null);
                }

            }
        });
        //If Not equals null means get the enter the batch details.....
        viewHolder.productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size()) {
                    if (mDataset.get(position).getIssueQuantity() == null || TextUtils.isEmpty(mDataset.get(position).getIssueQuantity())) {
                        Toast.makeText(context, "Please enter the Adjust quantity", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (mDataset.get(position).getUnitPrice() == null || TextUtils.isEmpty(mDataset.get(position).getUnitPrice())) {
                        Toast.makeText(context, "Please enter the Cost", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Log.d("DataCheck", "CurrentPosition onCLick " + position);
                    if (mDataset.get(position).getIsBatchControlled() != null && mDataset.get(position).getIsBatchControlled().equals("Yes"))
                        viewHolder.batchListener.onClick(v, position);
                }
            }
        });


        viewHolder.removeRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Product Info?");
                        //alertDialog.setMessage("your message ");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                notifyDataSetChanged();
                                viewHolder.mListener.onClick(v, position);
                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }


            }
        });

//        viewHolder.etRemarks.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//                String value = cs.toString();
////                            mDataset.get(this.position).setRemarks(value);
//
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable arg0) {
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextCallBackListener {

        public TextView productName, uom, quantity;
        public ImageButton removeRow;
        public EditText returnQuantity,price,etRemarks;
        DecimalFormat decimalFormat;
        CustomQtyChangeListener customQtyChangeListener;
        LinearLayout productLine;
        private RecyclerViewItemListener mListener, batchListener;
        CustomPriceChangeListener CustomPriceChangeListener;
        CustomRemarkChangeListener CustomRemarkChangeListener;



        public ViewHolder(View v, RecyclerViewItemListener listener, CustomQtyChangeListener customQtyChangeListener, RecyclerViewItemListener blistener, CustomPriceChangeListener CustomPriceChangeListener,CustomRemarkChangeListener CustomRemarkChangeListener) {
            super(v);
            mListener = listener;
            batchListener = blistener;
            productLine = v.findViewById(R.id.productLine);
            productName = v.findViewById(R.id.product);
            uom = v.findViewById(R.id.uom);
            quantity = v.findViewById(R.id.quantity);
            price = (EditText)v.findViewById(R.id.price);

//            valueTV = v.findViewById(R.id.value);
            returnQuantity = v.findViewById(R.id.returnQuantity);
            etRemarks = v.findViewById(R.id.etRemarks);


            removeRow = v.findViewById(R.id.stockIssueRemoveButton);
            price.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER), new DecimalDigitsInputFilter()});

            this.CustomPriceChangeListener = CustomPriceChangeListener;
            price.addTextChangedListener(CustomPriceChangeListener);
            this.customQtyChangeListener = customQtyChangeListener;

            //returnQuantity.setFilters(new InputFilter[]{ new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
            decimalFormat = new DecimalFormat("#,###.00");
//            returnQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
            returnQuantity.addTextChangedListener(customQtyChangeListener);
            this.CustomRemarkChangeListener = CustomRemarkChangeListener;
            etRemarks.addTextChangedListener(CustomRemarkChangeListener);

        }



        @Override
        public void updateText(String val, String val2) {
            //productQuantity.setText(val);
            if (!TextUtils.isEmpty(val2)) {
                Log.d("DataCheck", "Vlaue " + val2);
                formatPrice = decimalFormat.format(Double.parseDouble(val2));
                String value = formatPrice;
                if (value != null && !value.equals("")) {
                    if (value.startsWith(".")) {
                        value = "0" + value;
                    }
                }
//                valueTV.setText(value);
                if (!TextUtils.isEmpty(returnQuantity.getText().toString())) {
                    returnQuantity.setError(null);
                }
            } else {
//                valueTV.setText(val2);
            }

        }
    }

    /**
     * Custom class which implements Text Watcher
     */
    private class CustomPriceChangeListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        ViewHolder v;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
            v = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String value = charSequence.toString();
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + charSequence.toString();
                }
            }
            // Change the value of array according to the position
          /*  if (!mDataset.get(this.position).getUnitPrice().equals("")) {
                mDataset.get(this.position).setUnitPrice(value);
            }else
                mDataset.get(this.position).setUnitPrice("");*/

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

    }


    /**
     * Custom class which implements Text Watcher
     */
    private class CustomQtyChangeListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        ViewHolder v;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
            v = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String value = charSequence.toString();
            int intVal = 0;

            if(!charSequence.equals("") ) {
                if (value.startsWith(".")) {
                    value = "0" + charSequence.toString();
                }
                else if (value.startsWith("-")){

                      try
                    {
                        intVal = Integer.parseInt(value);
                        System.out.println("valu "+ intVal);
                    }catch(NumberFormatException e)
                    {

                    }
                    value = String.valueOf(intVal);

                }

            }

            // Change the value of array according to the position
           mDataset.get(this.position).setIssueQuantity(value);

            // Doing the calculation
            if (!mDataset.get(this.position).getIssueQuantity().equals("")) {
                if (Double.parseDouble(mDataset.get(this.position).getIssueQuantity()) != 0d)
                    mDataset.get(this.position).setErrorStockIssueQty(null);
                mDataset.get(this.position).setErrorStockIssueQty(null);


                Double total = Double.parseDouble(mDataset.get(this.position).getIssueQuantity()) * Double.parseDouble(mDataset.get(this.position).getUnitPrice());

//                formatPrice = decimalFormat.format(total);
                mDataset.get(this.position).setIssueValue(String.valueOf(total));
                // triggering the callback function to update the total
                textCallBackListener.updateText(value, String.valueOf(total));
            }
            else {
                mDataset.get(this.position).setIssueValue("");
                textCallBackListener.updateText(value, "");
            }
            mListener.onClick(v.returnQuantity, this.position);
        }

        @Override
        public void afterTextChanged(Editable editable) {


        }

    }

    /**
     * Custom class which implements Text Watcher
     */
    private class CustomRemarkChangeListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        ViewHolder v;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
            v = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String value = charSequence.toString();
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + charSequence.toString();
                }
            }
//            mDataset.get(this.position).setRemarks(charSequence.toString());


        }

        @Override
        public void afterTextChanged(Editable editable) {


        }

    }





}