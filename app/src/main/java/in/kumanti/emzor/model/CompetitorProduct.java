package in.kumanti.emzor.model;

public class CompetitorProduct {
    private String productCode;
    private String productName;
    private String productMasterCode;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductMasterCode() {
        return productMasterCode;
    }

    public void setProductMasterCode(String productMasterCode) {
        this.productMasterCode = productMasterCode;
    }

    @Override
    public String toString() {
        return productName;
    }
}
