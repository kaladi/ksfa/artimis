package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class PriceListLines {

    private String price_code;
    private String product_name;
    private String product_code;
    private String price;
    /*private String price2;
    private String effective_start_date;
    private String effective_end_date;*/


    public PriceListLines(String price_code,
                          String product_name,
                          String product_code,
                          String price
                         /* String price2,
                          String effective_start_date,
                          String effective_end_date */
    ) {

        this.price_code = price_code;
        this.product_name = product_name;
        this.product_code = product_code;
        this.price = price;
       /* this.price2 = price2;
        this.effective_start_date = effective_start_date;
        this.effective_end_date = effective_end_date; */

    }

    public String getPrice_code() {
        return price_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_code() {
        return product_code;
    }

    public String getPrice() {
        return price;
    }

   /* public String getPrice2() {
        return price2;
    }

    public String getEffective_start_date() {
        return effective_start_date;
    }

    public String getEffective_end_date() {
        return effective_end_date;
    } */


}

