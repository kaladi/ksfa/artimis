package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.SurveyLines;
import in.kumanti.emzor.model.SurveyQuestions;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY_QUESTIONS;

public class SurveyQuestionsTable extends SQLiteOpenHelper {

    private static final String COLUMN_SURVEY_QUESTION_ID = "id";
    private static final String COLUMN_SURVEY_ID = "survey_id";
    private static final String COLUMN_SURVEY_QUESTION_CODE = "question_code";
    private static final String COLUMN_SURVEY_QUESTION_TYPE = "question_type";
    private static final String COLUMN_SURVEY_QUESTION_NAME = "question_name";
    private static final String COLUMN_SURVEY_QUESTION_CREATED_AT = "created_at";
    private static final String COLUMN_SURVEY_QUESTION_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;
    private SurveyQuestionOptionTable surveyQuestionOptionTable;

    public SurveyQuestionsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY_QUESTIONS);

        String surveyQuestionsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_SURVEY_QUESTIONS + "(" +
                COLUMN_SURVEY_QUESTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SURVEY_ID + " INT, " +
                COLUMN_SURVEY_QUESTION_CODE + " TEXT, " +
                COLUMN_SURVEY_QUESTION_NAME + " TEXT, " +
                COLUMN_SURVEY_QUESTION_TYPE + " TEXT, " +
                COLUMN_SURVEY_QUESTION_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SURVEY_QUESTION_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(surveyQuestionsTable);

        surveyQuestionOptionTable = new SurveyQuestionOptionTable(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(SurveyLines surveyQuestions) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SURVEY_ID, surveyQuestions.getSurvey_code());
        values.put(COLUMN_SURVEY_QUESTION_NAME, surveyQuestions.getQuestion_desc());
        values.put(COLUMN_SURVEY_QUESTION_CODE, surveyQuestions.getQuestion_code());
        values.put(COLUMN_SURVEY_QUESTION_TYPE, surveyQuestions.getQuestion_type());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SURVEY_QUESTIONS, null, values);
        db.close();
        return id;
    }

    ArrayList<SurveyQuestions> getQuestionsBySurveyId(String surveyId) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_SURVEY_QUESTIONS + " WHERE " + COLUMN_SURVEY_ID + "= ?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{surveyId});
        int id;

        ArrayList<SurveyQuestions> surveyQuestionsList = new ArrayList<>();
        while (cursor.moveToNext()) {
            SurveyQuestions surveyQuestion = new SurveyQuestions();
            id = cursor.getInt(0);
            surveyQuestion.setId(id);
            surveyQuestion.setSurvey_id(cursor.getString(1));
            surveyQuestion.setQuestion_code(cursor.getString(2));
            surveyQuestion.setQuestion_name(cursor.getString(3));
            surveyQuestion.setQuestion_type(cursor.getString(4));
            surveyQuestion.setSurveyQuestionOptions(surveyQuestionOptionTable.getOptionsByQuestionId(cursor.getString(2)));
            surveyQuestionsList.add(surveyQuestion);
        }
        cursor.close();
        sqlWrite.close();
        return surveyQuestionsList;
    }

    /*
     * Fetching the Survey Questions List from the Master via Web Service
     */
    public void insertingSurveyQuestionsMaster(ArrayList<SurveyLines> surveyLinesArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (surveyLinesArrayList != null) {
                for (int i = 0; i < surveyLinesArrayList.size(); i++) {
                    values.put(COLUMN_SURVEY_ID, surveyLinesArrayList.get(i).getSurvey_code());
                    values.put(COLUMN_SURVEY_QUESTION_NAME, surveyLinesArrayList.get(i).getQuestion_desc());
                    values.put(COLUMN_SURVEY_QUESTION_CODE, surveyLinesArrayList.get(i).getQuestion_code());
                    values.put(COLUMN_SURVEY_QUESTION_TYPE, surveyLinesArrayList.get(i).getQuestion_type());
                    db.insert(TABLE_SURVEY_QUESTIONS, null, values);
                }
                db.setTransactionSuccessful();

            }

        } finally {
            db.endTransaction();
        }

    }

   /* public void InsertTable(String Table_Name, ContentValues contentValues) {
        SQLiteDatabase  db=this.getWritableDatabase();
        db.insert(Table_Name,null,contentValues);
        db.close();
    }*/

   /* public void clearAllQuiz(){
        sqlWrite = this.getWritableDatabase();
        sqlWrite.delete(TABLE_SURVEY_QUESTIONS, COLUMN_SURVEY_QUESTION_ID + " >= 0", null);
        sqlWrite.close();
        Log.d("QuizData", "clearedAllQuiz");
    }*/

    public void deleteSurveyLines() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SURVEY_QUESTIONS, null, null);
        db.close();
    }

}
