package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class CompanyMaster {

    public String address1;
    public String address2;
    public String address3;
    public String imageName;
    public String imageUrl;
    private String company_code;
    private String company_name;
    private String contact_person;
    private String email;
    private String mobile_number;
    private String support_num1;
    private String support_num2;
    private String support_email;

    public CompanyMaster(

            String company_code,
            String company_name,
            String contact_person,
            String email,
            String mobile_number,
            String support_num1,
            String support_num2,
            String support_email,
            String address1,
            String address2,
            String address3,
            String imageFileName,
            String imageUrl

    ) {

        this.company_code = company_code;
        this.company_name = company_name;
        this.contact_person = contact_person;
        this.email = email;
        this.mobile_number = mobile_number;
        this.support_num1 = support_num1;
        this.support_num2 = support_num2;
        this.support_email = support_email;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.imageName = imageFileName;
        this.imageUrl = imageUrl;

    }

    public String getCompany_code() {
        return company_code;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getContact_person() {
        return contact_person;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public String getSupport_num1() {
        return support_num1;
    }

    public String getSupport_num2() {
        return support_num2;
    }

    public String getSupport_email() {
        return support_email;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getAddress3() {
        return address3;
    }


    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

