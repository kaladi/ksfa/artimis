package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CompetitorStockProduct;
import in.kumanti.emzor.model.CustomerStockProduct;

public class MarketInfoReportsAdapter extends RecyclerView.Adapter<MarketInfoReportsAdapter.ViewHolder> {
    public ArrayList<CustomerStockProduct> mDataset;
    public Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView date,customerName, stockProductName, stockProductAvailability, stockProductVolume;
        public TextView competitorProductName, competitorProductAvailability, competitorProductVolume, competitorProductPrice, competitorProductPromotion;
        public ImageView expandCompetitorListImageButton;
        public RecyclerView marketInfoExpandableRecyclerView;
        private RecyclerView.Adapter marketInfoExpandableAdapter;
        private RecyclerView.LayoutManager marketInfoExpandableLayoutManager;

        public ViewHolder(View v) {
            super(v);

            date = v.findViewById(R.id.marketReportsDate);
            customerName = v.findViewById(R.id.marketReportsCustomerName);
            stockProductName = v.findViewById(R.id.marketReportsStockProductName);
            stockProductAvailability = v.findViewById(R.id.marketReportsStockAvailability);
            stockProductVolume = v.findViewById(R.id.marketReportsStockVolume);
            competitorProductName = v.findViewById(R.id.marketReportsCompetitorName);
            competitorProductAvailability = v.findViewById(R.id.marketReportsCompetitorAvailability);
            competitorProductVolume = v.findViewById(R.id.marketReportsCompetitorVolume);
            competitorProductPrice = v.findViewById(R.id.marketReportsCompetitorPrice);
            competitorProductPromotion = v.findViewById(R.id.marketReportsCompetitorPromotion);
            expandCompetitorListImageButton =  v.findViewById(R.id.marketReportsCompetitorExpandImageView);
            marketInfoExpandableRecyclerView = v.findViewById(R.id.marketInfoExpandableReportsList);
        }
    }

    public void add(CustomerStockProduct item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CompetitorStockProduct item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public MarketInfoReportsAdapter(ArrayList<CustomerStockProduct> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_market_info_reports, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.date.setText(mDataset.get(position).getDate());
        viewHolder.customerName.setText(mDataset.get(position).getCustomer_name());
        viewHolder.stockProductName.setText(mDataset.get(position).getStock_product_name());
        viewHolder.stockProductAvailability.setText(mDataset.get(position).isStock_availability()?"Yes":"No");
        viewHolder.stockProductVolume.setText(String.valueOf(mDataset.get(position).getStock_volume()));

        if(mDataset.get(position).getCompetitorStockProductArrayList().size() <2) {
            viewHolder.expandCompetitorListImageButton.setBackgroundResource(R.drawable.border);
        }


        //To Set the value of the first Item
        if(mDataset.get(position).getCompetitorStockProductArrayList().size() >0)
        {
            CompetitorStockProduct cd = mDataset.get(position).getCompetitorStockProductArrayList().get(0);
            viewHolder.competitorProductName.setText(cd.getProductName());
            viewHolder.competitorProductAvailability.setText(cd.getCompetitor_product_availability()?"Yes":"No");
            viewHolder.competitorProductVolume.setText(String.valueOf(cd.getCompetitor_product_volume()));
            viewHolder.competitorProductPrice.setText(String.valueOf(cd.getCompetitor_product_price()));
            viewHolder.competitorProductPromotion.setText(cd.getCompetitor_product_promotion());
            viewHolder.marketInfoExpandableLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

            viewHolder.marketInfoExpandableRecyclerView.setHasFixedSize(true);
            viewHolder.marketInfoExpandableRecyclerView.setLayoutManager(viewHolder.marketInfoExpandableLayoutManager);

            Log.d("RecycleAdapter","Size"+mDataset.get(position).getCompetitorStockProductArrayList().size());
            ArrayList<CompetitorStockProduct> marketInfoExpandableArrayList = new ArrayList<CompetitorStockProduct>();
            int i = 0;
            for(CompetitorStockProduct cds: mDataset.get(position).getCompetitorStockProductArrayList())
            {
                if(i!=0)
                {
                    marketInfoExpandableArrayList.add(cds);
                }
                i++;
            }
            viewHolder.marketInfoExpandableAdapter = new MarketInfoExpandableAdapter(marketInfoExpandableArrayList);
            viewHolder.marketInfoExpandableRecyclerView.setAdapter(viewHolder.marketInfoExpandableAdapter);
            viewHolder.marketInfoExpandableRecyclerView.setVisibility(View.GONE);
        }

        viewHolder.expandCompetitorListImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.marketInfoExpandableRecyclerView.getVisibility() == View.VISIBLE) {
                    viewHolder.marketInfoExpandableRecyclerView.setVisibility(View.GONE);
                    viewHolder.expandCompetitorListImageButton.setImageResource(R.drawable.ic_expand_more_black_24dp);

                } else {
                    viewHolder.marketInfoExpandableRecyclerView.setVisibility(View.VISIBLE);
                    viewHolder.expandCompetitorListImageButton.setImageResource(R.drawable.ic_expand_less_black_24dp);
                }
            }
        });





    }



    @Override
    public int getItemCount() {
        return mDataset.size();
    }




}
class MarketInfoExpandableAdapter extends RecyclerView.Adapter<MarketInfoExpandableAdapter.ViewHolder> {
    public ArrayList<CompetitorStockProduct> mDataset;
    public Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView competitorProductName, competitorProductAvailability, competitorProductVolume, competitorProductPrice, competitorProductPromotion;
        public ViewHolder(View v) {
            super(v);

            competitorProductName = v.findViewById(R.id.marketReportsCompetitorName);
            competitorProductAvailability = v.findViewById(R.id.marketReportsCompetitorAvailability);
            competitorProductVolume = v.findViewById(R.id.marketReportsCompetitorVolume);
            competitorProductPrice = v.findViewById(R.id.marketReportsCompetitorPrice);
            competitorProductPromotion = v.findViewById(R.id.marketReportsCompetitorPromotion);

        }
    }

    public void add(CompetitorStockProduct item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CompetitorStockProduct item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public MarketInfoExpandableAdapter(ArrayList<CompetitorStockProduct> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_market_info_reports_expandable, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        CompetitorStockProduct cd = mDataset.get(position);
            Log.d("RecycleAdapter","Child"+position+" product"+cd.getCompetitor_product_id());

            viewHolder.competitorProductName.setText(cd.getProductName());
            viewHolder.competitorProductAvailability.setText(cd.getCompetitor_product_availability()?"Yes":"No");
            viewHolder.competitorProductVolume.setText(String.valueOf(cd.getCompetitor_product_volume()));
            viewHolder.competitorProductPrice.setText(String.valueOf(cd.getCompetitor_product_price()));
            viewHolder.competitorProductPromotion.setText(cd.getCompetitor_product_promotion());


    }



    @Override
    public int getItemCount() {
        return mDataset.size();
    }




}