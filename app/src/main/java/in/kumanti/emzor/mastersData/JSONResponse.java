package in.kumanti.emzor.mastersData;

public class JSONResponse {

    private PriceListHeader[] price_header;
    private PriceListLines[] price_line;
    private ItemMaster[] Product;
    private LocationHeader[] location_header;
    private LocationLines[] location_line;
    private TabMaster[] tab_master;
    private SRMaster[] sr_master;
    private CompanyMaster[] company_master;

    public PriceListHeader[] getPriceListHeader() {
        return price_header;
    }

    public PriceListLines[] getPrice_line() {
        return price_line;
    }

    public ItemMaster[] getItemMaster() {
        return Product;
    }

    public LocationHeader[] getLocation_header() {
        return location_header;
    }

    public LocationLines[] getLocation_line() {
        return location_line;
    }

    public TabMaster[] getTab_master() {
        return tab_master;
    }

    public SRMaster[] getSr_master() {
        return sr_master;
    }

    public CompanyMaster[] getCompanyMasters() {
        return company_master;
    }
}