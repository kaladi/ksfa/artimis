package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.BdeReceiveProducts;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_BDE_RECEIVE_PRODUCTS;

public class BdeReceiveProductsTable extends SQLiteOpenHelper {

    //Columns used for Stock Return Products Information details for a selected ordered product
    private static final String COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_PRODUCT_ID = "id";
    private static final String COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_STOCK_ISSUE_NUMBER = "stockIssueNumber";
    private static final String COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_STOCK_ISSUE_DATE = "stockIssueDate";
    private static final String COLUMN_BDE_RECEIVE_TO_BDE_CODE = "toBdeCode";
    private static final String COLUMN_BDE_RECEIVE_FROM_BDE_CODE = "fromBdeCode";
    private static final String COLUMN_BDE_RECEIVE_PRODUCT_CODE = "productCode";
    private static final String COLUMN_BDE_RECEIVE_PRODUCT_NAME = "productName";
    private static final String COLUMN_BDE_RECEIVE_PRODUCT_UOM = "productUom";
    private static final String COLUMN_BDE_RECEIVE_UNIT_PRICE = "unitPrice";
    private static final String COLUMN_BDE_RECEIVE_ISSUE_QUANTITY = "issueQuantity";
    private static final String COLUMN_BDE_RECEIVE_ISSUE_VALUE = "issueValue";
    private static final String COLUMN_BDE_RECEIVE_IS_BATCH_CONTROLLED = "isBatchControlled";
    private static final String COLUMN_BDE_RECEIVE_IS_SYNC = "isSync";
    private static final String COLUMN_BDE_RECEIVE_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_BDE_RECEIVE_CREATED_AT = "createdAt";
    private static final String COLUMN_BDE_RECEIVE_UPDATED_AT = "updatedAt";

    public Context context;
    private BdeReceiveBatchProductsTable bdeReceiveBatchProductsTable;

    public BdeReceiveProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_BDE_RECEIVE_PRODUCTS);

        String stockReturnProductTable = "CREATE TABLE IF NOT EXISTS " + TABLE_BDE_RECEIVE_PRODUCTS + "(" +
                COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_STOCK_ISSUE_NUMBER + " TEXT, " +
                COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_STOCK_ISSUE_DATE + " TEXT, " +
                COLUMN_BDE_RECEIVE_FROM_BDE_CODE + " TEXT, " +
                COLUMN_BDE_RECEIVE_TO_BDE_CODE + " TEXT, " +
                COLUMN_BDE_RECEIVE_PRODUCT_CODE + " TEXT, " +
                COLUMN_BDE_RECEIVE_PRODUCT_NAME + " TEXT, " +
                COLUMN_BDE_RECEIVE_PRODUCT_UOM + " INTEGER, " +
                COLUMN_BDE_RECEIVE_UNIT_PRICE + " INTEGER, " +
                COLUMN_BDE_RECEIVE_ISSUE_QUANTITY + " NUMERIC, " +
                COLUMN_BDE_RECEIVE_ISSUE_VALUE + " NUMERIC, " +
                COLUMN_BDE_RECEIVE_IS_BATCH_CONTROLLED + " TEXT, " +
                COLUMN_BDE_RECEIVE_IS_SYNC + " TEXT, " +
                COLUMN_BDE_RECEIVE_LATEST_SYNC_DATE + " TEXT, " +

                COLUMN_BDE_RECEIVE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_BDE_RECEIVE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(stockReturnProductTable);

        bdeReceiveBatchProductsTable = new BdeReceiveBatchProductsTable(this.context);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(BdeReceiveProducts bdeReceiveProducts) {
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_STOCK_ISSUE_NUMBER, bdeReceiveProducts.getStockIssueNumber());
            values.put(COLUMN_BDE_RECEIVE_PRODUCTS_TEMP_STOCK_ISSUE_DATE, bdeReceiveProducts.getStockIssueDate());
            values.put(COLUMN_BDE_RECEIVE_FROM_BDE_CODE, bdeReceiveProducts.getFromBdeCode());
            values.put(COLUMN_BDE_RECEIVE_TO_BDE_CODE, bdeReceiveProducts.getToBdeCode());
            values.put(COLUMN_BDE_RECEIVE_PRODUCT_CODE, bdeReceiveProducts.getProductCode());
            values.put(COLUMN_BDE_RECEIVE_PRODUCT_NAME, bdeReceiveProducts.getProductName());
            values.put(COLUMN_BDE_RECEIVE_PRODUCT_UOM, bdeReceiveProducts.getProductUom());
            values.put(COLUMN_BDE_RECEIVE_UNIT_PRICE, bdeReceiveProducts.getIssuePrice());
            values.put(COLUMN_BDE_RECEIVE_ISSUE_QUANTITY, bdeReceiveProducts.getIssueQuantity());
            values.put(COLUMN_BDE_RECEIVE_ISSUE_VALUE, bdeReceiveProducts.getIssueValue());
            values.put(COLUMN_BDE_RECEIVE_IS_BATCH_CONTROLLED, bdeReceiveProducts.getIsBatchControlled());

            values.put(COLUMN_BDE_RECEIVE_IS_SYNC, bdeReceiveProducts.isSync());
            values.put(COLUMN_BDE_RECEIVE_LATEST_SYNC_DATE, bdeReceiveProducts.getLatestSynDate());


            SQLiteDatabase db = getWritableDatabase();
            long id = db.insert(TABLE_BDE_RECEIVE_PRODUCTS, null, values);
            db.close();
            return id;
        } catch (Exception e) {
            Log.d("FileImport", "DB Error " + e.getMessage());
            return -1;
        }

    }

    public ArrayList<BdeReceiveProducts> getBdeReceiveProducts(String bdeCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BDE_RECEIVE_PRODUCTS;
        if (bdeCode != null)
            selectQuery += " WHERE frombdeCode = '" + bdeCode + "'";
        selectQuery += " order by stockIssueDate desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        ArrayList<BdeReceiveProducts> bdeReceiveProductsArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            BdeReceiveProducts brp = new BdeReceiveProducts();
            brp.setId(cursor.getInt(0));
            brp.setStockIssueNumber(cursor.getString(1));
            brp.setStockIssueDate(cursor.getString(2));
            brp.setFromBdeCode(cursor.getString(3));
            brp.setToBdeCode(cursor.getString(4));
            brp.setProductCode(cursor.getString(5));
            brp.setProductName(cursor.getString(6));
            brp.setProductUom(cursor.getString(7));
            brp.setIssuePrice(cursor.getString(8));
            brp.setIssueQuantity(cursor.getString(9));
            brp.setIssueValue(cursor.getString(10));
            brp.setIsBatchControlled(cursor.getString(11));
            brp.setBdeReceiveBatchProductsArrayList(bdeReceiveBatchProductsTable.getBdeReceiveBatchProducts(brp.getStockIssueNumber(), brp.getProductCode()));
            bdeReceiveProductsArrayList.add(i, brp);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return bdeReceiveProductsArrayList;
    }

    /*public ArrayList<BdeReceiveProducts> getBdeByReceiveProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BDE_RECEIVE_PRODUCTS ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<BdeReceiveProducts> bdeReceiveProductsArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            BdeReceiveProducts brp = new BdeReceiveProducts();
            brp.setId(cursor.getInt(0));
            brp.setStockIssueNumber(cursor.getString(1));
            brp.setStockIssueDate(cursor.getString(2));
            brp.setFromBdeCode(cursor.getString(3));
            brp.setToBdeCode(cursor.getString(4));
            brp.setProductCode(cursor.getString(5));
            brp.setProductName(cursor.getString(6));
            brp.setProductUom(cursor.getString(7));
            brp.setIssuePrice(cursor.getString(8));
            brp.setIssueQuantity(cursor.getString(9));
            brp.setIssueValue(cursor.getString(10));
            brp.setIsBatchControlled(cursor.getString(11));
            brp.setBdeReceiveBatchProductsArrayList(bdeReceiveBatchProductsTable.getBdeReceiveBatchProducts(brp.getStockIssueNumber(),brp.getProductCode()));
            bdeReceiveProductsArrayList.add(i, brp);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return bdeReceiveProductsArrayList;
    }*/

}
