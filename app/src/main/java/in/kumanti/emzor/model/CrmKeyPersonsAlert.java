package in.kumanti.emzor.model;


public class CrmKeyPersonsAlert {

    public int alertId;
    public String keyPersonId;
    public String opportunityId;
    public String alertType;
    public String sms;
    public String email;
    public boolean isSync;
    public String latestSyncDate;
    public String createdAt;
    public String updatedAt;

    public int getAlertId() {
        return alertId;
    }

    public void setAlertId(int alertId) {
        this.alertId = alertId;
    }

    public String getKeyPersonId() {
        return keyPersonId;
    }

    public void setKeyPersonId(String keyPersonId) {
        this.keyPersonId = keyPersonId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getAlertType() {
        return alertType;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getLatestSyncDate() {
        return latestSyncDate;
    }

    public void setLatestSyncDate(String latestSyncDate) {
        this.latestSyncDate = latestSyncDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
