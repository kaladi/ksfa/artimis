package in.kumanti.emzor.model;

public class CustomerReturn {

    public boolean isSync;
    public String visitSeqNumber;
    private String tabCode;
    private String companyCode;
    private String srCode;
    private int customerReturnId;
    private String customerReturnNumber;
    private String customerReturnDate;
    private String customerCode;
    private String invoiceNumber;
    private String returnProductsCount;
    private String returnValue;
    private String returnDiscount;
    private String returnNetValue;
    private String paidAmount;
    private String balanceAmount;
    private String changeType;
    private String signeeName;
    private String signature;
    private String emailSent;
    private String smsSent;
    private String printDone;
    private int randomNum;
    private String createdAt;
    private String updatedAt;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public int getCustomerReturnId() {
        return customerReturnId;
    }

    public void setCustomerReturnId(int customerReturnId) {
        this.customerReturnId = customerReturnId;
    }

    public String getCustomerReturnNumber() {
        return customerReturnNumber;
    }

    public void setCustomerReturnNumber(String customerReturnNumber) {
        this.customerReturnNumber = customerReturnNumber;
    }

    public String getCustomerReturnDate() {
        return customerReturnDate;
    }

    public void setCustomerReturnDate(String customerReturnDate) {
        this.customerReturnDate = customerReturnDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getReturnProductsCount() {
        return returnProductsCount;
    }

    public void setReturnProductsCount(String returnProductsCount) {
        this.returnProductsCount = returnProductsCount;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public String getReturnDiscount() {
        return returnDiscount;
    }

    public void setReturnDiscount(String returnDiscount) {
        this.returnDiscount = returnDiscount;
    }

    public String getReturnNetValue() {
        return returnNetValue;
    }

    public void setReturnNetValue(String returnNetValue) {
        this.returnNetValue = returnNetValue;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    public String getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(String emailSent) {
        this.emailSent = emailSent;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    public String getPrintDone() {
        return printDone;
    }

    public void setPrintDone(String printDone) {
        this.printDone = printDone;
    }

    public int getRandomNum() {
        return randomNum;
    }

    public void setRandomNum(int randomNum) {
        this.randomNum = randomNum;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getVisitSeqNumber() {
        return visitSeqNumber;
    }

    public void setVisitSeqNumber(String visitSeqNumber) {
        this.visitSeqNumber = visitSeqNumber;
    }

    public String getSigneeName() {
        return signeeName;
    }

    public void setSigneeName(String signeeName) {
        this.signeeName = signeeName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }
}
