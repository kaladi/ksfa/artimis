package in.kumanti.emzor.model;


public class DoctorVisitInfo {

    public int id;
    public String srCode;
    public String doctorVisitId;
    public String doctorCode;
    public String visitSeqNo;
    public String avgVisitPatients;
    public String avgPrescriptionsIssued;
    public String visitDate;
    public String createdAt;
    public String updatedAt;
    public String tabCode;
    public String companyCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }


    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getDoctorVisitId() {
        return doctorVisitId;
    }

    public void setDoctorVisitId(String doctorVisitId) {
        this.doctorVisitId = doctorVisitId;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getVisitSeqNo() {
        return visitSeqNo;
    }

    public void setVisitSeqNo(String visitSeqNo) {
        this.visitSeqNo = visitSeqNo;
    }

    public String getAvgVisitPatients() {
        return avgVisitPatients;
    }

    public void setAvgVisitPatients(String avgVisitPatients) {
        this.avgVisitPatients = avgVisitPatients;
    }

    public String getAvgPrescriptionsIssued() {
        return avgPrescriptionsIssued;
    }

    public void setAvgPrescriptionsIssued(String avgPrescriptionsIssued) {
        this.avgPrescriptionsIssued = avgPrescriptionsIssued;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPharmacyInfoId() {
        return doctorVisitId;
    }

    public void setPharmacyInfoId(String pharmacyInfoId) {
        this.doctorVisitId = pharmacyInfoId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
