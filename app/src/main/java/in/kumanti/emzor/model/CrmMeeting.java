package in.kumanti.emzor.model;


public class CrmMeeting {

    public int meetingId = 0;
    public String companyCode;
    public String tabCode;
    public String srCode;
    public String customerCode;
    public String crmNumber;
    public String crmDate;
    public String meetingDate;
    public String meetingType;
    public String meetingStatus;
    public String remarks;
    public boolean isSync;
    public String createdAt;
    public String updatedAt;
    public String error_meetingDate;
    public String error_meetingType;
    public String error_meetingStatus;
    public String error_remarks;

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCrmNumber() {
        return crmNumber;
    }

    public void setCrmNumber(String crmNumber) {
        this.crmNumber = crmNumber;
    }

    public String getCrmDate() {
        return crmDate;
    }

    public void setCrmDate(String crmDate) {
        this.crmDate = crmDate;
    }

    public String getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(String meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(String meetingType) {
        this.meetingType = meetingType;
    }

    public String getMeetingStatus() {
        return meetingStatus;
    }

    public void setMeetingStatus(String meetingStatus) {
        this.meetingStatus = meetingStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getError_MeetingDate() {
        return error_meetingDate;
    }

    public void setError_MeetingDate(String error_meetingDate) {
        this.error_meetingDate = error_meetingDate;
    }

    public String getError_MeetingType() {
        return error_meetingType;
    }

    public void setError_MeetingType(String error_meetingType) {
        this.error_meetingType = error_meetingType;
    }

    public String getError_MeetingStatus() {
        return error_meetingStatus;
    }

    public void setError_MeetingStatus(String error_meetingStatus) {
        this.error_meetingStatus = error_meetingStatus;
    }

    public String getError_Remarks() {
        return error_remarks;
    }

    public void setError_Remarks(String error_remarks) {
        this.error_remarks = error_remarks;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
