package in.kumanti.emzor.model;


public class LogInOut {

    public int logInOutId;
    public String logInDate;
    public String logInTime;
    public String logOutDate;
    public String logOutTime;
    public String createdAt;
    public String updatedAt;
    private String tabCode;
    private String companyCode;
    private String srCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public int getLogInOutId() {
        return logInOutId;
    }

    public void setLogInOutId(int logInOutId) {
        this.logInOutId = logInOutId;
    }

    public String getLogInDate() {
        return logInDate;
    }

    public void setLogInDate(String logInDate) {
        this.logInDate = logInDate;
    }

    public String getLogInTime() {
        return logInTime;
    }

    public void setLogInTime(String logInTime) {
        this.logInTime = logInTime;
    }

    public String getLogOutDate() {
        return logOutDate;
    }

    public void setLogOutDate(String logOutDate) {
        this.logOutDate = logOutDate;
    }

    public String getLogOutTime() {
        return logOutTime;
    }

    public void setLogOutTime(String logOutTime) {
        this.logOutTime = logOutTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
