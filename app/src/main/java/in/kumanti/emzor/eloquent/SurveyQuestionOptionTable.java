package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.SurveyLines;
import in.kumanti.emzor.model.SurveyQuestionOption;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY_QUESTIONS_OPTION;

public class SurveyQuestionOptionTable extends SQLiteOpenHelper {

    private static final String COLUMN_OPTION_ID = "id";
    private static final String COLUMN_SURVEY_ID = "survey_id";
    private static final String COLUMN_QUESTION_ID = "question_id";
    private static final String COLUMN_OPTION_TEXT = "option_text";
    private static final String COLUMN_CREATED_AT = "created_at";
    private static final String COLUMN_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public SurveyQuestionOptionTable(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY_QUESTIONS_OPTION);

        String surveyQuestionOptionTable = "CREATE TABLE IF NOT EXISTS " + TABLE_SURVEY_QUESTIONS_OPTION + "(" +
                COLUMN_OPTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SURVEY_ID + " TEXT, " +
                COLUMN_QUESTION_ID + " TEXT, " +
                COLUMN_OPTION_TEXT + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(surveyQuestionOptionTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY_QUESTIONS_OPTION);

    }

    public long create(SurveyLines surveyQuestionOption) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SURVEY_ID, surveyQuestionOption.getSurvey_code());
        values.put(COLUMN_QUESTION_ID, surveyQuestionOption.getQuestion_code());
        values.put(COLUMN_OPTION_TEXT, surveyQuestionOption.getOption_text());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SURVEY_QUESTIONS_OPTION, null, values);
        db.close();
        return id;
    }

    ArrayList<SurveyQuestionOption> getOptionsByQuestionId(String questionId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SURVEY_QUESTIONS_OPTION + " WHERE " + COLUMN_QUESTION_ID + "=? ";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{questionId});

        Log.d("Data", "getAnswerByQuestionId  " + questionId);
        ArrayList<SurveyQuestionOption> surveyQuestionOptionList = new ArrayList<>();

        while (cursor.moveToNext()) {
            SurveyQuestionOption surveyQuestionOption = new SurveyQuestionOption();
            surveyQuestionOption.setId(cursor.getInt(0));
            surveyQuestionOption.setSurveyId(cursor.getString(1));
            surveyQuestionOption.setQuestionId(cursor.getString(2));
            surveyQuestionOption.setOptionText(cursor.getString(3));
            surveyQuestionOptionList.add(surveyQuestionOption);
        }

        cursor.close();
        sqlWrite.close();
        return surveyQuestionOptionList;
    }

    /*
     * Fetching the Survey Options List from the Master via Web Service
     */
    public void insertingSurveyOptionsMaster(ArrayList<SurveyLines> surveyOptionsArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (surveyOptionsArrayList != null) {
                for (int i = 0; i < surveyOptionsArrayList.size(); i++) {
                    values.put(COLUMN_SURVEY_ID, surveyOptionsArrayList.get(i).getSurvey_code());
                    values.put(COLUMN_QUESTION_ID, surveyOptionsArrayList.get(i).getQuestion_code());
                    values.put(COLUMN_OPTION_TEXT, surveyOptionsArrayList.get(i).getOption_text());
                    db.insert(TABLE_SURVEY_QUESTIONS_OPTION, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }
    }

    public void clearAllQuestions() {
        sqlWrite = this.getWritableDatabase();
        sqlWrite.close();
        Log.d("QuizData", "clearedAllQuiz");
    }

    public void deleteSurveyOptions() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SURVEY_QUESTIONS_OPTION, null, null);
        db.close();
    }

    /*public long insertQuiz(String questionId, String selectedAnswerId, String correctAnswerId) {
        int id = this.getIdByQuiz(questionId);
        Log.d("QuizData", "Question Id " + questionId + " -id " + id + " Selected Answer Id " + selectedAnswerId + " Correct Answer " + correctAnswerId);
        if(id == 0) {
            Log.d("QuizData","Inside Insert");
            sqlWrite = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_QUESTION_ID, questionId);
            sqlWrite.insert(TABLE_SURVEY_QUESTIONS_OPTION, null, cv);
            sqlWrite.close();
            return id;
        }
        else {
            updateQuiz(id, questionId, selectedAnswerId, correctAnswerId);
            return id;
        }
    }*/

   /* private int updateQuiz(int id, String questionId, String selectedAnswerId, String correctAnswerId) {
        sqlWrite = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_QUESTION_ID, questionId);

        sqlWrite.update(TABLE_SURVEY_QUESTIONS_OPTION, cv, "id=" + id, null);
        sqlWrite.close();
        Log.d("QuizData", "Qus " +questionId);
        Log.d("QuizData", "Sel " +selectedAnswerId);
        Log.d("QuizData", "Cor " +correctAnswerId);

        return id;
    }*/

    /*private int getIdByQuiz(String questionId) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_SURVEY_QUESTIONS_OPTION + " WHERE " + COLUMN_QUESTION_ID + "= ?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{questionId});

        int id = 0;
        while (cursor.moveToNext()) {
            id = cursor.getInt(0);
            Log.d("QuizData", "ID " + cursor.getColumnIndex("id"));
            Log.d("QuizData", "col0 " + cursor.getString(0));
            Log.d("QuizData", "col1 " + cursor.getString(1));
            Log.d("QuizData", "col2 " + cursor.getString(2));
            Log.d("QuizData", "col3 " + cursor.getString(3));
        }
        cursor.close();
        sqlWrite.close();
        return id;
    }*/


}
