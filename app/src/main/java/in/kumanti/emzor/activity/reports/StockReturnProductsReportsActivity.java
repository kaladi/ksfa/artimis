package in.kumanti.emzor.activity.reports;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.adapter.StockReturnProductsReportsAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.StockReturnProductsReport;

public class StockReturnProductsReportsActivity extends MainActivity {

    public RecyclerView stockReturnProductsRecyclerView;
    String login_id = "";
    ImageView actionbarBackButton, deviceInfo;
    String checkin_time = "", customer_id = "", marqueeTextString;
    MyDBHandler dbHandler;
    TextView actionbarTitle;
    private RecyclerView.Adapter stockReturnProductsAdapter;
    private RecyclerView.LayoutManager stockReturnProductsLayoutManager;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_stock_return_products_reports);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Stock Return Report");

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);


        //Populate the Quotation Product details using the Recycler view


        try {
            stockReturnProductsRecyclerView.setHasFixedSize(true);
            stockReturnProductsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            stockReturnProductsRecyclerView.setLayoutManager(stockReturnProductsLayoutManager);

            ArrayList<StockReturnProductsReport> expiredProductsReportArrayList = dbHandler.getStockReturnProductsReports();
            stockReturnProductsAdapter = new StockReturnProductsReportsAdapter(expiredProductsReportArrayList);
            stockReturnProductsRecyclerView.setAdapter(stockReturnProductsAdapter);
        } catch (Exception e) {
            Log.d("Expired Response", "" + e.getMessage());
        }


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }


    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        stockReturnProductsRecyclerView = findViewById(R.id.stockReturnProductsReportsList);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            Log.d("LoginId", login_id);
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


}
