package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class PurchaseHistoryItems {
    private String date;
    private String quantity;
    private String price;
    private String stock;
    private String stock1;

    public PurchaseHistoryItems(String date1, String quantity1, String price1, String stk, String stk1) {
        date = date1;
        quantity = quantity1;
        price = price1;
        stock = stk;
        stock1 = stk1;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date1) {
        date = date1;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity1) {
        quantity = quantity1;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price1) {
        price = price1;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stk) {
        stock = stk;
    }

    public String getStock1() {
        return stock1;
    }

    public void setStock1(String stk1) {
        stock1 = stk1;
    }

}

