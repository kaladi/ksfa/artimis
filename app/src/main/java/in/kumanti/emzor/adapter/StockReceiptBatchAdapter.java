package in.kumanti.emzor.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class StockReceiptBatchAdapter extends RecyclerView.Adapter<StockReceiptBatchAdapter.ViewHolder> {

    public ArrayList<StockReceiptBatch> mDataset;
    public Context context;
    View alertLayout;
    private RecyclerViewItemListener mListener;
    private int mYear, mMonth, mDay;

    public StockReceiptBatchAdapter(ArrayList<StockReceiptBatch> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(StockReceiptBatch item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockReceiptBatch item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockReceiptBatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_receipt_batch, parent, false);


        alertLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_receipt_batch_delete_confirmation, parent, false);


        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getExpiryDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.expiryDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }
        viewHolder.batchNumber.setText(mDataset.get(position).getBatchNumber());
        viewHolder.batchQuantity.setText(mDataset.get(position).getBatchQuantity());

        //To Set Error message
        if (!TextUtils.isEmpty(mDataset.get(position).getError_batchNumber())) {
            viewHolder.batchNumber.setError(mDataset.get(position).getError_batchNumber());
        } else {
            viewHolder.batchNumber.setError(null);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getError_batchQuantity())) {
            viewHolder.batchQuantity.setError(mDataset.get(position).getError_batchQuantity());
        } else {
            viewHolder.batchQuantity.setError(null);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getError_expiryDate())) {
            viewHolder.expiryDate.setError(mDataset.get(position).getError_expiryDate());
        } else {
            viewHolder.expiryDate.setError(null);
        }
        viewHolder.expiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInspectionDatePickerView(viewHolder.expiryDate, mDataset.get(position));
            }
        });

        viewHolder.batchNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setBatchNumber(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });

        viewHolder.batchQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setBatchQuantity(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });

        viewHolder.expiryDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setExpiryDate(caption.getText().toString());
                }
            }
        });

        viewHolder.removeBatchRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Batch?");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                notifyDataSetChanged();
                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();


                    }

                } catch (Exception e) {

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void getInspectionDatePickerView(Button b, StockReceiptBatch srbd) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        try {
                            /*String PATTERN = "dd MMM YY";
                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d= new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            //txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            String date = dateFormat.format(d);
                            b.setText(date);
                            srbd.setExpiryDate(date);
                            b.setError(null); */

                           /* String date = (year + "-" + (monthOfYear +1 ) + "-" + dayOfMonth);
                            b.setText(date);
                            srbd.setExpiryDate(date);
                            b.setError(null); */

                            String PATTERN = "dd MMM YY";
                            String PATTERN1 = "yyyy-MM-dd";

                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            //txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            SimpleDateFormat dateFormat1 = new SimpleDateFormat();
                            Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat1.applyPattern(PATTERN1);
                            dateFormat1.format(d1);

                            b.setText(dateFormat.format(d1));
                            srbd.setExpiryDate(dateFormat1.format(d1));
                            b.setError(null);
                        } catch (Exception e) {

                        }
                    }
                }, mDay, mMonth, mYear);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
        datePickerDialog.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView batchNumber, batchQuantity;
        public ImageButton removeBatchRow;
        public Button expiryDate;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            expiryDate = v.findViewById(R.id.batchExpiryDateButton);
            batchNumber = v.findViewById(R.id.batchNumberEt);
            batchQuantity = v.findViewById(R.id.batchQuantityEt);
            removeBatchRow = v.findViewById(R.id.batchDeleteImageButton);
            batchQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
        }
    }


}