package in.kumanti.emzor.model;

public class Orders {

    private String tabCode;
    private String companyCode;
    private String srCode;
    private String customerCode;
    private String orderId;
    private String orderCode;
    private String orderDate;
    private String value;
    private String status;
    private String lineItems;
    private String lpoNumber;
    private String remarks;
    private String deliveryDate;
    private String shipAddressLine1;
    private String shipAddressLine2;
    private String customerSignature;
    private String visitSequenceNumber;
    public String signatureFilePath;
    private String signatureFileName;

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }

    public String getTabId() {
        return tabCode;
    }

    public void setTabId(String tabId) {
        this.tabCode = tabId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getDate() {
        return orderDate;
    }

    public void setDate(String date) {
        this.orderDate = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLineItems() {
        return lineItems;
    }

    public void setLineItems(String lineItems) {
        this.lineItems = lineItems;
    }

    public String getLpoNumber() {
        return lpoNumber;
    }

    public void setLpoNumber(String lpoNumber) {
        this.lpoNumber = lpoNumber;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getShipAddress1() {
        return shipAddressLine1;
    }

    public void setShipAddress1(String shipAddress1) {
        this.shipAddressLine1 = shipAddress1;
    }

    public String getShipAddress2() {
        return shipAddressLine2;
    }

    public void setShipAddress2(String shipAddress2) {
        this.shipAddressLine2 = shipAddress2;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getShipAddressLine1() {
        return shipAddressLine1;
    }

    public void setShipAddressLine1(String shipAddressLine1) {
        this.shipAddressLine1 = shipAddressLine1;
    }

    public String getShipAddressLine2() {
        return shipAddressLine2;
    }

    public void setShipAddressLine2(String shipAddressLine2) {
        this.shipAddressLine2 = shipAddressLine2;
    }

    public String getCustomerSignature() {
        return customerSignature;
    }

    public void setCustomerSignature(String customerSignature) {
        this.customerSignature = customerSignature;
    }


    public String getSignatureFileName() {
        return signatureFileName;
    }

    public void setSignatureFileName(String signatureFileName) {
        this.signatureFileName = signatureFileName;
    }

    public String getSignatureFilePath() {
        return signatureFilePath;
    }

    public void setSignatureFilePath(String signatureFilePath) {
        this.signatureFilePath = signatureFilePath;
    }


    @Override
    public String toString() {
        return orderCode;
    }
}
