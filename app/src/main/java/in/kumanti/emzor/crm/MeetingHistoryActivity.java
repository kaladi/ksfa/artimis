package in.kumanti.emzor.crm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.adapter.MeetingListAdapter;
import in.kumanti.emzor.eloquent.CrmMeetingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CrmMeeting;
import in.kumanti.emzor.utils.FileUtils;

public class MeetingHistoryActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    TextView actionbarTitle;

    private TextView marqueeText;
    String  marqueeTextString;
    MyDBHandler dbHandler;
    CrmMeetingsTable crmMeetingsTable;

    public RecyclerView meetingListRecyclerView;
    RecyclerView.Adapter meetingListAdapter;
    RecyclerView.LayoutManager meetingListLayoutManager;

    ArrayList<CrmMeeting> meetingHistoryArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_meeting_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        getFooterDateTime();

        actionbarTitle.setText("Meetings");

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        meetingListRecyclerView.setHasFixedSize(true);
        meetingListLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        meetingListRecyclerView.setLayoutManager(meetingListLayoutManager);

        bindAdapter();

        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

    }

    public void bindAdapter() {
        meetingHistoryArrayList = crmMeetingsTable.getCrmMeetingHistory();
        meetingListAdapter = new MeetingListAdapter(meetingHistoryArrayList,null);
        meetingListRecyclerView.setAdapter(meetingListAdapter);
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        meetingListRecyclerView = findViewById(R.id.rvMeetings);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        crmMeetingsTable = new CrmMeetingsTable(getApplicationContext());

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

    }

    /*
     * Setting the footer date and time of the page.
     */
    public void getFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String date = FileUtils.getMonthYear();
        datetime.setText(date);

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            String dateString = FileUtils.getHourMin();
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException ignored) {
                }
            }
        };
        t.start();

    }

}
