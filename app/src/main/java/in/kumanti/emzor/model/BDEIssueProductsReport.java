package in.kumanti.emzor.model;

public class BDEIssueProductsReport {

    public int bdeIssueProdId;
    public String bdeIssueDate;
    public String bdeIssueNumber;
    public String toBdeName;
    public String productCode;
    public String productName;
    public String issueQuantity;
    public String issuePrice;
    public String issueValue;
    public String createdAt;
    public String updatedAt;

    public int getBdeIssueProdId() {
        return bdeIssueProdId;
    }

    public void setBdeIssueProdId(int bdeIssueProdId) {
        this.bdeIssueProdId = bdeIssueProdId;
    }

    public String getBdeIssueDate() {
        return bdeIssueDate;
    }

    public void setBdeIssueDate(String bdeIssueDate) {
        this.bdeIssueDate = bdeIssueDate;
    }

    public String getBdeIssueNumber() {
        return bdeIssueNumber;
    }

    public void setBdeIssueNumber(String bdeIssueNumber) {
        this.bdeIssueNumber = bdeIssueNumber;
    }

    public String getToBdeName() {
        return toBdeName;
    }

    public void setToBdeName(String toBdeName) {
        this.toBdeName = toBdeName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIssueQuantity() {
        return issueQuantity;
    }

    public void setIssueQuantity(String issueQuantity) {
        this.issueQuantity = issueQuantity;
    }

    public String getIssuePrice() {
        return issuePrice;
    }

    public void setIssuePrice(String issuePrice) {
        this.issuePrice = issuePrice;
    }

    public String getIssueValue() {
        return issueValue;
    }

    public void setIssueValue(String issueValue) {
        this.issueValue = issueValue;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
