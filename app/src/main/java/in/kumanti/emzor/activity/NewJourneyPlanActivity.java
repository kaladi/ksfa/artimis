package in.kumanti.emzor.activity;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.NewJourneyPlanDayAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.NewJourneyPlanCustomerTable;
import in.kumanti.emzor.eloquent.NewJourneyPlanTable;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.NewJourneyPlan;
import in.kumanti.emzor.model.NewJourneyPlanCustomers;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.SharedPreferenceManager;

import static java.util.Calendar.DATE;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.MONDAY;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SUNDAY;
import static java.util.Calendar.WEEK_OF_MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;


public class NewJourneyPlanActivity extends MainActivity {

    public RecyclerView customersRecyclerView;
    public RecyclerView.Adapter customersAdapter;
    public RecyclerView.LayoutManager customersLayoutManager;
    ImageView actionbarBackButton, deviceInfo;
    TextView actionbarTitle, marqueeText;
    SharedPreferenceManager sharedPreferenceManager;
    String login_id = "", checkin_time = "", customerCode = "", bdeCodeString = "", customerNameString = "", journeyPlanIdString = "", journeyPlanDateDB, journeyPlanTime;
//    TextView bdeName, bdeRegion, journeyPlanNumber, journeyPlanDate, journeyStartDate, journeyPlanStatus, saveJpCardViewText;
    TextView bdeName, bdeRegion, journeyPlanNumber, journeyPlanDate, journeyStartDate, journeyPlanStatus;
    CardView mondayCardView, tuesdayCardView, wednesdayCardView, thursdayCardView, fridayCardView, saturdayCardView, sundayCardView;
    ImageButton saveJourneyPlanCardView,cancelJourneyPlanCardView,saveJpCardViewText;
//    CardView mondayCardView, tuesdayCardView, wednesdayCardView, thursdayCardView, fridayCardView, saturdayCardView, sundayCardView, saveJourneyPlanCardView, cancelJourneyPlanCardView;
    String mondayDateDb, tuesdayDateDb, wednesdayDateDb, thursdayDateDb, fridayDateDb, saturdayDateDb;
    MyDBHandler dbHandler;
    ArrayList<ArrayList<NewJourneyPlanCustomers>> customerJourneyDetailsArrayList = new ArrayList<>();
    ArrayList<NewJourneyPlanCustomers> customerDetailsArrayList = new ArrayList<>();
    CustomSearchableSpinner customerNameSpinner;
    ArrayList<NewJourneyPlanCustomers> newCustomerDetailsList;
    RecyclerViewItemListener listener;
    Button addCustomerNameButton;
    ArrayAdapter<NewJourneyPlanCustomers> customerListSpinnerArrayAdapter;
    GeneralSettingsTable generalSettingsTable;
    GeneralSettings gs;
    NewJourneyPlanTable newJourneyPlanTable;
    NewJourneyPlanCustomerTable newJourneyPlanCustomerTable;
    Date journeyDateObj = null;
    Date journeyStartDateObj, journeyEndDateObj;
    ArrayList<HashMap> weekData = new ArrayList<>();
    String[] dayCode = new String[]{"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
    NewJourneyPlan newJourneyPlan;
    private int mYear, mMonth, mDay, selectedWeekOfMonth, selectedDayOfWeek;
    boolean isSavedClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_new_journey_plan);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();
        journeyPlanDate();

        actionbarTitle.setText(R.string.journey_plan_actionbar_title);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);


        bdeName.setText(sr_name_details);
        bdeRegion.setText(sr_region);


        generateJourneyPlanNumber();

        //Initializing Week Data
        weekDataInit();
        updateCardViewUI();

        journeyStartDate.setOnClickListener(v -> {
            final Calendar c = getInstance();
            mYear = c.get(YEAR);
            mMonth = c.get(MONTH);
            mDay = c.get(DAY_OF_MONTH);

            int week = c.get(WEEK_OF_MONTH);
            int day = c.get(DAY_OF_WEEK);
            Log.d("NewJourneyPlan", "--day--" + day + "--week----" + week);


            DatePickerDialog datePickerDialog = new DatePickerDialog(NewJourneyPlanActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        try {

                            if (newJourneyPlan != null) {
                                weekDataInit();
                            }
                            String PATTERN = "dd MMM YY";
                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            journeyDateObj = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(journeyDateObj);
                            journeyStartDate.setText(dateFormat.format(journeyDateObj));
                            dateFormat.applyPattern("yyyy-MM-dd");
                            newJourneyPlan = newJourneyPlanTable.getJourneyPlanByDate(dateFormat.format(journeyDateObj));

                            if (newJourneyPlan != null) {

                                if (newJourneyPlan.getJourneyPlanStatus().equals("Rejected")) {
//                                    saveJpCardViewText.setText("NEW");
                                    saveJpCardViewText.setVisibility(View.VISIBLE);
                                    saveJourneyPlanCardView.setVisibility(View.VISIBLE);
                                    cancelJourneyPlanCardView.setVisibility(View.VISIBLE);

                                } else {
                                    saveJourneyPlanCardView.setVisibility(View.INVISIBLE);
                                    cancelJourneyPlanCardView.setVisibility(View.INVISIBLE);
                                }
                                customerJourneyDetailsArrayList = new ArrayList<>();
                                weekData = new ArrayList<>();

                                for (int i = 0; i < 7; i++) {
                                    HashMap<String, String> dayData = new HashMap<>();
                                    dayData.put("date", "");
                                    dayData.put("isRequired", "FALSE");
                                    dayData.put("isDisabled", "FALSE");
                                    weekData.add(dayData);
                                    ArrayList<NewJourneyPlanCustomers> njpc = newJourneyPlanCustomerTable.getJourneyPlanCustomersByDate(newJourneyPlan.journeyPlanNumber, dayCode[i]);
                                    customerJourneyDetailsArrayList.add(njpc);
                                }

                                journeyPlanNumber.setText(newJourneyPlan.journeyPlanNumber);
                                journeyPlanStatus.setText(newJourneyPlan.journeyPlanStatus);
                            } else {

//                                saveJpCardViewText.setText(R.string.new_journey_plan_save_cardview_text);
                                saveJpCardViewText.setVisibility(View.GONE);
                                saveJourneyPlanCardView.setVisibility(View.VISIBLE);
                                cancelJourneyPlanCardView.setVisibility(View.VISIBLE);

                                journeyPlanNumber.setText(journeyPlanIdString);
                                journeyPlanStatus.setText(R.string.new_journey_plan_status);
                            }

                            final Calendar c1 = getInstance();
                            c1.setTime(journeyDateObj);
                            c1.setFirstDayOfWeek(MONDAY);

                            selectedWeekOfMonth = c1.get(WEEK_OF_MONTH);
                            selectedDayOfWeek = c1.get(DAY_OF_WEEK) - 1;

                            selectedDayOfWeek = selectedDayOfWeek == 0 ? 7 : selectedDayOfWeek;
                            journeyStartDateObj = getWeekStartDate(journeyDateObj);
                            journeyEndDateObj = getWeekEndDate(journeyDateObj);
                            Log.d("PlanPrint", "--selectedDayOfWeek--" + selectedDayOfWeek + "--selectedWeekOfMonth----" + selectedWeekOfMonth + "--" + dateFormat.format(journeyStartDateObj) + "---" + dateFormat.format(journeyEndDateObj));
                            //weekDataInit();
                            updateCardViewUI();
                        } catch (Exception e) {
                            Log.d("PlanPrint", "Error" + e.getMessage());

                        }

                    }, mDay, mMonth, mYear);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.getDatePicker().setFirstDayOfWeek(MONDAY);
            }

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });


        mondayCardView.setOnClickListener(v -> {
            if (weekData.get(0).get("isDisabled").equals("TRUE"))
                return;

            showDayCustomersListPopup("MONDAY", customerJourneyDetailsArrayList.get(0),0);
        });


        tuesdayCardView.setOnClickListener(v -> {
            if (weekData.get(1).get("isDisabled").equals("TRUE"))
                return;

            showDayCustomersListPopup("TUESDAY", customerJourneyDetailsArrayList.get(1),1);

        });


        wednesdayCardView.setOnClickListener(v -> {
            if (weekData.get(2).get("isDisabled").equals("TRUE"))
                return;

            showDayCustomersListPopup("WEDNESDAY", customerJourneyDetailsArrayList.get(2),2);
        });

        thursdayCardView.setOnClickListener(v -> {
            if (weekData.get(3).get("isDisabled").equals("TRUE"))
                return;

            showDayCustomersListPopup("THURSDAY", customerJourneyDetailsArrayList.get(3),3);

        });

        fridayCardView.setOnClickListener(v -> {
            if (weekData.get(4).get("isDisabled").equals("TRUE"))
                return;

            showDayCustomersListPopup("FRIDAY", customerJourneyDetailsArrayList.get(4),4);
        });

        saturdayCardView.setOnClickListener(v -> {
            if (weekData.get(5).get("isDisabled").equals("TRUE"))
                return;

            showDayCustomersListPopup("SATURDAY", customerJourneyDetailsArrayList.get(5),5);
        });

        sundayCardView.setOnClickListener(v -> {
            if (weekData.get(6).get("isDisabled").equals("TRUE"))
                return;

            showDayCustomersListPopup("SUNDAY", customerJourneyDetailsArrayList.get(6),6);
        });

        saveJourneyPlanCardView.setOnClickListener(v -> {
           /* if(NewJourneyPlanActivity.this.newJourneyPlan !=null)
            {
                return;
            }*/
            isSavedClicked = true;

            String PATTERN = "yyyy-MM-dd";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern(PATTERN);
            //Toast.makeText(getApplicationContext(),"Save",Toast.LENGTH_LONG).show();
            String visitSequence = sharedPreferences.getString("VisitSequence", "");

            if (journeyDateObj == null) {
                Toast.makeText(getApplicationContext(), "Please select journey start date", Toast.LENGTH_SHORT).show();
                return;
            }
            int validationStartDate = journeyStartDay();

            //if condition fails then issue from the date
            if (!validateDaysFrom(validationStartDate)) {
                updateCardViewUI();
                // Toast.makeText(getApplicationContext(),"Required data from "+validationStartDate,Toast.LENGTH_SHORT).show();
                return;
            }


            final Dialog alertbox = new Dialog(v.getRootView().getContext());

            LayoutInflater mInflater = (LayoutInflater)
                    getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            final View alertLayout = mInflater.inflate(R.layout.journey_plan_save_confirmation, null);

            alertbox.setCancelable(false);
            alertbox.setContentView(alertLayout);

            final Button Yes = alertLayout.findViewById(R.id.jpYesButton);
            final Button No = alertLayout.findViewById(R.id.jpNoButton);

            Yes.setOnClickListener(v1 -> {


                Calendar calendar = getInstance();
                calendar.setTime(journeyStartDateObj);
                NewJourneyPlan newJourneyPlan = new NewJourneyPlan();
                newJourneyPlan.setBdeCode(bdeCodeString);
                newJourneyPlan.setBdeName(bdeName.getText().toString());
                newJourneyPlan.setBdeRegion(bdeRegion.getText().toString());
                newJourneyPlan.setJourneyStartDate(dateFormat.format(journeyDateObj));
                newJourneyPlan.setJourneyPlanDate(journeyPlanDateDB);

                if (validationStartDate <= 0) {
                    String formattedDate = dateFormat.format(calendar.getTime());
                    weekData.get(0).put("date", formattedDate);
                    newJourneyPlan.setMonday(formattedDate);
                }
                calendar.add(DATE, 1);

                if (validationStartDate <= 1) {
                    String formattedDate = dateFormat.format(calendar.getTime());
                    weekData.get(1).put("date", formattedDate);
                    newJourneyPlan.setTuesday(formattedDate);
                }
                calendar.add(DATE, 1);

                if (validationStartDate <= 2) {
                    String formattedDate = dateFormat.format(calendar.getTime());
                    weekData.get(2).put("date", formattedDate);
                    newJourneyPlan.setWednesday(formattedDate);
                }
                calendar.add(DATE, 1);

                if (validationStartDate <= 3) {
                    String formattedDate = dateFormat.format(calendar.getTime());
                    weekData.get(3).put("date", formattedDate);
                    newJourneyPlan.setThursday(formattedDate);
                }
                calendar.add(DATE, 1);

                if (validationStartDate <= 4) {
                    String formattedDate = dateFormat.format(calendar.getTime());
                    weekData.get(4).put("date", formattedDate);
                    newJourneyPlan.setFriday(formattedDate);
                }
                calendar.add(DATE, 1);

                if (validationStartDate <= 5) {
                    String formattedDate = dateFormat.format(calendar.getTime());
                    weekData.get(5).put("date", formattedDate);
                    newJourneyPlan.setSaturday(formattedDate);
                }
                calendar.add(DATE, 1);

                if (validationStartDate <= 6) {
                    String formattedDate = dateFormat.format(calendar.getTime());
                    weekData.get(6).put("date", formattedDate);
                    newJourneyPlan.setSunday(formattedDate);
                }

                newJourneyPlan.setVisitSequenceNumber(visitSequence);
                newJourneyPlan.setJourneyPlanStatus("Pending");
                newJourneyPlan.setJourneyPlanNumber(journeyPlanIdString);
                newJourneyPlan.setRandomNumber(random_num);

                long planid = newJourneyPlanTable.create(newJourneyPlan);
                Log.d("PlanPrint", "plantid" + planid);
                int i = 0;

                for (ArrayList<NewJourneyPlanCustomers> customersArrayList : customerJourneyDetailsArrayList) {
                    for (NewJourneyPlanCustomers jpc : customersArrayList) {
                        jpc.setJourneyPlanNumber(journeyPlanIdString);
                        jpc.setDayCode(dayCode[i]);
                        jpc.setCurrentDayDate(weekData.get(i).get("date").toString());
                        newJourneyPlanCustomerTable.create(jpc);
                    }
                    i++;
                }
                Toast.makeText(getApplicationContext(), "Journey Plan Created", Toast.LENGTH_LONG).show();

                finish();
            });

            No.setOnClickListener(v12 -> alertbox.dismiss());
            alertbox.show();
        });

        cancelJourneyPlanCardView.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

        actionbarBackButton.setOnClickListener(v -> finish());
    }


    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public boolean validateDaysFrom(int startDay) {
        boolean flag = true;
        for (int i = startDay; i < 7; i++) {

            flag = validateDay(flag,i);
        }


        return flag;
    }
    public boolean validateDay(boolean flag,int i){
        boolean validate = true;
        if (i == 5 || i == 6) {
            String key = i == 5 ? "enableJpSaturday" : "enableJpSunday";
            gs = generalSettingsTable.getSettingByKey(key);
            validate = gs != null && gs.getValue().equals("Yes");
        }

        if (validate) {
            if (customerJourneyDetailsArrayList.get(i).size() == 0) {
                weekData.get(i).put("isRequired", "TRUE");
                flag = false;
            } else {
                weekData.get(i).put("isRequired", "FALSE");
            }
        }
        return flag;
    }
    public int journeyStartDay() {
        Date currentDate = getInstance().getTime();
        Date currentStartDate = getWeekStartDate(currentDate);
        Date currentEndDate = getWeekEndDate(currentDate);
        int startDate = 0;
        if (journeyDateObj != null) {
            if (journeyDateObj.compareTo(currentStartDate) >= 0 && journeyDateObj.compareTo(currentEndDate) <= 0) {
                if (journeyDateObj.compareTo(currentStartDate) == 0)
                    startDate = 0;
                else
                    startDate = selectedDayOfWeek - 1;
            }
        } else
            startDate = 7;
        return startDate;
    }

    public void updateCardViewUI() {
        mondayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_white));
        tuesdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_white));
        wednesdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_white));
        thursdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_white));
        fridayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_white));
        saturdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_white));
        sundayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_white));

        int journeyStateDayNum = journeyStartDay();
        if (journeyStateDayNum > 0) {
            for (int i = 0; i < journeyStateDayNum; i++) {
                HashMap k = weekData.get(i);
                k.put("isDisabled", "TRUE");
                if (i == 0)
                    mondayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_grey));
                if (i == 1)
                    tuesdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_grey));
                if (i == 2)
                    wednesdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_grey));
                if (i == 3)
                    thursdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_grey));
                if (i == 4)
                    fridayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_grey));
                if (i == 5)
                    saturdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_grey));
                if (i == 6)
                    sundayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_grey));
            }
        }

        for (int i = journeyStateDayNum; i < 7; i++) {
            HashMap k = weekData.get(i);
            k.put("isDisabled", "FALSE");
            if (k.get("isRequired").equals("TRUE")) {
                if (i == 0)
                    mondayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_red));
                if (i == 1)
                    tuesdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_red));
                if (i == 2)
                    wednesdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_red));
                if (i == 3)
                    thursdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_red));
                if (i == 4)
                    fridayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_red));
                if (i == 5)
                    saturdayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_red));
                if (i == 6)
                    sundayCardView.setCardBackgroundColor(getResources().getColor(R.color.card_red));

                Toast.makeText(getApplicationContext(), "Please Add Customer Name", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void showDayCustomersListPopup(String dayNameString, ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList,int dayPosition) {

        TextView dayName, deleteButtonSpace;
        LinearLayout customerNameContainer;
        boolean isView = false;

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_new_journey_plan_day, null);

        customerNameContainer = alertLayout.findViewById(R.id.customerNameContainer);
        customerNameSpinner = alertLayout.findViewById(R.id.newJpCustomerNameSpinner);
        addCustomerNameButton = alertLayout.findViewById(R.id.newJpCustomerAddButton);
        customersRecyclerView = alertLayout.findViewById(R.id.newJourneyPlanDayCusRecyclerView);
        deleteButtonSpace = alertLayout.findViewById(R.id.deleteButtonSpace);

        if (newJourneyPlan != null) {
            if (newJourneyPlan.getJourneyPlanStatus().equals("Rejected")) {
                customerNameSpinner.setVisibility(View.VISIBLE);
                addCustomerNameButton.setVisibility(View.VISIBLE);
                customerNameContainer.setVisibility(View.VISIBLE);
                deleteButtonSpace.setVisibility(View.VISIBLE);
            } else {
                customerNameSpinner.setVisibility(View.GONE);
                addCustomerNameButton.setVisibility(View.GONE);
                customerNameContainer.setVisibility(View.GONE);
                deleteButtonSpace.setVisibility(View.GONE);
                isView = true;
            }

        }

        dayName = alertLayout.findViewById(R.id.newJourneyPlanDayNameTv);
        dayName.setText(dayNameString);


        AlertDialog.Builder alert = new AlertDialog.Builder(NewJourneyPlanActivity.this);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", (dialog, which) -> {
            Log.d("PlanPrint", "Error Message "+isSavedClicked+" "+dayPosition);
//            int validationStartDate = journeyStartDay();
//            validateDaysFrom(validationStartDate);
            if(isSavedClicked)
                validateDay(true, dayPosition);
            updateCardViewUI();
        });

        customerNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CustomSearchableSpinner.isSpinnerDialogOpen = false;
                NewJourneyPlanCustomers customerDetails = (NewJourneyPlanCustomers) parent.getItemAtPosition(position);
                customerNameString = customerDetails.getCustomerName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                CustomSearchableSpinner.isSpinnerDialogOpen = false;

            }
        });

        updateCustomerSpinner(newJourneyPlanCustomersArrayList);

        customersRecyclerView.setHasFixedSize(true);
        customersLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        customersRecyclerView.setLayoutManager(customersLayoutManager);

        listener = (view, position) -> {
            updateCustomerSpinner(newJourneyPlanCustomersArrayList);
        };

        customersAdapter = new NewJourneyPlanDayAdapter(newJourneyPlanCustomersArrayList, listener, isView);
        customersRecyclerView.setAdapter(customersAdapter);

        addCustomerNameButton.setOnClickListener(v -> addingCustomersToList(newJourneyPlanCustomersArrayList));

        AlertDialog dialog = alert.create();
        dialog.show();


    }

    private void weekDataInit() {
        customerJourneyDetailsArrayList = new ArrayList<>();
        weekData = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            HashMap<String, String> dayData = new HashMap<>();
            dayData.put("date", "");
            dayData.put("isRequired", "FALSE");
            dayData.put("isDisabled", "FALSE");
            weekData.add(dayData);
            customerJourneyDetailsArrayList.add(new ArrayList<>());
        }
    }

    public Date getWeekStartDate(Date checkDate) {
        Calendar calendar = getInstance();
        calendar.setTime(checkDate);

        while (calendar.get(DAY_OF_WEEK) != MONDAY) {
            calendar.add(DATE, -1);
        }

        return calendar.getTime();
    }

    public Date getWeekEndDate(Date checkDate) {
        Calendar calendar = getInstance();
        calendar.setTime(checkDate);

        while (calendar.get(DAY_OF_WEEK) != SUNDAY) {
            calendar.add(DATE, 1);
        }

        calendar.add(DATE, -1);
        return calendar.getTime();
    }

    private void updateCustomerSpinner(ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList) {
        customerDetailsArrayList = dbHandler.getCustomersDetails();

        //Remove the already selected product
        int k = 0;
        newCustomerDetailsList = new ArrayList<>();

        for (NewJourneyPlanCustomers newJourneyPlanCustomers : customerDetailsArrayList) {
            boolean flag = true;
            for (NewJourneyPlanCustomers jpc : newJourneyPlanCustomersArrayList) {
                if (newJourneyPlanCustomers.getCustomerCode().equals(jpc.getCustomerCode())) {
                    flag = false;
                    break;
                }

            }
            if (flag)
                newCustomerDetailsList.add(newJourneyPlanCustomers);
            k++;
        }

        NewJourneyPlanCustomers jpc = new NewJourneyPlanCustomers();
        jpc.setCustomerCode("-1");
        jpc.setCustomerName("Select");
        newCustomerDetailsList.add(0, jpc);

        customerNameSpinner.setTitle("Select a Customer");
        customerNameSpinner.setPositiveButton("OK");
        customerListSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, newCustomerDetailsList);
        customerListSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        customerNameSpinner.setAdapter(customerListSpinnerArrayAdapter);

    }

    /**
     * Adding of Customer Details in Opportunity Recycler View List
     */
    public void addingCustomersToList(ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList) {
        try {
            if (customerNameSpinner.getSelectedItemPosition() == 0) {
                Toast.makeText(getApplicationContext(), "Please choose a Customer before adding to the List", Toast.LENGTH_SHORT).show();
                return;
            }

            NewJourneyPlanCustomers selectedCustomer = newCustomerDetailsList.get(customerNameSpinner.getSelectedItemPosition());
            NewJourneyPlanCustomers jpCus = new NewJourneyPlanCustomers();
            jpCus.setCustomerCode(selectedCustomer.getCustomerCode());
            jpCus.setCustomerName(selectedCustomer.getCustomerName());
            jpCus.setCustomerType(selectedCustomer.getCustomerType());

            newJourneyPlanCustomersArrayList.add(jpCus);
            customersAdapter.notifyDataSetChanged();
            Log.d("Recycle", "Data Added: " + selectedCustomer.getCustomerName());
            updateCustomerSpinner(newJourneyPlanCustomersArrayList);

        } catch (Exception e) {
            Log.d("Expired Response", "" + e.getMessage());
        }

    }


    public void generateJourneyPlanNumber() {
        random_num = newJourneyPlanTable.getJourneyPlanNumber();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        Log.d("Stock Return", "Random No---" + random_num);

        if (random_num <= 9) {
            journeyPlanNumber.setText(tab_code + "JP" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            journeyPlanNumber.setText(tab_code + "JP" + rec_seq_date + "0" + random_num);

        } else {
            journeyPlanNumber.setText(tab_code + "JP" + rec_seq_date + random_num);
        }

        journeyPlanIdString = journeyPlanNumber.getText().toString();

    }


    private void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        journeyPlanNumber = findViewById(R.id.journeyPlanNumberTv);
        journeyPlanDate = findViewById(R.id.journeyPlanDateTv);


        bdeName = findViewById(R.id.journeyPlanBdeNameTv);
        bdeRegion = findViewById(R.id.journeyPlanRegionTv);
        journeyStartDate = findViewById(R.id.journeyPlanStartDateButton);
        journeyPlanStatus = findViewById(R.id.journeyPlanStatusTv);
        mondayCardView = findViewById(R.id.journeyPlanMondayCardView);
        tuesdayCardView = findViewById(R.id.journeyPlanTuesdayCardView);
        wednesdayCardView = findViewById(R.id.journeyPlanWednesdayCardView);
        thursdayCardView = findViewById(R.id.journeyPlanThursdayCardView);
        fridayCardView = findViewById(R.id.journeyPlanFridayCardView);
        saturdayCardView = findViewById(R.id.journeySaturdayCardView);
        sundayCardView = findViewById(R.id.journeyPlanSundayCardView);
        saveJourneyPlanCardView = findViewById(R.id.saveButton);
        cancelJourneyPlanCardView = findViewById(R.id.cancelJpTextView);
        saveJpCardViewText = findViewById(R.id.saveJpTextView);


        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        newJourneyPlanTable = new NewJourneyPlanTable(getApplicationContext());
        newJourneyPlanCustomerTable = new NewJourneyPlanCustomerTable(getApplicationContext());
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());

    }

    public void journeyPlanDate() {
        String PATTERN = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        String PATTERN_DISPLAY = "dd MMM YY";
        SimpleDateFormat dateFormatDisplay = new SimpleDateFormat();
        dateFormatDisplay.applyPattern(PATTERN_DISPLAY);
        String dateDisplay = dateFormatDisplay.format(Calendar.getInstance().getTime());
        journeyPlanDateDB = date;
        journeyPlanDate.setText(dateDisplay);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        journeyPlanTime = sdf.format(outDate);
    }

    private void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");

        }

        getCurrentLoginBDEInfo();
    }

    public void getCurrentLoginBDEInfo() {
        Log.d("Invoice", "Login---" + login_id);
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {

                bdeCodeString = salesRepDetails.getString(1);
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }
    }


    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);

        String date = dateFormat.format(getInstance().getTime());
        datetime.setText(date);

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


}



