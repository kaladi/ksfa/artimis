package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.reports.CollectionReportsActivity;
import in.kumanti.emzor.activity.reports.CompetitorInfoReportsActivity;
import in.kumanti.emzor.activity.reports.CustomerReportsActivity;
import in.kumanti.emzor.activity.reports.CustomerReturnProductsReportsActivity;
import in.kumanti.emzor.activity.reports.EnquiryReportsActivity;
import in.kumanti.emzor.activity.reports.ExpiredProductsReportsActivity;
import in.kumanti.emzor.activity.reports.FeedbackReportActivity;
import in.kumanti.emzor.activity.reports.InvoiceReportsActivity;
import in.kumanti.emzor.activity.reports.MyMessagesActivity;
import in.kumanti.emzor.activity.reports.OrderReportsActivity;
import in.kumanti.emzor.activity.reports.QuotationReportsActivity;
import in.kumanti.emzor.activity.reports.StockIssueProductsReportsActivity;
import in.kumanti.emzor.activity.reports.StockReturnProductsReportsActivity;
import in.kumanti.emzor.activity.reports.SurveyReportsActivity;
import in.kumanti.emzor.activity.reports.WaybillReportsActivity;
import in.kumanti.emzor.adapter.ExpandableListAdapter;
import in.kumanti.emzor.adapter.GPSTrackerService;
import in.kumanti.emzor.adapter.JourneyPlanViewAdapter;
import in.kumanti.emzor.adapter.SalesRepCustomerListAdapter;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.backgroundService.FetchingDataService;
import in.kumanti.emzor.backgroundService.MasterRDLService;
import in.kumanti.emzor.backgroundService.MyService;
import in.kumanti.emzor.backgroundService.PostingDataGPSService;
import in.kumanti.emzor.crm.CrmHistoryActivity;
import in.kumanti.emzor.crm.MeetingHistoryActivity;
import in.kumanti.emzor.eloquent.AgeTable;
import in.kumanti.emzor.eloquent.ArtifactsTable;
import in.kumanti.emzor.eloquent.BDEListTable;
import in.kumanti.emzor.eloquent.CompetitorProductTable;
import in.kumanti.emzor.eloquent.CompetitorStockProductTable;
import in.kumanti.emzor.eloquent.CrmDetailsTable;
import in.kumanti.emzor.eloquent.CrmMeetingsTable;
import in.kumanti.emzor.eloquent.CrmNewProductsTable;
import in.kumanti.emzor.eloquent.CrmProductsTable;
import in.kumanti.emzor.eloquent.CrmStatusTable;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.CustomerReturnProductsTable;
import in.kumanti.emzor.eloquent.CustomerReturnTable;
import in.kumanti.emzor.eloquent.CustomerShippingAddressTable;
import in.kumanti.emzor.eloquent.CustomerStockProductTable;
import in.kumanti.emzor.eloquent.DistributorsTable;
import in.kumanti.emzor.eloquent.DoctorPrescribedProductsTable;
import in.kumanti.emzor.eloquent.DoctorVisitInfoTable;
import in.kumanti.emzor.eloquent.ExpenseTable;
import in.kumanti.emzor.eloquent.ExpenseTypeTable;
import in.kumanti.emzor.eloquent.LedgerTable;
import in.kumanti.emzor.eloquent.LogInOutHistoryTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.NewJourneyPlanCustomerTable;
import in.kumanti.emzor.eloquent.NewJourneyPlanTable;
import in.kumanti.emzor.eloquent.NotesTable;
import in.kumanti.emzor.eloquent.PharmacyInfoProductsTable;
import in.kumanti.emzor.eloquent.PharmacyInfoTable;
import in.kumanti.emzor.eloquent.PrimaryInvoiceProductTable;
import in.kumanti.emzor.eloquent.PrimaryInvoiceTable;
import in.kumanti.emzor.eloquent.RDLSyncTable;
import in.kumanti.emzor.eloquent.StockIssueProductsTable;
import in.kumanti.emzor.eloquent.StockIssueTable;
import in.kumanti.emzor.eloquent.StockReturnProductsTable;
import in.kumanti.emzor.eloquent.StockReturnTable;
import in.kumanti.emzor.eloquent.SurveyQuestionOptionTable;
import in.kumanti.emzor.eloquent.SurveyQuestionsTable;
import in.kumanti.emzor.eloquent.SurveyResponseAnswerTable;
import in.kumanti.emzor.eloquent.SurveyResponseTable;
import in.kumanti.emzor.eloquent.SurveyTable;
import in.kumanti.emzor.eloquent.WarehouseStockProductsTable;
import in.kumanti.emzor.eloquent.WarehouseTable;
import in.kumanti.emzor.mastersData.ActivationFlag;
import in.kumanti.emzor.mastersData.CustomerMaster;
import in.kumanti.emzor.mastersData.FeedbackType;
import in.kumanti.emzor.mastersData.GPSTrackerModel;
import in.kumanti.emzor.mastersData.ItemMaster;
import in.kumanti.emzor.mastersData.JourneyPlanMaster;
import in.kumanti.emzor.mastersData.LocationMaster;
import in.kumanti.emzor.mastersData.MarqueeText;
import in.kumanti.emzor.mastersData.PriceListHeader;
import in.kumanti.emzor.mastersData.PriceListLines;
import in.kumanti.emzor.model.Age;
import in.kumanti.emzor.model.Artifacts;
import in.kumanti.emzor.model.BDEList;
import in.kumanti.emzor.model.CheckInDetails;
import in.kumanti.emzor.model.CollectionDetails;
import in.kumanti.emzor.model.CompetitorProduct;
import in.kumanti.emzor.model.CompetitorStockProduct;
import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.model.CrmColdCall;
import in.kumanti.emzor.model.CrmMeeting;
import in.kumanti.emzor.model.CrmNewProducts;
import in.kumanti.emzor.model.CrmProducts;
import in.kumanti.emzor.model.CrmStatus;
import in.kumanti.emzor.model.CustomerDetails;
import in.kumanti.emzor.model.CustomerReturn;
import in.kumanti.emzor.model.CustomerReturnProducts;
import in.kumanti.emzor.model.CustomerShippingAddress;
import in.kumanti.emzor.model.CustomerStockProduct;
import in.kumanti.emzor.model.Details;
import in.kumanti.emzor.model.Distributors;
import in.kumanti.emzor.model.DoctorPrescribedProducts;
import in.kumanti.emzor.model.DoctorVisitInfo;
import in.kumanti.emzor.model.DoctorVisitSponsorship;
import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.model.ExpenseType;
import in.kumanti.emzor.model.Feedback;
import in.kumanti.emzor.model.GPSDistance;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.InvoiceItems;
import in.kumanti.emzor.model.Invoices;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.Ledger;
import in.kumanti.emzor.model.LogInOut;
import in.kumanti.emzor.model.MDLSync;
import in.kumanti.emzor.model.MasterResponseJson;
import in.kumanti.emzor.model.MetricsTarget;
import in.kumanti.emzor.model.NewJourneyPlan;
import in.kumanti.emzor.model.NewJourneyPlanCustomers;
import in.kumanti.emzor.model.Notes;
import in.kumanti.emzor.model.OrderItems;
import in.kumanti.emzor.model.Orders;
import in.kumanti.emzor.model.PharmacyInfo;
import in.kumanti.emzor.model.PharmacyInfoProducts;
import in.kumanti.emzor.model.PrimaryInvoice;
import in.kumanti.emzor.model.PrimaryInvoiceProduct;
import in.kumanti.emzor.model.RDLSync;
import in.kumanti.emzor.model.ResponseJson;
import in.kumanti.emzor.model.SRPriceList;
import in.kumanti.emzor.model.SalesRep_Customer_List;
import in.kumanti.emzor.model.SrInvoiceBatch;
import in.kumanti.emzor.model.StockIssue;
import in.kumanti.emzor.model.StockIssueProducts;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.model.StockReturn;
import in.kumanti.emzor.model.StockReturnProducts;
import in.kumanti.emzor.model.SurveyHeader;
import in.kumanti.emzor.model.SurveyLines;
import in.kumanti.emzor.model.SurveyResponse;
import in.kumanti.emzor.model.SurveyResponseAnswer;
import in.kumanti.emzor.model.TripDetails;
import in.kumanti.emzor.model.ViewStockWarehouseProduct;
import in.kumanti.emzor.model.WaybillDetails;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.utils.Constants.FTP_HOST_NAME;
import static in.kumanti.emzor.utils.Constants.FTP_PASSWORD;
import static in.kumanti.emzor.utils.Constants.FTP_USER_NAME;
import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class StartStopActivity extends MainActivity {

    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;
    Spinner Customer_Name, Product;
    ImageView start_trip, pick_camera, pick_camera1, sr_image, info, back, img_map;
    Button img_metrics, notesButton;
    EditText mileage;
    MyDBHandler dbHandler;
    String login_id = "", company_code = "";
    TextView salesrep_name, manager, region, gps, warehouse, header_txt, marqueeText, start_time, total_trips;
    LinearLayout plan_details, today_activity, today_trip;
    TableRow plan;
    String startstop = "Start";
    String trip_num, trip_name = null;
    String mileage_val, sr_name_details, sr_rm_name, sr_region, sr_warehouse, nav_home;
    Bitmap thumbnail;
    Globals globals;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat, lng, lat_lng;
    DecimalFormat formatter;
    Bitmap selectedImage = null;
    TextView orderValue, invoiceValue, collectionValue;
    ImageView orderCurveImage, invoiceCurveImage, collectionCurveImage;
    TextView tdate;
    TextView ttime;
    TextView tripno;
    static int sync_data = 0;
    static int sync_data1 = 0;
    DrawerLayout drawer;
    String user_role;
    SharedPreferences sharedPreferences;
    SharedPreferenceManager preferenceManager;
    SharedPreferences.Editor editor;
    String visitSeqNumber;
    // BDEListTable bdeListTable;
    ImageView bdeImageView;
    Handler masterDownloadHandler;
    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    private boolean hasImageChanged = false;
    String postSyncDate, postSyncTime;
    RDLSyncTable rdlSyncTable;


    private Handler handler1;
    ProgressDialog dialog;
    Handler receiveHandler;
    private Context contextForDialog = null;

    String  companyCode = "";
    ArrayList<Orders> ordersArrayList1;
    ArrayList<CollectionDetails> collectionPrArraylist1;
    ArrayList<CollectionDetails> collectionSecArraylist1;
    ArrayList<NewJourneyPlan> newJourneyPlanArrayList1;
    NewJourneyPlanTable newJourneyPlanTable1;

    ArrayList<Expense> expenseArrayList1;
    ExpenseTable expenseTable1;

    public static void hideSoftKeyboardEditTextView(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_start_stop);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        formatter = new DecimalFormat("####.00");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        toggle.syncState();
        setDateTime();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        contextForDialog = this;
        initializeViews();
        populate_header_details();
        if ("User".equals(userrole)) {
            MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
            int line_count = dbHandler.getmaster_count();

            if (line_count == 0) {
                if (sync_data1 == 0) {

                    boolean networkConnected = isNetworkConnected();
                    boolean networking = isInternetAvailable();
                    if (networkConnected == true) {
//                        dialog = new ProgressDialog(contextForDialog);
//                        dialog.setMessage("Master is downloading,Please Wait!!");
//                        dialog.setCancelable(false);
//                        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                        dialog.show();
//                        new Thread() {
//                            public void run() {
                        setProgressBar();
                        sync_data1 = 1;
                        Toast.makeText(getApplicationContext(), "Master is downloading,Please Wait....", Toast.LENGTH_LONG).show();
                        dbHandler.deleteMaster();
                        fetch_data1();
//
//                            }
//                        }.start();
//
//                        handler1 = new Handler() {
//                            public void handleMessage(android.os.Message msg) {
//                                dialog.dismiss();
//                            }
//                        };
                    } else {
                        Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();

                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Master is still downloading,Please Wait....", Toast.LENGTH_LONG).show();

                }
            }
        }
        prepareListData();
        showexpandablelistView();
        today_trip_details();
        distance_travelled();

        populateTodayActivity(orderValue, invoiceValue, collectionValue, orderCurveImage, invoiceCurveImage, collectionCurveImage);

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        pick_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ImageTest", "asdf");
                if (selectedImage != null) {
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.image_preview, null);
                    ImageView capturedImage, closeButton;
                    capturedImage = alertLayout.findViewById(R.id.capturedImage);
                    closeButton = alertLayout.findViewById(R.id.closeImageView);
                    capturedImage.setImageBitmap(selectedImage);
                    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    // this is set the view from XML inside AlertDialog

                    alert.setView(alertLayout);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(true);

                    android.app.AlertDialog dialog = alert.create();
                    dialog.show();


                }
            }
        });

        start_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (trip_num != null) {
                    start_stop(v);
                } else {
                    if (sync_data == 1) {
                        Toast.makeText(getApplicationContext(), "Master is still downloading,Please Wait....", Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (startstop.equals("Start")) {

                        Cursor getlatestmessage = dbHandler.get_latest_message();
                        int numRows1 = getlatestmessage.getCount();
                        if (numRows1 == 0) {
                            // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
                        } else {
                            int i = 0;
                            while (getlatestmessage.moveToNext()) {
                                messageText = getlatestmessage.getString(0);

                            }
                        }

                        editor = sharedPreferences.edit();
                        editor.putString("MarqueeText", messageText);
                        editor.commit();
                        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
                        marqueeText.setText(marquee_txt);

                        String mileage_data = dbHandler.get_mileage_data();

                        if ("Yes".equals(mileage_data)) {

                            int km = 0;

                            if (("").equals(mileage.getText().toString())) {

                            } else {
                                mileage_val = mileage.getText().toString();
                                globals.setMileage(mileage_val);
                                km = dbHandler.getMileage(save_trip_date);
                                mileage1 = Integer.parseInt(mileage_val);
                            }

                            if ("".equals(mileage.getText().toString())) {
                                mileage.setError("Please Enter Mileage");
                            } else if (mileage1 <= km) {
                                mileage.setError("Enter Mileage greater than : " + km);
                            } else {
                                after_populate_journey_plans();
                                start_stop(v);
                                //Toast.makeText(getApplicationContext(), "KM" + String.valueOf(km) + "Mileage" + mileage_val, Toast.LENGTH_LONG).show();
                            }

                        } else {
                            start_stop(v);
                        }

                    } else {
                        start_stop(v);
                    }
                }
            }
        });

        pick_camera1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("".equals(mileage.getText().toString())) {

                } else {
                    if ("Stop".equals(startstop)) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, CAPTURE_PHOTO);
                    } else {

                    }
                }

            }
        });

        img_metrics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(getApplicationContext(), MetricsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("login_id", login_id);
                myintent.putExtras(bundle);
                startActivity(myintent);

            }
        });

        btnNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(getApplicationContext(), NotesActivity.class);

                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("login_id", login_id);
                bundle.putString("notesType", "SALES_REP");
                bundle.putString("notesReferenceNo", sr_code);
                //Add the bundle to the intent
                myintent.putExtras(bundle);
                //Fire that second activity
                startActivity(myintent);
            }
        });

        img_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("login_id", login_id);
                bundle.putString("nav_type", "SRLanding");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        btnJP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.activity_journey_plan, null);
                AlertDialog.Builder alert = new AlertDialog.Builder(StartStopActivity.this);
                img_map = alertLayout.findViewById(R.id.img_map);
                //PopupWindow  pwindow = new PopupWindow(alertLayout, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT,true);

                alert.setView(alertLayout);
                alert.setCancelable(false);

                img_map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("login_id", login_id);
                        bundle.putString("nav_type", "SRLanding");
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });

                //Working with List to Show Data's
                userList = new ArrayList<SalesRep_Customer_List>();
                Cursor data = dbHandler.getSalesrepCustomers(login_id, tripno1);
                numRows1 = data.getCount();
                if (numRows1 == 0) {
                    // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
                } else {
                    int i = 0;
                    while (data.moveToNext()) {

                        serial_num = serial_num + 1;
                        user = new SalesRep_Customer_List(data.getString(1), data.getString(2), data.getString(3), data.getString(0), data.getString(4), data.getString(5));
                        userList.add(i, user);
                        //System.out.println(userList.get(i).getFirstName());
                        i++;

                    }

                    journeyPlanViewAdapter = new JourneyPlanViewAdapter(getApplicationContext(), R.layout.list_customer_start_stop, userList, login_id, tripno1);
                    ListView customerTable1 = alertLayout.findViewById(R.id.customer_list);
                    customerTable1.setAdapter(journeyPlanViewAdapter);
                    serial_num = 0;
                }
                alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = alert.create();
                dialog.show();

            }
        });

    }

    private void showexpandablelistView() {
        boolean flag = true;
        if ("User".equals(userrole)) {
            flag = false;
        }

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList, flag);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                String header_val = headerList.get(groupPosition);
                if ("Customer List".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), CustomerReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }  else if ("Meetings".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), MeetingHistoryActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("CRM List".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), CrmHistoryActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }else if ("Journey Plan".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), NewJourneyPlanActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                }
                else if ("View Stock".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), ViewStockActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Stock Receipt".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), StockReceiptActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Stock Adjustment".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), StockAdjustmentActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Stock Return".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), StockReturnActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Stock Issue".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), StockIssueActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                }
                else if ("Expense Booking".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), ExpenseSummaryActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Device Info".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("My Messages".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), MyMessagesActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }
//                else if ("Sync Data".equals(header_val)) {
//                    if (sync_data == 0) {
//
//                        boolean networkConnected = isNetworkConnected();
//                        boolean networking = isInternetAvailable();
//                        if (networkConnected == true) {
//                            sync_data = 1;
//                            Toast.makeText(getApplicationContext(), "Master is downloading,Please Wait....", Toast.LENGTH_LONG).show();
//                            dbHandler.deleteMaster();
//                            fetch_data();
//
//
//                            postingDbThread = new Thread(new Runnable() {
//
//                                @Override
//                                public void run() {
//                                    Log.d("Multi Thread DB*****", "Starting T------1.");
//                                    postingDatabaseToFtp();
//                                    Log.d("Multi Thread DB*****", "Finishing T-----1.");
//                                }
//                            });
//
//                            postingDbThread.start();
//
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();
//
//                        }
//
//                    } else {
//
//                    }
//
//
//                }
                else if ("Logout".equals(header_val)) {


                    final Dialog alertbox = new Dialog(v.getRootView().getContext());

                    LayoutInflater mInflater = (LayoutInflater)
                            getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    final View alertLayout = mInflater.inflate(R.layout.customer_logout_save_confirmation, null);

                    alertbox.setCancelable(false);
                    alertbox.setContentView(alertLayout);

                    final Button Yes = alertLayout.findViewById(R.id.logoutYesButton);
                    final Button No = alertLayout.findViewById(R.id.logoutNoButton);

                    Yes.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {
                            alertbox.dismiss();

                            Toast.makeText(getApplicationContext(), "Logout", Toast.LENGTH_LONG).show();
                            String logInOutId = sharedPreferences.getString("logInOutId", "");
                            if (!TextUtils.isEmpty(logInOutId)) {
                                LogInOut logInOut = new LogInOut();
                                logInOut.setLogOutDate(logOutDate);
                                logInOut.setLogOutTime(logOutTime);
                                Log.d("logInOutId", "Logout " + logInOutId);
                                logInOutHistoryTable.updaterLogout(logInOut, logInOutId);
                            }

                            Intent intent = new Intent(getApplicationContext(), StartUpActivity.class);
                            globals.setLogin_email("");
                            globals.setLogin_id("");
                            stopBackgroundService();

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            //  intent.putExtra("login_id", login_id);
                            startActivity(intent);

                        }
                    });

                    No.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            alertbox.dismiss();
                        }

                    });

                    alertbox.show();

                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                String child = childList.get(
                        headerList.get(groupPosition)).get(
                        childPosition);

                if ("Orders".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), OrderReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Invoices".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), InvoiceReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Collections".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CollectionReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }else if ("Meetings".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), MeetingHistoryActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("CRM List".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CrmHistoryActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Enquiry".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), EnquiryReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }
                else if ("Quotation".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), QuotationReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }
                else if ("Waybill".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), WaybillReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } /*else if ("Opportunities".equals(child)) {

                } */ else if ("Survey".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), SurveyReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Feedback".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), FeedbackReportActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Competitor Info".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CompetitorInfoReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Expired Products".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), ExpiredProductsReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Customer Return Products".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CustomerReturnProductsReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Stock Return Products".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), StockReturnProductsReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Stock Issue Products".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), StockIssueProductsReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }
               /* else if ("Learning Stats".equals(child)) {

                }*/
                else if ("Stock Receipt".equals(child)) {

                } else if ("Stock Ledger".equals(child)) {

                }else if ("Import Master Data".equals(child)) {
                    if (sync_data == 0) {

                        boolean networkConnected = isNetworkConnected();
                        boolean networking = isInternetAvailable();
                        if (networkConnected == true) {
                            sync_data = 1;
                            Toast.makeText(getApplicationContext(), "Master is downloading,Please Wait....", Toast.LENGTH_LONG).show();
                            dbHandler.deleteMaster();
                            fetch_data();
                            importRDLData();
                            login_api1();

//                            postingDbThread = new Thread(new Runnable() {
//
//                                @Override
//                                public void run() {
//                                    Log.d("Multi Thread DB*****", "Starting T------1.");
//                                    postingDatabaseToFtp();
//                                    Log.d("Multi Thread DB*****", "Finishing T-----1.");
//                                }
//                            });
//
//                            postingDbThread.start();

                        } else {
                            Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Master is still downloading,Please Wait....", Toast.LENGTH_LONG).show();

                    }

                }else if ("Export Transactions".equals(child)) {
                    boolean isNetworkAvail = isNetworkConnected();

                    if(isNetworkAvail) {
                        postingOrderDetailsToWeb();
                    } else {
                        Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();
                    }
                }
                else if ("Post Images".equals(child)) {
                    boolean isNetworkAvail = isNetworkConnected();

                    if (isNetworkAvail) {
                        dialog = new ProgressDialog(contextForDialog);
                        dialog.setMessage("Processing , Please Wait!!");
                        dialog.setCancelable(false);
                        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        dialog.show();
                        receiveHandler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                dialog.dismiss();
                                if (msg.what == 1) {//
                                    Toast.makeText(getApplicationContext(), "Images Posted Successfully", Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(getApplicationContext(), "Unable to process your transaction.", Toast.LENGTH_LONG).show();

                                }


                            }
                        };

                        Thread receiveThread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                if (postFtp())
                                    receiveHandler.sendEmptyMessage(1);
                                else
                                    receiveHandler.sendEmptyMessage(0);
                            }
                        });


                        receiveThread.start();



                    } else {
                        Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();
                    }
                } else if ("DB Backup".equals(child)) {
                    boolean isNetworkAvail = isNetworkConnected();

                    if (isNetworkAvail) {
                        dialog = new ProgressDialog(contextForDialog);
                        dialog.setMessage("Processing , Please Wait!!");
                        dialog.setCancelable(false);
                        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        dialog.show();
                        receiveHandler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                dialog.dismiss();
                                if (msg.what == 1) {//
                                    Toast.makeText(getApplicationContext(), "Database Posted Successfully", Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(getApplicationContext(), "Unable to process your transaction.", Toast.LENGTH_LONG).show();

                                }


                            }
                        };

                        Thread receiveThread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                if (postingDBData())
                                    receiveHandler.sendEmptyMessage(1);
                                else
                                    receiveHandler.sendEmptyMessage(0);
                            }
                        });


                        receiveThread.start();



                    } else {
                        Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();
                    }
                }else if ("Tab Setup".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), TabSetupActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);
                } else if ("User Setup".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), UserSetupActivity.class);
                    //intent.putExtra("login_id", login_id);
                    startActivity(intent);
                } else if ("Company Setup".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CompanySetupActivity.class);
                    //intent.putExtra("login_id", login_id);
                    startActivity(intent);
                } else if ("Change Password".equals(child)) {
                    // Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                    // intent.putExtra("login_id", login_id);
                    // startActivity(intent);
                }
                return false;
            }
        });

    }

    boolean postingDBData(){
        String sourcePath1 = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/KSFA/dbBackup";
        File source1 = new File(sourcePath1);
//                        boolean deleted = source.delete();
        File[] files = source1.listFiles();
        if(files != null)
            for(File file : files) {
                file.delete();
            }
//        backupDatabase();
//        postingDatabaseFtp(login_id);
        System.out.println("TTT::m1login_id = " + login_id);
//        Toast.makeText(getApplicationContext(), "Database Posted Successfully", Toast.LENGTH_LONG).show();

        String sourcePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/ksfaDB.DB";
        File source = new File(sourcePath);

        File backupDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/KSFA/", "dbBackup");
        backupDir.mkdir();
      /*  SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String date = timeStampFormat.format(myDate);*/

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_hh_mm");
        String dateString = sdf.format(date);
//        datetime.setText(dateString);
        System.out.println("TTT1::dateString = " + dateString);

        String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/dbBackup/" + tab_code + "_" + login_id + "_" + dateString + "_";
        try {
            File f = new File(destinationPath + "ksfaDB.DB");
            if (f.exists() && !f.isDirectory()) {
                return false;
            }
            File temp = File.createTempFile(destinationPath, "ksfaDB.DB");

            boolean exists = temp.exists();

            if (exists) {
                return exists;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        destinationPath += "ksfaDB.DB";

        File copied = new File(destinationPath);
        try (
                InputStream in = new BufferedInputStream(
                        new FileInputStream(source));
                OutputStream out = new BufferedOutputStream(
                        new FileOutputStream(copied))) {

            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        FTPClient con = null;
        con = new FTPClient();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll()
                .build();
        StrictMode.setThreadPolicy(policy);
        try {
            con.connect(FTP_HOST_NAME);
            if (con.login(FTP_USER_NAME, FTP_PASSWORD)) {
                Log.d("FileUpload", "FilePath Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d("FileUpload", "FilePath Connected");
     /*  SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
       Date myDate = new Date();
       String date = timeStampFormat.format(myDate);*/
        long date1 = System.currentTimeMillis();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd_hh_mm");
        String dateString1 = sdf1.format(date1);
//        datetime.setText(dateString);
        System.out.println("TTT33::dateString = " + dateString);

        String destinationPath1 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/dbBackup/" + tab_code + "_" + login_id + "_" + dateString1 + "_";


        destinationPath1 += "ksfaDB.DB";

        String file_name = tab_code + "_" + login_id + "_" + dateString1 + "_" + "ksfaDB.DB";
        Log.d("FileUpload", "FileName--" + file_name);
        System.out.println("TTT::file_name2 = " + file_name);
        try {
            String data = Environment.getExternalStorageDirectory() + File.separator
                    + "KSFA" + File.separator + "ksfaDB.DB";
            Log.d("FileUpload", "FilePath " + data);

            FileInputStream in = new FileInputStream(new File(destinationPath1));
            String serverPath = "/home/vistagftp/vistagftp/Db/" + file_name;
            boolean result = con.storeFile(serverPath, in);
            Log.d("FileUpload", "File Result--" + result);
            System.out.println("TTT::result2 = " + result);
            in.close();

            if (result) {
                Log.d("FileUpload", "Success");
                return true;
            }


            con.logout();
            con.disconnect();

            Log.d("FileUpload", "File Connection Closed--");


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Database Upload Error" + e.getMessage());
        }

        return false;
    }
    void login_api1(){
      /*  boolean networkConnected = isNetworkConnected();
        boolean networking = isInternetAvailable();
        if (networkConnected == true) {*/

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.ACTIVATION_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit.create(ApiInterface.class);
        String company_code = dbHandler.get_company_code();
        Call<ActivationFlag> validatedLogin = api.getActivationFlag(company_code);

        System.out.println("validatedLogin1 = " + validatedLogin);
        validatedLogin.enqueue(new Callback<ActivationFlag>() {

            @Override
            public void onResponse(Call<ActivationFlag> validatedLogin, retrofit2.Response<ActivationFlag> response) {
                System.out.println("validatedLogin2 = " + response);
                ActivationFlag mLoginObject = response.body();
                System.out.println("mLoginObject = " + response.body());
                System.out.println("mLoginObject.status = " + mLoginObject.flag);
                String returnedResponse = mLoginObject.flag;

                if ("N".equals(returnedResponse)) {
                    int flag = 0;
                    dbHandler.update_tab_flag(flag);
                    if ("User".equals(userrole)) {
                        //Toast.makeText(getApplicationContext(), "Logout", Toast.LENGTH_LONG).show();
                        Log.d("Testing---", "LogOut Date---" + logOutDate);
                        Log.d("Testing---", "LogOut Time---" + logOutTime);
                        LogInOut logInOut = new LogInOut();
                        logInOut.setLogOutDate(logOutDate);
                        logInOut.setLogOutTime(logOutTime);
                        String logInOutId = sharedPreferences.getString("logInOutId", "");
                        Log.d("logInOutId", "Logout " + logInOutId);
                        logInOutHistoryTable.updaterLogout(logInOut, logInOutId);
                    }
                    Intent intent = new Intent(getApplicationContext(), StartUpActivity.class);
                    globals.setLogin_email("");
                    globals.setLogin_id("");
                    intent.putExtra("user_role", userrole);
                    stopBackgroundService();

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //  intent.putExtra("login_id", login_id);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Please Contact Admin to Activate your login", Toast.LENGTH_SHORT).show();

                }
                if ("Y".equals(returnedResponse)) {
                    int flag = 1;
                    dbHandler.update_tab_flag(flag);
//                        Toast.makeText(getApplicationContext(), "Activation Successfully", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<ActivationFlag> call, Throwable t) {
                System.out.println("validatedLogin4 = " + validatedLogin);
                System.out.println("t.getMessage() = " + t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

//        }
//        else {
//            Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();

//        }
    }

    private void postingGpsData() {

        Gson gson = new GsonBuilder().setLenient().create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL).
                        client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the Gson ConverterFactory to directly convert json data to object
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        //Fetching the orders
        ArrayList<GPSTrackerModel> gpsTrackerModelArrayList = dbHandler.getGPSDataService();
        JsonArray gpsJsonArray = gson.toJsonTree(gpsTrackerModelArrayList).getAsJsonArray();

        /*
         * Adding the all Json Array of objects into a Json Object
         */
        JsonObject jp = new JsonObject();
        jp.add("gpsDetails", gpsJsonArray);

        Log.i("onResponse: POSTGP-----", "" + jp.toString());

        try {
            //Inserting the Json object into the Api
            Call<ResponseBody> call = api.sendGPSdata(jp);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    String[] gpsIds = new String[gpsTrackerModelArrayList.size()];
                    int i = 0;
                    for (GPSTrackerModel oh : gpsTrackerModelArrayList) {
                        gpsIds[i] = oh.getGps_id();
                        i++;
                        Log.i("onResponse: Data--", "oh " + oh.getGps_id());
                    }
                    dbHandler.updateGPSSync(gpsIds);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("onResponse: failure----", "" + t);
                    //Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });

            Log.i("MyTestService", "Service running");
        } catch (Exception e) {
            Log.i("MyTestService", "Exception GPS");

        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public boolean isInternetAvailable() {

        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;

    }

    private void fetch_data1() {

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        int line_count = dbHandler.get_count_master_download();

        if (line_count == 0) {

            customerShippingAddressTable = new CustomerShippingAddressTable(getApplicationContext());
            competitorProductTable = new CompetitorProductTable(getApplicationContext());
            surveyTable = new SurveyTable(getApplicationContext());
            surveyQuestionsTable = new SurveyQuestionsTable(getApplicationContext());
            surveyQuestionOptionTable = new SurveyQuestionOptionTable(getApplicationContext());
            warehouseStockProductsTable = new WarehouseStockProductsTable(getApplicationContext());
            ledgerTable = new LedgerTable(getApplicationContext());
            ageTable = new AgeTable(getApplicationContext());
            warehouseTable = new WarehouseTable(getApplicationContext());
            primaryInvoiceTable = new PrimaryInvoiceTable(getApplicationContext());
            primaryInvoiceProductTable = new PrimaryInvoiceProductTable(getApplicationContext());
            distributorsTable = new DistributorsTable(getApplicationContext());
            artifactsTable = new ArtifactsTable(getApplicationContext());
            bdeListTable = new BDEListTable(getApplicationContext());
            expenseTable = new ExpenseTable(getApplicationContext());
            expenseTypeTable = new ExpenseTypeTable(getApplicationContext());


            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(500000, TimeUnit.SECONDS).readTimeout(500000, TimeUnit.SECONDS).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);

            Call<MasterResponseJson> customerMaster = api.getMasterData(login_id, company_code);
            System.out.println("AAA::login_id123 = " + login_id);

            customerMaster.enqueue(new Callback<MasterResponseJson>() {

                @Override
                public void onResponse(Call<MasterResponseJson> customerMaster, retrofit2.Response<MasterResponseJson> response) {

                    MasterResponseJson resultSet = response.body();

                    if (resultSet.status.equals("true")) {

                        customerMasterList = new Gson().fromJson(resultSet.customer, new TypeToken<ArrayList<CustomerMaster>>() {
                        }.getType());

                        itemMastersList = new Gson().fromJson(resultSet.Product, new TypeToken<ArrayList<ItemMaster>>() {
                        }.getType());

                        competitor_productList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CompetitorProduct>>() {
                        }.getType());

                        price_headerList = new Gson().fromJson(resultSet.price_header, new TypeToken<ArrayList<PriceListHeader>>() {
                        }.getType());

                        price_lineList = new Gson().fromJson(resultSet.price_line, new TypeToken<ArrayList<PriceListLines>>() {
                        }.getType());

                        shipping_addressList = new Gson().fromJson(resultSet.shipping_address, new TypeToken<ArrayList<CustomerShippingAddress>>() {
                        }.getType());

                        journeyplanList = new Gson().fromJson(resultSet.journey_plan, new TypeToken<ArrayList<JourneyPlanMaster>>() {
                        }.getType());

                        warehouseProductsList = new Gson().fromJson(resultSet.warehouse_stock, new TypeToken<ArrayList<ViewStockWarehouseProduct>>() {
                        }.getType());

                        ledgerList = new Gson().fromJson(resultSet.customer_ledgerr, new TypeToken<ArrayList<Ledger>>() {
                        }.getType());

                        ageList = new Gson().fromJson(resultSet.customer_aging, new TypeToken<ArrayList<Age>>() {
                        }.getType());

                        feedbackList = new Gson().fromJson(resultSet.feedback, new TypeToken<ArrayList<FeedbackType>>() {
                        }.getType());

                        marqueeTextList = new Gson().fromJson(resultSet.message, new TypeToken<ArrayList<MarqueeText>>() {
                        }.getType());

                        locationList = new Gson().fromJson(resultSet.location, new TypeToken<ArrayList<LocationMaster>>() {
                        }.getType());

                        primaryInvoiceList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoice>>() {
                        }.getType());

                        srInvoiceBatchList = new Gson().fromJson(resultSet.sr_invoice_batch, new TypeToken<ArrayList<SrInvoiceBatch>>() {
                        }.getType());

                        primaryInvoiceProductsList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoiceProduct>>() {
                        }.getType());


                        coldCallArrayList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CrmColdCall>>() {
                        }.getType());

                        distributorsArrayList = new Gson().fromJson(resultSet.distributors, new TypeToken<ArrayList<Distributors>>() {
                        }.getType());

                        srPriceListArrayList = new Gson().fromJson(resultSet.sr_pricelist, new TypeToken<ArrayList<SRPriceList>>() {
                        }.getType());

                        settingsArrayList = new Gson().fromJson(resultSet.settings, new TypeToken<ArrayList<GeneralSettings>>() {
                        }.getType());

                        artifactsArrayList = new Gson().fromJson(resultSet.artifact, new TypeToken<ArrayList<Artifacts>>() {
                        }.getType());

                        surveyArrayList = new Gson().fromJson(resultSet.survey_header, new TypeToken<ArrayList<SurveyHeader>>() {
                        }.getType());

                        surveyLinesArrayList = new Gson().fromJson(resultSet.survey_line, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        surveyOptionsArrayList = new Gson().fromJson(resultSet.survey_option, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        bdeListArrayList = new Gson().fromJson(resultSet.sr_master_bde, new TypeToken<ArrayList<BDEList>>() {
                        }.getType());

                        metricsTargetList = new Gson().fromJson(resultSet.target, new TypeToken<ArrayList<MetricsTarget>>() {
                        }.getType());

                        expenseTypeList = new Gson().fromJson(resultSet.expense_type, new TypeToken<ArrayList<ExpenseType>>() {
                        }.getType());


                        deletingMasterData();

                        masterDownloadHandler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                if (msg.what == 1) {//
                                    Toast.makeText(getApplicationContext(), "Master Download has been Completed", Toast.LENGTH_SHORT).show();
                                    // Log.d("Multi", "Start Stop setIsDownloaded After Downloaded--" +mdlSync.getIsDownloaded());

                                } else {
                                    Toast.makeText(getApplicationContext(), "Master Data is not downloaded", Toast.LENGTH_SHORT).show();

                                }

                            }
                        };

                        thread1 = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                Log.d("Multi Thread*****", "Starting T------1.");
                                insertFirstData();
                                insertSecondData();
                                insertThirdData();
                                insertFourthData();
                                insertFifthData();
                                insertSixthData();
                                insertSeventhData();
                                Log.d("Multi Thread*****", "Finishing T------1.");

                                generateMDLSyncDateTime();
                                MDLSync mdlSync = new MDLSync();
                                mdlSync.setSyncDate(mdlSyncDate);
                                mdlSync.setSyncTime(mdlSyncTime);
                                dbHandler.createMasterDownload(mdlSync);

                                mdlSync.setIsDownloaded("Done");
                                dbHandler.createMasterDownload(mdlSync);

                                mdlSync.setIsDownloaded("Done");
                                dbHandler.createMasterDownload(mdlSync);
                                if (mdlSync.getIsDownloaded().equals("Done"))
                                    masterDownloadHandler.sendEmptyMessage(1);
                                else
                                    masterDownloadHandler.sendEmptyMessage(0);


                            }
                        });


                        thread1.start();

                        sync_data1 = 0;


                        Log.d("Multi Thread*****", "Data Downloaded-----.");


                    }
                }

                @Override
                public void onFailure(Call<MasterResponseJson> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    sync_data1 = 0;

                }
            });


//            postingOrderDetailsToWeb();

        } else {

        }
    }

    private void fetch_data() {

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        int line_count = dbHandler.get_count_master_download();

        if (line_count == 0) {

            customerShippingAddressTable = new CustomerShippingAddressTable(getApplicationContext());
            competitorProductTable = new CompetitorProductTable(getApplicationContext());
            surveyTable = new SurveyTable(getApplicationContext());
            surveyQuestionsTable = new SurveyQuestionsTable(getApplicationContext());
            surveyQuestionOptionTable = new SurveyQuestionOptionTable(getApplicationContext());
            warehouseStockProductsTable = new WarehouseStockProductsTable(getApplicationContext());
            ledgerTable = new LedgerTable(getApplicationContext());
            ageTable = new AgeTable(getApplicationContext());
            warehouseTable = new WarehouseTable(getApplicationContext());
            primaryInvoiceTable = new PrimaryInvoiceTable(getApplicationContext());
            primaryInvoiceProductTable = new PrimaryInvoiceProductTable(getApplicationContext());
            distributorsTable = new DistributorsTable(getApplicationContext());
            artifactsTable = new ArtifactsTable(getApplicationContext());
            bdeListTable = new BDEListTable(getApplicationContext());
            expenseTable = new ExpenseTable(getApplicationContext());
            expenseTypeTable = new ExpenseTypeTable(getApplicationContext());


            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(500000, TimeUnit.SECONDS).readTimeout(500000, TimeUnit.SECONDS).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);

            Call<MasterResponseJson> customerMaster = api.getMasterData(login_id, company_code);
            System.out.println("AAA::login_id12 = " + login_id);


            customerMaster.enqueue(new Callback<MasterResponseJson>() {

                @Override
                public void onResponse(Call<MasterResponseJson> customerMaster, retrofit2.Response<MasterResponseJson> response) {

                    MasterResponseJson resultSet = response.body();

                    if (resultSet.status.equals("true")) {

                        customerMasterList = new Gson().fromJson(resultSet.customer, new TypeToken<ArrayList<CustomerMaster>>() {
                        }.getType());

                        itemMastersList = new Gson().fromJson(resultSet.Product, new TypeToken<ArrayList<ItemMaster>>() {
                        }.getType());

                        competitor_productList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CompetitorProduct>>() {
                        }.getType());

                        price_headerList = new Gson().fromJson(resultSet.price_header, new TypeToken<ArrayList<PriceListHeader>>() {
                        }.getType());

                        price_lineList = new Gson().fromJson(resultSet.price_line, new TypeToken<ArrayList<PriceListLines>>() {
                        }.getType());

                        shipping_addressList = new Gson().fromJson(resultSet.shipping_address, new TypeToken<ArrayList<CustomerShippingAddress>>() {
                        }.getType());

                        journeyplanList = new Gson().fromJson(resultSet.journey_plan, new TypeToken<ArrayList<JourneyPlanMaster>>() {
                        }.getType());

                        warehouseProductsList = new Gson().fromJson(resultSet.warehouse_stock, new TypeToken<ArrayList<ViewStockWarehouseProduct>>() {
                        }.getType());

                        ledgerList = new Gson().fromJson(resultSet.customer_ledgerr, new TypeToken<ArrayList<Ledger>>() {
                        }.getType());

                        ageList = new Gson().fromJson(resultSet.customer_aging, new TypeToken<ArrayList<Age>>() {
                        }.getType());

                        feedbackList = new Gson().fromJson(resultSet.feedback, new TypeToken<ArrayList<FeedbackType>>() {
                        }.getType());

                        marqueeTextList = new Gson().fromJson(resultSet.message, new TypeToken<ArrayList<MarqueeText>>() {
                        }.getType());

                        locationList = new Gson().fromJson(resultSet.location, new TypeToken<ArrayList<LocationMaster>>() {
                        }.getType());

                        primaryInvoiceList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoice>>() {
                        }.getType());

                        srInvoiceBatchList = new Gson().fromJson(resultSet.sr_invoice_batch, new TypeToken<ArrayList<SrInvoiceBatch>>() {
                        }.getType());

                        primaryInvoiceProductsList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoiceProduct>>() {
                        }.getType());


                        coldCallArrayList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CrmColdCall>>() {
                        }.getType());

                        distributorsArrayList = new Gson().fromJson(resultSet.distributors, new TypeToken<ArrayList<Distributors>>() {
                        }.getType());

                        srPriceListArrayList = new Gson().fromJson(resultSet.sr_pricelist, new TypeToken<ArrayList<SRPriceList>>() {
                        }.getType());

                        settingsArrayList = new Gson().fromJson(resultSet.settings, new TypeToken<ArrayList<GeneralSettings>>() {
                        }.getType());

                        artifactsArrayList = new Gson().fromJson(resultSet.artifact, new TypeToken<ArrayList<Artifacts>>() {
                        }.getType());

                        surveyArrayList = new Gson().fromJson(resultSet.survey_header, new TypeToken<ArrayList<SurveyHeader>>() {
                        }.getType());

                        surveyLinesArrayList = new Gson().fromJson(resultSet.survey_line, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        surveyOptionsArrayList = new Gson().fromJson(resultSet.survey_option, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        bdeListArrayList = new Gson().fromJson(resultSet.sr_master_bde, new TypeToken<ArrayList<BDEList>>() {
                        }.getType());

                        metricsTargetList = new Gson().fromJson(resultSet.target, new TypeToken<ArrayList<MetricsTarget>>() {
                        }.getType());

                        expenseTypeList = new Gson().fromJson(resultSet.expense_type, new TypeToken<ArrayList<ExpenseType>>() {
                        }.getType());


                        deletingMasterData();

                        masterDownloadHandler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                if (msg.what == 1) {//
                                    Toast.makeText(getApplicationContext(), "Master Download has been Completed", Toast.LENGTH_SHORT).show();
                                    // Log.d("Multi", "Start Stop setIsDownloaded After Downloaded--" +mdlSync.getIsDownloaded());

                                } else {
                                    Toast.makeText(getApplicationContext(), "Master Data is not downloaded", Toast.LENGTH_SHORT).show();

                                }

                            }
                        };

                        thread1 = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                Log.d("Multi Thread*****", "Starting T------1.");
                                insertFirstData();
                                insertSecondData();
                                insertThirdData();
                                insertFourthData();
                                insertFifthData();
                                insertSixthData();
                                insertSeventhData();
                                Log.d("Multi Thread*****", "Finishing T------1.");

                                generateMDLSyncDateTime();
                                MDLSync mdlSync = new MDLSync();
                                mdlSync.setSyncDate(mdlSyncDate);
                                mdlSync.setSyncTime(mdlSyncTime);
                                dbHandler.createMasterDownload(mdlSync);

                                mdlSync.setIsDownloaded("Done");
                                dbHandler.createMasterDownload(mdlSync);

                                mdlSync.setIsDownloaded("Done");
                                dbHandler.createMasterDownload(mdlSync);
                                if (mdlSync.getIsDownloaded().equals("Done"))
                                    masterDownloadHandler.sendEmptyMessage(1);
                                else
                                    masterDownloadHandler.sendEmptyMessage(0);


                            }
                        });


                        thread1.start();

                        sync_data = 0;


                        Log.d("Multi Thread*****", "Data Downloaded-----.");


                    }
                }

                @Override
                public void onFailure(Call<MasterResponseJson> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    sync_data = 0;

                }
            });


//            postingOrderDetailsToWeb();

        } else {

        }
    }

    private void postingOrderDetailsToWeb() {

        gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(500000, TimeUnit.SECONDS).readTimeout(500000, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);


        try {

            //Fetching the orders
            ArrayList<Orders> ordersArrayList = dbHandler.getOrdersService();
            JsonArray ordersJsonArray = gson.toJsonTree(ordersArrayList).getAsJsonArray();

            //Fetching the order items
            ArrayList<OrderItems> orderItemsArrayList = dbHandler.getOrderItemsService();
            JsonArray orderItemsJsonArray = gson.toJsonTree(orderItemsArrayList).getAsJsonArray();



            //Fetching the invoices
            ArrayList<Invoices> invoicesArrayList = dbHandler.getInvoicesService();
            JsonArray invoicesJsonArray = gson.toJsonTree(invoicesArrayList).getAsJsonArray();

            //Fetching the invoice items
            ArrayList<InvoiceItems> invoiceItemsArrayList = dbHandler.getInvoiceItemsService();
            JsonArray invoiceItemsJsonArray = gson.toJsonTree(invoiceItemsArrayList).getAsJsonArray();

            //Fetching Primary Collection Details
            ArrayList<CollectionDetails> primaryCollectionDetailsArrayList = dbHandler.getPrimaryCollectionsService();
            JsonArray primaryCollectionsJsonArray = gson.toJsonTree(primaryCollectionDetailsArrayList).getAsJsonArray();

            //Fetching Secondary Collections
            ArrayList<CollectionDetails> secondaryCollectionDetailsArrayList = dbHandler.getSecondaryCollectionsService();
            JsonArray secondaryCollectionsJsonArray = gson.toJsonTree(secondaryCollectionDetailsArrayList).getAsJsonArray();

            //Fetching Stock Receipts
            ArrayList<StockReceipt> stockReceiptArrayList = dbHandler.getStockReceiptService();
            JsonArray stockReceiptsJsonArray = gson.toJsonTree(stockReceiptArrayList).getAsJsonArray();

            //Fetching Stock Receipt Batches
            ArrayList<StockReceiptBatch> stockReceiptBatchArrayList = dbHandler.getStockReceiptBatchService();
            JsonArray stockReceiptBatchesJsonArray = gson.toJsonTree(stockReceiptBatchArrayList).getAsJsonArray();

            //Fetching New customer details
            ArrayList<CustomerDetails> customerDetailsArrayList = dbHandler.getCustomerDetailsService();
            JsonArray customerDetailsJsonArray = gson.toJsonTree(customerDetailsArrayList).getAsJsonArray();

            //Fetching Competitor Stock Products
            CompetitorStockProductTable pct = new CompetitorStockProductTable(getApplicationContext());
            ArrayList<CompetitorStockProduct> competitorStockProductArrayList = pct.getAllCompetitorStockProductsForSync();
            JsonArray competitorStockProductsJsonArray = gson.toJsonTree(competitorStockProductArrayList).getAsJsonArray();

            //Fetching Customer Stock Products
            CustomerStockProductTable sp = new CustomerStockProductTable(getApplicationContext());
            ArrayList<CustomerStockProduct> customerStockProductArrayList = sp.getAllCustomerStockProductForSync();
            JsonArray customerStockProductsJsonArray = gson.toJsonTree(customerStockProductArrayList).getAsJsonArray();


            //Fetching Survey Response
            SurveyResponseTable responseTable = new SurveyResponseTable(getApplicationContext());
            ArrayList<SurveyResponse> surveyResponseArrayList = responseTable.getAllSurveyResponseForSync();
            JsonArray surveyResponseJsonArray = gson.toJsonTree(surveyResponseArrayList).getAsJsonArray();

            //Fetching Survey Answer Response
            SurveyResponseAnswerTable responseAnswerTable = new SurveyResponseAnswerTable(getApplicationContext());
            ArrayList<SurveyResponseAnswer> surveyResponseAnswerArrayList = responseAnswerTable.getAllSurveyResponseAnswerForSync();
            JsonArray surveyResponseAnswerJsonArray = gson.toJsonTree(surveyResponseAnswerArrayList).getAsJsonArray();


            //Fetching Feedback data
            ArrayList<Feedback> feedbackArrayList = dbHandler.getFeedbackService();
            JsonArray feedbackJsonArray = gson.toJsonTree(feedbackArrayList).getAsJsonArray();

            //Fetching Trip Details data
            ArrayList<TripDetails> tripDetailsArrayList = dbHandler.getTripDetailsService();
            JsonArray tripDetailsJsonArray = gson.toJsonTree(tripDetailsArrayList).getAsJsonArray();

            //Fetching Check In data
            ArrayList<CheckInDetails> checkInDetailsArrayList = dbHandler.getCheckInDetailsService();
            JsonArray checkInDetailsJsonArray = gson.toJsonTree(checkInDetailsArrayList).getAsJsonArray();

            //Fetching Doctor Visit Info
            DoctorVisitInfoTable doctorVisitInfoTable = new DoctorVisitInfoTable(getApplicationContext());
            ArrayList<DoctorVisitInfo> doctorVisitInfoArrayList = doctorVisitInfoTable.getDoctorVisitInfo();
            JsonArray doctorVisitInfoJsonArray = gson.toJsonTree(doctorVisitInfoArrayList).getAsJsonArray();

            //Fetching Doctor Visit Prescribed Info
            DoctorPrescribedProductsTable doctorPrescribedProductsTable = new DoctorPrescribedProductsTable(getApplicationContext());
            ArrayList<DoctorPrescribedProducts> doctorPrescribedProductsArrayList = doctorPrescribedProductsTable.getDoctorPrescribedProducts();
            JsonArray doctorPrescribedProductsJsonArray = gson.toJsonTree(doctorPrescribedProductsArrayList).getAsJsonArray();

            //Fetching Pharmacy Info
            PharmacyInfoTable pharmacyInfoTable = new PharmacyInfoTable(getApplicationContext());
            ArrayList<PharmacyInfo> pharmacyInfoArrayList = pharmacyInfoTable.getPharmacyInfo();
            JsonArray pharmacyInfoArrayJsonArray = gson.toJsonTree(pharmacyInfoArrayList).getAsJsonArray();

            //Fetching Pharmacy Info Products
            PharmacyInfoProductsTable pharmacyInfoProductsTable = new PharmacyInfoProductsTable(getApplicationContext());
            ArrayList<PharmacyInfoProducts> pharmacyInfoProductsArrayList = pharmacyInfoProductsTable.getPharmacyInfoProducts();
            JsonArray pharmacyInfoProductsJsonArray = gson.toJsonTree(pharmacyInfoProductsArrayList).getAsJsonArray();

            //Fetching Check In data
            ArrayList<DoctorVisitSponsorship> doctorVisitSponsorshipArrayList = dbHandler.getDoctorVisitSponsorship();
            JsonArray doctorVisitSponsorshipJsonArray = gson.toJsonTree(doctorVisitSponsorshipArrayList).getAsJsonArray();

            //Fetching Notes
            NotesTable notesTable = new NotesTable(getApplicationContext());
            ArrayList<Notes> notesArrayList = notesTable.getNotesData();
            JsonArray notesJsonArray = gson.toJsonTree(notesArrayList).getAsJsonArray();

            //Fetching the wayBillDetails
            ArrayList<WaybillDetails> waybillDetailsArrayList = dbHandler.getWaybillDetails();
            JsonArray WaybillDetailsJsonArray = gson.toJsonTree(waybillDetailsArrayList).getAsJsonArray();

            //Fetching Stock Return
            StockReturnTable stockReturnTable = new StockReturnTable(getApplicationContext());
            ArrayList<StockReturn> stockReturnInfoArrayList = stockReturnTable.getStockReturnDetails();
            JsonArray stockReturnInfoJsonArray = gson.toJsonTree(stockReturnInfoArrayList).getAsJsonArray();

            //Fetching Stock Return Products
            StockReturnProductsTable stockReturnProductsTable = new StockReturnProductsTable(getApplicationContext());
            ArrayList<StockReturnProducts> stockReturnProductsInfoArrayList = stockReturnProductsTable.getStockReturnDetails();
            JsonArray stockReturnProductsInfoJsonArray = gson.toJsonTree(stockReturnProductsInfoArrayList).getAsJsonArray();

            //Fetching Stock Return Products
            LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
            ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLPOImageDetails();
            JsonArray lpoImagesInfoJsonArray = gson.toJsonTree(lpoImagesArrayList).getAsJsonArray();

            //Fetching Stock Return Products
            LogInOutHistoryTable logInOutHistoryTable = new LogInOutHistoryTable(getApplicationContext());
            ArrayList<LogInOut> logInOutArrayList = logInOutHistoryTable.getLoginOutHistory();
            JsonArray logInOutInfoJsonArray = gson.toJsonTree(logInOutArrayList).getAsJsonArray();


            //Fetching Customer Stock Return Products
            CustomerReturnTable customerReturnTable = new CustomerReturnTable(getApplicationContext());
            ArrayList<CustomerReturn> customerReturnArrayList = customerReturnTable.getCustomerReturnDetailsForSync();
            JsonArray customerReturnJsonArray = gson.toJsonTree(customerReturnArrayList).getAsJsonArray();


            //Fetching Customer Stock Return Products
            CustomerReturnProductsTable customerReturnProductsTable = new CustomerReturnProductsTable(getApplicationContext());
            ArrayList<CustomerReturnProducts> customerReturnProductsArrayList = customerReturnProductsTable.getCustomerReturnProductsDetailsForSync();
            JsonArray customerReturnProductsJsonArray = gson.toJsonTree(customerReturnProductsArrayList).getAsJsonArray();


            //Posting Journey Plan Details
            NewJourneyPlanTable newJourneyPlanTable = new NewJourneyPlanTable(getApplicationContext());
            ArrayList<NewJourneyPlan> newJourneyPlanArrayList = newJourneyPlanTable.getJourneyPlanDetailsForSync();
            JsonArray newJourneyPlanJsonArray = gson.toJsonTree(newJourneyPlanArrayList).getAsJsonArray();


            //Posting Journey Plan Customer Details
            NewJourneyPlanCustomerTable newJourneyPlanCustomerTable = new NewJourneyPlanCustomerTable(getApplicationContext());
            ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList = newJourneyPlanCustomerTable.getJourneyPlanCustomersDetailsForSync();
            JsonArray newJourneyPlanCustomersJsonArray = gson.toJsonTree(newJourneyPlanCustomersArrayList).getAsJsonArray();

            //Posting Stock Issue
            StockIssueTable stockIssueTable = new StockIssueTable(getApplicationContext());
            ArrayList<StockIssue> stockIssueArrayList = stockIssueTable.getStockIssueDetailsForSync();
            JsonArray stockIssueJsonArray = gson.toJsonTree(stockIssueArrayList).getAsJsonArray();

            //Posting Stock Issue
            StockIssueProductsTable stockIssueProductsTable = new StockIssueProductsTable(getApplicationContext());
            ArrayList<StockIssueProducts> stockIssueProductsArrayList = stockIssueProductsTable.getStockIssueProductsDetailsForSync();
            JsonArray stockIssueProductsJsonArray = gson.toJsonTree(stockIssueProductsArrayList).getAsJsonArray();

            //Details Info
            CrmDetailsTable crmDetailsTable = new CrmDetailsTable(getApplicationContext());
            ArrayList<Details> crmDetailsArrayList = crmDetailsTable.getDetailsForSync();
            JsonArray crmDetailsJsonArray = gson.toJsonTree(crmDetailsArrayList).getAsJsonArray();

            //Crm Header Info
            CrmTable crmTable = new CrmTable(getApplicationContext());
            ArrayList<Crm> crmArrayList = crmTable.getCrmForSync();
            JsonArray crmJsonArray = gson.toJsonTree(crmArrayList).getAsJsonArray();

            //Crm Products Info
            CrmProductsTable crmProductsTable = new CrmProductsTable(getApplicationContext());
            ArrayList<CrmProducts> crmProductsArrayList = crmProductsTable.getCrmProductsForSync();
            JsonArray crmProductsJsonArray = gson.toJsonTree(crmProductsArrayList).getAsJsonArray();

            //Crm New Products Info
            CrmNewProductsTable crmNewProductsTable = new CrmNewProductsTable(getApplicationContext());
            ArrayList<CrmNewProducts> crmNewProductsArrayList = crmNewProductsTable.getCrmNewProductsForSync();
            JsonArray crmNewProductsJsonArray = gson.toJsonTree(crmNewProductsArrayList).getAsJsonArray();

            //Crm Meetings Info
            CrmMeetingsTable crmMeetingsTable = new CrmMeetingsTable(getApplicationContext());
            ArrayList<CrmMeeting> crmMeetingArrayList = crmMeetingsTable.getCrmMeetingsForSync();
            JsonArray crmMeetingsJsonArray = gson.toJsonTree(crmMeetingArrayList).getAsJsonArray();

            //Crm Status Info
            CrmStatusTable crmStatusTable = new CrmStatusTable(getApplicationContext());
            ArrayList<CrmStatus> crmStatusArrayList = crmStatusTable.getCrmStatusForSync();
            JsonArray crmStatusJsonArray = gson.toJsonTree(crmStatusArrayList).getAsJsonArray();

            ExpenseTable expenseTable = new ExpenseTable(getApplicationContext());
            ArrayList<Expense> expenseArrayList = expenseTable.getAllExpense();
            JsonArray expenseJsonArray = gson.toJsonTree(expenseArrayList).getAsJsonArray();

//            if (ordersArrayList.size() != 0 && orderItemsArrayList.size() != 0) {
            /*
             * Adding the all Json Array of objects into a Json Object
             */
            JsonObject jp = new JsonObject();
            jp.add("orders", ordersJsonArray);
            jp.add("orderItems", orderItemsJsonArray);
            jp.add("invoices", invoicesJsonArray);
            jp.add("invoiceItems", invoiceItemsJsonArray);
            jp.add("primaryCollections", primaryCollectionsJsonArray);
            jp.add("secondaryCollections", secondaryCollectionsJsonArray);
            jp.add("stockReceipts", stockReceiptsJsonArray);
            jp.add("stockReceiptBatches", stockReceiptBatchesJsonArray);
            jp.add("competitorStockProducts", competitorStockProductsJsonArray);
            jp.add("customerStockProducts", customerStockProductsJsonArray);
            jp.add("customerDetails", customerDetailsJsonArray);
            jp.add("surveys", surveyResponseJsonArray);
            jp.add("surveyResponseAnswers", surveyResponseAnswerJsonArray);
            jp.add("feedback", feedbackJsonArray);
            jp.add("tripDetails", tripDetailsJsonArray);
            jp.add("planVisitDetails", checkInDetailsJsonArray);
            jp.add("pharmacyInfo", pharmacyInfoArrayJsonArray);
            jp.add("pharmacyInfoProducts", pharmacyInfoProductsJsonArray);
            jp.add("doctorVisitPatientsInfo", doctorVisitInfoJsonArray);
            jp.add("doctorVisitPrescriptionProducts", doctorPrescribedProductsJsonArray);
            jp.add("sponsorShipDetails", doctorVisitSponsorshipJsonArray);
            jp.add("notesDetails", notesJsonArray);
            jp.add("waybillDetails", WaybillDetailsJsonArray);
            jp.add("stockReturn", stockReturnInfoJsonArray);
            jp.add("stockReturnProducts", stockReturnProductsInfoJsonArray);
            jp.add("lpoImages", lpoImagesInfoJsonArray);
            jp.add("loginLogout", logInOutInfoJsonArray);
            jp.add("customerReturn", customerReturnJsonArray);
            jp.add("customerReturnProducts", customerReturnProductsJsonArray);
            jp.add("newJourneyPlan", newJourneyPlanJsonArray);
            jp.add("newJourneyPlanCustomers", newJourneyPlanCustomersJsonArray);
            jp.add("stockIssue", stockIssueJsonArray);
            jp.add("stockIssueProducts", stockIssueProductsJsonArray);

            jp.add("details", crmDetailsJsonArray);
            jp.add("crm", crmJsonArray);
            jp.add("crmProducts", crmProductsJsonArray);
            jp.add("crmNewProducts", crmNewProductsJsonArray);
            jp.add("crmMeeting", crmMeetingsJsonArray);
            jp.add("crmStatus", crmStatusJsonArray);

            jp.add("expenseJsonArray", expenseJsonArray);
            System.out.println("expenseJsonArray = " + expenseJsonArray);

            System.out.println("jp.toString() = " + jp.toString());
            System.out.println("invoicesJsonArray = " + invoicesJsonArray);
            System.out.println("invoiceItemsJsonArray = " + invoiceItemsJsonArray);
            Log.i("SyncButton: POST-----", "" + jp.toString());

            //Inserting the Json object into the Api
            Call<ResponseJson> call = api.insertData(jp);

            call.enqueue(new Callback<ResponseJson>() {
                @Override
                public void onResponse(Call<ResponseJson> call, Response<ResponseJson> response) {

                    ResponseJson resultSet = response.body();

                    Log.i("SyncButton: POST--", "SUCCESS" + jp.toString());
                    System.out.println("SyncButton: POST--SUCCESS = " + jp.toString());
                    System.out.println("TTT1::expenseJsonArray = " + expenseJsonArray);
                    if (resultSet != null && resultSet.status.equals("true")) {
                        System.out.println("TTT11111::expenseJsonArray = " + expenseJsonArray);
                        String[] orderIds = new String[ordersArrayList.size()];
                        int i = 0;
                        for (Orders oh : ordersArrayList) {
                            orderIds[i] = oh.getOrderCode();
                            i++;
                            // Log.d("SyncButton: POST-----", "oh " + oh.getOrderCode());
                        }
                        dbHandler.updateOrdersSync(orderIds);
                        dbHandler.updateOrderItemsSync(orderIds);

                        String[] invoiceIds = new String[invoicesArrayList.size()];
                        i = 0;
                        for (Invoices oh : invoicesArrayList) {
                            invoiceIds[i] = oh.getInvoiceCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getInvoiceCode());
                        }
                        dbHandler.updateInvoicesSync(invoiceIds);
                        dbHandler.updateInvoiceItemsSync(invoiceIds);


                        String[] primaryCollectionIds = new String[primaryCollectionDetailsArrayList.size()];
                        i = 0;
                        for (CollectionDetails oh : primaryCollectionDetailsArrayList) {
                            primaryCollectionIds[i] = oh.getCollectionCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getCollectionCode());
                        }
                        dbHandler.updatePrimaryCollectionsSync(primaryCollectionIds);

                        String[] secondaryCollectionIds = new String[secondaryCollectionDetailsArrayList.size()];
                        i = 0;
                        for (CollectionDetails oh : secondaryCollectionDetailsArrayList) {
                            secondaryCollectionIds[i] = oh.getCollectionCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getCollectionCode());
                        }
                        dbHandler.updateSecondaryCollectionsSync(secondaryCollectionIds);

                        String[] customerIds = new String[customerDetailsArrayList.size()];
                        i = 0;
                        for (CustomerDetails oh : customerDetailsArrayList) {
                            customerIds[i] = oh.getCustomerCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getCustomerCode());
                        }
                        dbHandler.updateCustomerDetailsSync(customerIds);

                        String[] stockReceiptIds = new String[stockReceiptArrayList.size()];
                        i = 0;
                        for (StockReceipt oh : stockReceiptArrayList) {
                            stockReceiptIds[i] = oh.getStockReceiptCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getStockReceiptCode());
                        }
                        dbHandler.updateStockReceiptsSync(stockReceiptIds);

                        String[] stockReceiptBatchIds = new String[stockReceiptBatchArrayList.size()];
                        i = 0;
                        for (StockReceiptBatch oh : stockReceiptBatchArrayList) {
                            stockReceiptBatchIds[i] = oh.getStockReceiptCode();
                            i++;
                            // Log.d("onResponse: Data--", "oh " + oh.getStockReceiptCode());
                        }
                        dbHandler.updateStockReceiptBatchesSync(stockReceiptBatchIds);

                        String[] competitorStockId = new String[competitorStockProductArrayList.size()];
                        i = 0;
                        for (CompetitorStockProduct oh : competitorStockProductArrayList) {
                            competitorStockId[i] = oh.getCompetitor_stock_id();
                            i++;
                            // Log.d("onResponse: Data--", "oh " + oh.getCompetitor_stock_id());
                        }
                        dbHandler.updateCompetitorStockProductsSync(competitorStockId);

                        String[] customerStockId = new String[customerStockProductArrayList.size()];
                        i = 0;
                        for (CustomerStockProduct oh : customerStockProductArrayList) {
                            customerStockId[i] = oh.getProduct_id();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getProduct_id());
                        }
                        dbHandler.updateCustomerStockProductsSync(customerStockId);


                        String[] surveyId = new String[surveyResponseArrayList.size()];
                        i = 0;
                        for (SurveyResponse oh : surveyResponseArrayList) {
                            surveyId[i] = oh.getSurvey_id();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getSurvey_id());
                        }
                        dbHandler.updateSurveyResponseSync(surveyId);


                        String[] surveyResponseAnswerId = new String[surveyResponseAnswerArrayList.size()];
                        i = 0;
                        for (SurveyResponseAnswer oh : surveyResponseAnswerArrayList) {
                            surveyResponseAnswerId[i] = oh.getSurvey_response_answer_id();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getSurvey_response_answer_id());
                        }
                        dbHandler.updateSurveyResponseAnswerSync(surveyResponseAnswerId);

//                        postingImagesToFTP();

                        generatePostSyncDateTime();
                        RDLSync rdlSync = new RDLSync();
                        rdlSync.setPostSyncDate(postSyncDate);
                        rdlSync.setPostSyncTime(postSyncTime);
                        rdlSyncTable = new RDLSyncTable(getApplicationContext());
                        rdlSyncTable.create(rdlSync);

                            Toast.makeText(getApplicationContext(), "Data Posted Successfully", Toast.LENGTH_LONG).show();

                        }
                    System.out.println("FTpppppppp = ");
//                    FTPClient con = null;
//                    try {
//                        System.out.println("Enterrrrr = ");
//                        LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
//                        ArrayList<TripDetails> tripDetailsImageArrayList = dbHandler.getTripDetailsImageService();
//                        ArrayList<CustomerDetails> customerDetailsImageArrayList = dbHandler.getCustomerDetailsImageService();
//                        ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLpoImageService();
////                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
////                                .permitAll()
////                                .build();
////                        StrictMode.setThreadPolicy(policy);
//                        Log.d("FileUpload", "FilePath");
//
//                        System.out.println("FileUploadddddddd = ");
//
//                        if (tripDetailsImageArrayList.size() != 0 || customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
//                            con = new FTPClient();
//                            con.connect(Constants.FTP_HOST_NAME);
//                            Log.d("FileUpload", "FilePath Conneted");
//
//                            System.out.println("FilePath Conneteddddddddd = ");
//
//                            if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {
//
//                                System.out.println("AAA:::con = ");
//                                Log.d("FileUpload", "FilePath Login");
//                                con.enterLocalPassiveMode(); // important!
//                                con.setFileType(FTP.BINARY_FILE_TYPE);
//                                for (TripDetails tp : tripDetailsImageArrayList) {
//                                    if (tp.getOdometerMileage() != null) {
//                                        try {
//                                            System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);
//
//                                            String data = Environment.getExternalStorageDirectory() + File.separator
//                                                    + "KSFA" + File.separator + "images" + File.separator + tp.getOdometerMileage();
//                                            Log.d("FileUpload", "FilePath " + data);
//                                            FileInputStream in = new FileInputStream(new File(data));
//                                            boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getOdometerMileage(), in);
//
//                                            System.out.println("AAA:::result " + result);
//
//                                            in.close();
//                                            if (result) {
//                                                System.out.println("AAA:::FileUpload::::Success " + result);
//                                                dbHandler.updateTripMileageUploadFlag(tp.getTripNumber(), tp.getTripDate());
//                                                Log.d("FileUpload", "Success");
//
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                            Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
//                                        }
//                                    }
//
//                                }
//                                for (CustomerDetails cd : customerDetailsImageArrayList) {
//                                    if (cd.getImageURL() != null) {
//                                        try {
//                                            String data = Environment.getExternalStorageDirectory() + File.separator
//                                                    + "KSFA" + File.separator + "images" + File.separator + cd.getImageURL();
//                                            Log.d("FileUpload", "FilePath " + data);
//                                            FileInputStream in = new FileInputStream(new File(data));
//                                            boolean result = con.storeFile("/home/vistagftp/vistagftp/" + cd.getImageURL(), in);
//                                            in.close();
//                                            if (result) {
//                                                Log.d("FileUpload", "Success");
//                                                dbHandler.updateCustomerDetailsUploadFlag(cd.getCustomerCode());
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                            Log.d("FileUpload", "Customer Details Error" + e.getMessage());
//                                        }
//
//                                    }
//
//                                }
//                                for (LPOImages lp : lpoImagesArrayList) {
//                                    if (lp.getLpoImagePath() != null) {
//                                        try {
//                                            String data = Environment.getExternalStorageDirectory() + File.separator
//                                                    + "KSFA" + File.separator + "images" + File.separator + lp.getLpoImagePath();
//                                            Log.d("FileUpload", "FilePath " + data);
//                                            FileInputStream in = new FileInputStream(new File(data));
//                                            boolean result = con.storeFile("/home/vistagftp/vistagftp/" + lp.getLpoImagePath(), in);
//                                            in.close();
//                                            if (result) {
//                                                lpoImagesTable.updatelpoGalleryUploadFlag(String.valueOf(lp.getImageId()));
//                                                System.out.println("TTT::String.valueOf(lp.imageId) = " + String.valueOf(lp.getImageId()));
//                                                System.out.println("TTT::Sucesss= ");
//                                                Log.d("FileUpload", "Success");
//
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                            Log.d("FileUpload", "Lpo Error" + e.getMessage());
//                                        }
//                                    }
//
//                                }
//
//
//                                con.logout();
//                                con.disconnect();
//                            }
//                        }
//
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Log.d("FileUpload", "Error" + e.getMessage());
//                    }
                    }

                    @Override
                    public void onFailure(Call<ResponseJson> call, Throwable t) {


                        Log.i("SyncButton: POST-----", "Failure" + jp.toString());

                        //Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }
                });


        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Throwable" + e, Toast.LENGTH_LONG).show();

        }

    }

    /*void  postFtp(){
        System.out.println("FTpppppppp = ");
        FTPClient con = null;
        try {
            System.out.println("Enterrrrr = ");
            LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
            ExpenseTable expenseTable = new ExpenseTable(getApplicationContext());
            ArrayList<TripDetails> tripDetailsImageArrayList = dbHandler.getTripDetailsImageService();
            ArrayList<Expense> expenseSignImageArrayList = expenseTable.getExpenseSignImageService();
            ArrayList<CustomerDetails> customerDetailsImageArrayList = dbHandler.getCustomerDetailsImageService();
            ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLpoImageService();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();
            StrictMode.setThreadPolicy(policy);
            Log.d("FileUpload", "FilePath");

            System.out.println("FileUploadddddddd = ");
            if (tripDetailsImageArrayList.size() != 0 || expenseSignImageArrayList.size() != 0 || customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
//                        if (tripDetailsImageArrayList.size() != 0 || customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
                con = new FTPClient();
                con.connect(Constants.FTP_HOST_NAME);
                Log.d("FileUpload", "FilePath Conneted");
                System.out.println("FilePath Conneteddddddddd = ");
                if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {

                    System.out.println("AAA:::con = ");

                    Log.d("FileUpload", "FilePath Login");
                    con.enterLocalPassiveMode(); // important!
                    con.setFileType(FTP.BINARY_FILE_TYPE);
                    for (TripDetails tp : tripDetailsImageArrayList) {
                        if (tp.getOdometerMileage() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + tp.getOdometerMileage();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getOdometerMileage(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    dbHandler.updateTripMileageUploadFlag(tp.getTripNumber(), tp.getTripDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }

                    for (Expense tp : expenseSignImageArrayList) {
                        if (tp.getSignatureFilePath() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + tp.getSignatureFilePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getSignatureFilePath(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    expenseTable.updateExpenseSignUploadFlag(tp.getExpenseNumber(), tp.getExpensePostDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }


                    for (CustomerDetails cd : customerDetailsImageArrayList) {
                        if (cd.getImageURL() != null) {
                            try {
                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + cd.getImageURL();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + cd.getImageURL(), in);
                                in.close();
                                if (result) {
                                    Log.d("FileUpload", "Success");
                                    dbHandler.updateCustomerDetailsUploadFlag(cd.getCustomerCode());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Customer Details Error" + e.getMessage());
                            }

                        }

                    }
                    for (LPOImages lp : lpoImagesArrayList) {
                        if (lp.getLpoImagePath() != null) {
                            try {
                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + lp.getLpoImagePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + lp.getLpoImagePath(), in);
                                in.close();
                                System.out.println("ftp.getReplyString() = " + con.getReplyString());
                                if (result) {
                                    Toast.makeText(getApplicationContext(),"Success" , Toast.LENGTH_LONG).show();
                                    lpoImagesTable.updatelpoGalleryUploadFlag(String.valueOf(lp.getImageId()));
                                    System.out.println("TTT::String.valueOf(lp.imageId) = " + String.valueOf(lp.getImageId()));
                                    System.out.println("TTT::Sucesss= ");
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),e.getMessage() , Toast.LENGTH_LONG).show();

                                System.out.println("e.getMessage() = " + e.getMessage());
                                Log.d("FileUpload", "Lpo Error" + e.getMessage());
                            }
                        }

                    }


                    con.logout();
                    con.disconnect();

                    Toast.makeText(getApplicationContext(), "Images Posted Successfully", Toast.LENGTH_LONG).show();

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),e.getMessage() , Toast.LENGTH_LONG).show();
            Log.d("FileUpload", "Error" + e.getMessage());
        }
    }*/

    boolean postFtp(){
        System.out.println("FTpppppppp = ");
        FTPClient con = null;
        try {
            System.out.println("Enterrrrr = ");
            LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
            ExpenseTable expenseTable = new ExpenseTable(getApplicationContext());
            ArrayList<TripDetails> tripDetailsImageArrayList = dbHandler.getTripDetailsImageService();
            ArrayList<Invoices> invoiceSignImageArrayList = dbHandler.getInvoiceSignImageService();
            ArrayList<Orders> orderSignImageArrayList = dbHandler.getOrderSignImageService();
            ArrayList<CollectionDetails> secCollecSignImageArrayList = dbHandler.getSecCollecSignImageService();
            ArrayList<CollectionDetails>  priCollecSignImageArrayList = dbHandler.getPrimCollecSignImageService();
            ArrayList<Expense> expenseSignImageArrayList = expenseTable.getExpenseSignImageService();
            ArrayList<CustomerDetails> customerDetailsImageArrayList = dbHandler.getCustomerDetailsImageService();
            ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLpoImageService();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();
            StrictMode.setThreadPolicy(policy);
            Log.d("FileUpload", "FilePath");

            System.out.println("FileUploadddddddd = ");
            if (tripDetailsImageArrayList.size() != 0 || secCollecSignImageArrayList.size() !=0 || priCollecSignImageArrayList.size() != 0 ||  expenseSignImageArrayList.size() != 0 || invoiceSignImageArrayList.size() != 0 || orderSignImageArrayList.size() !=0 || customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
//                        if (tripDetailsImageArrayList.size() != 0 || customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
                con = new FTPClient();
                con.connect(Constants.FTP_HOST_NAME);
                Log.d("FileUpload", "FilePath Conneted");
                System.out.println("FilePath Conneteddddddddd = ");
                if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {

                    System.out.println("AAA:::con = ");

                    Log.d("FileUpload", "FilePath Login");
                    con.enterLocalPassiveMode(); // important!
                    con.setFileType(FTP.BINARY_FILE_TYPE);
                    for (TripDetails tp : tripDetailsImageArrayList) {
                        if (tp.getOdometerMileage() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + tp.getOdometerMileage();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getOdometerMileage(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    dbHandler.updateTripMileageUploadFlag(tp.getTripNumber(), tp.getTripDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }

                    for (Expense tp : expenseSignImageArrayList) {
                        if (tp.getSignatureFilePath() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + tp.getSignatureFilePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getSignatureFilePath(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    expenseTable.updateExpenseSignUploadFlag(tp.getExpenseNumber(), tp.getExpensePostDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }

                    for (Orders or : orderSignImageArrayList) {
                        if (or.getSignatureFilePath() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + or.getSignatureFilePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + or.getSignatureFilePath(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    dbHandler.updateOrderSignUploadFlag(or.getOrderCode(), or.getDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }

                    for (Invoices invoices : invoiceSignImageArrayList) {
                        if (invoices.getSignatureFilePath() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + invoices.getSignatureFilePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + invoices.getSignatureFilePath(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    dbHandler.updateInvoiceSignUploadFlag(invoices.getInvoiceCode(), invoices.getDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }

                    for (CollectionDetails collectionDetails : secCollecSignImageArrayList) {
                        if (collectionDetails.getSignatureFilePath() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + collectionDetails.getSignatureFilePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + collectionDetails.getSignatureFilePath(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    dbHandler.updateInvoiceSignUploadFlag(collectionDetails.getCollectionCode(), collectionDetails.getDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }

                    for (CollectionDetails collectionDetails1 : priCollecSignImageArrayList) {
                        if (collectionDetails1.getSignatureFilePath() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + collectionDetails1.getSignatureFilePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + collectionDetails1.getSignatureFilePath(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    dbHandler.updateInvoiceSignUploadFlag(collectionDetails1.getCollectionCode(), collectionDetails1.getDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }


                    for (CustomerDetails cd : customerDetailsImageArrayList) {
                        if (cd.getImageURL() != null) {
                            try {
                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + cd.getImageURL();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + cd.getImageURL(), in);
                                in.close();
                                if (result) {
                                    Log.d("FileUpload", "Success");
                                    dbHandler.updateCustomerDetailsUploadFlag(cd.getCustomerCode());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Customer Details Error" + e.getMessage());
                            }

                        }

                    }
                    for (LPOImages lp : lpoImagesArrayList) {
                        if (lp.getLpoImagePath() != null) {
                            try {
                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + lp.getLpoImagePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + lp.getLpoImagePath(), in);
                                in.close();
                                System.out.println("ftp.getReplyString() = " + con.getReplyString());
                                if (result) {
                                    Toast.makeText(getApplicationContext(),"Success" , Toast.LENGTH_LONG).show();
                                    lpoImagesTable.updatelpoGalleryUploadFlag(String.valueOf(lp.getImageId()));
                                    System.out.println("TTT::String.valueOf(lp.imageId) = " + String.valueOf(lp.getImageId()));
                                    System.out.println("TTT::Sucesss= ");
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),e.getMessage() , Toast.LENGTH_LONG).show();

                                System.out.println("e.getMessage() = " + e.getMessage());
                                Log.d("FileUpload", "Lpo Error" + e.getMessage());
                            }
                        }

                    }


                    con.logout();
                    con.disconnect();
                    return true;
                } else{
                    return false;
                }
            }else{
                return true;
            }


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),e.getMessage() , Toast.LENGTH_LONG).show();
            Log.d("FileUpload", "Error" + e.getMessage());
        }
        return false;
    }
    private void backupDatabase() {
        String sourcePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/ksfaDB.DB";
        File source = new File(sourcePath);

        File backupDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/KSFA/", "dbBackup");
        backupDir.mkdir();
       /* SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String date = timeStampFormat.format(myDate);*/

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_hh_mm");
        String dateString = sdf.format(date);
//        datetime.setText(dateString);
        System.out.println("TTT::dateString = " + dateString);

        String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/dbBackup/" + tab_code + "_" + login_id + "_" + dateString + "_";
        try {
            File f = new File(destinationPath + "ksfaDB.DB");
            if (f.exists() && !f.isDirectory()) {
                return;
            }
            File temp = File.createTempFile(destinationPath, "ksfaDB.DB");

            boolean exists = temp.exists();

            if (exists) {
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        destinationPath += "ksfaDB.DB";

        File copied = new File(destinationPath);
        try (
                InputStream in = new BufferedInputStream(
                        new FileInputStream(source));
                OutputStream out = new BufferedOutputStream(
                        new FileOutputStream(copied))) {

            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        postingDatabaseFtp(login_id);
      /*  postingDbThread = new Thread(new Runnable() {

            @Override
            public void run() {
                Log.d("Multi Thread DB*****", "Starting T------1.");
                postingDatabaseFtp(login_id);
                Log.d("Multi Thread DB*****", "Finishing T-----1.");
            }
        });

        postingDbThread.start();*/
    }
    private void postingDatabaseFtp(String login_id) {

        FTPClient con = null;
        con = new FTPClient();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll()
                .build();
        StrictMode.setThreadPolicy(policy);
        try {
            con.connect(FTP_HOST_NAME);
            if (con.login(FTP_USER_NAME, FTP_PASSWORD)) {
                Log.d("FileUpload", "FilePath Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d("FileUpload", "FilePath Connected");
      /*  SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String date = timeStampFormat.format(myDate);*/

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_hh_mm");
        String dateString = sdf.format(date);
//        datetime.setText(dateString);
        System.out.println("TTT1::dateString = " + dateString);

        String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/dbBackup/" + tab_code + "_" + login_id + "_" + dateString + "_";


        destinationPath += "ksfaDB.DB";

        String file_name = tab_code + "_" + login_id + "_" + dateString + "_" + "ksfaDB.DB";
        Log.d("FileUpload", "FileName--" + file_name);
        System.out.println("TTT::file_name1 = " + file_name);
        try {
            String data = Environment.getExternalStorageDirectory() + File.separator
                    + "KSFA" + File.separator + "ksfaDB.DB";
            Log.d("FileUpload", "FilePath " + data);

            FileInputStream in = new FileInputStream(new File(destinationPath));
            String serverPath = "/home/vistagftp/vistagftp/Db/" + file_name;
            boolean result = con.storeFile(serverPath, in);
            Log.d("FileUpload", "File Result--" + result);
            System.out.println("TTT::result1 = " + result);
            in.close();

            if (result) {
                Log.d("FileUpload", "Success");

            }


            con.logout();
            con.disconnect();

            Log.d("FileUpload", "File Connection Closed--");


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Database Upload Error" + e.getMessage());
        }
    }
    private void importRDLData() {

        Gson gson = new GsonBuilder().setLenient().create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL).
                        client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the Gson ConverterFactory to directly convert json data to object
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Call<MasterResponseJson> orderStatusUpdate = api.getMasterRDLData(login_id, companyCode);

        Log.d("PostingData", "Service Status---Company:--" + companyCode);

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        newJourneyPlanTable1 = new NewJourneyPlanTable(getApplicationContext());

        expenseTable1 = new ExpenseTable(getApplicationContext());

        orderStatusUpdate.enqueue(new Callback<MasterResponseJson>() {

            @Override
            public void onResponse(Call<MasterResponseJson> customerMaster, retrofit2.Response<MasterResponseJson> response) {

                MasterResponseJson resultSet = response.body();

                if (resultSet.status.equals("true")) {

                    ordersArrayList1 = new Gson().fromJson(resultSet.order_status_update, new TypeToken<ArrayList<Orders>>() {
                    }.getType());


                    Log.d("PostingData", "Status---OrderArrayList:--" + ordersArrayList1.size());


                    String[] orders = new String[resultSet.order_status_update.size()];

                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < ordersArrayList1.size(); i++) {
                        Log.i("PostingData", "OrderUpdateService---" + ordersArrayList1.get(i).getOrderCode() + ordersArrayList1.get(i).getStatus());
                        dbHandler.updateOrderStatusfromweb(ordersArrayList1.get(i).getOrderCode(), ordersArrayList1.get(i).getStatus());

                    }

                    newJourneyPlanArrayList1 = new Gson().fromJson(resultSet.journeyPlanStatus, new TypeToken<ArrayList<NewJourneyPlan>>() {
                    }.getType());

                    if (newJourneyPlanArrayList1 != null)
                        for (int i = 0; i < newJourneyPlanArrayList1.size(); i++) {
//                            System.out.println("newJourneyPlanArrayList1.size() = " + newJourneyPlanArrayList1.size());
//                            System.out.println("newJourneyPlanArrayList1.get(0).toString() = " +  newJourneyPlanArrayList1.get(0).toString());
                            Log.i("NewJourneyPlan Status", "JP Array Size---" + newJourneyPlanArrayList1.size());
                            Log.i("NewJourneyPlan Status", "JP Array First Element---" + newJourneyPlanArrayList1.get(0).toString());
                            Log.i("NewJourneyPlan Status", "NewJourneyPlan ID---" + newJourneyPlanArrayList1.get(i).getJourneyPlanNumber() + "  Status--" + newJourneyPlanArrayList1.get(i).getStatus());
                            newJourneyPlanTable1.updateJourneyPlanStatusFromWeb(newJourneyPlanArrayList1.get(i).getJourneyPlanNumber(), newJourneyPlanArrayList1.get(i).getStatus());

                        }


                    expenseArrayList1 = new Gson().fromJson(resultSet.expense, new TypeToken<ArrayList<Expense>>() {
                    }.getType());

                    if (expenseArrayList1 != null)
                        for (int i = 0; i < expenseArrayList1.size(); i++) {
                            System.out.println("expenseList.size() = " + expenseArrayList1.size());
                            System.out.println("expenseList.expenseArrayList1() = " + expenseArrayList1);
                            System.out.println("expenseList.getExpenseNumber() = " + expenseArrayList1.get(i).getExpenseNumber());
                            System.out.println("expenseList.getExpenseApprovedAmount() = " + expenseArrayList1.get(i).getExpenseApprovedAmount());
                            System.out.println("expenseList.getExpenseStatus() = " + expenseArrayList1.get(i).getExpenseStatus());
                            System.out.println("expenseList.getExpenseStatus() = " + expenseArrayList1.get(i).getExpenseReason());

                            expenseTable1.updateExpenseMaster(expenseArrayList1.get(i).getExpenseNumber(), expenseArrayList1.get(i).getExpenseApprovedAmount(),expenseArrayList1.get(i).getExpenseStatus(),expenseArrayList1.get(i).getExpenseReason());

                        }



                    collectionPrArraylist1 = new Gson().fromJson(resultSet.prstatus, new TypeToken<ArrayList<CollectionDetails>>() {
                    }.getType());


                    System.out.println("collectionPrArraylist1.size() = " + collectionPrArraylist1.size());
                    System.out.println("collectionPrArraylist1.size() = " + resultSet.prstatus.size());
                    String[] primaryCollectionDetails = new String[resultSet.prstatus.size()];
                    if (collectionPrArraylist1 != null) {
                        for (int i = 0; i < collectionPrArraylist1.size(); i++) {
                            System.out.println("collectionPrArraylist1 = " + collectionPrArraylist1);
                            System.out.println("collectionPrArraylist1.get(i).getCollectionCode() = " + collectionPrArraylist1.get(i).getCollectionCode());
                            System.out.println("collectionPrArraylist1.get(i).getCollectionCode() = " + collectionPrArraylist1.get(i).getCollectionStatus());
                            Log.i("PostingData", "OrderUpdateService---" + collectionPrArraylist1.get(i).getCollectionCode() + collectionPrArraylist1.get(i).getCollectionStatus());
                            System.out.println("CollectionPrimary= " + collectionPrArraylist1.get(i).getCollectionCode() + collectionPrArraylist1.get(i).getCollectionStatus());
                            dbHandler.uptPriCollectionStatfrweb(collectionPrArraylist1.get(i).getCollectionCode(), collectionPrArraylist1.get(i).getCollectionStatus());

                        }
                    }

                    collectionSecArraylist1 = new Gson().fromJson(resultSet.secstatus, new TypeToken<ArrayList<CollectionDetails>>() {
                    }.getType());


                    String[] secCollectionDetails = new String[resultSet.secstatus.size()];

                    for (int i = 0; i < collectionSecArraylist1.size(); i++) {
                        Log.i("PostingData", "OrderUpdateService---" + collectionSecArraylist1.get(i).getCollectionCode() + collectionSecArraylist1.get(i).getCollectionStatus());
                        System.out.println("CollectionSecondary= " +collectionSecArraylist1.get(i).getCollectionCode() + collectionSecArraylist1.get(i).getCollectionStatus() );
                        dbHandler.uptSecCollectionStatfrweb(collectionSecArraylist1.get(i).getCollectionCode(), collectionSecArraylist1.get(i).getCollectionStatus());

                    }


                }
            }

            @Override
            public void onFailure(Call<MasterResponseJson> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        Log.i("OrderUpdateService", "Service running");

    }
    void postingImagesToFTP(){

        FTPClient con = null;
        try {
            LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
            ArrayList<TripDetails> tripDetailsImageArrayList = dbHandler.getTripDetailsImageService();
            ArrayList<CustomerDetails> customerDetailsImageArrayList = dbHandler.getCustomerDetailsImageService();
            ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLpoImageService();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();
            StrictMode.setThreadPolicy(policy);
            Log.d("FileUpload", "FilePath");
            if (tripDetailsImageArrayList.size() != 0 || customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
                con = new FTPClient();
                con.connect(Constants.FTP_HOST_NAME);
                Log.d("FileUpload", "FilePath Conneted");
                if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {

                    System.out.println("AAA:::con = " + con);

                    Log.d("FileUpload", "FilePath Login");
                    con.enterLocalPassiveMode(); // important!
                    con.setFileType(FTP.BINARY_FILE_TYPE);
                    for (TripDetails tp : tripDetailsImageArrayList) {
                        if (tp.getOdometerMileage() != null) {
                            try {
                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + tp.getOdometerMileage();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getOdometerMileage(), in);

                                System.out.println("AAA:::result " + result);

                                in.close();
                                if (result) {
                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                    dbHandler.updateTripMileageUploadFlag(tp.getTripNumber(), tp.getTripDate());
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                            }
                        }

                    }
                    for (CustomerDetails cd : customerDetailsImageArrayList) {
                        if (cd.getImageURL() != null) {
                            try {
                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + cd.getImageURL();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + cd.getImageURL(), in);
                                in.close();
                                if (result) {
                                    Log.d("FileUpload", "Success");
                                    dbHandler.updateCustomerDetailsUploadFlag(cd.getCustomerCode());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Customer Details Error" + e.getMessage());
                            }

                        }

                    }
                    for (LPOImages lp : lpoImagesArrayList) {
                        if (lp.getLpoImagePath() != null) {
                            try {
                                String data = Environment.getExternalStorageDirectory() + File.separator
                                        + "KSFA" + File.separator + "images" + File.separator + lp.getLpoImagePath();
                                Log.d("FileUpload", "FilePath " + data);
                                FileInputStream in = new FileInputStream(new File(data));
                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + lp.getLpoImagePath(), in);
                                in.close();
                                if (result) {
                                    lpoImagesTable.updatelpoGalleryUploadFlag(String.valueOf(lp.getImageId()));
                                    System.out.println("TTT::String.valueOf(lp.imageId) = " + String.valueOf(lp.getImageId()));
                                    System.out.println("TTT::Sucesss= ");
                                    Log.d("FileUpload", "Success");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("FileUpload", "Lpo Error" + e.getMessage());
                            }
                        }

                    }


                    con.logout();
                    con.disconnect();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Error" + e.getMessage());
        }
    }

    private void generatePostSyncDateTime() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        postSyncDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        postSyncTime = sdf.format(outDate);
    }

    public void insertFirstData() {

        generalSettingsTable.insertingSettingsMaster(settingsArrayList);

        dbHandler.insertingJourneyPlanMaster(journeyplanList);

        dbHandler.insertingCustomerMaster(customerMasterList);


        if (customerMasterList != null) {
            for (int i = 0; i < customerMasterList.size(); i++) {

                new AsyncTaskLoadImage().execute(customerMasterList.get(i).getImage_url(), customerMasterList.get(i).getCustomer_image());
            }
        }

        dbHandler.insertingMetricsTargetMaster(metricsTargetList);
    }

    public void insertSecondData() {



    }

    public void insertThirdData() {

        warehouseTable.insertingWarehousesMaster(warehouseProductsList);
        dbHandler.insertingProductsMaster(itemMastersList);
        primaryInvoiceTable.insertingPrimaryInvoicesMaster(primaryInvoiceList);

        primaryInvoiceProductTable.insertingPrimaryInvoiceProductsMaster(primaryInvoiceProductsList);

        distributorsTable.insertingDistributorsMaster(distributorsArrayList);

        warehouseStockProductsTable.insertingWarehouseStockMaster(warehouseProductsList);

    }

    public void insertFourthData() {

        dbHandler.insertingBdePriceListsMaster(srPriceListArrayList);
        customerShippingAddressTable.insertingCusShippingAddressMaster(shipping_addressList);

        competitorProductTable.insertingCompetitorProductsMaster(competitor_productList);

        dbHandler.insertingLocationsMaster(locationList);

        dbHandler.insertingSrInvoiceBacthMaster(srInvoiceBatchList);

    }

    public void insertFifthData() {

        ledgerTable.insertingLedgersMaster(ledgerList);

        ageTable.insertingAgeDetailsMaster(ageList);

        dbHandler.insertingFooterMarqueeMsgText(marqueeTextList);

        bdeListTable.insertingBDEListMaster(bdeListArrayList);
        dbHandler.insertingPriceListLinesMaster(price_lineList);

    }

    public void insertSixthData() {
        dbHandler.insertingPriceListsHeaderMaster(price_headerList);


    }

    public void insertSeventhData() {


        artifactsTable.insertingArtifactsMaster(artifactsArrayList);

        if (artifactsArrayList != null) {
            for (int i = 0; i < artifactsArrayList.size(); i++) {

                new AsyncTaskLoadPdf().execute(artifactsArrayList.get(i).getFileURL(), artifactsArrayList.get(i).getFileName());

            }
        }


        surveyTable.insertingSurveysMaster(surveyArrayList);

        surveyQuestionsTable.insertingSurveyQuestionsMaster(surveyLinesArrayList);

        if (surveyOptionsArrayList != null) {
            surveyQuestionOptionTable.insertingSurveyOptionsMaster(surveyOptionsArrayList);
        }

        dbHandler.insertingFeedbackTypeMaster(feedbackList);
        expenseTypeTable.insertingExpenseTypeMaster(expenseTypeList);


    }

    private void postingDatabaseToFtp() {
        FTPClient con = null;
        con = new FTPClient();

        try {
            con.connect(Constants.FTP_HOST_NAME);
            if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {
                Log.d("FileUpload", "FilePath Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d("FileUpload", "FilePath Connected");

        String file_name = tab_code + "_" + sr_code + "_" + "ksfaDB.DB";
        Log.d("FileUpload", "FileName--" + file_name);

        try {
            String data = Environment.getExternalStorageDirectory() + File.separator
                    + "KSFA" + File.separator + "ksfaDB.DB";
            Log.d("FileUpload", "FilePath " + data);

            FileInputStream in = new FileInputStream(new File(data));
            String serverPath = "/home/vistagftp/vistagftp/Db/" + file_name;
            boolean result = con.storeFile(serverPath, in);
            Log.d("FileUpload", "File Result--" + result);

            in.close();

            if (result) {
                Log.d("FileUpload", "Success");

            }


            con.logout();
            con.disconnect();

            Log.d("FileUpload", "File Connection Closed--");


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Database Upload Error" + e.getMessage());
        }
    }

    private void deletingMasterData() {

        dbHandler.deleteMasterData();

        //dbHandler.deleteCustomerMaster();
        //dbHandler.deleteItemMaster();
        competitorProductTable.deleteCompetitorProducts();
        //dbHandler.deletePriceList();
        //dbHandler.deleteJourneyplan();
        customerShippingAddressTable.deleteShippingAddress();
        warehouseStockProductsTable.deleteWarehouseStockProduct();
        warehouseTable.deleteWarehouse();
        ledgerTable.deleteLedger();
        ageTable.deleteAge();
        primaryInvoiceProductTable.deletePrimaryInvoiceProduct();
        primaryInvoiceTable.deletePrimaryInvoice();
        distributorsTable.deleteDistributors();
        generalSettingsTable.deleteGeneralSettings();
        artifactsTable.deleteArtifacts();
        surveyTable.deleteSurveyHeader();
        surveyQuestionsTable.deleteSurveyLines();
        surveyQuestionOptionTable.deleteSurveyOptions();
        bdeListTable.deleteBdeList();
        expenseTypeTable.deleteExpenseType();
    }
   /* private void insertMasterData() {
        try {
            //looping through all the heroes and inserting the names inside the string array
            for (int i = 0; i < customerMasterList.size(); i++) {
                // Log.d("MyTestService", "Name" + customerMasterList.get(i).getCustomer_name());
                long id2 = dbHandler.create_customer_master

                        (
                                customerMasterList.get(i).getCustomer_code(),
                                customerMasterList.get(i).getCustomer_name(),
                                customerMasterList.get(i).getBill_address_line1(),
                                customerMasterList.get(i).getBill_address_line2(),
                                customerMasterList.get(i).getBill_address_line3(),
                                customerMasterList.get(i).getBill_city(),
                                customerMasterList.get(i).getBill_state(),
                                customerMasterList.get(i).getContact_no(),
                                customerMasterList.get(i).getCustomer_type(),
                                customerMasterList.get(i).getPrice_list(),
                                customerMasterList.get(i).getPrice_list_code(),
                                "Web",
                                null,
                                null,
                                null,
                                null,
                                customerMasterList.get(i).getCustomer_image(),
                                customerMasterList.get(i).getImage_url(),
                                customerMasterList.get(i).getCustomerGroup(),
                                ""


                        );

                new AsyncTaskLoadImage().execute(customerMasterList.get(i).getImage_url(), customerMasterList.get(i).getCustomer_image());
            }

            for (int i = 0; i < itemMastersList.size(); i++) {
                //Log.d("MyTestService", "Name" + customerMasterList.get(i).getCustomer_name());
                long id3 = dbHandler.create_item_master

                        (
                                itemMastersList.get(i).getProduct_id(),
                                itemMastersList.get(i).getProduct_name(),
                                itemMastersList.get(i).getLong_description(),
                                itemMastersList.get(i).getStock_uom(),
                                itemMastersList.get(i).getSale_uom(),
                                itemMastersList.get(i).getBatch(),
                                itemMastersList.get(i).getFocus(),
                                itemMastersList.get(i).getItem_cost()

                        );
            }

            for (int i = 0; i < price_headerList.size(); i++) {
                //   Log.d("MyTestService", "Name" + customerMasterList.get(i).getCustomer_name());
                long id4 = dbHandler.create_price_list_header

                        (
                                price_headerList.get(i).getPrice_list_code(),
                                price_headerList.get(i).getPrice_list_name()
                        );
            }

            for (int i = 0; i < price_lineList.size(); i++) {

                long id5 = dbHandler.create_price_list_lines

                        (
                                price_lineList.get(i).getPrice_code(),
                                price_lineList.get(i).getProduct_name(),
                                price_lineList.get(i).getProduct_code(),
                                price_lineList.get(i).getPrice()


                        );
            }

            for (int i = 0; i < shipping_addressList.size(); i++) {

                CustomerShippingAddress customerShippingAddress = new CustomerShippingAddress();
                customerShippingAddress.setCustomer_id(shipping_addressList.get(i).getCustomer_id());
                customerShippingAddress.setShip_address1(shipping_addressList.get(i).getShip_address1());
                customerShippingAddress.setShip_address2(shipping_addressList.get(i).getShip_address2());
                customerShippingAddress.setShip_address3(shipping_addressList.get(i).getShip_address3());
                customerShippingAddress.setShip_city(shipping_addressList.get(i).getShip_city());
                customerShippingAddress.setShip_state(shipping_addressList.get(i).getShip_state());

                customerShippingAddressTable.create(customerShippingAddress);

            }

            for (int i = 0; i < competitor_productList.size(); i++) {

                CompetitorProduct productCompetitor = new CompetitorProduct();
                productCompetitor.setProductCode(competitor_productList.get(i).getProductCode());
                productCompetitor.setProductName(competitor_productList.get(i).getProductName());
                productCompetitor.setProductMasterCode(competitor_productList.get(i).getProductMasterCode());

                competitorProductTable.create(productCompetitor);

            }


            for (int i = 0; i < journeyplanList.size(); i++) {

                long id6 = dbHandler.create_journey_plan

                        (
                                journeyplanList.get(i).getSeq_no(),
                                journeyplanList.get(i).getCustomer_code(),
                                journeyplanList.get(i).getCustomer_name(),
                                journeyplanList.get(i).getLocation(),
                                journeyplanList.get(i).getCustomer_type(),
                                journeyplanList.get(i).getGps_latitude(),
                                journeyplanList.get(i).getGps_longitude()

                        );

            }

            for (int i = 0; i < srPriceListArrayList.size(); i++) {

                long id6 = dbHandler.create_sr_pricelist

                        (
                                srPriceListArrayList.get(i).getPrice_id(),
                                srPriceListArrayList.get(i).getSr_code(),
                                srPriceListArrayList.get(i).getFileUrl(),
                                srPriceListArrayList.get(i).getFileName()

                        );

            }

            for (int i = 0; i < warehouseProductsList.size(); i++) {

                ViewStockWarehouseProduct viewStockWarehouseProduct = new ViewStockWarehouseProduct();
                viewStockWarehouseProduct.setProductCode(warehouseProductsList.get(i).getProductCode());
                viewStockWarehouseProduct.setProductName(warehouseProductsList.get(i).getProductName());
                viewStockWarehouseProduct.setProductPrice(warehouseProductsList.get(i).getProductPrice());
                viewStockWarehouseProduct.setProductUom(warehouseProductsList.get(i).getProductUom());
                viewStockWarehouseProduct.setProductQuantity(warehouseProductsList.get(i).getProductQuantity());
                viewStockWarehouseProduct.setProductTotalPrice(warehouseProductsList.get(i).getProductTotalPrice());
                viewStockWarehouseProduct.setWarehouseCode(warehouseProductsList.get(i).getWarehouseCode());

                warehouseStockProductsTable.create(viewStockWarehouseProduct);

            }

            for (int i = 0; i < warehouseProductsList.size(); i++) {

                Warehouse warehouse = new Warehouse();
                warehouse.setWarehouse_code(warehouseProductsList.get(i).getWarehouseCode());
                warehouse.setWarehouse_name(warehouseProductsList.get(i).getWarehouseName());
                warehouseTable.create(warehouse);

            }

            for (int i = 0; i < ledgerList.size(); i++) {

                Ledger ledger = new Ledger();

                ledger.setInvoiceNumber(ledgerList.get(i).getInvoiceNumber());
                ledger.setInvoiceDate(ledgerList.get(i).getInvoiceDate());
                ledger.setInvoiceAmount(ledgerList.get(i).getInvoiceAmount());
                ledger.setReceiptAmount(ledgerList.get(i).getReceiptAmount());
                ledger.setOutstandingAmount(ledgerList.get(i).getOutstandingAmount());
                ledger.setCustomerId(ledgerList.get(i).getCustomerId());
                ledgerTable.create(ledger);

            }

            for (int i = 0; i < ageList.size(); i++) {

                Age age = new Age();

                age.setCustomerId(ageList.get(i).getCustomerId());
                age.setBelowThirtyVal(ageList.get(i).getBelowThirtyVal());
                age.setThirtyOneToSixtyVal(ageList.get(i).getThirtyOneToSixtyVal());
                age.setSixtyOneToNinetyVal(ageList.get(i).getSixtyOneToNinetyVal());
                age.setAboveNinetyVal(ageList.get(i).getAboveNinetyVal());
                age.setCustomerId(ageList.get(i).getCustomerId());
                ageTable.create(age);

            }

            for (int i = 0; i < feedbackList.size(); i++) {

                long id6 = dbHandler.create_feedback_type

                        (
                                feedbackList.get(i).getFeedback_type()

                        );

            }

            for (int i = 0; i < marqueeTextList.size(); i++) {

                long id6 = dbHandler.create_global_message

                        (
                                marqueeTextList.get(i).getMsgContent()

                        );

            }

            for (int i = 0; i < locationList.size(); i++) {

                long id6 = dbHandler.create_location_lines

                        (
                                locationList.get(i).getLocation_code(),
                                locationList.get(i).getLocation_name(),
                                locationList.get(i).getLga_name(),
                                locationList.get(i).getCity_name(),
                                locationList.get(i).getState_name()

                        );

            }

            for (int i = 0; i < primaryInvoiceList.size(); i++) {

                PrimaryInvoice primaryInvoice = new PrimaryInvoice();
                primaryInvoice.setWarehouseId(primaryInvoiceList.get(i).getWarehouseId());
                primaryInvoice.setPrimary_invoice_number(primaryInvoiceList.get(i).getPrimary_invoice_number());
                primaryInvoiceTable.create(primaryInvoice);

            }

            for (int i = 0; i < primaryInvoiceProductsList.size(); i++) {

                PrimaryInvoiceProduct primaryInvoiceProduct = new PrimaryInvoiceProduct();
                primaryInvoiceProduct.setPrimaryInvoiceNumber(primaryInvoiceProductsList.get(i).getPrimaryInvoiceNumber());
                primaryInvoiceProduct.setPrimaryInvoiceProductCode(primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductCode());
                primaryInvoiceProduct.setPrimaryInvoiceProductName(primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductName());
                primaryInvoiceProduct.setPrimaryInvoiceProductQuantity(primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductQuantity());
                primaryInvoiceProduct.setPrimaryInvoiceProductUom(primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductUom());
                primaryInvoiceProduct.setPrimaryInvoiceProductPrice(primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductPrice());
                primaryInvoiceProduct.setPrimaryInvoiceProductTotalPrice(primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductTotalPrice());
                primaryInvoiceProductTable.create(primaryInvoiceProduct);

            }

            for (int i = 0; i < distributorsArrayList.size(); i++) {

                Distributors distributors = new Distributors();

                distributors.setDistributorCode(distributorsArrayList.get(i).getDistributorCode());
                distributors.setDistributorName(distributorsArrayList.get(i).getDistributorName());

                distributorsTable.create(distributors);

            }

            for (int i = 0; i < settingsArrayList.size(); i++) {

                GeneralSettings generalSettings = new GeneralSettings();
                generalSettings.setConfigKey(settingsArrayList.get(i).getConfigKey());
                generalSettings.setValue(settingsArrayList.get(i).getValue());
                generalSettingsTable.create(generalSettings);

            }

            for (int i = 0; i < artifactsArrayList.size(); i++) {

                Artifacts artifacts = new Artifacts();
                artifacts.setFileName(artifactsArrayList.get(i).getFileName());
                artifacts.setFileType(artifactsArrayList.get(i).getFileType());
                artifacts.setArtifactType(artifactsArrayList.get(i).getArtifactType());
                artifacts.setFileURL(artifactsArrayList.get(i).getFileURL());
                artifacts.setFilePath("/mSale/artifacts/" + artifactsArrayList.get(i).getFileName());

                artifactsTable.create(artifacts);

                new AsyncTaskLoadPdf().execute(artifactsArrayList.get(i).getFileURL(), artifactsArrayList.get(i).getFileName());


            }

            for (int i = 0; i < surveyArrayList.size(); i++) {

                SurveyHeader survey = new SurveyHeader();
                survey.setSurvey_name(surveyArrayList.get(i).getSurvey_name());
                survey.setSurvey_code(surveyArrayList.get(i).getSurvey_code());
                survey.setSurvey_type(surveyArrayList.get(i).getSurvey_type());
                survey.setSurvey_description(surveyArrayList.get(i).getSurvey_description());
                surveyTable.create(survey);

            }

            for (int i = 0; i < surveyLinesArrayList.size(); i++) {

                SurveyLines surveyLines = new SurveyLines();
                surveyLines.setSurvey_code(surveyLinesArrayList.get(i).getSurvey_code());
                surveyLines.setQuestion_code(surveyLinesArrayList.get(i).getQuestion_code());
                surveyLines.setQuestion_desc(surveyLinesArrayList.get(i).getQuestion_desc());
                surveyLines.setQuestion_type(surveyLinesArrayList.get(i).getQuestion_type());

                surveyQuestionsTable.create(surveyLines);

            }

            for (int i = 0; i < surveyOptionsArrayList.size(); i++) {

                SurveyLines surveyLines = new SurveyLines();
                surveyLines.setSurvey_code(surveyOptionsArrayList.get(i).getSurvey_code());
                surveyLines.setQuestion_code(surveyOptionsArrayList.get(i).getQuestion_code());
                surveyLines.setOption_text(surveyOptionsArrayList.get(i).getOption_text());

                surveyQuestionOptionTable.create(surveyLines);

            }

            long id6 = dbHandler.create_master_download

                    (
                            "Done"

                    );
            sync_data = 0;

        } catch (Exception e) {

        }

    }*/

    private void generateMDLSyncDateTime() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        mdlSyncDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        mdlSyncTime = sdf.format(outDate);
    }

    private void populate_header_details() {

        globals = ((Globals) getApplicationContext());
        //globals.setLogin_id("BDE002");
        globals.setLogin_id(getIntent().getStringExtra("login_id"));
        globals.setLogin_email(getIntent().getStringExtra("email"));
        globals.setUser_role(getIntent().getStringExtra("login_type"));

        System.out.println("AAA::globals.getLogin_id(); = " + globals.getLogin_id());
        login_id = globals.getLogin_id();
        System.out.println("AAA::populatelogin_id = " + login_id);
        email_id = globals.getLogin_email();
        trip_num = globals.getTrip_num();
        trip_name = globals.getTrip_name();

        Bundle bundle = getIntent().getExtras();
        nav_home = bundle.getString("Home");
        user_role = bundle.getString("user_role");

        back.setVisibility(View.GONE);

        getlocation();
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        // mileage.setEnabled(false);

        Cursor tripDetails = dbHandler.getodometerimage(trip_num);
        int numRows1 = tripDetails.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (tripDetails.moveToNext()) {
                byte[] blob = tripDetails.getBlob(5);
                System.out.println("blob = " + blob);
                System.out.println("blob = " + trip_num);
                int odometer_image = dbHandler.get_length_odometer_image(trip_num);
                Bitmap customerImageBitmap = dbHandler.getTripImage(tripno1);

                if (odometer_image == 0) {

                } else {

                    if (customerImageBitmap != null) {
                        pick_camera.setMaxWidth(1);
                        pick_camera.setImageBitmap(customerImageBitmap);
                        selectedImage = customerImageBitmap;
                        pick_camera.setMaxHeight(1);

                    }
                    System.out.println("TTT::customerImageBitmap = " + customerImageBitmap);

                }
            }
        }

        //Populate SalesRep Details
        Cursor navdetails = dbHandler.getUserDetails(login_id);
        int navdetailscount = navdetails.getCount();
        if (navdetailscount == 0) {

        } else {
            int i = 0;
            while (navdetails.moveToNext()) {

                profile_email.setText(navdetails.getString(0));
                profile_name.setText(navdetails.getString(1));

                Bitmap bdeImageBitmap = dbHandler.getBDELogoImage(login_id);
                if (bdeImageBitmap != null) {
                    bdeProfileImage.setImageBitmap(bdeImageBitmap);
                }

            }
        }

        //Populate Header Details

        Cursor salesrepDetails = dbHandler.getSalesRepDetails(login_id);
        int numRows = salesrepDetails.getCount();
        if (numRows == 0) {
        } else {
            int i = 0;
            while (salesrepDetails.moveToNext()) {
                header_txt.setText(salesrepDetails.getString(2));

            }
        }


        //Populate SalesRep Details
        Cursor setupDetails = dbHandler.getSetupDetails();
        int setupDetailsCount = setupDetails.getCount();
        if (setupDetailsCount == 0) {

        } else {
            int i = 0;
            while (setupDetails.moveToNext()) {

                company_code = Constants.COMPANY_CODE = setupDetails.getString(2);
                tab_code = Constants.TAB_CODE = setupDetails.getString(0);
                sr_code = Constants.SR_CODE = setupDetails.getString(1);
                tab_prefix = Constants.TAB_PREFIX = setupDetails.getString(3);
                Constants.COMPANY_NAME = setupDetails.getString(6);
                //base_url = Constants.BASE_URL = setupDetails.getString(4);

               /* editor.putString(COMPANY_CODE, setupDetails.getString(2));
                editor.putString(TAB_CODE, setupDetails.getString(0));
                editor.putString(SR_CODE, setupDetails.getString(1));
                editor.commit(); */

            }
        }

        setTripImage();
        if (trip_num != null) {

            start_trip.post(new Runnable() {
                @Override
                public void run() {
                    start_trip.performClick();
                }
            });
            plan.setVisibility(View.VISIBLE);
            plan_details.setVisibility(View.VISIBLE);
            populate_journey_plan();

        } else {

        }
    }
    void setTripImage(){
      /*  String fileName = "trip_" + tripno1 + "_" + save_trip_date + ".png";
        fileUtils.storeImage(bitmap, fileName, tripMileageFolderPath);
        dbHandler.updatemileageimage(tripno1, tripMileageFolderPath + File.separator + fileName, fileName);*/


        Bitmap customerImageBitmap = dbHandler.getTripImage(tripno1);
        if (customerImageBitmap != null) {
            pick_camera.setImageBitmap(customerImageBitmap);
        }

      /*  File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath()
                + "/KSFA/CustomerImages/" + customerDetails.getString(36));

        File imgFile = new File(String.valueOf(dir));

        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
            myImage.setImageBitmap(myBitmap);

        }*/
    }

    private void initializeViews() {

        start_time = findViewById(R.id.start_time);
        total_trips = findViewById(R.id.total_trips);
        expandableListView = findViewById(R.id.expandableListView);
        start_time = findViewById(R.id.start_time);
        total_trips = findViewById(R.id.total_trips);
        customer_checkin = findViewById(R.id.customer_checkin);
        transaction = findViewById(R.id.transaction);
        btnJP = findViewById(R.id.btnJP);
        btnNotes = findViewById(R.id.btnNotes);
        header_txt = findViewById(R.id.toolbar_title);
        salesrep_name = findViewById(R.id.sales_rep_name);
        profile_name = navHeader.findViewById(R.id.profile_name);
        profile_email = navHeader.findViewById(R.id.profile_email);
        bdeProfileImage = navHeader.findViewById(R.id.navHeaderBdeImageView);

        //  manager = findViewById(R.id.manager);
        //  region = findViewById(R.id.region);
        gps = findViewById(R.id.gps);
        pick_camera = findViewById(R.id.pick_camera);
        pick_camera1 = findViewById(R.id.pick_camera1);
        start_trip = findViewById(R.id.start_trip);
        mileage = findViewById(R.id.mileage);
        sr_image = findViewById(R.id.sr_image);
        //bdeImageView = findViewById(R.id.nav_header_image);
        plan_details = findViewById(R.id.plan_details);
        plan = findViewById(R.id.plan);
        img_metrics = findViewById(R.id.img_metrics);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        today_activity = findViewById(R.id.today_activity);
        today_trip = findViewById(R.id.today_trip);
        idle_time = findViewById(R.id.idle_time);
        distance_travelled = findViewById(R.id.distance_travelled);
        img_map = findViewById(R.id.img_map);
        back = findViewById(R.id.back);
        //total_trips=findViewById(R.id.total_trips);
        // start_time=findViewById(R.id.start_time);
        sharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        tdate = findViewById(R.id.date);
        ttime = findViewById(R.id.time);
        tripno = findViewById(R.id.trip_no);
        marquee_txt = findViewById(R.id.marquee);
        orderValue = findViewById(R.id.orderValueTextView);
        invoiceValue = findViewById(R.id.invoiceValueTextView);
        collectionValue = findViewById(R.id.collectionValueTextView);
        orderCurveImage = findViewById(R.id.orderCurveImage);
        invoiceCurveImage = findViewById(R.id.invoiceCurveImage);
        collectionCurveImage = findViewById(R.id.collectionCurveImage);
    }

    private void after_populate_journey_plans() {

        plan.setVisibility(View.VISIBLE);
        plan_details.setVisibility(View.VISIBLE);
        populate_journey_plan();
        //mileage.setEnabled(false);
        btnJP.setVisibility(View.GONE);

    }

    private void start_stop(View v) {


        if (startstop.equals("Start")) {

            //drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            start_trip.setImageResource(R.drawable.stop_btn);
            startstop = "Stop";
            today_activity.setVisibility(View.GONE);
            today_trip.setVisibility(View.GONE);
            long date = System.currentTimeMillis();

            SimpleDateFormat sdfd = new SimpleDateFormat("dd MMM yy");
            SimpleDateFormat sdft = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat sdft1 = new SimpleDateFormat("kk:mm");

            String dateString = sdfd.format(date);
            String timeString = sdft.format(date);
            String timeString1 = sdft1.format(date);

            btnJP.setVisibility(View.GONE);

            if ("Home".equals(nav_home)) {
                //mileage.setEnabled(false);
                today_activity.setVisibility(View.GONE);
                today_trip.setVisibility(View.GONE);
            } else {
                // mileage.setEnabled(true);
            }
            nav_home = "";

            if (trip_num != null) {

                tdate.setText(dateString);
                ttime.setText(timeString);
                tripno.setText(trip_num);
                mileage.setText(globals.getMileage());
                mileage.setEnabled(false);
                //Populate Header Details
                /*Cursor tripDetails = dbHandler.getodometerimage(trip_num);
                int numRows = tripDetails.getCount();
                if (numRows == 0) {
                    // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
                } else {
                    int i = 0;
                    while (tripDetails.moveToNext()) {
                        byte[] blob = tripDetails.getBlob(5);
                        int odometer_image = dbHandler.get_length_odometer_image(trip_num);
                        if (odometer_image == 0) {

                        } else {
                            Bitmap bmp = convertByteArrayToBitmap(blob);
                            selectedImage = bmp;
                            pick_camera.setImageBitmap(bmp);
                        }
                    }
                } */

            } else {

                startService(new Intent(getApplicationContext(),GPSTrackerService.class));
               /* Intent lintent = new Intent(getBaseContext(), GPSTrackerService.class);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(lintent);
                } else {
                    startService(lintent);
                }
*/
                after_populate_journey_plan();
                plan_details.setVisibility(View.VISIBLE);

                // Toast.makeText(getApplicationContext(), " null Trip", Toast.LENGTH_LONG).show();
                tdate.setText(dateString);
                ttime.setText(timeString);
                int minteger = dbHandler.get_trip_number();

                minteger = minteger + 1;
                String trip_no = String.valueOf(minteger);
                globals.setTrip_num(trip_no);
                tripno1 = globals.getTrip_num();
                // tripno1 = tripno.getText().toString();
                String tdate1 = tdate.getText().toString();
                String ttime1 = ttime.getText().toString();
                SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date myDate = new Date();
                save_trip_date = timeStampFormat.format(myDate);
                if ("".equals(mileage_val)) {
                    mileage_val = "0";

                } else {
                    mileage_val = mileage.getText().toString();

                }

                long id = dbHandler.create_trip_details(trip_no, save_trip_date, timeString1, login_id, null, null, mileage_val);

                if (id <= 0) {
                    //  Toast.makeText(getApplicationContext(), "Error in Starting the Trip", Toast.LENGTH_LONG).show();

                } else {
                    //  Toast.makeText(getApplicationContext(), "Trip Started", Toast.LENGTH_LONG).show();

                }
                String trip_name = dbHandler.get_trip_name();
                globals.setTrip_name(trip_name);

                tripno.setText(trip_no);

                Bitmap customerImageBitmap = dbHandler.getTripImage(tripno1);
                if (customerImageBitmap != null) {
                    pick_camera.setImageBitmap(customerImageBitmap);
                }

            }

        } else {

            AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
            alertbox.setTitle("Do you want to stop the trip ?");
            alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {


                    long date = System.currentTimeMillis();
                    SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat sdft = new SimpleDateFormat("hh:mm a");
                    SimpleDateFormat sdft1 = new SimpleDateFormat("kk:mm");

                    String dateString = sdfd.format(date);
                    String timeString = sdft1.format(date);

                    dbHandler.updatetripstopdate(tripno.getText().toString(), dateString, timeString);

                    start_trip.setImageResource(R.drawable.start_button);
                    startstop = "Start";
                    today_activity.setVisibility(View.VISIBLE);
                    today_trip.setVisibility(View.VISIBLE);
                    // mileage.setEnabled(false);
                    tripno.setText("");
                    tdate.setText("");
                    ttime.setText("");
                    dialog.dismiss();
                    if (numRows1 == 0) {
                    } else {
                        plan.setVisibility(View.INVISIBLE);
                        plan_details.setVisibility(View.INVISIBLE);
                        userList.clear();
                        adapter.notifyDataSetChanged();
                    }
                    trip_num = null;
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    globals.setTrip_num(null);
                    mileage.setText("");
                    pick_camera.setImageResource(R.drawable.layer_1219);
                    today_trip_details();
                    btnJP.setVisibility(View.VISIBLE);
                    mileage.setEnabled(true);
                    stop_gps_service();


                }

            })

                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    });

            alertbox.show();

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                pick_camera.setEnabled(true);

            }
        }
    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {

        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    private void prepareListData() {
        headerList = new ArrayList<String>();
        childList = new HashMap<String, List<String>>();


        headerList.add("Customer List");
        headerList.add("Transaction List");
        headerList.add("Meetings");
        headerList.add("CRM List");
        headerList.add("Journey Plan");
        headerList.add("View Stock");
        headerList.add("Stock Receipt");
        headerList.add("Stock Adjustment");
        headerList.add("Stock Return");
        headerList.add("Stock Issue");
        headerList.add("Expense Booking");
        headerList.add("Device Info");
        headerList.add("My Messages");
        headerList.add("Sync Data");
        headerList.add("Logout");

        // Adding child data
        List<String> transactionList = new ArrayList<String>();

        transactionList.add("Orders");
        transactionList.add("Invoices");
        transactionList.add("Collections");
        transactionList.add("Meetings");
        transactionList.add("CRM List");
        transactionList.add("Enquiry");
        transactionList.add("Quotation");
        transactionList.add("Waybill");
        // transactionList.add("Opportunities");
        transactionList.add("Survey");
        transactionList.add("Feedback");
        transactionList.add("Competitor Info");
        transactionList.add("Expired Products");
        transactionList.add("Customer Return Products");
        transactionList.add("Stock Return Products");
        transactionList.add("Stock Issue Products");
        //transactionList.add("Learning Stats");

        childList.put(headerList.get(1), transactionList); // Header, Child data
        List<String> syncDataList = new ArrayList<String>();

        syncDataList.add("Import Master Data");
        syncDataList.add("Export Transactions");
        syncDataList.add("Post Images");
        syncDataList.add("DB Backup");
        childList.put(headerList.get(13), syncDataList);

    }

    public void setProgressBar() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Please wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressBarStatus < 100) {
                    progressBarStatus += 30;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    progressBarbHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }
                if (progressBarStatus >= 100) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progressBar.dismiss();
                }

            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    selectedImage = getResizedBitmap(selectedImage, 50);
                    pick_camera.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAPTURE_PHOTO) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
                pick_camera.setDrawingCacheEnabled(true);
                pick_camera.buildDrawingCache();
                Bitmap bitmap = pick_camera.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] mileage_data = baos.toByteArray();
                String fileName = tripno1 + "_" + save_trip_date + ".png";
                fileUtils.storeImage(bitmap, fileName, tripMileageFolderPath);
                dbHandler.updatemileageimage(tripno1, tripMileageFolderPath + File.separator + fileName, fileName);

            }
        }

    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");

        //set Progress Bar
        setProgressBar();
        //set profile picture form camera
        pick_camera.setMaxWidth(1);
        selectedImage = thumbnail;
        pick_camera.setImageBitmap(thumbnail);
        pick_camera.setMaxHeight(1);

    }

    public void populate_journey_plan() {
        // Toast.makeText(getApplicationContext(), "Login ID: " + login_id + "TripNo: " + trip_num, Toast.LENGTH_LONG).show();

        //Working with List to Show Data's
        userList = new ArrayList<SalesRep_Customer_List>();
        Cursor data = dbHandler.getSalesrepCustomers(login_id, trip_num);
        numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                serial_num = serial_num + 1;
                user = new SalesRep_Customer_List(data.getString(1), data.getString(2), data.getString(3), data.getString(0), data.getString(4), data.getString(5));
                userList.add(i, user);
                //System.out.println(userList.get(i).getFirstName());
                i++;


            }

            adapter = new SalesRepCustomerListAdapter(getApplicationContext(), R.layout.list_customer_start_stop, userList, login_id, tripno1, user_role);
            ListView customerTable = findViewById(R.id.customer_list);
            customerTable.setAdapter(adapter);
            serial_num = 0;
        }
    }

    private void setDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }

    public void showsrdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_sr_details, null);
        final TextView sr_name = alertLayout.findViewById(R.id.sr_name_details);
        final TextView sr_rm = alertLayout.findViewById(R.id.sr_rm_name);
        final TextView sr_rgn = alertLayout.findViewById(R.id.sr_region);
        final TextView sr_whouse = alertLayout.findViewById(R.id.sr_warehouse);
        final TextView sr_gps = alertLayout.findViewById(R.id.sr_gps);

        sr_name.setText(sr_name_details);
        sr_rm.setText(sr_rm_name);
        sr_rgn.setText(sr_region);
        sr_whouse.setText(sr_warehouse);
        sr_gps.setText(lat_lng);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void today_trip_details() {

        String ideal_time = dbHandler.get_ideal_time();
        idle_time.setText(ideal_time);


        Cursor gettrip_starttime = dbHandler.gettrip_starttime();
        int gettrip_starttimecount = gettrip_starttime.getCount();
        if (gettrip_starttimecount == 0) {

        } else {
            int i = 0;
            while (gettrip_starttime.moveToNext()) {

                start_time.setText(gettrip_starttime.getString(0));


            }
        }

        Cursor gettrip_count = dbHandler.gettrip_count();
        int gettrip_count1 = gettrip_count.getCount();
        if (gettrip_count1 == 0) {

        } else {
            int i = 0;
            while (gettrip_count.moveToNext()) {

                total_trips.setText(gettrip_count.getString(0));


            }
        }

        Cursor getcustomer_count = dbHandler.getcustomer_count();
        int getcustomer_count1 = getcustomer_count.getCount();
        if (getcustomer_count1 == 0) {

        } else {
            int i = 0;
            while (getcustomer_count.moveToNext()) {

                customer_checkin.setText(getcustomer_count.getString(0));

            }
        }

        Cursor getTransactionCount = dbHandler.getTransactionCount();
        int getTransactionCount1 = getTransactionCount.getCount();
        if (getTransactionCount1 == 0) {

        } else {
            int i = 0;
            while (getTransactionCount.moveToNext()) {

                int order_count = getTransactionCount.getInt(0);
                int invoice_count = getTransactionCount.getInt(1);
                // int primary_collection_count=getTransactionCount.getInt(2);
                //int secondary_collection_count=getTransactionCount.getInt(3);
                int total_transactions = order_count + invoice_count;
                transaction.setText(String.valueOf(total_transactions));

            }
        }
    }

    private void distance_travelled() {

        // Calculate Distance Travelled
        Cursor gpsdistance = dbHandler.getGPSdistance();
        int gpsdistancecount = gpsdistance.getCount();
        if (gpsdistancecount == 0) {

        } else {
            int i = 0;
            while (gpsdistance.moveToNext()) {

                gpsDistance = new GPSDistance(gpsdistance.getDouble(0), gpsdistance.getDouble(1));
                gpsTrackerModelArrayList.add(i, gpsDistance);

            }
        }

        double total_distance = 0.00;

        for (int i = 0; i < gpsTrackerModelArrayList.size() - 1; i++) {

            double distance_km = distance(gpsTrackerModelArrayList.get(i).getLatitude(), gpsTrackerModelArrayList.get(i).getLongitude(), gpsTrackerModelArrayList.get(i + 1).getLatitude(), gpsTrackerModelArrayList.get(i + 1).getLongitude());
            total_distance += distance_km;

            String formatted_distance = formatter1.format(total_distance);
            distance_travelled.setText(formatted_distance + " KM");

        }

    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta));
        dist = acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        double km = dist * 1.60934;

        return (km);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void stop_gps_service() {

        stopService(new Intent(getApplicationContext(), GPSTrackerService.class));

    }

    private void stopBackgroundService() {

        stopService(new Intent(getApplicationContext(), GPSTrackerService.class));
//        stopService(new Intent(getApplicationContext(), PostingDataService.class));
        stopService(new Intent(getApplicationContext(), MyService.class));
        stopService(new Intent(getApplicationContext(), PostingDataGPSService.class));
        stopService(new Intent(getApplicationContext(), MasterRDLService.class));
        stopService(new Intent(getApplicationContext(), FetchingDataService.class));

    }

    private void getlocation() {


        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            lat = formatter.format(latitude);
            lng = formatter.format(longitude);
            lat_lng = lat + " : " + lng;
            //lng = Double.toString(longitude);

            //lat = Double.toString(latitude);
            //lng = Double.toString(longitude);
            gps.setText(lat_lng);

        } else {

            gpsTracker.showSettingsAlert();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
