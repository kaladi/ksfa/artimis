package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.FeedbackReport;

public class FeedbackActionTakenAdapter extends RecyclerView.Adapter<FeedbackActionTakenAdapter.ViewHolder> {

    public ArrayList<FeedbackReport> mDataset;
    public Context context;
    public RecyclerView feedbackReportsRecyclerView;
    String login_id, feedback_type = "";
    ImageView actionbarBackButton, deviceInfo;
    String checkin_time = "";
    String customer_id = "";
    TextView actionbarTitle, feedbackDateTextView,
            feedbackTypeTextView,
            actionTakenTextView,
            customerNameTextView,
            customerFeedbackTextView;
    MyDBHandler myDBHandler;
    private RecyclerView.Adapter feedbackReportsAdapter;
    private RecyclerView.LayoutManager feedbackReportsLayoutManager;
    private TextView marqueeText;

    public FeedbackActionTakenAdapter(ArrayList<FeedbackReport> myDataset) {
        mDataset = myDataset;
    }

    public void add(FeedbackReport item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(FeedbackReport item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public FeedbackActionTakenAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_feedback_actiontaken, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.feedbackDate.setText(mDataset.get(position).getFeedback_date());
        viewHolder.actionTaken.setText(mDataset.get(position).getAction_taken());
//        System.out.println("getFeedback_status() = " + mDataset.get(position).getFeedback_status() );
        if(mDataset.get(position).getFeedback_status().equals(0) || mDataset.get(position).getFeedback_status() == null )
        viewHolder.status.setText("Closed");
        else viewHolder.status.setText("Open");


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView feedbackType, feedbackDate, customerName, customerFeedback, actionTaken,status;
        public View vm;

        public ViewHolder(View v) {
            super(v);

            feedbackDate = v.findViewById(R.id.feedbackDateTextView);
            actionTaken = v.findViewById(R.id.actionTakenTextView);
            status = v.findViewById(R.id.statusTextView);

            vm = v;

        }
    }


}