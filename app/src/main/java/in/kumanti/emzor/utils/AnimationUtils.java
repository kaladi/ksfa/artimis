package in.kumanti.emzor.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

public class AnimationUtils {
    // Needed to keep track of a view that is fading out since visibility won't be set until
    // animation is complete.
    private static Set<View> animatingSet = Collections.newSetFromMap(new IdentityHashMap<View, Boolean>());

    public static void fadeIn(View view) {
        if (!animatingSet.remove(view) && view.getVisibility() == View.VISIBLE) {
            return;
        }
        cancel(view);
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0);
        view.animate().alpha(1).setListener(null);
    }

    public static void fadeOut(final View view) {
        if (view.getVisibility() != View.VISIBLE) {
            return;
        }
        cancel(view);
        animatingSet.add(view);
        view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setAlpha(1);
                // Set to INVISIBLE instead of GONE so that the view retains it's measured size.
                // This is important for more complex animations where it's size is needed before
                // it's animated.
                view.setVisibility(View.INVISIBLE);
                animatingSet.remove(view);
            }
        });
    }

    public static void cancel(View view) {
        animatingSet.remove(view);
        view.animate().cancel();
    }
}
