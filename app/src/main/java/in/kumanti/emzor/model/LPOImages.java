package in.kumanti.emzor.model;


public class LPOImages {

    public int imageId;
    public String orderCode;
    public String lpoImageName;
    public String lpoImagePath;
    public String imageType;
    public String createdAt;
    public String updatedAt;
    public String isUploaded;
    public String visitSeqNumber;
    private String tabCode;
    private String companyCode;
    private String srCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }

    public String getLpoImageName() {
        return lpoImageName;
    }

    public void setLpoImageName(String lpoImageName) {
        this.lpoImageName = lpoImageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }


    public String getLpoImagePath() {
        return lpoImagePath;
    }

    public void setLpoImagePath(String lpoImagePath) {
        this.lpoImagePath = lpoImagePath;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getVisitSeqNumber() {
        return visitSeqNumber;
    }

    public void setVisitSeqNumber(String visitSeqNumber) {
        this.visitSeqNumber = visitSeqNumber;
    }
}
