package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.CrmNewProducts;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_NEW_PRODUCTS;

public class CrmNewProductsTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_CRM_NEW_PRODUCT_ID = "productId";
    private static final String COLUMN_CRM_NEW_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_CRM_NEW_PRODUCT_CODE = "productCode";
    private static final String COLUMN_CRM_NEW_PRODUCT_NAME = "productName";
    private static final String COLUMN_CRM_NEW_PRODUCT_DESCRIPTION = "description";
    private static final String COLUMN_CRM_NEW_PRODUCT_UOM = "uom";
    private static final String COLUMN_CRM_NEW_PRODUCT_TYPE = "type";
    private static final String COLUMN_CRM_NEW_PRODUCT_BATCH = "batch";
    private static final String COLUMN_CRM_NEW_PRODUCT_CATEGORY = "category";
    private static final String COLUMN_CRM_RANDOM_NUMBER = "randomNumber";
    private static final String COLUMN_CRM_NEW_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_CRM_NEW_PRODUCT_UPDATED_AT = "updatedAt";


    public Context context;


    public CrmNewProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        
       // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_NEW_PRODUCTS);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_NEW_PRODUCTS + "(" +
                COLUMN_CRM_NEW_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CRM_NEW_CUSTOMER_CODE + " TEXT, " +
                COLUMN_CRM_NEW_PRODUCT_CODE + " TEXT, " +
                COLUMN_CRM_NEW_PRODUCT_NAME + " TEXT, " +
                COLUMN_CRM_NEW_PRODUCT_DESCRIPTION + " TEXT, " +
                COLUMN_CRM_NEW_PRODUCT_UOM + " TEXT, " +
                COLUMN_CRM_NEW_PRODUCT_TYPE + " TEXT, " +
                COLUMN_CRM_NEW_PRODUCT_BATCH + " TEXT, " +
                COLUMN_CRM_NEW_PRODUCT_CATEGORY + " TEXT, " +
                COLUMN_CRM_RANDOM_NUMBER + " INTEGER, " +
                COLUMN_CRM_NEW_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CRM_NEW_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmNewProducts crmNewProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CRM_NEW_PRODUCT_CODE, crmNewProducts.getProductCode());
        values.put(COLUMN_CRM_NEW_CUSTOMER_CODE, crmNewProducts.getCustomerCode());
        values.put(COLUMN_CRM_NEW_PRODUCT_NAME, crmNewProducts.getProductName());
        values.put(COLUMN_CRM_NEW_PRODUCT_DESCRIPTION, crmNewProducts.getDescription());
        values.put(COLUMN_CRM_NEW_PRODUCT_UOM, crmNewProducts.getUom());
        values.put(COLUMN_CRM_NEW_PRODUCT_TYPE, crmNewProducts.getType());
        values.put(COLUMN_CRM_NEW_PRODUCT_BATCH, crmNewProducts.getBatch());
        values.put(COLUMN_CRM_NEW_PRODUCT_CATEGORY, crmNewProducts.getCategory());
        values.put(COLUMN_CRM_RANDOM_NUMBER,crmNewProducts.getRandomNo());
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_NEW_PRODUCTS, null, values);
        db.close();
        return id;
    }


   /* public ArrayList<CrmNewProducts> getNewCrmProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_PRODUCTS +" ORDER BY productName ASC" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        Log.d("ViewStock--","Get Products  " );
        ArrayList<crmProducts> products = new ArrayList<>();
        while (cursor.moveToNext()) {
            crmProducts product = new crmProducts();
            Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            product.setProductId(cursor.getInt(0));
            product.setGroupCode(cursor.getString(1));
            product.setProductCode(cursor.getString(2));
            product.setProductName(cursor.getString(3));
            product.setProductPrice(cursor.getString(4));
            product.setLongDescription(cursor.getString(5));
            product.setProductUom(cursor.getString(6));
            product.setProductType(cursor.getString(7));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }*/


    public ArrayList<CrmNewProducts> getCrmNewProductsForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CRM_NEW_PRODUCTS + " WHERE DATE(datetime(createdAt,'unixepoch')) >= date('now','-5 Days')";

        ArrayList<CrmNewProducts> crmNewProductsArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            CrmNewProducts crmNewProducts = new CrmNewProducts();
            crmNewProducts.setTabCode(Constants.TAB_CODE);
            crmNewProducts.setCompanyCode(Constants.COMPANY_CODE);
            crmNewProducts.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            crmNewProducts.setProductId(id);
            crmNewProducts.setCustomerCode(cursor.getString(1));
            crmNewProducts.setProductCode(cursor.getString(2));
            crmNewProducts.setProductName(cursor.getString(3));
            crmNewProducts.setDescription(cursor.getString(4));
            crmNewProducts.setUom(cursor.getString(5));
            crmNewProducts.setType(cursor.getString(6));
            crmNewProducts.setBatch(cursor.getString(7));
            crmNewProducts.setCategory(cursor.getString(8));
            crmNewProducts.setRandomNo(cursor.getString(9));
            crmNewProducts.setCreatedAt(cursor.getString(10));
            crmNewProducts.setUpdatedAt(cursor.getString(11));

            crmNewProductsArrayList.add(crmNewProducts);
        }
        cursor.close();
        sqlWrite.close();
        return crmNewProductsArrayList;
    }

    public ArrayList<CrmNewProducts> getProductsByCustomer(String customerCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //String selectQuery = " SELECT * FROM " + TABLE_CRM_NEW_PRODUCTS + " WHERE customerCode = ? createdAt >= date('now','-5 Days')";
        String selectQuery = " SELECT * FROM " + TABLE_CRM_NEW_PRODUCTS+ " WHERE customerCode = ?";

        ArrayList<CrmNewProducts> crmNewProductsArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerCode});

        while (cursor.moveToNext()) {
            CrmNewProducts crmNewProducts = new CrmNewProducts();
            crmNewProducts.setTabCode(Constants.TAB_CODE);
            crmNewProducts.setCompanyCode(Constants.COMPANY_CODE);
            crmNewProducts.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            crmNewProducts.setProductId(id);
            crmNewProducts.setCustomerCode(cursor.getString(1));
            crmNewProducts.setProductCode(cursor.getString(2));
            crmNewProducts.setProductName(cursor.getString(3));
            crmNewProducts.setDescription(cursor.getString(4));
            crmNewProducts.setUom(cursor.getString(5));
            crmNewProducts.setType(cursor.getString(6));
            crmNewProducts.setBatch(cursor.getString(7));
            crmNewProducts.setCategory(cursor.getString(8));
            crmNewProducts.setRandomNo(cursor.getString(9));
            crmNewProducts.setCreatedAt(cursor.getString(10));
            crmNewProducts.setUpdatedAt(cursor.getString(11));

            crmNewProductsArrayList.add(crmNewProducts);
        }
        cursor.close();
        sqlWrite.close();
        return crmNewProductsArrayList;
    }
    public int getRandomNumber() {
        int crmId = 0;

        String selectQuery = "select randomNumber from "+TABLE_CRM_NEW_PRODUCTS+"  order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            crmId = cursor.getInt(0);
            cursor.close();
        }

        return crmId;
    }
}
