package in.kumanti.emzor.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.StockReceiptBdeReceiveAdapter;
import in.kumanti.emzor.adapter.ViewStockOrderAdapter;
import in.kumanti.emzor.eloquent.BDEListTable;
import in.kumanti.emzor.eloquent.BDEReceiveFileListTable;
import in.kumanti.emzor.eloquent.BdeReceiveBatchProductsTable;
import in.kumanti.emzor.eloquent.BdeReceiveProductsTable;
import in.kumanti.emzor.eloquent.DistributorsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.model.BDEList;
import in.kumanti.emzor.model.BDEReceiveFilesList;
import in.kumanti.emzor.model.BdeReceiveBatchProducts;
import in.kumanti.emzor.model.BdeReceiveProducts;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.model.StockViewInvoiceList;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;

import static android.content.Context.MODE_PRIVATE;
import static in.kumanti.emzor.activity.MainActivity.PREF_NAME;


public class StockReceiptBDEReceiveFragment extends Fragment {

    public RecyclerView bdeReceiveProdRecyclerView;
    Button bdeStockImportButton;
    LinearLayout bdeNameContainer, productNamesContainer, receivingProductsHeaderContainer, receivingProductsListContainer;
    CustomSearchableSpinner bdeNameSpinner, productSpinner;
    int random_num = 0;
    String lat_lng, nav_type, customer_type, tab_prefix;
    Handler receiveHandler;
    BDEListTable bdeListTable;
    ArrayList<BDEList> bdeListArrayList;
    ArrayAdapter<BDEList> bdeListSpinnerArrayAdapter;
    ArrayAdapter<Product> productSpinnerArrayAdapter;
    ArrayList<Product> productArrayList;
    ArrayList<Product> newProductList;
    ProductTable productTable;
    BdeReceiveProductsTable bdeReceiveProductsTable;
    BdeReceiveBatchProductsTable bdeReceiveBatchProductsTable;
    ArrayList<BdeReceiveProducts> stockReceiptBdeReceiveProductArrayList = new ArrayList<BdeReceiveProducts>();
    ArrayList<StockReceipt> stockReceiptArrayList;
    Context mContext;
    MyDBHandler dbHandler;
    DistributorsTable distributorsTable;
    String login_id = "", bdeNameString = "", toBdeCode = "";
    RecyclerViewItemListener listener, listener1;
    ImageButton saveBdeReceiveButton;
    String fromBdeCode, sr_name_details, sr_rm_name, sr_region, sr_warehouse;
    String companyName;
    ProgressDialog dialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private RecyclerView.Adapter bdeReceiveProdAdapter;
    private RecyclerView.LayoutManager bdeReceiveProdLayoutManager;
    private Context contextForDialog = null;

    public StockReceiptBDEReceiveFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_stock_receipt_bde_receive, container, false);
        mContext = rootView.getContext();

        contextForDialog = mContext;
        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        dbHandler = new MyDBHandler(mContext, null, null, 1);
        distributorsTable = new DistributorsTable(mContext);

        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
        }
        tab_prefix = Constants.TAB_PREFIX;


        productTable = new ProductTable(mContext);
        bdeListTable = new BDEListTable(mContext);
        bdeReceiveProductsTable = new BdeReceiveProductsTable(mContext);
        bdeReceiveBatchProductsTable = new BdeReceiveBatchProductsTable(mContext);
        bdeStockImportButton = rootView.findViewById(R.id.stockReceiptBDEStockImportButton);

        bdeNameContainer = rootView.findViewById(R.id.stockReceivingBdeNameContainer);

        receivingProductsHeaderContainer = rootView.findViewById(R.id.bdeReceiptProductsHeaderContainer);
        receivingProductsListContainer = rootView.findViewById(R.id.bdeReceiptReceivingStockListContainer);


        bdeNameSpinner = rootView.findViewById(R.id.bdeReceiptBdeNameSpinner);

        bdeReceiveProdRecyclerView = rootView.findViewById(R.id.stockReceiveProductRecyclerView);

        getCurrentLoginBDEInfo();
        getCompanyInfo();

        bdeReceiveProdRecyclerView.setHasFixedSize(true);
        bdeReceiveProdLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        bdeReceiveProdRecyclerView.setLayoutManager(bdeReceiveProdLayoutManager);

        listener = (view, position) -> {
            Log.d("Popup", "Listener rece");
            //showStockView(stockReceiptBdeReceiveProductArrayList.get(position).getProductCode(),stockReceiptBdeReceiveProductArrayList.get(position).getProductUom(), stockReceiptBdeReceiveProductArrayList.get(position).getProductName(),stockReceiptBdeReceiveProductArrayList.get(position).getIssueQuantity(),stockReceiptBdeReceiveProductArrayList.get(position).getBdeReceiveBatchProductsArrayList());
        };
        getBdeReceiveList(null);

        bdeStockImportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new ProgressDialog(contextForDialog);
                dialog.setMessage("Downloading Stock Transfer, Please Wait!!");
                dialog.setCancelable(false);
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.show();
                receiveHandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        dialog.dismiss();
                        if (msg.what == 1) {//
                            Toast.makeText(mContext, "Stock Receive has been Completed", Toast.LENGTH_SHORT).show();
                            // Log.d("Multi", "Start Stop setIsDownloaded After Downloaded--" +mdlSync.getIsDownloaded());
                            getBdeList();
                        } else {
                            Toast.makeText(mContext, "Unable to process your transaction.", Toast.LENGTH_LONG).show();

                        }


                    }
                };

                Thread receiveThread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        if (receiveStockFromFTP())
                            receiveHandler.sendEmptyMessage(1);
                        else
                            receiveHandler.sendEmptyMessage(0);
                    }
                });


                receiveThread.start();


            }
        });


        getBdeList();

        bdeNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bdeNameSpinner.isSpinnerDialogOpen = false;
                BDEList bdeList = (BDEList) parent.getItemAtPosition(position);
                bdeNameString = bdeList.getBdeName();
                toBdeCode = bdeList.bdeCode;
                getBdeReceiveList(bdeList.bdeCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                bdeNameSpinner.isSpinnerDialogOpen = false;

            }
        });


        return rootView;
    }

    private boolean receiveStockFromFTP() {
        try {

            BDEReceiveFileListTable bdeReceiveFileListTable = new BDEReceiveFileListTable(mContext);
            stockReceiptArrayList = new ArrayList<StockReceipt>();

            String serverFolderName = companyName + "_" + fromBdeCode;
            FTPClient con = new FTPClient();
            con.connect(Constants.FTP_HOST_NAME);

            Log.d("FileImport", "FilePath Connected");
            if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {
                File filepath = Environment.getExternalStorageDirectory();
                Log.d("FileImport", "FileImport Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
                FTPFile[] listFiles = con.listFiles("/home/vistagftp/vistagftp/BDETransfer/" + serverFolderName);

                for (FTPFile fp : listFiles) {
                    if (fp.getName().equals("batch"))
                        continue;
                    Log.d("FileImport", fp.getName());
                    if (bdeReceiveFileListTable.getBdeReceiveFilesListByName(fp.getName()) == null) {
                        Log.d("FileImport", "FileRead " + fp.getName());
                        //Check if already s
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            //Download the file from ftp to local
                            File stockDir = new File(filepath.getAbsolutePath() + "/KSFA/", "BDEReceive");
                            stockDir.mkdir();

                            String localFilePath = filepath.getAbsolutePath() + "/KSFA/BDEReceive/" + fp.getName();

                            FileOutputStream out = new FileOutputStream(new File(localFilePath));
                            con.retrieveFile("/home/vistagftp/vistagftp/BDETransfer/" + serverFolderName + "/" + fp.getName(), out);
                            out.close();
                            //con.deleteFile("TAB/BDETransfer/32" + fp.getName());
                            Log.d("FileImport", "FileRead LocalWrite done" + fp.getName());
                            //Read and Write into Database
                            File file = new File(localFilePath);
                            String line = "";
                            String cvsSplitBy = ",";
                            FileReader fileReader = new FileReader(file);
                            BufferedReader br = new BufferedReader(fileReader);

                            while ((line = br.readLine()) != null) {
                                // use comma as separator
                                String[] row = line.split(cvsSplitBy);
                                String stockIssueNumber = row[0];
                                String stockIssueDate = row[1];
                                String fromBdeCode = row[2];
                                String toBdeCode = row[3];
                                String productCode = row[4];
                                String productName = row[5];
                                String productUom = row[6];
                                String issueQuantity = row[7];
                                String issuePrice = row[8];
                                String issueValue = row[9];
                                String isBatchControlled = row[10];

                                Log.d("FileImport", "Country [code= " + row[0] + " , name=" + row[1] + "]");
                                Log.d("FileImport", "Data " + row[0]);
                                BdeReceiveProducts bdeReceiveProducts = new BdeReceiveProducts();
                                bdeReceiveProducts.setStockIssueNumber(stockIssueNumber);
                                bdeReceiveProducts.setStockIssueDate(stockIssueDate);
                                bdeReceiveProducts.setFromBdeCode(fromBdeCode);
                                bdeReceiveProducts.setToBdeCode(toBdeCode);
                                bdeReceiveProducts.setProductCode(productCode);
                                bdeReceiveProducts.setProductName(productName);
                                bdeReceiveProducts.setProductUom(productUom);
                                bdeReceiveProducts.setIssueQuantity(issueQuantity);
                                bdeReceiveProducts.setIssuePrice(issuePrice);
                                bdeReceiveProducts.setIssueValue(issueValue);
                                bdeReceiveProducts.setIsBatchControlled(isBatchControlled);
                                long id = bdeReceiveProductsTable.create(bdeReceiveProducts);
                                Log.d("FileImport", "Inserted " + id);
                                System.out.println("First column of line: " + row[0]);

                                StockReceipt srwp = new StockReceipt();
                                srwp.setProductId(productCode);
                                srwp.setProductName(productName);
                                srwp.setProductPrice(issuePrice);
                                int i = (int) Float.parseFloat(issueQuantity);
                                srwp.setProductQuantity(String.valueOf(issueQuantity));
                                srwp.setProductTotalPrice(issueValue);
                                srwp.setProductUom(productUom);
                                srwp.setBatchControlled(isBatchControlled);
                                srwp.setInvoiceCode("");
                                srwp.setStockIssueNumber(stockIssueNumber);
                                srwp.setSourceName(fromBdeCode);
                                stockReceiptArrayList.add(srwp);
                            }
                            BDEReceiveFilesList on = new BDEReceiveFilesList();
                            on.setFileName(fp.getName());

                            bdeReceiveFileListTable.create(on);


                        }
                    }

                }

                FTPFile[] listBatchFiles = con.listFiles("/home/vistagftp/vistagftp/BDETransfer/" + serverFolderName + "/batch");

                for (FTPFile fp : listBatchFiles) {
                    Log.d("FileImport", "Batch Name " + fp.getName());
                    if (bdeReceiveFileListTable.getBdeReceiveFilesListByName(fp.getName()) == null) {
                        //Check if already s
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            //Download the file from ftp to local
                            File stockDir = new File(filepath.getAbsolutePath() + "/KSFA/BDEReceive/", "batch");
                            stockDir.mkdir();

                            String localFilePath = filepath.getAbsolutePath() + "/KSFA/BDEReceive/batch/" + fp.getName();

                            FileOutputStream out = new FileOutputStream(new File(localFilePath));
                            con.retrieveFile("/home/vistagftp/vistagftp/BDETransfer/" + serverFolderName + "/batch/" + fp.getName(), out);
                            out.close();

                            //Read and Write into Database
                            File file = new File(localFilePath);
                            String line = "";
                            String cvsSplitBy = ",";
                            FileReader fileReader = new FileReader(file);
                            BufferedReader br = new BufferedReader(fileReader);

                            while ((line = br.readLine()) != null) {
                                String[] row = line.split(cvsSplitBy);
                                String stockIssueNumber = row[0];
                                String stockIssueDate = row[1];
                                String fromBdeCode = row[2];
                                String toBdeCode = row[3];
                                String productCode = row[4];
                                String expiryDate = row[5];
                                String batchNumber = row[6];
                                String issueQuantity = row[7];
                                Log.d("FileInsert", "Batch " + row[0]);
                                BdeReceiveBatchProducts bdeReceiveBatchProducts = new BdeReceiveBatchProducts();
                                bdeReceiveBatchProducts.setStockIssueNumber(stockIssueNumber);
                                bdeReceiveBatchProducts.setStockIssueDate(stockIssueDate);
                                bdeReceiveBatchProducts.setFromBdeCode(fromBdeCode);
                                bdeReceiveBatchProducts.setToBdeCode(toBdeCode);
                                bdeReceiveBatchProducts.setProductCode(productCode);
                                bdeReceiveBatchProducts.setExpiryDate(expiryDate);
                                bdeReceiveBatchProducts.setBatchNumber(batchNumber);
                                bdeReceiveBatchProducts.setIssueQuantity(issueQuantity);

                                bdeReceiveBatchProductsTable.create(bdeReceiveBatchProducts);
                                System.out.println("First column of line: " + row[0]);
                                StockReceiptBatch stockReceiptBatch = new StockReceiptBatch();
                                stockReceiptBatch.setSrCode(login_id);
                                stockReceiptBatch.setProductCode(productCode);
                                stockReceiptBatch.setExpiryDate(expiryDate);
                                stockReceiptBatch.setBatchNumber(batchNumber);
                                stockReceiptBatch.setBatchQuantity(issueQuantity);
                                stockReceiptBatch.setBatchStatus("Completed");
                                stockReceiptBatch.setInvoiceId("");

                                for (StockReceipt sr : stockReceiptArrayList) {
                                    if (sr.getProductId().equals(productCode)) {
                                        if (sr.getBatchDetailArrayList() != null) {
                                            sr.getBatchDetailArrayList().add(stockReceiptBatch);
                                        } else {
                                            ArrayList<StockReceiptBatch> batchArrayList = new ArrayList<StockReceiptBatch>();
                                            batchArrayList.add(stockReceiptBatch);
                                            sr.setBatchDetailArrayList(batchArrayList);
                                        }
                                    }
                                }

                            }

                            BDEReceiveFilesList on = new BDEReceiveFilesList();
                            on.setFileName(fp.getName());

                            bdeReceiveFileListTable.create(on);


                        }
                    }
                }

                //Inserting in to Stock Receipt
                insertingToStockReceipt();
                return true;
            } else
                return false;

        } catch (Exception e) {
            Log.d("FileInsert", "Error " + e.getMessage());
        }
        return false;
    }

    private void insertingToStockReceipt() {
        int rec_random_num = dbHandler.get_receipt_random_num();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);
        SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate1 = new Date();
        String receiptDate = timeStampFormat1.format(myDate1);
        String receipt_num;
        rec_random_num = rec_random_num + 1;

        if (rec_random_num <= 9) {
            receipt_num = tab_prefix + "SR" + rec_seq_date + "00" + rec_random_num;

        } else if (rec_random_num > 9 & random_num < 99) {
            receipt_num = tab_prefix + "SR" + rec_seq_date + "0" + rec_random_num;

        } else {
            receipt_num = tab_prefix + "SR" + rec_seq_date + rec_random_num;
        }

        String visitSequence = sharedPreferences.getString("VisitSequence", "");

        for (StockReceipt sr : stockReceiptArrayList) {
            if (sr.getProductId().equals("-1"))
                continue;
            if (!TextUtils.isEmpty(sr.getProductQuantity())) {
                long id = dbHandler.createstockreciept(
                        receipt_num,
                        receiptDate,
                        "NGN",
                        login_id,
                        sr.getSourceName(),
                        "BDE Receive",
                        "",
                        sr.getProductId(),
                        sr.getProductUom(),
                        String.valueOf(Integer.parseInt(sr.getProductQuantity())),
                        null,
                        null,
                        "",
                        "",
                        "Completed",
                        rec_random_num,
                        visitSequence,
                        sr.getStockIssueNumber(),
                        login_id);

                if (sr.getBatchControlled().equals("Yes")) {
                    for (StockReceiptBatch srb : sr.getBatchDetailArrayList()) {
                        if (!TextUtils.isEmpty(srb.getBatchQuantity()))
                            dbHandler.insertingProductBatch(srb.getProductCode(), srb.getBatchNumber(), srb.getExpiryDate(), String.valueOf(Integer.parseInt(srb.getBatchQuantity())), "", receipt_num, "", "Completed", "StockReceive");
                    }
                }
            }


        }


    }


    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);


            }
        }

    }

    public void getCurrentLoginBDEInfo() {
        //Populate SalesRep Details
        Log.d("Invoice", "Login---" + login_id);
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {

                fromBdeCode = salesRepDetails.getString(1);
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }
    }

    public void getBdeList() {
        bdeListArrayList = bdeListTable.getBdeByReceiveProducts();

        BDEList bdeList = new BDEList();
        bdeList.setBdeCode("-1");
        bdeList.setBdeName("Select");

        bdeListArrayList.add(0, bdeList);
        bdeNameSpinner.setTitle("Select a BDE");
        bdeNameSpinner.setPositiveButton("OK");

        bdeListSpinnerArrayAdapter = new ArrayAdapter<BDEList>(mContext, android.R.layout.simple_spinner_item, bdeListArrayList);
        bdeListSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        bdeNameSpinner.setAdapter(bdeListSpinnerArrayAdapter);
    }

    public void getBdeReceiveList(String bdeCode) {
        stockReceiptBdeReceiveProductArrayList = bdeReceiveProductsTable.getBdeReceiveProducts(bdeCode);
        bdeReceiveProdAdapter = new StockReceiptBdeReceiveAdapter(stockReceiptBdeReceiveProductArrayList, listener);
        bdeReceiveProdRecyclerView.setAdapter(bdeReceiveProdAdapter);

    }


    public void showStockView(String product_id, String product_uom, String product_name, String stockonhand, ArrayList<BdeReceiveBatchProducts> bdeReceiveBatchProducts) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_create_view_invoice, null);

        ListView stock_list = alertLayout.findViewById(R.id.stock_list);
        TextView txt_stock = alertLayout.findViewById(R.id.txt_stock);
        TextView txt_product_uom = alertLayout.findViewById(R.id.txt_product_uom);
        TextView txt_product_code = alertLayout.findViewById(R.id.txt_product_code);
        TextView txt_product_name = alertLayout.findViewById(R.id.txt_product_name);

        Log.d("Popup", "Listener fun");

        txt_stock.setText(stockonhand);
        txt_product_uom.setText(product_uom);
        txt_product_code.setText(product_id);
        txt_product_name.setText(product_name);

        //Working with List to Show Data's
        ArrayList<StockViewInvoiceList> stockList = new ArrayList<StockViewInvoiceList>();
        for (BdeReceiveBatchProducts brdp : bdeReceiveBatchProducts) {
            StockViewInvoiceList stocks = new StockViewInvoiceList(
                    brdp.getExpiryDate(),
                    brdp.getBatchNumber(),
                    brdp.getIssueQuantity(),
                    "");
            stockList.add(stocks);
        }

        ViewStockOrderAdapter adapter1 = new ViewStockOrderAdapter(getContext(), R.layout.list_view_stock_invoice, stockList);
        //stock_list = (ListView) findViewById(R.id.stock_list);
        stock_list.setAdapter(adapter1);
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }


}






