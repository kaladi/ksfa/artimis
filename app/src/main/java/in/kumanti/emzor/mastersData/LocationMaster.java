package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class LocationMaster {

    private String id;
    private String location_code;
    private String location_name;
    private String lga_code;
    private String lga_name;
    private String city_code;
    private String city_name;
    private String state_code;
    private String state_name;
    private String country_code;
    private String country_name;

    public LocationMaster(String id, String location_code, String location_name, String lga_code, String lga_name, String city_code, String city_name, String state_code, String state_name,String country_code,String country_name ) {
        this.id = id;
        this.location_code = location_code;
        this.location_name = location_name;
        this.lga_code = lga_code;
        this.lga_name = lga_name;
        this.city_code = city_code;
        this.city_name = city_name;
        this.state_code = state_code;
        this.state_name = state_name;
        this.country_code = country_code;
        this.country_name = country_name;
    }

    public String getId() {
        return id;
    }

    public String getLocation_code() {
        return location_code;
    }

    public String getLocation_name() {
        return location_name;
    }

    public String getLga_code() {
        return lga_code;
    }

    public String getLga_name() {
        return lga_name;
    }

    public String getCity_code() {
        return city_code;
    }

    public String getCity_name() {
        return city_name;
    }

    public String getState_code() {
        return state_code;
    }

    public String getState_name() {
        return state_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getCountry_name() {
        return country_name;
    }

}

