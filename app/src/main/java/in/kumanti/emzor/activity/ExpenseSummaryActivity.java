package in.kumanti.emzor.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.ExpenseAdapter;
import in.kumanti.emzor.eloquent.ExpenseTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.Globals;

public class ExpenseSummaryActivity extends MainActivity implements OnItemSelectedListener {

    ImageView info, back;
    MyDBHandler dbHandler;
    String login_id = "", tripno1;
    ImageView new_customer;
    TextView eventFromDate, eventToDate;
    String fromDateString = "", toDateString = "", selectedType = "All",selectedStatus = "All";
    String trip_num = null;

    Globals globals;
    int numRows1 = 0;
    ArrayList<Expense> expenseArrayList;
    ArrayAdapter<String> expenseTypeSpinnerArrayAdapter;
    ArrayAdapter<String> statusSpinnerArrayAdapter;
    TextView actionbarTitle, marqueeText;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    String user_role;
    CustomSearchableSpinner expenseTypeSpinner,statusSpinner;
    TextView tvAmount, tvApprovedAmount, tvStatus;
    private boolean ascending = true;
    String customerName = "";
    ExpenseTable expenseTable;
    ExpenseAdapter expenseAdapter;
    private int mYear, mMonth, mDay;
    private int selectedStartDay, selectedStartMonth, selectedStartYear, selectedEndDay, selectedEndMonth, selectedEndYear;
    private RecyclerView expense_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_expense_summary);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Expense Booking");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        //fetchExpenseDetails();

        new_customer.setOnClickListener(v -> {
            Intent myintent = new Intent(getApplicationContext(), NewSRExpenseActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("login_id", login_id);
            myintent.putExtras(bundle);
            startActivity(myintent);
        });

     /*   actionbarBackButton.setOnClickListener(v -> {
            Intent myintent = new Intent(getApplicationContext(), StartStopActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("login_id", login_id);
            bundle.putString("trip_num", trip_num);
            bundle.putString("Home", "Home");
            myintent.putExtras(bundle);
            startActivity(myintent);
        });*/


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), DeviceInfoActivity.class)));

        eventFromDate.setOnClickListener(v -> {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(ExpenseSummaryActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> {

                        selectedStartYear = year;
                        selectedStartMonth = monthOfYear;
                        selectedStartDay = dayOfMonth;

                        String PATTERN = "dd MMM YY";
                        SimpleDateFormat dateFormat = new SimpleDateFormat();
                        Date d = null;
                        try {
                            d = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dateFormat.applyPattern(PATTERN);
                        dateFormat.format(d);
                        eventFromDate.setText(dateFormat.format(d));


                        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
                        fromDateString = sdfd.format(d);

                        System.out.println("RRR :: fromDateString = " + fromDateString);
                        if (!TextUtils.isEmpty(fromDateString) && !TextUtils.isEmpty(toDateString))
                            showExpenseList(selectedType, fromDateString, toDateString);
                    }, mYear, mMonth, mDay);

            if (eventToDate.getText().equals("")) datePickerDialog.show();
            else {
                c.set(Calendar.DATE, selectedEndDay);
                c.set(Calendar.MONTH, selectedEndMonth);
                c.set(Calendar.YEAR, selectedEndYear);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        eventToDate.setOnClickListener(v -> {
            final Calendar c1 = Calendar.getInstance();
            mYear = c1.get(Calendar.YEAR);
            mMonth = c1.get(Calendar.MONTH);
            mDay = c1.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(ExpenseSummaryActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> {

                        selectedEndYear = year;
                        selectedEndMonth = monthOfYear;
                        selectedEndDay = dayOfMonth;

                        String PATTERN = "dd MMM YY";
                        SimpleDateFormat dateFormat = new SimpleDateFormat();
                        Date d = null;
                        try {
                            d = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dateFormat.applyPattern(PATTERN);
                        dateFormat.format(d);
                        eventToDate.setText(dateFormat.format(d));

                        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
                        toDateString = sdfd.format(d);

//                        toDateString = eventToDate.getText().toString();
                        System.out.println("RRR :: toDateString = " + toDateString);
                        if (!TextUtils.isEmpty(fromDateString) && !TextUtils.isEmpty(toDateString))
                            showExpenseList(selectedType, fromDateString, toDateString);
                    }, mYear, mMonth, mDay);

            if (eventFromDate.getText().equals("")) datePickerDialog.show();
            else {
                c1.set(Calendar.DATE, selectedStartDay);
                c1.set(Calendar.MONTH, selectedStartMonth);
                c1.set(Calendar.YEAR, selectedStartYear);
                datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        tvAmount.setOnClickListener(view -> {
            sortAmount(ascending);
            ascending = !ascending;
        });

        tvApprovedAmount.setOnClickListener(view -> {
            sortAmount(ascending);
            ascending = !ascending;
        });

        tvStatus.setOnClickListener(view -> {
            sortAmount(ascending);
            ascending = !ascending;
        });

        String[] expenseType = {"All","Boarding","Lodging","Communication","Entertainment","Fuel","Vehicle Repair","Toll","Parking","Miscellaneous"};
        String[] status = {"All","Approved","Pending","Rejected"};

        expenseTypeSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, expenseType);
        expenseTypeSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        expenseTypeSpinner.setAdapter(expenseTypeSpinnerArrayAdapter);

        expenseTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                expenseTypeSpinner.isSpinnerDialogOpen = false;
                selectedType = adapterView.getItemAtPosition(pos).toString();
                showExpenseList(selectedType, fromDateString, toDateString);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                expenseTypeSpinner.isSpinnerDialogOpen = false;
            }
        });

        statusSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, status);
        statusSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        statusSpinner.setAdapter(statusSpinnerArrayAdapter);

        statusSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                statusSpinner.isSpinnerDialogOpen = false;
                selectedStatus = adapterView.getItemAtPosition(pos).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                statusSpinner.isSpinnerDialogOpen = false;
            }
        });
        expense_list.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        expense_list.setLayoutManager(mLayoutManager);

        showExpenseList("All", fromDateString, toDateString);
    }

    private void showExpenseList(String type, String from, String to) {
        System.out.println("RRR :: type = [" + type + "], from = [" + from + "], to = [" + to + "]");
        expenseArrayList = new ArrayList<>();
        expenseArrayList = expenseTable.getExpenseByType(type, from, to);
        expenseAdapter = new ExpenseAdapter(expenseArrayList);
        expense_list.setAdapter(expenseAdapter);
    }

    private void sortAmount(boolean asc) {
        Collections.sort(expenseArrayList, asc ? Expense.expenseAsc : Expense.expenseDes);
        System.out.println("RRR :: expenseAmountArrayList = " + new Gson().toJson(expenseArrayList));
        expenseAdapter.setItems(expenseArrayList);
        expenseAdapter.notifyDataSetChanged();
    }

    private void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        globals = ((Globals) getApplicationContext());
        tripno1 = globals.getTrip_num();

        eventFromDate = findViewById(R.id.eventFromDateButton);
        eventToDate = findViewById(R.id.eventToDateButton);
        tvAmount = findViewById(R.id.tvAmount);
        tvApprovedAmount = findViewById(R.id.tvApprovedAmount);
        tvStatus = findViewById(R.id.tvStatus);
        new_customer = findViewById(R.id.new_customer);
        expense_list = findViewById(R.id.expense_list);
        expenseTypeSpinner = findViewById(R.id.expenseTypeSpinner);
        statusSpinner = findViewById(R.id.statusSpinner);

        expenseTable = new ExpenseTable(getApplicationContext());
    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        login_id = bundle.getString("sales_rep_id");
        user_role = bundle.getString("user_role");
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 3) {
            Bundle bundle = new Bundle();
            bundle.putString("login_id", login_id);
            bundle.putString("trip_num", trip_num);
            bundle.putString("Home", "Home");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
