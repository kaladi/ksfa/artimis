package in.kumanti.emzor.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.DoctorPrescribedProductsTable;
import in.kumanti.emzor.eloquent.DoctorVisitInfoTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.fragment.DoctorVisitCompetitorProductsFragment;
import in.kumanti.emzor.fragment.DoctorVisitOurProductsFragment;
import in.kumanti.emzor.model.DoctorVisitInfo;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class DoctorVisitActivity extends MainActivity {

    String login_id = "";
    ImageView actionbarBackButton, deviceInfo, savePresProdButton, customerImage;
    TextView doctorName, visitDate, checkInTime, accountNo, city, sr_name;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    Chronometer duration;
    EditText avgPatients, avgPrescriptions;
    String checkin_time1 = "";
    SharedPreferenceManager sharedPreferenceManager;
    String checkin_time, visitDateString = null;
    String customer_id = null;
    String customer_type = null;
    Globals globals;
    Button focButton, inHousePharmacyButton, neighPharmacyButton, sponsorshipButton;
    MyDBHandler dbHandler;
    DoctorVisitInfoTable doctorVisitInfoTable;
    DoctorPrescribedProductsTable prescriptionsProductsTable;
    DoctorVisitOurProductsFragment ourProductsFragment;
    DoctorVisitCompetitorProductsFragment competitorProductsFragment;
    private TextView actionbarTitle, marqueeText;
    private TabLayout productTabLayout;
    private ViewPager productPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_doctor_visit);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();
        setDoctorVisitDate();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViewPager(productPager);
        productTabLayout.setupWithViewPager(productPager);

        actionbarTitle.setText(R.string.doctor_visit_actionbar_title);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        //Prefetch doctor visit Info
        DoctorVisitInfo dvi = doctorVisitInfoTable.getDoctorVisitByCode(customer_id);
        if (dvi != null) {
            avgPatients.setText(dvi.getAvgVisitPatients());
            avgPrescriptions.setText(dvi.getAvgPrescriptionsIssued());
        }

        savePresProdButton.setOnClickListener(v -> {
            String visitSequence = sharedPreferences.getString("VisitSequence", "");

            hideSoftKeyboard(getApplicationContext(), v);
            View current = getCurrentFocus();
            current.clearFocus();
            DoctorVisitInfo dvi1 = new DoctorVisitInfo();
            dvi1.setAvgVisitPatients(avgPatients.getText().toString());
            dvi1.setDoctorCode(customer_id);
            dvi1.setAvgPrescriptionsIssued(avgPrescriptions.getText().toString());
            dvi1.setVisitDate(visitDateString);
            dvi1.setVisitSeqNo(visitSequence);
            doctorVisitInfoTable.create(dvi1);

            boolean ourProducts = ourProductsFragment.savePresciptionProducts();
            boolean competitorProducts = competitorProductsFragment.savePresciptionCompetitorProducts();

            if (TextUtils.isEmpty(avgPatients.getText()) && (TextUtils.isEmpty(avgPrescriptions.getText()))){
                Toast.makeText(getApplicationContext(), "Please add Patients or Prescriptions count", Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(getApplicationContext(), "Prescription products saved successfully.", Toast.LENGTH_LONG).show();
        });

        focButton.setOnClickListener(v -> {
            Intent myintent = new Intent(getApplicationContext(), InvoiceActivity.class);
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();
            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);
            bundle.putString("customer_type", "Doctor");

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        });

        inHousePharmacyButton.setOnClickListener(v -> {
            Intent myintent = new Intent(getApplicationContext(), PharmacyActivity.class);
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();
            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time1);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);
            bundle.putString("customer_type", customer_type);
            bundle.putString("pharmacyType", "InHouse");

            Log.d("Test--", "CustomerId--" + checkin_time);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);

        });


        neighPharmacyButton.setOnClickListener(v -> {
            Intent myintent = new Intent(getApplicationContext(), PharmacyActivity.class);
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();
            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time1);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);
            bundle.putString("customer_type", customer_type);
            bundle.putString("pharmacyType", "Neighbourhood");


            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);

        });


        sponsorshipButton.setOnClickListener(v -> {
            Intent myintent = new Intent(getApplicationContext(), SponsorshipActivity.class);
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();
            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time1);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);
            bundle.putString("customer_type", customer_type);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);

        });


        actionbarBackButton.setOnClickListener(v -> {

            Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);

            //Create the bundle
            Bundle bundle = new Bundle();
            //Add your data to bundle
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("sales_rep_id", login_id);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        });


        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });


    }


    @Override
    public void onPause() {
        try {
            super.onPause();
        } catch (Exception e) {

        }
    }

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private void setupViewPager(ViewPager viewPager) {
        // set ViewStockWarehouseFragment Arguments
        ourProductsFragment = new DoctorVisitOurProductsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("login_id", login_id);
        bundle.putString("customer_id", customer_id);
        String visitSequence = sharedPreferences.getString("VisitSequence", "");
        bundle.putString("visit_sequence", visitSequence);
        ourProductsFragment.setArguments(bundle);

        competitorProductsFragment = new DoctorVisitCompetitorProductsFragment();
        competitorProductsFragment.setArguments(bundle);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(ourProductsFragment, "OUR PRODUCTS");
        adapter.addFragment(competitorProductsFragment, "COMPETITOR PRODUCTS");
        viewPager.setAdapter(adapter);
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        doctorName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);


        visitDate = findViewById(R.id.doctorVisitDateTv);
        avgPatients = findViewById(R.id.doctorVisitAvgPatientsCountEt);
        avgPrescriptions = findViewById(R.id.doctorVisitAvgPrescIssuedEt);


        productPager = findViewById(R.id.doctorVisitProductPager);
        productTabLayout = findViewById(R.id.doctorVisitProductTabs);

        focButton = findViewById(R.id.focButton);
        inHousePharmacyButton = findViewById(R.id.inHousePharmacyButton);
        neighPharmacyButton = findViewById(R.id.neighbourhoodPharmacyButton);
        sponsorshipButton = findViewById(R.id.sponsorshipButton);
        savePresProdButton = findViewById(R.id.doctorVisitSaveImageButton);


        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        globals = ((Globals) getApplicationContext());

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        doctorVisitInfoTable = new DoctorVisitInfoTable(getApplicationContext());
        prescriptionsProductsTable = new DoctorPrescribedProductsTable(getApplicationContext());


    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void setDoctorVisitDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        visitDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        visitDateString = timeStampFormat.format(myDate);
    }

    private void populateHeaderDetails() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time1 = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
        }

        checkInTime.setText(checkin_time1);
        customer_type = globals.getCustomer_type();

        //Populate Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                doctorName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                /*File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/mSale/CustomerImages/" + customerDetails.getString(33));

                File imgFile = new  File(String.valueOf(dir));

                if(imgFile.exists()){

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }*/

            }
        }


    }


    @Override
    public void onBackPressed() {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }


}
