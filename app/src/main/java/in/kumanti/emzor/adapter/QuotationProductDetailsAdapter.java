package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.OpportunityProductDetails;

public class QuotationProductDetailsAdapter extends RecyclerView.Adapter<QuotationProductDetailsAdapter.ViewHolder> {

    public ArrayList<OpportunityProductDetails> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;

    public QuotationProductDetailsAdapter(ArrayList<OpportunityProductDetails> myDataset) {
        mDataset = myDataset;
    }

    public void add(OpportunityProductDetails item1, int position) {
        mDataset.add(position, item1);
        Log.d("Recycle", "Data set Add" + position + "--" + mDataset.size());
        notifyItemInserted(position);
    }

    public void remove(OpportunityProductDetails item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public QuotationProductDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_quotation_products_details, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        //To Set the values to the Product Quantity
        if (viewHolder.productQuantity != null && mDataset.get(position).getOpportunity_product_quantity() != null) {
            viewHolder.productQuantity.setText(String.valueOf(mDataset.get(position).getOpportunity_product_quantity()));
        } else {
            viewHolder.productQuantity.setText("0");
        }


        //To Set the values to the Product Price
        if (viewHolder.productPrice != null && mDataset.get(position).getOpportunity_product_quantity() != null) {
            viewHolder.productPrice.setText(String.valueOf(mDataset.get(position).getOpportunity_product_quantity()));
        } else {
            viewHolder.productPrice.setText("0");
        }


        //To Set the values to the Product Value
        if (viewHolder.productValue != null && mDataset.get(position).getOpportunity_product_quantity() != null) {
            viewHolder.productValue.setText(String.valueOf(mDataset.get(position).getOpportunity_product_quantity()));
        } else {
            viewHolder.productValue.setText("0");
        }


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView enquiryNumber, enquiryDate, quotationNumber, quotationDate, productName, productUom, productQuantity, productPrice, productValue, productDesign;

        public ViewHolder(View v) {

            super(v);
            enquiryNumber = v.findViewById(R.id.opportunityEnquiryNumberTextView);
            enquiryDate = v.findViewById(R.id.opportunityEnquiryDateTextView);
            quotationNumber = v.findViewById(R.id.opportunityQuotationNumberTextView);
            quotationDate = v.findViewById(R.id.opportunityQuotationDateTextView);

            productName = v.findViewById(R.id.quotationProductNameTextView);
            productUom = v.findViewById(R.id.quotationProductUomTextView);
            productQuantity = v.findViewById(R.id.quotationProductQuantityEditText);
            productPrice = v.findViewById(R.id.quotationProductPriceEditText);
            productValue = v.findViewById(R.id.quotationProductValueTextView);
            productDesign = v.findViewById(R.id.quotationProductDesignTextView);

        }
    }


}