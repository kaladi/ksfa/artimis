package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import in.kumanti.emzor.model.Notes;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_NOTES;

public class NotesTable extends SQLiteOpenHelper {

    //Columns used for displaying the notes which entered in Customer Landing Page, Competitor Info Details, Opportunity Details
    private static final String COLUMN_NOTES_ID = "id";
    private static final String COLUMN_CUSTOMER_ID = "customer_id";
    //private static final String COLUMN_OPPORTUNITY_ID = "opportunity_id";
    // private static final String COLUMN_COMPETITOR_INFO = "competitor_info_id";
    private static final String COLUMN_SALES_REP_CODE = "sr_code";
    private static final String COLUMN_NOTES_TEXT = "notes_text";
    private static final String COLUMN_NOTES_TYPE = "notes_type";
    private static final String COLUMN_NOTES_REFERENCE_NUMBER = "notes_reference_no";
    private static final String COLUMN_NOTES_CREATED_AT = "created_at";
    private static final String COLUMN_NOTES_UPDATED_AT = "updated_at";
    private final static String COLUMN_NOTES_SEQUENCE_NO = "visitSeqNo";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public NotesTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);

        String competitorInfoDetailTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTES + "(" +
                COLUMN_NOTES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SALES_REP_CODE + " TEXT, " +
                COLUMN_CUSTOMER_ID + " TEXT, " +
                COLUMN_NOTES_TEXT + " TEXT, " +
                COLUMN_NOTES_TYPE + " TEXT, " +
                COLUMN_NOTES_REFERENCE_NUMBER + " BOOLEAN, " +
                COLUMN_NOTES_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_NOTES_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_NOTES_SEQUENCE_NO + " TEXT " +
                ");";

        sqlWrite.execSQL(competitorInfoDetailTable);


    }

    private static String[] merge(String[]... arrays) {
        List<String> list = new ArrayList<>();

        for (String[] array : arrays)
            Collections.addAll(list, array);

        return list.toArray(new String[0]);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(Notes notes) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALES_REP_CODE, notes.getSales_rep_id());
        values.put(COLUMN_CUSTOMER_ID, notes.getCustomer_id());
        values.put(COLUMN_NOTES_TEXT, notes.getNotes_data());
        values.put(COLUMN_NOTES_REFERENCE_NUMBER, notes.getNotes_reference_no());
        values.put(COLUMN_NOTES_TYPE, notes.getNotes_type());
        values.put(COLUMN_NOTES_SEQUENCE_NO, notes.getVisitSeqNumber());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_NOTES, null, values);
        db.close();
        return id;
    }

    public long update(Notes notes) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NOTES_TEXT, notes.getNotes_data());
        values.put(COLUMN_NOTES_UPDATED_AT, String.valueOf(System.currentTimeMillis() / 1000L));
        SQLiteDatabase db = getWritableDatabase();
        long id = db.update(TABLE_NOTES, values, COLUMN_NOTES_ID + " = ?", new String[]{String.valueOf(notes.getId())});
        db.close();
        return id;
    }

    public ArrayList<Notes> getNotesData() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_NOTES;

        ArrayList<Notes> notesArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            Notes notes = new Notes();
            notes.setTabCode(Constants.TAB_CODE);
            notes.setCompany_id(Constants.COMPANY_CODE);
            notes.setSales_rep_id(Constants.SR_CODE);

            int id = cursor.getInt(0);
            notes.setId(id);
            notes.setCustomer_id(cursor.getString(2));
            notes.setNotes_data(cursor.getString(3));
            notes.setNotes_type(cursor.getString(4));
            notes.setNotes_reference_no(cursor.getString(5));
            notes.setVisitSeqNumber(cursor.getString(8));
            notes.setCreated_at(cursor.getString(7));
            notes.setUpdated_at(cursor.getString(6));

            notesArrayList.add(notes);
        }
        cursor.close();
        sqlWrite.close();
        return notesArrayList;
    }

    public ArrayList<Notes> getNotesByCustomer(String customerId, String type) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_CUSTOMER_ID + "=? AND " + COLUMN_NOTES_TYPE + "=? ORDER BY " + COLUMN_NOTES_UPDATED_AT + " desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerId, type});

        Log.d("Data", "getCompetitorDetailsByProductId  ");
        ArrayList<Notes> notesArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            Notes note = new Notes();
            note.setId(cursor.getInt(0));
            note.setSales_rep_id(cursor.getString(1));
            note.setCustomer_id(cursor.getString(2));
            note.setNotes_data(cursor.getString(3));
            note.setNotes_reference_no(cursor.getString(4));

            String dateTime = cursor.getString(6);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd MMM yy hh:mm a");
            Date myDate = new Date(Long.parseLong(dateTime) * 1000L);
            String date = dateFormat.format(myDate);

            note.setTimeStamp(date);
            notesArrayList.add(i, note);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return notesArrayList;
    }

    public ArrayList<Notes> getNotesByCustomer(String customerId, String[] typeList, String[] referenceIdList) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_CUSTOMER_ID + "=? AND " + COLUMN_NOTES_TYPE + " IN (" + makePlaceholders(typeList.length) + ") AND " + COLUMN_NOTES_REFERENCE_NUMBER + " IN (" + makePlaceholders(referenceIdList.length) + ") ORDER BY " + COLUMN_NOTES_UPDATED_AT + " desc";
        String[] queryParams = merge(new String[]{customerId}, typeList, referenceIdList);
        for (String a : queryParams) {
            Log.d("DatasTest", a);
        }
        Cursor cursor = sqlWrite.rawQuery(selectQuery, queryParams);

        Log.d("Data", "getCompetitorDetailsByProductId  ");
        ArrayList<Notes> notesArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            Notes note = new Notes();
            note.setId(cursor.getInt(0));
            note.setSales_rep_id(cursor.getString(1));
            note.setCustomer_id(cursor.getString(2));
            note.setNotes_data(cursor.getString(3));
            note.setNotes_reference_no(cursor.getString(4));

            String dateTime = cursor.getString(6);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd MMM yy hh:mm a");
            Date myDate = new Date(Long.parseLong(dateTime) * 1000L);
            String date = dateFormat.format(myDate);

            note.setTimeStamp(date);
            notesArrayList.add(i, note);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return notesArrayList;
    }

    public ArrayList<Notes> getNotesBySalesRep(String salesRepId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_SALES_REP_CODE + "=? AND " + COLUMN_NOTES_TYPE + "='SALES_REP' ORDER BY " + COLUMN_NOTES_UPDATED_AT + " desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{salesRepId});

        Log.d("Data", "getCompetitorDetailsByProductId  ");
        ArrayList<Notes> notesArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            Notes note = new Notes();
            note.setId(cursor.getInt(0));
            note.setSales_rep_id(cursor.getString(1));
            note.setCustomer_id(cursor.getString(2));
            note.setNotes_data(cursor.getString(3));
            note.setNotes_reference_no(cursor.getString(4));

            String dateTime = cursor.getString(6);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd MMM yy hh:mm a");
            Date myDate = new Date(Long.parseLong(dateTime) * 1000L);
            String date = dateFormat.format(myDate);

            note.setTimeStamp(date);
            notesArrayList.add(i, note);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return notesArrayList;
    }


    private String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

  /*
    public Notes getNotesTextByReferenceId(String bookId, String topicId, String componentId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NOTES + " WHERE "+COLUMN_CUSTOMER_ID+"=?;

        sqlWrite.close();
        return n;
    }*/

   /* public long insertNotes(String customerId, String notesData) {
        sqlWrite = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CUSTOMER_ID, customerId);
        cv.put(COLUMN_NOTES_TEXT, notesData);

        Log.d("Notes","KEY_DATA" + notesData);
        long id = sqlWrite.insert(TABLE_NOTES, null, cv);
        sqlWrite.close();
        return id;
    }*/


   /* public int updateNotes(int id, String customerId, String notesData) {
        sqlWrite = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CUSTOMER_ID, customerId);
        cv.put(COLUMN_NOTES_TEXT, notesData);

        sqlWrite.update(TABLE_NOTES, cv, "id=" + id, null);
        sqlWrite.close();
        return id;
    }*/


     /* public void clearAllNotes(){
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.delete(NOTES_TABLE, KEY_ROW_ID + " >= 0", null);
        sqlWrite.close();
    }*/


}
