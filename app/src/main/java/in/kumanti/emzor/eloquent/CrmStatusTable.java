package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.CrmStatus;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_STATUS;

public class CrmStatusTable extends SQLiteOpenHelper {

    private static final String COLUMN_CRM_ID = "crmStatusId";
    private static final String COLUMN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_CRM_NUMBER = "crmNo";
    private static final String COLUMN_CRM_STATUS_DATE = "statusDate";
    private static final String COLUMN_CRM_STATUS = "status";
    private static final String COLUMN_CRM_IS_SYNC = "isSync";
    private static final String COLUMN_CRM_CREATED_AT = "createdAt";
    private static final String COLUMN_CRM_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmStatusTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_STATUS);
        String crmTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_STATUS + "(" +
                COLUMN_CRM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_CRM_NUMBER + " TEXT, " +
                COLUMN_CRM_STATUS_DATE + " TEXT, " +
                COLUMN_CRM_STATUS + " TEXT, " +
                COLUMN_CRM_IS_SYNC + " TEXT, " +
                COLUMN_CRM_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CRM_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(crmTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmStatus crmStatus) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_CODE, crmStatus.getCustomerCode());
        values.put(COLUMN_CRM_NUMBER, crmStatus.getCrmNumber());
        values.put(COLUMN_CRM_STATUS_DATE, crmStatus.getCrmStatusDate());
        values.put(COLUMN_CRM_STATUS, crmStatus.getCrmStatus());
        values.put(COLUMN_CRM_IS_SYNC, crmStatus.isSync());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_STATUS, null, values);
        db.close();
        return id;
    }


    public ArrayList<CrmStatus> getCRMStatus(String customerCode, String crmNumber) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_STATUS + " WHERE "+ COLUMN_CUSTOMER_CODE + "=? AND "+COLUMN_CRM_NUMBER +"=? GROUP BY STATUS ORDER BY crmStatusId DESC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerCode, crmNumber});

        Log.d("Crm--", "Crm Numbers  ");
        ArrayList<CrmStatus> crmArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmStatus crmStatus = new CrmStatus();
            crmStatus.setCrmStatusId(cursor.getInt(0));
            crmStatus.setCustomerCode(cursor.getString(1));
            crmStatus.setCrmNumber(cursor.getString(2));
            crmStatus.setCrmStatusDate(cursor.getString(3));
            crmStatus.setCrmStatus(cursor.getString(4));
            crmArrayList.add(crmStatus);
        }
        cursor.close();
        sqlWrite.close();
        return crmArrayList;
    }


    public ArrayList<CrmStatus> getCrmStatusForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CRM_STATUS + " WHERE DATE(datetime(createdAt,'unixepoch')) >= date('now','-5 Days')";

        ArrayList<CrmStatus> crmStatusArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            CrmStatus crmStatus = new CrmStatus();
            crmStatus.setTabCode(Constants.TAB_CODE);
            crmStatus.setCompanyCode(Constants.COMPANY_CODE);
            crmStatus.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            crmStatus.setCrmStatusId(id);
            crmStatus.setCustomerCode(cursor.getString(1));
            crmStatus.setCrmNumber(cursor.getString(2));
            crmStatus.setCrmStatusDate(cursor.getString(3));
            crmStatus.setCrmStatus(cursor.getString(4));
            crmStatus.setCreatedAt(cursor.getString(6));
            crmStatus.setUpdatedAt(cursor.getString(7));

            crmStatusArrayList.add(crmStatus);
        }
        cursor.close();
        sqlWrite.close();
        return crmStatusArrayList;
    }


    //select * from xxmsales_crm_status where crmNo = 'H001CRM030320001' and customerCode = 'CU1163' order by createdAt desc limit 1

    public ArrayList<CrmStatus> getCrmCurrentStatus(String customerCode, String crmNumber) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT status FROM " + TABLE_CRM_STATUS + " WHERE "+ COLUMN_CUSTOMER_CODE + "=? AND "+COLUMN_CRM_NUMBER +"=? ORDER BY createdAt DESC LIMIT 1";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerCode, crmNumber});

        Log.d("Crm--", "Crm Numbers  ");
        ArrayList<CrmStatus> crmArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmStatus crmStatus = new CrmStatus();
            crmStatus.setCrmStatusId(cursor.getInt(0));
            crmStatus.setCustomerCode(cursor.getString(1));
            crmStatus.setCrmNumber(cursor.getString(2));
            crmStatus.setCrmStatusDate(cursor.getString(3));
            crmStatus.setCrmStatus(cursor.getString(4));
            crmArrayList.add(crmStatus);
        }
        cursor.close();
        sqlWrite.close();
        return crmArrayList;
    }


}
