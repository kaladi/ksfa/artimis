package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.PrimaryInvoiceProduct;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_ITEM_DETAILS;
import static in.kumanti.emzor.utils.Constants.TABLE_PRIMARY_INVOICE_PRODUCT;

public class PrimaryInvoiceProductTable extends SQLiteOpenHelper {

    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_ID = "id";
    private static final String COLUMN_PRIMARY_INVOICE_NUMBER = "primary_invoice_number";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_CODE = "product_code";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_NAME = "product_name";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_UOM = "product_uom";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_QUANTITY = "product_quantity";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_PRICE = "product_price";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_TOTAL_PRICE = "total_price";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_CREATED_AT = "created_at";
    private static final String COLUMN_PRIMARY_INVOICE_PRODUCT_UPDATED_AT = "updated_at";

    public Context context;

    public PrimaryInvoiceProductTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_PRIMARY_INVOICE_PRODUCT);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_PRIMARY_INVOICE_PRODUCT + "(" +
                COLUMN_PRIMARY_INVOICE_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PRIMARY_INVOICE_NUMBER + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_CODE + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_NAME + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_UOM + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_QUANTITY + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_PRICE + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_TOTAL_PRICE + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_PRIMARY_INVOICE_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(PrimaryInvoiceProduct primaryInvoiceProduct) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_PRIMARY_INVOICE_NUMBER, primaryInvoiceProduct.getPrimaryInvoiceNumber());
        values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_CODE, primaryInvoiceProduct.getPrimaryInvoiceProductCode());
        values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_NAME, primaryInvoiceProduct.getPrimaryInvoiceProductName());
        values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_UOM, primaryInvoiceProduct.getPrimaryInvoiceProductUom());
        values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_QUANTITY, primaryInvoiceProduct.getPrimaryInvoiceProductQuantity());
        values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_PRICE, primaryInvoiceProduct.getPrimaryInvoiceProductPrice());
        values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_TOTAL_PRICE, primaryInvoiceProduct.getPrimaryInvoiceProductTotalPrice());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRIMARY_INVOICE_PRODUCT, null, values);
        db.close();
        return id;
    }

    /*
     * Fetching the Primary Invoice Products List details from the Master via Web Service
     */
    public void insertingPrimaryInvoiceProductsMaster(ArrayList<PrimaryInvoiceProduct> primaryInvoiceProductsList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (primaryInvoiceProductsList != null) {
                for (int i = 0; i < primaryInvoiceProductsList.size(); i++) {
                    values.put(COLUMN_PRIMARY_INVOICE_NUMBER, primaryInvoiceProductsList.get(i).getPrimaryInvoiceNumber());
                    values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_CODE, primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductCode());
                    values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_NAME, primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductName());
                    values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_UOM, primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductUom());
                    values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_QUANTITY, primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductQuantity());
                    values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_PRICE, primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductPrice());
                    values.put(COLUMN_PRIMARY_INVOICE_PRODUCT_TOTAL_PRICE, primaryInvoiceProductsList.get(i).getPrimaryInvoiceProductTotalPrice());
                    db.insert(TABLE_PRIMARY_INVOICE_PRODUCT, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }

    public ArrayList<PrimaryInvoiceProduct> getPrimaryInvoiceByInvoiceNumber(String invoice_number) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT TPIP.*,TID.batch_controlled,TID.conversion FROM " + TABLE_PRIMARY_INVOICE_PRODUCT + " as TPIP LEFT JOIN " + TABLE_ITEM_DETAILS + " as TID ON TPIP.product_code=TID.product_id WHERE " + COLUMN_PRIMARY_INVOICE_NUMBER + "=? ORDER BY TPIP.product_name ASC";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{invoice_number});
        Log.d("Data", "Get Products  ");
        ArrayList<PrimaryInvoiceProduct> primaryInvoiceProducts = new ArrayList<>();

        while (cursor.moveToNext()) {
            PrimaryInvoiceProduct product = new PrimaryInvoiceProduct();
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            product.setPrimaryInvoiceProductId(cursor.getInt(0));
            product.setPrimaryInvoiceNumber(cursor.getString(1));
            product.setPrimaryInvoiceProductCode(cursor.getString(2));
            product.setPrimaryInvoiceProductName(cursor.getString(3));
            product.setPrimaryInvoiceProductUom(cursor.getString(4));
            product.setPrimaryInvoiceProductQuantity(cursor.getString(5));
            product.setPrimaryInvoiceProductPrice(cursor.getString(6));
            product.setPrimaryInvoiceProductTotalPrice(cursor.getString(7));
            product.setBatchControlled(cursor.getString(10));
            product.setConversion(cursor.getString(11));
            primaryInvoiceProducts.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return primaryInvoiceProducts;
    }


 /*   public ArrayList<StockReceipt> getPrimaryInvoiceProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRIMARY_INVOICE_PRODUCT +" WHERE "+COLUMN_PRIMARY_INVOICE_PRODUCT_NAME+"=?" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );
        Log.d("Data","Get Products  " );

        ArrayList<StockReceipt> primaryInvoiceProducts = new ArrayList<StockReceipt>();
        while (cursor.moveToNext()) {
            StockReceipt product = new StockReceipt();
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            product.setPrimaryInvoiceProductId(cursor.getInt(0));
            product.setPrimaryInvoiceNumber(cursor.getString(1));
            product.setPrimaryInvoiceProductCode(cursor.getString(2));
            product.setPrimaryInvoiceProductName(cursor.getString(3));
            product.setPrimaryInvoiceProductUom(cursor.getString(4));
            product.setPrimaryInvoiceProductQuantity(cursor.getString(5));
            product.setPrimaryInvoiceProductPrice(cursor.getString(6));
            product.setPrimaryInvoiceProductTotalPrice(cursor.getString(7));
            primaryInvoiceProducts.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return primaryInvoiceProducts;
    }*/

    public void deletePrimaryInvoiceProduct() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRIMARY_INVOICE_PRODUCT, null, null);
        db.close();
    }


}
