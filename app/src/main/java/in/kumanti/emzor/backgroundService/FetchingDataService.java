package in.kumanti.emzor.backgroundService;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.AgeTable;
import in.kumanti.emzor.eloquent.ArtifactsTable;
import in.kumanti.emzor.eloquent.CompetitorProductTable;
import in.kumanti.emzor.eloquent.CustomerShippingAddressTable;
import in.kumanti.emzor.eloquent.DistributorsTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LedgerTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.PrimaryInvoiceProductTable;
import in.kumanti.emzor.eloquent.PrimaryInvoiceTable;
import in.kumanti.emzor.eloquent.SurveyQuestionOptionTable;
import in.kumanti.emzor.eloquent.SurveyQuestionsTable;
import in.kumanti.emzor.eloquent.SurveyTable;
import in.kumanti.emzor.eloquent.WarehouseStockProductsTable;
import in.kumanti.emzor.eloquent.WarehouseTable;
import in.kumanti.emzor.mastersData.CustomerMaster;
import in.kumanti.emzor.mastersData.FeedbackType;
import in.kumanti.emzor.mastersData.ItemMaster;
import in.kumanti.emzor.mastersData.JourneyPlanMaster;
import in.kumanti.emzor.mastersData.LocationMaster;
import in.kumanti.emzor.mastersData.MarqueeText;
import in.kumanti.emzor.mastersData.PriceListHeader;
import in.kumanti.emzor.mastersData.PriceListLines;
import in.kumanti.emzor.model.Age;
import in.kumanti.emzor.model.Artifacts;
import in.kumanti.emzor.model.CompetitorProduct;
import in.kumanti.emzor.model.CrmColdCall;
import in.kumanti.emzor.model.CustomerShippingAddress;
import in.kumanti.emzor.model.Distributors;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.Ledger;
import in.kumanti.emzor.model.Orders;
import in.kumanti.emzor.model.PrimaryInvoice;
import in.kumanti.emzor.model.PrimaryInvoiceProduct;
import in.kumanti.emzor.model.SRPriceList;
import in.kumanti.emzor.model.SurveyHeader;
import in.kumanti.emzor.model.SurveyLines;
import in.kumanti.emzor.model.ViewStockWarehouseProduct;
import in.kumanti.emzor.utils.Constants;


public class FetchingDataService extends JobIntentService {

    public static final String MY_ACTION = "action.SOME_ACTION";
    private static final int MY_JOB_INTENT_SERVICE_ID = 500;

    CustomerShippingAddressTable customerShippingAddressTable;
    CompetitorProductTable competitorProductTable;
    SurveyTable surveyTable;
    SurveyQuestionsTable surveyQuestionsTable;
    SurveyQuestionOptionTable surveyQuestionOptionTable;
    WarehouseStockProductsTable warehouseStockProductsTable;
    LedgerTable ledgerTable;
    AgeTable ageTable;
    WarehouseTable warehouseTable;
    PrimaryInvoiceTable primaryInvoiceTable;
    PrimaryInvoiceProductTable primaryInvoiceProductTable;
    DistributorsTable distributorsTable;
    GeneralSettingsTable generalSettingsTable;
    ArtifactsTable artifactsTable;
    String login_id;
    MyDBHandler dbHandler;
    Thread thread1, thread2, thread3, thread4, thread5, thread6;
    String mdlSyncDate, mdlSyncTime;
    int mileage1 = 0;
    int sync_data = 0;


    ArrayList<CustomerMaster> customerMasterList;
    ArrayList<ItemMaster> itemMastersList;
    ArrayList<CompetitorProduct> competitor_productList;
    ArrayList<PriceListHeader> price_headerList;
    ArrayList<PriceListLines> price_lineList;
    ArrayList<CustomerShippingAddress> shipping_addressList;
    ArrayList<JourneyPlanMaster> journeyplanList;
    ArrayList<ViewStockWarehouseProduct> warehouseProductsList;
    ArrayList<Ledger> ledgerList;
    ArrayList<Age> ageList;
    ArrayList<FeedbackType> feedbackList;
    ArrayList<MarqueeText> marqueeTextList;
    ArrayList<LocationMaster> locationList;
    ArrayList<PrimaryInvoice> primaryInvoiceList;
    ArrayList<PrimaryInvoiceProduct> primaryInvoiceProductsList;
    ArrayList<CrmColdCall> coldCallArrayList;
    ArrayList<Distributors> distributorsArrayList;
    ArrayList<SRPriceList> srPriceListArrayList;
    ArrayList<GeneralSettings> settingsArrayList;
    ArrayList<Artifacts> artifactsArrayList;
    ArrayList<SurveyHeader> surveyArrayList;
    ArrayList<Orders> ordersArrayList;
    ArrayList<SurveyLines> surveyLinesArrayList;
    ArrayList<SurveyLines> surveyOptionsArrayList;


    public FetchingDataService() {

    }

    // Helper Methods to start this JobIntentService.
    public static void enqueueJobAction(Context context, String action, String login_id) {

        Intent intent = new Intent(context, FetchingDataService.class);
        intent.putExtra("login_id", login_id);

        intent.setAction(MY_ACTION);
        enqueueWork(context, FetchingDataService.class, MY_JOB_INTENT_SERVICE_ID, intent);

        Toast.makeText(context, "DS" + login_id, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
//            String CHANNEL_ID = "my_channel_01";
//            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
//                    "Channel human readable title",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//
//            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
//
//            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
//                    .setContentTitle("NEw")
//                    .setContentText("Background Upload Running").build();
//
//            startForeground(1, notification);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = getString(R.string.app_name);
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription(channelId);
            notificationChannel.setSound(null, null);

            notificationManager.createNotificationChannel(notificationChannel);
            Notification notification = new Notification.Builder(this, channelId)
                    .setContentTitle(getString(R.string.app_name))
//                    .setContentText("Connected through SDL")

                    .setAutoCancel(true)
                    .build();
            startForeground(1, notification);
        }

    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                // fetchingMasterData();
                postingDatabaseFtp();

            }
        }).run();


    }


   /* private void fetchingMasterData() {

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        int line_count = dbHandler.get_count_master_download();

        if (line_count == 0) {

            customerShippingAddressTable = new CustomerShippingAddressTable(getApplicationContext());
            competitorProductTable = new CompetitorProductTable(getApplicationContext());
            surveyTable = new SurveyTable(getApplicationContext());
            surveyQuestionsTable = new SurveyQuestionsTable(getApplicationContext());
            surveyQuestionOptionTable = new SurveyQuestionOptionTable(getApplicationContext());
            warehouseStockProductsTable = new WarehouseStockProductsTable(getApplicationContext());
            ledgerTable = new LedgerTable(getApplicationContext());
            ageTable = new AgeTable(getApplicationContext());
            warehouseTable = new WarehouseTable(getApplicationContext());
            primaryInvoiceTable = new PrimaryInvoiceTable(getApplicationContext());
            primaryInvoiceProductTable = new PrimaryInvoiceProductTable(getApplicationContext());
            distributorsTable = new DistributorsTable(getApplicationContext());
            artifactsTable = new ArtifactsTable(getApplicationContext());

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(500000, TimeUnit.SECONDS).readTimeout(500000, TimeUnit.SECONDS).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);

            Call<MasterResponseJson> customerMaster = api.getMasterData(login_id);


            customerMaster.enqueue(new Callback<MasterResponseJson>() {

                @Override
                public void onResponse(Call<MasterResponseJson> customerMaster, retrofit2.Response<MasterResponseJson> response) {

                    MasterResponseJson resultSet = response.body();

                    if (resultSet.status.equals("true")) {

                        customerMasterList = new Gson().fromJson(resultSet.customer, new TypeToken<ArrayList<CustomerMaster>>() {
                        }.getType());

                        itemMastersList = new Gson().fromJson(resultSet.Product, new TypeToken<ArrayList<ItemMaster>>() {
                        }.getType());

                        competitor_productList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CompetitorProduct>>() {
                        }.getType());

                        price_headerList = new Gson().fromJson(resultSet.price_header, new TypeToken<ArrayList<PriceListHeader>>() {
                        }.getType());

                        price_lineList = new Gson().fromJson(resultSet.price_line, new TypeToken<ArrayList<PriceListLines>>() {
                        }.getType());

                        shipping_addressList = new Gson().fromJson(resultSet.shipping_address, new TypeToken<ArrayList<CustomerShippingAddress>>() {
                        }.getType());

                        journeyplanList = new Gson().fromJson(resultSet.journey_plan, new TypeToken<ArrayList<JourneyPlanMaster>>() {
                        }.getType());

                        warehouseProductsList = new Gson().fromJson(resultSet.warehouse_stock, new TypeToken<ArrayList<ViewStockWarehouseProduct>>() {
                        }.getType());

                        ledgerList = new Gson().fromJson(resultSet.customer_ledgerr, new TypeToken<ArrayList<Ledger>>() {
                        }.getType());

                        ageList = new Gson().fromJson(resultSet.customer_aging, new TypeToken<ArrayList<Age>>() {
                        }.getType());

                        feedbackList = new Gson().fromJson(resultSet.feedback, new TypeToken<ArrayList<FeedbackType>>() {
                        }.getType());

                        marqueeTextList = new Gson().fromJson(resultSet.message, new TypeToken<ArrayList<FeedbackType>>() {
                        }.getType());

                        locationList = new Gson().fromJson(resultSet.location, new TypeToken<ArrayList<LocationMaster>>() {
                        }.getType());

                        primaryInvoiceList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoice>>() {
                        }.getType());

                        primaryInvoiceProductsList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoiceProduct>>() {
                        }.getType());


                        coldCallArrayList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CrmColdCall>>() {
                        }.getType());

                        distributorsArrayList = new Gson().fromJson(resultSet.distributors, new TypeToken<ArrayList<Distributors>>() {
                        }.getType());

                        srPriceListArrayList = new Gson().fromJson(resultSet.sr_pricelist, new TypeToken<ArrayList<SRPriceList>>() {
                        }.getType());

                        settingsArrayList = new Gson().fromJson(resultSet.settings, new TypeToken<ArrayList<GeneralSettings>>() {
                        }.getType());

                        artifactsArrayList = new Gson().fromJson(resultSet.artifact, new TypeToken<ArrayList<Artifacts>>() {
                        }.getType());

                        surveyArrayList = new Gson().fromJson(resultSet.survey_header, new TypeToken<ArrayList<SurveyHeader>>() {
                        }.getType());

                        surveyLinesArrayList = new Gson().fromJson(resultSet.survey_line, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        surveyOptionsArrayList = new Gson().fromJson(resultSet.survey_option, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        deletingMasterData();

                        thread1 = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                Log.d("Multi Thread*****", "Starting T------1.");
                                insertFirstData();
                                insertSecondData();
                                insertThirdData();
                                insertFourthData();
                                insertFifthData();
                                insertSixthData();
                                Log.d("Multi Thread*****", "Finishing T------1.");
                            }
                        });


                        thread1.start();


                        Log.d("Multi Thread*****", "Data Downloaded-----.");

                        generateMDLSyncDateTime();

                        MDLSync mdlSync = new MDLSync();
                        mdlSync.setIsDownloaded("Done");
                        mdlSync.setSyncDate(mdlSyncDate);
                        mdlSync.setSyncTime(mdlSyncTime);

                        dbHandler.createMasterDownload(mdlSync);

                        if (mdlSync.getIsDownloaded().equals("Done")) {
                            Toast.makeText(getApplicationContext(), "MasterData Download has been Completed", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Master Data is not downloaded", Toast.LENGTH_SHORT).show();

                        }

                        sync_data = 0;


                    }
                }

                @Override
                public void onFailure(Call<MasterResponseJson> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    sync_data = 0;

                }
            });

        } else {

        }
    }*/

  /*  private void deletingMasterData() {

       // dbHandler.deleteMasterData();

        //dbHandler.deleteCustomerMaster();
        //dbHandler.deleteItemMaster();
        competitorProductTable.deletecompetitorproducts();
        //dbHandler.deletePriceList();
        //dbHandler.deleteJourneyplan();
        customerShippingAddressTable.deleteShippingAddress();
        warehouseStockProductsTable.deleteWarehouseStockProduct();
        warehouseTable.deleteWarehouse();
        ledgerTable.deleteLedger();
        ageTable.deleteAge();
        primaryInvoiceProductTable.deletePrimaryInvoiceProduct();
        primaryInvoiceTable.deletePrimaryInvoice();
        distributorsTable.deleteDistributors();
        generalSettingsTable.deleteGeneralSettings();
        artifactsTable.deleteArtifacts();
        surveyTable.deleteSurveyHeader();
        surveyQuestionsTable.deleteSurveyLines();
        surveyQuestionOptionTable.deleteSurveyOptions();

    }

    public void insertFirstData() {

        generalSettingsTable.insertingSettingsMaster(settingsArrayList);

        dbHandler.insertingJourneyPlanMaster(journeyplanList);

        dbHandler.insertingCustomerMaster(customerMasterList);


        if(customerMasterList != null) {
            for (int i = 0; i < customerMasterList.size(); i++) {

                new MainActivity.AsyncTaskLoadImage().execute(customerMasterList.get(i).getImage_url(), customerMasterList.get(i).getCustomer_image());
            }
        }


    }

    public void insertSecondData() {

        dbHandler.insertingProductsMaster(itemMastersList);

        dbHandler.insertingPriceListsHeaderMaster(price_headerList);

        dbHandler.insertingPriceListLinesMaster(price_lineList);

        dbHandler.insertingBdePriceListsMaster(srPriceListArrayList);


    }


    public void insertThirdData() {

        warehouseTable.insertingWarehousesMaster(warehouseProductsList);

        primaryInvoiceTable.insertingPrimaryInvoicesMaster(primaryInvoiceList);

        primaryInvoiceProductTable.insertingPrimaryInvoiceProductsMaster(primaryInvoiceProductsList);

        distributorsTable.insertingDistributorsMaster(distributorsArrayList);

    }

    public void insertFourthData() {


        customerShippingAddressTable.insertingCusShippingAddressMaster(shipping_addressList);

        competitorProductTable.insertingCompetitorProductsMaster(competitor_productList);

        dbHandler.insertingLocationsMaster(locationList);


    }

    public void insertFifthData() {

        ledgerTable.insertingLedgersMaster(ledgerList);

        ageTable.insertingAgeDetailsMaster(ageList);

        // dbHandler.insertingFooterMarqueeMsgText(marqueeTextList);


    }

    public void insertSixthData() {


        artifactsTable.insertingArtifactsMaster(artifactsArrayList);

        if(artifactsArrayList != null) {
            for (int i = 0; i < artifactsArrayList.size(); i++) {

                new MainActivity.AsyncTaskLoadPdf().execute(artifactsArrayList.get(i).getFileURL(), artifactsArrayList.get(i).getFileName());

            }
        }


        surveyTable.insertingSurveysMaster(surveyArrayList);

        surveyQuestionsTable.insertingSurveyQuestionsMaster(surveyLinesArrayList);

        if(surveyOptionsArrayList != null) {
            surveyQuestionOptionTable.insertingSurveyOptionsMaster(surveyOptionsArrayList);
        }

        dbHandler.insertingFeedbackTypeMaster(feedbackList);


    }

    private void generateMDLSyncDateTime() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        mdlSyncDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        mdlSyncTime = sdf.format(outDate);
    }*/

    private void postingDatabaseFtp() {
        FTPClient con = null;
        con = new FTPClient();


        try {
            con.connect(Constants.FTP_HOST_NAME);
            if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {
                Log.d("FileUpload", "FilePath Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d("FileUpload", "FilePath Connected");

        String file_name = "ksfaDB.DB";
        try {
            String data = Environment.getExternalStorageDirectory() + File.separator
                    + "KSFA" + File.separator + file_name;
            Log.d("FileUpload", "FilePath " + data);

            FileInputStream in = new FileInputStream(new File(data));

            boolean result = con.storeFile("/home/vistagftp/vistagftp/Db/ksfaDB.DB", in);
            Log.d("FileUpload", "File Result--" + result);

            in.close();

            if (result) {
                Log.d("FileUpload", "Success");

            }


            con.logout();
            con.disconnect();

            Log.d("FileUpload", "File Connection Closed--");


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Database Upload Error" + e.getMessage());
        }
    }


}
