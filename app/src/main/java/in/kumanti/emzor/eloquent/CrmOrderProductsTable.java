package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CrmOrderProducts;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_ORDER_PRODUCTS;

public class CrmOrderProductsTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_ORDER_PRODUCT_ID = "orderProductId";
    private static final String COLUMN_ORDER_ID = "tabOrderId";
    private static final String COLUMN_ORDER_PRODUCT_CODE = "productCode";
    private static final String COLUMN_ORDER_PRODUCT_NAME = "productName";
    private static final String COLUMN_ORDER_PRODUCT_UOM = "productUom";
    private static final String COLUMN_ORDER_PRODUCT_QUANTITY = "productQuantity";
    private static final String COLUMN_ORDER_PRODUCT_UNIT_PRICE = "productUnitPrice";
    private static final String COLUMN_ORDER_PRODUCT_VALUE = "productValue";
    private static final String COLUMN_ORDER_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_ORDER_PRODUCT_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmOrderProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_ORDER_PRODUCTS);
        String opportunityTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_ORDER_PRODUCTS + "(" +
                COLUMN_ORDER_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ORDER_ID + " INTEGER, " +
                COLUMN_ORDER_PRODUCT_CODE + " TEXT, " +
                COLUMN_ORDER_PRODUCT_NAME + " REAL, " +
                COLUMN_ORDER_PRODUCT_UOM + " TEXT, " +
                COLUMN_ORDER_PRODUCT_QUANTITY + " TEXT, " +
                COLUMN_ORDER_PRODUCT_UNIT_PRICE + " TEXT, " +
                COLUMN_ORDER_PRODUCT_VALUE + " TEXT, " +
                COLUMN_ORDER_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_ORDER_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(opportunityTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(CrmOrderProducts orderProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ORDER_ID, orderProducts.getTabOrderId());
        values.put(COLUMN_ORDER_PRODUCT_CODE, orderProducts.getProductCode());
        values.put(COLUMN_ORDER_PRODUCT_NAME, orderProducts.getProductName());
        values.put(COLUMN_ORDER_PRODUCT_UOM, orderProducts.getProductUom());
        values.put(COLUMN_ORDER_PRODUCT_QUANTITY, orderProducts.getProductQuantity());
        values.put(COLUMN_ORDER_PRODUCT_UNIT_PRICE, orderProducts.getProductUnitPrice());
        values.put(COLUMN_ORDER_PRODUCT_VALUE, orderProducts.getProductValue());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_ORDER_PRODUCTS, null, values);
        db.close();
        return id;
    }

   /* public ArrayList<CrmOrderProducts> getCrmOrderProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT productCode FROM " + TABLE_CRM_ORDER_PRODUCTS ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<CrmOrderProducts> crmOrderProductsArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            CrmOrderProducts op = new CrmOrderProducts();
            op.setOrderProductId(cursor.getInt(0));
            op.setTabOrderId(cursor.getInt(1));
            op.setProductCode(cursor.getString(2));
            op.setProductName(cursor.getString(3));
            op.setProductUom(cursor.getString(4));
            op.setProductQuantity(cursor.getString(5));
            op.setProductUnitPrice(cursor.getString(6));
            op.setProductValue(cursor.getString(7));

            crmOrderProductsArrayList.add(i,op);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return crmOrderProductsArrayList;
    }*/


}
