package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CrmKeyPersons;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_KEY_PERSONS;

public class CrmKeyPersonsTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_KEY_PERSON_ID = "keyPersonId";
    private static final String COLUMN_KEY_PERSON_TYPE = "keyPersonType";
    private static final String COLUMN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_OPPORTUNITY_ID = "opportunityId";
    private static final String COLUMN_KEY_PERSON_SERIAL_NO = "serialNo";
    private static final String COLUMN_KEY_PERSON_NAME = "personName";
    private static final String COLUMN_KEY_PERSON_TITLE = "title";
    private static final String COLUMN_KEY_PERSON_EMAIL = "email";
    private static final String COLUMN_KEY_PERSON_MOBILE = "mobile";
    private static final String COLUMN_KEY_PERSON_IS_SYNC = "isSync";
    private static final String COLUMN_KEY_PERSON_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_KEY_PERSON_CREATED_AT = "createdAt";
    private static final String COLUMN_KEY_PERSON_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmKeyPersonsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_KEY_PERSONS);
        String keyPersonsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_KEY_PERSONS + "(" +
                COLUMN_KEY_PERSON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_OPPORTUNITY_ID + " INTEGER, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_KEY_PERSON_TYPE + " TEXT, " +
                COLUMN_KEY_PERSON_SERIAL_NO + " TEXT, " +
                COLUMN_KEY_PERSON_NAME + " REAL, " +
                COLUMN_KEY_PERSON_TITLE + " TEXT, " +
                COLUMN_KEY_PERSON_EMAIL + " TEXT, " +
                COLUMN_KEY_PERSON_MOBILE + " TEXT, " +
                COLUMN_KEY_PERSON_IS_SYNC + " TEXT, " +
                COLUMN_KEY_PERSON_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_KEY_PERSON_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_KEY_PERSON_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(keyPersonsTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmKeyPersons keyPersons) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_OPPORTUNITY_ID, keyPersons.getOpportunityId());
        values.put(COLUMN_CUSTOMER_CODE, keyPersons.getCustomerCode());
        values.put(COLUMN_KEY_PERSON_TYPE, keyPersons.getKeyPersonType());
        values.put(COLUMN_KEY_PERSON_SERIAL_NO, keyPersons.getSerialNo());
        values.put(COLUMN_KEY_PERSON_NAME, keyPersons.getPersonName());
        values.put(COLUMN_KEY_PERSON_TITLE, keyPersons.getTitle());
        values.put(COLUMN_KEY_PERSON_EMAIL, keyPersons.getEmail());
        values.put(COLUMN_KEY_PERSON_MOBILE, keyPersons.getMobile());
        values.put(COLUMN_KEY_PERSON_IS_SYNC, keyPersons.isSync());
        values.put(COLUMN_KEY_PERSON_LATEST_SYNC_DATE, keyPersons.getLatestSyncDate());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_KEY_PERSONS, null, values);
        db.close();
        return id;
    }


}
