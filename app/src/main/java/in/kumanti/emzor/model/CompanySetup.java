package in.kumanti.emzor.model;


public class CompanySetup {

    public int id;
    public String company_name;
    public String short_name;
    public String contact_person;
    public String email_id;
    public String mobile_number;
    public String support_number1;
    public String support_number2;
    public String support_email;
    public byte[] company_logo;
    public String address1;
    public String address2;
    public String address3;


    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getSupport_number1() {
        return support_number1;
    }

    public void setSupport_number1(String support_number1) {
        this.support_number1 = support_number1;
    }

    public String getSupport_number2() {
        return support_number2;
    }

    public void setSupport_number2(String support_number2) {
        this.support_number2 = support_number2;
    }

    public String getSupport_email() {
        return support_email;
    }

    public void setSupport_email(String support_email) {
        this.support_email = support_email;
    }

    public byte[] getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(byte[] company_logo) {
        this.company_logo = company_logo;
    }

    @Override
    public String toString() {
        return this.company_name;
    }
}
