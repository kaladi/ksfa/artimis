package in.kumanti.emzor.model;

public class StockIssue {

    public String visitSeqNumber;
    public String isExported;
    public boolean isSync;
    private String tabCode;
    private String companyCode;
    private String srCode;
    private int stockIssueId;
    private String stockIssueNumber;
    private String stockIssueDate;
    private String bdeCode;
    private String issueProductsCount;
    private String signeeName;
    private String bdeSignature;
    private String emailSent;
    private String smsSent;
    private String printDone;
    private int randomNum;
    private String createdAt;
    private String updatedAt;


    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public int getStockIssueId() {
        return stockIssueId;
    }

    public void setStockIssueId(int stockIssueId) {
        this.stockIssueId = stockIssueId;
    }

    public String getStockIssueNumber() {
        return stockIssueNumber;
    }

    public void setStockIssueNumber(String stockIssueNumber) {
        this.stockIssueNumber = stockIssueNumber;
    }

    public String getStockIssueDate() {
        return stockIssueDate;
    }

    public void setStockIssueDate(String stockIssueDate) {
        this.stockIssueDate = stockIssueDate;
    }

    public String getBdeCode() {
        return bdeCode;
    }

    public void setBdeCode(String bdeCode) {
        this.bdeCode = bdeCode;
    }

    public String getIssueProductsCount() {
        return issueProductsCount;
    }

    public void setIssueProductsCount(String issueProductsCount) {
        this.issueProductsCount = issueProductsCount;
    }

    public String getSigneeName() {
        return signeeName;
    }

    public void setSigneeName(String signeeName) {
        this.signeeName = signeeName;
    }

    public String getBdeSignature() {
        return bdeSignature;
    }

    public void setBdeSignature(String bdeSignature) {
        this.bdeSignature = bdeSignature;
    }

    public String getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(String emailSent) {
        this.emailSent = emailSent;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    public String getPrintDone() {
        return printDone;
    }

    public void setPrintDone(String printDone) {
        this.printDone = printDone;
    }

    public String getVisitSeqNumber() {
        return visitSeqNumber;
    }

    public void setVisitSeqNumber(String visitSeqNumber) {
        this.visitSeqNumber = visitSeqNumber;
    }

    public String getIsExported() {
        return isExported;
    }

    public void setIsExported(String isExported) {
        this.isExported = isExported;
    }

    public int getRandomNum() {
        return randomNum;
    }

    public void setRandomNum(int randomNum) {
        this.randomNum = randomNum;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
