package in.kumanti.emzor.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.reports.CustomerReportsActivity;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.mastersData.LocationHeader;
import in.kumanti.emzor.model.CityHeader;
import in.kumanti.emzor.model.CountryHeader;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.LgaHeader;
import in.kumanti.emzor.model.StateHeader;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class NewCustomerActivity extends MainActivity {

    private static final int SELECT_PHOTO1 = 1;
    private static final int CAPTURE_PHOTO1 = 2;
    private static final int PERMISSION_REQUEST_CODE = 200;
    String login_id = "",nav_type;
    ImageView actionbarBackButton, deviceInfo, customerSaveButton;
    ImageView captureCustomerImage, showCustomerImage;
    String customer_type = "Lead";
//    RadioButton primary, secondary, doctor,prospect;
    RadioButton  secondary, doctor,prospect;
    LinearLayout input_layout_credit_limit,input_layout_credit_days;
    RadioGroup radioGroup;
    String checkin_time1 = "";
    String customer_id = "";
    TextView actionbarTitle, customerCode, checkInTime, location_label;
    //    EditText city, state, lga,country;
    Chronometer chronometer;
    MyDBHandler myDBHandler;
    EditText customerName, customerAddressLine1, customerAddressLine2, customerAddressLine3, customerPhoneNumber, customerEmailId,customerCreditLimit,customerCreditDays;
    SearchableSpinner customerLocationSpinner,newCustomerCitySpinner ,distributorSpinner, newCustomerStateSpinner, newCustomerCountrySpinner,newCustomerLGASpinner;
    SharedPreferenceManager sharedPreferenceManager;

    List<String> locationArrayList;
    ArrayAdapter<String> locationSpinnerArrayAdapter;

    ArrayAdapter<String> lgaSpinnerArrayAdapter;
    List<String> lgaArrayList;

    ArrayAdapter<String> citySpinnerArrayAdapter;
    List<String> cityArrayList;

    ArrayAdapter<String> stateSpinnerArrayAdapter;
    List<String> stateArrayList;

    ArrayAdapter<String> countrySpinnerArrayAdapter;
    List<String> countryArrayList;

    List<String> distributorArrayList;
    ArrayAdapter<String> distributorSpinnerArrayAdapter;

    Button updateCustomerButton;

    byte[] customer_image;
    Bitmap customer_image_bitmap;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String trip_num = null,company_code1;
    Globals globals;
    int random_num, order_id = 0;
    Constants constants;
    String company_code, tab_code, sr_code, tab_prefix,locationString ="",cityString="",stateString="",lgaString="",countryString="",distributorString="",distributorCodeString="";
    Bitmap selectedImage;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_new_customer);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        initializeViews();
        populateHeaderDetails();
        getSetupDetails();

        if ("Reports".equals(nav_type)) {
            prospect.setVisibility(View.VISIBLE);
            secondary.setVisibility(View.VISIBLE);
//            primary.setVisibility(View.VISIBLE);
//            doctor.setVisibility(View.VISIBLE);
            if (company_code1.equals("ALS001")){
                doctor.setVisibility(View.VISIBLE);
            }
            else {
                doctor.setVisibility(View.GONE);
            }
            updateCustomerButton.setVisibility(View.VISIBLE);
            customerSaveButton.setVisibility(View.GONE);
//            primary.setEnabled(false);
            secondary.setEnabled(false);
            doctor.setEnabled(false);
            prospect.setEnabled(false);
            if(customer_type.equals("Secondary")){
                input_layout_credit_limit.setVisibility(View.VISIBLE);
                input_layout_credit_days.setVisibility(View.VISIBLE);

            }
        }
//
//        if(Constants.COMPANY_CODE.equals("DemoCRM") ) {
//            prospect.setVisibility(View.VISIBLE);
//            secondary.setVisibility(View.VISIBLE);
//
//            primary.setVisibility(View.GONE);
//            doctor.setVisibility(View.GONE);
//
//            customer_type = "Prospect";
//        } else {
        if (!"Reports".equals(nav_type)) {
            GeneralSettings gs = generalSettingsTable.getSettingByKey("enable_crm");
            if (gs != null && gs.getValue().equals("Yes")) {
                prospect.setVisibility(View.VISIBLE);
                secondary.setVisibility(View.VISIBLE);
//                primary.setVisibility(View.VISIBLE);
//                doctor.setVisibility(View.VISIBLE);
                if (company_code1.equals("ALS001")){
                    doctor.setVisibility(View.VISIBLE);
                }
                else {
                    doctor.setVisibility(View.GONE);
                }
                customer_type = "Prospect";
            } else {
//                primary.setVisibility(View.VISIBLE);
                secondary.setVisibility(View.VISIBLE);
//                doctor.setVisibility(View.VISIBLE);
                if (company_code1.equals("ALS001")){
                    doctor.setVisibility(View.VISIBLE);
                }
                else {
                    doctor.setVisibility(View.GONE);
                }
                prospect.setVisibility(View.GONE);

                customer_type = "Lead";
            }
        }

//        }

        setFooterDateTime();
        trip_num = globals.getTrip_num();
        //Toast.makeText(NewCustomerActivity.this, trip_num, Toast.LENGTH_SHORT).show();

        actionbarTitle.setText("New Customer");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        company_code = Constants.COMPANY_CODE;
        tab_code = Constants.TAB_CODE;
        sr_code = Constants.SR_CODE;
        tab_prefix = Constants.TAB_PREFIX;

        if (!"Reports".equals(nav_type)) {
            random_num = myDBHandler.get_customer_id();
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
            Date myDate = new Date();
            String customer_seq_date = timeStampFormat.format(myDate);
            random_num = random_num + 1;

            if (random_num < 10) {
                customerCode.setText(tab_prefix + "CU" + tab_code + customer_seq_date + "00" + random_num);
            } else if (random_num > 10 & random_num < 99) {
                customerCode.setText(tab_prefix + "CU" + tab_code + customer_seq_date + "0" + random_num);
            } else {
                customerCode.setText(tab_prefix + "CU" + tab_code + customer_seq_date + random_num);
            }
        }

//        locationArrayList = myDBHandler.getLocationName();
//        locationArrayList.add(0, "Select");
//        locationSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, locationArrayList);
//        locationSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
//        customerLocationSpinner.setAdapter(locationSpinnerArrayAdapter);
//        customerLocationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int pos, long id) {
//
//                locationString =  adapterView.getItemAtPosition(pos).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//
//            }
//        });

//        cityArrayList = myDBHandler.getCityName();
//        cityArrayList.add(0, "Select");
//        citySpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, cityArrayList);
//        citySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
//        newCustomerCitySpinner.setAdapter(citySpinnerArrayAdapter);
//        newCustomerCitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int pos, long id) {
//                cityString = adapterView.getItemAtPosition(pos).toString();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//
//            }
//        });

//        stateArrayList = myDBHandler.getStateName();
//        stateArrayList.add(0, "Select");
//        stateSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, stateArrayList);
//        stateSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
//        newCustomerStateSpinner.setAdapter(stateSpinnerArrayAdapter);
//        newCustomerStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int pos, long id) {
//                stateString = adapterView.getItemAtPosition(pos).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//
//            }
//        });
//
//        countryArrayList = myDBHandler.getCountryName();
//        countryArrayList.add(0, "Select");
//        countrySpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, countryArrayList);
//        countrySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
//        newCustomerCountrySpinner.setAdapter(countrySpinnerArrayAdapter);
//        newCustomerCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int pos, long id) {
//                CountryHeader cs3 = (CountryHeader) adapterView.getItemAtPosition(pos);
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//
//            }
//        });
//
//        lgaArrayList = myDBHandler.getLgaName();
//        lgaArrayList.add(0, "Select");
//        lgaSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, lgaArrayList);
//        lgaSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
//        newCustomerLGASpinner.setAdapter(lgaSpinnerArrayAdapter);
//        newCustomerLGASpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view,
//                                       int pos, long id) {
//                LgaHeader cs5 = (LgaHeader) adapterView.getItemAtPosition(pos);
//                lgaString = cs5.getLga();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapter) {
//
//            }
//        });

//        primary.setOnCheckedChangeListener(new Radio_check());
        secondary.setOnCheckedChangeListener(new Radio_check());
        doctor.setOnCheckedChangeListener(new Radio_check());
        prospect.setOnCheckedChangeListener(new Radio_check());


        captureCustomerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent1, CAPTURE_PHOTO1);
            }
        });

        showCustomerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedImage != null) {
                    ImageView capturedImage, closeButton;

                    LayoutInflater inflater = getLayoutInflater();

                    View alertLayout = inflater.inflate(R.layout.image_preview, null);
                    capturedImage = alertLayout.findViewById(R.id.capturedImage);
                    closeButton = alertLayout.findViewById(R.id.closeImageView);

                    capturedImage.setImageBitmap(selectedImage);
                    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    alert.setCancelable(true);

                    android.app.AlertDialog dialog = alert.create();
                    dialog.show();

                }

            }
        });

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        customerSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean validForm = true;

                if (customerLocationSpinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), "Please choose a Location", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (newCustomerCitySpinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), "Please choose a City", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(customerName.getText().toString())) {
                    validForm = false;
                    customerName.setError("Please enter value");
                } else
                    customerName.setError(null);
                if (TextUtils.isEmpty(customerAddressLine1.getText().toString())) {
                    validForm = false;
                    customerAddressLine1.setError("Please enter value");
                } else
                    customerAddressLine1.setError(null);

//                LocationHeader ls = (LocationHeader) customerLocationSpinner.getSelectedItem();
//                if (ls.getLocation_id().equals("-1")) {
//                    validForm = false;
//                    location_label.setError("Please select value");
//                } else {
//                    location_label.setError(null);
//                }
                if (TextUtils.isEmpty(customerPhoneNumber.getText().toString())) {
                    validForm = false;
                    customerPhoneNumber.setError("Please enter value");
                } else
                    customerPhoneNumber.setError(null);

                /*if(TextUtils.isEmpty(city.getText().toString())){
                    validForm = false;
                    city.setError("Please enter value");
                }
                else
                    city.setError(null);
                if(TextUtils.isEmpty(lga.getText().toString())){
                    validForm = false;
                    lga.setError("Please enter value");
                }
                else
                    lga.setError(null);
                if(TextUtils.isEmpty(state.getText().toString())){
                    validForm = false;
                    state.setError("Please enter value");
                }
                else
                    state.setError(null);
                if(TextUtils.isEmpty(customerEmailId.getText().toString())){
                    validForm = false;
                    customerEmailId.setError("Please enter value");
                }
                else
                    customerEmailId.setError(null);*/
                if ("".equals(customerEmailId.getText().toString())) {

                } else {
                    if (!customerEmailId.getText().toString().matches(emailPattern)) {
                        validForm = false;
                        customerEmailId.setError("Invalid Email Address");
                    } else
                        customerEmailId.setError(null);

                }

                //If not Valid form return empty
                if (!validForm)
                    return;

                SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date myDate = new Date();
                String creation_date = timeStampFormat.format(myDate);
                String creditlimit = "",creditDays="";
                if(customer_type.equals("Secondary"))
              {  creditlimit = customerCreditLimit.getText().toString();
                creditDays = customerCreditDays.getText().toString();}
                if (!"Reports".equals(nav_type)) {
//                    System.out.println("TTTT::nav_type = " + nav_type);
                    long id2 = myDBHandler.create_customer_master
                            (
                                    customerCode.getText().toString(),
                                    customerName.getText().toString(),
                                    customerAddressLine1.getText().toString(),
                                    customerAddressLine2.getText().toString(),
                                    customerAddressLine3.getText().toString(),
                                    cityString,
                                    stateString,
                                    customerPhoneNumber.getText().toString(),
                                    creditlimit,
                                    creditDays,
                                    customer_type,
                                    null,
                                    null,
                                    "Tab",
                                    customerEmailId.getText().toString(),
                                    locationString,
                                    lgaString,
                                    countryString,
                                    creation_date,
                                    String.valueOf(random_num),
                                    "",
                                    "",
                                    "",
                                    "",
                                    distributorString,
                                    distributorCodeString
                            );

                    if (customer_image_bitmap != null) {
                        String fileName = "cus_" + customerCode.getText().toString() + ".png";
                        fileUtils.storeImage(customer_image_bitmap, fileName, customerImageFolderPath);
                        String customer_image_path = customerImageFolderPath + File.separator + fileName;
                        dbHandler.updatecustomerimage(String.valueOf(id2), customer_image_path, fileName);
                    }

                    Intent myintent = new Intent(getApplicationContext(), UnplannedActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("sales_rep_id", login_id);
                    bundle.putString("trip_num", trip_num);
                    myintent.putExtras(bundle);
                    startActivity(myintent);
                }

            }
        });

        updateCustomerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean validForm = true;


                if (customerLocationSpinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), "Please choose a Location", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (newCustomerCitySpinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(getApplicationContext(), "Please choose a City", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(customerName.getText().toString())) {
                    validForm = false;
                    customerName.setError("Please enter value");
                } else
                    customerName.setError(null);
                if (TextUtils.isEmpty(customerAddressLine1.getText().toString())) {
                    validForm = false;
                    customerAddressLine1.setError("Please enter value");
                } else
                    customerAddressLine1.setError(null);


                if (TextUtils.isEmpty(customerPhoneNumber.getText().toString())) {
                    validForm = false;
                    customerPhoneNumber.setError("Please enter value");
                } else
                    customerPhoneNumber.setError(null);

                /*if(TextUtils.isEmpty(city.getText().toString())){
                    validForm = false;
                    city.setError("Please enter value");
                }
                else
                    city.setError(null);
                if(TextUtils.isEmpty(lga.getText().toString())){
                    validForm = false;
                    lga.setError("Please enter value");
                }
                else
                    lga.setError(null);
                if(TextUtils.isEmpty(state.getText().toString())){
                    validForm = false;
                    state.setError("Please enter value");
                }
                else
                    state.setError(null);
                if(TextUtils.isEmpty(customerEmailId.getText().toString())){
                    validForm = false;
                    customerEmailId.setError("Please enter value");
                }
                else
                    customerEmailId.setError(null);*/
                if ("".equals(customerEmailId.getText().toString())) {

                } else {
                    if (!customerEmailId.getText().toString().matches(emailPattern)) {
                        validForm = false;
                        customerEmailId.setError("Invalid Email Address");
                    } else
                        customerEmailId.setError(null);

                }

                //If not Valid form return empty
                if (!validForm)
                    return;

                SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date myDate = new Date();
                String creation_date = timeStampFormat.format(myDate);

                String creditlimit = "",creditDays="";
                if(customer_type.equals("Secondary"))
                {  creditlimit = customerCreditLimit.getText().toString();
                    creditDays = customerCreditDays.getText().toString();}
                if ("Reports".equals(nav_type)) {

                    String id3 = dbHandler.getCustomerInfo_id(customer_id);
//                    System.out.println("TTTT::id3 = " + id3);
                    myDBHandler.update_customer_master
                            (
                                    customerCode.getText().toString(),
                                    customerName.getText().toString(),
                                    customerAddressLine1.getText().toString(),
                                    customerAddressLine2.getText().toString(),
                                    customerAddressLine3.getText().toString(),
                                    cityString,
                                    stateString,
                                    customerPhoneNumber.getText().toString(),
                                    creditlimit,
                                    creditDays,
                                    customer_type,
                                    null,
                                    null,
                                    "Tab",
                                    customerEmailId.getText().toString(),
                                    locationString,
                                    lgaString,
                                    countryString,
                                    creation_date,
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    distributorString,
                                    distributorCodeString
                            );

                    if (customer_image_bitmap != null) {
                        String fileName = "cus_" + customerCode.getText().toString() + ".png";
                        fileUtils.storeImage(customer_image_bitmap, fileName, customerImageFolderPath);
                        String customer_image_path = customerImageFolderPath + File.separator + fileName;
                        dbHandler.updatecustomerimage(id3, customer_image_path, fileName);
                    }


                }
                customerEmailId.setEnabled(false);
                customerPhoneNumber.setEnabled(false);
                customerName.setEnabled(false);
                customerCreditDays.setEnabled(false);
                customerCreditLimit.setEnabled(false);
                customerAddressLine1.setEnabled(false);
                customerAddressLine2.setEnabled(false);
                customerAddressLine3.setEnabled(false);
                customerLocationSpinner.setEnabled(false);
                newCustomerCountrySpinner.setEnabled(false);
                newCustomerCitySpinner.setEnabled(false);
                newCustomerStateSpinner.setEnabled(false);
                newCustomerLGASpinner.setEnabled(false);
                updateCustomerButton.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
            }
        });


    }

    void getSetupDetails(){
        Cursor setupDetails = dbHandler.getSetupDetails();
        int setupDetailsCount = setupDetails.getCount();
        if (setupDetailsCount == 0) {

        } else {
            int i = 0;
            while (setupDetails.moveToNext()) {

                company_code1 = Constants.COMPANY_CODE = setupDetails.getString(2);
                Constants.COMPANY_NAME = setupDetails.getString(6);
            }
        }

    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerCode = findViewById(R.id.customerCodeTv);
        customerName = findViewById(R.id.customerNameEt);
        customerAddressLine1 = findViewById(R.id.customerAddressLine1Et);
        customerAddressLine2 = findViewById(R.id.customerAddressLine2Et);
        customerAddressLine3 = findViewById(R.id.customerAddressLine3Et);
        customerLocationSpinner = findViewById(R.id.customerLocationSpinner);
        newCustomerCitySpinner = findViewById(R.id.newCustomerCitySpinner);
        distributorSpinner = findViewById(R.id.distributorSpinner);
        newCustomerStateSpinner = findViewById(R.id.newCustomerStateSpinner);
        updateCustomerButton = findViewById(R.id.updateCustomerButton);
        newCustomerCountrySpinner = findViewById(R.id.newCustomerCountrySpinner);
        newCustomerLGASpinner = findViewById(R.id.newCustomerLGASpinner);
        location_label = findViewById(R.id.location_label);
//        city = findViewById(R.id.newCustomerCityTv);
//        state = findViewById(R.id.customerStateTv);
//        country = findViewById(R.id.customerCountryTv);
//        lga = findViewById(R.id.customerLgaTv);
        customerPhoneNumber = findViewById(R.id.customerPhoneNumberEt);
        customerCreditLimit = findViewById(R.id.creditLimitEt);
        customerCreditDays = findViewById(R.id.creditDaysEt);

        customerEmailId = findViewById(R.id.customerEmailEt);
        captureCustomerImage = findViewById(R.id.captureCustomerImageView);
        showCustomerImage = findViewById(R.id.showCustomerImageView);
        radioGroup = findViewById(R.id.radioGroup);
        input_layout_credit_limit = findViewById(R.id.input_layout_credit_limit);
        input_layout_credit_days = findViewById(R.id.input_layout_credit_days);

//        primary = findViewById(R.id.Primary);
        secondary = findViewById(R.id.Secondary);
        doctor = findViewById(R.id.doctor);
        prospect = findViewById(R.id.Prospect);


        customerSaveButton = findViewById(R.id.saveCustomerImageView);
        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        globals = ((Globals) getApplicationContext());
        constants = new Constants();

    }

    public void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time1 = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            nav_type = bundle.getString("nav_type");
            customer_type = bundle.getString("customer_type");
        }

        if ("Reports".equals(nav_type)) {
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {

//                if (customer_type.equals("Lead") || customer_type.equals("Primary")) {
//                if (customer_type.equals("Lead")) {
//                    customer_type = "Lead";
//                    radioGroup.check(R.id.Primary);
//                    Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();
//
//
//                } else
                    if (customer_type.equals("Secondary"))  {
                    customer_type = "Secondary";
                    radioGroup.check(R.id.Secondary);
//                    Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();

                } else if(customer_type.equals("Doctor")) {

                    customer_type = "Doctor";
                    radioGroup.check(R.id.doctor);
//                    Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();

                }else if (customer_type.equals("Prospect")) {
                    customer_type = "Prospect";
                    radioGroup.check(R.id.Prospect);
//                    Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();

                }

//                random_num = Integer.parseInt(customerDetails.getString(37));
                customerCode.setText(customerDetails.getString(1));
                customerName.setText(customerDetails.getString(2));
                customerAddressLine1.setText(customerDetails.getString(6));
                customerAddressLine2.setText(customerDetails.getString(7));
                customerAddressLine3 .setText( customerDetails.getString(8));
                customerPhoneNumber.setText( customerDetails.getString(11));
                customerCreditLimit.setText( customerDetails.getString(12));
                customerCreditDays.setText( customerDetails.getString(13));
                customerEmailId.setText(customerDetails.getString(33));

                distributorArrayList = myDBHandler.getDistributorsName();
                distributorArrayList.add(0, "Select");
                distributorSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, distributorArrayList);
                distributorSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                distributorSpinner.setAdapter(distributorSpinnerArrayAdapter);

                int spinnerPosi = distributorSpinnerArrayAdapter.getPosition(customerDetails.getString(45));

                distributorSpinner.setSelection(spinnerPosi);

                distributorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int pos, long id) {

                        if (pos != 0) {
                            distributorString = adapterView.getItemAtPosition(pos).toString();
                            distributorCodeString = myDBHandler.getDistributorsCode(distributorString);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {

                    }
                });


                cityArrayList = myDBHandler.getCityName();
                cityArrayList.add(0, "Select");
                citySpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, cityArrayList);
                citySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                newCustomerCitySpinner.setAdapter(citySpinnerArrayAdapter);

                int spinnerPosition1 = citySpinnerArrayAdapter.getPosition(customerDetails.getString(9));

                newCustomerCitySpinner.setSelection(spinnerPosition1);

                newCustomerCitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int pos, long id) {
                        if(pos !=0)
                        cityString = adapterView.getItemAtPosition(pos).toString();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {

                    }
                });


                locationArrayList = myDBHandler.getLocationName();
                locationArrayList.add(0, "Select");
                locationSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, locationArrayList);
                locationSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                customerLocationSpinner.setAdapter(locationSpinnerArrayAdapter);

                int spinnerPosition = locationSpinnerArrayAdapter.getPosition(customerDetails.getString(30));

                customerLocationSpinner.setSelection(spinnerPosition);

                customerLocationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int pos, long id) {

                        if(pos !=0)
                        locationString =  adapterView.getItemAtPosition(pos).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {

                    }
                });




                stateArrayList = myDBHandler.getStateName();
                stateArrayList.add(0, "Select");
                stateSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, stateArrayList);
                stateSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                newCustomerStateSpinner.setAdapter(stateSpinnerArrayAdapter);

                int stateArrayListrPosition = stateSpinnerArrayAdapter.getPosition(customerDetails.getString(10));

                newCustomerStateSpinner.setSelection(stateArrayListrPosition);

                newCustomerStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int pos, long id) {
                        if(pos !=0)
                        stateString = adapterView.getItemAtPosition(pos).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {

                    }
                });

                countryArrayList = myDBHandler.getCountryName();
                countryArrayList.add(0, "Select");
                countrySpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, countryArrayList);
                countrySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                newCustomerCountrySpinner.setAdapter(countrySpinnerArrayAdapter);

                int countryArrayListPosition = countrySpinnerArrayAdapter.getPosition(customerDetails.getString(32));

                newCustomerCountrySpinner.setSelection(countryArrayListPosition);

                newCustomerCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int pos, long id) {
                        if(pos !=0)
                        countryString = adapterView.getItemAtPosition(pos).toString();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {

                    }
                });

                lgaArrayList = myDBHandler.getLgaName();
                lgaArrayList.add(0, "Select");
                lgaSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, lgaArrayList);
                lgaSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                newCustomerLGASpinner.setAdapter(lgaSpinnerArrayAdapter);
                int lgaArrayListPosition = lgaSpinnerArrayAdapter.getPosition(customerDetails.getString(31));

                newCustomerLGASpinner.setSelection(lgaArrayListPosition);

                newCustomerLGASpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int pos, long id) {
                        if(pos !=0)
                        lgaString = adapterView.getItemAtPosition(pos).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {

                    }
                });



                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    showCustomerImage.setImageBitmap(customerImageBitmap);
                }

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }


            }
        }
        }else{
            cityArrayList = myDBHandler.getCityName();
            cityArrayList.add(0, "Select");
            citySpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, cityArrayList);
            citySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            newCustomerCitySpinner.setAdapter(citySpinnerArrayAdapter);

            newCustomerCitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    if(pos !=0)
                    cityString = adapterView.getItemAtPosition(pos).toString();

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {

                }
            });


            distributorArrayList = myDBHandler.getDistributorsName();
            distributorArrayList.add(0, "Select");
            distributorSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, distributorArrayList);
            distributorSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            distributorSpinner.setAdapter(distributorSpinnerArrayAdapter);


            distributorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {

                    if (pos != 0) {
                        distributorString = adapterView.getItemAtPosition(pos).toString();
                        distributorCodeString = myDBHandler.getDistributorsCode(distributorString);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {

                }
            });

            locationArrayList = myDBHandler.getLocationName();
            locationArrayList.add(0, "Select");
            locationSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, locationArrayList);
            locationSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            customerLocationSpinner.setAdapter(locationSpinnerArrayAdapter);


            customerLocationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    if(pos !=0)
                    locationString =  adapterView.getItemAtPosition(pos).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {

                }
            });




            stateArrayList = myDBHandler.getStateName();
            stateArrayList.add(0, "Select");
            stateSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, stateArrayList);
            stateSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            newCustomerStateSpinner.setAdapter(stateSpinnerArrayAdapter);


            newCustomerStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    if(pos !=0)
                    stateString = adapterView.getItemAtPosition(pos).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {

                }
            });

            countryArrayList = myDBHandler.getCountryName();
            countryArrayList.add(0, "Select");
            countrySpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, countryArrayList);
            countrySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            newCustomerCountrySpinner.setAdapter(countrySpinnerArrayAdapter);


            newCustomerCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    if(pos !=0)
                    countryString = adapterView.getItemAtPosition(pos).toString();

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {

                }
            });

            lgaArrayList = myDBHandler.getLgaName();
            lgaArrayList.add(0, "Select");
            lgaSpinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, lgaArrayList);
            lgaSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            newCustomerLGASpinner.setAdapter(lgaSpinnerArrayAdapter);

            newCustomerLGASpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    if(pos !=0)
                    lgaString = adapterView.getItemAtPosition(pos).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {

                }
            });
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO1) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    selectedImage = getResizedBitmap(selectedImage, 50);
                    showCustomerImage.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAPTURE_PHOTO1) {
            if (resultCode == RESULT_OK) {

                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

              /*  image_id = dbHandler.get_customerimage_gallery_image_id();
                image_id = image_id + 1;
                String imagename = "customer_image"+image_id;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                dbHandler.insertcustomerimagegallery(image_id,imagename,customerimage,customer_id,sdfd.format(date),null);*/

                onCaptureImageResult(data);

                showCustomerImage.setDrawingCacheEnabled(true);
                showCustomerImage.buildDrawingCache();
                Bitmap bitmap = showCustomerImage.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] mileage_data = baos.toByteArray();
                //dbHandler.updatecustomerimage(customer_id, mileage_data);
                customer_image = baos.toByteArray();
                customer_image_bitmap = bitmap;
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                showCustomerImage.setEnabled(true);

            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        //set Progress Bar
        setProgressBar();
        //set profile picture form camera
        showCustomerImage.setMaxWidth(1);
        showCustomerImage.setImageBitmap(thumbnail);
        selectedImage = thumbnail;
        showCustomerImage.setMaxHeight(1);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    class Radio_check implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          /*  if (primary.isChecked()) {
                customer_type = "Lead";

                //Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();


            } else*/
                if (secondary.isChecked()) {
                customer_type = "Secondary";
                    input_layout_credit_limit.setVisibility(View.VISIBLE);
                    input_layout_credit_days.setVisibility(View.VISIBLE);
                //Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();

            } else if (doctor.isChecked()) {
                customer_type = "Doctor";
                    input_layout_credit_limit.setVisibility(View.GONE);
                    input_layout_credit_days.setVisibility(View.GONE);
                   /* customerCreditLimit.setVisibility(View.GONE);
                    customerCreditDays.setVisibility(View.GONE);*/

                //Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();

            }else if (prospect.isChecked()) {
                customer_type = "Prospect";
                    input_layout_credit_limit.setVisibility(View.GONE);
                    input_layout_credit_days.setVisibility(View.GONE);

                  /*  customerCreditLimit.setVisibility(View.GONE);
                    customerCreditDays.setVisibility(View.GONE);*/
                //Toast.makeText(NewCustomerActivity.this, customer_type, Toast.LENGTH_SHORT).show();

            }

        }
    }

}
