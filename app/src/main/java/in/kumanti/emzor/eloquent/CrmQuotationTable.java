package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CrmQuotation;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_QUOTATION;

public class CrmQuotationTable extends SQLiteOpenHelper {

    private static final String COLUMN_QUOTATION_ID = "quotationId";
    private static final String COLUMN_OPPORTUNITY_ID = "opportunityId";
    private static final String COLUMN_QUOTATION_NUMBER = "quotationNo";
    private static final String COLUMN_QUOTATION_DATE = "quotationDate";
    private static final String COLUMN_QUOTATION_STATUS = "quotationStatus";
    private static final String COLUMN_QUOTATION_DELIVERY_DATE = "quotationDeliveryDate";
    private static final String COLUMN_QUOTATION_CREATED_BY = "quotationCreatedBy";
    private static final String COLUMN_QUOTATION_CREATED_AT = "createdAt";
    private static final String COLUMN_QUOTATION_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmQuotationTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_QUOTATION);

        String quotationTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_QUOTATION + "(" +
                COLUMN_QUOTATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_OPPORTUNITY_ID + " INTEGER, " +
                COLUMN_QUOTATION_NUMBER + " TEXT, " +
                COLUMN_QUOTATION_DATE + " TEXT, " +
                COLUMN_QUOTATION_STATUS + " TEXT, " +
                COLUMN_QUOTATION_DELIVERY_DATE + " REAL, " +
                COLUMN_QUOTATION_CREATED_BY + " TEXT, " +
                COLUMN_QUOTATION_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_QUOTATION_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(quotationTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmQuotation quotation) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_OPPORTUNITY_ID, quotation.getOpportunityId());
        values.put(COLUMN_QUOTATION_NUMBER, quotation.getQuotationNo());
        values.put(COLUMN_QUOTATION_DATE, quotation.getQuotationDate());
        values.put(COLUMN_QUOTATION_STATUS, quotation.getQuotationStatus());
        values.put(COLUMN_QUOTATION_DELIVERY_DATE, quotation.getDeliveryDate());
        values.put(COLUMN_QUOTATION_CREATED_BY, quotation.getQuotationCreatedBy());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_QUOTATION, null, values);
        db.close();
        return id;
    }


}
