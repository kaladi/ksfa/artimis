package in.kumanti.emzor.model;

import java.util.ArrayList;

public class BdeReceiveProducts {

    public int id;
    public int customerReturnId;
    public String stockIssueNumber;
    public String stockIssueDate;
    public String productCode;
    public String productName;
    public String productUom;
    public String issuePrice;
    public String issueValue;
    public String issueQuantity;
    public String latestSynDate;
    public String errorStockReturnQty;
    public String errorStockIssueProduct;
    public boolean isSync;
    public String isBatchControlled;
    public String createdAt;
    public String updatedAt;
    public ArrayList<BdeReceiveBatchProducts> bdeReceiveBatchProductsArrayList;
    private String toBdeCode;
    private String fromBdeCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToBdeCode() {
        return toBdeCode;
    }

    public void setToBdeCode(String toBdeCode) {
        this.toBdeCode = toBdeCode;
    }

    public String getFromBdeCode() {
        return fromBdeCode;
    }

    public void setFromBdeCode(String fromBdeCode) {
        this.fromBdeCode = fromBdeCode;
    }

    public int getCustomerReturnId() {
        return customerReturnId;
    }

    public void setCustomerReturnId(int customerReturnId) {
        this.customerReturnId = customerReturnId;
    }

    public String getStockIssueNumber() {
        return stockIssueNumber;
    }

    public void setStockIssueNumber(String stockIssueNumber) {
        this.stockIssueNumber = stockIssueNumber;
    }

    public String getStockIssueDate() {
        return stockIssueDate;
    }

    public void setStockIssueDate(String stockIssueDate) {
        this.stockIssueDate = stockIssueDate;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }

    public String getIssuePrice() {
        return issuePrice;
    }

    public void setIssuePrice(String issuePrice) {
        this.issuePrice = issuePrice;
    }

    public String getIssueValue() {
        return issueValue;
    }

    public void setIssueValue(String issueValue) {
        this.issueValue = issueValue;
    }

    public String getIssueQuantity() {
        return issueQuantity;
    }

    public void setIssueQuantity(String issueQuantity) {
        this.issueQuantity = issueQuantity;
    }

    public String getLatestSynDate() {
        return latestSynDate;
    }

    public void setLatestSynDate(String latestSynDate) {
        this.latestSynDate = latestSynDate;
    }

    public String getErrorStockReturnQty() {
        return errorStockReturnQty;
    }

    public void setErrorStockReturnQty(String errorStockReturnQty) {
        this.errorStockReturnQty = errorStockReturnQty;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getErrorStockIssueProduct() {
        return errorStockIssueProduct;
    }

    public void setErrorStockIssueProduct(String errorStockIssueProduct) {
        this.errorStockIssueProduct = errorStockIssueProduct;
    }

    public String getIsBatchControlled() {
        return isBatchControlled;
    }

    public void setIsBatchControlled(String isBatchControlled) {
        this.isBatchControlled = isBatchControlled;
    }

    public ArrayList<BdeReceiveBatchProducts> getBdeReceiveBatchProductsArrayList() {
        return bdeReceiveBatchProductsArrayList;
    }

    public void setBdeReceiveBatchProductsArrayList(ArrayList<BdeReceiveBatchProducts> bdeReceiveBatchProductsArrayList) {
        this.bdeReceiveBatchProductsArrayList = bdeReceiveBatchProductsArrayList;
    }
}
