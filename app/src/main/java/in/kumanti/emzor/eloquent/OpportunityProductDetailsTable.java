package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.OpportunityProductDetails;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_OPPORTUNITY_PRODUCT_DETAILS;

public class OpportunityProductDetailsTable extends SQLiteOpenHelper {

    private static final String COLUMN_OPPORTUNITY_DETAILS_ID = "id";
    private static final String COLUMN_OPPORTUNITY_ID = "opportunity_id";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_GROUP_ID = "product_group_id";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_ID = "product_id";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_NAME = "product_name";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_CODE = "product_code";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_UOM = "product_uom";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_QUANTITY = "product_quantity";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_REMARKS = "product_remarks";
    private static final String COLUMN_OPPORTUNITY_STATUS = "product_status";
    private static final String COLUMN_PRODUCT_DESIGN_SPECIFICATION = "product_design_specification";
    private static final String COLUMN_QUOTATION_PRICE = "product_price";
    private static final String COLUMN_QUOTATION_VALUE = "product_value";
    private static final String COLUMN_OPPORTUNITY_DATE = "opportunity_date";
    private static final String COLUMN_OPPORTUNITY_CREATED_AT = "created_at";
    private static final String COLUMN_OPPORTUNITY_UPDATED_AT = "updated_at";

    public Context context;

    public OpportunityProductDetailsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_OPPORTUNITY_PRODUCT_DETAILS);

        String opportunityDetailTable = "CREATE TABLE IF NOT EXISTS " + TABLE_OPPORTUNITY_PRODUCT_DETAILS + "(" +
                COLUMN_OPPORTUNITY_DETAILS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_OPPORTUNITY_ID + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_GROUP_ID + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_ID + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_NAME + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_CODE + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_UOM + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_QUANTITY + " REAL, " +
                COLUMN_OPPORTUNITY_PRODUCT_REMARKS + " REAL, " +
                COLUMN_OPPORTUNITY_STATUS + " BOOLEAN, " +
                COLUMN_PRODUCT_DESIGN_SPECIFICATION + " TEXT, " +
                COLUMN_QUOTATION_PRICE + " REAL, " +
                COLUMN_QUOTATION_VALUE + " REAL, " +
                COLUMN_OPPORTUNITY_DATE + " TEXT, " +
                COLUMN_OPPORTUNITY_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_OPPORTUNITY_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(opportunityDetailTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(OpportunityProductDetails od) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_OPPORTUNITY_ID, od.opportunity_id);
        values.put(COLUMN_OPPORTUNITY_PRODUCT_GROUP_ID, od.product_group_id);
        values.put(COLUMN_OPPORTUNITY_PRODUCT_ID, od.product_id);
        values.put(COLUMN_OPPORTUNITY_PRODUCT_NAME, od.opportunity_product_name);
        values.put(COLUMN_OPPORTUNITY_PRODUCT_CODE, od.opportunity_product_code);
        values.put(COLUMN_OPPORTUNITY_PRODUCT_UOM, od.opportunity_product_uom);
        values.put(COLUMN_OPPORTUNITY_PRODUCT_QUANTITY, od.opportunity_product_quantity);
        values.put(COLUMN_OPPORTUNITY_PRODUCT_REMARKS, od.opportunity_product_remarks);
        values.put(COLUMN_OPPORTUNITY_STATUS, od.opportunity_product_status);

        if (od.product_specification != null)
            values.put(COLUMN_PRODUCT_DESIGN_SPECIFICATION, od.product_specification);

        values.put(COLUMN_QUOTATION_PRICE, od.quotation_product_price);
        values.put(COLUMN_QUOTATION_VALUE, od.quotation_product_value);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_OPPORTUNITY_PRODUCT_DETAILS, null, values);
        db.close();
        return id;
    }

   /* public ArrayList<OpportunityProductDetails> getOpportunityDetails(String opportunityId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_OPPORTUNITY_PRODUCT_DETAILS + " WHERE "+ COLUMN_OPPORTUNITY_ID + "=?";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {opportunityId} );
        Log.d("Data","getCompetitorDetailsByProductId  " );
        ArrayList<OpportunityProductDetails> opportunityProductDetailsArrayList = new ArrayList<>();

        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            OpportunityProductDetails od = new OpportunityProductDetails();
            od.setId(cursor.getInt(0));
            od.setProduct_group_id(cursor.getString(1));
            od.setProduct_id(cursor.getString(2));
            od.setOpportunity_product_name(cursor.getString(4));
            od.setOpportunity_product_code(cursor.getString(5));
            od.setOpportunity_product_uom(cursor.getString(6));
            od.setOpportunity_product_quantity(cursor.getString(7));
            od.setOpportunity_product_remarks(cursor.getString(8));
            od.setOpportunity_product_status(cursor.getInt(9) > 0);
            od.setProduct_specification(cursor.getString(10));
            od.setQuotation_product_price(cursor.getString(11));
            od.setQuotation_product_volume(cursor.getString(12));
            opportunityProductDetailsArrayList.add(i,od);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return opportunityProductDetailsArrayList;
    }*/

    /*public void deleteByOpportunityId(String opportunityId){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_OPPORTUNITY_PRODUCT_DETAILS, COLUMN_OPPORTUNITY_ID+ "='" + opportunityId+"'", null);
        db.close();
    }*/


}
