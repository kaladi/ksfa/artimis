package in.kumanti.emzor.model;


public class GeneralSettings {

    public int settingsId;
    public String keyname;
    public String value;
    public String createdAt;
    public String updatedAt;

    public int getSettingsId() {
        return settingsId;
    }

    public void setSettingsId(int settingsId) {
        this.settingsId = settingsId;
    }

    public String getConfigKey() {
        return keyname;
    }

    public void setConfigKey(String configKey) {
        this.keyname = configKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
