package in.kumanti.emzor.model;

public class OpportunityProductDetails {

    public int id;
    public String opportunity_id;
    public String product_group_id;
    public String product_id;
    public String opportunity_product_name;
    public String opportunity_product_code;
    public String opportunity_product_uom;
    public String opportunity_product_quantity;
    public String opportunity_product_remarks;
    public boolean opportunity_product_status;
    public String product_specification;
    public String product_specification_temp;
    public String quotation_product_price;
    public String quotation_product_value;
    public String created_at;
    public String updated_at;

    public String getOpportunity_id() {
        return opportunity_id;
    }

    public void setOpportunity_id(String opportunity_id) {
        this.opportunity_id = opportunity_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_group_id() {
        return product_group_id;
    }

    public void setProduct_group_id(String product_group_id) {
        this.product_group_id = product_group_id;
    }


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }


    public String getOpportunity_product_name() {
        return opportunity_product_name;
    }

    public void setOpportunity_product_name(String opportunity_product_name) {
        this.opportunity_product_name = opportunity_product_name;
    }


    public String getOpportunity_product_code() {
        return opportunity_product_code;
    }

    public void setOpportunity_product_code(String opportunity_product_code) {
        this.opportunity_product_code = opportunity_product_code;
    }


    public String getOpportunity_product_uom() {
        return opportunity_product_uom;
    }

    public void setOpportunity_product_uom(String opportunity_product_uom) {
        this.opportunity_product_uom = opportunity_product_uom;
    }


    public String getOpportunity_product_quantity() {
        return opportunity_product_quantity;
    }

    public void setOpportunity_product_quantity(String opportunity_product_quantity) {
        this.opportunity_product_quantity = opportunity_product_quantity;
    }


    public String getOpportunity_product_remarks() {
        return opportunity_product_remarks;
    }

    public void setOpportunity_product_remarks(String opportunity_product_remarks) {
        this.opportunity_product_remarks = opportunity_product_remarks;
    }


    public Boolean getOpportunity_product_status() {
        return opportunity_product_status;
    }

    public void setOpportunity_product_status(Boolean opportunity_product_status) {
        this.opportunity_product_status = opportunity_product_status;
    }


    public String getQuotation_product_price() {
        return quotation_product_price;
    }

    public void setQuotation_product_price(String quotation_product_price) {
        this.quotation_product_price = quotation_product_price;
    }


    public String getQuotation_product_volume() {
        return quotation_product_value;
    }

    public void setQuotation_product_volume(String quotation_product_volume) {
        this.quotation_product_value = quotation_product_volume;
    }


    public String getProduct_specification() {
        return product_specification;
    }

    public void setProduct_specification(String product_specification) {
        this.product_specification = product_specification;
    }

    public String getProduct_specification_temp() {
        return product_specification_temp;
    }

    public void setProduct_specification_temp(String product_specification_temp) {
        this.product_specification_temp = product_specification_temp;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


}
