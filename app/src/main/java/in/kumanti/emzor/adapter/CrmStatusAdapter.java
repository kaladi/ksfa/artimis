package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CrmStatus;

public class CrmStatusAdapter extends RecyclerView.Adapter<CrmStatusAdapter.ViewHolder> {

    public ArrayList<CrmStatus> mDataset;
    public Context context;

    public CrmStatusAdapter(ArrayList<CrmStatus> myDataset) {
        mDataset = myDataset;
    }

    public void add(CrmStatus item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmStatus item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @NotNull
    @Override
    public CrmStatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_crm_status, parent, false);

        context = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder viewHolder, final int position) {

        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getCrmStatusDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.crmStatusDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }

        viewHolder.crmStatus.setText(mDataset.get(position).getCrmStatus());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView crmStatusDate, crmStatus;

        public ViewHolder(View v) {
            super(v);
            crmStatusDate = v.findViewById(R.id.crmStatusDateTv);
            crmStatus = v.findViewById(R.id.crmStatusTv);

        }
    }


}