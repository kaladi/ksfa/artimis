package in.kumanti.emzor.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.GraphData;

public class GraphPageAdapter extends PagerAdapter implements OnChartValueSelectedListener {

    int numberOfAddress = 0;
    ArrayList<GraphData> dataSet;
    private Context mContext;

    public GraphPageAdapter(Context context, ArrayList<GraphData> dataSet) {
        mContext = context;
        this.dataSet = dataSet;
        numberOfAddress = dataSet.size();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Activity", "Selected: " + e.toString() + ", dataSet: " + h.getDataSetIndex());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Activity", "Nothing selected.");
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        GraphData gd = dataSet.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final ViewGroup page = (ViewGroup) inflater.inflate(R.layout.list_focus_scheme_bar_graph, container, false);
        BarChart barChart;
        TextView titleTextView;


        //CHART INITIALIZATION
        titleTextView = page.findViewById(R.id.primaryChartTitle);
        titleTextView.setText(gd.title);
        barChart = page.findViewById(R.id.primaryBarChart);
        barChart.setOnChartValueSelectedListener(this);
        barChart.getDescription().setEnabled(false);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);

        barChart.setDrawBarShadow(false);
        barChart.animateY(1400, Easing.EaseInOutQuad);


        barChart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        //l.setTypeface(tfLight);
        l.setYOffset(10f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(20f);


        XAxis xAxis = barChart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        String[] values = new String[]{"Products Sold", "Product Volume", "Product Value"};

        xAxis.setValueFormatter(new MyXAxisGraphValueFormatter(values));

        YAxis leftAxis = barChart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceBottom(50f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setGranularity(1f);
        barChart.getAxisRight().setEnabled(false);
        //DATA INITIALIZATION
        float groupSpace = 0.3f;
        float barSpace = 0.05f; // x4 DataSet
        float barWidth = 0.3f; // x4 DataSet
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

        int groupCount = 3;
        int startYear = 1;


        BarDataSet set1, set2;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);

            set1.setValues(gd.targetValues);
            set2.setValues(gd.achievementValues);

            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();

        } else {
            // create 2 DataSets
            set1 = new BarDataSet(gd.targetValues, "Target");
            set1.setColor(Color.rgb(131, 131, 131));
            set2 = new BarDataSet(gd.achievementValues, "Achievement");
            set2.setColor(Color.rgb(255, 124, 45));

            BarData data = new BarData(set1, set2);
            data.setValueFormatter(new LargeValueFormatter());
            //data.setValueTypeface(tfLight);

            barChart.setData(data);
        }

        // specify the width each bar should have
        barChart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        barChart.getXAxis().setAxisMinimum(startYear);

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        barChart.getXAxis().setAxisMaximum(startYear + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        Log.d("Metrics", "Maximum" + barChart.getXAxis().getAxisMaximum());
        barChart.groupBars(startYear, groupSpace, barSpace);
        barChart.invalidate();


        Log.d("MYDATA", "Test Instance" + position);
        container.addView(page);
        return page;
    }

    @Override
    public int getCount() {
        return numberOfAddress;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);

    }


}

class MyXAxisGraphValueFormatter implements IAxisValueFormatter {

    private String[] mValues;

    public MyXAxisGraphValueFormatter(String[] values) {
        this.mValues = values;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        Log.d("Metrics", "" + value);
        if (value - 1 < 0)
            value = 1;
        if (value - 1 > 2)
            value = 3;
        // "value" represents the position of the label on the axis (x or y)
        return mValues[(int) value - 1];
    }
}