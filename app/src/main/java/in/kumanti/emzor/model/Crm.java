package in.kumanti.emzor.model;


import java.io.Serializable;

public class Crm implements Serializable {

    private int crmId;
    public String companyCode;
    public String tabCode;
    public String srCode;
    private String customerCode;
    public String crmNumber;
    private String crmDate;
    private String currency;
    private String status;
    private String description;
    private String crmLines;
    private String crmBudget;
    private String crmValue;
    private boolean isSync;
    private String latestSyncDate;
    private int randomNumber;
    private String createdAt;
    private String reason;


    public int getCrmId() {
        return crmId;
    }

    public void setCrmId(int crmId) {
        this.crmId = crmId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCrmNumber() {
        return crmNumber;
    }

    public void setCrmNumber(String crmNumber) {
        this.crmNumber = crmNumber;
    }

    public String getCrmDate() {
        return crmDate;
    }

    public void setCrmDate(String crmDate) {
        this.crmDate = crmDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCrmLines() {
        return crmLines;
    }

    public void setCrmLines(String crmLines) {
        this.crmLines = crmLines;
    }

    public String getCrmBudget() {
        return crmBudget;
    }

    public void setCrmBudget(String crmBudget) {
        this.crmBudget = crmBudget;
    }

    public String getCrmValue() {
        return crmValue;
    }

    public void setCrmValue(String crmValue) {
        this.crmValue = crmValue;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getLatestSyncDate() {
        return latestSyncDate;
    }

    public void setLatestSyncDate(String latestSyncDate) {
        this.latestSyncDate = latestSyncDate;
    }

    public int getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(int randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    private String updatedAt;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return crmNumber;
    }


}
