package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.LogInOut;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_LOG_IN_OUT_HISTORY;

public class LogInOutHistoryTable extends SQLiteOpenHelper {

    private static final String COLUMN_LOG_IN_OUT_ID = "logInOutId";
    private static final String COLUMN_LOG_IN_DATE = "logInDate";
    private static final String COLUMN_LOG_IN_TIME = "logInTime";
    private static final String COLUMN_LOG_OUT_DATE = "logOutDate";
    private static final String COLUMN_LOG_OUT_TIME = "logOutTime";
    private static final String COLUMN_LOG_IN_OUT_CREATED_AT = "createdAt";
    private static final String COLUMN_LOG_IN_OUT_UPDATED_AT = "updatedAt";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public LogInOutHistoryTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG_IN_OUT_HISTORY);

        String logInOutTable = "CREATE TABLE IF NOT EXISTS " + TABLE_LOG_IN_OUT_HISTORY + "(" +
                COLUMN_LOG_IN_OUT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_LOG_IN_DATE + " TEXT, " +
                COLUMN_LOG_IN_TIME + " TEXT, " +
                COLUMN_LOG_OUT_DATE + " TEXT, " +
                COLUMN_LOG_OUT_TIME + " TEXT, " +
                COLUMN_LOG_IN_OUT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_LOG_IN_OUT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(logInOutTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(LogInOut logInOut) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_LOG_IN_DATE, logInOut.getLogInDate());
        values.put(COLUMN_LOG_IN_TIME, logInOut.getLogInTime());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_LOG_IN_OUT_HISTORY, null, values);
        db.close();
        return id;

    }

    public void updaterLogout(LogInOut logInOut, String loginId) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_LOG_OUT_DATE, logInOut.getLogOutDate());
        values.put(COLUMN_LOG_OUT_TIME, logInOut.getLogOutTime());

        SQLiteDatabase db = getWritableDatabase();
        db.update(TABLE_LOG_IN_OUT_HISTORY, values, COLUMN_LOG_IN_OUT_ID + " = ?", new String[]{loginId});
        db.close();
    }

    public ArrayList<LogInOut> getLoginOutHistory() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_LOG_IN_OUT_HISTORY;

        ArrayList<LogInOut> lpoImagesArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            LogInOut logInOut = new LogInOut();
            logInOut.setTabCode(Constants.TAB_CODE);
            logInOut.setCompanyCode(Constants.COMPANY_CODE);
            logInOut.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            logInOut.setLogInOutId(id);
            logInOut.setLogInDate(cursor.getString(1));
            logInOut.setLogInTime(cursor.getString(2));
            logInOut.setLogOutDate(cursor.getString(3));
            logInOut.setLogOutTime(cursor.getString(4));
            logInOut.setCreatedAt(cursor.getString(5));
            logInOut.setUpdatedAt(cursor.getString(6));

            lpoImagesArrayList.add(logInOut);
        }
        cursor.close();
        sqlWrite.close();
        return lpoImagesArrayList;
    }

   /* public ArrayList<LogInOut> getLogInOutList() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_LOG_IN_OUT_HISTORY;

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );
        Log.d("Data","Get Log In Out Lists  " );
        ArrayList<LogInOut> logInOutArrayList = new ArrayList<>();

        while (cursor.moveToNext()) {
            LogInOut warehouse = new LogInOut();
            int id = cursor.getInt(0);
            warehouse.setLogInOutId(id);
            warehouse.setLogInDate(cursor.getString(1));
            warehouse.setLogInTime(cursor.getString(2));
            warehouse.setLogOutDate(cursor.getString(3));
            warehouse.setLogOutTime(cursor.getString(3));
            logInOutArrayList.add(warehouse);
        }
        cursor.close();
        sqlWrite.close();
        return logInOutArrayList;
    }*/

    /*public void deleteLogInOutHistory() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_LOG_IN_OUT_HISTORY, null, null);
        db.close();
    }*/
}
