package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.ViewStockWarehouseProduct;
import in.kumanti.emzor.model.Warehouse;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_PRIMARY_INVOICE;
import static in.kumanti.emzor.utils.Constants.TABLE_WAREHOUSE;

public class WarehouseTable extends SQLiteOpenHelper {

    private static final String COLUMN_WAREHOUSE_ID = "id";
    private static final String COLUMN_SALES_PERSON_CODE = "sr_code";
    private static final String COLUMN_WAREHOUSE_CODE = "warehouse_code";
    private static final String COLUMN_WAREHOUSE_NAME = "warehouse_name";
    private static final String COLUMN_WAREHOUSE_CREATED_AT = "created_at";
    private static final String COLUMN_WAREHOUSE_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public WarehouseTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_WAREHOUSE);

        String surveyTable = "CREATE TABLE IF NOT EXISTS " + TABLE_WAREHOUSE + "(" +
                COLUMN_WAREHOUSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SALES_PERSON_CODE + " TEXT, " +
                COLUMN_WAREHOUSE_CODE + " TEXT, " +
                COLUMN_WAREHOUSE_NAME + " TEXT, " +
                COLUMN_WAREHOUSE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_WAREHOUSE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(surveyTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(Warehouse warehouse) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_WAREHOUSE_NAME, warehouse.getWarehouse_name());
        values.put(COLUMN_WAREHOUSE_CODE, warehouse.getWarehouse_code());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_WAREHOUSE, null, values);
        db.close();
        return id;
    }

    /*
     * Fetching the Warehouse Options List from the Master via Web Service
     */
    public void insertingWarehousesMaster(ArrayList<ViewStockWarehouseProduct> warehouseProductsList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (warehouseProductsList != null) {
                for (int i = 0; i < warehouseProductsList.size(); i++) {
                    values.put(COLUMN_WAREHOUSE_NAME, warehouseProductsList.get(i).getWarehouseName());
                    values.put(COLUMN_WAREHOUSE_CODE, warehouseProductsList.get(i).getWarehouseCode());
                    db.insert(TABLE_WAREHOUSE, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }
    }


    public ArrayList<Warehouse> getWarehouseListByPrimaryInvoice() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_WAREHOUSE + " WHERE warehouse_code IN (SELECT warehouse_id FROM " + TABLE_PRIMARY_INVOICE + " GROUP BY warehouse_id) GROUP BY warehouse_code  ORDER BY warehouse_name ASC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("Data", "Get Warehouse Lists  ");
        ArrayList<Warehouse> warehouseList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Warehouse w = new Warehouse();
            w.setWarehouse_code(cursor.getString(2));
            w.setWarehouse_name(cursor.getString(3));
            warehouseList.add(w);
        }
        cursor.close();
        sqlWrite.close();
        return warehouseList;
    }

   /* public ArrayList<Warehouse> getWarehouseList() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_WAREHOUSE +" GROUP BY warehouse_code ORDER BY warehouse_name ASC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        Log.d("Data","Get Warehouse Lists  " );
        ArrayList<Warehouse> warehouseList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Warehouse warehouse = new Warehouse();
            int id = cursor.getInt(0);
            warehouse.setWarehouse_id(id);
            warehouse.setSales_person_code(cursor.getString(1));
            warehouse.setWarehouse_code(cursor.getString(2));
            warehouse.setWarehouse_name(cursor.getString(3));
            warehouseList.add(warehouse);
        }
        cursor.close();
        sqlWrite.close();
        return warehouseList;
    }*/

    public void deleteWarehouse() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WAREHOUSE, null, null);
        db.close();
    }
}
