package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.ViewStockWarehouseProduct;
import in.kumanti.emzor.model.Warehouse;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_PRODUCT;
import static in.kumanti.emzor.utils.Constants.TABLE_WAREHOUSE;
import static in.kumanti.emzor.utils.Constants.TABLE_WAREHOUSE_STOCK_PRODUCT;

public class WarehouseStockProductsTable extends SQLiteOpenHelper {

    //Columns used for Warehouse Stock Product Information details for a selected ordered product
    private static final String COLUMN_WAREHOUSE_STOCK_PRODUCT_ID = "id";
    private static final String COLUMN_SALES_REP_ID = "sales_rep_id";
    private static final String COLUMN_WAREHOUSE_ID = "warehouse_id";
    private static final String COLUMN_PRODUCT_ID = "product_id";
    private static final String COLUMN_PRODUCT_NAME = "product_name";
    private static final String COLUMN_PRODUCT_UOM = "product_uom";
    private static final String COLUMN_PRODUCT_QUANTITY = "product_quantity";
    private static final String COLUMN_PRODUCT_PRICE = "product_price";
    private static final String COLUMN_PRODUCT_VALUE = "product_value";
    private static final String COLUMN_ORDER_COUNT = "product_order_count";
    private static final String COLUMN_WAREHOUSE_CREATED_AT = "created_at";
    private static final String COLUMN_WAREHOUSE_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public WarehouseStockProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_WAREHOUSE_STOCK_PRODUCT);

        String warehouseStockProdTable = "CREATE TABLE IF NOT EXISTS " + TABLE_WAREHOUSE_STOCK_PRODUCT + "(" +
                COLUMN_WAREHOUSE_STOCK_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SALES_REP_ID + " TEXT, " +
                COLUMN_WAREHOUSE_ID + " TEXT, " +
                COLUMN_PRODUCT_ID + " TEXT, " +
                COLUMN_PRODUCT_NAME + " TEXT, " +
                COLUMN_PRODUCT_UOM + " TEXT, " +
                COLUMN_PRODUCT_PRICE + " TEXT, " +
                COLUMN_PRODUCT_QUANTITY + " TEXT, " +
                COLUMN_PRODUCT_VALUE + " TEXT, " +
                COLUMN_ORDER_COUNT + " TEXT, " +
                COLUMN_WAREHOUSE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_WAREHOUSE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(warehouseStockProdTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(ViewStockWarehouseProduct stockWarehouseProduct) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_PRODUCT_ID, stockWarehouseProduct.getProductCode());
        values.put(COLUMN_WAREHOUSE_ID, stockWarehouseProduct.getWarehouseCode());
        values.put(COLUMN_PRODUCT_NAME, stockWarehouseProduct.getProductName());
        values.put(COLUMN_PRODUCT_UOM, stockWarehouseProduct.getProductUom());
        values.put(COLUMN_PRODUCT_QUANTITY, stockWarehouseProduct.getProductQuantity());
        values.put(COLUMN_PRODUCT_PRICE, stockWarehouseProduct.getProductPrice());
        values.put(COLUMN_PRODUCT_VALUE, stockWarehouseProduct.getProductTotalPrice());
        values.put(COLUMN_ORDER_COUNT, stockWarehouseProduct.getPrimaryOrderCount());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_WAREHOUSE_STOCK_PRODUCT, null, values);

        db.close();
        return id;
    }

    /*
     * Fetching the Warehouse Stock Products List details from the Master via Web Service
     */
    public void insertingWarehouseStockMaster(ArrayList<ViewStockWarehouseProduct> viewStockWarehouseProductArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        try {
            ContentValues values = new ContentValues();

            if (viewStockWarehouseProductArrayList != null) {
                for (int i = 0; i < viewStockWarehouseProductArrayList.size(); i++) {
                    values.put(COLUMN_PRODUCT_ID, viewStockWarehouseProductArrayList.get(i).getProductCode());
                    values.put(COLUMN_WAREHOUSE_ID, viewStockWarehouseProductArrayList.get(i).getWarehouseCode());
                    values.put(COLUMN_PRODUCT_NAME, viewStockWarehouseProductArrayList.get(i).getProductName());
                    values.put(COLUMN_PRODUCT_UOM, viewStockWarehouseProductArrayList.get(i).getProductUom());
                    values.put(COLUMN_PRODUCT_QUANTITY, viewStockWarehouseProductArrayList.get(i).getProductQuantity());
                    values.put(COLUMN_PRODUCT_PRICE, viewStockWarehouseProductArrayList.get(i).getProductPrice());
                    values.put(COLUMN_PRODUCT_VALUE, viewStockWarehouseProductArrayList.get(i).getProductTotalPrice());
                    values.put(COLUMN_ORDER_COUNT, viewStockWarehouseProductArrayList.get(i).getPrimaryOrderCount());

                    db.insert(TABLE_WAREHOUSE_STOCK_PRODUCT, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }
    }

    public void deleteWarehouseStockProduct() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WAREHOUSE_STOCK_PRODUCT, null, null);
        db.close();
    }

    public ArrayList<Product> getProductsByWareshouse(String warehouseCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        Cursor cursor;
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT + " WHERE product_id in (select wsp.product_id from " + TABLE_WAREHOUSE_STOCK_PRODUCT + " as wsp WHERE wsp.warehouse_id=? GROUP BY wsp.product_id) ORDER BY product_name ASC";
        if (warehouseCode.equals("-1")) {
            selectQuery = "SELECT * FROM " + TABLE_PRODUCT + " WHERE product_id in (select wsp.product_id from " + TABLE_WAREHOUSE_STOCK_PRODUCT + " as wsp GROUP BY wsp.product_id) ORDER BY product_name ASC";
            cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        } else
            cursor = sqlWrite.rawQuery(selectQuery, new String[]{warehouseCode});

        // Log.d("ViewStock--","Get Products  " );
        ArrayList<Product> products = new ArrayList<>();
        while (cursor.moveToNext()) {
            Product product = new Product();
            //Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            product.setId(cursor.getInt(0));
            product.setProduct_id(cursor.getString(1));
            product.setProduct_name(cursor.getString(2));
            product.setProduct_price(cursor.getString(5));
            product.setProduct_uom(cursor.getString(7));
            product.setBatch_controlled(cursor.getString(8));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }

    public ArrayList<Warehouse> getWarehouseList() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_WAREHOUSE + " WHERE warehouse_code in (select warehouse_id from " + TABLE_WAREHOUSE_STOCK_PRODUCT + " GROUP BY warehouse_id) GROUP BY warehouse_code ORDER BY warehouse_name ASC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        // Log.d("Data","Get Warehouse Lists  " );
        ArrayList<Warehouse> warehouseList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Warehouse w = new Warehouse();
            w.setWarehouse_name(cursor.getString(3));
            w.setWarehouse_code(cursor.getString(2));
            warehouseList.add(w);
        }
        cursor.close();
        sqlWrite.close();
        return warehouseList;
    }

}
