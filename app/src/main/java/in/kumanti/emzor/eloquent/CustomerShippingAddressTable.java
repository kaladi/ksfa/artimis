package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.CustomerShippingAddress;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_SHIPPING_ADDRESS;

public class CustomerShippingAddressTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_CI_ID = "id";
    private static final String COLUMN_CI_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_CI_CUSTOMER_NAME = "customer_name";
    private static final String COLUMN_CI_SALES_REP_ID = "sales_rep_id";
    private static final String COLUMN_CI_SALES_REP_NAME = "sales_rep_name";
    private static final String COLUMN_CI_SERIAL_NUMBER = "serial_number";
    private static final String COLUMN_CI_ADDRESS1 = "address1";
    private static final String COLUMN_CI_ADDRESS2 = "address2";
    private static final String COLUMN_CI_ADDRESS3 = "address3";
    private static final String COLUMN_CI_CITY = "city";
    private static final String COLUMN_CI_STATE = "state";
    private static final String COLUMN_CI_MOBILE_NUMBE = "mobile_number";
    private static final String COLUMN_CI_PINCODE = "pincode";
    private static final String COLUMN_CI_CUSTOMER_IMAGE = "customer_image";
    private static final String COLUMN_CI_SHIP_ADDRESS1 = "ship_address1";
    private static final String COLUMN_CI_SHIP_ADDRESS2 = "ship_address2";
    private static final String COLUMN_CI_SHIP_ADDRESS3 = "ship_address3";
    private static final String COLUMN_CI_SHIP_STATE = "ship_state";
    private static final String COLUMN_CI_SHIP_CITY = "ship_city";
    private static final String COLUMN_CI_SHIP_COUNTRY = "ship_country";
    private static final String COLUMN_CI_CUSTOMER_TYPE = "customer_type";
    private static final String COLUMN_CI_CREATED_AT = "created_at";
    private static final String COLUMN_CI_UPDATED_AT = "updated_at";

    public Context context;

    public CustomerShippingAddressTable(@Nullable Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPETITOR_INFO_DETAILS);

        String competitorInfoDetailTable = "CREATE TABLE IF NOT EXISTS " + TABLE_SHIPPING_ADDRESS + "(" +
                COLUMN_CI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CI_CUSTOMER_ID + " TEXT, " +
                COLUMN_CI_CUSTOMER_NAME + " TEXT, " +
                COLUMN_CI_SALES_REP_ID + " BOOLEAN, " +
                COLUMN_CI_SALES_REP_NAME + " REAL, " +
                COLUMN_CI_SERIAL_NUMBER + " REAL, " +
                COLUMN_CI_ADDRESS1 + " TEXT, " +
                COLUMN_CI_ADDRESS2 + " TEXT, " +
                COLUMN_CI_ADDRESS3 + " TEXT, " +
                COLUMN_CI_CITY + " TEXT, " +
                COLUMN_CI_STATE + " TEXT, " +
                COLUMN_CI_MOBILE_NUMBE + " TEXT, " +
                COLUMN_CI_PINCODE + " TEXT, " +
                COLUMN_CI_CUSTOMER_IMAGE + " TEXT, " +
                COLUMN_CI_SHIP_ADDRESS1 + " TEXT, " +
                COLUMN_CI_SHIP_ADDRESS2 + " TEXT, " +
                COLUMN_CI_SHIP_ADDRESS3 + " TEXT, " +
                COLUMN_CI_SHIP_STATE + " TEXT, " +
                COLUMN_CI_SHIP_CITY + " TEXT, " +
                COLUMN_CI_SHIP_COUNTRY + " TEXT, " +
                COLUMN_CI_CUSTOMER_TYPE + " TEXT, " +
                COLUMN_CI_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CI_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        Log.d("CompetitorInfo", "Database Created" + DATABASE_NAME);

        sqlWrite.execSQL(competitorInfoDetailTable);
        sqlWrite.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CustomerShippingAddress cd) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CI_CUSTOMER_ID, cd.customer_id);
        values.put(COLUMN_CI_SHIP_ADDRESS1, cd.ship_address1);
        values.put(COLUMN_CI_SHIP_ADDRESS2, cd.ship_address2);
        values.put(COLUMN_CI_SHIP_ADDRESS3, cd.ship_address3);
        values.put(COLUMN_CI_SHIP_CITY, cd.ship_city);
        values.put(COLUMN_CI_SHIP_STATE, cd.ship_state);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SHIPPING_ADDRESS, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Customer Shipping Address List details from the Master via Web Service
     */
    public void insertingCusShippingAddressMaster(ArrayList<CustomerShippingAddress> shipping_addressList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (shipping_addressList != null) {
                for (int i = 0; i < shipping_addressList.size(); i++) {
                    values.put(COLUMN_CI_CUSTOMER_ID, shipping_addressList.get(i).getCustomer_id());
                    values.put(COLUMN_CI_SHIP_ADDRESS1, shipping_addressList.get(i).getShip_address1());
                    values.put(COLUMN_CI_SHIP_ADDRESS2, shipping_addressList.get(i).getShip_address2());
                    values.put(COLUMN_CI_SHIP_ADDRESS3, shipping_addressList.get(i).getShip_address3());
                    values.put(COLUMN_CI_SHIP_CITY, shipping_addressList.get(i).getShip_city());
                    values.put(COLUMN_CI_SHIP_STATE, shipping_addressList.get(i).getShip_state());
                    db.insert(TABLE_SHIPPING_ADDRESS, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }

    public ArrayList<CustomerShippingAddress> getShippingAddressCompanyId(String customerId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SHIPPING_ADDRESS + " WHERE " + COLUMN_CI_CUSTOMER_ID + "=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerId});

        ArrayList<CustomerShippingAddress> shippingAddressesArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            CustomerShippingAddress csa = new CustomerShippingAddress();
            csa.setId(cursor.getInt(0));
            csa.setCustomer_id(cursor.getString(1));
            csa.setCustomer_name(cursor.getString(2));
            csa.setSales_rep_id(cursor.getString(3));
            csa.setSales_rep_name(cursor.getString(4));
            csa.setSerial_number(cursor.getString(5));
            csa.setAddress1(cursor.getString(6));
            csa.setAddress2(cursor.getString(7));
            csa.setAddress3(cursor.getString(8));
            csa.setCity(cursor.getString(9));
            csa.setState(cursor.getString(10));
            csa.setMobile_number(cursor.getString(11));
            csa.setPincode(cursor.getString(12));
            csa.setCustomer_image(cursor.getString(13));
            csa.setShip_address1(cursor.getString(14));
            csa.setShip_address2(cursor.getString(15));
            csa.setShip_address3(cursor.getString(16));
            csa.setShip_country(cursor.getString(17));
            csa.setShip_country(cursor.getString(18));
            csa.setShip_country(cursor.getString(19));
            shippingAddressesArrayList.add(i, csa);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return shippingAddressesArrayList;
    }

    public void deleteShippingAddress() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SHIPPING_ADDRESS, null, null);
        db.close();
    }

    /*public void deleteByProduct(String customerId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SHIPPING_ADDRESS, COLUMN_CI_CUSTOMER_ID + "='" + customerId + "'", null);
        db.close();
    }*/
}
