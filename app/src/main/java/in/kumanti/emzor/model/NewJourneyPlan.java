package in.kumanti.emzor.model;


public class NewJourneyPlan {

    public int journeyPlanId;
    public String companyCode;
    public String tabCode;
    public String bdeCode;
    public String bdeName;
    public String journeyPlanNumber;
    public String journeyPlanDate;
    public String bdeRegion;
    public String journeyStartDate;
    public String monday;
    public String tuesday;
    public String wednesday;
    public String thursday;
    public String friday;
    public String saturday;
    public String sunday;
    public String visitSequenceNumber;
    public int randomNumber;
    public String isSync;
    public String latestSynDate;
    public String journeyPlanStatus;
    public String status;
    public String createdAt;
    public String updatedAt;


    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public int getJourneyPlanId() {
        return journeyPlanId;
    }

    public void setJourneyPlanId(int journeyPlanId) {
        this.journeyPlanId = journeyPlanId;
    }

    public String getBdeCode() {
        return bdeCode;
    }

    public void setBdeCode(String bdeCode) {
        this.bdeCode = bdeCode;
    }

    public String getBdeName() {
        return bdeName;
    }

    public void setBdeName(String bdeName) {
        this.bdeName = bdeName;
    }

    public String getJourneyPlanNumber() {
        return journeyPlanNumber;
    }

    public void setJourneyPlanNumber(String journeyPlanNumber) {
        this.journeyPlanNumber = journeyPlanNumber;
    }

    public String getJourneyPlanDate() {
        return journeyPlanDate;
    }

    public void setJourneyPlanDate(String journeyPlanDate) {
        this.journeyPlanDate = journeyPlanDate;
    }

    public String getBdeRegion() {
        return bdeRegion;
    }

    public void setBdeRegion(String bdeRegion) {
        this.bdeRegion = bdeRegion;
    }

    public String getJourneyStartDate() {
        return journeyStartDate;
    }

    public void setJourneyStartDate(String journeyStartDate) {
        this.journeyStartDate = journeyStartDate;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }

    public int getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(int randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getIsSync() {
        return isSync;
    }

    public void setIsSync(String isSync) {
        this.isSync = isSync;
    }

    public String getLatestSynDate() {
        return latestSynDate;
    }

    public void setLatestSynDate(String latestSynDate) {
        this.latestSynDate = latestSynDate;
    }

    public String getJourneyPlanStatus() {
        return journeyPlanStatus;
    }

    public void setJourneyPlanStatus(String journeyPlanStatus) {
        this.journeyPlanStatus = journeyPlanStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
