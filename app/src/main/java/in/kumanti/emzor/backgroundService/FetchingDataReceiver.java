package in.kumanti.emzor.backgroundService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import in.kumanti.emzor.eloquent.MyDBHandler;

public class FetchingDataReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.codepath.example.servicesdemo.alarm";
    MyDBHandler dbHandler;
    Intent i;
    String login_id;

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context, "onReceive" , Toast.LENGTH_LONG).show();

        dbHandler = new MyDBHandler(context, null, null, 1);

        Bundle bundle = intent.getExtras();
        login_id = bundle.getString("login_id");

        //int line_count = dbHandler.get_count_master_download();
        //Toast.makeText(context,login_id,Toast.LENGTH_SHORT).show();
        int line_count = dbHandler.get_count_master_download();
        //Toast.makeText(context, Integer.toString(line_count), Toast.LENGTH_SHORT).show();

        if (line_count == 0) {

            i = new Intent(context, FetchingDataService.class);
            i.putExtra("login_id", login_id);
            FetchingDataService.enqueueJobAction(context, "action.SOME_ACTION", login_id);


            // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //context.startForegroundService(i);
            // } else {
            //   context.startService(i);
            // }

        } else {
            //context.stopService(i);
        }

    }
}
