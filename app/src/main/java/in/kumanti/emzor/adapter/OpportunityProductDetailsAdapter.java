package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.OpportunityProductDetails;

public class OpportunityProductDetailsAdapter extends RecyclerView.Adapter<OpportunityProductDetailsAdapter.ViewHolder> {

    public ArrayList<OpportunityProductDetails> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;

    public OpportunityProductDetailsAdapter(ArrayList<OpportunityProductDetails> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(OpportunityProductDetails item1, int position) {
        mDataset.add(position, item1);
        Log.d("Recycle", "Data set Add" + position + "--" + mDataset.size());
        notifyItemInserted(position);
    }

    public void remove(OpportunityProductDetails item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public OpportunityProductDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_opportunity_details, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.opportunityStatus.setOnCheckedChangeListener(null);
        //To Set the values to the Product Name
        if (viewHolder.productName != null && mDataset.get(position).getOpportunity_product_name() != null) {
            viewHolder.productName.setText(String.valueOf(mDataset.get(position).getOpportunity_product_name()));
        } else {
            viewHolder.productName.setText("");
        }

        //To Set the values to the Product Code
        if (viewHolder.productCode != null && mDataset.get(position).getOpportunity_product_code() != null) {
            viewHolder.productCode.setText(String.valueOf(mDataset.get(position).getOpportunity_product_code()));
        } else {
            viewHolder.productCode.setText("");
        }

        //To Set the values to the Product Quantity
        if (viewHolder.productUom != null && mDataset.get(position).getOpportunity_product_uom() != null) {
            viewHolder.productUom.setText(String.valueOf(mDataset.get(position).getOpportunity_product_uom()));
        } else {
            viewHolder.productUom.setText("");
        }

        //To Set the values to the Product Quantity
        if (viewHolder.productQuantity != null && mDataset.get(position).getOpportunity_product_quantity() != null) {
            viewHolder.productQuantity.setText(String.valueOf(mDataset.get(position).getOpportunity_product_quantity()));
        } else {
            viewHolder.productQuantity.setText("0");
        }

        //To Set the values to the Product Remarks
        if (viewHolder.productRemarks != null && mDataset.get(position).getOpportunity_product_remarks() != null) {
            viewHolder.productRemarks.setText(String.valueOf(mDataset.get(position).getOpportunity_product_remarks()));
        } else {
            viewHolder.productRemarks.setText("0");
        }

        //To Set the values to the OldOpportunity Status Checkbox
        if (viewHolder.opportunityStatus != null) {
            Log.d("BooleanCheck", "" + mDataset.get(position).getOpportunity_product_status());
            viewHolder.opportunityStatus.setChecked(mDataset.get(position).getOpportunity_product_status());
        }


        viewHolder.opportunityRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size()) {
                    mDataset.remove(position);
                    notifyDataSetChanged();
                    viewHolder.mListener.onClick(v, position);
                }
            }
        });
        //viewHolder.mListener.onClick(v,position);
        Log.d("Recycle", "" + mDataset.get(position).getOpportunity_product_status());


        //we need to update adapter once we finish with editing
        viewHolder.productQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    //if (!hasFocus) {
                    //final int position = v.getId();
                    final EditText Caption = (EditText) v;
                    mDataset.get(position).opportunity_product_quantity = Caption.getText().toString().isEmpty() ? null : Caption.getText().toString();
                }
            }
        });

        //we need to update adapter once we finish with editing
        viewHolder.productRemarks.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    //if (!hasFocus) {
                    //final int position = v.getId();
                    final EditText Caption = (EditText) v;
                    mDataset.get(position).opportunity_product_remarks = Caption.getText().toString();
                }
            }
        });

        //we need to update adapter once we finish with editing
        viewHolder.opportunityStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (position < mDataset.size()) {
                    mDataset.get(position).opportunity_product_status = isChecked;
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView productName, productCode, productUom;
        public CheckBox opportunityStatus;
        public EditText productQuantity, productRemarks;
        public ImageButton opportunityRemoveButton;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {

            super(v);
            mListener = listener;
            productName = v.findViewById(R.id.opportunityProductNameTextView);
            productCode = v.findViewById(R.id.opportunityProductCodeTextView);
            productUom = v.findViewById(R.id.opportunityProductUomTextView);
            productQuantity = v.findViewById(R.id.opportunityProductQuantityEditText);
            productRemarks = v.findViewById(R.id.opportunityProductRemarksEditText);
            opportunityStatus = v.findViewById(R.id.opportunityStatusCheckbox);
            opportunityRemoveButton = v.findViewById(R.id.opportunityDetailsRemoveButton);
        }
    }


}