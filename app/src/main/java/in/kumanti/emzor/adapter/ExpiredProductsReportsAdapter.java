package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.ExpiredProductsReport;

public class ExpiredProductsReportsAdapter extends RecyclerView.Adapter<ExpiredProductsReportsAdapter.ViewHolder> {

    public ArrayList<ExpiredProductsReport> mDataset;
    public Context context;

    public ExpiredProductsReportsAdapter(ArrayList<ExpiredProductsReport> myDataset) {
        mDataset = myDataset;
    }

    public void add(ExpiredProductsReport item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(ExpiredProductsReport item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public ExpiredProductsReportsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_expired_products_reports, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.batchName.setText(mDataset.get(position).getBatchName());
        viewHolder.expiredBatchDate.setText(mDataset.get(position).getExpiredBatchDate());
        viewHolder.expiredQty.setText(mDataset.get(position).getExpiredQty());
        viewHolder.quarantineDate.setText(mDataset.get(position).getQuarantineDate());
        viewHolder.quarantineQty.setText(mDataset.get(position).getQuarantineQty());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName, batchName, expiredBatchDate, expiredQty, quarantineDate, quarantineQty;

        public ViewHolder(View v) {
            super(v);

            productName = v.findViewById(R.id.expProdNameTv);
            batchName = v.findViewById(R.id.expBatchNameTv);
            expiredBatchDate = v.findViewById(R.id.expBatchDateTv);
            expiredQty = v.findViewById(R.id.expQuantityTv);
            quarantineDate = v.findViewById(R.id.expQuarantineDateTv);
            quarantineQty = v.findViewById(R.id.expQuarantineQtyTv);
        }
    }


}