package in.kumanti.emzor.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.ViewStockMyStockProduct;

public class StockViewMyStockAdapter extends RecyclerView.Adapter<StockViewMyStockAdapter.ViewHolder> {

    public ArrayList<ViewStockMyStockProduct> mDataset;
    public Context context;
    DecimalFormat decimalFormat, quantitydecimalFormat;
    double quantityDouble, priceDouble, totalPriceDouble;
    String formatQuantity, formatPrice, formatTotalPrice;
    private RecyclerViewItemListener mListener;

    public StockViewMyStockAdapter(ArrayList<ViewStockMyStockProduct> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(ViewStockMyStockProduct item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(ViewStockMyStockProduct item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockViewMyStockAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_view_my_stock, parent, false);

        decimalFormat = new DecimalFormat("#,###.00");
        quantitydecimalFormat = new DecimalFormat("#,###");


        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.productUom.setText(mDataset.get(position).getProductUom());
        viewHolder.productInvoiceCount.setText(mDataset.get(position).getSecondaryInvoiceCount());


        String expiredCount = mDataset.get(position).getExpiredCount();

        if (!TextUtils.isEmpty(expiredCount) && Integer.parseInt(expiredCount) != 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                viewHolder.productName.setBackground(context.getDrawable(R.drawable.expiry_products_border));
            }
        }


        if (!TextUtils.isEmpty(mDataset.get(position).getProductQuantity())) {
            quantityDouble = Double.parseDouble(mDataset.get(position).getProductQuantity());
            formatQuantity = quantitydecimalFormat.format(quantityDouble);
            String value = formatQuantity;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.productQuantity.setText(value);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getProductPrice())) {
            priceDouble = Double.parseDouble(mDataset.get(position).getProductPrice());
            formatPrice = decimalFormat.format(priceDouble);
            String value = formatPrice;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.productPrice.setText(value);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getProductTotalPrice())) {
            totalPriceDouble = Double.parseDouble(mDataset.get(position).getProductTotalPrice());
            formatTotalPrice = decimalFormat.format(totalPriceDouble);
            String value = formatTotalPrice;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.productTotalPrice.setText(value);
        }

        viewHolder.productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size() && mDataset.get(position).getBatchControlled().equals("Yes")) {

                    Log.d("DataCheck", "CurrentPosition onCLick " + position);
                    viewHolder.mListener.onClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName, productUom, productQuantity, productPrice, productTotalPrice, productInvoiceCount;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            productName = v.findViewById(R.id.myStockProductNameTextView);
            productUom = v.findViewById(R.id.myStockProductUomTextView);
            productQuantity = v.findViewById(R.id.myStockProductQuantityTextView);
            productPrice = v.findViewById(R.id.myStockProductPriceTextView);
            productTotalPrice = v.findViewById(R.id.myStockProductTotalPriceTextView);
            productInvoiceCount = v.findViewById(R.id.myStockInvoiceCountTextView);
        }
    }


}