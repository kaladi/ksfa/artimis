package in.kumanti.emzor.model;

public class StockReturn {


    public boolean isSync;
    public String visitSeqNumber;
    private String tabCode;
    private String companyCode;
    private String srCode;
    private int stockReturnId;
    private String stockReturnNumber;
    private String stockReturnDate;
    private String stockReturnType;
    private String signature;
    private String signeeName;
    private String emailSent;
    private String smsSent;
    private String printDone;
    private int randomNum;
    private String createdAt;
    private String updatedAt;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public int getStockReturnId() {
        return stockReturnId;
    }

    public void setStockReturnId(int stockReturnId) {
        this.stockReturnId = stockReturnId;
    }

    public String getStockReturnNumber() {
        return stockReturnNumber;
    }

    public void setStockReturnNumber(String stockReturnNumber) {
        this.stockReturnNumber = stockReturnNumber;
    }

    public String getStockReturnDate() {
        return stockReturnDate;
    }

    public void setStockReturnDate(String stockReturnDate) {
        this.stockReturnDate = stockReturnDate;
    }

    public String getStockReturnType() {
        return stockReturnType;
    }

    public void setStockReturnType(String stockReturnType) {
        this.stockReturnType = stockReturnType;
    }

    public String getSigneeName() {
        return signeeName;
    }

    public void setSigneeName(String signeeName) {
        this.signeeName = signeeName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(String emailSent) {
        this.emailSent = emailSent;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    public String getPrintDone() {
        return printDone;
    }

    public void setPrintDone(String printDone) {
        this.printDone = printDone;
    }

    public int getRandomNum() {
        return randomNum;
    }

    public void setRandomNum(int randomNum) {
        this.randomNum = randomNum;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getVisitSeqNumber() {
        return visitSeqNumber;
    }

    public void setVisitSeqNumber(String visitSeqNumber) {
        this.visitSeqNumber = visitSeqNumber;
    }
}
