package in.kumanti.emzor.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CrmOpportunityProducts;


public class CrmOppProductsAdapter extends RecyclerView.Adapter<CrmOppProductsAdapter.ViewHolder> {

    public ArrayList<CrmOpportunityProducts> mDataset;
    public Context context;
    DecimalFormat decimalFormat;
    private RecyclerViewItemListener mListener;


    public CrmOppProductsAdapter(ArrayList<CrmOpportunityProducts> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(CrmOpportunityProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmOpportunityProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public CrmOppProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_crm_opportunity_products, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {


        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.uom.setText(mDataset.get(position).getProductUom());
        viewHolder.unitPrice.setText(mDataset.get(position).getProductUnitPrice());

        viewHolder.quantity.setText(mDataset.get(position).getProductQuantity());


        //To Set Error message
        if (!TextUtils.isEmpty(mDataset.get(position).getErrorProductName())) {
            viewHolder.productName.setError(mDataset.get(position).getErrorProductName());
        } else {
            viewHolder.productName.setError(null);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getErrorProductQty())) {
            viewHolder.quantity.setError(mDataset.get(position).getErrorProductQty());
        } else {
            viewHolder.quantity.setError(null);
        }


        //To Set the values to the Availabilty Checkbox
        viewHolder.quantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setProductQuantity(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });


        viewHolder.removeRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Prescribed Product Info?");
                        //alertDialog.setMessage("your message ");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                mListener.onClick(v, position);
                                notifyDataSetChanged();
                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName, uom, unitPrice;
        public EditText quantity;
        public ImageButton catalogue, removeRow;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;

            productName = v.findViewById(R.id.crmOppProductNameTv);
            uom = v.findViewById(R.id.crmOppProductUomTv);
            unitPrice = v.findViewById(R.id.crmOppProductPriceTv);
            quantity = v.findViewById(R.id.crmOppProductQtyTv);
            catalogue = v.findViewById(R.id.crmOppProductCatalogueImageButton);
            removeRow = v.findViewById(R.id.crmOppProductDeleteButton);
        }


    }


}