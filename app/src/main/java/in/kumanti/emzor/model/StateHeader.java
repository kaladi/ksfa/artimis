package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class StateHeader {

    public String state;
    public String state_id;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    @Override
    public String toString() {
        return this.state;
    }
}

