package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class MarqueeText {

    private String message_name;
    private String serail;
    private String insert_at;


    public String getSerail() {
        return serail;
    }

    public void setSerail(String serail) {
        this.serail = serail;
    }

    public String getCreated_date() {
        return insert_at;
    }

    public void setCreated_date(String created_date) {
        this.insert_at = created_date;
    }


    public String getMsgContent() {
        return message_name;
    }

    public void setMsgContent(String msgContent) {
        this.message_name = msgContent;
    }
}

