package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_LPO_IMAGES;

public class LpoImagesTable extends SQLiteOpenHelper {

    private static final String COLUMN_LPO_IMAGE_ID = "lpoImageId";
    private static final String COLUMN_ORDER_CODE = "orderCode";
    private static final String COLUMN_LPO_IMAGE_NAME = "lpoImageName";
    private static final String COLUMN_LPO_IMAGE_PATH = "lpoImagePath";
    private static final String COLUMN_LPO_IMAGE_TYPE = "imageType";
    private static final String COLUMN_LPO_IMAGE_CREATED_AT = "createdAt";
    private static final String COLUMN_LPO_IMAGE_UPDATED_AT = "updatedAt";
    private static final String COLUMN_LPO_IMAGE_UPLOADED = "isUploaded";
    private final static String COLUMN_LPO_IMAGE_SEQUENCE_NO = "visitSeqNo";

    public Context context;
    String selectQuery;
    Cursor cursor;
//    private SQLiteDatabase sqlWrite;

    public LpoImagesTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_LPO_IMAGES);

        String distributorsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_LPO_IMAGES + "(" +
                COLUMN_LPO_IMAGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ORDER_CODE + " TEXT, " +
                COLUMN_LPO_IMAGE_NAME + " TEXT, " +
                COLUMN_LPO_IMAGE_PATH + " TEXT, " +
                COLUMN_LPO_IMAGE_TYPE + " TEXT, " +
                COLUMN_LPO_IMAGE_UPLOADED + " INT DEFAULT 0, " +
                COLUMN_LPO_IMAGE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_LPO_IMAGE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_LPO_IMAGE_SEQUENCE_NO + " TEXT " +
                ");";
        sqlWrite.execSQL(distributorsTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(LPOImages lpoImages) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ORDER_CODE, lpoImages.getOrderCode());
        values.put(COLUMN_LPO_IMAGE_NAME, lpoImages.getLpoImageName());
        values.put(COLUMN_LPO_IMAGE_PATH, lpoImages.getLpoImagePath());
        values.put(COLUMN_LPO_IMAGE_TYPE, lpoImages.getImageType());
        values.put(COLUMN_LPO_IMAGE_SEQUENCE_NO, lpoImages.getVisitSeqNumber());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_LPO_IMAGES, null, values);
        db.close();
        return id;
    }

    public int get_lpo_gallery_image_id() {
        int cig_image_id = 0;
        String selectQuery = "select * from " + TABLE_LPO_IMAGES + " order by " + COLUMN_LPO_IMAGE_ID + " desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            cig_image_id = cursor.getInt(0);
            cursor.close();
        }

        return cig_image_id;
    }

    public void updatelpoGalleryUploadFlag(String lpoId) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update " + TABLE_LPO_IMAGES + " set " + COLUMN_LPO_IMAGE_UPLOADED + "='1' where " + COLUMN_LPO_IMAGE_ID + "= " + lpoId);
        db.close();

    }

        public ArrayList<LPOImages> getLpoImageService() {
        Cursor data = null;
        SQLiteDatabase db = this.getWritableDatabase();
        try  {

            data = db.rawQuery("select * from " + TABLE_LPO_IMAGES + " where " + COLUMN_LPO_IMAGE_UPLOADED + "=0 and " + COLUMN_LPO_IMAGE_PATH + " NOT NULL", null);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor data = db.rawQuery("select * from " + TABLE_LPO_IMAGES + " where " + COLUMN_LPO_IMAGE_UPLOADED + "=0 and " + COLUMN_LPO_IMAGE_PATH + " NOT NULL", null);


        ArrayList<LPOImages> lpoImagesArrayList = new ArrayList<>();

        while (data.moveToNext()) {
            LPOImages lpoImage = new LPOImages();
            lpoImage.setImageId(data.getInt(0));
            lpoImage.setOrderCode(data.getString(1));
            lpoImage.setLpoImageName(data.getString(2));
            lpoImage.setLpoImagePath(data.getString(3));
            lpoImagesArrayList.add(lpoImage);
        }

        data.close();
        return lpoImagesArrayList;
    }

    /**
     * Fetching the new customer details  from customer details table and returning the result as a ArrayList
     */
//    public ArrayList<LPOImages> getLpoImageService() {
//        Cursor data;
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        data = db.rawQuery("select * from " + TABLE_LPO_IMAGES + " where " + COLUMN_LPO_IMAGE_UPLOADED + "=0 and " + COLUMN_LPO_IMAGE_PATH + " NOT NULL", null);
//
//        ArrayList<LPOImages> lpoImagesArrayList = new ArrayList<>();
//
//        while (data.moveToNext()) {
//            LPOImages lpoImage = new LPOImages();
//            lpoImage.setImageId(data.getInt(0));
//            lpoImage.setOrderCode(data.getString(1));
//            lpoImage.setLpoImageName(data.getString(2));
//            lpoImage.setLpoImagePath(data.getString(3));
//            lpoImagesArrayList.add(lpoImage);
//        }
//        data.close();
//        db.close();
//        return lpoImagesArrayList;
//    }

    //Get Customer Details
    public Cursor getOrderImages(String bde_code, String order_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;
        data = db.rawQuery("select lpoImageId,orderCode,lpoImageName,lpoImagePath,imageType,createdAt,updatedAt,isUploaded,visitSeqNo from " + TABLE_LPO_IMAGES + " where " + COLUMN_ORDER_CODE + "= '" + order_code + "'", null);

        return data;
    }

    public int get_lpo_order_image_count(String order_id) {
        String selectQuery = "select " + COLUMN_ORDER_CODE + " from " + TABLE_LPO_IMAGES + " where " + COLUMN_ORDER_CODE + "='" + order_id + "'";
        SQLiteDatabase database = this.getReadableDatabase();

        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public ArrayList<LPOImages> getLPOImageDetails() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_LPO_IMAGES + " WHERE DATE(datetime(createdAt,'unixepoch')) >= date('now','-5 Days')";

        ArrayList<LPOImages> lpoImagesArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            LPOImages lpoImages = new LPOImages();
            lpoImages.setTabCode(Constants.TAB_CODE);
            lpoImages.setCompanyCode(Constants.COMPANY_CODE);
            lpoImages.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            lpoImages.setImageId(id);
            lpoImages.setOrderCode(cursor.getString(1));
            lpoImages.setLpoImageName(cursor.getString(2));
            lpoImages.setLpoImagePath(cursor.getString(3));
            lpoImages.setIsUploaded(cursor.getString(4));
            lpoImages.setCreatedAt(cursor.getString(5));
            lpoImages.setUpdatedAt(cursor.getString(6));
            lpoImages.setVisitSeqNumber(cursor.getString(7));


            lpoImagesArrayList.add(lpoImages);
        }
        cursor.close();
        sqlWrite.close();
        return lpoImagesArrayList;
    }



    public  void deleteCancelledImages (String orderCode) {

        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        sqlWrite.delete(TABLE_LPO_IMAGES, COLUMN_ORDER_CODE + "=?", new String[]{orderCode});
        sqlWrite.close();
    }
}
