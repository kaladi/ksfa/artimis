package in.kumanti.emzor.model;

public class StockViewInvoiceList {
    private String expiry_date;
    private String batch_number;
    private String stock_quantity;
    private String isExpired;

    public StockViewInvoiceList(String exdate, String bnum, String sqty) {
        expiry_date = exdate;
        batch_number = bnum;
        stock_quantity = sqty;
        isExpired = "FALSE";
    }

    public StockViewInvoiceList(String exdate, String bnum, String sqty, String isExpiry) {
        expiry_date = exdate;
        batch_number = bnum;
        stock_quantity = sqty;
        isExpired = isExpiry;
    }


    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String exdate) {
        expiry_date = exdate;
    }

    public String getBatch_number() {
        return batch_number;
    }

    public void setBatch_number(String bnum) {
        batch_number = bnum;
    }

    public String getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(String sqty) {
        stock_quantity = sqty;
    }

    public String getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(String isExpired) {
        this.isExpired = isExpired;
    }

    @Override
    public String toString() {
        return this.batch_number;
    }
}
