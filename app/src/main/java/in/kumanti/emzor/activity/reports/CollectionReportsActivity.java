package in.kumanti.emzor.activity.reports;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CollectionReports;


public class CollectionReportsActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    TextView actionBarTitle, unappliedReceipts, appliedReceipts;
    String login_id, marqueeTextString;
    ArrayList<CollectionReports> collectionReports;
    ArrayList<CollectionReports> secondarycollectionReports;
    CollectionReports collectionDetailsLists1;
    CollectionReports secondarycollectionDetailsLists1;
    CollectionDetailsListAdapter adapter = null;
    CollectionDetailsListAdapter adapter1 = null;
    RadioButton primary, secondary;
    int currentCheckedRadio;
    MyDBHandler dbHandler;
    TextView transactionTypeHeaderTV;
    DecimalFormat formatter, formatter1;
    String format_value, format_invoice_amount, format_due_amount;
    private RecyclerView recyclerView;
    private TextView marqueeText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_collection_detail);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();


        actionBarTitle.setText("Collection Report");

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);


        formatter = new DecimalFormat("#,###.00");


        primary.setOnCheckedChangeListener(new Radio_check());
        secondary.setOnCheckedChangeListener(new Radio_check());
        currentCheckedRadio = R.id.primary;

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        //transactionTypeHeaderTV.setVisibility(View.GONE);
        primary_collection_data();

        double applied_val = dbHandler.get_applied_value("Primary");
        double unapplied_val = dbHandler.get_unapplied_value("Primary");

        appliedReceipts.setText(formatter.format(applied_val));
        unappliedReceipts.setText(formatter.format(unapplied_val));


    }

    private void primary_collection_data() {

        collectionReports = new ArrayList<CollectionReports>();
        System.out.println("AAA3Collection::login_id = " + login_id);
        Cursor data = dbHandler.getPrimaryCollectionReportDetails(login_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                String value = data.getString(3);
                String invoice_amt = data.getString(5);
                String due_amount = data.getString(6);

                if (value == null) {

                } else {
                    double total_value = Double.parseDouble(value);
                    format_value = formatter.format(total_value);
                }

                if (invoice_amt == null) {

                } else {
                    double invoice_value = Double.parseDouble(invoice_amt);
                    format_invoice_amount = formatter.format(invoice_value);
                }

                if (due_amount == null) {

                } else {
                    double due_value = Double.parseDouble(due_amount);
                    format_due_amount = formatter.format(due_value);
                }


                String inputDateStr = data.getString(0);

                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                Date date = null;
                try {
                    date = inputFormat.parse(inputDateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputDateStr = outputFormat.format(date);

                collectionDetailsLists1 = new CollectionReports(outputDateStr, data.getString(1), data.getString(2), format_value, data.getString(4), format_invoice_amount, format_due_amount, data.getString(7), data.getString(8), data.getString(9), "",data.getString(11));
                collectionReports.add(i, collectionDetailsLists1);
                i++;
            }
        }
        // transactionTypeHeaderTV.setVisibility(View.GONE);
        adapter = new CollectionDetailsListAdapter(collectionReports, "Primary");
        recyclerView.setAdapter(adapter);
    }

    private void secondary_collection_data() {
        if (collectionReports == null) {

        } else {
            collectionReports.clear();

        }
        secondarycollectionReports = new ArrayList<CollectionReports>();
        Cursor data = dbHandler.getSecondaryCollectionReportDetails(login_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                String value = data.getString(3);
                String invoice_amt = data.getString(5);
                String due_amount = data.getString(6);

                if (value == null) {

                } else {
                    double total_value = Double.parseDouble(value);
                    format_value = formatter.format(total_value);
                }

                if (invoice_amt == null) {

                } else {
                    double invoice_value = Double.parseDouble(invoice_amt);
                    format_invoice_amount = formatter.format(invoice_value);
                }

                if (due_amount == null) {

                } else {
                    double due_value = Double.parseDouble(due_amount);
                    format_due_amount = formatter.format(due_value);
                }


                String inputDateStr = data.getString(0);

                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                Date date = null;
                try {
                    date = inputFormat.parse(inputDateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputDateStr = outputFormat.format(date);
                secondarycollectionDetailsLists1 = new CollectionReports(outputDateStr, data.getString(1), data.getString(2), format_value, data.getString(4), format_invoice_amount, format_due_amount, data.getString(7), data.getString(8), data.getString(9), data.getString(11),data.getString(12));
                secondarycollectionReports.add(i, secondarycollectionDetailsLists1);
                i++;
            }
            //  transactionTypeHeaderTV.setVisibility(View.VISIBLE);
        }
        adapter1 = new CollectionDetailsListAdapter(secondarycollectionReports, "Secondary");
        recyclerView.setAdapter(adapter1);
    }

    private void initializeViews() {

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        recyclerView = findViewById(R.id.order_list);
        primary = findViewById(R.id.primary);
        secondary = findViewById(R.id.secondary);
        appliedReceipts = findViewById(R.id.appliedReceipts);
        unappliedReceipts = findViewById(R.id.unappliedReceipts);
        transactionTypeHeaderTV = findViewById(R.id.transactionTypeHeaderTV);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        login_id = bundle.getString("login_id");

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }

    class Radio_check implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (primary.isChecked()) {
                //transactionTypeHeaderTV.setVisibility(View.GONE);
                primary_collection_data();
                double applied_val = dbHandler.get_applied_value("Primary");
                double unapplied_val = dbHandler.get_unapplied_value("Primary");

                appliedReceipts.setText(formatter.format(applied_val));
                unappliedReceipts.setText(formatter.format(unapplied_val));

            } else if (secondary.isChecked()) {
                secondary_collection_data();
                double applied_val = dbHandler.get_applied_value("Secondary");
                double unapplied_val = dbHandler.get_unapplied_value("Secondary");

                appliedReceipts.setText(formatter.format(applied_val));
                unappliedReceipts.setText(formatter.format(unapplied_val));

            }

        }
    }

    public class CollectionDetailsListAdapter extends RecyclerView.Adapter<CollectionDetailsListAdapter.MyViewHolder> {

        private ArrayList<CollectionReports> users;
        private String collectionType;

        public CollectionDetailsListAdapter(ArrayList<CollectionReports> users, String collectionType) {
            this.users = users;
            this.collectionType = collectionType;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_collection_details, parent, false);

            return new MyViewHolder(itemView, collectionType);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            final CollectionReports movie = users.get(position);

            holder.date.setText(movie.getDate());
            holder.customer_name.setText(movie.getCustomer_name());
            holder.receipt_num.setText(movie.getReceipt_num());
            holder.value.setText(movie.getValue());
            holder.invoice_num.setText(movie.getInvoice_num());
            holder.invoice_amount.setText(movie.getInvoice_amount());
            holder.due_amount.setText(movie.getDue_amount());
            holder.mode.setText(movie.getMode());
            holder.bankAndBranch.setText(movie.getBank());
//            System.out.println("movie.getStatus() = " + movie.getStatus());
            if (movie.getStatus() != null )
                holder.status.setText(movie.getStatus());
            else
                holder.status.setText("Pending");


            holder.transactionType.setText(movie.getTransactionType());
            if (this.collectionType.equals("Primary"))
                holder.transactionType.setText("Collection");
            //holder.transactionType.setVisibility(View.GONE);

        }

        @Override
        public int getItemCount() {
            return users.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public TextView date, customer_name, receipt_num, value, invoice_num, invoice_amount, due_amount, mode, bankAndBranch, transactionType,status;
            public String collectionType;

            public MyViewHolder(View view, String collectionType) {
                super(view);

                date = view.findViewById(R.id.date);
                customer_name = view.findViewById(R.id.customer_name);
                receipt_num = view.findViewById(R.id.receipt_num);
                value = view.findViewById(R.id.value);
                invoice_num = view.findViewById(R.id.invoice_num);
                invoice_amount = view.findViewById(R.id.invoice_amount);
                due_amount = view.findViewById(R.id.due_amount);
                mode = view.findViewById(R.id.mode);
                status = view.findViewById(R.id.status);
                bankAndBranch = view.findViewById(R.id.bankAndBranch);
                transactionType = view.findViewById(R.id.collectionReportsTransType);
                // branch = view.findViewById(R.id.branch);
                this.collectionType = collectionType;
            }
        }

    }

}
