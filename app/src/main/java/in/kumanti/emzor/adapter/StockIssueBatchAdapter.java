package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockIssueBatch;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class StockIssueBatchAdapter extends RecyclerView.Adapter<StockIssueBatchAdapter.ViewHolder> {

    public ArrayList<StockIssueBatch> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;
    private int mYear, mMonth, mDay;

    public StockIssueBatchAdapter(ArrayList<StockIssueBatch> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(StockIssueBatch item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockIssueBatch item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockIssueBatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_issue_batch, parent, false);


        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getExpiryDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.expiryDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }

        //viewHolder.expiryDate.setText(mDataset.get(position).getExpiryDate());
        viewHolder.batchNumber.setText(mDataset.get(position).getBatchNumber());
        viewHolder.batchQuantity.setText(String.valueOf(Integer.parseInt(mDataset.get(position).getBatchQuantity())));
        viewHolder.issueQuantity.setText(mDataset.get(position).getIssueQuantity());
        //To Set Error message
        viewHolder.issueQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, mDataset.get(position).getBatchQuantity())});
        if (!TextUtils.isEmpty(mDataset.get(position).getErrorIssueQuantity())) {
            viewHolder.issueQuantity.setError(mDataset.get(position).getErrorIssueQuantity());
        } else {
            viewHolder.issueQuantity.setError(null);
        }


        viewHolder.issueQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setIssueQuantity(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView expiryDate, batchNumber, batchQuantity;
        public EditText issueQuantity;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;

            expiryDate = v.findViewById(R.id.stockIssueExpiryDateTv);
            batchNumber = v.findViewById(R.id.stockIssueBatchNameTv);
            batchQuantity = v.findViewById(R.id.stockIssueBatchStockQtyTv);
            issueQuantity = v.findViewById(R.id.stockIssueBatchIssueQtyTv);

            issueQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
        }
    }


}