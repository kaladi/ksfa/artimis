package in.kumanti.emzor.model;

public class CompetitorStockProduct {

    public int id;
    public String companyCode;
    public String tabCode;
    public String srCode;
    public String  productCode;
    public String  stockId;
    public String  productName;
    public boolean productAvailability;
    public Integer productVolume;
    public Double productPrice;
    public String promotionText = "";

    public String error_competitor_product_volume;
    public String error_competitor_product_price;
    public String error_competitor_product_promotion;
    public String createdAt;
    public String updatedAt;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }



    public String getStockVolume() {
        return stockVolume;
    }

    public void setStockVolume(String stockVolume) {
        this.stockVolume = stockVolume;
    }

    public String stockVolume;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_id() {
        return productCode;
    }

    public void setProduct_id(String product_id) {
        this.productCode = product_id;
    }

    public String getCompetitor_stock_id() {
        return stockId;
    }

    public void setCompetitor_stock_id(String competitor_stock_id) {
        this.stockId = competitor_stock_id;
    }

    public String getCompetitor_product_id() {
        return productName;
    }

    public void setCompetitor_product_id(String competitor_product_id) {
        this.productName = competitor_product_id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public boolean getCompetitor_product_availability() {
        return productAvailability;
    }

    public void setCompetitor_product_availability(boolean competitor_product_availability) {
        this.productAvailability = competitor_product_availability;
    }

    public Integer getCompetitor_product_volume() {
        return productVolume;
    }

    public void setCompetitor_product_volume(Integer competitor_product_volume) {
        this.productVolume = competitor_product_volume;
    }

    public Double getCompetitor_product_price() {
        return productPrice;
    }

    public void setCompetitor_product_price(Double competitor_product_price) {
        this.productPrice = competitor_product_price;
    }

    public String getCompetitor_product_promotion() {
        return promotionText;
    }

    public void setCompetitor_product_promotion(String competitor_product_promotion) {
        this.promotionText = competitor_product_promotion;
    }

    public String getError_competitor_product_price() {
        return error_competitor_product_price;
    }

    public void setError_competitor_product_price(String error_competitor_product_price) {
        this.error_competitor_product_price = error_competitor_product_price;
    }

    public String getError_competitor_product_promotion() {
        return error_competitor_product_promotion;
    }

    public void setError_competitor_product_promotion(String error_competitor_product_promotion) {
        this.error_competitor_product_promotion = error_competitor_product_promotion;
    }

    public String getError_competitor_product_volume() {
        return error_competitor_product_volume;
    }

    public void setError_competitor_product_volume(String error_competitor_product_volume) {
        this.error_competitor_product_volume = error_competitor_product_volume;
    }

}
