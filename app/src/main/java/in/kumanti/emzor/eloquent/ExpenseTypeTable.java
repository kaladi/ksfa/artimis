package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;

import in.kumanti.emzor.model.Age;
import in.kumanti.emzor.model.CustomerDetails;
import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.model.ExpenseType;
import in.kumanti.emzor.model.TripDetails;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_AGE;
import static in.kumanti.emzor.utils.Constants.TABLE_EXPENSE;
import static in.kumanti.emzor.utils.Constants.TABLE_EXPENSE_TYPE;
import static in.kumanti.emzor.utils.Constants.TABLE_NEW_JOURNEY_PLAN;

public class ExpenseTypeTable extends SQLiteOpenHelper {

    private static final String COLUMN_EXPENSE_TYPE_ID = "expenseTypeId";
    private static final String COLUMN_EXPENSE_TYPE_CODE = "code";
    private static final String COLUMN_EXPENSE_TYPE_VALUE = "value";
    private static final String COLUMN_EXPENSE_TYPE_CREATED_AT = "createdAt";
    private static final String COLUMN_EXPENSE_TYPE_UPDATED_AT = "updatedAt";

    public Context context;
    String selectQuery;
    Cursor cursor;

    public ExpenseTypeTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSE);

        String expenseTable = "CREATE TABLE IF NOT EXISTS " + TABLE_EXPENSE_TYPE + "(" +
                COLUMN_EXPENSE_TYPE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_EXPENSE_TYPE_CODE + " TEXT, " +
                COLUMN_EXPENSE_TYPE_VALUE + " TEXT, " +
                COLUMN_EXPENSE_TYPE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_EXPENSE_TYPE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(expenseTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public void insertingExpenseTypeMaster(ArrayList<ExpenseType> expenseTypeList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (expenseTypeList != null) {
                for (int i = 0; i < expenseTypeList.size(); i++) {

                    values.put(COLUMN_EXPENSE_TYPE_CODE, expenseTypeList.get(i).getCode());
                    values.put(COLUMN_EXPENSE_TYPE_VALUE, expenseTypeList.get(i).getValue());
                    db.insert(TABLE_EXPENSE_TYPE, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }

    public ArrayList<ExpenseType> getExpenseType() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select expenseTypeId,code,value from xxmsales_expense_type "
                , null);
        ArrayList<ExpenseType> customerDetailsArrayList = new ArrayList<ExpenseType>();
        while (data.moveToNext()) {
            ExpenseType expenseType = new ExpenseType();

            expenseType.setCode(data.getString(1));
            expenseType.setValue(data.getString(2));

            customerDetailsArrayList.add(expenseType);
        }
        return customerDetailsArrayList;
    }

    public void deleteExpenseType() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EXPENSE_TYPE, null, null);
        db.close();
    }

}
