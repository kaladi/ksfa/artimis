package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CrmPriceList;

public class CrmPriceListAdapter extends RecyclerView.Adapter<CrmPriceListAdapter.ViewHolder> {

    public ArrayList<CrmPriceList> mDataset;
    public Context context;

    public CrmPriceListAdapter(ArrayList<CrmPriceList> myDataset) {
        mDataset = myDataset;
    }

    public void add(CrmPriceList item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmPriceList item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public CrmPriceListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_price_list, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.uom.setText(mDataset.get(position).getProductUom());
        viewHolder.unitPrice.setText(mDataset.get(position).getUnitPrice());
        viewHolder.effectiveDate.setText(mDataset.get(position).getEffectiveDate());
        viewHolder.endDate.setText(mDataset.get(position).getEndDate());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName, uom, unitPrice, effectiveDate, endDate;

        public ViewHolder(View v) {
            super(v);

            productName = v.findViewById(R.id.feedbackDateTextView);
            uom = v.findViewById(R.id.feedbackTypeTextView);
            unitPrice = v.findViewById(R.id.customerNameTextView);
            effectiveDate = v.findViewById(R.id.customerFeedbackTextView);
            endDate = v.findViewById(R.id.actionTakenTextView);
        }
    }


}