package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CrmKeyPersonsAlert;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_KEY_PERSONS_ALERT;

public class CrmKeyPersonsAlertTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_KEY_PERSON_ALERT_ID = "alertId";
    private static final String COLUMN_KEY_PERSON_ID = "keyPersonId";
    private static final String COLUMN_KEY_PERSON_ALERT_TYPE = "alertType";
    private static final String COLUMN_KEY_PERSON_ALERT_SMS = "sms";
    private static final String COLUMN_KEY_PERSON_ALERT_EMAIL = "email";
    private static final String COLUMN_KEY_PERSON_ALERT_IS_SYNC = "isSync";
    private static final String COLUMN_KEY_PERSON_ALERT_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_KEY_PERSON_ALERT_CREATED_AT = "createdAt";
    private static final String COLUMN_KEY_PERSON_ALERT_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmKeyPersonsAlertTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_KEY_PERSONS_ALERT);
        String keyPersonAlertTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_KEY_PERSONS_ALERT + "(" +
                COLUMN_KEY_PERSON_ALERT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_KEY_PERSON_ID + " INTEGER, " +
                COLUMN_KEY_PERSON_ALERT_TYPE + " TEXT, " +
                COLUMN_KEY_PERSON_ALERT_SMS + " TEXT, " +
                COLUMN_KEY_PERSON_ALERT_EMAIL + " TEXT, " +
                COLUMN_KEY_PERSON_ALERT_IS_SYNC + " TEXT, " +
                COLUMN_KEY_PERSON_ALERT_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_KEY_PERSON_ALERT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_KEY_PERSON_ALERT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(keyPersonAlertTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmKeyPersonsAlert keyPersonsAlert) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_KEY_PERSON_ID, keyPersonsAlert.getKeyPersonId());
        values.put(COLUMN_KEY_PERSON_ALERT_TYPE, keyPersonsAlert.getAlertType());
        values.put(COLUMN_KEY_PERSON_ALERT_SMS, keyPersonsAlert.getSms());
        values.put(COLUMN_KEY_PERSON_ALERT_EMAIL, keyPersonsAlert.getEmail());
        values.put(COLUMN_KEY_PERSON_ALERT_IS_SYNC, keyPersonsAlert.isSync());
        values.put(COLUMN_KEY_PERSON_ALERT_LATEST_SYNC_DATE, keyPersonsAlert.getLatestSyncDate());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_KEY_PERSONS_ALERT, null, values);
        db.close();
        return id;
    }



 /*   public ArrayList<CrmKeyPersonsAlert> getColdCallList() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT productCode FROM " + TABLE_CRM_KEY_PERSONS_ALERT ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<CrmKeyPersonsAlert> keyPersonsAlertArrayList = new ArrayList<CrmKeyPersonsAlert>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            CrmKeyPersonsAlert cd = new CrmKeyPersonsAlert();
            cd.s(cursor.getInt(0));
            cd.setCustomerCode(cursor.getString(1));
            cd.setColdCallDate(cursor.getString(2));
            cd.setPersonsMet1(cursor.getString(3));
            cd.setPersonsMet2(cursor.getString(4));
            cd.setPersonsMet3(cursor.getString(5));
            cd.setSubject(cursor.getString(6));
            cd.setOutcomeType(cursor.getString(7));
            cd.setOutcomeDescription(cursor.getString(8));
            cd.setVisitDate(cursor.getString(9));
            cd.setVisitTime(cursor.getString(10));
            cd.setVisitAddress(cursor.getString(11));

            keyPersonsAlertArrayList.add(i,cd);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return keyPersonsAlertArrayList;
    }*/


}
