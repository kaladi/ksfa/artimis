package in.kumanti.emzor.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockReturnProducts;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class StockReturnAdapter extends RecyclerView.Adapter<StockReturnAdapter.ViewHolder> {

    public ArrayList<StockReturnProducts> mDataset;
    public Context context;
    public String quantity;
    DecimalFormat decimalFormat;
    ArrayList<StockReturnProducts> stockReturnProductsArrayList;
    private RecyclerViewItemListener mListener;
    private boolean isStockReturnSaved;


    public StockReturnAdapter(ArrayList<StockReturnProducts> myDataset, RecyclerViewItemListener listener, boolean stockReturnSavedVal) {
        mDataset = myDataset;
        mListener = listener;
        isStockReturnSaved = stockReturnSavedVal;
    }

    public void add(StockReturnProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockReturnProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockReturnAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_return, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");
        context = parent.getContext();
        return new ViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.batch.setText(mDataset.get(position).getBatch());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
        Date date = null;
        try {
            date = inputFormat.parse(mDataset.get(position).getExpiryDate());
            String outputDateStr = outputFormat.format(date);
            viewHolder.expiryDate.setText(outputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        viewHolder.availQuantity.setText(mDataset.get(position).getAvailQuantity());
        viewHolder.returnQuantity.setText(mDataset.get(position).getReturnQuantity());

        if(isStockReturnSaved) {
            viewHolder.returnQuantity.setEnabled(false);
            viewHolder.removeRow.setEnabled(false);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getErrorStockReturnQty())) {
            viewHolder.returnQuantity.setError(mDataset.get(position).getErrorStockReturnQty());
        } else {
            viewHolder.returnQuantity.setError(null);
        }


        viewHolder.returnQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setReturnQuantity(caption.getText().toString());
                    caption.setError(null);
                }

            }
        });


        viewHolder.removeRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Product Info?");
                        //alertDialog.setMessage("your message ");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                notifyDataSetChanged();
                                viewHolder.mListener.onClick(v, position);

                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextCallBackListener {

        public TextView productName, batch, expiryDate, availQuantity;
        public ImageButton removeRow;
        public EditText returnQuantity;
        DecimalFormat decimalFormat;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            productName = v.findViewById(R.id.stockReturnProdNameTv);
            batch = v.findViewById(R.id.stockReturnBatchTv);
            expiryDate = v.findViewById(R.id.stockReturnExpiryDateTv);
            availQuantity = v.findViewById(R.id.stockReturnAvailQtyTv);
            returnQuantity = v.findViewById(R.id.stockReturnReturnQtyTv);
            removeRow = v.findViewById(R.id.stockReturnRemoveButton);
            //returnQuantity.setFilters(new InputFilter[]{ new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
            decimalFormat = new DecimalFormat("#,###.00");
            returnQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});

        }

        @Override
        public void updateText(String val, String val2) {
            //productQuantity.setText(val);


        }
    }

    /**
     * Custom class which implements Text Watcher
     */
    private class CustomEtListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, DoctorVisitOurProdAdapter.ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {


        }

        @Override
        public void afterTextChanged(Editable editable) {


        }

    }


}