package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CrmMeeting;

public class MeetingListAdapter extends RecyclerView.Adapter<MeetingListAdapter.ViewHolder> {

    public ArrayList<CrmMeeting> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;

    public MeetingListAdapter(ArrayList<CrmMeeting> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(CrmMeeting item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmMeeting item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @NotNull
    @Override
    public MeetingListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_meeting_history, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder viewHolder, final int position) {

        viewHolder.tvCrmNo.setText(mDataset.get(position).getCrmNumber());

        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getCrmDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.tvCrmDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }

        viewHolder.tvCustomerName.setText(mDataset.get(position).getCustomerCode());
        viewHolder.tvRemarks.setText(mDataset.get(position).getRemarks());
        viewHolder.tvMeetingDate.setText(mDataset.get(position).getMeetingDate());
        viewHolder.tvMeetingType.setText(mDataset.get(position).getMeetingType());
        viewHolder.tvMeetingStatus.setText(mDataset.get(position).getMeetingStatus());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCrmNo, tvCrmDate, tvCustomerName, tvRemarks, tvMeetingDate, tvMeetingType, tvMeetingStatus;

        public ViewHolder(View v) {
            super(v);
            tvCrmNo = v.findViewById(R.id.tvCrmNo);
            tvCrmDate = v.findViewById(R.id.tvCrmDate);
            tvCustomerName = v.findViewById(R.id.tvCustomerName);
            tvRemarks = v.findViewById(R.id.tvRemarks);
            tvMeetingDate = v.findViewById(R.id.tvMeetingDate);
            tvMeetingType = v.findViewById(R.id.tvMeetingType);
            tvMeetingStatus = v.findViewById(R.id.tvMeetingStatus);
        }
    }


}