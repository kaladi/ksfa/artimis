package in.kumanti.emzor.model;

public class CrmOrder {

    public String demoDate;
    public String inspectionDate;
    public String deliveryDate;
    public String installationDate;
    public String trainingDate;
    public String demoAddress;
    public String inspectionAddress;
    public String deliveryAddress;
    public String installationAddress;
    public String trainingAddress;
    public boolean isSync;
    public String latestSyncDate;
    public String createdAt;
    public String updatedAt;
    private int tabOrderId;
    private String quotationId;
    private String tabOrderNumber;
    private String orderDate;
    private String orderStatus;

    public int getTabOrderId() {
        return tabOrderId;
    }

    public void setTabOrderId(int tabOrderId) {
        this.tabOrderId = tabOrderId;
    }

    public String getQuotationId() {
        return quotationId;
    }

    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    public String getTabOrderNumber() {
        return tabOrderNumber;
    }

    public void setTabOrderNumber(String tabOrderNumber) {
        this.tabOrderNumber = tabOrderNumber;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDemoDate() {
        return demoDate;
    }

    public void setDemoDate(String demoDate) {
        this.demoDate = demoDate;
    }

    public String getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(String installationDate) {
        this.installationDate = installationDate;
    }

    public String getTrainingDate() {
        return trainingDate;
    }

    public void setTrainingDate(String trainingDate) {
        this.trainingDate = trainingDate;
    }

    public String getDemoAddress() {
        return demoAddress;
    }

    public void setDemoAddress(String demoAddress) {
        this.demoAddress = demoAddress;
    }

    public String getInspectionAddress() {
        return inspectionAddress;
    }

    public void setInspectionAddress(String inspectionAddress) {
        this.inspectionAddress = inspectionAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getInstallationAddress() {
        return installationAddress;
    }

    public void setInstallationAddress(String installationAddress) {
        this.installationAddress = installationAddress;
    }

    public String getTrainingAddress() {
        return trainingAddress;
    }

    public void setTrainingAddress(String trainingAddress) {
        this.trainingAddress = trainingAddress;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getLatestSyncDate() {
        return latestSyncDate;
    }

    public void setLatestSyncDate(String latestSyncDate) {
        this.latestSyncDate = latestSyncDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
