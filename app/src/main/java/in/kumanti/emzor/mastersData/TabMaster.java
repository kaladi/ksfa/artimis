package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class TabMaster {

    private String tab_code;
    private String sr_code;
    private String tab_seq_prefix;
    private String tab_password;
    private String website_url;
    private String sec_price_list;


    public TabMaster(

            String tab_code,
            String sr_code,
            String tab_seq_prefix,
            String tab_password,
            String website_url,
            String sec_price_list


    ) {

        this.tab_code = tab_code;
        this.sr_code = sr_code;
        this.tab_seq_prefix = tab_seq_prefix;
        this.tab_password = tab_password;
        this.website_url = website_url;
        this.sec_price_list = sec_price_list;


    }

    public String getTab_code() {
        return tab_code;
    }

    public String getSr_code() {
        return sr_code;
    }

    public String getTab_seq_prefix() {
        return tab_seq_prefix;
    }

    public String getTab_password() {
        return tab_password;
    }

    public String getWebsite_url() {
        return website_url;
    }
    public String getSec_price_list() {
        return sec_price_list;
    }

}

