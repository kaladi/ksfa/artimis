package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CustomerReturnProductsReport;

public class CustomerReturnProductsReportsAdapter extends RecyclerView.Adapter<CustomerReturnProductsReportsAdapter.ViewHolder> {

    public ArrayList<CustomerReturnProductsReport> mDataset;
    public Context context;

    public CustomerReturnProductsReportsAdapter(ArrayList<CustomerReturnProductsReport> myDataset) {
        mDataset = myDataset;
    }

    public void add(CustomerReturnProductsReport item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CustomerReturnProductsReport item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public CustomerReturnProductsReportsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_customer_return_products_reports, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.returnDate.setText(mDataset.get(position).getCusReturnDate());
        viewHolder.invoiceNum.setText(mDataset.get(position).getInvoiceNum());
        viewHolder.returnNumber.setText(mDataset.get(position).getCusReturnNumber());
        viewHolder.customerName.setText(mDataset.get(position).getCustomerName());
        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.returnQuantity.setText(mDataset.get(position).getReturnQuantity());
        viewHolder.returnPrice.setText(mDataset.get(position).getReturnPrice());
        viewHolder.returnValue.setText(mDataset.get(position).getReturnValue());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView returnDate, invoiceNum, returnNumber, customerName, productName, returnQuantity, returnPrice, returnValue;

        public ViewHolder(View v) {
            super(v);

            returnDate = v.findViewById(R.id.cusReturnReportsDateTv);
            invoiceNum = v.findViewById(R.id.invoiceNumberTv);
            returnNumber = v.findViewById(R.id.cusReturnNumberDateTv);
            customerName = v.findViewById(R.id.cusReturnCustomerNameTv);
            productName = v.findViewById(R.id.cusReturnProductNameTv);
            returnQuantity = v.findViewById(R.id.cusReturnReturnQtyTv);
            returnPrice = v.findViewById(R.id.cusReturnReturnPriceTv);
            returnValue = v.findViewById(R.id.cusReturnReturnValueTv);
        }
    }


}