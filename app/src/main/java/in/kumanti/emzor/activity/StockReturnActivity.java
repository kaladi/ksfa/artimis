package in.kumanti.emzor.activity;


import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.StockReturnAdapter;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.StockReturnProductsTable;
import in.kumanti.emzor.eloquent.StockReturnTable;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.StockReturn;
import in.kumanti.emzor.model.StockReturnProducts;
import in.kumanti.emzor.model.StockViewInvoiceList;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class StockReturnActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo, saveStockReturnButton;
    TextView actionbarTitle, marqueeText;

    SharedPreferenceManager sharedPreferenceManager;
    String login_id = "", checkin_time = "", customerCode = "";
    MyDBHandler dbHandler;

    TextView stockReturnNumber, stockReturnDate;
    String stockReturnDateDB;
    int stockReturnId;
    String stockReturnIdString = null, stockReturnTime, stockReturnQty;
    int random_num = 0;

    LinearLayout returnTypeContainer, productsContainer, batchContainer;

    CustomSearchableSpinner stockReturnTypeSpinner;
    StockReturnTable stockReturnTable;
    StockReturnProductsTable stockReturnProductTable;
    ArrayList<String> typeList;

    String stockReturnType = "Expired";

    CustomSearchableSpinner productSpinner;
    ArrayList<Product> productArrayList;
    ArrayAdapter<Product> productSpinnerArrayAdapter;

    Button stockReturnAddProdButton, stockReturnAddBatchProductsButton;

    CustomSearchableSpinner batchNameSpinner;
    ArrayList<StockViewInvoiceList> batchNameArrayList;
    ArrayAdapter<StockViewInvoiceList> batchNameArrayAdapter;

    ArrayList<StockReturnProducts> stockReturnProductsArrayList;
    StockReturnAdapter stockReturnProductsAdapter;
    RecyclerViewItemListener listener;
    RecyclerView stockReturnProdRecyclerView;
    ImageView createSignature, viewSignature;
    Bitmap selectedImage = null;
    EditText signatureName;
    String emailType = "StockReturn";
    String companyName,companyCode1, compAdd1, compAdd2, compAdd3, compMblNo;
    String stockReturnStatus = "";
    Button smsButton, printButton, emailButton;
    PrintBluetooth printBluetooth;
    String customerPhoneNumber = "";
    String smsMessageText = "";
    Print print;
    GeneralSettingsTable generalSettingsTable;
    // android built in classes for bluetooth operations
    String printerName = "MP80-17031810239";
    String printMsg;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    String product_id = "", productName, productBatch, expiryDate, productUOM, productPrice, availQty, showExpiryDate;
    Boolean isBatchControlled = false;
    GeneralSettings gs;
    Boolean isBluetoothConnected = false;
    //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //Device found
                isBluetoothConnected = true;
            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                //Device is now connected
                isBluetoothConnected = true;
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //Done searching
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
                //Device is about to disconnect
                isBluetoothConnected = false;
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                //Device has disconnected
                isBluetoothConnected = false;
            }
        }
    };
    private RecyclerView.LayoutManager stockReturnProductsManager;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private boolean isStockReturnSaved = false;
    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";
    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_stock_return);

        initializeViews();
        setFooterDateTime();


        actionbarTitle.setText("Stock Return");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        setStockReturnDate();
        generateStockReturnSeqNumber();
        loadOutcomeType();
        populateHeaderDetails();

        stockReturnProdRecyclerView.setHasFixedSize(true);

        stockReturnProductsManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        stockReturnProdRecyclerView.setLayoutManager(stockReturnProductsManager);
        stockReturnProductsArrayList = new ArrayList<StockReturnProducts>();


        stockReturnTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {

                stockReturnTypeSpinner.isSpinnerDialogOpen = false;
                stockReturnType = (String) adapterView.getItemAtPosition(pos);
                updateProductSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                stockReturnTypeSpinner.isSpinnerDialogOpen = false;
            }
        });

        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;
                Log.d("Products", "Selected Product" + pos);
                Product p = (Product) adapterView.getItemAtPosition(pos);
                Log.d("Products", "Selected Product" + pos + p.product_name);

                product_id = p.product_id;
                productName = p.product_name;
                productBatch = "";
                expiryDate = "";
                productUOM = p.product_uom;
                productPrice = p.product_price;
                if ("Yes".equals(p.getBatch_controlled())) {
                    isBatchControlled = true;
                    //Working with List to Show Data's
                    batchNameArrayList = new ArrayList<StockViewInvoiceList>();
                    Cursor data = dbHandler.getstock_viewinvoiceByExpiry(product_id, stockReturnType.equals("Expired") ? true : false);
                    int numRows1 = data.getCount();
                    if (numRows1 == 0) {
                        Toast.makeText(getApplicationContext(), "The Database is empty", Toast.LENGTH_LONG).show();
                    } else {
                        int i = 0;
                        while (data.moveToNext()) {
                            boolean flag = true;
                            for (StockReturnProducts srd : stockReturnProductsArrayList) {
                                if (srd.getProductCode().equals(product_id) && data.getString(1).equals(srd.getBatch()))
                                    flag = false;
                            }
                            if (flag) {
                                StockViewInvoiceList stocks = new StockViewInvoiceList(
                                        data.getString(0),
                                        data.getString(1),
                                        data.getString(2));
                                batchNameArrayList.add(i, stocks);
                                i++;
                            }

                            //System.out.println(userList.get(i).getFirstName());


                        }
                        StockViewInvoiceList svi = new StockViewInvoiceList("", "Select Batch", "");
                        batchNameArrayList.add(0, svi);
                        //ViewStockOrderAdapter adapter1 = new ViewStockOrderAdapter(getApplicationContext(), R.layout.list_view_stock_invoice, stockList);
                        //stock_list = (ListView) findViewById(R.id.stock_list);
                        batchNameArrayAdapter = new ArrayAdapter<StockViewInvoiceList>(getApplicationContext(), android.R.layout.simple_spinner_item, batchNameArrayList);
                        batchNameSpinner.setAdapter(batchNameArrayAdapter);
                        batchContainer.setVisibility(View.VISIBLE);
                        stockReturnAddProdButton.setVisibility(View.INVISIBLE);

                    }
                } else {
                    availQty = String.valueOf(dbHandler.getstockonhand(product_id));
                    isBatchControlled = false;
                    batchContainer.setVisibility(View.GONE);
                    stockReturnAddProdButton.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });


        batchNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {

                batchNameSpinner.isSpinnerDialogOpen = false;

                StockViewInvoiceList svi = (StockViewInvoiceList) adapterView.getItemAtPosition(pos);

                productBatch = svi.getBatch_number();
                expiryDate = svi.getExpiry_date();

                getShowExpiryDate();


                availQty = svi.getStock_quantity();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                batchNameSpinner.isSpinnerDialogOpen = false;

            }
        });

        listener = (view, position) -> {
            updateProductSpinner();
        };

        stockReturnAddProdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addStockReturnProductsList();

            }
        });


        stockReturnAddBatchProductsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addStockReturnProductsList();
            }
        });


        createSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewSignature();
            }
        });

        viewSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedImage != null) {
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.image_preview, null);
                    ImageView capturedImage, closeButton;
                    capturedImage = alertLayout.findViewById(R.id.capturedImage);
                    closeButton = alertLayout.findViewById(R.id.closeImageView);
                    capturedImage.setImageBitmap(selectedImage);
                    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());

                    alert.setView(alertLayout);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(true);

                    android.app.AlertDialog dialog = alert.create();
                    dialog.show();


                }

            }
        });

        saveStockReturnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gs = generalSettingsTable.getSettingByKey("stockReturnSignature");
                if (gs != null && gs.getValue().equals("Yes")) {
                    if (TextUtils.isEmpty(signatureName.getText())) {
                        Toast.makeText(getApplicationContext(), "Please Enter Signee's Name", Toast.LENGTH_LONG).show();
                        return;
                    } else if (selectedImage == null) {
                        Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                        return;
                    }

                }


                if (stockReturnProductsArrayList.size() != 0) {
                    hideSoftKeyboard(getApplicationContext(), v);
                    View current = getCurrentFocus();
                    current.clearFocus();
                    boolean validCheck = true;

                    for (StockReturnProducts srp : stockReturnProductsArrayList) {
                        if (TextUtils.isEmpty(srp.getReturnQuantity())) {
                            srp.setErrorStockReturnQty("Please enter quantity");
                            validCheck = false;
                        } else {
                            if (Integer.parseInt(srp.getAvailQuantity()) < Integer.parseInt(srp.getReturnQuantity()) || 0 > Integer.parseInt(srp.getReturnQuantity())) {
                                srp.setErrorStockReturnQty("Return quantity should be less than available quantity");
                                Toast.makeText(getApplicationContext(), "Return quantity should be less than available quantity", Toast.LENGTH_LONG).show();
                                validCheck = false;
                            } else {
                                srp.setErrorStockReturnQty(null);
                            }
                        }
                    }
                    if (!validCheck) {
                        stockReturnProductsAdapter = new StockReturnAdapter(stockReturnProductsArrayList, listener, false);

                       /* stockReturnProductsAdapter = new StockReturnAdapter(stockReturnProductsArrayList, isStockReturnSaved);
                        Log.d("StockReturn", "isStockReturnSave----"+isStockReturnSaved);*/
                        stockReturnProdRecyclerView.setAdapter(stockReturnProductsAdapter);
                        return;
                    }

                    final Dialog alertbox = new Dialog(v.getRootView().getContext());

                    LayoutInflater mInflater = (LayoutInflater)
                            getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    final View alertLayout = mInflater.inflate(R.layout.stock_return_save_confirmation, null);

                    alertbox.setCancelable(false);
                    alertbox.setContentView(alertLayout);

                    final Button Yes = alertLayout.findViewById(R.id.stockReturnYesButton);
                    final Button No = alertLayout.findViewById(R.id.stockReturnNoButton);

                    Yes.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            Toast.makeText(getApplicationContext(), "Stock Return saved Successfully", Toast.LENGTH_LONG).show();
                            alertbox.dismiss();

                            String visitSequence = sharedPreferences.getString("VisitSequence", "");


                            StockReturn stockReturn = new StockReturn();
                            stockReturn.setStockReturnNumber(stockReturnNumber.getText().toString());
                            stockReturn.setStockReturnDate(stockReturnDateDB);
                            stockReturn.setStockReturnType(stockReturnType);
                            stockReturn.setRandomNum(random_num);
                            stockReturn.setVisitSeqNumber(visitSequence);

                            if (selectedImage != null) {
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                                byte[] byteArrayImage = baos.toByteArray();
                                String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

                                stockReturn.setSignature(encodedImage);

                            }
                            stockReturn.setSigneeName(signatureName.getText().toString());

                            stockReturnTable.create(stockReturn);

                            for (StockReturnProducts srp : stockReturnProductsArrayList) {
                                // StockReturnProducts srp1 = new StockReturnProducts();
                                srp.setStockReturnNumber(stockReturnNumber.getText().toString());
                                srp.setProductCode(srp.getProductCode());
                                srp.setProductName(srp.getProductName());
                                srp.setAvailQuantity(srp.getAvailQuantity());
                                srp.setReturnQuantity(srp.getReturnQuantity());
                                try {
                                    srp.setReturnValue(String.valueOf(Double.parseDouble(srp.getReturnQuantity()) * Double.parseDouble(srp.getReturnPrice())));
                                } catch (Exception e) {

                                }
                                srp.setReturnPrice(srp.getReturnPrice());


                                stockReturnProductTable.create(srp);


                                int rec_random_num = dbHandler.get_receipt_random_num();

                                SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
                                Date myDate = new Date();
                                String rec_seq_date = timeStampFormat.format(myDate);
                                String receipt_num;
                                rec_random_num = rec_random_num + 1;

                                if (rec_random_num <= 9) {
                                    receipt_num = tab_prefix + "SR" + rec_seq_date + "00" + rec_random_num;

                                } else if (rec_random_num > 9 & random_num < 99) {
                                    receipt_num = tab_prefix + "SR" + rec_seq_date + "0" + rec_random_num;

                                } else {
                                    receipt_num = tab_prefix + "SR" + rec_seq_date + rec_random_num;
                                }


                                long id = dbHandler.createstockreciept
                                        (receipt_num,
                                                stockReturnDateDB,
                                                "NGN",
                                                login_id,
                                                null,
                                                "Stock Return",
                                                "",
                                                srp.getProductCode(),
                                                srp.getProductUOM(),
                                                String.valueOf((-1) * Integer.parseInt(srp.returnQuantity)),
                                                srp.getReturnPrice(),
                                                srp.getReturnValue(),
                                                "",
                                                "Return",
                                                "Completed",
                                                rec_random_num,
                                                visitSequence,
                                                stockReturnNumber.getText().toString(),
                                                "");
                                if (!TextUtils.isEmpty(srp.batch)) {
                                    dbHandler.insertingProductBatch(srp.getProductCode(), srp.batch, srp.getExpiryDate(), String.valueOf((-1) * Integer.parseInt(srp.returnQuantity)), "return", receipt_num, "return", "Completed", "WarehouseReturn");
                                }
                            }

                            saveStockReturnButton.setVisibility(View.GONE);
                            createSignature.setEnabled(false);
                            signatureName.setEnabled(false);
                            returnTypeContainer.setVisibility(View.GONE);
                            productsContainer.setVisibility(View.GONE);
                            stockReturnStatus = "Completed";
                            isStockReturnSaved = true;

                            stockReturnProductsAdapter = new StockReturnAdapter(stockReturnProductsArrayList, listener, true);
                            stockReturnProdRecyclerView.setAdapter(stockReturnProductsAdapter);


                        }
                    });

                    No.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            alertbox.dismiss();
                        }

                    });

                    alertbox.show();
                } else
                    Toast.makeText(getApplicationContext(), "There is no products added", Toast.LENGTH_LONG).show();
            }
        });

        /*
         *  Check the bluetooth connection state of the Printer
         */

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter);


        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isBluetoothConnected) {
                    Toast.makeText(getApplicationContext(), "Unable to connect to Bluetooth Device", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!stockReturnStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save stock return products to print", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    int totalSize = 48;
                    String ESC_NEW_LINE = "\n";
                    String horizontalLine = "-----------------------------------------------";
                    printMsg = "";
                    printMsg += print.centerString(totalSize, companyName);
                    printMsg += print.centerString(totalSize, compAdd1);
                    printMsg += print.centerString(totalSize, compAdd2);
                    printMsg += print.centerString(totalSize, compAdd3);
                    printMsg += print.centerString(totalSize, compMblNo);

                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE + "Stock Return Number:" + stockReturnNumber.getText().toString();
                    printMsg += ESC_NEW_LINE + print.padRight("Date:" + stockReturnDate.getText().toString(), 24) + print.padLeft("TIME:" + stockReturnTime, 24);
                    printMsg += "BDE Name:" + sr_name_details;

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += print.centerString(15, "Product Name") + " " + "  VOL " + print.padLeft("BATCH", 10) + "  " + print.padLeft("EXP DATE", 14);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    int total = 0;
                    int i = 1;
                    for (StockReturnProducts ls : stockReturnProductsArrayList) {
                        printMsg += print.padLeft(String.valueOf(i) + ".", 3) + print.padRight(ls.getProductName().substring(0, 11), 12) + " " + print.padLeft(String.valueOf(ls.getReturnQuantity()), 5) + " " + print.padLeft(ls.getBatch(), 10) + "  " + print.padLeft(((ls.getExpiryDate().toString())), 14);
                        total += (Integer.parseInt(ls.getReturnQuantity()));
                        i++;
                    }


                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;


                    printMsg += print.padRight("", 15) + print.padRight("Total QTY:", 19) + print.padLeft(String.valueOf((total)), 14);


                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.padLeft("Customer Sign", totalSize);
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    print.printContent(printMsg);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        });


        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!stockReturnStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save stock return products to send email", Toast.LENGTH_LONG).show();
                    return;
                }


                int total = 0;
                int i = 1;
                for (StockReturnProducts ls : stockReturnProductsArrayList) {
                    total += (Integer.parseInt(ls.getReturnQuantity()));
                    i++;
                }


                Gson gson = new GsonBuilder().setLenient().create();
                JsonArray stockReturnProductsJsonArray = gson.toJsonTree(stockReturnProductsArrayList).getAsJsonArray();

                JsonObject jp = new JsonObject();

                jp.addProperty("emailType", emailType);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);
                jp.addProperty("stockReturnNumber", stockReturnNumber.getText().toString());
                jp.addProperty("stockReturnDate", stockReturnDate.getText().toString());
                jp.addProperty("stockReturnTime", stockReturnTime);
                jp.addProperty("bdeName", sr_name_details);

                jp.add("stockReturnProducts", stockReturnProductsJsonArray);
                jp.addProperty("totalQuantity", String.valueOf(total));


                if (selectedImage != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                    jp.addProperty("customerSignature", encodedImage);
                }

                Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingEmailData(jp);

                Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND:", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }

                });

            }
        });

        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!stockReturnStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save stock return products to send sms", Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(customerPhoneNumber)) {
                    Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                    return;
                }


                int total = 0;
                int i = 1;
                for (StockReturnProducts ls : stockReturnProductsArrayList) {
                    total += (Integer.parseInt(ls.getReturnQuantity()));
                    i++;
                }

                smsMessageText = "Sale Stock Return\n" +
                        "\n" +
                        "Stock Return No: #" + stockReturnNumber.getText().toString() + "\n" +
                        "Stock Return Date: " + stockReturnDate.getText().toString() + "\n" +
                        "Stock Return Qty: " + total + "\n";

                Gson gson = new GsonBuilder().setLenient().create();
                JsonArray stockReturnProductsJsonArray = gson.toJsonTree(stockReturnProductsArrayList).getAsJsonArray();

                JsonObject jp = new JsonObject();

                jp.addProperty("smsType", emailType);
                jp.addProperty("smsMessageText", smsMessageText);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);
                jp.addProperty("customerPhoneNumber", customerPhoneNumber);
                jp.addProperty("stockReturnNumber", stockReturnNumber.getText().toString());
                jp.addProperty("stockReturnDate", stockReturnDate.getText().toString());
                jp.addProperty("stockReturnTime", stockReturnTime);
                jp.addProperty("bdeName", sr_name_details);
                jp.addProperty("totalQuantity", String.valueOf(total));

                Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.SMS_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingStockReturnSMSData(jp);

                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND:", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }

                });

              /*  try {
                    smsMessageText = "Sale Stock Return\n" +
                            "\n" +
                            "Stock Return No: #" + stockReturnNumber.getText().toString() + "\n" +
                            "Stock Return Date: " + stockReturnDate.getText().toString() + "\n" +
                            "Stock Return Time: " + stockReturnTime + "\n" +
                            "Stock Return Qty: " + total + "\n";

                    Log.d("SMS TEST", "TEST MESSAGE--" + smsMessageText);


                    SmsManager smsManager = SmsManager.getDefault();
                    ArrayList<String> parts = smsManager.divideMessage(smsMessageText);
                    smsManager.sendMultipartTextMessage(customerPhoneNumber, null, parts,
                            null, null);

                    Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.d("Sms", "Exception" + e);
                }
*/

            }
        });


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }

    private void getShowExpiryDate() {

        Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(expiryDate);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");
            showExpiryDate = format.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void updateProductSpinner() {
        if (stockReturnType.equals("Expired")) {
            Log.d("StockReturnLog", "Expired Products***** ");
            productArrayList = dbHandler.getStockReceiptProductsByType(1, 0);

        } else {
            Log.d("StockReturnLog", "Regular Products***** ");
            productArrayList = dbHandler.getStockReceiptProductsByType(0, 0);
        }

        ArrayList<Product> newProductList = new ArrayList<Product>();
        for (Product product : productArrayList) {
            boolean flag = true;
            for (StockReturnProducts srd : stockReturnProductsArrayList) {
                if (product.product_id.equals(srd.getProductCode()) && TextUtils.isEmpty(srd.getBatch()))
                    flag = false;
            }
            if (flag)
                newProductList.add(product);
        }
        Log.d("ViewStock--", "MyStock productArrayList  " + productArrayList.size());

        Product p = new Product();
        p.product_name = "Select Product";
        p.product_id = "-1";
        newProductList.add(0, p);
        productSpinnerArrayAdapter = new ArrayAdapter<Product>(getApplicationContext(), android.R.layout.simple_spinner_item, newProductList);
        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(productSpinnerArrayAdapter);
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        stockReturnNumber = findViewById(R.id.stockReturnNumberTextView);
        stockReturnDate = findViewById(R.id.stockReturnDateTv);

        productsContainer = findViewById(R.id.stockReturnProductContainer);
        returnTypeContainer = findViewById(R.id.returnTypeContainer);

        stockReturnTypeSpinner = findViewById(R.id.stockReturnTypeSpinner);
        productSpinner = findViewById(R.id.stockReturnProdSpinner);

        batchContainer = findViewById(R.id.stockReturnBatchContainer);
        batchNameSpinner = findViewById(R.id.stockReturnBatchSpinner);

        stockReturnProdRecyclerView = findViewById(R.id.stockReturnProdRecyclerView);

        stockReturnAddProdButton = findViewById(R.id.stockReturnAddProdButton);
        stockReturnAddBatchProductsButton = findViewById(R.id.stockReturnBatchAddButton);

        signatureName = findViewById(R.id.signCustomerNameEditText);
        createSignature = findViewById(R.id.CreateSignImageView);
        viewSignature = findViewById(R.id.ViewSignatureImageView);

        saveStockReturnButton = findViewById(R.id.stockReturnSaveImageButton);

        printButton = findViewById(R.id.stockReturnPrintButton);
        emailButton = findViewById(R.id.stockReturnEmailButton);
        smsButton = findViewById(R.id.stockReturnSmsButton);

        print = new Print(this);
        printBluetooth = new PrintBluetooth(this);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        stockReturnTable = new StockReturnTable(getApplicationContext());
        stockReturnProductTable = new StockReturnProductsTable(getApplicationContext());
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        batchContainer.setVisibility(View.GONE);

        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());

        gs = generalSettingsTable.getSettingByKey("enablePrint");
        if (gs != null && gs.getValue().equals("Yes")) {
            printButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableSms");
        if (gs != null && gs.getValue().equals("Yes")) {
            smsButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableEmail");
        if (gs != null && gs.getValue().equals("Yes")) {
            emailButton.setVisibility(View.VISIBLE);
        }


        signatureName.setFilters(new InputFilter[]{filter});

    }

    private void loadOutcomeType() {
        typeList = new ArrayList<String>();
        typeList.add("Regular");
        typeList.add("Expired");
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, typeList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        stockReturnTypeSpinner.setAdapter(spinnerArrayAdapter);
    }

    private void createNewSignature() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);
        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                selectedImage = signatureBitmap;
                viewSignature.setImageBitmap(signatureBitmap);
                dialog.dismiss();
            }
        });

    }

    private void addStockReturnProductsList() {

        if (productSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Please choose a product before adding to the Product List", Toast.LENGTH_SHORT).show();
            return;
        }
        if (isBatchControlled && batchNameSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Please choose a product batch before adding to the Product List", Toast.LENGTH_SHORT).show();
            return;
        }

        StockReturnProducts stockReturnProducts = new StockReturnProducts();
        stockReturnProducts.setProductName(productName);
        stockReturnProducts.setProductCode(product_id);
        stockReturnProducts.setBatch(productBatch);
        stockReturnProducts.setAvailQuantity(availQty);
        stockReturnProducts.setExpiryDate(expiryDate);
        stockReturnProducts.setProductUOM(productUOM);
        stockReturnProducts.setReturnPrice(productPrice);
        stockReturnProductsArrayList.add(stockReturnProducts);
        stockReturnProductsAdapter = new StockReturnAdapter(stockReturnProductsArrayList, listener, false);

        /*stockReturnProductsAdapter = new StockReturnAdapter(stockReturnProductsArrayList, isStockReturnSaved);
        Log.d("StockReturn", "isStockReturnSave----"+isStockReturnSaved);*/

        stockReturnProdRecyclerView.setAdapter(stockReturnProductsAdapter);
        updateProductSpinner();
    }

    private void setStockReturnDate() {
        String PATTERN = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        String PATTERN_DISPLAY = "dd MMM YY";
        SimpleDateFormat dateFormatDisplay = new SimpleDateFormat();
        dateFormatDisplay.applyPattern(PATTERN_DISPLAY);
        String dateDisplay = dateFormatDisplay.format(Calendar.getInstance().getTime());
        stockReturnDateDB = date;
        stockReturnDate.setText(dateDisplay);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        stockReturnTime = sdf.format(outDate);


    }

    private void setFooterDateTime() {
        final TextView datetime = (TextView) findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void generateStockReturnSeqNumber() {

        random_num = stockReturnTable.getStockReturnNumber();


        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        Log.d("Stock Return", "Random No---" + random_num);

        if (random_num <= 9) {
            stockReturnNumber.setText(Constants.TAB_PREFIX + "SRT" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            stockReturnNumber.setText(Constants.TAB_PREFIX + "SRT" + rec_seq_date + "0" + random_num);

        } else {
            stockReturnNumber.setText(Constants.TAB_PREFIX + "SRT" + rec_seq_date + random_num);
        }

        stockReturnIdString = stockReturnNumber.getText().toString();

    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        login_id = bundle.getString("login_id");
        //Populate SalesRep Image
        Cursor salesimage = dbHandler.getsalesrepimage(login_id);
        int count = salesimage.getCount();
        if (count == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesimage.moveToNext()) {
                byte[] blob = salesimage.getBlob(9);
                int salesimage_length = dbHandler.get_length_salesrep_image(login_id);
                if (salesimage_length == 0) {

                } else {
                    Bitmap bmp = convertByteArrayToBitmap(blob);
                    sr_image.setImageBitmap(bmp);
                }
            }
        }

        //Populate SalesRep Details
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                //byte[] blob = salesRepDetails.getBlob(9);
                //Bitmap bmp= convertByteArrayToBitmap(blob);
                //sr_image.setImageBitmap(bmp);
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }

        getCompanyInfo();

    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);


            }
        }

    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mReceiver != null)
            unregisterReceiver(mReceiver);
    }

}
