package in.kumanti.emzor.model;


import java.io.Serializable;

public class Product implements Serializable {

    public int id;
    public String product_id;
    public String product_code;
    public String company_id;
    public String customer_id;
    public String sales_rep_id;
    public String product_name;
    public String product_price;
    public String product_description;
    public String product_uom;
    public String product_type;
    public String remarks;
    public String batch_controlled;
    public String created_at;
    public String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }


    public String getSales_rep_id() {
        return sales_rep_id;
    }

    public void setSales_rep_id(String sales_rep_id) {
        this.sales_rep_id = sales_rep_id;
    }


    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }


    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }


    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }


    public String getProduct_uom() {
        return product_uom;
    }

    public void setProduct_uom(String product_uom) {
        this.product_uom = product_uom;
    }


    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getBatch_controlled() {
        return batch_controlled;
    }

    public void setBatch_controlled(String batch_controlled) {
        this.batch_controlled = batch_controlled;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    @Override
    public String toString() {
        return product_name;
    }

}
