package in.kumanti.emzor.utils;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/**
 * A FrameLayout that includes the new onApplyWindowInsetsListener api. Note that this returns a
 * simpler Rect instead of a WindowsInsets to simplify the implementation.
 */
public class InsetFrameLayout extends FrameLayout {
    private Rect mInsets;
    private OnApplyWindowInsetsListener mInsetListener;

    public InsetFrameLayout(Context context) {
        super(context);
    }

    public InsetFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InsetFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected boolean fitSystemWindows(@NonNull Rect insets) {
        mInsets = new Rect(insets);
        if (mInsetListener != null) {
            mInsetListener.onApplyWindowInsets(this, mInsets);
        }
        return true;
    }

    public void setOnApplyInsetWindowListener(OnApplyWindowInsetsListener listener) {
        mInsetListener = listener;
        if (mInsetListener != null && mInsets != null) {
            mInsetListener.onApplyWindowInsets(this, mInsets);
        }
    }

    public static interface OnApplyWindowInsetsListener {
        void onApplyWindowInsets(View v, Rect insets);
    }
}
