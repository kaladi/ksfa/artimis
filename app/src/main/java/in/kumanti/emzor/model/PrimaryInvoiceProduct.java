package in.kumanti.emzor.model;

import java.util.ArrayList;

public class PrimaryInvoiceProduct {

    ArrayList<StockReceiptBatch> batchDetailArrayList = new ArrayList<StockReceiptBatch>();
    private int primaryInvoiceProductId;
    private String primaryInvoiceNumber;
    private String primaryInvoiceProductCode;
    private String primaryInvoiceProductName;
    private String primaryInvoiceProductUom;
    private String primaryInvoiceProductQuantity;
    private String error_primaryInvoiceProductName;
    private String primaryInvoiceProductPrice;
    private String primaryInvoiceProductTotalPrice;
    private String batchControlled;
    private String conversion;

    public int getPrimaryInvoiceProductId() {
        return primaryInvoiceProductId;
    }

    public void setPrimaryInvoiceProductId(int primaryInvoiceProductId) {
        this.primaryInvoiceProductId = primaryInvoiceProductId;
    }

    public String getPrimaryInvoiceNumber() {
        return primaryInvoiceNumber;
    }

    public void setPrimaryInvoiceNumber(String primaryInvoiceNumber) {
        this.primaryInvoiceNumber = primaryInvoiceNumber;
    }

    public String getPrimaryInvoiceProductCode() {
        return primaryInvoiceProductCode;
    }

    public void setPrimaryInvoiceProductCode(String primaryInvoiceProductCode) {
        this.primaryInvoiceProductCode = primaryInvoiceProductCode;
    }

    public String getPrimaryInvoiceProductName() {
        return primaryInvoiceProductName;
    }

    public void setPrimaryInvoiceProductName(String primaryInvoiceProductName) {
        this.primaryInvoiceProductName = primaryInvoiceProductName;
    }


    public String getPrimaryInvoiceProductUom() {
        return primaryInvoiceProductUom;
    }

    public void setPrimaryInvoiceProductUom(String primaryInvoiceProductUom) {
        this.primaryInvoiceProductUom = primaryInvoiceProductUom;
    }


    public String getPrimaryInvoiceProductQuantity() {
        return primaryInvoiceProductQuantity;
    }

    public void setPrimaryInvoiceProductQuantity(String primaryInvoiceProductQuantity) {
        this.primaryInvoiceProductQuantity = primaryInvoiceProductQuantity;
    }


    public String getPrimaryInvoiceProductPrice() {
        return primaryInvoiceProductPrice;
    }

    public void setPrimaryInvoiceProductPrice(String primaryInvoiceProductPrice) {
        this.primaryInvoiceProductPrice = primaryInvoiceProductPrice;
    }


    public String getPrimaryInvoiceProductTotalPrice() {
        return primaryInvoiceProductTotalPrice;
    }

    public void setPrimaryInvoiceProductTotalPrice(String primaryInvoiceProductTotalPrice) {
        this.primaryInvoiceProductTotalPrice = primaryInvoiceProductTotalPrice;
    }

    public String getBatchControlled() {
        return batchControlled;
    }

    public void setBatchControlled(String batchControlled) {
        this.batchControlled = batchControlled;
    }

    public ArrayList<StockReceiptBatch> getBatchDetailArrayList() {
        return batchDetailArrayList;
    }

    public void setBatchDetailArrayList(ArrayList<StockReceiptBatch> batchDetailArrayList) {
        this.batchDetailArrayList = batchDetailArrayList;
    }

    public String getError_primaryInvoiceProductName() {
        return error_primaryInvoiceProductName;
    }

    public void setError_primaryInvoiceProductName(String error_primaryInvoiceProductName) {
        this.error_primaryInvoiceProductName = error_primaryInvoiceProductName;
    }

    public String getConversion() {
        return conversion;
    }

    public void setConversion(String conversion) {
        this.conversion = conversion;
    }

    @Override
    public String toString() {
        return primaryInvoiceProductName;
    }

}
