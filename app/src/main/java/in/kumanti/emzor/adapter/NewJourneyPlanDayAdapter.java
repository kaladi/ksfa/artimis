package in.kumanti.emzor.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.NewJourneyPlanCustomers;

public class NewJourneyPlanDayAdapter extends RecyclerView.Adapter<NewJourneyPlanDayAdapter.ViewHolder> {

    public ArrayList<NewJourneyPlanCustomers> mDataset;
    public Context context;
    public boolean isView = false;
    private RecyclerViewItemListener mListener;

    public NewJourneyPlanDayAdapter(ArrayList<NewJourneyPlanCustomers> myDataset, RecyclerViewItemListener listener, boolean isViewFlag) {
        mDataset = myDataset;
        mListener = listener;
        isView = isViewFlag;
    }

    public void add(NewJourneyPlanCustomers item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(NewJourneyPlanCustomers item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public NewJourneyPlanDayAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_new_journey_plan_day, parent, false);
        context = parent.getContext();


        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.customerName.setText(mDataset.get(position).getCustomerName());
        viewHolder.customerCode.setText(mDataset.get(position).getCustomerCode());
        viewHolder.customerType.setText(mDataset.get(position).getCustomerType());
        if (isView)
            viewHolder.deleteCustomerButton.setVisibility(View.GONE);
        viewHolder.deleteCustomerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Customer Info?");
                        //alertDialog.setMessage("your message ");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                notifyDataSetChanged();
                                viewHolder.mListener.onClick(v, position);
                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView customerName, customerCode, customerType;
        public Button productName;
        public ImageButton deleteCustomerButton;
        private RecyclerViewItemListener mListener;


        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;

            customerName = v.findViewById(R.id.newJourneyPlanCusNameTv);
            customerCode = v.findViewById(R.id.newJourneyPlanCusCodeTv);
            customerType = v.findViewById(R.id.newJourneyPlanCusTypeTv);
            deleteCustomerButton = v.findViewById(R.id.newJourneyPlanRemoveButton);

        }
    }


}