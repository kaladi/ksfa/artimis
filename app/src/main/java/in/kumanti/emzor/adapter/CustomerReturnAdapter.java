package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CustomerReturnProducts;
import in.kumanti.emzor.model.InvoiceProductItems;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class CustomerReturnAdapter extends RecyclerView.Adapter<CustomerReturnAdapter.ViewHolder> {

    public ArrayList<InvoiceProductItems> mDataset;
    public Context context;
    public String quantity;
    DecimalFormat decimalFormat;
    ArrayList<CustomerReturnProducts> customerReturnProductsArrayList;
    String formatPrice, totalReturnValue;
    private RecyclerViewItemListener mListener, batchListener;
    private boolean isStockReturnSaved;


    public CustomerReturnAdapter(ArrayList<InvoiceProductItems> myDataset, RecyclerViewItemListener listener, RecyclerViewItemListener blistener) {
        mDataset = myDataset;
        mListener = listener;
        batchListener = blistener;
        // isStockReturnSaved = stockReturnSaved;
    }

    public void add(InvoiceProductItems item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(InvoiceProductItems item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public CustomerReturnAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_customer_stock_return_products, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener, new CustomQtyChangeListener(), batchListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int pos) {
        int currentPos = pos;
        if (mDataset.get(0).getProduct().equals("Select") && mDataset.get(0).getProduct_id().equals("-1")) {
            currentPos = pos + 1;
        }
        final int position = currentPos;
        viewHolder.productName.setText(mDataset.get(position).getProduct());
        viewHolder.uom.setText(mDataset.get(position).getUom());
        viewHolder.quantity.setText(mDataset.get(position).getQuantity());
        viewHolder.price.setText(mDataset.get(position).getPrice());
        viewHolder.valueTV.setText(mDataset.get(position).getValue());
        viewHolder.customQtyChangeListener.updatePosition(position, viewHolder);

        viewHolder.returnQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, mDataset.get(position).getQuantity())});

        viewHolder.returnQuantity.setText(String.valueOf(mDataset.get(position).getReturnQuantity()));

        if (!TextUtils.isEmpty(mDataset.get(position).getErrorQuantity())) {
            viewHolder.returnQuantity.setError(mDataset.get(position).getErrorQuantity());
        } else {
            viewHolder.returnQuantity.setError(null);
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getErrorProduct())) {
            viewHolder.productName.setError(mDataset.get(position).getErrorProduct());
        } else {
            viewHolder.productName.setError(null);
        }

        viewHolder.returnQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setReturnQuantity(caption.getText().toString());
                    caption.setError(null);
                }

            }
        });
        //If Not equals null means get the enter the batch details.....
        viewHolder.productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size()) {
                    if (mDataset.get(position).getReturnQuantity() == null || TextUtils.isEmpty(mDataset.get(position).getReturnQuantity())) {
                        Toast.makeText(context, "Please enter the return quantity", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Log.d("DataCheck", "CurrentPosition onCLick " + position);
                    if (mDataset.get(position).getBatch_controlled() != null && mDataset.get(position).getBatch_controlled().equals("Yes"))
                        viewHolder.batchListener.onClick(v, position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mDataset.size() != 0 && mDataset.get(0).getProduct().equals("Select") && mDataset.get(0).getProduct_id().equals("-1")) {
            return mDataset.size() - 1;
        } else
            return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextCallBackListener {

        public TextView productName, uom, quantity, price, valueTV;
        public ImageButton removeRow;
        public EditText returnQuantity;
        DecimalFormat decimalFormat;
        CustomQtyChangeListener customQtyChangeListener;
        LinearLayout productLine;
        private RecyclerViewItemListener mListener, batchListener;

        public ViewHolder(View v, RecyclerViewItemListener listener, CustomQtyChangeListener customQtyChangeListener, RecyclerViewItemListener blistener) {
            super(v);
            mListener = listener;
            batchListener = blistener;
            productLine = v.findViewById(R.id.productLine);
            productName = v.findViewById(R.id.product);
            uom = v.findViewById(R.id.uom);
            quantity = v.findViewById(R.id.quantity);
            price = v.findViewById(R.id.price);
            valueTV = v.findViewById(R.id.value);
            returnQuantity = v.findViewById(R.id.returnQuantity);
            this.customQtyChangeListener = customQtyChangeListener;
            //returnQuantity.setFilters(new InputFilter[]{ new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
            decimalFormat = new DecimalFormat("#,###.00");
            returnQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
            returnQuantity.addTextChangedListener(customQtyChangeListener);
        }

        @Override
        public void updateText(String val, String val2) {
            //productQuantity.setText(val);
            if (!TextUtils.isEmpty(val2)) {
                Log.d("DataCheck", "Vlaue " + val2);
                formatPrice = decimalFormat.format(Double.parseDouble(val2));
                String value = formatPrice;
                if (value != null && !value.equals("")) {
                    if (value.startsWith(".")) {
                        value = "0" + value;
                    }
                }
                valueTV.setText(value);
                if (!TextUtils.isEmpty(returnQuantity.getText().toString())) {
                    returnQuantity.setError(null);
                }
            } else {
                valueTV.setText(val2);
            }

        }
    }

    /**
     * Custom class which implements Text Watcher
     */
    private class CustomQtyChangeListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        ViewHolder v;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
            v = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String value = charSequence.toString();
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + charSequence.toString();
                }
            }
            // Change the value of array according to the position
            mDataset.get(this.position).setReturnQuantity(value);
            //textCallBackListener.updateText(charSequence.toString(),charSequence.toString());
            // Doing the calculation
            if (!mDataset.get(this.position).getReturnQuantity().equals("")) {
                if (Double.parseDouble(mDataset.get(this.position).getReturnQuantity()) != 0d)
                    mDataset.get(this.position).setErrorQuantity(null);
                mDataset.get(this.position).setErrorQuantity(null);
                Double total = Double.parseDouble(mDataset.get(this.position).getReturnQuantity()) * Double.parseDouble(mDataset.get(this.position).getPrice());
                //formatPrice = decimalFormat.format(total);
                mDataset.get(this.position).setValue(String.valueOf(total));
                // triggering the callback function to update the total
                textCallBackListener.updateText(value, String.valueOf(total));
            } else {
                mDataset.get(this.position).setValue("");
                textCallBackListener.updateText(value, "");
            }
            mListener.onClick(v.returnQuantity, this.position);
        }

        @Override
        public void afterTextChanged(Editable editable) {


        }

    }


}