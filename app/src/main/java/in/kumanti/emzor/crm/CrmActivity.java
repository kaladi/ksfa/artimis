package in.kumanti.emzor.crm;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.ArtifactsActivity;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.GetLocationActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.activity.OrderGalleryActivity;
import in.kumanti.emzor.adapter.CrmPurchaseHistoryAdapter;
import in.kumanti.emzor.eloquent.CrmDetailsTable;
import in.kumanti.emzor.eloquent.CrmNewProductsTable;
import in.kumanti.emzor.eloquent.CrmProductsTable;
import in.kumanti.emzor.eloquent.CrmStatusTable;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.model.CrmNewProducts;
import in.kumanti.emzor.model.CrmProducts;
import in.kumanti.emzor.model.CrmPurchaseHisProd;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.FileUtils;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.SharedPreferenceManager;

public class CrmActivity extends MainActivity implements AdapterView.OnItemSelectedListener{

    private static final int SELECT_PHOTO1 = 1;
    private static final int CAPTURE_PHOTO1 = 2;

    Bitmap selectedImage = null;
    int image_id = 0;

    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionbarTitle, marqueeText, customerName, accountNo, city, checkInTime;
    Chronometer duration;

    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1, marqueeTextString;
    String login_id = "", checkin_time = "", customerCode = "", productQuantityString,crmNewProductNumber="",crmNewProductRandomNumber="";

    SharedPreferenceManager sharedPreferenceManager;
    MyDBHandler dbHandler;

    TextView crmNumber, currency, crmDate, crmNoStatus, crmNoStatusDate, crmProdName, crmProdValue, crmProdCount, crmProdTotalBudget, crmProdTotalValue ;
    CustomSearchableSpinner crmNoSpinner, crmProdSpinner, uomSpinner, newProdBatchSp, newProdTypeSp, categorySp;
    ImageButton generateCrmNoButton, createNewCrmProdButton;

    EditText crmDescription, crmProdQty, crmProdPrice, crmProdBudget, newProdName, newProdDesc, newProdCategory;
    Button detailsButton, addProdButton, viewCrmButton, artifactsButton;

    ImageView showProdsGallery, captureProdImage;

    double productPriceDouble = 0.00, productValueDouble = 0.00, unformattedValueDouble = 0.00;

    DecimalFormat valueFormatter, quantityFormatter;

    ProductTable productTable;
    CrmTable crmTable;
    CrmProductsTable crmProductsTable;
    CrmNewProductsTable crmNewProductsTable;
    CrmDetailsTable crmDetailsTable;
    CrmStatusTable crmStatusTable;
    LpoImagesTable lpoImagesTable;
    ArrayList<Crm> crmArrayList;
    ArrayAdapter<Crm> crmSpinnerArrayAdapter;

    ArrayList<CrmProducts> crmProductsArrayList;
    ArrayList<CrmPurchaseHisProd> crmPurchaseHisProdArrayList;

    ArrayList<in.kumanti.emzor.model.Product> productsArrayList;
    ArrayList<CrmNewProducts> tempProductsArrayList;
    ArrayAdapter<in.kumanti.emzor.model.Product> crmProductSpinnerArrayAdapter;

    ArrayList<CrmNewProducts> crmNewProductsArrayList;

    RecyclerView rvCreateCrmPurchaseHis;
    RecyclerView.Adapter crmPurchaseHisAdapter;
    RecyclerView.LayoutManager crmPurchaseHisProdLayoutManager;

    String crmIdString = null, crmNumberString = "",crmStatusString = "", customerTypeString = "", storeCrmDate = "", productCodeString = "", productNameString = "", productId = "-1", productPrice = "";
    ArrayList<String> typeList;
    int crm_random_num = 0,random_num=0;

    int randomNumberInteger = 0, orderId = 0, order_line_id = 0, numRows1 = 0, numbRows = 0, serial_num = 0;

    String uomString,newProductTypeString,newProductBatchString, newProdCategoryString;
    GeneralSettings gs;
    GeneralSettingsTable generalSettingsTable;


    GPSTracker gpsTracker;
    double latitudeDouble, longitudeDouble;
    String latitude, longitude, latitudeLongitude;
    DecimalFormat formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_crm);

        initializeViews();
        populateHeaderDetails();
        getFooterDateTime();
//        getCustomerLocation();

        actionbarTitle.setText(R.string.crm_header);
        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        crmProductsArrayList = new ArrayList<>();

        setCrmDate();
        loadCrmNo();
        loadCrmStatusInfo();
        loadCrmProducts();
        generateCrmSeqNumber();
        initializePurchaseRv();

        crmNumberString = crmNumber.getText().toString();

        /*
         * Displays the customer geo location on long press of the customer image.
         */
        customerImage.setOnLongClickListener(v -> {
            displaysCustomerLocation();
            return true;
        });

        detailsButton.setOnClickListener(v -> {

            if(productNameString.equals("Select")) {
                Toast.makeText(CrmActivity.this, "Select Product to View Details", Toast.LENGTH_SHORT).show();
            } else  {
                Intent notesIntent = new Intent(getApplicationContext(), CrmDetailsActivity.class);
                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("login_id", login_id);
                bundle.putString("customer_id", customerCode);
                bundle.putString("productCode", productCodeString);

                Log.d("Crm--", "Crm Product Code: " +productCodeString);

                notesIntent.putExtras(bundle);
                startActivity(notesIntent);
            }

        });

        crmProdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                Log.d("Products", "Selected Product" + pos);
                in.kumanti.emzor.model.Product p = (in.kumanti.emzor.model.Product) adapterView.getItemAtPosition(pos);
                Log.d("Products", "Selected Product" + pos + p.product_name);

                productId = p.product_id;
                productNameString = p.product_name;
                productPrice = p.product_price;
                productCodeString = p.product_code;

                if(productNameString.equals("Select")) {
                    crmPurchaseHisProdArrayList = new ArrayList<>();
                    if(crmPurchaseHisAdapter != null)
                        crmPurchaseHisAdapter.notifyDataSetChanged();

                    resetViews();
                } else {
                    renderProductInfo();
                    bindPurchaseHisAdapter();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });


        /*
         * addTextChangedListener for the selected product quantity EditText
         */
        crmProdQty.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                calculateProdValue();
            }
        });


        crmProdPrice.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                calculateProdValue();
            }
        });


        crmProdBudget.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {


            }
        });


        addProdButton.setOnClickListener(v -> {
            boolean isValid = true;
            if(productNameString.equals("Select")) {
                Toast.makeText(CrmActivity.this, "Select Product to Add", Toast.LENGTH_SHORT).show();
                isValid = false;
            }

            if (TextUtils.isEmpty(crmProdQty.getText().toString())) {
                crmProdQty.setError(getString(R.string.error_field_required));
                isValid = false;
            } else if ("0".equals(crmProdQty.getText().toString())) {
                crmProdQty.setError(getString(R.string.Zero_Validation));
                isValid = false;
            }else{
                crmProdQty.setError(null);
            }


//            if (TextUtils.isEmpty(crmProdBudget.getText().toString())) {
//                crmProdBudget.setError(getString(R.string.error_field_required));
//                isValid = false;
//            } else if ("0".equals(crmProdBudget.getText().toString())) {
//                crmProdBudget.setError(getString(R.string.Zero_Validation));
//                isValid = false;
//            }else{
//                crmProdBudget.setError(null);
//            }


//            if (TextUtils.isEmpty(crmProdPrice.getText().toString())) {
//                crmProdPrice.setError(getString(R.string.error_field_required));
//                isValid = false;
//            } else if ("0".equals(crmProdPrice.getText().toString())) {
//                crmProdPrice.setError(getString(R.string.Zero_Validation));
//                isValid = false;
//            }else{
//                crmProdPrice.setError(null);
//            }

            if(isValid)
                addCrmProducts();
        });

        createNewCrmProdButton.setOnClickListener(v -> createNewCrmProductInfo());

        viewCrmButton.setOnClickListener(v -> {
            if(crmProductsArrayList.size() == 0) {
                Toast.makeText(CrmActivity.this,"Please add atleast one product to View CRM", Toast.LENGTH_SHORT).show();
                return;
            }
            navigateToViewCrm();
        });

        artifactsButton.setOnClickListener(v -> {

            if(productNameString.equals("Select")) {
                Toast.makeText(CrmActivity.this, "Select Product to View Artifacts", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(getApplicationContext(), ArtifactsActivity.class);
                long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                duration.stop();
                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("login_id", login_id);
                bundle.putString("customer_id", customerCode);
                bundle.putString("productCode", productCodeString);

                intent.putExtras(bundle);
                startActivity(intent);
            }

        });

        actionbarBackButton.setOnClickListener(v -> {
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();
            finish();
        });

        deviceInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);
        });


    }

//    private void getCustomerLocation() {
//
//        gpsTracker = new GPSTracker(this);
//
//        if (gpsTracker.canGetLocation()) {
//            latitudeDouble = gpsTracker.getLatitude();
//            longitudeDouble = gpsTracker.getLongitude();
//
//            latitude = formatter.format(latitudeDouble);
//            longitude = formatter.format(longitudeDouble);
//
//            latitudeLongitude = latitude + "," + longitude;
//        } else {
//            gpsTracker.showSettingsAlert();
//        }
//    }

    public void bindPurchaseHisAdapter() {
        crmPurchaseHisProdArrayList = dbHandler.getPurchaseHistoryForCrm(customerCode, productCodeString);

        if(crmProductsArrayList != null) {
            crmPurchaseHisAdapter = new CrmPurchaseHistoryAdapter(crmPurchaseHisProdArrayList);
            rvCreateCrmPurchaseHis.setAdapter(crmPurchaseHisAdapter);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d("Crm***", "onResume: ");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + sharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d("Crm***", "onPause: ");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + sharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Crm***", "onRestart: ");

    }

    public void navigateToViewCrm() {
        Intent viewCrmIntent = new Intent(getApplicationContext(), ViewCrmActivity.class);

        long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
        sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
        duration.stop();


        //Create the bundle
        Bundle bundle = new Bundle();
        bundle.putString("checkin_time", checkin_time);
        bundle.putString("customer_id", customerCode);
        bundle.putString("login_id", login_id);
        bundle.putString("crmNo", crmNumber.getText().toString());
        bundle.putString("crmStatus", crmNoStatus.getText().toString());
        bundle.putString("crmStatusDate", crmNoStatusDate.getText().toString());
        bundle.putString("randomNumber", String.valueOf(crm_random_num));
        bundle.putString("description", crmDescription.getText().toString());

        Log.d("Crm--", "crmStatus: " +crmNoStatus.getText().toString());
        Log.d("Crm--", "crmStatusDate: " +crmNoStatusDate.getText().toString());

        viewCrmIntent.putExtra("crmProdList", crmProductsArrayList);
        viewCrmIntent.putExtras(bundle);
        startActivityForResult(viewCrmIntent,99);
    }
    public void updateProductSpinner(){

    }
    public void addCrmProducts() {

        CrmProducts crmProducts = new CrmProducts();
        crmProducts.setCrmNumber(crmNumber.getText().toString());
        crmProducts.setProductCode(productCodeString);
        crmProducts.setProductName(crmProdName.getText().toString());
        crmProducts.setQuantity(crmProdQty.getText().toString());
        crmProducts.setBudget(crmProdBudget.getText().toString());
        crmProducts.setUnitPrice(crmProdPrice.getText().toString());
        crmProducts.setValue(String.valueOf(unformattedValueDouble));

        crmProductsArrayList.add(crmProducts);

        Log.d("Crm--", "Adding Pro: " +crmProductsArrayList.size());
        Log.d("Crm--", "crmProdBudget: " +crmProdBudget.getText().toString());
        Log.d("Crm--", "crmProdValue: " +crmProdValue.getText().toString());

        resetViews();
        calculateCrmValues();
        loadCrmProducts();
    }

    public void calculateProdValue() {
        String prodQty = crmProdQty.getText().toString();
        String prodPrice = crmProdPrice.getText().toString();

        if(!prodQty.isEmpty() && !prodPrice.isEmpty() ) {
            int quantityInt = Integer.parseInt(prodQty);
            double priceDouble = Double.parseDouble(prodPrice);

            unformattedValueDouble = quantityInt * priceDouble;
            String formattedTotalValue = valueFormatter.format(unformattedValueDouble);

            crmProdValue.setText(formattedTotalValue);
        }
        else{
            unformattedValueDouble = 0;
            crmProdValue.setText("0.00");
        }
        calculateCrmValues();
    }

    public void calculateCrmValues() {
        int crmProductCount = crmProductsArrayList.size();
        crmProdCount.setText(String.valueOf(crmProductCount));

        double budgetDouble = 0.0, valueDouble = 0.0;

        for(CrmProducts crmProduct :crmProductsArrayList) {
            budgetDouble += crmProduct.getBudget() != null && !crmProduct.getBudget().isEmpty() ? Double.parseDouble(crmProduct.getBudget()) : 0.0;
            valueDouble  += crmProduct.getValue() != null && !crmProduct.getValue().isEmpty() ? Double.parseDouble(crmProduct.getValue()) : 0.0;
            //budgetDouble += Double.parseDouble(crmProduct.getBudget());
            //valueDouble += Double.parseDouble(crmProduct.getValue());
            Log.d("Crm--", "crmProdValue & Budget:  " +crmProduct.getValue()+" "+crmProduct.getBudget());
        }

        String formattedBudgetValue = valueFormatter.format(budgetDouble);
        String formattedTotalValue = valueFormatter.format(valueDouble);

        crmProdTotalBudget.setText(formattedBudgetValue);
        crmProdTotalValue.setText(formattedTotalValue);
    }

    public void loadCrmProducts() {

        if(customerTypeString.equals("Prospect"))
            productsArrayList = productTable.getProducts();
        else
            productsArrayList = productTable.getCrmProducts(customerCode);

        crmProdSpinner.setTitle("Select Product");
        crmProdSpinner.setPositiveButton("OK");

        tempProductsArrayList = crmNewProductsTable.getProductsByCustomer(customerCode);
        ArrayList<in.kumanti.emzor.model.Product> newProductList = new ArrayList<>();
        for(in.kumanti.emzor.model.Product product: productsArrayList) {
            boolean isPresent = false;
            for (CrmProducts crmProduct : crmProductsArrayList) {
                if (product.product_code.equals(crmProduct.productCode)) {
                    isPresent = true;
                    break;
                }
            }
            if (!isPresent)
                newProductList.add(product);
        }

        for(CrmNewProducts product: tempProductsArrayList) {
            boolean isPresent = false;
            for (CrmProducts crmProduct : crmProductsArrayList) {
                if(product.getProductCode().equals(crmProduct.productCode))
                {
                    isPresent = true;
                    break;
                }
            }
            if(!isPresent) {
                in.kumanti.emzor.model.Product p = new Product();
                p.setProduct_code(product.getProductCode());
                p.setCustomer_id(product.getCustomerCode());
                p.setProduct_name(product.getProductName());
                p.setBatch_controlled(product.getBatch());
                p.setProduct_type(product.getType());
                p.setProduct_uom(product.getUom());
                newProductList.add(p);
            }

        }

        in.kumanti.emzor.model.Product product = new Product();
        product.setProduct_id("-1");
        product.setProduct_name("Select");
        newProductList.add(0, product);

        crmProductSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, newProductList);
        crmProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        crmProdSpinner.setAdapter(crmProductSpinnerArrayAdapter);
    }

    private void renderProductInfo() {
        crmProdName.setText(productNameString);
        crmProdPrice.setText(productPrice);
    }

    /*
     * Functionality of setOnItemSelectedListener of selected product.
     */
    private void resetViews() {
        crmProdSpinner.setSelection(0);
        crmProdName.setText("");
        crmProdQty.setText("");
        crmProdBudget.setText("");
        crmProdPrice.setText("");
        crmProdValue.setText("");
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        crmNumber = findViewById(R.id.createCrmNumberTv);
        currency = findViewById(R.id.createCrmCurrencyTv);
        crmDate = findViewById(R.id.createCrmDateTv);

        crmNoSpinner = findViewById(R.id.createCrmNoSpinner);
        generateCrmNoButton = findViewById(R.id.createCrmNoAddBt);

        crmNoStatus = findViewById(R.id.createCrmStatusSpinner);
        crmNoStatusDate = findViewById(R.id.viewCrmStatusDateTv);

        crmDescription = findViewById(R.id.createCrmDescriptionEt);
        detailsButton = findViewById(R.id.createCrmDetailsButton);

        crmProdSpinner = findViewById(R.id.createCrmProdSpinner);
        createNewCrmProdButton = findViewById(R.id.createCrmProductAddBt);

        crmProdName = findViewById(R.id.createCrmProdNameTv);
        crmProdQty = findViewById(R.id.createCrmProdQuantityEt);
        crmProdBudget = findViewById(R.id.createCrmProdBudgetEt);
        crmProdPrice = findViewById(R.id.createCrmProdPriceTv);
        crmProdValue = findViewById(R.id.createCrmProdValueTv);

        addProdButton = findViewById(R.id.createCrmAddProdButton);
        viewCrmButton = findViewById(R.id.createCrmViewProdButton);
        artifactsButton = findViewById(R.id.createCrmArtifactsBt);
        rvCreateCrmPurchaseHis = findViewById(R.id.createCrmPurchaseHisRv);

        crmProdCount = findViewById(R.id.createCrmTotalProdCountTv);
        crmProdTotalBudget = findViewById(R.id.createCrmTotalBudgetTv);
        crmProdTotalValue = findViewById(R.id.createCrmTotalValueTv);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        productTable = new ProductTable(getApplicationContext());
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());

        crmTable = new CrmTable(getApplicationContext());
        crmProductsTable = new CrmProductsTable(getApplicationContext());
        crmNewProductsTable = new CrmNewProductsTable(getApplicationContext());
        crmDetailsTable = new CrmDetailsTable(getApplicationContext());
        crmStatusTable = new CrmStatusTable(getApplicationContext());
        lpoImagesTable = new LpoImagesTable(getApplicationContext());
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        valueFormatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");

    }


    private void loadCrmStatusInfo() {
        crmNoStatus.setText(R.string.new_opportunity);
        if(crmNoSpinner != null) {
            crmNoStatusDate.setText(crmDate.getText().toString());
            crmNoStatus.setText(R.string.new_opportunity);
        }
    }

    private void loadCrmNo() {

        Log.d("Crm--", "loadCrmNo:" );

        crmArrayList = crmTable.getCRMNumber(customerCode);

        crmProdSpinner.setTitle("Select CRM Number");
        crmProdSpinner.setPositiveButton("OK");

        Crm crm = new Crm();
        crm.setCrmId(-1);
        crm.setCrmNumber("Select");
        crmArrayList.add(0, crm);

        ArrayAdapter<Crm> spinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, crmArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        crmNoSpinner.setAdapter(spinnerArrayAdapter);

        crmNoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                crmNoSpinner.isSpinnerDialogOpen = false;
                Crm crm = (Crm) adapterView.getItemAtPosition(pos);
                crmNumberString = crm.getCrmNumber();

                Log.d("Crm--", "setOnItemSelectedListener: No " +crmNumberString);

                Log.d("Crm--", "setOnItemSelectedListener crmStatus: " +crmNoStatus.getText().toString());



                if(crmNumberString != null && !crmNumberString.equals("Select")) {
                    crmNumber.setText(crmNumberString);
                    crmNoStatus.setText(crm.getStatus());

                    Log.d("Crm--", "existing crm No " +crmNumberString);
                    Log.d("Crm--", "existing crm no crmStatus: " +crmNoStatus.getText().toString());


                    crmProductsArrayList = crmProductsTable.getCrmProducts(crmNumberString);
                    navigateToViewCrm();
                } else {
                    loadCrmStatusInfo();

                    generateCrmSeqNumber();
                    crmProductsArrayList = new ArrayList<>();
                }

                Log.d("Crm--", "before No " +crmNumberString);

                loadCrmProducts();
                calculateCrmValues();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                crmNoSpinner.isSpinnerDialogOpen = false;
            }
        });

    }

    /*
     * Displays the Stock Product Information for the selected product.
     */
    public void createNewCrmProductInfo() {
        generateNewProductCode();

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_create_crm_product, null);

        newProdName = alertLayout.findViewById(R.id.crmNewProdNameEt);
        newProdDesc = alertLayout.findViewById(R.id.crmNewProdDesEt);
        uomSpinner = alertLayout.findViewById(R.id.crmNewProdUomSp);
        newProdTypeSp = alertLayout.findViewById(R.id.crmNewProdTypeSp);
        newProdBatchSp = alertLayout.findViewById(R.id.crmNewProdBatchSp);
        categorySp = alertLayout.findViewById(R.id.crmNewProdCategorySp);

        showProdsGallery = alertLayout.findViewById(R.id.crmShowProdGalleryIv);
        captureProdImage = alertLayout.findViewById(R.id.crmCaptureProdIv);

        newProdName.setText(newProdName.getText().toString());

        setUomSpinner();

        setNewProdTypeSpinner();

        setNewProdBatchSpinner();

        setCategorySpinner();

        captureProdImage.setOnClickListener(v -> {
            Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent1, CAPTURE_PHOTO1);

        });

        showProdsGallery.setOnClickListener(v -> {
            int lpo_image_count = lpoImagesTable.get_lpo_order_image_count(crmNewProductNumber);

            if (lpo_image_count == 0) {
                Toast.makeText(CrmActivity.this, "No Images,Capture and Check again.", Toast.LENGTH_SHORT).show();
            } else {
                Intent myintent = new Intent(getApplicationContext(), OrderGalleryActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("customer_id", customerCode);
                bundle.putString("order_number", crmNewProductNumber);
                bundle.putString("imageType", "Product");

                myintent.putExtras(bundle);
                startActivity(myintent);
            }


        });
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("CLOSE", (dialog, which) -> {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        });

        alert.setPositiveButton("SAVE", (dialog, which) -> {

            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                CrmNewProducts crmNewProducts = new CrmNewProducts();
                crmNewProducts.setCustomerCode(customerCode);
                crmNewProducts.setProductCode(crmNewProductNumber);
                crmNewProducts.setProductName(newProdName.getText().toString());
                crmNewProducts.setDescription(newProdDesc.getText().toString());
                crmNewProducts.setUom(uomString);
                crmNewProducts.setType(newProductTypeString);
                crmNewProducts.setBatch(newProductBatchString);
                crmNewProducts.setCategory(newProdCategoryString);
                crmNewProducts.setRandomNo(crmNewProductRandomNumber);
                crmNewProductsTable.create(crmNewProducts);
                loadCrmProducts();

        });
        AlertDialog dialog = alert.create();
        dialog.show();

    }

    public void setCategorySpinner() {

        String[] uomArrayList = {"Portable X-Ray", " Power Drill Systems"};

        categorySp.setAdapter(new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, uomArrayList));

        categorySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                newProdCategoryString = adapterView.getItemAtPosition(pos).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

    }

    public void setUomSpinner() {

        String[] uomArrayList = {"PK"};

        uomSpinner.setAdapter(new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, uomArrayList));

        uomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                uomString = adapterView.getItemAtPosition(pos).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

    }

    public void setNewProdTypeSpinner() {

        String[] newProductTypeArrayList = {"Finished Goods", "Services"};

        newProdTypeSp.setAdapter(new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, newProductTypeArrayList));

        newProdTypeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                newProductTypeString = adapterView.getItemAtPosition(pos).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

    }

    public void setNewProdBatchSpinner() {

        String[] newProductBatchArrayList = {"Yes", "No"};

        newProdBatchSp.setAdapter(new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, newProductBatchArrayList));

        newProdBatchSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                newProductBatchString = adapterView.getItemAtPosition(pos).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

    }





    public void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            checkin_time = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            crmNumberString = bundle.getString("crmNo");
            customerTypeString = bundle.getString("customerType");

            Log.d("Crm--", "Crm Customer Code: " +customerCode+" loginId"+login_id);

        }

        checkInTime.setText(checkin_time);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + sharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        //Populate Customer Information Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        Log.d("Crm--", "Crm customerDetails : " +customerCode);

        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));

                String cityVal = customerDetails.getString(9);
                String stateVal = customerDetails.getString(10);
                if(cityVal != null || stateVal != null) {
                    city.setText(String.format("%s-%s", cityVal, stateVal));
                } else {
                    city.setText("");
                }

                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

            }
        }

        getCurrencyFromSettings();

    }

    public void getCurrencyFromSettings() {
        gs = generalSettingsTable.getSettingByKey("currency");
        if(gs != null) {
            currency.setText(gs.getValue());
        }
    }

    /*
     * Displays the current check-in customer geo location.
     */
    public void displaysCustomerLocation() {
        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }



    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    private void setCrmDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        crmDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        storeCrmDate = timeStampFormat.format(myDate);

    }

    /*
     * Setting the footer date and time of the page.
     */
    public void getFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String date = FileUtils.getMonthYear();
        datetime.setText(date);

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            String dateString = FileUtils.getHourMin();
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException ignored) {
                }
            }
        };
        t.start();

    }


    private void generateCrmSeqNumber() {

        crm_random_num = crmTable.getRandomNumber();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        crm_random_num = crm_random_num + 1;

        Log.d("Crm---", "Random No---" + crm_random_num);

        if (crm_random_num <= 9) {
            crmNumber.setText(Constants.TAB_CODE + "CRM" + rec_seq_date + "00" + crm_random_num);

        } else if (crm_random_num > 9 & crm_random_num < 99) {
            crmNumber.setText(Constants.TAB_CODE + "CRM" + rec_seq_date + "0" + crm_random_num);

        } else {
            crmNumber.setText(Constants.TAB_CODE + "CRM" + rec_seq_date + crm_random_num);
        }

        crmIdString = crmNumber.getText().toString();

    }

    public void initializePurchaseRv() {
        rvCreateCrmPurchaseHis.setHasFixedSize(true);
        crmPurchaseHisProdLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCreateCrmPurchaseHis.setLayoutManager(crmPurchaseHisProdLayoutManager);
    }

    private void generateNewProductCode() {

        random_num = crmNewProductsTable.getRandomNumber();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;
        crmNewProductRandomNumber = String.valueOf(random_num);
        Log.d("Crm---", "Random No---" + random_num);

        if (random_num <= 9) {
            crmNewProductNumber = Constants.TAB_CODE + Constants.SR_CODE+ "CRM_PRO" + rec_seq_date + "00" + random_num;

        } else if (random_num > 9 & random_num < 99) {
            crmNewProductNumber = Constants.TAB_CODE + Constants.SR_CODE+ "CRM_PRO" + rec_seq_date + "0" + random_num;

        } else {
            crmNewProductNumber = Constants.TAB_CODE + Constants.SR_CODE+ "CRM_PRO" + rec_seq_date + random_num;
        }


    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_PHOTO1) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    selectedImage = getResizedBitmap(selectedImage, 50);
                    customerImage.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                }
            }

        } else if (requestCode == CAPTURE_PHOTO1) {
            if (resultCode == RESULT_OK) {

                image_id = lpoImagesTable.get_lpo_gallery_image_id();
                image_id = image_id + 1;
                String imagename = "customer_image" + image_id;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                String fileName = "crm_new_product_" + crmNewProductNumber + "_" + image_id + ".png";
                fileUtils.storeImage(photo, fileName, orderGalleryImageFolderPath);

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                LPOImages lpoImages = new LPOImages();
                lpoImages.setOrderCode(crmNewProductNumber);
                lpoImages.setLpoImageName(fileName);
                lpoImages.setImageType("Product");
                lpoImages.setLpoImagePath(orderGalleryImageFolderPath + File.separator + fileName);
                lpoImages.setVisitSeqNumber(visitSequence);
                lpoImagesTable.create(lpoImages);
            }
        }else if(requestCode == 99){
            try{
                crmProductsArrayList =  (ArrayList<CrmProducts>) data.getSerializableExtra("crmProdList");
            }
            catch (Exception e){

            }
            loadCrmProducts();
            calculateCrmValues();
//            Log.d("ResultReceived","Rs"+resultCode);
//            if(resultCode == RESULT_CANCELED)
//                finish();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}
