package in.kumanti.emzor.model;

public class ExpiredProductsReport {

    public int expiredProdId;
    public String productCode;
    public String productName;
    public String batchName;
    public String expiredBatchDate;
    public String expiredQty;
    public String quarantineDate;
    public String quarantineQty;
    public String createdAt;
    public String updatedAt;

    public int getExpiredProdId() {
        return expiredProdId;
    }

    public void setExpiredProdId(int expiredProdId) {
        this.expiredProdId = expiredProdId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }


    public String getExpiredBatchDate() {
        return expiredBatchDate;
    }

    public void setExpiredBatchDate(String expiredBatchDate) {
        this.expiredBatchDate = expiredBatchDate;
    }

    public String getExpiredQty() {
        return expiredQty;
    }

    public void setExpiredQty(String expiredQty) {
        this.expiredQty = expiredQty;
    }

    public String getQuarantineDate() {
        return quarantineDate;
    }

    public void setQuarantineDate(String quarantineDate) {
        this.quarantineDate = quarantineDate;
    }

    public String getQuarantineQty() {
        return quarantineQty;
    }

    public void setQuarantineQty(String quarantineQty) {
        this.quarantineQty = quarantineQty;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
