package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.CrmProductGroup;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_PRODUCT_GROUP;

public class CrmProductGroupTable extends SQLiteOpenHelper {

    private static final String COLUMN_CRM_PRODUCT_GROUP_ID = "productGroupId";
    private static final String COLUMN_CRM_PRODUCT_GROUP_CODE = "groupCode";
    private static final String COLUMN_CRM_PRODUCT_GROUP_NAME = "groupName";
    private static final String COLUMN_CRM_PRODUCT_SHORT_NAME = "shortName";
    private static final String COLUMN_CRM_PRODUCT_GROUP_DESCRIPTION = "description";
    private static final String COLUMN_PRODUCT_GROUP_CREATED_AT = "createdAt";
    private static final String COLUMN_PRODUCT_GROUP_UPDATED_AT = "updatedAt";


    public Context context;

    public CrmProductGroupTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_PRODUCT_GROUP);

        String productGroupTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_PRODUCT_GROUP + "(" +
                COLUMN_CRM_PRODUCT_GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CRM_PRODUCT_GROUP_CODE + " TEXT, " +
                COLUMN_CRM_PRODUCT_GROUP_NAME + " TEXT, " +
                COLUMN_CRM_PRODUCT_SHORT_NAME + " TEXT, " +
                COLUMN_CRM_PRODUCT_GROUP_DESCRIPTION + " TEXT, " +
                COLUMN_PRODUCT_GROUP_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_PRODUCT_GROUP_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";


        sqlWrite.execSQL(productGroupTable);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmProductGroup productGroup) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CRM_PRODUCT_GROUP_CODE, productGroup.getGroupCode());
        values.put(COLUMN_CRM_PRODUCT_GROUP_NAME, productGroup.getGroupName());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_PRODUCT_GROUP, null, values);
        db.close();
        return id;
    }


    public ArrayList<CrmProductGroup> getProductGroups() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_PRODUCT_GROUP;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("Data", "Get Product Groups  ");
        ArrayList<CrmProductGroup> crmProductGroups = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmProductGroup crmProductGroup = new CrmProductGroup();
            // Log.d("MYDATA","Data  id" + cursor.getInt(0));
            crmProductGroup.setProductGroupId(cursor.getInt(0));
            crmProductGroup.setGroupCode(cursor.getString(1));
            crmProductGroup.setGroupName(cursor.getString(2));


            crmProductGroups.add(crmProductGroup);
        }
        cursor.close();
        sqlWrite.close();
        return crmProductGroups;
    }


}
