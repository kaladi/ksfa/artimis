package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CrmColdCall;

public class CrmColdCallHistoryAdapter extends RecyclerView.Adapter<CrmColdCallHistoryAdapter.ViewHolder> {

    public ArrayList<CrmColdCall> mDataset;
    public Context context;

    public CrmColdCallHistoryAdapter(ArrayList<CrmColdCall> myDataset) {
        mDataset = myDataset;
    }

    public void add(CrmColdCall item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmColdCall item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public CrmColdCallHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_crm_coldcall_history, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        String personsMet = mDataset.get(position).getPersonsMet1();
        if (!TextUtils.isEmpty(mDataset.get(position).getPersonsMet2())) {
            personsMet += ", \n" + mDataset.get(position).getPersonsMet2();
        }
        viewHolder.date.setText(mDataset.get(position).getColdCallDate());
        viewHolder.personsMet.setText(personsMet);
        viewHolder.outcomeType.setText(mDataset.get(position).getOutcomeType());
        viewHolder.description.setText(mDataset.get(position).getOutcomeDescription());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date, personsMet, outcomeType, description;

        public ViewHolder(View v) {
            super(v);

            date = v.findViewById(R.id.coldHisCallDateTv);
            personsMet = v.findViewById(R.id.coldCallHisPersonsMetTv);
            outcomeType = v.findViewById(R.id.coldCallHisOutcomeTypeTv);
            description = v.findViewById(R.id.coldCallHisOutcomeDescriptionTv);
        }
    }


}