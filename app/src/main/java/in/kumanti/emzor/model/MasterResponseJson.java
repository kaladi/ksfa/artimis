package in.kumanti.emzor.model;

import com.google.gson.JsonArray;

public class MasterResponseJson {

    public String msg;
    public String status;
    public JsonArray customer;
    public JsonArray Product;
    public JsonArray competitor_product;
    public JsonArray price_header;
    public JsonArray price_line;
    public JsonArray shipping_address;
    public JsonArray journey_plan;
    public JsonArray survey_header;
    public JsonArray survey_line;
    public JsonArray warehouse_stock;
    public JsonArray customer_ledgerr;
    public JsonArray feedback;
    public JsonArray message;
    public JsonArray customer_aging;
    public JsonArray location;
    public JsonArray sr_invoice;
    public JsonArray order_status_update;
    public JsonArray distributors;
    public JsonArray sr_pricelist;
    public JsonArray settings;
    public JsonArray artifact;
    public JsonArray survey_option;
    public JsonArray sr_master_bde;
    public JsonArray journeyPlanStatus;
    public JsonArray target;
    public JsonArray expense;
    public JsonArray expense_type;
    public JsonArray prstatus;
    public JsonArray secstatus;
    public JsonArray sr_invoice_batch;

    public MasterResponseJson(String msg, String status, JsonArray customer, JsonArray product, JsonArray competitor_product, JsonArray price_header, JsonArray price_line, JsonArray shipping_address, JsonArray journey_plan, JsonArray survey_header, JsonArray survey_line, JsonArray warehouse_stock, JsonArray customer_ledgerr, JsonArray feedback, JsonArray message, JsonArray customer_aging, JsonArray location, JsonArray sr_invoice, JsonArray order_status_update, JsonArray distributors, JsonArray sr_pricelist, JsonArray settings, JsonArray artifact, JsonArray sr_master_bde, JsonArray journeyPlanStatus,JsonArray target,JsonArray expense,JsonArray expense_type,JsonArray prstatus,JsonArray secstatus,JsonArray sr_invoice_batch) {

        this.msg = msg;
        this.status = status;
        this.customer = customer;
        this.Product = product;
        this.competitor_product = competitor_product;
        this.price_header = price_header;
        this.price_line = price_line;
        this.shipping_address = shipping_address;
        this.journey_plan = journey_plan;
        this.survey_header = survey_header;
        this.survey_line = survey_line;
        this.warehouse_stock = warehouse_stock;
        this.customer_ledgerr = customer_ledgerr;
        this.feedback = feedback;
        this.message = message;
        this.customer_aging = customer_aging;
        this.location = location;
        this.sr_invoice = sr_invoice;
        this.order_status_update = order_status_update;
        this.distributors = distributors;
        this.sr_pricelist = sr_pricelist;
        this.settings = settings;
        this.artifact = artifact;
        this.sr_master_bde = sr_master_bde;
        this.journeyPlanStatus = journeyPlanStatus;
        this.target = target;
        this.expense = expense;
        this.expense_type = expense_type;
        this.prstatus = prstatus;
        this.secstatus = secstatus;
        this.sr_invoice_batch = sr_invoice_batch;


    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JsonArray getCustomer() {
        return customer;
    }

    public void setCustomer(JsonArray customer) {
        this.customer = customer;
    }

    public JsonArray getProduct() {
        return Product;
    }

    public void setProduct(JsonArray product) {
        this.Product = product;
    }

    public JsonArray getCompetitor_product() {
        return competitor_product;
    }

    public void setCompetitor_product(JsonArray competitor_product) {
        this.competitor_product = competitor_product;
    }

    public JsonArray getPrice_header() {
        return price_header;
    }

    public void setPrice_header(JsonArray price_header) {
        this.price_header = price_header;
    }

    public JsonArray getPrice_line() {
        return price_line;
    }

    public void setPrice_line(JsonArray price_line) {
        this.price_line = price_line;
    }

    public JsonArray getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(JsonArray shipping_address) {
        this.shipping_address = shipping_address;
    }

    public JsonArray getJourney_plan() {
        return journey_plan;
    }

    public void setJourney_plan(JsonArray journey_plan) {
        this.journey_plan = journey_plan;
    }

    public JsonArray getSurvey_header() {
        return survey_header;
    }

    public void setSurvey_header(JsonArray survey_header) {
        this.survey_header = survey_header;
    }

    public JsonArray getSurvey_line() {
        return survey_line;
    }

    public void setSurvey_line(JsonArray survey_line) {
        this.survey_line = survey_line;
    }

    public JsonArray getWarehouse_stock() {
        return warehouse_stock;
    }

    public void setWarehouse_stock(JsonArray warehouse_stock) {
        this.warehouse_stock = warehouse_stock;
    }

    public JsonArray getCustomer_ledgerr() {
        return customer_ledgerr;
    }

    public void setCustomer_ledgerr(JsonArray customer_ledgerr) {
        this.customer_ledgerr = customer_ledgerr;
    }

    public JsonArray getFeedback() {
        return feedback;
    }

    public void setFeedback(JsonArray feedback) {
        this.feedback = feedback;
    }

    public JsonArray getMessage() {
        return message;
    }

    public void setMessage(JsonArray message) {
        this.message = message;
    }

    public JsonArray getCustomer_aging() {
        return customer_aging;
    }

    public void setCustomer_aging(JsonArray customer_aging) {
        this.customer_aging = customer_aging;
    }

    public JsonArray getLocation() {
        return location;
    }

    public void setLocation(JsonArray sr_invoice) {
        this.sr_invoice = sr_invoice;
    }

    public JsonArray getSr_invoice() {
        return sr_invoice;
    }

    public void setSr_invoice(JsonArray sr_invoice) {
        this.sr_invoice = sr_invoice;
    }

    public JsonArray getOrder_status_update() {
        return order_status_update;
    }

    public void setOrder_status_update(JsonArray order_status_update) {
        this.order_status_update = order_status_update;
    }

    public JsonArray getDistributors() {
        return distributors;
    }

    public void setDistributors(JsonArray distributors) {
        this.distributors = distributors;
    }

    public JsonArray getSr_pricelist() {
        return sr_pricelist;
    }

    public void setSr_pricelist(JsonArray sr_pricelist) {
        this.sr_pricelist = sr_pricelist;
    }

    public JsonArray getSettings() {
        return settings;
    }

    public void setSettings(JsonArray settings) {
        this.settings = settings;
    }


    public JsonArray getArtifact() {
        return artifact;
    }

    public void setArtifact(JsonArray artifact) {
        this.artifact = artifact;
    }

    public JsonArray getSr_master_bde() {
        return sr_master_bde;
    }

    public void setSr_master_bde(JsonArray sr_master_bde) {
        this.sr_master_bde = sr_master_bde;
    }

    public JsonArray getTarget() {
        return target;
    }

    public void setTarget(JsonArray target) {
        this.target = target;
    }


    public JsonArray getExpense_type() {
        return expense_type;
    }

    public void setExpense_type(JsonArray expense_type) {
        this.expense_type = expense_type;
    }


    public JsonArray getPrstatus() {
        return prstatus;
    }

    public void setPrstatus(JsonArray prstatus) {
        this.prstatus = prstatus;
    }

    public JsonArray getSecstatus() {
        return secstatus;
    }

    public void setSecstatus(JsonArray secstatus) {
        this.secstatus = secstatus;
    }

    public JsonArray getSr_invoice_batch() {
        return sr_invoice_batch;
    }

    public void setSr_invoice_batch(JsonArray sr_invoice_batch) {
        this.sr_invoice_batch = sr_invoice_batch;
    }

}
