package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.SurveyResponseAnswer;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY_RESPONSE_ANSWER;

public class SurveyResponseAnswerTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_SURVEY_RESPONSE_ID = "id";
    private static final String COLUMN_SURVEY_ID = "survey_id";
    private static final String COLUMN_SURVEY_QUESTION_ID = "survey_question_id";
    private static final String COLUMN_RESPONSE_ANSWER_ID = "response_answer_id";
    private static final String COLUMN_OPTION_VALUE = "option_value";
    private static final String COLUMN_OPTION_VALUE_ID = "option_value_ids";
    private static final String COLUMN_SURVEY_CREATED_AT = "created_at";
    private static final String COLUMN_SURVEY_UPDATED_AT = "updated_at";
    private static final String COLUMN_SURVEY_RESPONSE_ANSWER_IS_SYNC = "is_sync";
    private final static String COLUMN_SURVEY_RESPONSE_ANSWER_SEQUENCE_NO = "visitSeqNo";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public SurveyResponseAnswerTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY_RESPONSE_ANSWER);

        String surveyTable = "CREATE TABLE IF NOT EXISTS " + TABLE_SURVEY_RESPONSE_ANSWER + "(" +
                COLUMN_SURVEY_RESPONSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SURVEY_ID + " TEXT, " +
                COLUMN_SURVEY_QUESTION_ID + " TEXT, " +
                COLUMN_RESPONSE_ANSWER_ID + " TEXT, " +
                COLUMN_OPTION_VALUE_ID + " TEXT," +
                COLUMN_OPTION_VALUE + " TEXT, " +
                COLUMN_SURVEY_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SURVEY_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SURVEY_RESPONSE_ANSWER_SEQUENCE_NO + " TEXT, " +
                COLUMN_SURVEY_RESPONSE_ANSWER_IS_SYNC + " TEXT " +
                ");";
        sqlWrite.execSQL(surveyTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(SurveyResponseAnswer surveyResponse) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SURVEY_ID, surveyResponse.getSurvey_id());
        values.put(COLUMN_RESPONSE_ANSWER_ID, surveyResponse.getSurvey_response_answer_id());
        values.put(COLUMN_SURVEY_QUESTION_ID, surveyResponse.getSurvey_question_id());
        values.put(COLUMN_OPTION_VALUE, surveyResponse.getOption_value());
        values.put(COLUMN_OPTION_VALUE_ID, surveyResponse.getOptionsValuesIds());
        values.put(COLUMN_SURVEY_RESPONSE_ANSWER_SEQUENCE_NO, surveyResponse.getVisitSequenceNumber());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SURVEY_RESPONSE_ANSWER, null, values);
        db.close();
        return id;
    }

    public void deleteAnswers(String responseAnswerId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SURVEY_RESPONSE_ANSWER, COLUMN_RESPONSE_ANSWER_ID + "='" + responseAnswerId + "'", null);
        db.close();
    }


    public ArrayList<SurveyResponseAnswer> getAllSurveyResponseAnswerForSync() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_SURVEY_RESPONSE_ANSWER;

        ArrayList<SurveyResponseAnswer> responseAnswerArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {
            SurveyResponseAnswer surveyAnswer = new SurveyResponseAnswer();
            surveyAnswer.setTabCode(Constants.TAB_CODE);
            surveyAnswer.setCompanyCode(Constants.COMPANY_CODE);
            surveyAnswer.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            surveyAnswer.setId(id);
            surveyAnswer.setSurvey_id(cursor.getString(1));
            surveyAnswer.setSurvey_question_id(cursor.getString(2));
            surveyAnswer.setSurvey_response_answer_id(cursor.getString(3));
            surveyAnswer.setOption_value(cursor.getString(5));
            surveyAnswer.setOptionsValuesIds(cursor.getString(4));
            surveyAnswer.setVisitSequenceNumber(cursor.getString(8));

            responseAnswerArrayList.add(surveyAnswer);
        }
        cursor.close();
        sqlWrite.close();
        return responseAnswerArrayList;
    }

    ArrayList<SurveyResponseAnswer> getSurveyResponseById(String surveyResponseAnswerId) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_SURVEY_RESPONSE_ANSWER + " WHERE " + COLUMN_RESPONSE_ANSWER_ID + "= ?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{surveyResponseAnswerId});

        ArrayList<SurveyResponseAnswer> answerList = new ArrayList<>();
        while (cursor.moveToNext()) {
            SurveyResponseAnswer surveyAnswer = new SurveyResponseAnswer();
            int id = cursor.getInt(0);
            surveyAnswer.setId(id);
            surveyAnswer.setSurvey_id(cursor.getString(1));
            surveyAnswer.setSurvey_question_id(cursor.getString(2));
            surveyAnswer.setOptionsValuesIds(cursor.getString(4));
            surveyAnswer.setOption_value(cursor.getString(5));
            answerList.add(surveyAnswer);
        }
        cursor.close();
        sqlWrite.close();
        return answerList;
    }
}
