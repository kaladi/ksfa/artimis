package in.kumanti.emzor.activity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.DeliveryChallanItems;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.InvoiceBatch;
import in.kumanti.emzor.model.Invoices;
import in.kumanti.emzor.model.Orders;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.InputFilterMinMax;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;


public class WayBillActivity extends MainActivity {


    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionBarTitle, marqueeText, checkInTime;

    String login_id = "", customerCode = "", checkInTimeString = "";
    MyDBHandler dbHandler;

    TextView customerName, accountNo, city;
    String customerNameString, customerEmailAddress, ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;

    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;

    LinearLayout headerDurationContainer;

    TextView waybillNumber, waybillDate;
    String wayBillIdString = null, wayBillDateDB, wayBillTime;

    CustomSearchableSpinner orderNumberSpinner;

    String wayBillStatus = "";


    Button smsButton, printButton, emailButton;

    ImageView createSignature, viewSignature, saveWaybillButton;

    EditText customerSignatureName;
    Bitmap selectedImage = null;
    int wayBillQuantity;
    String customerAddress1, customerAddress2, customerAddress3;
    String invoice_id = null;
    String sr_name1;
    String sr_name_details, sr_rm_name, sr_region, sr_warehouse;
    DecimalFormat formatter, formatter1;
    ArrayList<InvoiceBatch> productbatchLists;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng, nav_type, customer_type;
    GeneralSettingsTable generalSettingsTable;
    ArrayList<Orders> ordersArrayList;
    ArrayAdapter<Orders> ordersSpinnerArrayAdapter;
    ArrayList<DeliveryChallanItems> deliveryChallanItemsArrayList;
    String orderNumber;
    DeliveryChallanItems deliveryChallanItems;
    int random_num = 0;
    GeneralSettings gs;
    double price = 0.00;
    String tab_prefix;
    int remaining_qty = 0;
    int allocate_qty = 0;
    String emailType = "Waybill";
    String companyName,companyCode1, compAdd1, compAdd2, compAdd3, compMblNo;
    //Variables declaration for print and sms
    String customerPhoneNumber = "";
    String smsMessageText = "";
    // android built in classes for bluetooth operations
    PrintBluetooth printBluetooth;
    Print print;
    String printerName = "MP80-17031810239";
    String printMsg;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_waybill);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        wayBillDate();
        generateWayBillNumber();

        actionBarTitle.setText("Waybill");
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        wayBillStatus = "InProcess";

        getlocation();

        if ("Reports".equals(nav_type)) {

            saveWaybillButton.setVisibility(View.GONE);

            createSignature.setEnabled(false);
           // headerDurationContainer.setVisibility(View.GONE);
            wayBillStatus = "Completed";
        }


        wayBillQuantity = dbHandler.getWaybillCount(invoice_id);


        //Warehouse Spinner Initialization
        ordersArrayList = dbHandler.getOrderNumber(customerCode);
        Orders w = new Orders();
        if ("Reports".equals(nav_type)) {
            w.setOrderCode(orderNumber);
            ordersArrayList.add(0, w);
        } else {
            w.setOrderCode("Select");
            ordersArrayList.add(0, w);
        }

        ordersSpinnerArrayAdapter = new ArrayAdapter<Orders>(getApplicationContext(), android.R.layout.simple_spinner_item, ordersArrayList);
        ordersSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        orderNumberSpinner.setAdapter(ordersSpinnerArrayAdapter);
        orderNumberSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                orderNumberSpinner.isSpinnerDialogOpen = false;

                Orders w = (Orders) adapterView.getItemAtPosition(pos);
                dbHandler.deleteDeliveryDetails();

                if ("Reports".equals(nav_type)) {

                } else {
                    orderNumber = w.getOrderCode();

                }

                if ("Select".equals(orderNumber)) {

                } else {
                    deliveryChallanItemsArrayList = new ArrayList<DeliveryChallanItems>();
                    Cursor data;
                    if ("Reports".equals(nav_type)) {
                        data = dbHandler.getWaybillDeliveryChallanDetails(orderNumber);

                    } else {
                        data = dbHandler.getOrderDeliveryChallanDetails(orderNumber);

                    }
                    int numRows1 = data.getCount();
                    if (numRows1 == 0) {
                        //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
                    } else {
                        int i = 0;
                        while (data.moveToNext()) {
                            long id2 = 0;
                            if ("Reports".equals(nav_type)) {

                            } else {
                                id2 = dbHandler.insert_waybill_details
                                        (login_id,
                                                data.getString(8),
                                                data.getString(7),
                                                data.getInt(0),
                                                data.getString(9),
                                                data.getString(10),
                                                data.getString(11),
                                                data.getString(6),
                                                data.getString(1),
                                                data.getString(2),
                                                data.getString(3),
                                                data.getDouble(4),
                                                data.getDouble(5),
                                                null,
                                                null,
                                                null,
                                                random_num,
                                                "Draft"

                                        );

                                if (id2 <= 0) {
                                    Toast.makeText(getApplicationContext(), "Order Creation Failed", Toast.LENGTH_LONG).show();
                                } else {
                                    // Toast.makeText(getApplicationContext(), "Product is added to cart", Toast.LENGTH_LONG).show();
                                }
                            }


                            String formatted_price = formatter.format(data.getDouble(4));
                            String formatted_value = formatter.format(data.getDouble(5));
                            final String item_type = dbHandler.get_order_type(data.getString(6));

                            deliveryChallanItems = new DeliveryChallanItems(data.getString(0), data.getString(1), data.getString(2), data.getInt(3), formatted_price, formatted_value, data.getDouble(4), data.getDouble(5), item_type, orderNumber, data.getString(6), data.getString(12), data.getInt(13));
                            deliveryChallanItemsArrayList.add(i, deliveryChallanItems);

                            i++;
                        }

                        ListDeliveryChallanAdapter adapter = new ListDeliveryChallanAdapter(getApplicationContext(), R.layout.list_delivery_challan, deliveryChallanItemsArrayList, orderNumber, checkInTimeString, customerCode);
                        ListView list_sales_line = findViewById(R.id.list_sales_line);
                        list_sales_line.setAdapter(adapter);
                    }
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                orderNumberSpinner.isSpinnerDialogOpen = false;

            }

        });


        customerImage.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                showcustomerLocation();
                return true;
            }

        });


        createSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewSignature();
            }
        });


        viewSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedImage != null) {
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.image_preview, null);
                    ImageView capturedImage, closeButton;
                    capturedImage = alertLayout.findViewById(R.id.capturedImage);
                    closeButton = alertLayout.findViewById(R.id.closeImageView);
                    capturedImage.setImageBitmap(selectedImage);
                    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    // this is set the view from XML inside AlertDialog

                    alert.setView(alertLayout);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(true);

                    android.app.AlertDialog dialog = alert.create();
                    dialog.show();


                }

            }
        });

        /*
         * Sending SMS Manager API using default SMS manager
         */


        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!wayBillStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save Waybill to send SMS", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(customerPhoneNumber)) {
                    Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                    return;
                }

                Gson gson = new GsonBuilder().setLenient().create();
                JsonArray waybillProductsJsonArray = gson.toJsonTree(deliveryChallanItemsArrayList).getAsJsonArray();

                smsMessageText ="Sale Waybill\n" +
                        "\n" +
                        "Thanks for trusting " + Constants.COMPANY_NAME + " Products." +
                        "\n" +
                        "Please refer your"+
                        "\n" +
                        "Waybill No: #" + waybillNumber.getText().toString() + "\n" +
                        "Return Date: " + waybillDate.getText().toString() + "\n" +
                        "Waybill Lines: " + wayBillQuantity + "\n" +
                        "care.nigeria@artemislife.com"+ "\n" ;

                JsonObject jp = new JsonObject();

                jp.addProperty("smsType", emailType);
                jp.addProperty("smsMessageText", smsMessageText);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("customerPhoneNumber", customerPhoneNumber);
                jp.addProperty("companyMblNo", compMblNo);
                jp.addProperty("waybillNumber", waybillNumber.getText().toString());
                jp.addProperty("waybillDate", waybillDate.getText().toString());
                jp.addProperty("waybillTime", wayBillTime);
                jp.addProperty("bdeName", sr_name_details);
                jp.addProperty("totalQuantity", wayBillQuantity);


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.SMS_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingWaybillSMSData(jp);

                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND:", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }

                });


              /*  try {
                    smsMessageText = "Sale Waybill\n" +
                            "\n" +
                            "Waybill No: #" + waybillNumber.getText().toString() + "\n" +
                            "Return Date: " + waybillDate.getText().toString() + "\n" +
                            "Return Time: " + wayBillTime + "\n" +
                            "Waybill Lines: " + wayBillQuantity + "\n";
                    // "Waybill Value: " + invoiceValue.getText().toString() + "\n";

                    Log.d("SMSTEST", "TESTMESSAGE" + smsMessageText);

                    SmsManager smsManager = SmsManager.getDefault();
                    ArrayList<String> parts = smsManager.divideMessage(smsMessageText);
                    smsManager.sendMultipartTextMessage(customerPhoneNumber, null, parts, null, null);
                    //smsManager.sendTextMessage(customerPhoneNumber, null, smsMessageText, null, null);

                    Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.d("Sms", "Exception" + e);
                }*/

            }
        });

        /**
         * Print the order
         */


        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!wayBillStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save Waybill to Print", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    int totalSize = 48;
                    String ESC_NEW_LINE = "\n";
                    String horizontalLine = "-----------------------------------------------";
                    printMsg = "";
                    printMsg += print.centerString(totalSize, companyName);
                    printMsg += print.centerString(totalSize, compAdd1);
                    printMsg += print.centerString(totalSize, compAdd2);
                    printMsg += print.centerString(totalSize, compAdd3);
                    printMsg += print.centerString(totalSize, compMblNo);

                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + "Waybill Number:" + waybillNumber.getText().toString();
                    printMsg += ESC_NEW_LINE + print.padRight("Date:" + waybillDate.getText(), 24) + print.padLeft("TIME:" + wayBillTime, 24);
                    printMsg += "BDE Name:" + sr_name_details;
                    printMsg += ESC_NEW_LINE + "Customer Name:" + customerNameString;
                    printMsg += ESC_NEW_LINE + "Address:";
                    printMsg += print.padRight(customerAddress1, totalSize);
                    printMsg += print.padRight(customerAddress2, totalSize);
                    printMsg += print.padRight(customerAddress3, totalSize);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += print.centerString(15, "Product Name") + " " + "  VOL " + print.padLeft("PRICE", 10) + "  " + print.padLeft("VALUE", 14);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    double total = 0;
                    int i = 1;
                    for (DeliveryChallanItems ls : deliveryChallanItemsArrayList) {
                        printMsg += print.padLeft(String.valueOf(i) + ".", 3) + print.padRight(ls.getProduct().substring(0, 11), 12) + " " + print.padLeft(String.valueOf(ls.getWaybill_quantity()), 5) + " " + print.padLeft(ls.getPrice(), 10) + "  " + print.padLeft(formatter.format((ls.getPrice_db() * Double.parseDouble(String.valueOf(ls.getWaybill_quantity())))), 14);
                        total += (ls.getPrice_db() * Double.parseDouble(String.valueOf(ls.getWaybill_quantity())));
                        i++;
                    }

                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;


                    printMsg += print.padRight("", 15) + print.padRight("Total Value:", 19) + print.padLeft(String.valueOf(formatter.format(total)), 14);


                    printMsg += ESC_NEW_LINE + horizontalLine;


                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.padLeft("Customer Sign", totalSize);
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    print.printContent(printMsg);
                    //printBluetooth.printContent(printMsg, selectedImage);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });


        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!wayBillStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save Waybill to EMAIL", Toast.LENGTH_LONG).show();
                    return;
                }

                int total = 0;
                int i = 1;
                for (DeliveryChallanItems ls : deliveryChallanItemsArrayList) {
                    total += (Integer.parseInt(String.valueOf(ls.getWaybill_quantity())));
                    i++;
                }


                Gson gson = new GsonBuilder().setLenient().create();
                JsonArray waybillProductsJsonArray = gson.toJsonTree(deliveryChallanItemsArrayList).getAsJsonArray();

                JsonObject jp = new JsonObject();

                jp.addProperty("emailType", emailType);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);
                jp.addProperty("customerEmailAddress", customerEmailAddress);
                jp.addProperty("waybillNumber", waybillNumber.getText().toString());
                jp.addProperty("waybillDate", waybillDate.getText().toString());
                jp.addProperty("waybillTime", wayBillTime);
                jp.addProperty("bdeName", sr_name_details);

                jp.add("waybillProducts", waybillProductsJsonArray);
                jp.addProperty("totalQuantity", String.valueOf(total));


                if (selectedImage != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                    jp.addProperty("customerSignature", encodedImage);
                }

                Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingEmailData(jp);

                Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND:", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }

                });


            }
        });


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


        saveWaybillButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                gs = generalSettingsTable.getSettingByKey("wayBillSignature");
                if (gs != null && gs.getValue().equals("Yes")) {
                    if (TextUtils.isEmpty(customerSignatureName.getText())) {
                        Toast.makeText(getApplicationContext(), "Please Enter Signee's Name", Toast.LENGTH_LONG).show();
                        return;
                    } else if (selectedImage == null) {
                        Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                        return;
                    }

                }

                android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                alertbox.setTitle("Save Transaction ?");
                alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        Toast.makeText(getApplicationContext(), "Waybill saved Successfully", Toast.LENGTH_LONG).show();


                        Cursor data = dbHandler.getWaybillDetails(orderNumber);
                        int rec_random_num = dbHandler.get_receipt_random_num();

                        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
                        Date myDate = new Date();
                        String rec_seq_date = timeStampFormat.format(myDate);
                        String receipt_num;
                        rec_random_num = rec_random_num + 1;

                        if (rec_random_num <= 9) {
                            receipt_num = tab_prefix + "SR" + rec_seq_date + "00" + rec_random_num;

                        } else if (rec_random_num > 9 & random_num < 99) {
                            receipt_num = tab_prefix + "SR" + rec_seq_date + "0" + rec_random_num;

                        } else {
                            receipt_num = tab_prefix + "SR" + rec_seq_date + rec_random_num;
                        }

                        int numRows1 = data.getCount();
                        if (numRows1 == 0) {
                        } else {
                            int i = 0;
                            while (data.moveToNext()) {

                                long id = dbHandler.createstockreciept
                                        (
                                                receipt_num,
                                                wayBillDateDB,
                                                "NGN",
                                                login_id,
                                                null,
                                                "Waybill",
                                                "",
                                                data.getString(0),
                                                data.getString(1),
                                                data.getString(2),
                                                null,
                                                null,
                                                "",
                                                orderNumber,
                                                "Completed",
                                                rec_random_num,
                                                Constants.VISIT_SEQ_NUMBER,
                                                waybillNumber.getText().toString(),
                                                ""
                                        );

                                if (id <= 0) {
                                    Toast.makeText(getApplicationContext(), "Error.. Please try again", Toast.LENGTH_LONG).show();

                                } else {


                                }
                            }
                        }

                        dbHandler.deletewaybillbatchrow(orderNumber);
                        dbHandler.updatewaybillbatchstatus(orderNumber, "Completed");
                        dbHandler.updateWaybillInvoiceBatchquantity(orderNumber);
                        dbHandler.update_waybill_status(orderNumber, "Completed", waybillNumber.getText().toString(), wayBillDateDB, customerSignatureName.getText().toString());
                        dbHandler.update_order_header_status_from_waybill(orderNumber);
                        saveWaybillButton.setEnabled(false);
                        wayBillStatus = "Completed";
                        saveWaybillButton.setVisibility(View.GONE);


                    }


                })

                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                saveWaybillButton.setEnabled(true);

                            }
                        });

                alertbox.show();

            }

        });

    }


    public void wayBillDate() {
        String PATTERN = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        String PATTERN_DISPLAY = "dd MMM YY";
        SimpleDateFormat dateFormatDisplay = new SimpleDateFormat();
        dateFormatDisplay.applyPattern(PATTERN_DISPLAY);
        String dateDisplay = dateFormatDisplay.format(Calendar.getInstance().getTime());
        wayBillDateDB = date;
        waybillDate.setText(dateDisplay);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        wayBillTime = sdf.format(outDate);
    }


    public void generateWayBillNumber() {
        random_num = dbHandler.get_waybill_id();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        Log.d("Waybill", "Random No---" + random_num);

        if (random_num <= 9) {
            waybillNumber.setText(Constants.TAB_PREFIX + "WB" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            waybillNumber.setText(Constants.TAB_PREFIX + "WB" + rec_seq_date + "0" + random_num);

        } else {
            waybillNumber.setText(Constants.TAB_PREFIX + "WB" + rec_seq_date + random_num);
        }

        wayBillIdString = waybillNumber.getText().toString();

    }


    private void createNewSignature() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);
        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                selectedImage = signatureBitmap;
                viewSignature.setImageBitmap(signatureBitmap);
                dialog.dismiss();

            }
        });

    }


    private void getlocation() {


        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            String lat = Double.toString(latitude);
            String lng = Double.toString(longitude);

            lat_lng = lat + "," + lng;

        } else {

            gpsTracker.showSettingsAlert();
        }
    }

    private void initializeViews() {

        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

      /*  duration = findViewById(R.id.durationValueTextView);
        checkInTime = findViewById(R.id.checkInTimeValueTextView);

        headerDurationContainer = findViewById(R.id.headerDurationContainer);*/

        waybillNumber = findViewById(R.id.waybillNumber);
        waybillDate = findViewById(R.id.waybillDate);

        orderNumberSpinner = findViewById(R.id.orderNumberSpinner);

        customerSignatureName = findViewById(R.id.signCustomerNameEditText);
        createSignature = findViewById(R.id.CreateSignImageView);
        viewSignature = findViewById(R.id.ViewSignatureImageView);

        saveWaybillButton = findViewById(R.id.wayBillSaveButton);

        printButton = findViewById(R.id.wayBillPrintButton);
        emailButton = findViewById(R.id.wayBillEmailButton);
        smsButton = findViewById(R.id.wayBillSmsButton);

        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("####.00");

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        printBluetooth = new PrintBluetooth(this);

        print = new Print(this);
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());


        gs = generalSettingsTable.getSettingByKey("enablePrint");
        if (gs != null && gs.getValue().equals("Yes")) {
            printButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableSms");
        if (gs != null && gs.getValue().equals("Yes")) {
            smsButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableEmail");
        if (gs != null && gs.getValue().equals("Yes")) {
            emailButton.setVisibility(View.VISIBLE);
        }


    }

    private void populateHeaderDetails() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
       /* duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();*/


        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            invoice_id = bundle.getString("invoice_id");
            checkInTimeString = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            nav_type = bundle.getString("nav_type");
            orderNumber = bundle.getString("order_num");
            customer_type = bundle.getString("customer_type");
            price = bundle.getDouble("price");

        }

       // checkInTime.setText(checkInTimeString);

        //Populate SalesRep Details
        Log.d("Invoice", "Login---" + login_id);
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                //byte[] blob = salesRepDetails.getBlob(9);
                //Bitmap bmp= convertByteArrayToBitmap(blob);
                //sr_image.setImageBitmap(bmp);
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }


        //Populate Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                customerNameString = customerDetails.getString(2);
                customerEmailAddress = customerDetails.getString(33);
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));
              /*  byte[] blob = customerDetails.getBlob(13);
                int customer_image = dbHandler.get_length_customer_image(customer_id);
                if (customer_image == 0) {

                } else {
                    Bitmap bmp = convertByteArrayToBitmap(blob);
                    customer_pic.setImageBitmap(bmp);
                }*/

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);


                customerAddress1 = customerDetails.getString(6);
                customerAddress2 = customerDetails.getString(7);
                customerAddress3 = customerDetails.getString(8);
                customerPhoneNumber = customerDetails.getString(11);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }
            }
        }
        getCompanyInfo();

        Invoices invoice = dbHandler.getinvoiceHeader(invoice_id);
        try {
            String dateTime = invoice.getCreatedAt();

            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("HH:ss");
            Date myDate = new Date(Long.parseLong(dateTime) * 1000L);
            // invoiceTime = dateFormat.format(myDate);
        } catch (Exception e) {
            // ledger.setInvoiceDate(cursor.getString(3));
        }


    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);

            }
        }

    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    public void showsrdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_sr_details, null);
        final TextView sr_name = alertLayout.findViewById(R.id.sr_name_details);
        final TextView sr_rm = alertLayout.findViewById(R.id.sr_rm_name);
        final TextView sr_rgn = alertLayout.findViewById(R.id.sr_region);
        final TextView sr_whouse = alertLayout.findViewById(R.id.sr_warehouse);
        final TextView sr_gps = alertLayout.findViewById(R.id.sr_gps);

        sr_name.setText(sr_name_details);
        sr_rm.setText(sr_rm_name);
        sr_rgn.setText(sr_region);
        sr_whouse.setText(sr_warehouse);
        sr_gps.setText(lat_lng);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);

        printBluetooth.onActivityResult(requestCode, resultCode, data, printMsg);
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    private void loadproductbatchlist_batch(String product_id, String orderNumber, int order_qty) {

        Cursor data = dbHandler.getProductBatchDetails2(product_id, orderNumber);
        int numRows1 = data.getCount();
        //order_qty = Integer.parseInt(order_quantity);
        if (numRows1 == 0) {
        } else {
            int i = 0;
            allocate_qty = order_qty;
            while (data.moveToNext()) {

                int onhand_qty = data.getInt(2);
                if (onhand_qty >= allocate_qty) {
                    // remaining_qty=onhand_qty-allocate_qty;

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(allocate_qty), data.getString(0), orderNumber);
                    //dbHandler.updatebatchqty(String.valueOf(invoice_id), product_id, data.getString(1));

                    break;

                } else {

                    remaining_qty = allocate_qty - onhand_qty;
                    Log.i("remaining_qty", String.valueOf(remaining_qty));

                    int test = allocate_qty;
                    if (allocate_qty <= onhand_qty) {
                        test = allocate_qty;
                    } else {
                        test = onhand_qty;
                    }

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(test), data.getString(0), orderNumber);
                    allocate_qty = remaining_qty;
                }
                i++;
            }

        }
    }

    private void loadproductbatchlist(String orderNumber, int quantity, String product_id) {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails(product_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {

        } else {
            int i = 0;
            while (data.moveToNext()) {
                long id2 = dbHandler.insert_product_batch
                        (
                                data.getString(0),
                                data.getString(1),
                                data.getString(2),
                                data.getString(3),
                                null,
                                null,
                                orderNumber,
                                "Draft"
                        );

            }

            i++;
        }

    }

    public class ListDeliveryChallanAdapter extends ArrayAdapter<DeliveryChallanItems> {

        Activity mActivity = null;
        android.support.v7.app.AlertDialog alertDialog1;
        String customer_id = "";
        String checkin_time = "";
        String order_id = "";
        String salesline_id = "";
        String trip_num = "";
        Globals globals;
        MyDBHandler dbHandler;
        View v;
        private LayoutInflater mInflater;
        private ArrayList<DeliveryChallanItems> users;
        private int mViewResourceId;
        private Context c;

        public ListDeliveryChallanAdapter(Context context, int textViewResourceId, ArrayList<DeliveryChallanItems> users, String order_id_val, String checkintime_val, String customer_id_val) {
            super(context, textViewResourceId, users);
            this.users = users;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            order_id = order_id_val;
            checkin_time = checkintime_val;
            customer_id = customer_id_val;
            dbHandler = new MyDBHandler(c, null, null, 1);


        }


        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            convertView = mInflater.inflate(mViewResourceId, null);
            final DeliveryChallanItems user = users.get(position);
            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.product = convertView.findViewById(R.id.product);
                viewHolder.uom = convertView.findViewById(R.id.uom);
                viewHolder.quantity = convertView.findViewById(R.id.quantity);
                viewHolder.price = convertView.findViewById(R.id.price);
                viewHolder.value = convertView.findViewById(R.id.value);
                viewHolder.item_type = convertView.findViewById(R.id.item_type);
                viewHolder.waybill_qty = convertView.findViewById(R.id.waybill_qty);
                viewHolder.waybill_qty.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});

                String qty_formatted = "";
                String waybill_qty = String.valueOf(user.getWaybill_quantity());
                String waybill_qty1;
                if ("0".equals(waybill_qty)) {
                    waybill_qty1 = "";
                } else {
                    waybill_qty1 = waybill_qty;
                }

                if (viewHolder.product != null) {
                    viewHolder.product.setText(user.getProduct());
                }
                if (viewHolder.uom != null) {
                    viewHolder.uom.setText((user.getUom()));
                }
                if (viewHolder.quantity != null) {
                    viewHolder.quantity.setText(String.valueOf(user.getQuantity()));
                }
                if (viewHolder.price != null) {
                    viewHolder.price.setText((user.getPrice()));
                }
                if (viewHolder.value != null) {
                    viewHolder.value.setText((user.getValue()));
                }
                if (viewHolder.waybill_qty != null) {
                    viewHolder.waybill_qty.setText(waybill_qty1);
                }

                if (("Reports".equals(nav_type)) || ("Completed".equals(wayBillStatus))) {
                    viewHolder.waybill_qty.setEnabled(false);
                }

            }

            ViewHolder finalViewHolder = viewHolder;

            viewHolder.waybill_qty.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                    if (("".equals(finalViewHolder.waybill_qty.getText().toString())) || ("Completed".equals(wayBillStatus))) {

                    } else {
                        final String batchControlled = user.getBatchControlled();
                        final String id = user.getSales_line_id();
                        final String orderNumber = user.getOrderNumber();

                        if ("No".equals(batchControlled)) {
                            int entered_qty = Integer.parseInt(finalViewHolder.waybill_qty.getText().toString());
                            int stock_batch_value = dbHandler.get_stockreceipt_quantity(user.getProductId());
                            int order_qty = user.getQuantity();

                            if (entered_qty > stock_batch_value) {
                                finalViewHolder.waybill_qty.setError("Entered Quantity Exceeds Stock Quantity");
                                finalViewHolder.waybill_qty.setText(null);
                            } else if (entered_qty > order_qty) {
                                finalViewHolder.waybill_qty.setError("Entered Quantity Exceeds Ordered Quantity");
                                finalViewHolder.waybill_qty.setText(null);
                            } else {
                                dbHandler.updateDeliveryDetails(id, orderNumber, finalViewHolder.waybill_qty.getText().toString());

                            }
                        } else {
                            int entered_qty = Integer.parseInt(finalViewHolder.waybill_qty.getText().toString());
                            int batch_value = dbHandler.get_productbatch_quantity(user.getProductId());
                            int order_qty = user.getQuantity();

                            if (entered_qty > batch_value) {
                                finalViewHolder.waybill_qty.setError("Entered Quantity Exceeds Stock Quantity");
                                finalViewHolder.waybill_qty.setText(null);
                            } else if (entered_qty > order_qty) {
                                finalViewHolder.waybill_qty.setError("Entered Quantity Exceeds Ordered Quantity");
                                finalViewHolder.waybill_qty.setText(null);
                            } else {
                                dbHandler.updateDeliveryDetails(id, orderNumber, finalViewHolder.waybill_qty.getText().toString());
                                dbHandler.deletebatch(user.getProductId());
                                loadproductbatchlist(orderNumber, Integer.parseInt(finalViewHolder.waybill_qty.getText().toString()), user.getProductId());
                                loadproductbatchlist_batch(user.getProductId(), orderNumber, Integer.parseInt(finalViewHolder.waybill_qty.getText().toString()));
                                dbHandler.updatebatchstatusDelivery(orderNumber, "Inprocess");
                            }
                        }


                    }
                }

            });

            viewHolder.product.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (("Reports".equals(nav_type)) || ("Completed".equals(wayBillStatus))) {

                    } else {
                        final String id = user.getSales_line_id();
                        if ("Yes".equals(user.getBatchControlled())) {

                            if ("".equals(finalViewHolder.waybill_qty.getText().toString())) {
                                finalViewHolder.waybill_qty.setError("Enter Quantity to View Batch Details");

                            } else {
                                call_alert(v, position, id, finalViewHolder.waybill_qty.getText().toString());

                            }

                        } else {

                        }
                    }

                }

            });

            viewHolder.uom.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (("Reports".equals(nav_type)) || ("Completed".equals(wayBillStatus))) {

                    } else {
                        final String id = user.getSales_line_id();
                        if ("Yes".equals(user.getBatchControlled())) {

                            if ("".equals(finalViewHolder.waybill_qty.getText().toString())) {
                                finalViewHolder.waybill_qty.setError("Enter Quantity to View Batch Details");

                            } else {
                                call_alert(v, position, id, finalViewHolder.waybill_qty.getText().toString());

                            }

                        } else {

                        }
                    }

                }

            });

            viewHolder.quantity.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (("Reports".equals(nav_type)) || ("Completed".equals(wayBillStatus))) {

                    } else {
                        final String id = user.getSales_line_id();
                        if ("Yes".equals(user.getBatchControlled())) {

                            if ("".equals(finalViewHolder.waybill_qty.getText().toString())) {
                                finalViewHolder.waybill_qty.setError("Enter Quantity to View Batch Details");

                            } else {
                                call_alert(v, position, id, finalViewHolder.waybill_qty.getText().toString());

                            }

                        } else {

                        }
                    }

                }

            });

            viewHolder.price.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (("Reports".equals(nav_type)) || ("Completed".equals(wayBillStatus))) {

                    } else {
                        final String id = user.getSales_line_id();
                        if ("Yes".equals(user.getBatchControlled())) {

                            if ("".equals(finalViewHolder.waybill_qty.getText().toString())) {
                                finalViewHolder.waybill_qty.setError("Enter Quantity to View Batch Details");

                            } else {
                                call_alert(v, position, id, finalViewHolder.waybill_qty.getText().toString());

                            }

                        } else {

                        }
                    }

                }

            });

            viewHolder.value.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (("Reports".equals(nav_type)) || ("Completed".equals(wayBillStatus))) {

                    } else {
                        final String id = user.getSales_line_id();
                        if ("Yes".equals(user.getBatchControlled())) {

                            if ("".equals(finalViewHolder.waybill_qty.getText().toString())) {
                                finalViewHolder.waybill_qty.setError("Enter Quantity to View Batch Details");

                            } else {
                                call_alert(v, position, id, finalViewHolder.waybill_qty.getText().toString());

                            }

                        } else {

                        }
                    }

                }

            });

            return convertView;
        }

        public void call_alert(final View v, final int position, final String id, final String delivery_quantity) {

            final DeliveryChallanItems user = users.get(position);

            // final String id = user.getInvoice_line_id();
            final String prod = user.getProduct();
            final String uom = user.getUom();
            final int qty = user.getQuantity();
            final double price = user.getPrice_db();
            final double value = user.getValue_db();
            final String prod_id = user.getProductId();
            final String batch_controlled = user.getBatchControlled();

            Intent myintent = new Intent(getApplicationContext(), WaybillUpdateActivity.class);

            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("login_id", login_id);
            bundle.putString("trip_num", trip_num);
            bundle.putString("product", prod);
            bundle.putString("uom", uom);
            bundle.putString("qty", delivery_quantity);
            bundle.putString("id", id);
            bundle.putString("prod_id", prod_id);
            bundle.putString("customer_id", customer_id);
            bundle.putString("invoice_num", orderNumber);
            bundle.putString("customer_type", customer_type);
            bundle.putString("batch_controlled", batch_controlled);

            myintent.putExtras(bundle);
            startActivity(myintent);

        }

        public class ViewHolder {
            TextView product, uom, quantity, price, value;
            EditText waybill_qty;
            ImageView item_type;
            View v;
        }


    }

}
