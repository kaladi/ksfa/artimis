package in.kumanti.emzor.model;

public class CustomerReturnProducts {

    public int customerReturnProductId;
    public int customerReturnId;
    public String customerReturnNumber;
    public String customerReturnDate;
    public String productCode;
    public String productName;
    public String productUom;
    public String returnPrice;
    public String returnValue;
    public String availQuantity;
    public String returnQuantity;
    public String batchName;
    public String expiryDate;
    public String batchAvailQty;
    public String batchReturnQty;
    public String latestSynDate;
    public String errorStockReturnQty;
    public boolean isSync;
    public String createdAt;
    public String updatedAt;
    public String customerName;
    private String tabCode;
    private String companyCode;
    private String srCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public int getCustomerReturnProductId() {
        return customerReturnProductId;
    }

    public void setCustomerReturnProductId(int customerReturnProductId) {
        this.customerReturnProductId = customerReturnProductId;
    }

    public int getCustomerReturnId() {
        return customerReturnId;
    }

    public void setCustomerReturnId(int customerReturnId) {
        this.customerReturnId = customerReturnId;
    }

    public String getCustomerReturnDate() {
        return customerReturnDate;
    }

    public void setCustomerReturnDate(String customerReturnDate) {
        this.customerReturnDate = customerReturnDate;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public String getAvailQuantity() {
        return availQuantity;
    }

    public void setAvailQuantity(String availQuantity) {
        this.availQuantity = availQuantity;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getBatchAvailQty() {
        return batchAvailQty;
    }

    public void setBatchAvailQty(String batchAvailQty) {
        this.batchAvailQty = batchAvailQty;
    }

    public String getBatchReturnQty() {
        return batchReturnQty;
    }

    public void setBatchReturnQty(String batchReturnQty) {
        this.batchReturnQty = batchReturnQty;
    }

    public String getErrorStockReturnQty() {
        return errorStockReturnQty;
    }

    public void setErrorStockReturnQty(String errorStockReturnQty) {
        this.errorStockReturnQty = errorStockReturnQty;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCustomerReturnNumber() {
        return customerReturnNumber;
    }

    public void setCustomerReturnNumber(String customerReturnNumber) {
        this.customerReturnNumber = customerReturnNumber;
    }

    public String getLatestSynDate() {
        return latestSynDate;
    }

    public void setLatestSynDate(String latestSynDate) {
        this.latestSynDate = latestSynDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
