package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.CustomerReturnAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.CustomerReturnProductsTable;
import in.kumanti.emzor.eloquent.CustomerReturnTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CustomerReturn;
import in.kumanti.emzor.model.CustomerReturnBatch;
import in.kumanti.emzor.model.CustomerReturnProducts;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.InvoiceProductItems;
import in.kumanti.emzor.model.Invoices;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.utils.Constants.REQUEST_CODE_CUSTOMER_RETURN;

public class CustomerReturnActivity extends MainActivity {

    public RecyclerView.Adapter customerReturnProductsAdapter;
    public RecyclerView.LayoutManager customerReturnProductsLayoutManager;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionBarTitle, marqueeText, checkInTime;
    String login_id = "", customerCode = "", checkInTimeString = "";
    MyDBHandler dbHandler;
    TextView customerName, accountNo, city;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;
    TextView customerReturnNumber, customerReturnDate;
    String customerReturnIdString = null, customerReturnDateDB, customerReturnTime, invoiceId = null;
    String customerNameString, customerEmailAddress;
    String customerAddress1, customerAddress2, customerAddress3;
    int customerReturnId;
    int random_num = 0;
    CustomSearchableSpinner invoiceNumbSpinner, customerReturnProductsSpinner;
    LinearLayout invoiceContainer, productsContainer;
    RecyclerView customerReturnProductsRecyclerView;
    ImageView saveCustomerReturnButton, createSignature, viewSignature;
    Bitmap selectedImage = null;
    int line_count;
    CustomerReturnTable customerReturnTable;
    CustomerReturnProductsTable customerReturnProductsTable;
    TextView totalReturnValue, returnNetVale, returnDiscountValue, balanceAmount;
    EditText paidAmount, signatureName;
    ArrayList<CustomerReturnProducts> customerReturnProductsArrayList;
    CheckBox changePaidCheckBox, changeAccountCheckBox;
    String formatTotalReturnValue, formatNetValue, formatDiscountValue, formatPaidAmount, formatBalanceAmount, changeType, paymentMode;
    String invoiceCode;
    double returnValueDouble = 0.00, balanceAmountDouble = 0.00;
    int discountValueInteger = 0;
    Globals globals;
    long stopTime = 0;
    DecimalFormat formatter, formatter1;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng, nav_type, customer_type;
    String tab_prefix;
    double order_val = 0.00;
    double total_val = 0.00;
    int discount_value = 0;
    double price = 0.00;
    int discount = 0;
    String collection_num, collection_type;
    String format_paid_value;
    String format_balance_value;
    double paid_amount = 0.00;
    double due_amount = 0.00;
    String emailType = "CustomerReturn";
    String companyName,companyCode1, compAdd1, compAdd2, compAdd3, compMblNo;
    String balanceReturned = "", balanceReturnedVal = "", balanceReturnedKey = "";
    //Variables declaration for print and sms
    Button smsButton, printButton, emailButton;
    String customerPhoneNumber = "";
    String smsMessageText = "";
    // android built in classes for bluetooth operations
    PrintBluetooth printBluetooth;
    Print print;
    String printerName = "MP80-17031810239";
    String printMsg;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    String customerReturnStatus = "";
    GeneralSettingsTable generalSettingsTable;
    GeneralSettings gs;
    ArrayAdapter<Invoices> invoiceSpinnerArrayAdapter;
    ArrayList<Invoices> invoiceArrayList;
    ArrayAdapter<InvoiceProductItems> customerReturnProductsArrayAdapter;
    ArrayList<InvoiceProductItems> returnProductsArrayList;
    ArrayList<InvoiceProductItems> invoiceDetailsArrayList;
    RecyclerViewItemListener listener, batchListener;
    Double totalReturnValues = 0d;
    Double totalNetValues = 0d;
    Double totalDiscount = 0d;
    int filteredPosition = -1;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_customer_stock_return);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        customerReturnDate();
        generateCustomerReturnNumber();

        actionBarTitle.setText("Customer Return");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        listener = (view, position) -> {
            calculateTotalValues();
        };

        batchListener = (view, position) -> {
            if (filteredPosition != -1)
                position = filteredPosition;
            Intent intent = new Intent(getApplicationContext(), CustomerReturnBatchActivity.class);
            intent.putExtra("product_id", invoiceDetailsArrayList.get(position).getProduct_id());
            intent.putExtra("product_name", invoiceDetailsArrayList.get(position).getProduct());
            intent.putExtra("product_quantity", invoiceDetailsArrayList.get(position).getReturnQuantity());
            intent.putExtra("product_batch_details", invoiceDetailsArrayList.get(position).getCustomerReturnBatchArrayList());
            startActivityForResult(intent, 2);
        };

        customerReturnProductsRecyclerView.setHasFixedSize(true);
        customerReturnProductsLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        customerReturnProductsRecyclerView.setLayoutManager(customerReturnProductsLayoutManager);

        invoiceArrayList = dbHandler.getInvoiceHeaderForCustomerReturn(customerCode);
        Invoices i = new Invoices();
        i.setInvoiceCode("Select");
        invoiceArrayList.add(0, i);

        invoiceSpinnerArrayAdapter = new ArrayAdapter<Invoices>(getApplicationContext(), android.R.layout.simple_spinner_item, invoiceArrayList);
        invoiceSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        invoiceNumbSpinner.setAdapter(invoiceSpinnerArrayAdapter);

        invoiceNumbSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                invoiceNumbSpinner.isSpinnerDialogOpen = false;

                Invoices invoices = (Invoices) adapterView.getItemAtPosition(pos);
                if (!invoices.getInvoiceCode().equals("Select")) {
                    invoiceCode = invoices.getInvoiceCode();
                    invoiceId = invoices.getInvoiceId();
                    initCustomerReturnData(dbHandler.getinvoiceDetailsList(invoiceCode));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                invoiceNumbSpinner.isSpinnerDialogOpen = false;
            }
        });

        customerReturnProductsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                InvoiceProductItems invoicesProduct = (InvoiceProductItems) adapterView.getItemAtPosition(pos);
                if (!invoicesProduct.getProduct().equals("Select")) {
                    ArrayList<InvoiceProductItems> invDetailArrayList = new ArrayList<InvoiceProductItems>();
                    invDetailArrayList.add(invoicesProduct);
                    filteredPosition = pos;
                    customerReturnProductsAdapter = new CustomerReturnAdapter(invDetailArrayList, listener, batchListener);
                    customerReturnProductsRecyclerView.setAdapter(customerReturnProductsAdapter);
                    customerReturnProductsAdapter.notifyDataSetChanged();
                } else {
                    filteredPosition = -1;
                    customerReturnProductsAdapter = new CustomerReturnAdapter(invoiceDetailsArrayList, listener, batchListener);
                    customerReturnProductsRecyclerView.setAdapter(customerReturnProductsAdapter);
                    customerReturnProductsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                customerReturnProductsSpinner.isSpinnerDialogOpen = false;
            }
        });

        line_count = dbHandler.get_count_invoice_lines(invoiceId);

        saveCustomerReturnButton.setOnClickListener(v -> {

            gs = generalSettingsTable.getSettingByKey("customerStockReturn");

            if (gs != null && gs.getValue().equals("Yes")) {
                if (TextUtils.isEmpty(signatureName.getText())) {
                    Toast.makeText(getApplicationContext(), "Please Enter Signee's Name", Toast.LENGTH_LONG).show();
                    return;
                } else if (selectedImage == null) {
                    Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                    return;
                }

            }


            if ("".equals(paidAmount.getText().toString())) {
                paidAmount.setError(getString(R.string.error_field_required));
                return;
            } else if ("".equals(changeType)) {
                Toast.makeText(getApplicationContext(), "Please Select Change Type", Toast.LENGTH_LONG).show();
                return;
            } else if (invoiceDetailsArrayList.size() == 0) {
                Toast.makeText(getApplicationContext(), "Add Product to Save the Customer Return", Toast.LENGTH_LONG).show();
                return;
            }

            boolean validCheck = true;
            int totalProductReturnQuantity = 0;
            for (InvoiceProductItems ipi : invoiceDetailsArrayList) {
                int totalQuantity = 0;
                if (ipi.getProduct_id().equals("-1"))
                    continue;
                if (ipi.getBatch_controlled() != null && ipi.getBatch_controlled().equals("Yes") && !TextUtils.isEmpty(ipi.getReturnQuantity())) {

                    for (CustomerReturnBatch crp : ipi.getCustomerReturnBatchArrayList()) {
                        if (!TextUtils.isEmpty(crp.getReturnQuantity()))
                            totalQuantity += Integer.parseInt(crp.getReturnQuantity());
                    }
                    if (totalQuantity != Integer.parseInt(ipi.getReturnQuantity())) {
                        ipi.setErrorProduct("Please add Batch Return Quantity");
                        validCheck = false;
                    }
                } else {
                    ipi.setErrorProduct(null);
                }
                if (!TextUtils.isEmpty(ipi.getReturnQuantity()))
                    totalProductReturnQuantity += Integer.parseInt(ipi.getReturnQuantity());

            }

            if (!validCheck) {
                initRecyclerView();
                return;
            }

            if (totalProductReturnQuantity == 0) {
                initRecyclerView();
                Toast.makeText(getApplicationContext(), "Please enter the return quantity for at least one product", Toast.LENGTH_LONG).show();
                return;
            }

            final Dialog alertbox = new Dialog(v.getRootView().getContext());

            LayoutInflater mInflater = (LayoutInflater)
                    getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            final View alertLayout = mInflater.inflate(R.layout.customer_return_save_confirmation, null);

            alertbox.setCancelable(false);
            alertbox.setContentView(alertLayout);

            final Button Yes = alertLayout.findViewById(R.id.stockReturnYesButton);
            final Button No = alertLayout.findViewById(R.id.stockReturnNoButton);

            Yes.setOnClickListener(v1 -> {
                calculateTotalValues();
                Toast.makeText(getApplicationContext(), "Customer Return saved Successfully", Toast.LENGTH_LONG).show();
                alertbox.dismiss();

                String visitSequence = sharedPreferences.getString("VisitSequence", "");


                CustomerReturn customerReturn = new CustomerReturn();
                customerReturn.setCustomerReturnNumber(customerReturnNumber.getText().toString());
                customerReturn.setCustomerReturnDate(customerReturnDateDB);
                customerReturn.setCustomerCode(customerCode);
                customerReturn.setInvoiceNumber(invoiceCode);
                customerReturn.setReturnValue(totalReturnValue.getText().toString());
                customerReturn.setReturnDiscount(String.valueOf(totalDiscount));
                customerReturn.setReturnNetValue(String.valueOf(totalNetValues));
                customerReturn.setPaidAmount(paidAmount.getText().toString());
                customerReturn.setBalanceAmount(balanceAmount.getText().toString());
                customerReturn.setChangeType(changeType);
                customerReturn.setRandomNum(random_num);
                customerReturn.setVisitSeqNumber(visitSequence);

                if (selectedImage != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

                    customerReturn.setSignature(encodedImage);

                }

                customerReturn.setSigneeName(signatureName.getText().toString());

                customerReturnTable.create(customerReturn);
                returnValueDouble = Double.parseDouble(paidAmount.getText().toString());
                collection_type = "Against Invoice";
                paymentMode = "Cash";

                if (returnValueDouble == 0 || balanceAmountDouble == 0) {
                    if (returnValueDouble == 0)
                        paymentMode = "OnAccount";
                    insert_collection();
                } else if (returnValueDouble != 0 && balanceAmountDouble != 0) {
                    insert_collection();
                    if ("Account".equals(changeType)) {
                        if (returnValueDouble > totalNetValues) {
                            paymentMode = "Debit";
                        } else {
                            paymentMode = "Credit";
                        }
                        insert_unapplied_receipts();
                    }
                    /*if(returnValueDouble>totalNetValues) {
                        insert_collection();
                        if ("Account".equals(changeType)) {
                            paymentMode = "Cash";
                            insert_unapplied_receipts();
                        }
                    }
                    else {
                        paymentMode = "Cash";
                        insert_collection();
                        if ("Account".equals(changeType)) {
                            paymentMode = "";
                            insert_unapplied_receipts();
                        }
                    }*/

                }

                insertingToStockReceipt();
                insertingToCustomerStockReturnProductsTable();

                saveCustomerReturnButton.setVisibility(View.GONE);
                createSignature.setEnabled(false);
                signatureName.setEnabled(false);
                paidAmount.setEnabled(false);
                customerReturnStatus = "Completed";
                invoiceContainer.setVisibility(View.GONE);
                productsContainer.setVisibility(View.GONE);


            });

            No.setOnClickListener(v12 -> alertbox.dismiss());

            alertbox.show();
        });

        customerImage.setOnLongClickListener(v -> {
            showcustomerLocation();
            return true;
        });


        createSignature.setOnClickListener(v -> createNewSignature());


        viewSignature.setOnClickListener(v -> {

            if (selectedImage != null) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.image_preview, null);
                ImageView capturedImage, closeButton;
                capturedImage = alertLayout.findViewById(R.id.capturedImage);
                closeButton = alertLayout.findViewById(R.id.closeImageView);
                capturedImage.setImageBitmap(selectedImage);
                android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                // this is set the view from XML inside AlertDialog

                alert.setView(alertLayout);
                alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);

                android.app.AlertDialog dialog = alert.create();
                dialog.show();


            }

        });

        /*
         * Sending SMS Manager API using default SMS manager
         */
        smsButton.setOnClickListener(v -> {

            if (!customerReturnStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save Return Products to send sms", Toast.LENGTH_LONG).show();
                return;
            }
            if (TextUtils.isEmpty(customerPhoneNumber)) {
                Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                return;
            }
            calculateBalanceReturn();
            Log.d("SMSTEST", "TEST " + balanceReturned);

            smsMessageText =  "Customer Return\n" +
                    "\n" +
                    "Thanks for trusting " + Constants.COMPANY_NAME + " Products." +
                    "\n" +
                    "Please refer your"+
                    "\n" +
                    "Cust Return No: #" + customerReturnNumber.getText().toString() + "\n" +
                    "Return Date: " + customerReturnDate.getText().toString() + "\n" +
                    "Return Value: " + "NGN " +  totalReturnValue.getText().toString() + "\n" +
                    "Paid Value: " + "NGN " +  paidAmount.getText().toString() + "\n" +
                    "Balance: " + "NGN " + balanceAmount.getText().toString() + "\n" +
                    balanceReturned + "\n" +
                    "care.nigeria@artemislife.com"+ "\n" ;

            Gson gson = new GsonBuilder().setLenient().create();
            JsonArray returnProductsJsonArray = gson.toJsonTree(invoiceDetailsArrayList).getAsJsonArray();

            JsonObject jp = new JsonObject();

            jp.addProperty("smsType", emailType);
            jp.addProperty("smsMessageText", smsMessageText);
            jp.addProperty("companyName", companyName);
            jp.addProperty("companyAddLine1", compAdd1);
            jp.addProperty("companyAddLine1", compAdd2);
            jp.addProperty("companyAddLine1", compAdd3);
            jp.addProperty("companyMblNo", compMblNo);
            jp.addProperty("customerReturnNumber", customerReturnNumber.getText().toString());
            jp.addProperty("date", customerReturnDate.getText().toString());
            jp.addProperty("time", customerReturnTime);
            jp.addProperty("bdeName", sr_name_details);
            jp.addProperty("customerName", customerNameString);
            jp.addProperty("customerPhoneNumber", customerPhoneNumber);
            jp.addProperty("address", customerAddress1 + customerAddress2);
            jp.addProperty("totalValue", totalReturnValue.getText().toString());
            jp.addProperty("cashPaid", paidAmount.getText().toString());
            jp.addProperty("balance",  balanceAmount.getText().toString());
            jp.addProperty(balanceReturnedKey, balanceReturnedVal);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.SMS_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);
            Call<ResponseBody> call = api.postingCustomerReturnSMSData(jp);
            Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.d("EMAIL SEND:", "Success----" + response.body().string());
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("EMAIL SEND: ", "failure------" + t);
                    Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });


       /*     try {
                smsMessageText = "Customer Return\n" +
                        "\n" +
                        "Customer Return No: #" + customerReturnNumber.getText().toString() + "\n" +
                        "Return Date: " + customerReturnDate.getText().toString() + "\n" +
                        "Return Time: " + customerReturnTime + "\n" +
                       // "Return Lines: " + line_count + "\n" +
                        "Return Value: " + totalReturnValue.getText().toString() + "\n" +
                        "Paid Value: " + paidAmount.getText().toString() + "\n" +
                        "Balance: " + balanceAmount.getText().toString() + "\n" +
                        balanceReturned + "\n";

                Log.d("SMSTEST", "TESTMESSAGE" + smsMessageText);

                SmsManager smsManager = SmsManager.getDefault();
                ArrayList<String> parts = smsManager.divideMessage(smsMessageText);
                smsManager.sendMultipartTextMessage(customerPhoneNumber, null, parts, null, null);

                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Log.d("Sms", "Exception" + e);
            }
*/

        });


        /*
         * Print the order
         */
        printButton.setOnClickListener(v -> {

            if (!customerReturnStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save Return Products to print", Toast.LENGTH_LONG).show();
                return;
            }

            try {
                calculateBalanceReturn();
                calculateTotalValues();

                int totalSize = 48;
                String ESC_NEW_LINE = "\n";
                String horizontalLine = "-----------------------------------------------";
                printMsg = "";
                printMsg += print.centerString(totalSize, companyName);
                printMsg += print.centerString(totalSize, compAdd1);
                printMsg += print.centerString(totalSize, compAdd2);
                printMsg += print.centerString(totalSize, compAdd3);
                printMsg += print.centerString(totalSize, compMblNo);

                printMsg += ESC_NEW_LINE + horizontalLine;
                printMsg += ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;

                printMsg += ESC_NEW_LINE + "Customer Return Number:" + customerReturnNumber;
                printMsg += ESC_NEW_LINE + print.padRight("Date:" + customerReturnDate.getText(), 24) + print.padLeft("TIME:" + customerReturnTime, 24);
                printMsg += "BDE Name:" + sr_name_details;
                printMsg += ESC_NEW_LINE + "Customer Name:" + customerNameString;
                printMsg += ESC_NEW_LINE + "Address:";
                printMsg += print.padRight(customerAddress1, totalSize);
                printMsg += print.padRight(customerAddress2, totalSize);
                printMsg += print.padRight(customerAddress3, totalSize);

                printMsg += ESC_NEW_LINE + horizontalLine;

                printMsg += print.centerString(15, "Product Name") + " " + "  VOL " + print.padLeft("PRICE", 10) + "  " + print.padLeft("VALUE", 14);

                printMsg += ESC_NEW_LINE + horizontalLine;

                totalReturnValues = 0d;
                totalNetValues = 0d;
                totalDiscount = 0d;

                for (InvoiceProductItems ipi : invoiceDetailsArrayList) {
                    if (!ipi.getProduct().equals("Select") && !TextUtils.isEmpty(ipi.getReturnQuantity())) {
                        discount_value = 0;
                        int vat = 0;
                        String quan = ipi.getReturnQuantity();
                        Double amnt = ipi.getPrice_db();

                        Log.d("CusReturnProducts****", "ArrayList Size----" +invoiceDetailsArrayList.size());
                        Log.d("CusReturnProducts****", "ArrayList Items----" +invoiceDetailsArrayList.toString());
                        Log.d("CusReturnProducts****", "Product Price----" +ipi.getPrice_db());
                        Log.d("CusReturnProducts****", "Return Qty----" +ipi.getReturnQuantity());
                        Log.d("CusReturnProducts****", "Total Return Values----" +totalReturnValues);

                        if (ipi.getDiscount().equals("")) {
                            totalReturnValues += Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                            totalNetValues = totalReturnValues;
                        } else {
                            String disc = ipi.getDiscount();
                            totalReturnValues += Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                            Double value = Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100
                                    - Integer.valueOf(quan) * amnt
                                    * Integer.valueOf(disc) / 100;
                            totalNetValues += value;
                            totalDiscount += ((Integer.valueOf(quan) * amnt) + (vat * Integer.valueOf(quan) * amnt) / 100) - value;
                        }
                    }
                }

                formatTotalReturnValue = formatter.format(totalReturnValues);
                formatNetValue = formatter.format(totalNetValues);
                formatDiscountValue = formatter.format(totalDiscount);

                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE + horizontalLine;
               // printMsg += print.centerString(34,"Total Value:")+print.padLeft(String.valueOf(total),14);
                //printMsg += print.centerString(34,"VAT:")+print.padLeft(formatter.format(vatValue),14);
                //printMsg += print.centerString(34,"Discounts:")+print.padLeft(String.valueOf(total_discount1),14);
               // printMsg += print.centerString(35,"Net Value:")+print.padLeft(net_value.getText().toString(),13);

                printMsg += print.padRight("", 15) + print.padRight("Return Value:", 19) + print.padLeft(String.valueOf(totalReturnValues), 14);
               // printMsg += print.padRight("", 15) + print.padRight("VAT:", 19) + print.padLeft(formatter.format(va), 14);
               // printMsg += print.padRight("", 15) + print.padRight("Discounts:", 19) + print.padLeft(String.valueOf(total_discount1), 14);
               // printMsg += print.padRight("", 15) + print.padRight("Net Value:", 19) + print.padLeft(net_value.getText().toString(), 14);


                printMsg += ESC_NEW_LINE + horizontalLine;

                printMsg += ESC_NEW_LINE + print.padRight("Cash Paid:" + format_paid_value, 24) + print.padLeft("Balance:" + format_balance_value, 24);

                printMsg += ESC_NEW_LINE + balanceReturned;

                printMsg += ESC_NEW_LINE + horizontalLine;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE + horizontalLine;
                printMsg += print.padLeft("Customer Sign", totalSize);
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                print.printContent(printMsg);
                //printBluetooth.printContent(printMsg, selectedImage);


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        /*
         * Email the order
         */
        emailButton.setOnClickListener(v -> {

            if (!customerReturnStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save Return Products to send email", Toast.LENGTH_LONG).show();
                return;
            }
            calculateBalanceReturn();
            calculateTotalValues();

            totalReturnValues = 0d;
            totalNetValues = 0d;
            totalDiscount = 0d;

            for (InvoiceProductItems ipi : invoiceDetailsArrayList) {
                if (!ipi.getProduct().equals("Select") && !TextUtils.isEmpty(ipi.getReturnQuantity())) {
                    discount_value = 0;
                    int vat = 0;
                    String quan = ipi.getReturnQuantity();
                    Double amnt = ipi.getPrice_db();

                    Log.d("CusReturnProducts****", "ArrayList Size----" +invoiceDetailsArrayList.size());
                    Log.d("CusReturnProducts****", "ArrayList Items----" +invoiceDetailsArrayList.toString());
                    Log.d("CusReturnProducts****", "Product Price----" +ipi.getPrice_db());
                    Log.d("CusReturnProducts****", "Return Qty----" +ipi.getReturnQuantity());
                    Log.d("CusReturnProducts****", "Total Return Values----" +totalReturnValues);

                    if (ipi.getDiscount().equals("")) {
                        totalReturnValues += Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                        totalNetValues = totalReturnValues;
                    } else {
                        String disc = ipi.getDiscount();
                        totalReturnValues += Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                        Double value = Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100
                                - Integer.valueOf(quan) * amnt
                                * Integer.valueOf(disc) / 100;
                        totalNetValues += value;
                        totalDiscount += ((Integer.valueOf(quan) * amnt) + (vat * Integer.valueOf(quan) * amnt) / 100) - value;
                    }
                }
            }

            formatTotalReturnValue = formatter.format(totalReturnValues);
            formatNetValue = formatter.format(totalNetValues);
            formatDiscountValue = formatter.format(totalDiscount);

            Gson gson = new GsonBuilder().setLenient().create();
            JsonArray returnProductsJsonArray = gson.toJsonTree(invoiceDetailsArrayList).getAsJsonArray();

            JsonObject jp = new JsonObject();

            jp.addProperty("emailType", emailType);
            jp.addProperty("companyName", companyName);
            jp.addProperty("companyCode", companyCode1);
            jp.addProperty("companyAddLine1", compAdd1);
            jp.addProperty("companyAddLine1", compAdd2);
            jp.addProperty("companyAddLine1", compAdd3);
            jp.addProperty("companyMblNo", compMblNo);
            jp.addProperty("customerReturnNumber", customerReturnNumber.getText().toString());
            jp.addProperty("date", customerReturnDate.getText().toString());
            jp.addProperty("time", customerReturnTime);
            jp.addProperty("bdeName", sr_name_details);
            jp.addProperty("customerName", customerNameString);
            jp.addProperty("customerEmailAddress", customerEmailAddress);
            jp.addProperty("address", customerAddress1 + customerAddress2);
            jp.add("returnProducts", returnProductsJsonArray);
            jp.addProperty("totalValue", String.valueOf(formatTotalReturnValue));
            //jp.addProperty("vat", formatter.format(vatValue));
            jp.addProperty("discounts", String.valueOf(totalDiscount));
            jp.addProperty("netValue", returnNetVale.getText().toString());
            jp.addProperty("cashPaid", format_paid_value);
            jp.addProperty("balance", format_balance_value);
            //jp.addProperty("balanceReturned", balanceReturned);
            jp.addProperty(balanceReturnedKey, balanceReturnedVal);



            if(selectedImage!=null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] byteArrayImage = baos.toByteArray();
                String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                jp.addProperty("customerSignature", encodedImage);
            }
            Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);
            Call<ResponseBody> call = api.postingEmailData(jp);

            Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.d("EMAIL SEND:", "Success----" + response.body().string());
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("EMAIL SEND: ", "failure------" + t);
                    Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });


        });


        actionbarBackButton.setOnClickListener(v -> {
           /* long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();*/

            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();

        });


        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

        changePaidCheckBox.setOnClickListener(v -> {
            changeAccountCheckBox.setChecked(false);
            changePaidCheckBox.setChecked(true);
            changeType = "Paid";
        });

        changeAccountCheckBox.setOnClickListener(v -> {
            changePaidCheckBox.setChecked(false);
            changeAccountCheckBox.setChecked(true);
            changeType = "Account";

        });

        paidAmount.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (paidAmount.getText().toString().equals("")) {

                    balanceAmount.setText("");
                    changePaidCheckBox.setChecked(false);
                    changeAccountCheckBox.setChecked(false);
                    changePaidCheckBox.setEnabled(true);
                    changeAccountCheckBox.setEnabled(true);

                } else {

                    double quan = Double.parseDouble(paidAmount.getText().toString());
                    double amnt = totalReturnValues;

                    String paidFormat = formatter1.format(quan);
                    String netValueFormat = formatter1.format(amnt);

                    double paidDoubleformat = Double.parseDouble(paidFormat);
                    double netValueDoubleformat = Double.parseDouble(netValueFormat);

                    format_paid_value = paidFormat;

                    if (paidDoubleformat > netValueDoubleformat) {

                        balanceAmountDouble = netValueDoubleformat - paidDoubleformat;

                        String bal_formatted = formatter.format(balanceAmountDouble);
                        format_balance_value = bal_formatted;
                        balanceAmount.setText(bal_formatted);
                        if (balanceAmountDouble < 0) {
                            changePaidCheckBox.setChecked(false);
                            changeAccountCheckBox.setChecked(false);
                            changePaidCheckBox.setEnabled(true);
                            changeAccountCheckBox.setEnabled(true);
                            changeType = "";

                        }
                    } else {
                        balanceAmountDouble = netValueDoubleformat - paidDoubleformat;

                        String bal_formatted = formatter.format(balanceAmountDouble);
                        balanceAmount.setText(bal_formatted);
                        format_balance_value = bal_formatted;

                        if (".00".equals(bal_formatted)) {
                            balanceAmount.setText("0");
                            format_balance_value = "0.00";
                            changePaidCheckBox.setChecked(true);
                            changeAccountCheckBox.setChecked(false);
                            changePaidCheckBox.setEnabled(false);
                            changeAccountCheckBox.setEnabled(false);
                            balanceAmountDouble = 0;
                            changeType = "Paid";

                        } else {

                            changePaidCheckBox.setChecked(false);
                            changeAccountCheckBox.setChecked(true);
                            changePaidCheckBox.setEnabled(false);
                            changeAccountCheckBox.setEnabled(false);
                            changeType = "Account";

                        }

                    }

                }
            }
        });

    }

    private void insert_collection() {

        int collection_random_num = dbHandler.get_collection_random_num("Secondary");
        int collection_id = dbHandler.get_collection_sequence_value("Secondary");
        collection_id = collection_id + 1;
        collection_random_num = collection_random_num + 1;

        SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("ddMMyy");
        Date myDate1 = new Date();
        String collection_seq_date = timeStampFormat1.format(myDate1);

        if (collection_random_num <= 9) {
            collection_num = tab_prefix + "SC" + collection_seq_date + "00" + collection_random_num;

        } else if (collection_random_num > 10 & collection_random_num < 99) {
            collection_num = tab_prefix + "SC" + collection_seq_date + "0" + collection_random_num;

        } else {
            collection_num = tab_prefix + "SC" + collection_seq_date + collection_random_num;
        }

        long id1 = dbHandler.insert_collection_details
                (login_id,
                        customerCode,
                        collection_id,
                        collection_num,
                        customerReturnDateDB,
                        totalNetValues,
                        0,
                        balanceAmountDouble,
                        collection_type,
                        returnValueDouble,
                        null,
                        paymentMode,
                        "",
                        "",
                        "",
                        invoiceId,
                        invoiceCode,
                        String.valueOf(collection_random_num),
                        "",
                        "",
                        "",
                        "",
                        customerReturnDateDB,
                        Constants.VISIT_SEQ_NUMBER,
                        "Return",
                        null
                );

        dbHandler.update_collection_sequence(collection_id);

    }

    private void insertingToStockReceipt() {
        int rec_random_num = dbHandler.get_receipt_random_num();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);
        String receipt_num;
        rec_random_num = rec_random_num + 1;

        if (rec_random_num <= 9) {
            receipt_num = tab_prefix + "SR" + rec_seq_date + "00" + rec_random_num;

        } else if (rec_random_num > 9 & random_num < 99) {
            receipt_num = tab_prefix + "SR" + rec_seq_date + "0" + rec_random_num;

        } else {
            receipt_num = tab_prefix + "SR" + rec_seq_date + rec_random_num;
        }

        String visitSequence = sharedPreferences.getString("VisitSequence", "");

        for (InvoiceProductItems ipi : invoiceDetailsArrayList) {
            if (ipi.getProduct_id().equals("-1"))
                continue;
            if (!TextUtils.isEmpty(ipi.getReturnQuantity())) {
                long id = dbHandler.createstockreciept(
                        receipt_num,
                        customerReturnDateDB,
                        "NGN",
                        login_id,
                        null,
                        "Return",
                        "",
                        ipi.getProduct_id(),
                        ipi.getUom(),
                        ipi.getReturnQuantity(),
                        ipi.getPrice(),
                        ipi.getValue(),
                        "",
                        invoiceId,
                        "Completed",
                        rec_random_num,
                        visitSequence,
                        customerReturnNumber.getText().toString(),
                        "");

                if (ipi.getBatch_controlled().equals("Yes")) {
                    for (CustomerReturnBatch crb : ipi.getCustomerReturnBatchArrayList()) {
                        dbHandler.insertingProductBatch(crb.getProductCode(), crb.getBatchNumber(), crb.getExpiryDate(), crb.getReturnQuantity(), invoiceCode, receipt_num, invoiceId, "Completed", "CustomerReturn");
                    }
                }
            }


        }


    }

    private void insertingToCustomerStockReturnProductsTable() {

        String visitSequence = sharedPreferences.getString("VisitSequence", "");

        for (InvoiceProductItems ipi : invoiceDetailsArrayList) {
            if (ipi.getProduct_id().equals("-1"))
                continue;
            if (!TextUtils.isEmpty(ipi.getReturnQuantity())) {

                CustomerReturnProducts crpt = new CustomerReturnProducts();
                crpt.setCustomerReturnNumber(customerReturnNumber.getText().toString());
                crpt.setCustomerReturnDate(customerReturnDateDB);
                crpt.setProductCode(ipi.getProduct_id());
                crpt.setProductName(ipi.getProduct());
                crpt.setProductUom(ipi.getUom());
                crpt.setAvailQuantity(ipi.getQuantity());
                crpt.setReturnQuantity(ipi.getReturnQuantity());
                crpt.setReturnPrice(ipi.getPrice());
                crpt.setReturnValue(ipi.getValue());
                crpt.setCustomerName(customerName.getText().toString());

                crpt.setProductName(ipi.getProduct());

                customerReturnProductsTable.create(crpt);

            }


        }


    }

    private void insert_unapplied_receipts() {

        int collection_random_num = dbHandler.get_collection_random_num("Secondary");
        int collection_id = dbHandler.get_collection_sequence_value("Secondary");
        collection_id = collection_id + 1;
        collection_random_num = collection_random_num + 1;

        SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("ddMMyy");
        Date myDate1 = new Date();
        String collection_seq_date = timeStampFormat1.format(myDate1);

        if (collection_random_num <= 9) {
            collection_num = tab_prefix + "SC" + collection_seq_date + "00" + collection_random_num;

        } else if (collection_random_num > 10 & collection_random_num < 99) {
            collection_num = tab_prefix + "SC" + collection_seq_date + "0" + collection_random_num;

        } else {
            collection_num = tab_prefix + "SC" + collection_seq_date + collection_random_num;
        }


        long id1 = dbHandler.insert_collection_details
                (login_id,
                        customerCode,
                        collection_id,
                        collection_num,
                        customerReturnDateDB,
                        0.00,
                        0.00,
                        0.00,
                        "On Account",
                        balanceAmountDouble,
                        null,
                        paymentMode,
                        "",
                        "",
                        "",
                        "",
                        "",
                        String.valueOf(collection_random_num),
                        "",
                        "",
                        "",
                        "",
                        customerReturnDateDB,
                        Constants.VISIT_SEQ_NUMBER,
                        "Return",
                        null
                );

        dbHandler.update_collection_sequence(collection_id);

    }

    private void initCustomerReturnData(ArrayList<InvoiceProductItems> invList) {
        invoiceDetailsArrayList = invList;
        returnProductsArrayList = invoiceDetailsArrayList;
        InvoiceProductItems i = new InvoiceProductItems();
        i.setProduct("Select");
        i.setProduct_id("-1");
        returnProductsArrayList.add(0, i);
        //Set the Product Spinner
        customerReturnProductsArrayAdapter = new ArrayAdapter<InvoiceProductItems>(getApplicationContext(), android.R.layout.simple_spinner_item, returnProductsArrayList);
        customerReturnProductsArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        customerReturnProductsSpinner.setAdapter(customerReturnProductsArrayAdapter);

        customerReturnProductsAdapter = new CustomerReturnAdapter(invoiceDetailsArrayList, listener, batchListener);
        customerReturnProductsRecyclerView.setAdapter(customerReturnProductsAdapter);
        customerReturnProductsAdapter.notifyDataSetChanged();
    }

    private void initRecyclerView() {
        if (filteredPosition != -1) {
            ArrayList<InvoiceProductItems> invDetailArrayList = new ArrayList<InvoiceProductItems>();
            invDetailArrayList.add(invoiceDetailsArrayList.get(filteredPosition));
            customerReturnProductsAdapter = new CustomerReturnAdapter(invDetailArrayList, listener, batchListener);
            customerReturnProductsRecyclerView.setAdapter(customerReturnProductsAdapter);
            customerReturnProductsAdapter.notifyDataSetChanged();
        } else {
            customerReturnProductsAdapter = new CustomerReturnAdapter(invoiceDetailsArrayList, listener, batchListener);
            customerReturnProductsRecyclerView.setAdapter(customerReturnProductsAdapter);
            customerReturnProductsAdapter.notifyDataSetChanged();
        }
    }

    private void createNewSignature() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);
        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                selectedImage = signatureBitmap;
                viewSignature.setImageBitmap(signatureBitmap);
                dialog.dismiss();
            }
        });

    }

    public void calculateTotalValues() {
        totalReturnValues = 0d;
        totalNetValues = 0d;
        totalDiscount = 0d;

        for (InvoiceProductItems ipi : invoiceDetailsArrayList) {
            if (!ipi.getProduct().equals("Select") && !TextUtils.isEmpty(ipi.getReturnQuantity())) {
                discount_value = 0;
                int vat = 0;
                String quan = ipi.getReturnQuantity();
                Double amnt = ipi.getPrice_db();

                Log.d("CusReturnProducts****", "ArrayList Size----" +invoiceDetailsArrayList.size());
                Log.d("CusReturnProducts****", "ArrayList Items----" +invoiceDetailsArrayList.toString());
                Log.d("CusReturnProducts****", "Product Price----" +ipi.getPrice_db());
                Log.d("CusReturnProducts****", "Return Qty----" +ipi.getReturnQuantity());
                Log.d("CusReturnProducts****", "Total Return Values----" +totalReturnValues);



                if (ipi.getDiscount().equals("")) {
                    totalReturnValues += Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                    totalNetValues = totalReturnValues;
                } else {
                    String disc = ipi.getDiscount();
                    totalReturnValues += Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100;
                    Double value = Integer.valueOf(quan) * amnt + (vat * Integer.valueOf(quan) * amnt) / 100
                            - Integer.valueOf(quan) * amnt
                            * Integer.valueOf(disc) / 100;
                    totalNetValues += value;
                    totalDiscount += ((Integer.valueOf(quan) * amnt) + (vat * Integer.valueOf(quan) * amnt) / 100) - value;
                }
            }
        }

        formatTotalReturnValue = formatter.format(totalReturnValues);
        formatNetValue = formatter.format(totalNetValues);
        formatDiscountValue = formatter.format(totalDiscount);


        totalReturnValue.setText(String.valueOf(formatTotalReturnValue));
        returnNetVale.setText(String.valueOf(formatNetValue));
        returnDiscountValue.setText(String.valueOf(formatDiscountValue));
    }

    public void customerReturnDate() {
        String PATTERN = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        String PATTERN_DISPLAY = "dd MMM YY";
        SimpleDateFormat dateFormatDisplay = new SimpleDateFormat();
        dateFormatDisplay.applyPattern(PATTERN_DISPLAY);
        String dateDisplay = dateFormatDisplay.format(Calendar.getInstance().getTime());
        customerReturnDateDB = date;
        customerReturnDate.setText(dateDisplay);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        customerReturnTime = sdf.format(outDate);
    }

    public void generateCustomerReturnNumber() {
        random_num = customerReturnTable.getCustomerReturnNumber();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        Log.d("Stock Return", "Random No---" + random_num);

        if (random_num <= 9) {
            customerReturnNumber.setText(Constants.TAB_PREFIX + "CRT" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            customerReturnNumber.setText(Constants.TAB_PREFIX + "CRT" + rec_seq_date + "0" + random_num);

        } else {
            customerReturnNumber.setText(Constants.TAB_PREFIX + "CRT" + rec_seq_date + random_num);
        }

        customerReturnIdString = customerReturnNumber.getText().toString();

    }

    private void initializeViews() {

        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        /*duration = findViewById(R.id.durationValueTextView);
        checkInTime = findViewById(R.id.checkInTimeValueTextView);*/

        customerReturnNumber = findViewById(R.id.customerReturnNumberTv);
        customerReturnDate = findViewById(R.id.customerReturnDateTv);

        invoiceContainer = findViewById(R.id.invoiceContainer);
        invoiceNumbSpinner = findViewById(R.id.customerReturnInvoiceSpinner);

        productsContainer = findViewById(R.id.productContainer);
        customerReturnProductsSpinner = findViewById(R.id.customerReturnProductSpinner);

        customerReturnProductsRecyclerView = findViewById(R.id.customerReturnProductsRecyclerView);

        totalReturnValue = findViewById(R.id.customerReturnTotalReturnValueTv);
        returnNetVale = findViewById(R.id.customerReturnNetValueTv);
        returnDiscountValue = findViewById(R.id.customerReturnDiscountTV);

        paidAmount = findViewById(R.id.customerReturnPaidEt);
        balanceAmount = findViewById(R.id.customerReturnBalanceTv);

        changePaidCheckBox = findViewById(R.id.customerReturnPaidCheckBox);
        changeAccountCheckBox = findViewById(R.id.customerReturnAccountCheckBox);


        signatureName = findViewById(R.id.signCustomerNameEditText);
        createSignature = findViewById(R.id.CreateSignImageView);
        viewSignature = findViewById(R.id.ViewSignatureImageView);

        saveCustomerReturnButton = findViewById(R.id.customerReturnSaveButton);

        printButton = findViewById(R.id.customerReturnPrintButton);
        emailButton = findViewById(R.id.customerReturnEmailButton);
        smsButton = findViewById(R.id.customerReturnSmsButton);

        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("####.00");

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        customerReturnTable = new CustomerReturnTable(getApplicationContext());
        customerReturnProductsTable = new CustomerReturnProductsTable(getApplicationContext());

        tab_prefix = Constants.TAB_PREFIX;
        globals = ((Globals) getApplicationContext());

        printBluetooth = new PrintBluetooth(this);
        print = new Print(this);

        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());
        gs = generalSettingsTable.getSettingByKey("enablePrint");
        if (gs != null && gs.getValue().equals("Yes")) {
            printButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableSms");
        if (gs != null && gs.getValue().equals("Yes")) {
            smsButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableEmail");
        if (gs != null && gs.getValue().equals("Yes")) {
            emailButton.setVisibility(View.VISIBLE);
        }

        signatureName.setFilters(new InputFilter[]{filter});


    }

    public void populateHeaderDetails() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        /*duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();*/

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkInTimeString = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            customer_type = bundle.getString("customer_type");
            nav_type = bundle.getString("nav_type");

        }

        //checkInTime.setText(checkInTimeString);


        //Populate Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                customerNameString = customerDetails.getString(2);

                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }

        getCompanyInfo();

    }

    void calculateBalanceReturn() {
        balanceReturned = "";
        balanceReturnedKey = "";
        balanceReturnedVal = "";
        double bal_val = balanceAmountDouble;
        if (bal_val < 0)
            bal_val = bal_val * (-1);
        if (changePaidCheckBox.isChecked()) {
            balanceReturned = "Change Returned: " + String.valueOf(formatter.format(bal_val));
            balanceReturnedKey = "changeReturn";
            balanceReturnedVal = String.valueOf(formatter.format(bal_val));
        } else if (changeAccountCheckBox.isChecked()) {
            balanceReturned = "Account Adjusted: " + String.valueOf(formatter.format(bal_val));
            balanceReturnedKey = "accountAdjusted";
            balanceReturnedVal = String.valueOf(formatter.format(bal_val));
        }
    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);

            }
        }

    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("DataCheck", "Entered Child Fragment" + requestCode);
        //super.onActivityResult(requestCode, resultCode, data); //comment this unless you want to pass your result to the activity.
        if (requestCode == 2) {
            Log.d("DataCheck", "Entered in to Check");
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String product_id = (String) bundle.get("product_id");
                ArrayList<CustomerReturnBatch> product_batch_details = (ArrayList<CustomerReturnBatch>) bundle.get("product_batch_details");

                for (InvoiceProductItems ipi : invoiceDetailsArrayList) {
                    if (product_id.equals(ipi.getProduct_id())) {
                        Log.d("DataCheck", "Value Assigned in product");
                        ipi.setCustomerReturnBatchArrayList(product_batch_details);
                        int totalQuantity = 0;
                        boolean validCheck = true;
                        for (CustomerReturnBatch crp : ipi.getCustomerReturnBatchArrayList()) {
                            if (!TextUtils.isEmpty(crp.getReturnQuantity()))
                                totalQuantity += Integer.parseInt(crp.getReturnQuantity());
                        }
                        if (totalQuantity != Integer.parseInt(ipi.getReturnQuantity())) {
                            ipi.setErrorProduct("Please add Batch Return Quantity");
                            validCheck = false;
                        }
                        if (validCheck)
                            ipi.setErrorProduct(null);
                    }
                }
                initRecyclerView();

            }
        }
    }

    @Override
    public void onBackPressed() {

    }

}
