package in.kumanti.emzor.model;

public class ViewStockMyStockProduct {
    public String expiredCount;
    private String productId;
    private String productName;
    private String productUom;
    private String productQuantity;
    private String productPrice;
    private String productTotalPrice;
    private String secondaryInvoiceCount;
    private String batchControlled;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }


    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }


    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }


    public String getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(String productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }


    public String getSecondaryInvoiceCount() {
        return secondaryInvoiceCount;
    }

    public void setSecondaryInvoiceCount(String secondaryInvoiceCount) {
        this.secondaryInvoiceCount = secondaryInvoiceCount;
    }

    public String getBatchControlled() {
        return batchControlled;
    }

    public void setBatchControlled(String batchControlled) {
        this.batchControlled = batchControlled;
    }

    public String getExpiredCount() {
        return expiredCount;
    }

    public void setExpiredCount(String expiredCount) {
        this.expiredCount = expiredCount;
    }
}
