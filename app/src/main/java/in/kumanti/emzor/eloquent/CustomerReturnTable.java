package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.CustomerReturn;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_RETURN;

public class CustomerReturnTable extends SQLiteOpenHelper {

    private static final String COLUMN_CUS_STOCK_RETURN_ID = "customerReturnId";
    private static final String COLUMN_CUS_STOCK_RETURN_NUMBER = "customerReturnNum";
    private static final String COLUMN_CUS_STOCK_RETURN_DATE = "customerReturnDate";
    private static final String COLUMN_CUS_STOCK_RETURN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_CUS_STOCK_RETURN_INVOICE_NUMBER = "invoiceNumber";
    private static final String COLUMN_CUS_STOCK_RETURN_PRODUCTS = "returnProductsCount";
    private static final String COLUMN_CUS_STOCK_RETURN_VALUE = "returnValue";
    private static final String COLUMN_CUS_STOCK_RETURN_DISCOUNT = "returnDiscount";
    private static final String COLUMN_CUS_STOCK_RETURN_NET_VALUE = "returnNetValue";
    private static final String COLUMN_CUS_STOCK_RETURN_PAID_AMOUNT = "paidAmount";
    private static final String COLUMN_CUS_STOCK_RETURN_BALANCE_AMOUNT = "balanceAmount";
    private static final String COLUMN_CUS_STOCK_RETURN_CHANGE_TYPE = "changeType";
    private static final String COLUMN_CUS_STOCK_RETURN_SIGNATURE = "signature";
    private final static String COLUMN_CUS_STOCK_RETURN_IS_SYNC = "isSync";
    private final static String COLUMN_CUS_STOCK_RETURN_RANDOM_NUM = "randomNumber";
    private final static String COLUMN_CUS_STOCK_RETURN_EMAIL_SENT = "emailSent";
    private final static String COLUMN_CUS_STOCK_RETURN_SMS_SENT = "smsSent";
    private final static String COLUMN_CUS_STOCK_RETURN_PRINT_DONE = "printDone";
    private final static String COLUMN_CUS_STOCK_RETURN_VISIT_SEQUENCE_NO = "visitSeqNo";
    private static final String COLUMN_CUS_STOCK_RETURN_CREATED_AT = "createdAt";
    private static final String COLUMN_CUS_STOCK_RETURN_UPDATED_AT = "updatedAt";
    private static final String COLUMN_CUS_STOCK_RETURN_SIGNEE_NAME = "signeeName";

    public Context context;
    String selectQuery;
    Cursor cursor;
    private SQLiteDatabase sqlWrite;

    public CustomerReturnTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_STOCK_RETURN);

        String stockReturnTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER_RETURN + "(" +
                COLUMN_CUS_STOCK_RETURN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUS_STOCK_RETURN_NUMBER + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_DATE + " TEXT, " +

                COLUMN_CUS_STOCK_RETURN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_INVOICE_NUMBER + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_PRODUCTS + " INTEGER, " +

                COLUMN_CUS_STOCK_RETURN_VALUE + " NUMERIC, " +
                COLUMN_CUS_STOCK_RETURN_DISCOUNT + " NUMERIC, " +
                COLUMN_CUS_STOCK_RETURN_NET_VALUE + " NUMERIC, " +
                COLUMN_CUS_STOCK_RETURN_PAID_AMOUNT + " NUMERIC, " +
                COLUMN_CUS_STOCK_RETURN_BALANCE_AMOUNT + " NUMERIC, " +
                COLUMN_CUS_STOCK_RETURN_CHANGE_TYPE + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_SIGNATURE + " TEXT, " +

                COLUMN_CUS_STOCK_RETURN_IS_SYNC + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_RANDOM_NUM + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_EMAIL_SENT + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_SMS_SENT + " TEXT, " +
                COLUMN_CUS_STOCK_RETURN_PRINT_DONE + " TEXT, " +

                COLUMN_CUS_STOCK_RETURN_VISIT_SEQUENCE_NO + " TEXT, " +

                COLUMN_CUS_STOCK_RETURN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CUS_STOCK_RETURN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CUS_STOCK_RETURN_SIGNEE_NAME + " TEXT " +

                ");";
        sqlWrite.execSQL(stockReturnTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(CustomerReturn customerReturn) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUS_STOCK_RETURN_NUMBER, customerReturn.getCustomerReturnNumber());
        values.put(COLUMN_CUS_STOCK_RETURN_DATE, customerReturn.getCustomerReturnDate());

        values.put(COLUMN_CUS_STOCK_RETURN_CUSTOMER_CODE, customerReturn.getCustomerCode());
        values.put(COLUMN_CUS_STOCK_RETURN_INVOICE_NUMBER, customerReturn.getInvoiceNumber());
        values.put(COLUMN_CUS_STOCK_RETURN_PRODUCTS, customerReturn.getReturnProductsCount());

        values.put(COLUMN_CUS_STOCK_RETURN_VALUE, customerReturn.getReturnValue());
        values.put(COLUMN_CUS_STOCK_RETURN_DISCOUNT, customerReturn.getReturnDiscount());
        values.put(COLUMN_CUS_STOCK_RETURN_NET_VALUE, customerReturn.getReturnNetValue());
        values.put(COLUMN_CUS_STOCK_RETURN_PAID_AMOUNT, customerReturn.getPaidAmount());
        values.put(COLUMN_CUS_STOCK_RETURN_BALANCE_AMOUNT, customerReturn.getBalanceAmount());
        values.put(COLUMN_CUS_STOCK_RETURN_CHANGE_TYPE, customerReturn.getChangeType());

        values.put(COLUMN_CUS_STOCK_RETURN_SIGNATURE, customerReturn.getSignature());
        values.put(COLUMN_CUS_STOCK_RETURN_IS_SYNC, customerReturn.isSync());
        values.put(COLUMN_CUS_STOCK_RETURN_RANDOM_NUM, customerReturn.getRandomNum());

        values.put(COLUMN_CUS_STOCK_RETURN_EMAIL_SENT, customerReturn.getEmailSent());
        values.put(COLUMN_CUS_STOCK_RETURN_SMS_SENT, customerReturn.getSmsSent());
        values.put(COLUMN_CUS_STOCK_RETURN_PRINT_DONE, customerReturn.getPrintDone());

        values.put(COLUMN_CUS_STOCK_RETURN_VISIT_SEQUENCE_NO, customerReturn.getVisitSeqNumber());
        values.put(COLUMN_CUS_STOCK_RETURN_SIGNEE_NAME, customerReturn.getSigneeName());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CUSTOMER_RETURN, null, values);
        db.close();
        return id;
    }

    public int getCustomerReturnNumber() {
        int customerReturnId = 0;
        String selectQuery = "select randomNumber from xxmsales_customer_return where customerReturnDate=date('now') order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            customerReturnId = cursor.getInt(0);
            cursor.close();
        }

        return customerReturnId;
    }

    public ArrayList<CustomerReturn> getCustomerReturnDetailsForSync() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CUSTOMER_RETURN + " where customerReturnDate >= date('now','-5 Days') ";

        ArrayList<CustomerReturn> customerReturnArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            CustomerReturn customerReturn = new CustomerReturn();
            customerReturn.setTabCode(Constants.TAB_CODE);
            customerReturn.setCompanyCode(Constants.COMPANY_CODE);
            customerReturn.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            customerReturn.setCustomerReturnId(id);
            customerReturn.setCustomerReturnNumber(cursor.getString(1));
            customerReturn.setCustomerReturnDate(cursor.getString(2));
            customerReturn.setCustomerCode(cursor.getString(3));
            customerReturn.setInvoiceNumber(cursor.getString(4));
            customerReturn.setReturnProductsCount(cursor.getString(5));
            customerReturn.setReturnValue(cursor.getString(6));
            customerReturn.setReturnDiscount(cursor.getString(7));
            customerReturn.setReturnNetValue(cursor.getString(8));
            customerReturn.setPaidAmount(cursor.getString(9));
            customerReturn.setBalanceAmount(cursor.getString(10));
            customerReturn.setChangeType(cursor.getString(11));
            customerReturn.setSignature(cursor.getString(12));

            customerReturn.setVisitSeqNumber(cursor.getString(18));
            customerReturn.setCreatedAt(cursor.getString(19));
            customerReturn.setUpdatedAt(cursor.getString(20));

            customerReturnArrayList.add(customerReturn);
        }
        cursor.close();
        sqlWrite.close();
        return customerReturnArrayList;
    }

/*
    public ArrayList<Distributors> getDistributors() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMER_RETURN +" ORDER BY distributorName ASC" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        Log.d("ViewStock--","Get Products  " );
        ArrayList<Distributors> distributorsArrayList = new ArrayList<Distributors>();
        while (cursor.moveToNext()) {
            Distributors distributors = new Distributors();
            Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            distributors.setDistributorId(cursor.getInt(0));
            distributors.setDistributorCode(cursor.getString(1));
            distributors.setDistributorName(cursor.getString(2));

            distributorsArrayList.add(distributors);
        }
        cursor.close();
        sqlWrite.close();
        return distributorsArrayList;
    }

    public void deleteDistributors() {
        SQLiteDatabase db = this.getWritableDatabase();

        // Deleting all records from database table
        db.delete(TABLE_CUSTOMER_RETURN, null, null);
        db.close();
    }

    public ArrayList<StockReturn> getStockReturnDetails() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CUSTOMER_RETURN;

        ArrayList<StockReturn> stockReturnArrayList = new ArrayList<StockReturn>();
        int i = 0;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            StockReturn stockReturn = new StockReturn();
            stockReturn.setTabCode(tab_code);
            stockReturn.setCompanyCode(company_code);
            stockReturn.setSrCode(sr_code);

            int id = cursor.getInt(0);
            stockReturn.setStockReturnId(id);
            stockReturn.setStockReturnNumber(cursor.getString(1));
            stockReturn.setStockReturnDate(cursor.getString(2));
            stockReturn.setStockReturnType(cursor.getString(3));
            stockReturn.setVisitSeqNumber(cursor.getString(10));
            stockReturn.setCreatedAt(cursor.getString(11));
            stockReturn.setUpdatedAt(cursor.getString(12));


            stockReturnArrayList.add(stockReturn);
        }
        cursor.close();
        sqlWrite.close();
        return stockReturnArrayList;
    }*/


}
