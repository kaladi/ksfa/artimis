package in.kumanti.emzor.model;

import java.io.Serializable;

public class BdeReceiveBatchProducts implements Serializable {

    public int id;
    public String stockIssueNumber;
    public String stockIssueDate;
    public String errorIssueQuantity;
    private String toBdeCode;
    private String fromBdeCode;
    private String productCode;
    private String expiryDate;
    private String batchNumber;
    private String issueQuantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStockIssueNumber() {
        return stockIssueNumber;
    }

    public void setStockIssueNumber(String stockIssueNumber) {
        this.stockIssueNumber = stockIssueNumber;
    }

    public String getStockIssueDate() {
        return stockIssueDate;
    }

    public void setStockIssueDate(String stockIssueDate) {
        this.stockIssueDate = stockIssueDate;
    }

    public String getToBdeCode() {
        return toBdeCode;
    }

    public void setToBdeCode(String toBdeCode) {
        this.toBdeCode = toBdeCode;
    }

    public String getFromBdeCode() {
        return fromBdeCode;
    }

    public void setFromBdeCode(String fromBdeCode) {
        this.fromBdeCode = fromBdeCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getIssueQuantity() {
        return issueQuantity;
    }

    public void setIssueQuantity(String issueQuantity) {
        this.issueQuantity = issueQuantity;
    }

    public String getErrorIssueQuantity() {
        return errorIssueQuantity;
    }

    public void setErrorIssueQuantity(String errorIssueQuantity) {
        this.errorIssueQuantity = errorIssueQuantity;
    }
}
