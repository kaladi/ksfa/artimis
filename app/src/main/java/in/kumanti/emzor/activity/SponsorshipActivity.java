package in.kumanti.emzor.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.DoctorVisitSponsorship;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class SponsorshipActivity extends MainActivity {

    public Context context;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionBarTitle, marqueeText, checkInTime;
    String login_id = "", customerCode = "", checkInTimeString = "";
    TextView customerName, accountNo, city;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;
    CustomSearchableSpinner eventTypeSpinner, currencyTypeSpinner;
    String eventTypeString, currencyTypeString;
    EditText eventName, budgetAmount;
    TextView eventFromDate, eventToDate;
    Button eventDetailsAddButton;
    ImageView saveSponsorshipButton;
    int sponsorship_id = 0;
    RecyclerViewItemListener listener;
    ArrayList<DoctorVisitSponsorship> sponsorshipArrayList;
    ArrayList<DoctorVisitSponsorship> sponsorshipTypeArrayList;
    DoctorVisitSponsorship doctorVisitSponsorship;
    SponsorshipAdapter sponsorshipAdapter = null;
    DecimalFormat formatter;
    MyDBHandler dbHandler;
    private int mYear, mMonth, mDay;
    private int selectedStartDay, selectedStartMonth, selectedStartYear, selectedEndDay, selectedEndMonth, selectedEndYear;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_doctor_visit_sponsorship);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();


        actionBarTitle.setText(R.string.sponshorship_actionbar_header);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        sponsorship_id = dbHandler.get_sponsor_sequence_value();
        sponsorship_id = sponsorship_id + 1;

        loadingEventTypeSpinnerData();

        getCalendarValue();

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                eventTypeSpinner.isSpinnerDialogOpen = false;
                eventTypeString = adapterView.getItemAtPosition(pos).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                eventTypeSpinner.isSpinnerDialogOpen = false;

            }
        });

        eventFromDate.setOnClickListener(v -> {

            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(SponsorshipActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> {

                        selectedStartYear = year;
                        selectedStartMonth = monthOfYear;
                        selectedStartDay = dayOfMonth;

                        try {
                            String PATTERN = "dd MMM YY";
                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            eventFromDate.setText(dateFormat.format(d));

                        } catch (Exception e) {

                        }

                    }, mDay, mMonth, mYear);
            if (eventToDate.getText().equals("")) {
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

                datePickerDialog.show();
            } else {
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                c.set(Calendar.DATE, selectedEndDay);
                c.set(Calendar.MONTH, selectedEndMonth);
                c.set(Calendar.YEAR, selectedEndYear);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                datePickerDialog.show();
            }


        });


        eventToDate.setOnClickListener(v -> {

            final Calendar c1 = Calendar.getInstance();
            mYear = c1.get(Calendar.YEAR);
            mMonth = c1.get(Calendar.MONTH);
            mDay = c1.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(SponsorshipActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> {

                        selectedEndYear = year;
                        selectedEndMonth = monthOfYear;
                        selectedEndDay = dayOfMonth;

                        try {
                            String PATTERN = "dd MMM YY";
                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            eventToDate.setText(dateFormat.format(d));

                        } catch (Exception e) {

                        }

                    }, mDay, mMonth, mYear);
            if (eventFromDate.getText().equals("")) {
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            } else {
                c1.set(Calendar.DATE, selectedStartDay);
                c1.set(Calendar.MONTH, selectedStartMonth);
                c1.set(Calendar.YEAR, selectedStartYear);
                datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
                datePickerDialog.show();
            }


        });

        currencyTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                currencyTypeSpinner.isSpinnerDialogOpen = false;

                currencyTypeString = adapterView.getItemAtPosition(pos).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter1) {
                currencyTypeSpinner.isSpinnerDialogOpen = false;
            }
        });

        sponsorship_details();


        saveSponsorshipButton.setOnClickListener(v -> {
            int sponsorships_count = dbHandler.get_count_sponsorships(customerCode);

            if (sponsorships_count == 0) {
                Toast.makeText(getApplicationContext(), "Add Records to Save", Toast.LENGTH_LONG).show();

            } else {
                dbHandler.updatesponsorshipstatus(customerCode, "Completed");
                Toast.makeText(getApplicationContext(), "Data Saved Successfully", Toast.LENGTH_LONG).show();

            }


        });

        eventDetailsAddButton.setOnClickListener(v -> {

            if ("Select".equals(eventTypeString)) {
                Toast.makeText(getApplicationContext(), "Select Type", Toast.LENGTH_LONG).show();

            } else if ("Select".equals(currencyTypeString)) {
                Toast.makeText(getApplicationContext(), "Select Currency", Toast.LENGTH_LONG).show();

            } else if ("".equals(eventName.getText().toString())) {
                eventName.setError("Enter Event Name");

            } else if ("".equals(budgetAmount.getText().toString())) {
                budgetAmount.setError("Enter Budget Amount");


            } else if ("".equals(eventFromDate.getText().toString())) {
                eventFromDate.setError("Enter From Date");

            } else if ("".equals(eventToDate.getText().toString())) {
                eventToDate.setError("Enter To Date");

            } else {

                String fromDate1 ="",toDate1="";
                DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat inputFormat = new SimpleDateFormat("dd MMM yy");
                Date fromDate = null,toDate = null;
                try {
                    fromDate = inputFormat.parse(eventFromDate.getText().toString());
                    toDate = inputFormat.parse( eventToDate.getText().toString());

                    fromDate1 = outputFormat.format(fromDate);
                    toDate1 = outputFormat.format(toDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                System.out.println("toDate = " + toDate1);

                long id1 = dbHandler.insert_dr_visit_sponsorship(
                        sponsorship_id,
                        eventTypeString,
                        eventName.getText().toString(),
                        fromDate1,
                        toDate1,
                        budgetAmount.getText().toString(),
                        customerCode,
                        "",
                        "Draft"
                );

                if (id1 <= 0) {
                    Toast.makeText(getApplicationContext(), "Sponsorship Failed to Save", Toast.LENGTH_LONG).show();
                } else {
                    dbHandler.update_sponsorship_id_sequence(sponsorship_id);
                    sponsorship_details();
                    eventName.setText("");
                    eventFromDate.setText("");
                    eventToDate.setText("");
                    budgetAmount.setText("");
                    loadingEventTypeSpinnerData();
                    eventFromDate.setError(null);
                    eventToDate.setError(null);

                }
            }

        });


        actionbarBackButton.setOnClickListener(v -> finish());

    }

    private void populateHeaderDetails() {


        Bundle bundle = getIntent().getExtras();
        customerCode = bundle.getString("customer_id");
        login_id = bundle.getString("login_id");
        checkInTimeString = bundle.getString("checkin_time");


        checkInTime.setText(checkInTimeString);


        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        //Populate Customer Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

            }
        }

    }

    private void loadingEventTypeSpinnerData() {

        sponsorshipTypeArrayList = dbHandler.getSponsorshiptype();
        DoctorVisitSponsorship p = new DoctorVisitSponsorship();
        p.setType("Select Type");
        sponsorshipTypeArrayList.add(0, p);

        List<String> list = new ArrayList<>();

        list.add("Select");
        list.add("Study Tour");
        list.add("Exhibition");
        list.add("Conference");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        eventTypeSpinner.setAdapter(dataAdapter);

        List<String> list1 = new ArrayList<>();

        list1.add("Select");
        list1.add("₦");
        list1.add("$");
        list1.add("€");
        list1.add("₹");
        list1.add("R");

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list1);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        currencyTypeSpinner.setAdapter(dataAdapter1);

    }


    public void getCalendarValue() {

        final Calendar c = Calendar.getInstance();
        selectedEndDay = selectedStartDay = c.get(Calendar.DAY_OF_MONTH);
        selectedEndMonth = selectedStartMonth = c.get(Calendar.MONTH);
        selectedEndYear = selectedStartYear = c.get(Calendar.YEAR);

    }

    private void initializeViews() {

        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        duration = findViewById(R.id.durationValueTextView);
        checkInTime = findViewById(R.id.checkInTimeValueTextView);

        eventTypeSpinner = findViewById(R.id.eventTypeSpinner);
        eventName = findViewById(R.id.eventNameEditText);

        currencyTypeSpinner = findViewById(R.id.currencySpinner);

        eventFromDate = findViewById(R.id.eventFromDateButton);
        eventToDate = findViewById(R.id.eventToDateButton);

        budgetAmount = findViewById(R.id.budgetValueEditText);

        eventDetailsAddButton = findViewById(R.id.eventInfoAddButton);

        recyclerView = findViewById(R.id.sponsorshipRecyclerView);

        saveSponsorshipButton = findViewById(R.id.saveSponsorshipImageButton);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        formatter = new DecimalFormat("#,###.00");

    }


    public void sponsorship_details() {

        sponsorshipArrayList = new ArrayList<>();
        Cursor data = dbHandler.getDoctorvisitSponsorship(customerCode);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {

        } else {
            int i = 0;
            while (data.moveToNext()) {

                String budget_amount = data.getString(6);
                String formatted_budget_amount = "";
                if ("".equals(budget_amount)) {

                } else {
                    double budget_amount_val = Double.parseDouble(budget_amount);
                    formatted_budget_amount = formatter.format(budget_amount_val);

                }

                doctorVisitSponsorship = new DoctorVisitSponsorship();
                doctorVisitSponsorship.setId(Integer.parseInt(data.getString(0)));
                doctorVisitSponsorship.setType(data.getString(2));
                doctorVisitSponsorship.setEvent(data.getString(3));
                doctorVisitSponsorship.setFromDate(data.getString(4));
                doctorVisitSponsorship.setToDate(data.getString(5));
                doctorVisitSponsorship.setBudgetAmount(formatted_budget_amount);
                sponsorshipArrayList.add(i, doctorVisitSponsorship);
                i++;
            }

            sponsorshipAdapter = new SponsorshipAdapter(sponsorshipArrayList, listener);
            recyclerView.setHasFixedSize(true);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(sponsorshipAdapter);


        }
    }

    @Override
    public void onBackPressed() {

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    public class SponsorshipAdapter extends RecyclerView.Adapter<SponsorshipAdapter.MyViewHolder> {

        public Context context;
        private ArrayList<DoctorVisitSponsorship> mDataset;
        private RecyclerViewItemListener mListener;

        public SponsorshipAdapter(ArrayList<DoctorVisitSponsorship> myDataset, RecyclerViewItemListener listener) {
            this.mDataset = myDataset;
            mListener = listener;
        }

        @Override
        public SponsorshipAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_dr_visit_sponsorship, parent, false);
            context = parent.getContext();

            MyViewHolder vh = new MyViewHolder(itemView, mListener);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            final DoctorVisitSponsorship doctorVisitSponsorship1 = mDataset.get(position);

            holder.type.setText(doctorVisitSponsorship1.getType());
            holder.event.setText(doctorVisitSponsorship1.getEvent());

//            viewHolder.expenseDate.setText(mDataset.get(position).getExpenseDate());
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
            Date date = null,date1=null;
            try {
                date = inputFormat.parse(doctorVisitSponsorship1.getFromDate());
                String outputDateStr = outputFormat.format(date);

                date1 = inputFormat.parse(doctorVisitSponsorship1.getToDate());
                String outputDateStr1 = outputFormat.format(date1);

                holder.from_date.setText(outputDateStr);
                holder.to_date.setText(outputDateStr1);
            } catch (ParseException e) {
                e.printStackTrace();
            }

//            holder.from_date.setText(doctorVisitSponsorship1.getFromDate());
//            holder.to_date.setText(doctorVisitSponsorship1.getToDate());
            holder.budget_amount.setText(doctorVisitSponsorship1.getBudgetAmount());

            holder.removeRow.setOnClickListener(v -> {

                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Product Info?");
                        alertDialog.setPositiveButton("NO", (dialog, which) -> dialog.cancel());

                        alertDialog.setNegativeButton("YES", (dialog, which) -> {
                            dialog.dismiss();
                            mDataset.remove(position);
                            dbHandler.deleteDoctorvisitSponsorship(doctorVisitSponsorship1.getId());
                            notifyDataSetChanged();
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }


            });


        }

        @Override
        public int getItemCount() {
            return mDataset.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public TextView type, event, from_date, to_date, budget_amount;
            public ImageButton removeRow;


            public MyViewHolder(View view, RecyclerViewItemListener listener
            ) {
                super(view);
                mListener = listener;
                type = view.findViewById(R.id.type);
                event = view.findViewById(R.id.event);
                from_date = view.findViewById(R.id.from_date);
                to_date = view.findViewById(R.id.to_date);
                budget_amount = view.findViewById(R.id.budget_amount);
                removeRow = view.findViewById(R.id.spRemoveButton);

            }
        }


    }

}

