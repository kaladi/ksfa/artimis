package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class TopProductDrillList {

    private int date;
    private String order_num;
    private String customer_name;
    private String location;
    private String city;


    public TopProductDrillList(int date1, String order_num1, String customer_name1, String location1, String city1) {

        date = date1;
        order_num = order_num1;
        customer_name = customer_name1;
        location = location1;
        city = city1;

    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date1) {
        date = date1;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num1) {
        order_num = order_num1;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name1) {
        customer_name = customer_name1;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location1) {
        location = location1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city1) {
        city = city1;
    }


}

