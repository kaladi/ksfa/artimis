package in.kumanti.emzor.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.GraphPageAdapter;
import in.kumanti.emzor.adapter.MyMarkerView;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.GraphData;
import in.kumanti.emzor.model.OrderProductSpinner;
import in.kumanti.emzor.utils.CustomSearchableSpinner;

public class MetricsSecondaryFragment extends Fragment implements OnChartValueSelectedListener, View.OnClickListener {

    protected final String[] customers = new String[]{"Target(YTD)", "Achievement(YTD)"};
    protected final String[] incentives = new String[]{"Tar Year", "Ach Year"};
    ImageView planVisitImageButton, saleVisitImageButton, totalSaleImageViewButton, focusSchemeImageButton;
    Context mContext;
    MyDBHandler dbHandler;
    CardView planVisitCardView, saleVisitCardView, totalSaleCardView, focusSchemeCardView;
    private PieChart customerChart, incentiveChart;

    CustomSearchableSpinner targetTypeSpinner, productSpinner;
    String targetTypeString,productNameString;
    int  numbRows = 0;


    ArrayList<OrderProductSpinner> orderProductSpinnerArrayList;
    ArrayAdapter<OrderProductSpinner> orderProductSpinnerArrayAdapter;

    public MetricsSecondaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_metrics_secondary, container, false);
        mContext = rootView.getContext();
        dbHandler = new MyDBHandler(mContext, null, null, 1);

        customerChart = rootView.findViewById(R.id.secondaryCustomerPieChartView);
        //incentiveChart = rootView.findViewById(R.id.secondaryIncentivePieChartView);
        planVisitImageButton = rootView.findViewById(R.id.planVisitImageView);
        saleVisitImageButton = rootView.findViewById(R.id.saleVisitImageView);
        totalSaleImageViewButton = rootView.findViewById(R.id.totalSaleImageView);
        focusSchemeImageButton = rootView.findViewById(R.id.focusSchemeImageView);

        planVisitCardView = rootView.findViewById(R.id.planVisitCardView);
        saleVisitCardView = rootView.findViewById(R.id.saleVisitCardView);
        totalSaleCardView = rootView.findViewById(R.id.totalSaleCardView);
        focusSchemeCardView = rootView.findViewById(R.id.focusSchemeCardView);


        planVisitCardView.setOnClickListener(this);
        saleVisitCardView.setOnClickListener(this);
        totalSaleCardView.setOnClickListener(this);
        focusSchemeCardView.setOnClickListener(this);

        showCustomerPieChartGraph();
        //showIncentivePieChartGraph();

        return rootView;
    }

    private void showCustomerPieChartGraph() {
        customerChart.setUsePercentValues(true);
        customerChart.getDescription().setEnabled(false);
        customerChart.setExtraOffsets(5, 10, 5, 5);

        customerChart.setDragDecelerationFrictionCoef(0.95f);

        //customerChart.setCenterTextTypeface(tfLight);
        customerChart.setCenterText("");

        customerChart.setDrawHoleEnabled(true);
        customerChart.setHoleColor(Color.WHITE);

        customerChart.setTransparentCircleColor(Color.WHITE);
        customerChart.setTransparentCircleAlpha(110);

        customerChart.setHoleRadius(58f);
        customerChart.setTransparentCircleRadius(61f);

        customerChart.setDrawCenterText(true);

        customerChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        customerChart.setRotationEnabled(false);
        customerChart.setHighlightPerTapEnabled(true);
        setCustomerChartData(2, 2, customerChart);


        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        customerChart.setOnChartValueSelectedListener(this);

        customerChart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = customerChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        customerChart.setEntryLabelColor(Color.WHITE);
        //customerChart.setEntryLabelTypeface(tfRegular);
        customerChart.setEntryLabelTextSize(12f);
        customerChart.setDrawEntryLabels(false);

    }

    private void showIncentivePieChartGraph() {

        incentiveChart.setUsePercentValues(true);
        incentiveChart.getDescription().setEnabled(false);
        incentiveChart.setExtraOffsets(5, 10, 5, 5);

        incentiveChart.setDragDecelerationFrictionCoef(0.95f);

        //incentiveChart.setCenterTextTypeface(tfLight);
        incentiveChart.setCenterText("");

        incentiveChart.setDrawHoleEnabled(true);
        incentiveChart.setHoleColor(Color.WHITE);

        incentiveChart.setTransparentCircleColor(Color.WHITE);
        incentiveChart.setTransparentCircleAlpha(110);

        incentiveChart.setHoleRadius(58f);
        incentiveChart.setTransparentCircleRadius(61f);

        incentiveChart.setDrawCenterText(true);

        incentiveChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        incentiveChart.setRotationEnabled(false);
        incentiveChart.setHighlightPerTapEnabled(true);
        setIncentiveChartData(2, 2, incentiveChart);


        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        incentiveChart.setOnChartValueSelectedListener(this);

        incentiveChart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = incentiveChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        incentiveChart.setEntryLabelColor(Color.WHITE);
        //incentiveChart.setEntryLabelTypeface(tfRegular);
        incentiveChart.setEntryLabelTextSize(12f);
        incentiveChart.setDrawEntryLabels(false);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.planVisitCardView:
                showPlanVisitBarGraph();

                break;

            case R.id.saleVisitCardView:
                showSaleVisitTargetBarGraph();

                break;

            case R.id.totalSaleCardView:
                showTotalSaleTargetBarGraph();

                break;

            case R.id.focusSchemeCardView:
                showFocusSchemeTargetBarGraph();

                break;
        }


    }


    private void setCustomerChartData(int count, float range, PieChart chartName) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of the chart

        Cursor planVisitYearCount = dbHandler.get_customer_year_plancount1();
        int planVisitYearCountValue = planVisitYearCount.getCount();
        if (planVisitYearCountValue == 0) {
            entries.add(new PieEntry(0f, customers[0], getResources().getDrawable(R.drawable.up_arrow)));
            entries.add(new PieEntry(0f, customers[1], getResources().getDrawable(R.drawable.up_arrow)));
        } else {
            int i = 0;
            while (planVisitYearCount.moveToNext()) {
                Log.d("PieChartTest", "" + planVisitYearCount.getString(1) + " " + planVisitYearCount.getString(0));
                System.out.println("TTT::planVisitYearCount.getString(1) = " + planVisitYearCount.getString(1));
                System.out.println("TTT::planVisitYearCount.getString(1) = " + planVisitYearCount.getString(0));
                String target = planVisitYearCount.getString(1) == null ? "0" : planVisitYearCount.getString(1);
                String achievement = planVisitYearCount.getString(0) == null ? "0" : planVisitYearCount.getString(0);

                entries.add(new PieEntry(Float.parseFloat(target), customers[0], getResources().getDrawable(R.drawable.up_arrow)));
                entries.add(new PieEntry(Float.parseFloat(achievement), customers[1], getResources().getDrawable(R.drawable.up_arrow)));
            }
        }


        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(4f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);
        dataSet.setValueFormatter(new PieChartValueFormatter());

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(131, 131, 131));
        colors.add(Color.rgb(255, 124, 45));
        colors.add(Color.rgb(64, 31, 11));
        colors.add(Color.rgb(191, 93, 34));
        dataSet.setColors(colors);

        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        //data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(tfLight);
        chartName.setData(data);

        // undo all highlights
        chartName.highlightValues(null);

        chartName.invalidate();
    }


    private void setIncentiveChartData(int count, float range, PieChart chartName) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count; i++) {
            entries.add(new PieEntry((float) ((Math.random() * range) + range / 5), incentives[i % incentives.length], getResources().getDrawable(R.drawable.up_arrow)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(4f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(131, 131, 131));
        colors.add(Color.rgb(255, 124, 45));
        colors.add(Color.rgb(64, 31, 11));
        colors.add(Color.rgb(191, 93, 34));
        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        //data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(tfLight);
        chartName.setData(data);

        // undo all highlights
        chartName.highlightValues(null);

        chartName.invalidate();
    }

    private void showPlanVisitBarGraph() {

        ArrayList<BarEntry> targetValues = new ArrayList<>();
        ArrayList<BarEntry> achievementValues = new ArrayList<>();

        Cursor planVisitDayCount = dbHandler.get_current_plancount1();
        int planVisitDayCountValue = planVisitDayCount.getCount();
        if (planVisitDayCountValue == 0) {
            targetValues.add(new BarEntry(1, 0F));
            achievementValues.add(new BarEntry(1, 0F));

        } else {
            int i = 0;
            while (planVisitDayCount.moveToNext()) {
                /*targetValues.add(new BarEntry(1, Float.parseFloat(planVisitDayCount.getString(1))));
                achievementValues.add(new BarEntry(1, Float.parseFloat(planVisitDayCount.getString(0))));*/

                targetValues.add(new BarEntry(1, Float.parseFloat(planVisitDayCount.getString(1) != null ? planVisitDayCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(1, Float.parseFloat(planVisitDayCount.getString(0) != null ? planVisitDayCount.getString(0) : "0")));


            }
        }

        Cursor planVisitWeekCount = dbHandler.get_week_plancount1();
        int planVisitWeekCountValue = planVisitWeekCount.getCount();
        if (planVisitWeekCountValue == 0) {
            targetValues.add(new BarEntry(2, 0F));
            achievementValues.add(new BarEntry(2, 0F));
        } else {
            int i = 0;
            while (planVisitWeekCount.moveToNext()) {
                // targetValues.add(new BarEntry(2, Float.parseFloat(planVisitWeekCount.getString(1))));
                //achievementValues.add(new BarEntry(2, Float.parseFloat(planVisitWeekCount.getString(0))));

                targetValues.add(new BarEntry(2, Float.parseFloat(planVisitWeekCount.getString(1) != null ? planVisitWeekCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(2, Float.parseFloat(planVisitWeekCount.getString(0) != null ? planVisitWeekCount.getString(0) : "0")));


            }
        }

        Cursor planVisitMonthCount = dbHandler.get_month_plancount1();
        int planVisitMonthCountValue = planVisitMonthCount.getCount();
        if (planVisitMonthCountValue == 0) {
            targetValues.add(new BarEntry(3, 0F));
            achievementValues.add(new BarEntry(3, 0F));
        } else {
            int i = 0;
            while (planVisitMonthCount.moveToNext()) {
               /* targetValues.add(new BarEntry(3, Float.parseFloat(planVisitMonthCount.getString(1))));
                achievementValues.add(new BarEntry(3, Float.parseFloat(planVisitMonthCount.getString(0))));*/

                targetValues.add(new BarEntry(3, Float.parseFloat(planVisitMonthCount.getString(1) != null ? planVisitMonthCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(3, Float.parseFloat(planVisitMonthCount.getString(0) != null ? planVisitMonthCount.getString(0) : "0")));


            }
        }

        Cursor planVisitQuarterCount = dbHandler.get_quarter_plancount1();
        int planVisitQuarterCountValue = planVisitQuarterCount.getCount();
        if (planVisitQuarterCountValue == 0) {
            targetValues.add(new BarEntry(4, 0F));
            achievementValues.add(new BarEntry(4, 0F));
        } else {
            int i = 0;
            while (planVisitQuarterCount.moveToNext()) {
                // targetValues.add(new BarEntry(4, Float.parseFloat(planVisitQuarterCount.getString(1))));
                //  achievementValues.add(new BarEntry(4, Float.parseFloat(planVisitQuarterCount.getString(0))));

                targetValues.add(new BarEntry(4, Float.parseFloat(planVisitQuarterCount.getString(1) != null ? planVisitQuarterCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(4, Float.parseFloat(planVisitQuarterCount.getString(0) != null ? planVisitQuarterCount.getString(0) : "0")));


            }
        }

        Cursor planVisitYearCount = dbHandler.get_year_plancount1();
        int planVisitYearCountValue = planVisitYearCount.getCount();
        if (planVisitYearCountValue == 0) {
            targetValues.add(new BarEntry(5, 0F));
            achievementValues.add(new BarEntry(5, 0F));
        } else {
            int i = 0;
            while (planVisitYearCount.moveToNext()) {
                /*targetValues.add(new BarEntry(5, Float.parseFloat(planVisitYearCount.getString(1))));
                achievementValues.add(new BarEntry(5, Float.parseFloat(planVisitYearCount.getString(0))));*/

                targetValues.add(new BarEntry(5, Float.parseFloat(planVisitYearCount.getString(1) != null ? planVisitYearCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(5, Float.parseFloat(planVisitYearCount.getString(0) != null ? planVisitYearCount.getString(0) : "0")));


            }
        }
        showTargetAchievementGraph(targetValues, achievementValues, "Plan Visit");
    }


    private void showSaleVisitTargetBarGraph() {

        BarChart barChart;
        TextView titleTextView;

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.fragment_primary_sale_visit_pop_up, null);

        //CHART INITIALIZATION
        titleTextView = alertLayout.findViewById(R.id.primaryChartTitle);
        titleTextView.setText("Sale Visit");

        targetTypeSpinner = alertLayout.findViewById(R.id.targetTypeSpinnerSpinner);
//        productSpinner = alertLayout.findViewById(R.id.ProductSpinner);
        barChart = alertLayout.findViewById(R.id.primaryBarChart);
        barChart.setOnChartValueSelectedListener(this);
       /* targetTypeString="";
        productNameString="";*/
//        loadSpinnerTypeSaleVisit();

        String[] labels = {"Select Target","Count"};

        targetTypeSpinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, labels));

        targetTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                targetTypeString = adapterView.getItemAtPosition(pos).toString();
                System.out.println("TTT::targetTypeString1 = " + targetTypeString);
//                loadSpinnerProductSaleVisit(targetTypeString);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        ArrayList<BarEntry> targetValues = new ArrayList<>();
        ArrayList<BarEntry> achievementValues = new ArrayList<>();

        Cursor saleVisitDayCount = dbHandler.get_current_salescount1(targetTypeString,productNameString);
        int saleVisitDayCountValue = saleVisitDayCount.getCount();
        if (saleVisitDayCountValue == 0) {
            targetValues.add(new BarEntry(1, 0F));
            achievementValues.add(new BarEntry(1, 0F));
        } else {
            int i = 0;
            while (saleVisitDayCount.moveToNext()) {
                //  targetValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(1))));
                //  achievementValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(0))));

                targetValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(1) != null ? saleVisitDayCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(0) != null ? saleVisitDayCount.getString(0) : "0")));


            }
        }

        Cursor saleVisitWeekCount = dbHandler.get_week_salescount1(targetTypeString,productNameString);
        int saleVisitWeekCountValue = saleVisitWeekCount.getCount();
        if (saleVisitWeekCountValue == 0) {
            targetValues.add(new BarEntry(2, 0F));
            achievementValues.add(new BarEntry(2, 0F));
        } else {
            int i = 0;
            while (saleVisitWeekCount.moveToNext()) {
              /*  targetValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(1))));
                achievementValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(0))));*/

                targetValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(1) != null ? saleVisitWeekCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(0) != null ? saleVisitWeekCount.getString(0) : "0")));


            }
        }

        Cursor saleVisitMonthCount = dbHandler.get_month_salescount1(targetTypeString,productNameString);
        int saleVisitMonthCountValue = saleVisitMonthCount.getCount();
        if (saleVisitMonthCountValue == 0) {
            targetValues.add(new BarEntry(3, 0F));
            achievementValues.add(new BarEntry(3, 0F));
        } else {
            int i = 0;
            while (saleVisitMonthCount.moveToNext()) {
            /*    targetValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(1))));
                achievementValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(0))));
*/
                targetValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(1) != null ? saleVisitMonthCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(0) != null ? saleVisitMonthCount.getString(0) : "0")));


            }
        }

        Cursor saleVisitQuarterCount = dbHandler.get_quarter_salescount1(targetTypeString,productNameString);
        int saleVisitQuarterCountValue = saleVisitQuarterCount.getCount();
        if (saleVisitQuarterCountValue == 0) {
            targetValues.add(new BarEntry(4, 0F));
            achievementValues.add(new BarEntry(4, 0F));
        } else {
            int i = 0;
            while (saleVisitQuarterCount.moveToNext()) {
                // targetValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(1))));
                // achievementValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(0))));

                targetValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(1) != null ? saleVisitQuarterCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(0) != null ? saleVisitQuarterCount.getString(0) : "0")));


            }
        }

        Cursor saleVisitYearCount = dbHandler.get_year_salescount1(targetTypeString,productNameString);
        int saleVisitYearCountValue = saleVisitYearCount.getCount();
        if (saleVisitYearCountValue == 0) {
            targetValues.add(new BarEntry(5, 0F));
            achievementValues.add(new BarEntry(5, 0F));
        } else {
            int i = 0;
            while (saleVisitYearCount.moveToNext()) {
                // targetValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(1))));
                //  achievementValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(0))));

                targetValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(1) != null ? saleVisitYearCount.getString(1) : "0")));
                achievementValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(0) != null ? saleVisitYearCount.getString(0) : "0")));


            }
        }


        barChart.getDescription().setEnabled(false);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);

        barChart.setDrawBarShadow(false);

        barChart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        //l.setTypeface(tfLight);
        l.setYOffset(10f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(20f);


        XAxis xAxis = barChart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        String[] values = new String[]{"Day", "Week", "Month", "This Quarter", "Year"};

        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

        YAxis leftAxis = barChart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceBottom(50f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setGranularity(1f);
        barChart.getAxisRight().setEnabled(false);
        barChart.animateY(1400, Easing.EaseInOutQuad);

        //DATA INITIALIZATION
        float groupSpace = 0f;
        float barSpace = 0.25f; // x4 DataSet
        float barWidth = 0.25f;
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

        int groupCount = 5;
        int startYear = 1;


        BarDataSet set1, set2;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);

            set1.setValues(targetValues);
            set2.setValues(achievementValues);

            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();

        } else {
            // create 2 DataSets
            set1 = new BarDataSet(targetValues, "Target");
            set1.setColor(Color.rgb(131, 131, 131));
            set2 = new BarDataSet(achievementValues, "Achievement");
            set2.setColor(Color.rgb(255, 124, 45));

            BarData data = new BarData(set1, set2);
//            data.setValueFormatter(new LargeValueFormatter());
            //data.setValueTypeface(tfLight);

            barChart.setData(data);
        }

        // specify the width each bar should have
        barChart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        barChart.getXAxis().setAxisMinimum(startYear);

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        barChart.getXAxis().setAxisMaximum(startYear + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        Log.d("Metrics", "Maximum" + barChart.getXAxis().getAxisMaximum());
        barChart.groupBars(startYear, groupSpace, barSpace);
        barChart.invalidate();

        /*productSpinner.setTitle("Select Product");
        productSpinner.setPositiveButton("OK");

        orderProductSpinnerArrayList = dbHandler.getMetricsProductSpinner();

        OrderProductSpinner cs = new OrderProductSpinner();
        cs.setProduct("All");
        cs.setProd_type("-1");
        cs.setProduct_id("-1");
        orderProductSpinnerArrayList.add(0, cs);

        orderProductSpinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, orderProductSpinnerArrayList);
        orderProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(orderProductSpinnerArrayAdapter);

*/



        /*
         * Functionality of setOnItemSelectedListener of selected product and its corresponding type.
         */
      /*  productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;
                productNameString = adapterView.getItemAtPosition(pos).toString();


                ArrayList<BarEntry> targetValues = new ArrayList<>();
                ArrayList<BarEntry> achievementValues = new ArrayList<>();

                Cursor saleVisitDayCount = dbHandler.get_current_salescount1(targetTypeString,productNameString);
                int saleVisitDayCountValue = saleVisitDayCount.getCount();
                if (saleVisitDayCountValue == 0) {
                    targetValues.add(new BarEntry(1, 0F));
                    achievementValues.add(new BarEntry(1, 0F));
                } else {
                    int i = 0;
                    while (saleVisitDayCount.moveToNext()) {
                        //  targetValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(1))));
                        //  achievementValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(0))));

                        targetValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(1) != null ? saleVisitDayCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(1, Float.parseFloat(saleVisitDayCount.getString(0) != null ? saleVisitDayCount.getString(0) : "0")));


                    }
                }

                Cursor saleVisitWeekCount = dbHandler.get_week_salescount1(targetTypeString,productNameString);
                int saleVisitWeekCountValue = saleVisitWeekCount.getCount();
                if (saleVisitWeekCountValue == 0) {
                    targetValues.add(new BarEntry(2, 0F));
                    achievementValues.add(new BarEntry(2, 0F));
                } else {
                    int i = 0;
                    while (saleVisitWeekCount.moveToNext()) {
                targetValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(1))));
                achievementValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(0))));

                        targetValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(1) != null ? saleVisitWeekCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(2, Float.parseFloat(saleVisitWeekCount.getString(0) != null ? saleVisitWeekCount.getString(0) : "0")));


                    }
                }

                Cursor saleVisitMonthCount = dbHandler.get_month_salescount1(targetTypeString,productNameString);
                int saleVisitMonthCountValue = saleVisitMonthCount.getCount();
                if (saleVisitMonthCountValue == 0) {
                    targetValues.add(new BarEntry(3, 0F));
                    achievementValues.add(new BarEntry(3, 0F));
                } else {
                    int i = 0;
                    while (saleVisitMonthCount.moveToNext()) {
                targetValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(1))));
                achievementValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(0))));

                        targetValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(1) != null ? saleVisitMonthCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(3, Float.parseFloat(saleVisitMonthCount.getString(0) != null ? saleVisitMonthCount.getString(0) : "0")));


                    }
                }

                Cursor saleVisitQuarterCount = dbHandler.get_quarter_salescount1(targetTypeString,productNameString);
                int saleVisitQuarterCountValue = saleVisitQuarterCount.getCount();
                if (saleVisitQuarterCountValue == 0) {
                    targetValues.add(new BarEntry(4, 0F));
                    achievementValues.add(new BarEntry(4, 0F));
                } else {
                    int i = 0;
                    while (saleVisitQuarterCount.moveToNext()) {
                        // targetValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(1))));
                        // achievementValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(0))));

                        targetValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(1) != null ? saleVisitQuarterCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(4, Float.parseFloat(saleVisitQuarterCount.getString(0) != null ? saleVisitQuarterCount.getString(0) : "0")));


                    }
                }

                Cursor saleVisitYearCount = dbHandler.get_year_salescount1(targetTypeString,productNameString);
                int saleVisitYearCountValue = saleVisitYearCount.getCount();
                if (saleVisitYearCountValue == 0) {
                    targetValues.add(new BarEntry(5, 0F));
                    achievementValues.add(new BarEntry(5, 0F));
                } else {
                    int i = 0;
                    while (saleVisitYearCount.moveToNext()) {
                        // targetValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(1))));
                        //  achievementValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(0))));

                        targetValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(1) != null ? saleVisitYearCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(5, Float.parseFloat(saleVisitYearCount.getString(0) != null ? saleVisitYearCount.getString(0) : "0")));


                    }
                }


                barChart.getDescription().setEnabled(false);

                // scaling can now only be done on x- and y-axis separately
                barChart.setPinchZoom(false);

                barChart.setDrawBarShadow(false);

                barChart.setDrawGridBackground(false);

                // create a custom MarkerView (extend MarkerView) and specify the layout
                // to use for it
                MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
                mv.setChartView(barChart); // For bounds control
                barChart.setMarker(mv); // Set the marker to the chart

                Legend l = barChart.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
                l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                l.setDrawInside(false);
                //l.setTypeface(tfLight);
                l.setYOffset(10f);
                l.setXOffset(10f);
                l.setYEntrySpace(0f);
                l.setTextSize(20f);


                XAxis xAxis = barChart.getXAxis();
                //xAxis.setTypeface(tfLight);
                xAxis.setGranularity(1f);
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis.setCenterAxisLabels(true);
                xAxis.setDrawGridLines(false);
                String[] values = new String[]{"Day", "Week", "Month", "This Quarter", "Year"};

                xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

                YAxis leftAxis = barChart.getAxisLeft();
                //leftAxis.setTypeface(tfLight);
                leftAxis.setValueFormatter(new LargeValueFormatter());
                leftAxis.setDrawGridLines(false);
                leftAxis.setSpaceBottom(50f);
                leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
                leftAxis.setGranularity(1f);
                barChart.getAxisRight().setEnabled(false);
                barChart.animateY(1400, Easing.EaseInOutQuad);

                //DATA INITIALIZATION
                float groupSpace = 0.3f;
                float barSpace = 0.05f; // x4 DataSet
                float barWidth = 0.3f; // x4 DataSet
                // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

                int groupCount = 5;
                int startYear = 1;


                BarDataSet set1, set2;

                if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

                    set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
                    set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);

                    set1.setValues(targetValues);
                    set2.setValues(achievementValues);

                    barChart.getData().notifyDataChanged();
                    barChart.notifyDataSetChanged();

                } else {
                    // create 2 DataSets
                    set1 = new BarDataSet(targetValues, "Target");
                    set1.setColor(Color.rgb(131, 131, 131));
                    set2 = new BarDataSet(achievementValues, "Achievement");
                    set2.setColor(Color.rgb(255, 124, 45));

                    BarData data = new BarData(set1, set2);
                    data.setValueFormatter(new LargeValueFormatter());
                    //data.setValueTypeface(tfLight);

                    barChart.setData(data);
                }

                // specify the width each bar should have
                barChart.getBarData().setBarWidth(barWidth);

                // restrict the x-axis range
                barChart.getXAxis().setAxisMinimum(startYear);

                // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
                barChart.getXAxis().setAxisMaximum(startYear + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
                Log.d("Metrics", "Maximum" + barChart.getXAxis().getAxisMaximum());
                barChart.groupBars(startYear, groupSpace, barSpace);
                barChart.invalidate();


                System.out.println("TTT::productNameString1 = " + productNameString);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });*/




        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void  showTotalSaleTargetBarGraph(){
        BarChart barChart;
        TextView titleTextView;

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.fragment_primary_total_sale_pop_up, null);

        //CHART INITIALIZATION
        titleTextView = alertLayout.findViewById(R.id.primaryChartTitle);
        titleTextView.setText("Total Sales");

        targetTypeSpinner = alertLayout.findViewById(R.id.targetTypeSpinnerSpinner);
        productSpinner = alertLayout.findViewById(R.id.ProductSpinner);
        barChart = alertLayout.findViewById(R.id.primaryBarChart);
        barChart.setOnChartValueSelectedListener(this);
      /*  targetTypeString="Value";
        productNameString="All";*/
//        loadSpinnerTypeSaleVisit();

        String[] labels = {"Select Target","Quantity","Value"};

        targetTypeSpinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, labels));

        targetTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                if(pos==0){
                    targetTypeString = "Value";
                    productNameString = "All";

                }else
                {
                    targetTypeString = adapterView.getItemAtPosition(pos).toString();
                    productNameString = "All";
                    System.out.println("TTT::targetTypeString1 = " + targetTypeString);
                }

                productSpinner.setTitle("Select Product");
                productSpinner.setPositiveButton("OK");

                orderProductSpinnerArrayList = dbHandler.getMetricsProductSpinner();

                OrderProductSpinner cs = new OrderProductSpinner();
                cs.setProduct("All");
                cs.setProd_type("-1");
                cs.setProduct_id("-1");
                orderProductSpinnerArrayList.add(0, cs);

                orderProductSpinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, orderProductSpinnerArrayList);
                orderProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                productSpinner.setAdapter(orderProductSpinnerArrayAdapter);

                ArrayList<BarEntry> targetValues = new ArrayList<>();
                ArrayList<BarEntry> achievementValues = new ArrayList<>();

                Cursor totalSaleValueDayCount = dbHandler.get_current_salesvalue1(targetTypeString, productNameString);
                int totalSaleDayCountValue = totalSaleValueDayCount.getCount();
                if (totalSaleDayCountValue == 0) {
                    targetValues.add(new BarEntry(1, 0F));
                    achievementValues.add(new BarEntry(1, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueDayCount.moveToNext()) {
                        targetValues.add(new BarEntry(1, Float.parseFloat(totalSaleValueDayCount.getString(1) != null ? totalSaleValueDayCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(1, Float.parseFloat(totalSaleValueDayCount.getString(0) != null ? totalSaleValueDayCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueWeekCount = dbHandler.get_week_salesvalue1(targetTypeString, productNameString);
                int totalSaleWeekCountValue = totalSaleValueWeekCount.getCount();
                if (totalSaleWeekCountValue == 0) {
                    targetValues.add(new BarEntry(2, 0F));
                    achievementValues.add(new BarEntry(2, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueWeekCount.moveToNext()) {
                        targetValues.add(new BarEntry(2, Float.parseFloat(totalSaleValueWeekCount.getString(1) != null ? totalSaleValueWeekCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(2, Float.parseFloat(totalSaleValueWeekCount.getString(0) != null ? totalSaleValueWeekCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueMonthCount = dbHandler.get_month_salesvalue1(targetTypeString, productNameString);
                int totalSaleMonthCountValue = totalSaleValueMonthCount.getCount();
                if (totalSaleMonthCountValue == 0) {
                    targetValues.add(new BarEntry(3, 0F));
                    achievementValues.add(new BarEntry(3, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueMonthCount.moveToNext()) {
                        targetValues.add(new BarEntry(3, Float.parseFloat(totalSaleValueMonthCount.getString(1) != null ? totalSaleValueMonthCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(3, Float.parseFloat(totalSaleValueMonthCount.getString(0) != null ? totalSaleValueMonthCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueQuarterCount = dbHandler.get_quarter_salesvalue1(targetTypeString, productNameString);
                int totalSaleQuarterCountValue = totalSaleValueQuarterCount.getCount();
                if (totalSaleQuarterCountValue == 0) {
                    targetValues.add(new BarEntry(4, 0F));
                    achievementValues.add(new BarEntry(4, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueQuarterCount.moveToNext()) {
                        targetValues.add(new BarEntry(4, Float.parseFloat(totalSaleValueQuarterCount.getString(1) != null ? totalSaleValueQuarterCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(4, Float.parseFloat(totalSaleValueQuarterCount.getString(0) != null ? totalSaleValueQuarterCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueYearCount = dbHandler.get_year_salesvalue1(targetTypeString, productNameString);
                int totalSaleYearCountValue = totalSaleValueYearCount.getCount();
                if (totalSaleYearCountValue == 0) {
                    targetValues.add(new BarEntry(5, 0F));
                    achievementValues.add(new BarEntry(5, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueYearCount.moveToNext()) {
                        targetValues.add(new BarEntry(5, Float.parseFloat(totalSaleValueYearCount.getString(1) != null ? totalSaleValueYearCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(5, Float.parseFloat(totalSaleValueYearCount.getString(0) != null ? totalSaleValueYearCount.getString(0) : "0")));
                    }
                }

                barChart.getDescription().setEnabled(false);

                // scaling can now only be done on x- and y-axis separately
                barChart.setPinchZoom(false);

                barChart.setDrawBarShadow(false);

                barChart.setDrawGridBackground(false);

                // create a custom MarkerView (extend MarkerView) and specify the layout
                // to use for it
                MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
                mv.setChartView(barChart); // For bounds control
                barChart.setMarker(mv); // Set the marker to the chart

                Legend l = barChart.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
                l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                l.setDrawInside(false);
                //l.setTypeface(tfLight);
                l.setYOffset(10f);
                l.setXOffset(10f);
                l.setYEntrySpace(0f);
                l.setTextSize(20f);


                XAxis xAxis = barChart.getXAxis();
                //xAxis.setTypeface(tfLight);
                xAxis.setGranularity(1f);
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis.setCenterAxisLabels(true);
                xAxis.setDrawGridLines(false);
                String[] values = new String[]{"Day", "Week", "Month", "This Quarter", "Year"};

                xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

                YAxis leftAxis = barChart.getAxisLeft();
                //leftAxis.setTypeface(tfLight);
                leftAxis.setValueFormatter(new LargeValueFormatter());
                leftAxis.setDrawGridLines(false);
                leftAxis.setSpaceBottom(50f);
                leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
                leftAxis.setGranularity(1f);
                barChart.getAxisRight().setEnabled(false);
                barChart.animateY(1400, Easing.EaseInOutQuad);

                //DATA INITIALIZATION
                float groupSpace = 0f;
                float barSpace = 0.25f; // x4 DataSet
                float barWidth = 0.25f;
                // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

                int groupCount = 5;
                int startYear = 1;


                BarDataSet set1, set2;

                if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

                    set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
                    set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);

                    set1.setValues(targetValues);
                    set2.setValues(achievementValues);

                    barChart.getData().notifyDataChanged();
                    barChart.notifyDataSetChanged();

                } else {
                    // create 2 DataSets
                    set1 = new BarDataSet(targetValues, "Target");
                    set1.setColor(Color.rgb(131, 131, 131));
                    set2 = new BarDataSet(achievementValues, "Achievement");
                    set2.setColor(Color.rgb(255, 124, 45));

                    BarData data = new BarData(set1, set2);
//                    data.setValueFormatter(new LargeValueFormatter());
                    //data.setValueTypeface(tfLight);

                    barChart.setData(data);
                }

                // specify the width each bar should have
                barChart.getBarData().setBarWidth(barWidth);

                // restrict the x-axis range
                barChart.getXAxis().setAxisMinimum(startYear);

                // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
                barChart.getXAxis().setAxisMaximum(startYear + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
                Log.d("Metrics", "Maximum" + barChart.getXAxis().getAxisMaximum());
                barChart.groupBars(startYear, groupSpace, barSpace);
                barChart.invalidate();


                System.out.println("TTT::productNameString1 = " + productNameString);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        productSpinner.setTitle("Select Product");
        productSpinner.setPositiveButton("OK");

        orderProductSpinnerArrayList = dbHandler.getMetricsProductSpinner();

        OrderProductSpinner cs = new OrderProductSpinner();
        cs.setProduct("All");
        cs.setProd_type("-1");
        cs.setProduct_id("-1");
        orderProductSpinnerArrayList.add(0, cs);

        orderProductSpinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, orderProductSpinnerArrayList);
        orderProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(orderProductSpinnerArrayAdapter);

        /*
         * Functionality of setOnItemSelectedListener of selected product and its corresponding type.
         */
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;
                productNameString = adapterView.getItemAtPosition(pos).toString();

                ArrayList<BarEntry> targetValues = new ArrayList<>();
                ArrayList<BarEntry> achievementValues = new ArrayList<>();

                Cursor totalSaleValueDayCount = dbHandler.get_current_salesvalue1(targetTypeString, productNameString);
                int totalSaleDayCountValue = totalSaleValueDayCount.getCount();
                if (totalSaleDayCountValue == 0) {
                    targetValues.add(new BarEntry(1, 0F));
                    achievementValues.add(new BarEntry(1, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueDayCount.moveToNext()) {
                        targetValues.add(new BarEntry(1, Float.parseFloat(totalSaleValueDayCount.getString(1) != null ? totalSaleValueDayCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(1, Float.parseFloat(totalSaleValueDayCount.getString(0) != null ? totalSaleValueDayCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueWeekCount = dbHandler.get_week_salesvalue1(targetTypeString, productNameString);
                int totalSaleWeekCountValue = totalSaleValueWeekCount.getCount();
                if (totalSaleWeekCountValue == 0) {
                    targetValues.add(new BarEntry(2, 0F));
                    achievementValues.add(new BarEntry(2, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueWeekCount.moveToNext()) {
                        targetValues.add(new BarEntry(2, Float.parseFloat(totalSaleValueWeekCount.getString(1) != null ? totalSaleValueWeekCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(2, Float.parseFloat(totalSaleValueWeekCount.getString(0) != null ? totalSaleValueWeekCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueMonthCount = dbHandler.get_month_salesvalue1(targetTypeString, productNameString);
                int totalSaleMonthCountValue = totalSaleValueMonthCount.getCount();
                if (totalSaleMonthCountValue == 0) {
                    targetValues.add(new BarEntry(3, 0F));
                    achievementValues.add(new BarEntry(3, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueMonthCount.moveToNext()) {
                        targetValues.add(new BarEntry(3, Float.parseFloat(totalSaleValueMonthCount.getString(1) != null ? totalSaleValueMonthCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(3, Float.parseFloat(totalSaleValueMonthCount.getString(0) != null ? totalSaleValueMonthCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueQuarterCount = dbHandler.get_quarter_salesvalue1(targetTypeString, productNameString);
                int totalSaleQuarterCountValue = totalSaleValueQuarterCount.getCount();
                if (totalSaleQuarterCountValue == 0) {
                    targetValues.add(new BarEntry(4, 0F));
                    achievementValues.add(new BarEntry(4, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueQuarterCount.moveToNext()) {
                        targetValues.add(new BarEntry(4, Float.parseFloat(totalSaleValueQuarterCount.getString(1) != null ? totalSaleValueQuarterCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(4, Float.parseFloat(totalSaleValueQuarterCount.getString(0) != null ? totalSaleValueQuarterCount.getString(0) : "0")));
                    }
                }

                Cursor totalSaleValueYearCount = dbHandler.get_year_salesvalue1(targetTypeString, productNameString);
                int totalSaleYearCountValue = totalSaleValueYearCount.getCount();
                if (totalSaleYearCountValue == 0) {
                    targetValues.add(new BarEntry(5, 0F));
                    achievementValues.add(new BarEntry(5, 0F));
                } else {
                    int i = 0;
                    while (totalSaleValueYearCount.moveToNext()) {
                        targetValues.add(new BarEntry(5, Float.parseFloat(totalSaleValueYearCount.getString(1) != null ? totalSaleValueYearCount.getString(1) : "0")));
                        achievementValues.add(new BarEntry(5, Float.parseFloat(totalSaleValueYearCount.getString(0) != null ? totalSaleValueYearCount.getString(0) : "0")));
                    }
                }


                barChart.getDescription().setEnabled(false);

                // scaling can now only be done on x- and y-axis separately
                barChart.setPinchZoom(false);

                barChart.setDrawBarShadow(false);

                barChart.setDrawGridBackground(false);

                // create a custom MarkerView (extend MarkerView) and specify the layout
                // to use for it
                MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
                mv.setChartView(barChart); // For bounds control
                barChart.setMarker(mv); // Set the marker to the chart

                Legend l = barChart.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
                l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                l.setDrawInside(false);
                //l.setTypeface(tfLight);
                l.setYOffset(10f);
                l.setXOffset(10f);
                l.setYEntrySpace(0f);
                l.setTextSize(20f);


                XAxis xAxis = barChart.getXAxis();
                //xAxis.setTypeface(tfLight);
                xAxis.setGranularity(1f);
                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                xAxis.setCenterAxisLabels(true);
                xAxis.setDrawGridLines(false);
                String[] values = new String[]{"Day", "Week", "Month", "This Quarter", "Year"};

                xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

                YAxis leftAxis = barChart.getAxisLeft();
                //leftAxis.setTypeface(tfLight);
                leftAxis.setValueFormatter(new LargeValueFormatter());
                leftAxis.setDrawGridLines(false);
                leftAxis.setSpaceBottom(50f);
                leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
                leftAxis.setGranularity(1f);
                barChart.getAxisRight().setEnabled(false);
                barChart.animateY(1400, Easing.EaseInOutQuad);

                //DATA INITIALIZATION
                float groupSpace = 0f;
                float barSpace = 0.25f; // x4 DataSet
                float barWidth = 0.25f;
                // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

                int groupCount = 5;
                int startYear = 1;


                BarDataSet set1, set2;

                if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

                    set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
                    set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);

                    set1.setValues(targetValues);
                    set2.setValues(achievementValues);

                    barChart.getData().notifyDataChanged();
                    barChart.notifyDataSetChanged();

                } else {
                    // create 2 DataSets
                    set1 = new BarDataSet(targetValues, "Target");
                    set1.setColor(Color.rgb(131, 131, 131));
                    set2 = new BarDataSet(achievementValues, "Achievement");
                    set2.setColor(Color.rgb(255, 124, 45));

                    BarData data = new BarData(set1, set2);
//                    data.setValueFormatter(new LargeValueFormatter());
                    //data.setValueTypeface(tfLight);

                    barChart.setData(data);
                }

                // specify the width each bar should have
                barChart.getBarData().setBarWidth(barWidth);

                // restrict the x-axis range
                barChart.getXAxis().setAxisMinimum(startYear);

                // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
                barChart.getXAxis().setAxisMaximum(startYear + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
                Log.d("Metrics", "Maximum" + barChart.getXAxis().getAxisMaximum());
                barChart.groupBars(startYear, groupSpace, barSpace);
                barChart.invalidate();


                System.out.println("TTT::productNameString1 = " + productNameString);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });




        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void showSaleVisitTargetAchievementGraph(ArrayList targetValues, ArrayList achievementValues, String barChartTitle) {
        BarChart barChart;
        TextView titleTextView;

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.fragment_primary_sale_visit_pop_up, null);

        //CHART INITIALIZATION
        titleTextView = alertLayout.findViewById(R.id.primaryChartTitle);
        titleTextView.setText(barChartTitle);

        barChart = alertLayout.findViewById(R.id.primaryBarChart);
        barChart.setOnChartValueSelectedListener(this);
        barChart.getDescription().setEnabled(false);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);

        barChart.setDrawBarShadow(false);

        barChart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        //l.setTypeface(tfLight);
        l.setYOffset(10f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(20f);


        XAxis xAxis = barChart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        String[] values = new String[]{"Day", "Week", "Month", "This Quarter", "Year"};

        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

        YAxis leftAxis = barChart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceBottom(50f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setGranularity(1f);
        barChart.getAxisRight().setEnabled(false);
        barChart.animateY(1400, Easing.EaseInOutQuad);

        //DATA INITIALIZATION
        float groupSpace = 0.3f;
        float barSpace = 0.05f; // x4 DataSet
        float barWidth = 0.3f; // x4 DataSet
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

        int groupCount = 5;
        int startYear = 1;


        BarDataSet set1, set2;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);

            set1.setValues(targetValues);
            set2.setValues(achievementValues);

            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();

        } else {
            // create 2 DataSets
            set1 = new BarDataSet(targetValues, "Target");
            set1.setColor(Color.rgb(131, 131, 131));
            set2 = new BarDataSet(achievementValues, "Achievement");
            set2.setColor(Color.rgb(255, 124, 45));

            BarData data = new BarData(set1, set2);
//            data.setValueFormatter(new LargeValueFormatter());
            //data.setValueTypeface(tfLight);

            barChart.setData(data);
        }

        // specify the width each bar should have
        barChart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        barChart.getXAxis().setAxisMinimum(startYear);

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        barChart.getXAxis().setAxisMaximum(startYear + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        Log.d("Metrics", "Maximum" + barChart.getXAxis().getAxisMaximum());
        barChart.groupBars(startYear, groupSpace, barSpace);
        barChart.invalidate();

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();

    }


    private void showFocusSchemeTargetBarGraph(){
        ViewPager graphViewPager;

        productNameString="All";
        ArrayList<GraphData> graphPage = new ArrayList<GraphData>();

        GraphData focusProductGraphData = new GraphData();
        ArrayList<BarEntry> focusProductTargetValues = new ArrayList<>();
        ArrayList<BarEntry> focusProductAchievementValues = new ArrayList<>();

        Cursor focusProductCount = dbHandler.get_focus_product_achv(productNameString);
        int focusProductCountValue = focusProductCount.getCount();

        if (focusProductCountValue == 0) {
            focusProductTargetValues.add(new BarEntry(1, 0f));
            focusProductTargetValues.add(new BarEntry(2, 0f));
            focusProductTargetValues.add(new BarEntry(3, 0f));

            focusProductAchievementValues.add(new BarEntry(1, 0f));
            focusProductAchievementValues.add(new BarEntry(2, 0f));
            focusProductAchievementValues.add(new BarEntry(3, 0f));
        } else {
            int i = 0;
            while (focusProductCount.moveToNext()) {
                focusProductTargetValues.add(new BarEntry(1, Float.parseFloat(focusProductCount.getString(3) != null ? focusProductCount.getString(3) : "0")));
                focusProductTargetValues.add(new BarEntry(2, Float.parseFloat(focusProductCount.getString(4) != null ? focusProductCount.getString(4) : "0")));
                focusProductTargetValues.add(new BarEntry(3, Float.parseFloat(focusProductCount.getString(5) != null ? focusProductCount.getString(5) : "0")));

                focusProductAchievementValues.add(new BarEntry(1, Float.parseFloat(focusProductCount.getString(0) != null ? focusProductCount.getString(0) : "0")));
                focusProductAchievementValues.add(new BarEntry(2, Float.parseFloat(focusProductCount.getString(1) != null ? focusProductCount.getString(1) : "0")));
                focusProductAchievementValues.add(new BarEntry(3, Float.parseFloat(focusProductCount.getString(2) != null ? focusProductCount.getString(2) : "0")));

            }
        }

        focusProductGraphData.setTargetValues(focusProductTargetValues);
        focusProductGraphData.setAchievementValues(focusProductAchievementValues);
        focusProductGraphData.setTitle("Focus");
        graphPage.add(focusProductGraphData);

        GraphData schemeProductGraphData = new GraphData();

        ArrayList<BarEntry> schemeProductTargetValues = new ArrayList<>();
        ArrayList<BarEntry> schemeProductAchievementValues = new ArrayList<>();

        Cursor schemeProductCount = dbHandler.get_scheme_product_achv(productNameString);
        int orderSchemeProductCountValue = schemeProductCount.getCount();
        if (orderSchemeProductCountValue == 0) {
            schemeProductTargetValues.add(new BarEntry(1, 0f));
            schemeProductTargetValues.add(new BarEntry(2, 0f));
            schemeProductTargetValues.add(new BarEntry(3, 0f));

            schemeProductAchievementValues.add(new BarEntry(1, 0f));
            schemeProductAchievementValues.add(new BarEntry(2, 0f));
            schemeProductAchievementValues.add(new BarEntry(3, 0f));
        } else {
            int i = 0;
            while (schemeProductCount.moveToNext()) {
                schemeProductTargetValues.add(new BarEntry(1, Float.parseFloat(schemeProductCount.getString(3) != null ? schemeProductCount.getString(3) : "0")));
                schemeProductTargetValues.add(new BarEntry(2, Float.parseFloat(schemeProductCount.getString(4) != null ? schemeProductCount.getString(4) : "0")));
                schemeProductTargetValues.add(new BarEntry(3, Float.parseFloat(schemeProductCount.getString(5) != null ? schemeProductCount.getString(5) : "0")));

                schemeProductAchievementValues.add(new BarEntry(1, Float.parseFloat(schemeProductCount.getString(0) != null ? schemeProductCount.getString(0) : "0")));
                schemeProductAchievementValues.add(new BarEntry(2, Float.parseFloat(schemeProductCount.getString(1) != null ? schemeProductCount.getString(1) : "0")));
                schemeProductAchievementValues.add(new BarEntry(3, Float.parseFloat(schemeProductCount.getString(2) != null ? schemeProductCount.getString(2) : "0")));

            }
        }

        schemeProductGraphData.setTargetValues(schemeProductTargetValues);
        schemeProductGraphData.setAchievementValues(schemeProductAchievementValues);
        schemeProductGraphData.setTitle("Scheme");
        graphPage.add(schemeProductGraphData);


        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.fragment_secondary_focus_scheme_pop_up, null);
        graphViewPager = alertLayout.findViewById(R.id.secondaryFocusSchemeViewpager);
        productSpinner = alertLayout.findViewById(R.id.ProductSpinner);

        productSpinner.setTitle("Select Product");
        productSpinner.setPositiveButton("OK");

        orderProductSpinnerArrayList = dbHandler.getMetricsProductSpinner();

        OrderProductSpinner cs = new OrderProductSpinner();
        cs.setProduct("All");
        cs.setProd_type("-1");
        cs.setProduct_id("-1");
        orderProductSpinnerArrayList.add(0, cs);

        orderProductSpinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, orderProductSpinnerArrayList);
        orderProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(orderProductSpinnerArrayAdapter);
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;
                productNameString = adapterView.getItemAtPosition(pos).toString();

                ArrayList<GraphData> graphPage = new ArrayList<GraphData>();

                GraphData focusProductGraphData = new GraphData();
                ArrayList<BarEntry> focusProductTargetValues = new ArrayList<>();
                ArrayList<BarEntry> focusProductAchievementValues = new ArrayList<>();

                Cursor focusProductCount = dbHandler.get_focus_product_achv(productNameString);
                int focusProductCountValue = focusProductCount.getCount();

                if (focusProductCountValue == 0) {
                    focusProductTargetValues.add(new BarEntry(1, 0f));
                    focusProductTargetValues.add(new BarEntry(2, 0f));
                    focusProductTargetValues.add(new BarEntry(3, 0f));

                    focusProductAchievementValues.add(new BarEntry(1, 0f));
                    focusProductAchievementValues.add(new BarEntry(2, 0f));
                    focusProductAchievementValues.add(new BarEntry(3, 0f));
                } else {
                    int i = 0;
                    while (focusProductCount.moveToNext()) {
                        focusProductTargetValues.add(new BarEntry(1, Float.parseFloat(focusProductCount.getString(3) != null ? focusProductCount.getString(3) : "0")));
                        focusProductTargetValues.add(new BarEntry(2, Float.parseFloat(focusProductCount.getString(4) != null ? focusProductCount.getString(4) : "0")));
                        focusProductTargetValues.add(new BarEntry(3, Float.parseFloat(focusProductCount.getString(5) != null ? focusProductCount.getString(5) : "0")));

                        focusProductAchievementValues.add(new BarEntry(1, Float.parseFloat(focusProductCount.getString(0) != null ? focusProductCount.getString(0) : "0")));
                        focusProductAchievementValues.add(new BarEntry(2, Float.parseFloat(focusProductCount.getString(1) != null ? focusProductCount.getString(1) : "0")));
                        focusProductAchievementValues.add(new BarEntry(3, Float.parseFloat(focusProductCount.getString(2) != null ? focusProductCount.getString(2) : "0")));

                    }
                }

                focusProductGraphData.setTargetValues(focusProductTargetValues);
                focusProductGraphData.setAchievementValues(focusProductAchievementValues);
                focusProductGraphData.setTitle("Focus");
                graphPage.add(focusProductGraphData);

                GraphData schemeProductGraphData = new GraphData();

                ArrayList<BarEntry> schemeProductTargetValues = new ArrayList<>();
                ArrayList<BarEntry> schemeProductAchievementValues = new ArrayList<>();

                Cursor schemeProductCount = dbHandler.get_scheme_product_achv(productNameString);
                int orderSchemeProductCountValue = schemeProductCount.getCount();
                if (orderSchemeProductCountValue == 0) {
                    schemeProductTargetValues.add(new BarEntry(1, 0f));
                    schemeProductTargetValues.add(new BarEntry(2, 0f));
                    schemeProductTargetValues.add(new BarEntry(3, 0f));

                    schemeProductAchievementValues.add(new BarEntry(1, 0f));
                    schemeProductAchievementValues.add(new BarEntry(2, 0f));
                    schemeProductAchievementValues.add(new BarEntry(3, 0f));
                } else {
                    int i = 0;
                    while (schemeProductCount.moveToNext()) {
                        schemeProductTargetValues.add(new BarEntry(1, Float.parseFloat(schemeProductCount.getString(3) != null ? schemeProductCount.getString(3) : "0")));
                        schemeProductTargetValues.add(new BarEntry(2, Float.parseFloat(schemeProductCount.getString(4) != null ? schemeProductCount.getString(4) : "0")));
                        schemeProductTargetValues.add(new BarEntry(3, Float.parseFloat(schemeProductCount.getString(5) != null ? schemeProductCount.getString(5) : "0")));

                        schemeProductAchievementValues.add(new BarEntry(1, Float.parseFloat(schemeProductCount.getString(0) != null ? schemeProductCount.getString(0) : "0")));
                        schemeProductAchievementValues.add(new BarEntry(2, Float.parseFloat(schemeProductCount.getString(1) != null ? schemeProductCount.getString(1) : "0")));
                        schemeProductAchievementValues.add(new BarEntry(3, Float.parseFloat(schemeProductCount.getString(2) != null ? schemeProductCount.getString(2) : "0")));

                    }
                }

                schemeProductGraphData.setTargetValues(schemeProductTargetValues);
                schemeProductGraphData.setAchievementValues(schemeProductAchievementValues);
                schemeProductGraphData.setTitle("Scheme");
                graphPage.add(schemeProductGraphData);

                System.out.println("Focus::productNameString = " + productNameString);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });

        graphViewPager.setAdapter(new GraphPageAdapter(mContext, graphPage));

        graphViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();

    }





    public void showTargetAchievementGraph(ArrayList targetValues, ArrayList achievementValues, String barChartTitle) {
        BarChart barChart;
        TextView titleTextView;

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.fragment_secondary_plan_visit_pop_up, null);

        //CHART INITIALIZATION
        titleTextView = alertLayout.findViewById(R.id.secondaryChartTitle);
        titleTextView.setText(barChartTitle);


        barChart = alertLayout.findViewById(R.id.secondaryBarChart);
        barChart.setOnChartValueSelectedListener(this);
        barChart.getDescription().setEnabled(false);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);

        barChart.setDrawBarShadow(false);
        barChart.animateY(1400, Easing.EaseInOutQuad);


        barChart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
        mv.setChartView(barChart); // For bounds control
        barChart.setMarker(mv); // Set the marker to the chart

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        //l.setTypeface(tfLight);
        l.setYOffset(10f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(20f);


        XAxis xAxis = barChart.getXAxis();
        //xAxis.setTypeface(tfLight);
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        String[] values = new String[]{"Day", "Week", "Month", "This Quarter", "Year"};

        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));

        YAxis leftAxis = barChart.getAxisLeft();
        //leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceBottom(50f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        leftAxis.setGranularity(1f);

        barChart.getAxisRight().setEnabled(false);
        //DATA INITIALIZATION
        float groupSpace = 0f;
        float barSpace = 0.25f; // x4 DataSet
        float barWidth = 0.25f;
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

        int groupCount = 5;
        int startYear = 1;


        BarDataSet set1, set2;

        if (barChart.getData() != null && barChart.getData().getDataSetCount() > 0) {

            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet) barChart.getData().getDataSetByIndex(1);

            set1.setValues(targetValues);
            set2.setValues(achievementValues);

            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();

        } else {
            // create 2 DataSets
            set1 = new BarDataSet(targetValues, "Target");
            set1.setColor(Color.rgb(131, 131, 131));
            set2 = new BarDataSet(achievementValues, "Achievement");
            set2.setColor(Color.rgb(255, 124, 45));

            BarData data = new BarData(set1, set2);
//            data.setValueFormatter(new LargeValueFormatter());
            //data.setValueTypeface(tfLight);

            barChart.setData(data);
        }

        // specify the width each bar should have
        barChart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        barChart.getXAxis().setAxisMinimum(startYear);

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        barChart.getXAxis().setAxisMaximum(startYear + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        Log.d("Metrics", "Maximum" + barChart.getXAxis().getAxisMaximum());
        barChart.groupBars(startYear, groupSpace, barSpace);
        barChart.invalidate();

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();


    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }


}






