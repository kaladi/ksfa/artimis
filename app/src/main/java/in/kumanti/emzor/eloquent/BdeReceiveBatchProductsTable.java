package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.BdeReceiveBatchProducts;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_BDE_RECEIVE_BATCH_PRODUCTS;

public class BdeReceiveBatchProductsTable extends SQLiteOpenHelper {

    //Columns used for BDE Receive Batch Products Information details for a selected ordered product
    private static final String COLUMN_BDE_RECEIVE_BATCH_PRODUCTS_ID = "Id";
    private static final String COLUMN_STOCK_ISSUE_NUMBER = "stockIssueNumber";
    private static final String COLUMN_STOCK_ISSUE_DATE = "stockIssueDate";
    private static final String COLUMN_FROM_BDE_CODE = "fromBdeCode";
    private static final String COLUMN_TO_BDE_CODE = "toBdeCode";
    private static final String COLUMN_PRODUCT_CODE = "productCode";
    private static final String COLUMN_EXPIRY_DATE = "expiryDate";
    private static final String COLUMN_BATCH_NUMBER = "batchNumber";
    private static final String COLUMN_ISSUE_QUANTITY = "issueQuantity";
    private static final String COLUMN_CREATED_AT = "createdAt";
    private static final String COLUMN_UPDATED_AT = "updatedAt";

    public Context context;
    String selectQuery;
    Cursor cursor;

    public BdeReceiveBatchProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_BDE_RECEIVE_BATCH_PRODUCTS);

        String distributorsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_BDE_RECEIVE_BATCH_PRODUCTS + "(" +
                COLUMN_BDE_RECEIVE_BATCH_PRODUCTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_STOCK_ISSUE_NUMBER + " TEXT, " +
                COLUMN_STOCK_ISSUE_DATE + " TEXT, " +
                COLUMN_FROM_BDE_CODE + " TEXT, " +
                COLUMN_TO_BDE_CODE + " TEXT, " +
                COLUMN_PRODUCT_CODE + " TEXT, " +
                COLUMN_EXPIRY_DATE + " TEXT, " +
                COLUMN_BATCH_NUMBER + " TEXT, " +
                COLUMN_ISSUE_QUANTITY + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(distributorsTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(BdeReceiveBatchProducts bdeReceiveBatchProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_STOCK_ISSUE_NUMBER, bdeReceiveBatchProducts.getStockIssueNumber());
        values.put(COLUMN_STOCK_ISSUE_DATE, bdeReceiveBatchProducts.getStockIssueDate());
        values.put(COLUMN_FROM_BDE_CODE, bdeReceiveBatchProducts.getFromBdeCode());
        values.put(COLUMN_TO_BDE_CODE, bdeReceiveBatchProducts.getToBdeCode());
        values.put(COLUMN_PRODUCT_CODE, bdeReceiveBatchProducts.getProductCode());
        values.put(COLUMN_EXPIRY_DATE, bdeReceiveBatchProducts.getExpiryDate());
        values.put(COLUMN_BATCH_NUMBER, bdeReceiveBatchProducts.getBatchNumber());
        values.put(COLUMN_ISSUE_QUANTITY, bdeReceiveBatchProducts.getIssueQuantity());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_BDE_RECEIVE_BATCH_PRODUCTS, null, values);
        db.close();
        return id;
    }


    ArrayList<BdeReceiveBatchProducts> getBdeReceiveBatchProducts(String stockIssueNumber, String productCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BDE_RECEIVE_BATCH_PRODUCTS + " where stockIssueNumber =? and productCode=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{stockIssueNumber, productCode});

        Log.d("ViewStock--", "Get Products  ");
        ArrayList<BdeReceiveBatchProducts> bdeReceiveBatchProductsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            BdeReceiveBatchProducts batchProducts = new BdeReceiveBatchProducts();
            Log.d("View stock MYDATA--", "Data  id" + cursor.getInt(0));
            batchProducts.setId(cursor.getInt(0));
            batchProducts.setStockIssueNumber(cursor.getString(1));
            batchProducts.setStockIssueDate(cursor.getString(2));

            batchProducts.setFromBdeCode(cursor.getString(3));
            batchProducts.setToBdeCode(cursor.getString(4));

            batchProducts.setExpiryDate(cursor.getString(5));
            batchProducts.setBatchNumber(cursor.getString(6));
            batchProducts.setIssueQuantity(cursor.getString(7));


            bdeReceiveBatchProductsArrayList.add(batchProducts);
        }
        cursor.close();
        sqlWrite.close();
        return bdeReceiveBatchProductsArrayList;
    }

}
