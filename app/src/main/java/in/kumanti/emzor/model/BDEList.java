package in.kumanti.emzor.model;


import java.io.Serializable;

public class BDEList implements Serializable {
    public int bdeId;
    public String bdeCode;
    public String bdeName;
    public String createdAt;
    public String updatedAt;


    public int getBdeId() {
        return bdeId;
    }

    public void setBdeId(int bdeId) {
        this.bdeId = bdeId;
    }

    public String getBdeCode() {
        return bdeCode;
    }

    public void setBdeCode(String bdeCode) {
        this.bdeCode = bdeCode;
    }

    public String getBdeName() {
        return bdeName;
    }

    public void setBdeName(String bdeName) {
        this.bdeName = bdeName;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return bdeName;
    }

}
