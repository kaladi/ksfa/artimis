package in.kumanti.emzor.model;

public class MetricsTarget {

    private String sales_rep_id;
    private String targetID;
    private String targetCode;
    private String targetType;

    private String targetValue;
    private String type;
    private String product;
    private String value;

    private String fromDate;
    private String toDate;

    private String cType;


    public MetricsTarget(String sales_rep_id,
                         String targetID,
                         String targetCode,
                         String targetType,
                         String targetValue,
                         String type,
                         String product,
                         String value,
                         String fromDate,
                         String toDate,
                         String cType



    ) {

        this.sales_rep_id = sales_rep_id;
        this.targetID = targetID;
        this.targetCode = targetCode;
        this.targetType = targetType;

        this.targetValue = targetValue;
        this.type = type;
        this.product = product;
        this.value = value;

        this.fromDate = fromDate;
        this.toDate = toDate;

        this.cType = cType;
    }

    public String getSales_rep_id() {
        return sales_rep_id;
    }

    public String getTargetID() {
        return targetID;
    }

    public String getTargetCode() {
        return targetCode;
    }

    public String getTargetType() {
        return targetType;
    }

    public String getTargetValue() {
        return targetValue;
    }
    public String getType() {
        return type;
    }

    public String getProduct() {
        return product;
    }

    public String getValue() {
        return value;
    }
    public String getFromDate() {
        return fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public String getcType() {
        return cType;
    }


}

