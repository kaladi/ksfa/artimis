package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.NewJourneyPlanCustomers;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_DETAILS;
import static in.kumanti.emzor.utils.Constants.TABLE_NEW_JOURNEY_PLAN_CUSTOMERS;

public class NewJourneyPlanCustomerTable extends SQLiteOpenHelper {

    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_ID = "jpCustomerId";
    private static final String COLUMN_JOURNEY_PLAN_NUMBER = "journeyPlanNumber";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_DAY_CODE = "dayCode";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_CURRENT_DAY_DATE = "currentDayDate";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_TYPE = "customerType";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_IS_SYNC = "isSync";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_CREATED_AT = "createdAt";
    private static final String COLUMN_JOURNEY_PLAN_CUSTOMER_UPDATED_AT = "updatedAt";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public NewJourneyPlanCustomerTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_NEW_JOURNEY_PLAN_CUSTOMERS);

        String stockReturnProductTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NEW_JOURNEY_PLAN_CUSTOMERS + "(" +
                COLUMN_JOURNEY_PLAN_CUSTOMER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_JOURNEY_PLAN_NUMBER + " TEXT, " +
                COLUMN_JOURNEY_PLAN_CUSTOMER_DAY_CODE + " TEXT, " +
                COLUMN_JOURNEY_PLAN_CUSTOMER_CURRENT_DAY_DATE + " TEXT, " +
                COLUMN_JOURNEY_PLAN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_JOURNEY_PLAN_CUSTOMER_TYPE + " TEXT, " +
                COLUMN_JOURNEY_PLAN_CUSTOMER_IS_SYNC + " TEXT, " +
                COLUMN_JOURNEY_PLAN_CUSTOMER_LATEST_SYNC_DATE + " TEXT, " +

                COLUMN_JOURNEY_PLAN_CUSTOMER_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_JOURNEY_PLAN_CUSTOMER_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(stockReturnProductTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(NewJourneyPlanCustomers newJourneyPlanCustomers) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_JOURNEY_PLAN_NUMBER, newJourneyPlanCustomers.getJourneyPlanNumber());
        values.put(COLUMN_JOURNEY_PLAN_CUSTOMER_DAY_CODE, newJourneyPlanCustomers.getDayCode());
        values.put(COLUMN_JOURNEY_PLAN_CUSTOMER_CURRENT_DAY_DATE, newJourneyPlanCustomers.getCurrentDayDate());

        values.put(COLUMN_JOURNEY_PLAN_CUSTOMER_CODE, newJourneyPlanCustomers.getCustomerCode());
        values.put(COLUMN_JOURNEY_PLAN_CUSTOMER_TYPE, newJourneyPlanCustomers.getCustomerType());
        values.put(COLUMN_JOURNEY_PLAN_CUSTOMER_IS_SYNC, newJourneyPlanCustomers.getIsSync());
        values.put(COLUMN_JOURNEY_PLAN_CUSTOMER_LATEST_SYNC_DATE, newJourneyPlanCustomers.getLatestSynDate());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_NEW_JOURNEY_PLAN_CUSTOMERS, null, values);
        db.close();
        return id;
    }


    public ArrayList<NewJourneyPlanCustomers> getJourneyPlanCustomersByDate(String journeyPlanNumber, String dayCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT jpc.jpCustomerId,jpc.journeyPlanNumber,jpc.dayCode,jpc.currentDayDate,jpc.customerCode,jpc.customerType,cd.customer_name FROM " + TABLE_NEW_JOURNEY_PLAN_CUSTOMERS + " as jpc left join " + TABLE_CUSTOMER_DETAILS + " as cd on cd.customer_id=jpc.customerCode WHERE journeyPlanNumber=? and dayCode=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{journeyPlanNumber, dayCode});

        ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            NewJourneyPlanCustomers newJourneyPlanCustomers = new NewJourneyPlanCustomers();
            newJourneyPlanCustomers.setJpCustomerId(cursor.getInt(0));
            newJourneyPlanCustomers.setJourneyPlanNumber(cursor.getString(1));
            newJourneyPlanCustomers.setDayCode(cursor.getString(2));
            newJourneyPlanCustomers.setCurrentDayDate(cursor.getString(3));
            newJourneyPlanCustomers.setCustomerCode(cursor.getString(4));
            newJourneyPlanCustomers.setCustomerType(cursor.getString(5));
            newJourneyPlanCustomers.setCustomerName(cursor.getString(6));

            newJourneyPlanCustomersArrayList.add(i, newJourneyPlanCustomers);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return newJourneyPlanCustomersArrayList;
    }

    public ArrayList<NewJourneyPlanCustomers> getJourneyPlanCustomersDetailsForSync() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_NEW_JOURNEY_PLAN_CUSTOMERS;

        ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            NewJourneyPlanCustomers newJourneyPlanCustomers = new NewJourneyPlanCustomers();
            newJourneyPlanCustomers.setTabCode(Constants.TAB_CODE);
            newJourneyPlanCustomers.setCompanyCode(Constants.COMPANY_CODE);
            newJourneyPlanCustomers.setBdeCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            newJourneyPlanCustomers.setJpCustomerId(id);
            newJourneyPlanCustomers.setJourneyPlanNumber(cursor.getString(1));
            newJourneyPlanCustomers.setDayCode(cursor.getString(2));
            newJourneyPlanCustomers.setCurrentDayDate(cursor.getString(3));
            newJourneyPlanCustomers.setCustomerCode(cursor.getString(4));
            newJourneyPlanCustomers.setCustomerType(cursor.getString(5));
            newJourneyPlanCustomers.setCreatedAt(cursor.getString(8));

            newJourneyPlanCustomersArrayList.add(newJourneyPlanCustomers);
        }
        cursor.close();
        sqlWrite.close();
        return newJourneyPlanCustomersArrayList;
    }


}
