package in.kumanti.emzor.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FileUtils {

    public static File getStorageDir() {
        File storageDir = new File(Environment.getExternalStorageDirectory()
                + File.separator + "KSFA" + File.separator);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        return storageDir;
    }


    public static File getCSVStorageDir() {
        File storageDir = new File(Environment.getExternalStorageDirectory()
                + File.separator + "KSFA" + File.separator + "csv" + File.separator);

        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        return storageDir;
    }


    public static boolean fileFTPUpload(String sourcePath, String destinationPath) {
        FTPClient con = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();
            StrictMode.setThreadPolicy(policy);
            Log.d("FileUpload", "FilePath");
            con = new FTPClient();
            con.connect("182.74.154.75");
            Log.d("FileUpload", "FilePath Conneted");
            if (con.login("emzor", "Kum*emz#123")) {
                Log.d("FileUpload", "FilePath Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
                String data = Environment.getExternalStorageDirectory() + File.separator
                        + "KSFA" + File.separator + sourcePath;
                Log.d("FileUpload", "FilePath " + data);
                FileInputStream in = new FileInputStream(new File(data));
                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + destinationPath, in);
                in.close();
                if (result) {
                    Log.d("FileUpload", "Success");
                    return true;
                }

                con.logout();
                con.disconnect();

//                StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder()
//                        .detectAll()
//                        .penaltyLog()
//                        .penaltyDeath()
//                        .build();
//                StrictMode.setThreadPolicy(policy1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Error" + e.getMessage());
        }
        return false;
    }

    /**
     * Writing the json string into the local text file
     */


    private static void writeUsingFiles(String jsonString) {
        try {
            String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/test/jsonString.txt";


            FileWriter fw = new FileWriter(destinationPath);
            fw.write(jsonString);
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public void storeImage(Bitmap image, String fileName, String folderPath) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + File.separator + "KSFA" + File.separator + "images" + File.separator + folderPath);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return;
            }
        }

        // Create a media file name
        File pictureFile;
        pictureFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
        if (pictureFile == null) {
            Log.d("FileStore",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("FileStore", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("FileStore", "Error accessing file: " + e.getMessage());
        }
    }

    public static String getMonthYear() {
        return new SimpleDateFormat("dd MMM yy", Locale.getDefault()).format(new Date());
    }

    public static String getHourMin() {
        return new SimpleDateFormat("dd MMM YY\nhh:mm a", Locale.getDefault()).format(new Date());
    }
}
