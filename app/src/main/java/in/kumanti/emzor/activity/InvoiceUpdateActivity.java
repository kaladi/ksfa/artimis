package in.kumanti.emzor.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.InvoiceBatch;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;


public class InvoiceUpdateActivity extends MainActivity {

    TextView prod_name, uom_txt, qty_txt, price_txt, value_txt;
    String prod, uom, qty, customer_id, prod_id, id, checkin_time, invoice_number, customer_type, login_id, batch_controlled;
    ListView batch_list;
    int invoice_id = 0;
    ArrayList<InvoiceBatch> productbatchLists;
    InvoiceBatch batchlist;
    Double price_val_update = 0.00;
    Double order_val_update = 0.00;
    DecimalFormat formatter;
    ImageView no, yes;
    Double price = 0.00;
    Double value = 0.00;
    int remaining_qty = 0, vat;
    int allocate_qty = 0;
    int order_quantity = 0;
    InvoiceBatchAdapter adapter1;
    int disc = 0;
    double discount_value = 0.00;
    MyDBHandler dbHandler;
    ImageView actionbarBackButton, deviceInfo;
    String invoiceType;
    private TextView marqueeText, actionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_update_invoice);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        //Toast.makeText(getApplicationContext(), invoiceType, Toast.LENGTH_LONG).show();

        actionBarTitle.setText("Update Invoice");


        qty_txt.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});

        if ("Doctor".equals(customer_type)) {
            price_txt.setEnabled(false);
        }

        price_val_update = price;
        String value_format = formatter.format(value);
        prod_name.setText(prod);
        uom_txt.setText(uom);
        qty_txt.setText(qty);
        price_txt.setText(Double.toString(price));
        value_txt.setText(value_format);

        if ("Invoice".equals(invoiceType)) {

            loadproductbatchlist(Integer.parseInt(qty));
            showbatchdeatils();

        }

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uom_txt.setEnabled(true);
                uom_txt.requestFocus();
                uom_txt.setEnabled(false);

                if (qty_txt.getText().toString().equals("")) {
                    qty_txt.setError(getString(R.string.error_field_required));

                } else if (qty_txt.getText().toString().equals("0")) {
                    qty_txt.setError(getString(R.string.Zero_Validation));
                } else {

                    int batch_value = dbHandler.get_updatedbatch_quantity(String.valueOf(invoice_id), prod_id);
                    int qty1 = Integer.parseInt(qty_txt.getText().toString());

                    if ("Invoice".equals(invoiceType)) {

                        if ("Yes".equals(batch_controlled)) {
                            if (qty1 == batch_value) {

                                int quan = Integer.parseInt(qty_txt.getText().toString());
                                double amnt = Double.parseDouble(price_txt.getText().toString());
                                order_val_update = quan * amnt + vat * quan * amnt / 100 - Integer.valueOf(quan) * amnt
                                        * Integer.valueOf(disc) / 100;
                                discount_value = ((Integer.valueOf(quan) * amnt) + (vat * Integer.valueOf(quan) * amnt) / 100) - order_val_update;

                                dbHandler.update_invoice_details(id, String.valueOf(invoice_id), qty_txt.getText().toString(), price_txt.getText().toString(), order_val_update, quan * amnt + vat * quan * amnt / 100, discount_value);
                                Intent myintent = new Intent(getApplicationContext(), InvoiceProductActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("checkin_time", checkin_time);
                                bundle.putString("customer_id", customer_id);
                                bundle.putString("invoice_id", Integer.toString(invoice_id));
                                bundle.putString("login_id", login_id);
                                bundle.putString("invoice_num", invoice_number);
                                bundle.putString("invoice_type", invoiceType);


                                //Add the bundle to the intent
                                myintent.putExtras(bundle);

                                //Fire that second activity
                                startActivity(myintent);
                            } else {
                                AlertDialog.Builder alert1 = new AlertDialog.Builder(v.getRootView().getContext());
                                alert1.setMessage("Invoiced Quantity doesn't matched with Batch Quantity");
                                alert1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        dialog.dismiss();

                                    }

                                });

                                alert1.show();
                            }
                        } else {
                            int quan = Integer.parseInt(qty_txt.getText().toString());
                            int stock_batch_value = dbHandler.get_stockreceipt_quantity(prod_id);
                            if (quan > stock_batch_value) {
                                qty_txt.setError(getString(R.string.BatchQuantity));
                            } else {
                                navigateInvoice();
                            }


                        }
                    } else {
                        navigateInvoice();
                    }

                }

            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Invoice".equals(invoiceType)) {
                    dbHandler.deletebatch(prod_id);
                    loadproductbatchlist(invoice_id, Integer.parseInt(qty));
                    loadproductbatchlist_batch();
                    loadproductbatchlist1();
                    dbHandler.updatebatchstatus(invoice_id, "Inprocess");

                    finish();
                } else {
                    finish();
                }

            }
        });

        qty_txt.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (qty_txt.getText().toString().equals("")) {
                    value_txt.setText("");
                    if (productbatchLists.isEmpty()) {

                    } else {
                        productbatchLists.clear();

                    }
                    if ("Invoice".equals(invoiceType)) {
                        if ("Yes".equals(batch_controlled)) {
                            adapter1.notifyDataSetChanged();

                        } else {

                        }
                    }


                } else {
                    int quan = Integer.parseInt(qty_txt.getText().toString());
                    double amnt = Double.parseDouble(price_txt.getText().toString());
                    order_val_update = quan * amnt;
                    String orderval_formatted = formatter.format(order_val_update);
                    value_txt.setText(orderval_formatted);
                    if ("Invoice".equals(invoiceType)) {
                        dbHandler.updatebatchqty(invoice_id, prod_id);
                        loadproductbatchlist(quan);
                        showbatchdeatils();
                    } else {

                    }

                }
            }
        });

        price_txt.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (price_txt.getText().toString().equals("")) {
                    value_txt.setText("");

                } else {
                    int quan = Integer.parseInt(qty_txt.getText().toString());
                    double amnt = Double.parseDouble(price_txt.getText().toString());
                    order_val_update = Double.valueOf(quan * amnt);
                    String orderval_formatted = formatter.format(order_val_update);
                    value_txt.setText(orderval_formatted);


                }
            }
        });

    }

    private void navigateInvoice() {
        int quan = Integer.parseInt(qty_txt.getText().toString());
        int stock_batch_value = dbHandler.get_stockreceipt_quantity(prod_id);
        double amnt = Double.parseDouble(price_txt.getText().toString());
        order_val_update = quan * amnt + vat * quan * amnt / 100 - Integer.valueOf(quan) * amnt
                * Integer.valueOf(disc) / 100;
        discount_value = ((Integer.valueOf(quan) * amnt) + (vat * Integer.valueOf(quan) * amnt) / 100) - order_val_update;

        dbHandler.update_invoice_details(id, String.valueOf(invoice_id), qty_txt.getText().toString(), price_txt.getText().toString(), order_val_update, quan * amnt + vat * quan * amnt / 100, discount_value);
        Intent myintent = new Intent(getApplicationContext(), InvoiceProductActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("checkin_time", checkin_time);
        bundle.putString("customer_id", customer_id);
        bundle.putString("invoice_id", Integer.toString(invoice_id));
        bundle.putString("login_id", login_id);
        bundle.putString("invoice_num", invoice_number);
        bundle.putString("invoice_type", invoiceType);


        //Add the bundle to the intent
        myintent.putExtras(bundle);

        //Fire that second activity
        startActivity(myintent);
    }

    private void populateHeaderDetails() {

        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            login_id = bundle.getString("login_id");
            customer_id = bundle.getString("customer_id");
            checkin_time = bundle.getString("checkin_time");
            prod = bundle.getString("product");
            uom = bundle.getString("uom");
            qty = bundle.getString("qty");
            price = bundle.getDouble("price");
            value = bundle.getDouble("value");
            prod_id = bundle.getString("prod_id");
            invoice_id = bundle.getInt("invoice_id");
            id = bundle.getString("id");
            invoice_number = bundle.getString("invoice_num");
            customer_type = bundle.getString("customer_type");
            disc = bundle.getInt("discount");
            vat = bundle.getInt("vat");
            batch_controlled = bundle.getString("batch_controlled");
            invoiceType = bundle.getString("invoice_type");

        }

    }

    private void initializeViews() {
        deviceInfo = findViewById(R.id.img_info);
        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        prod_name = findViewById(R.id.prod_name);
        uom_txt = findViewById(R.id.uom);
        qty_txt = findViewById(R.id.quantity);
        price_txt = findViewById(R.id.price);
        value_txt = findViewById(R.id.value);
        batch_list = findViewById(R.id.batch_list);
        formatter = new DecimalFormat("#,###.00");
        no = findViewById(R.id.back);
        yes = findViewById(R.id.Yes);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        productbatchLists = new ArrayList<InvoiceBatch>();


    }

    public void showbatchdeatils() {

        batch_list.setAdapter(null);

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails1(prod_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
            batch_list.setAdapter(null);
        } else {
            int i = 0;
            while (data.moveToNext()) {
                batchlist = new InvoiceBatch(
                        data.getString(1),
                        data.getString(0),
                        data.getInt(2),
                        data.getInt(3));
                productbatchLists.add(i, batchlist);

                i++;
            }

            adapter1 = new InvoiceBatchAdapter(getApplicationContext(), R.layout.list_invoice_batch, productbatchLists, prod_id);
            batch_list.setAdapter(adapter1);

        }

    }

    private void loadproductbatchlist(int quan) {

        Cursor data = dbHandler.getProductBatchDetails1(prod_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
        } else {
            int i = 0;
            allocate_qty = quan;
            while (data.moveToNext()) {

                int onhand_qty = data.getInt(2);

                if (onhand_qty >= allocate_qty) {
                    // remaining_qty=onhand_qty-allocate_qty;

                    dbHandler.updateInvoiceBatch(prod_id, String.valueOf(allocate_qty), data.getString(0), String.valueOf(invoice_id));

                    break;

                } else {

                    remaining_qty = allocate_qty - onhand_qty;

                    int test = allocate_qty;
                    if (allocate_qty <= onhand_qty) {
                        test = allocate_qty;
                    } else {
                        test = onhand_qty;
                    }

                    dbHandler.updateInvoiceBatch(prod_id, String.valueOf(test), data.getString(0), String.valueOf(invoice_id));
                    allocate_qty = remaining_qty;
                }
                i++;
            }

        }
    }

    private void loadproductbatchlist(int invoice_id, int quantity) {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails(prod_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {

        } else {
            int i = 0;
            while (data.moveToNext()) {


                long id2 = dbHandler.insert_product_batch
                        (data.getString(0),
                                data.getString(1),
                                data.getString(2),
                                data.getString(3),
                                null,
                                null,
                                String.valueOf(invoice_id),
                                "Draft"
                        );

            }

            i++;


        }

    }

    private void loadproductbatchlist_batch() {

        Cursor data = dbHandler.getProductBatchDetails2(prod_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
        } else {
            int i = 0;
            allocate_qty = Integer.parseInt(qty);
            while (data.moveToNext()) {

                int onhand_qty = data.getInt(2);
                if (onhand_qty >= allocate_qty) {
                    // remaining_qty=onhand_qty-allocate_qty;

                    dbHandler.updateInvoiceBatch(prod_id, String.valueOf(allocate_qty), data.getString(0), String.valueOf(invoice_id));
                    //dbHandler.updatebatchqty(String.valueOf(invoice_id), product_id, data.getString(1));

                    break;

                } else {

                    remaining_qty = allocate_qty - onhand_qty;
                    Log.i("remaining_qty", String.valueOf(remaining_qty));


                    int test = allocate_qty;
                    if (allocate_qty <= onhand_qty) {
                        test = allocate_qty;
                    } else {
                        test = onhand_qty;
                    }

                    dbHandler.updateInvoiceBatch(prod_id, String.valueOf(test), data.getString(0), String.valueOf(invoice_id));
                    allocate_qty = remaining_qty;
                }
                i++;
            }

        }
    }

    private void loadproductbatchlist1() {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails2(prod_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
            batch_list.setAdapter(null);
        } else {
            int i = 0;
            while (data.moveToNext()) {

                batchlist = new InvoiceBatch(
                        data.getString(1),
                        data.getString(0),
                        data.getInt(2),
                        data.getInt(3));
                productbatchLists.add(i, batchlist);
                i++;
            }

            InvoiceBatchAdapter adapter = new InvoiceBatchAdapter(getApplicationContext(), R.layout.list_invoice_batch, productbatchLists, prod_id);
            batch_list.setAdapter(adapter);

        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    public class InvoiceBatchAdapter extends ArrayAdapter<InvoiceBatch> {

        Activity mActivity = null;
        AlertDialog alertDialog1;
        Globals globals;
        MyDBHandler dbHandler;
        ViewHolder viewHolder = null;
        private LayoutInflater mInflater;
        private ArrayList<InvoiceBatch> productbatchLists;
        private int mViewResourceId;
        private Context c;


        public InvoiceBatchAdapter(Context context, int textViewResourceId, ArrayList<InvoiceBatch> productbatchLists, String prod_id1) {
            super(context, textViewResourceId, productbatchLists);
            this.productbatchLists = productbatchLists;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            dbHandler = new MyDBHandler(c, null, null, 1);
            viewHolder = null;

        }

        public int getCount() {
            return productbatchLists.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public View getView(final int position, View convertView, ViewGroup parent) {


            convertView = mInflater.inflate(mViewResourceId, null);
            final InvoiceBatch user = productbatchLists.get(position);
            final int pos = position;

            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.expirydate = convertView.findViewById(R.id.batch_expirydate);
                viewHolder.batchnumber = convertView.findViewById(R.id.batch_batchnumber);
                viewHolder.quantity = convertView.findViewById(R.id.batch_quantity);
                viewHolder.invoices = convertView.findViewById(R.id.batch_invoices);


                if (viewHolder.expirydate != null) {
                    viewHolder.expirydate.setText(user.getBatchexpirydate());
                }
                if (viewHolder.batchnumber != null) {
                    viewHolder.batchnumber.setText((user.getBatchnumber()));
                }

                if (viewHolder.quantity != null) {
                    viewHolder.quantity.setText((String.valueOf(user.getBatchquantity())));
                } else {
                    viewHolder.quantity.setText("0");
                }

                if (viewHolder.invoices != null) {
                    viewHolder.invoices.setText((String.valueOf(user.getBatchinvoice())));
                } else {
                    viewHolder.invoices.setText("0");
                }

                //viewHolder.invoices.setText(productbatchLists.get(position).getBatchinvoice());
                // viewHolder.invoices.setId(position);

                //we need to update adapter once we finish with editing
                viewHolder.invoices.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            final int position = v.getId();
                            final EditText Caption = (EditText) v;
                            String batchnumber = user.getBatchnumber();
                            //String qty=Caption.getText().toString();
                            //int qty1=Integer.parseInt(qty);
                            //Toast.makeText(getApplicationContext(), qty1, Toast.LENGTH_LONG).show();

                            dbHandler.updateInvoiceBatch(prod_id, Caption.getText().toString(), batchnumber, String.valueOf(invoice_id));

                        }
                    }
                });

            }

            return convertView;
        }


        public class ViewHolder {
            TextView expirydate, batchnumber, quantity, invoices;
            EditText dummy;

        }

        class ListItem {
            String caption;
        }


    }
}
