package in.kumanti.emzor.model;

import java.util.ArrayList;

public class InvoiceProductItems {

    private String product;
    private String uom;
    private String quantity;
    private String price;
    private String value;
    private String invoice_line_id;
    private String product_id;
    private String discount;
    private double price_db;
    private double value_db;
    private int vat, invoice_id;
    private String batch_controlled;
    private String error_productQuantity;

    private String returnQuantity;
    private String errorQuantity;
    private String errorProduct;
    private ArrayList<CustomerReturnBatch> customerReturnBatchArrayList;

    public InvoiceProductItems() {

    }

    public InvoiceProductItems(String product1, String uom1, String quantity1, String price1, String value1, String invoice_line_id1, String product_id1, String discount1, double price_db1, double value_db1, int vat1, String batch_controlled1) {

        product = product1;
        uom = uom1;
        quantity = quantity1;
        price = price1;
        value = value1;
        invoice_line_id = invoice_line_id1;
        product_id = product_id1;
        discount = discount1;
        price_db = price_db1;
        value_db = value_db1;
        vat = vat1;
        batch_controlled = batch_controlled1;


    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product1) {
        product = product1;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom1) {
        uom = uom1;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity1) {
        quantity = quantity1;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price1) {
        price = price1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value1) {
        value = value1;
    }

    public String getInvoice_line_id() {
        return invoice_line_id;
    }

    public void setInvoice_line_id(String invoice_line_id1) {
        invoice_line_id = invoice_line_id1;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id1) {
        product_id = product_id1;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount1) {
        discount = discount1;
    }

    public double getPrice_db() {
        return price_db;
    }

    public void setPrice_db(double price_db1) {
        price_db = price_db1;
    }

    public double getValue_db() {
        return value_db;
    }

    public void setValue_db(double value_db1) {
        value_db = value_db1;
    }

    public int getVat() {
        return vat;
    }

    public void setVat(int vat1) {
        vat = vat1;
    }

    public String getBatch_controlled() {
        return batch_controlled;
    }

    public void setBatch_controlled(String batch_controlled1) {
        batch_controlled = batch_controlled1;
    }

    public String getError_productQuantity() {
        return error_productQuantity;
    }

    public void setError_productQuantity(String error_productQuantity) {
        this.error_productQuantity = error_productQuantity;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getErrorQuantity() {
        return errorQuantity;
    }

    public void setErrorQuantity(String errorQuantity) {
        this.errorQuantity = errorQuantity;
    }

    public ArrayList<CustomerReturnBatch> getCustomerReturnBatchArrayList() {
        return customerReturnBatchArrayList;
    }

    public void setCustomerReturnBatchArrayList(ArrayList<CustomerReturnBatch> customerReturnBatchArrayList) {
        this.customerReturnBatchArrayList = customerReturnBatchArrayList;
    }

    public int getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(int invoice_id) {
        this.invoice_id = invoice_id;
    }

    public String getErrorProduct() {
        return errorProduct;
    }

    public void setErrorProduct(String errorProduct) {
        this.errorProduct = errorProduct;
    }

    @Override
    public String toString() {
        return product;
    }
}

