package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class SRMaster {

    public String imageName;
    public String imageUrl;
    public String region;
    private String sr_code;
    private String sr_name;
    private String manager_code;
    private String manager_name;
    private String branch_code;
    private String branch_name;
    private String email;
    private String mobile;


    public SRMaster(

            String sr_code,
            String sr_name,
            String manager_code,
            String manager_name,
            String branch_code,
            String branch_name,
            String email,
            String mobile,
            String imageFileName,
            String imageUrl,
            String region
    ) {

        this.sr_code = sr_code;
        this.sr_name = sr_name;
        this.manager_code = manager_code;
        this.manager_name = manager_name;
        this.branch_code = branch_code;
        this.branch_name = branch_name;
        this.email = email;
        this.mobile = mobile;
        this.imageName = imageFileName;
        this.imageUrl = imageUrl;
        this.region = region;


    }

    public String getSr_code() {
        return sr_code;
    }

    public String getSr_name() {
        return sr_name;
    }

    public String getManager_code() {
        return manager_code;
    }

    public String getManager_name() {
        return manager_name;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile() {
        return mobile;
    }


    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}

