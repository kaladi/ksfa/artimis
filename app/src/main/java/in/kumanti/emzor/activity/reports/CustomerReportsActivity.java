package in.kumanti.emzor.activity.reports;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.activity.NewCustomerActivity;
import in.kumanti.emzor.activity.OrderProductActivity;
import in.kumanti.emzor.activity.StartStopActivity;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CustomerReports;
import in.kumanti.emzor.utils.Globals;


public class CustomerReportsActivity extends MainActivity {

    TextView sr_name, actionBarTitle;
    String login_id, checkin_time;
    ArrayList<CustomerReports> customerReports;
    CustomerReports customerDetailsLists1;
    CustomerDetailsListAdapter adapter = null;
    String sr_name1, marqueeTextString;
    MyDBHandler dbHandler;
    ImageView actionbarBackButton, deviceInfo;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_customer_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        marqueeText.setSelected(true);
        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);

        actionBarTitle.setText("Customer Report");


        sr_name.setText(sr_name1);

        customerReports = new ArrayList<CustomerReports>();

//        getCustomerDetails();


        /*actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
*/


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

    }

    private void getCustomerDetails() {
        customerReports.clear();
        System.out.println("AAA0Customer::login_id = " + login_id);
        Cursor data = dbHandler.getCustomerDetailsofSalesRep(login_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                customerDetailsLists1 = new CustomerReports(data.getString(0), data.getString(1), data.getString(2), data.getString(3), data.getString(4), data.getString(5), data.getString(6),data.getString(7));
                customerReports.add(i, customerDetailsLists1);
                i++;
            }

            adapter = new CustomerDetailsListAdapter(getApplicationContext(), R.layout.list_customer_details, customerReports);
            ListView list_sales_line = findViewById(R.id.customer_list);
            list_sales_line.setAdapter(adapter);
            //adapter.refreshEvents(invoiceProductLists);
        }

    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        login_id = bundle.getString("login_id");


    }

    private void initializeViews() {

        actionbarBackButton = findViewById(R.id.back);
        sr_name = findViewById(R.id.sr_name);
        actionBarTitle = findViewById(R.id.toolbar_title);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);


    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();


    }

    public class CustomerDetailsListAdapter extends ArrayAdapter<CustomerReports> {

        CustomerReports customerReports1;
        Activity mActivity = null;
        android.support.v7.app.AlertDialog alertDialog1;
        Globals globals;
        MyDBHandler dbHandler;
        private LayoutInflater mInflater;
        private ArrayList<CustomerReports> users;
        private int mViewResourceId;
        private Context c;

        public CustomerDetailsListAdapter(Context context, int textViewResourceId, ArrayList<CustomerReports> users) {
            super(context, textViewResourceId, users);
            this.users = users;
            this.customerReports1 = customerReports1;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            dbHandler = new MyDBHandler(c, null, null, 1);

        }


        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            convertView = mInflater.inflate(mViewResourceId, null);
            final CustomerReports user = users.get(position);
            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.serial = convertView.findViewById(R.id.serial);
                viewHolder.customer_name = convertView.findViewById(R.id.customer_name);
                viewHolder.customer_type = convertView.findViewById(R.id.customer_type);
                viewHolder.location = convertView.findViewById(R.id.location);
                viewHolder.city = convertView.findViewById(R.id.city);
                viewHolder.mobile = convertView.findViewById(R.id.mobile);
                viewHolder.email = convertView.findViewById(R.id.email);
                viewHolder.vm = convertView;

                if (viewHolder.serial != null) {
                    viewHolder.serial.setText(user.getSerial());
                }
                if (viewHolder.customer_name != null) {
                    viewHolder.customer_name.setText((user.getCustomer_name()));
                }
                if (viewHolder.customer_type != null) {
                    viewHolder.customer_type.setText((user.getCustomerType()));
                }
                if (viewHolder.location != null) {
                    viewHolder.location.setText((user.getLocation_name()));
                }
                if (viewHolder.city != null) {
                    viewHolder.city.setText((user.getCity()));
                }
                if (viewHolder.mobile != null) {
                    viewHolder.mobile.setText((user.getMobile()));
                }
                if (viewHolder.email != null) {
                    viewHolder.email.setText((user.getEmail()));
                }

                if(user.getCustomerType().equals("Secondary") || user.getCustomerType().equals("Doctor") || user.getCustomerType().equals("Prospect"))
                {
                    viewHolder.vm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent myintent = new Intent(getApplicationContext(), NewCustomerActivity.class);

                        //Create the bundle
                        Bundle bundle = new Bundle();

                        //Add your data to bundle
                        bundle.putString("checkin_time", checkin_time);
                        bundle.putString("customer_id", user.getCustomerCode());
                        bundle.putString("login_id", login_id);
                        bundle.putString("customer_type", user.getCustomerType());
                        bundle.putString("nav_type", "Reports");

                        //Add the bundle to the intent
                        myintent.putExtras(bundle);

                        //Fire that second activity
                        startActivity(myintent);

                    }
                });
                }

            }

            return convertView;
        }

        public class ViewHolder {

            TextView serial, customer_name, customer_type, location, city, mobile, email;
            public View vm;
        }

    }


    @Override
    protected void onResume() {

        super.onResume();
        getCustomerDetails();
    }

}
