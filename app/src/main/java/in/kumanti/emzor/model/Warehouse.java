package in.kumanti.emzor.model;


public class Warehouse {

    public int warehouse_id;
    public String sales_person_code;
    public String warehouse_code;
    public String warehouse_name;
    public String created_at;
    public String updated_at;


    public int getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(int warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getSales_person_code() {
        return sales_person_code;
    }

    public void setSales_person_code(String sales_person_code) {
        this.sales_person_code = sales_person_code;
    }


    public String getWarehouse_name() {
        return warehouse_name;
    }

    public void setWarehouse_name(String warehouse_name) {
        this.warehouse_name = warehouse_name;
    }


    public String getWarehouse_code() {
        return warehouse_code;
    }

    public void setWarehouse_code(String warehouse_code) {
        this.warehouse_code = warehouse_code;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return warehouse_name;
    }
}
