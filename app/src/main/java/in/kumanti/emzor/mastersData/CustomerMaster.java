package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class CustomerMaster {

    private String customer_id;
    private String customer_name;
    private String customer_code;
    private String customer_type;
    private String customer_image;
    private String contact_no;
    private String credit_limit;
    private String credit_days;
    private String bde_code;
    private String price_list;
    private String gps_latitude;
    private String gps_longitude;
    private String bill_address_line1;
    private String bill_address_line2;
    private String bill_address_line3;
    private String bill_city;
    private String bill_state;
    private String cus_acc_num;
    private String location_name;
    private String location_code;
    private String lga;
    private String bill_country;
    private String email;
    private String image_url;
    private String price_list_code;
    private String customerGroup;
    private String credit_term;
    private String payment_term;


    public CustomerMaster(String customer_id,
                          String customer_name,
                          String customer_code,
                          String customer_type,
                          String customer_image,
                          String contact_no,
                          String credit_limit,
                          String credit_days,
                          String bde_code,
                          String price_list,
                          String gps_latitude,
                          String gps_longitude,
                          String bill_address_line1,
                          String bill_address_line2,
                          String bill_address_line3,
                          String bill_city,
                          String bill_state,
                          String cus_acc_num,
                          String location_name,
                          String location_code,
                          String lga,
                          String bill_country,
                          String email,
                          String image_url,
                          String price_list_code,
                          String customerGroup,
                          String credit_term,
                          String payment_term

    ) {

        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_code = customer_code;
        this.customer_type = customer_type;
        this.customer_image = customer_image;
        this.contact_no = contact_no;
        this.credit_limit = credit_limit;
        this.credit_days = credit_days;
        this.bde_code = bde_code;
        this.price_list = price_list;
        this.gps_latitude = gps_latitude;
        this.gps_longitude = gps_longitude;
        this.bill_address_line1 = bill_address_line1;
        this.bill_address_line2 = bill_address_line2;
        this.bill_address_line3 = bill_address_line3;
        this.bill_city = bill_city;
        this.bill_state = bill_state;
        this.cus_acc_num = cus_acc_num;
        this.location_name = location_name;
        this.location_code = location_code;
        this.lga = lga;
        this.bill_country = bill_country;
        this.email = email;
        this.image_url = image_url;
        this.price_list_code = price_list_code;
        this.customerGroup = customerGroup;
        this.credit_term = credit_term;
        this.payment_term = payment_term;

    }

    public String getCustomer_id() {
        return customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public String getCustomer_image() {
        return customer_image;
    }

    public String getContact_no() {
        return contact_no;
    }

    public String getCredit_limit() {
        return credit_limit;
    }
    public String getCredit_days() {
        return credit_days;
    }

    public String getBde_code() {
        return bde_code;
    }

    public String getGps_latitude() {
        return gps_latitude;
    }

    public String getGps_longitude() {
        return gps_longitude;
    }

    public String getCredit_term() {
        return credit_term;
    }

    public String getPayment_term() {
        return payment_term;
    }

    public String getBill_address_line1() {
        return bill_address_line1;
    }

    public String getBill_address_line2() {
        return bill_address_line2;
    }

    public String getBill_address_line3() {
        return bill_address_line3;
    }

    public String getBill_city() {
        return bill_city;
    }

    public String getBill_state() {
        return bill_state;
    }

    public String getCus_acc_num() {
        return cus_acc_num;
    }

    public String getLocation_name() {
        return location_name;
    }

    public String getLocation_code() {
        return location_code;
    }

    public String getLga() {
        return lga;
    }

    public String getBill_country() {
        return bill_country;
    }

    public String getEmail() {
        return email;
    }

    public String getPrice_list() {
        return price_list;
    }

    public String getPrice_list_code() {
        return price_list_code;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }


}

