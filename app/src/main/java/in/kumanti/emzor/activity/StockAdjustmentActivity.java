package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import de.siegmar.fastcsv.writer.CsvWriter;
import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.StockAdjustmentAdapter;
import in.kumanti.emzor.eloquent.BDEListTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.eloquent.StockIssueProductsTable;
import in.kumanti.emzor.eloquent.StockIssueTable;
import in.kumanti.emzor.model.BDEList;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.StockIssue;
import in.kumanti.emzor.model.StockIssueBatch;
import in.kumanti.emzor.model.StockIssueProducts;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;

public class StockAdjustmentActivity extends MainActivity {

    public RecyclerView.Adapter stockIssueProductsAdapter;
    public RecyclerView.LayoutManager stockIssueProductsLayoutManager;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionBarTitle, marqueeText, checkInTime;
    String stockIssueIdString = null, stockIssueDateDB, stockIssueTime, stockIssueTimeCSV, invoiceId = null;
    String loginId = "", customerCode = "", checkInTimeString = "", marqueeTextString = "", bdeNameString = "", toBdeCode = "", productId = "-1";
    MyDBHandler dbHandler;
    CustomSearchableSpinner  stockProductsSpinner;

    ProductTable productTable;
    ArrayList<Product> stockProductsArrayList, newProductList;
    ArrayAdapter<Product> stockProductsSpinnerArrayAdapter;
    Button addStockProductsButton;
    ArrayList<StockIssueProducts> stockIssueProductsArrayList;
    RecyclerView stockIssueProductsRecyclerView;
    SharedPreferenceManager sharedPreferenceManager;

    String stockIssueNumber,stockIssueDate;
    int stockIssueId;
    int random_num = 0;
    Handler issueHandler;


    StockIssueTable stockIssueTable;
    StockIssueProductsTable stockIssueProductsTable;

    ImageView saveStockIssueButton;
    Bitmap selectedImage = null;
    int line_count;
    GeneralSettingsTable generalSettingsTable;
    GeneralSettings gs;
    Globals globals;
    long stopTime = 0;
    DecimalFormat formatter, formatter1;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng, nav_type, customer_type, tab_prefix;
    double price = 0.00;
    String emailType = "CustomerReturn";
    String companyName, compAdd1, compAdd2, compAdd3, compMblNo, fromBdeCode;
    //Variables declaration for print and sms
    Button smsButton, printButton, emailButton;
    String customerPhoneNumber = "8883681519";
    String smsMessageText = "  Sample Test Sms ";
    // android built in classes for bluetooth operations
    PrintBluetooth printBluetooth;
    Print print;
    String printerName = "MP80-17031810239";
    String printMsg;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    String customerReturnStatus = "";
    RecyclerViewItemListener listener, batchListener;
    ProgressDialog dialog;
    int filteredPosition = -1;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private Context contextForDialog = null;

    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";
    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_stock_adjustment);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        stockIssueDate();
        generateStockIssueNumber();

        actionBarTitle.setText("Stock Adjustment");

        marqueeText.setSelected(true);
        marqueeText.setText(marqueeTextString);

        contextForDialog = this;




        stockIssueProductsArrayList = new ArrayList<StockIssueProducts>();


        updateProductSpinner();
        stockProductsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                Log.d("Products", "Selected Product" + pos);
                Product p = (Product) adapterView.getItemAtPosition(pos);
                Log.d("Products", "Selected Product" + pos + p.product_name);

                productId = p.product_id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });


        addStockProductsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addStockReceiptDetail();
            }
        });


        stockIssueProductsRecyclerView.setHasFixedSize(true);
        stockIssueProductsLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        stockIssueProductsRecyclerView.setLayoutManager(stockIssueProductsLayoutManager);

        listener = (view, position) -> {
            updateProductSpinner();
        };

        batchListener = (view, position) -> {

            Intent intent = new Intent(getApplicationContext(), StockAdjustBatchActivity.class);
            intent.putExtra("product_id", stockIssueProductsArrayList.get(position).getProductCode());
            intent.putExtra("product_name", stockIssueProductsArrayList.get(position).getProductName());
            intent.putExtra("product_quantity", stockIssueProductsArrayList.get(position).getIssueQuantity());
            intent.putExtra("product_batch_details", stockIssueProductsArrayList.get(position).getStockIssueBatchArrayList());
            startActivityForResult(intent, 2);
        };

        stockIssueProductsAdapter = new StockAdjustmentAdapter(stockIssueProductsArrayList, listener, batchListener);
        stockIssueProductsRecyclerView.setAdapter(stockIssueProductsAdapter);

        saveStockIssueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (stockIssueProductsArrayList.size() == 0) {
                    Toast.makeText(getApplicationContext(), "Please add atleast one Product", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!validationsForStoringStocks()) {
                    //Toast.makeText(getApplicationContext(),"Please add atleast one Product",Toast.LENGTH_LONG).show();
                    return;
                }



                final Dialog alertbox = new Dialog(v.getRootView().getContext());

                LayoutInflater mInflater = (LayoutInflater)
                        getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                final View alertLayout = mInflater.inflate(R.layout.stock_adjust_save_confirmation, null);

                alertbox.setCancelable(false);
                alertbox.setContentView(alertLayout);

                final Button Yes = alertLayout.findViewById(R.id.stockReturnYesButton);
                final Button No = alertLayout.findViewById(R.id.stockReturnNoButton);

                Yes.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        String visitSequence = sharedPreferences.getString("VisitSequence", "");

//                        dialog = new ProgressDialog(contextForDialog);
//                        dialog.setMessage("Uploading Stock Transfer, Please Wait!!");
//                        dialog.setCancelable(false);
//                        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                        dialog.show();
//                        issueHandler = new Handler() {
//                            @Override
//                            public void handleMessage(Message msg) {
//                                dialog.dismiss();
//                                if (msg.what == 1) {//
//                                    Toast.makeText(getApplicationContext(), "Stock Issue has been Completed", Toast.LENGTH_SHORT).show();
//                                    // Log.d("Multi", "Start Stop setIsDownloaded After Downloaded--" +mdlSync.getIsDownloaded());
//                                    finish();
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "Unable to process your transaction.", Toast.LENGTH_LONG).show();
//
//                                }
//
//
//                            }
//                        };



                        insertingToStockReceipt();





                        Toast.makeText(getApplicationContext(), "Stock Receipt Created Successfully", Toast.LENGTH_LONG).show();


                        alertbox.dismiss();
                        finish();





                    }
                });

                No.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        alertbox.dismiss();
                    }

                });

                alertbox.show();


            }
        });


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }



    public boolean validationsForStoringStocks() {

        boolean validCheck = true;
        int totalProductReturnQuantity = 0;

        for (StockIssueProducts sip : stockIssueProductsArrayList) {
            int totalQuantity = 0;
            if (sip.getProductCode().equals("-1"))
                continue;
            if (sip.getIsBatchControlled() != null && sip.getIsBatchControlled().equals("Yes") && !TextUtils.isEmpty(sip.getIssueQuantity())) {

                for (StockIssueBatch sib : sip.getStockIssueBatchArrayList()) {
                    if (!TextUtils.isEmpty(sib.getIssueQuantity()))
                        totalQuantity += Integer.parseInt(sib.getIssueQuantity());
                }
                if (totalQuantity != Integer.parseInt(sip.getIssueQuantity())) {
                    sip.setErrorProduct("Please add Batch Return Quantity");
                    validCheck = false;
                }
            } else {
                sip.setErrorProduct(null);
            }
            if (!TextUtils.isEmpty(sip.getIssueQuantity()))
                totalProductReturnQuantity += Integer.parseInt(sip.getIssueQuantity());
//            if (!TextUtils.isEmpty(sip.getUnitPrice()))
//                totalProductReturnPrice += Integer.parseInt(sip.getUnitPrice());

        }

        if (!validCheck) {
            stockIssueProductsAdapter = new StockAdjustmentAdapter(stockIssueProductsArrayList, listener, batchListener);
            stockIssueProductsRecyclerView.setAdapter(stockIssueProductsAdapter);
            return false;
        }

        if (totalProductReturnQuantity == 0) {
            Toast.makeText(getApplicationContext(), "Please add atleast one Quantity ", Toast.LENGTH_LONG).show();
            return false;
        }

        return validCheck;


    }

//    private void insertingToStockIssueProductsTable() {
//
//        String visitSequence = sharedPreferences.getString("VisitSequence", "");
//
//        for (StockIssueProducts srwp : stockIssueProductsArrayList) {
//            if (srwp.getProductCode().equals("-1"))
//                continue;
//            if (!TextUtils.isEmpty(srwp.getIssueQuantity())) {
//
//                long id = dbHandler.createstockreciept
//                        (str_receipt_id,
//                                str_receip_date,
//                                "NGN",
//                                login_id,
//                                distributor_name,
//                                "Stock Receipt",
//                                "distributor",
//                                srwp.getProductCode(),
//                                srwp.getProductUom(),
//                                srwp.getIssueQuantity(),
//                                srwp.getUnitPrice(),
//                                srwp.getIssueValue(),
//                                "",
//                                "Completed",
//                                random_num,
//                                visitSequence,
//                                stockIssueNumber,
//                                "");
//                for (StockReceiptBatch srbd : srwp.getBatchDetailArrayList()) {
//                    dbHandler.insertingProductBatch(srwp.getProductId(), srbd.getBatchNumber(), srbd.getExpiryDate(), srbd.getBatchQuantity(), invoice_id, str_receipt_id, invoice_id, "Completed", "StockReceipt");
//                }
//
//
//            }
//
//
//        }
//
//
//    }

    /**
     * Update the Product Spinner, based upon the addition and deletion of the products in Opportunity Product Details RecyclerView List
     */
    private void updateProductSpinner() {
        //To Set the Values to the Product Spinner
        String sec_price_list =  dbHandler.get_sec_price_list();
        System.out.println("TTT::sec_price_list = " + sec_price_list);
        if(!sec_price_list.isEmpty())
        stockProductsArrayList = dbHandler.getStockReceiptProductDetails(sec_price_list);
        else
        stockProductsArrayList = dbHandler.getStockReceiptProductDetails1();
//        stockProductsArrayList = dbHandler.getStockReceiptProducts();
        System.out.println("TTT::stockProductsArrayList = " + stockProductsArrayList.size());
        //Remove the already selected product
        int k = 0;
        newProductList = new ArrayList<Product>();
        for (Product product : stockProductsArrayList) {
            boolean flag = true;
            for (StockIssueProducts sip : stockIssueProductsArrayList) {
                if (product.product_id.equals(sip.getProductCode()))
                    flag = false;
            }
            if (flag)
                newProductList.add(product);
            k++;
        }
        Product p = new Product();
        p.product_name = "All";
        p.product_id = "-1";
        newProductList.add(0, p);

        stockProductsSpinnerArrayAdapter = new ArrayAdapter<Product>(getApplicationContext(), android.R.layout.simple_spinner_item, newProductList);
        stockProductsSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        stockProductsSpinner.setAdapter(stockProductsSpinnerArrayAdapter);

    }

    private void addStockReceiptDetail() {
        if (stockProductsSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Please choose a product before adding to the Product List", Toast.LENGTH_SHORT).show();
            return;
        }

        Product selectedProduct = newProductList.get(stockProductsSpinner.getSelectedItemPosition());
        StockIssueProducts sip = new StockIssueProducts();
        sip.setStockIssueNumber(stockIssueNumber);
        sip.setStockIssueDate(stockIssueDateDB);
        sip.setProductName(selectedProduct.product_name);
        sip.setProductUom(selectedProduct.product_uom);
        sip.setProductCode(selectedProduct.product_id);
        sip.setStockQuantity(String.valueOf(dbHandler.getstockonhand(selectedProduct.product_id)));
        sip.setUnitPrice(selectedProduct.product_price);
        sip.setRemarks(selectedProduct.remarks);

        System.out.println("TTT::selectedProduct.product_price = " + selectedProduct.product_price);
        System.out.println("TTT::selectedProduct.remarks = " + selectedProduct.remarks);

        sip.setIsBatchControlled(selectedProduct.batch_controlled);
        if (selectedProduct.batch_controlled.equals("Yes")) {
            sip.setStockIssueBatchArrayList(dbHandler.getStockIssueBatchList(selectedProduct.product_id));
        }

        System.out.println("TTT::sip = " + sip);
        stockIssueProductsArrayList.add(sip);
        stockIssueProductsAdapter.notifyDataSetChanged();
        Log.d("Recycle", "Data Added: " + selectedProduct.product_name);
        updateProductSpinner();
    }

    private void insertingToStockReceipt() {
        int rec_random_num = dbHandler.get_receipt_random_num();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);
        String receipt_num;
        rec_random_num = rec_random_num + 1;

        if (rec_random_num <= 9) {
            receipt_num = tab_prefix + "SR" + rec_seq_date + "00" + rec_random_num;

        } else if (rec_random_num > 9 & random_num < 99) {
            receipt_num = tab_prefix + "SR" + rec_seq_date + "0" + rec_random_num;

        } else {
            receipt_num = tab_prefix + "SR" + rec_seq_date + rec_random_num;
        }

        String visitSequence = sharedPreferences.getString("VisitSequence", "");

        for (StockIssueProducts sip : stockIssueProductsArrayList) {
            if (sip.getProductCode().equals("-1"))
                continue;
            if (!TextUtils.isEmpty(sip.getIssueQuantity())) {
                long id = dbHandler.createstockreciept(
                        receipt_num,
                        stockIssueDateDB,
                        "NGN",
                        login_id,
                        fromBdeCode,
                        "Stock Adjustment",
                        "",
                        sip.getProductCode(),
                        sip.getProductUom(),
                        String.valueOf(Integer.parseInt(sip.getIssueQuantity())),
                        sip.getUnitPrice(),
                        sip.getIssueValue(),
                        sip.getRemarks(),
                        "",
                        "Completed",
                        rec_random_num,
                        visitSequence,
                        receipt_num,
                        toBdeCode);

                if (sip.getIsBatchControlled().equals("Yes")) {
                    for (StockIssueBatch sib : sip.getStockIssueBatchArrayList()) {
                        if (!TextUtils.isEmpty(sib.getIssueQuantity()))
                            dbHandler.insertingProductBatch(sip.getProductCode(), sib.getBatchNumber(), sib.getExpiryDate(), String.valueOf(Integer.parseInt(sib.getIssueQuantity()) ), "", receipt_num, invoiceId, "Completed", "Stock Adjustment");
                    }
                }
            }


        }


    }



    public void stockIssueDate() {
        String PATTERN = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        String PATTERN_DISPLAY = "dd MMM YY";
        SimpleDateFormat dateFormatDisplay = new SimpleDateFormat();
        dateFormatDisplay.applyPattern(PATTERN_DISPLAY);
        String dateDisplay = dateFormatDisplay.format(Calendar.getInstance().getTime());
        stockIssueDateDB = date;
        stockIssueDate = (dateDisplay);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        stockIssueTime = sdf.format(outDate);
        SimpleDateFormat sdf1 = new SimpleDateFormat("hh mm a");
        stockIssueTimeCSV = sdf1.format(outDate);
    }

    public void generateStockIssueNumber() {
        random_num = stockIssueTable.getCustomerReturnNumber();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        Log.d("Stock Return", "Random No---" + random_num);

        if (random_num <= 9) {
            stockIssueNumber= (Constants.TAB_PREFIX + "SI" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            stockIssueNumber=(Constants.TAB_PREFIX + "SI" + rec_seq_date + "0" + random_num);

        } else {
            stockIssueNumber=(Constants.TAB_PREFIX + "SI" + rec_seq_date + random_num);
        }

        stockIssueIdString = stockIssueNumber;

    }

    private void initializeViews() {

        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);


        stockProductsSpinner = findViewById(R.id.stockIssueProdSpinner);

        addStockProductsButton = findViewById(R.id.stockIssueAddProdButton);

        stockIssueProductsRecyclerView = findViewById(R.id.stockIssueProdRecyclerView);



        saveStockIssueButton = findViewById(R.id.stockIssueSaveImageButton);

        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("####.00");

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());

        bdeListTable = new BDEListTable(getApplicationContext());
        productTable = new ProductTable(getApplicationContext());
        stockIssueTable = new StockIssueTable(getApplicationContext());
        stockIssueProductsTable = new StockIssueProductsTable(getApplicationContext());

        tab_prefix = Constants.TAB_PREFIX;
        globals = ((Globals) getApplicationContext());

        printBluetooth = new PrintBluetooth(this);
        print = new Print(this);



    }

    public void populateHeaderDetails() {

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkInTimeString = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            customer_type = bundle.getString("customer_type");
            nav_type = bundle.getString("nav_type");

        }

        getCompanyInfo();
        getCurrentLoginBDEInfo();

    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);

            }
        }

    }

    public void getCurrentLoginBDEInfo() {
        //Populate SalesRep Details
        Log.d("Invoice", "Login---" + login_id);
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                //byte[] blob = salesRepDetails.getBlob(9);
                //Bitmap bmp= convertByteArrayToBitmap(blob);
                //sr_image.setImageBitmap(bmp);
                fromBdeCode = salesRepDetails.getString(1);
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }
    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("DataCheck", "Entered Child Fragment" + requestCode);
        //super.onActivityResult(requestCode, resultCode, data); //comment this unless you want to pass your result to the activity.
        if (requestCode == 2) {
            Log.d("DataCheck", "Entered in to Check");
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String product_id = (String) bundle.get("product_id");
                ArrayList<StockIssueBatch> product_batch_details = (ArrayList<StockIssueBatch>) bundle.get("product_batch_details");

//                for (StockIssueProducts ipi : stockIssueProductsArrayList) {
//                    if (product_id.equals(ipi.getProductCode())) {
//                        Log.d("DataCheck", "Value Assigned in product");
//                        ipi.setStockIssueBatchArrayList(product_batch_details);
//                        int totalQuantity = 0;
//                        boolean validCheck = true;
//                        for (StockIssueBatch crp : ipi.getStockIssueBatchArrayList()) {
//                            if (!TextUtils.isEmpty(crp.getIssueQuantity()))
//                                totalQuantity += Integer.parseInt(crp.getIssueQuantity());
//                        }
//                        if (totalQuantity != Integer.parseInt(ipi.getIssueQuantity())) {
//                            ipi.setErrorProduct("Please add Batch Issue Quantity");
//                            validCheck = false;
//                        }
//                        if (validCheck)
//                            ipi.setErrorProduct(null);
//                    }
//                }

                for (StockIssueProducts srwp : stockIssueProductsArrayList) {
                    Log.d("DataCheck", "Product Ids " + product_id + " " + srwp.getProductCode());
                    Log.d("DataCheck", "Quantity" + srwp.getIssueQuantity());
                    if (product_id.equals(srwp.getProductCode())) {
                        Log.d("DataCheck", "Value Assigned in product");
                        srwp.setStockIssueBatchArrayList(product_batch_details);
                        if (product_batch_details.size() != 0)
                            srwp.setErrorProduct(null);
                    }
                }
                stockIssueProductsAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

}
