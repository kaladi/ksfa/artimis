package in.kumanti.emzor.model;

public class StockReturnProducts {

    public int stockReturnProductId;
    public int stockReturnId;
    public String productCode;
    public String productName;
    public String batch;
    public String expiryDate;
    public String availQuantity;
    public String returnQuantity;
    public String returnPrice;
    public String returnValue;
    public String errorStockReturnQty;
    public boolean isSync;
    public String createdAt;
    public String updatedAt;
    public String productUOM;
    public String stockReturnNumber;
    private String tabCode;
    private String companyCode;
    private String srCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getProductUOM() {
        return productUOM;
    }

    public void setProductUOM(String productUOM) {
        this.productUOM = productUOM;
    }

    public String getStockReturnNumber() {
        return stockReturnNumber;
    }

    public void setStockReturnNumber(String stockReturnNumber) {
        this.stockReturnNumber = stockReturnNumber;
    }

    public int getStockReturnProductId() {
        return stockReturnProductId;
    }

    public void setStockReturnProductId(int stockReturnProductId) {
        this.stockReturnProductId = stockReturnProductId;
    }

    public int getStockReturnId() {
        return stockReturnId;
    }

    public void setStockReturnId(int stockReturnId) {
        this.stockReturnId = stockReturnId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getAvailQuantity() {
        return availQuantity;
    }

    public void setAvailQuantity(String availQuantity) {
        this.availQuantity = availQuantity;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public String getErrorStockReturnQty() {
        return errorStockReturnQty;
    }

    public void setErrorStockReturnQty(String errorStockReturnQty) {
        this.errorStockReturnQty = errorStockReturnQty;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
