package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.model.CrmMeeting;

public class CRMListAdapter extends RecyclerView.Adapter<CRMListAdapter.ViewHolder> {

    public ArrayList<Crm> mDataset;
    public Context context;
    private DecimalFormat valueFormatter, quantityFormatter;
    double unformattedValue = 0.0;

    public CRMListAdapter(ArrayList<Crm> myDataset) {
        mDataset = myDataset;
    }

    public void add(Crm item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmMeeting item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }


    @NotNull
    @Override
    public CRMListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_crm_history, parent, false);

        valueFormatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder viewHolder, final int position) {

        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getCrmDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.crmDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }

        viewHolder.crmNumber.setText(mDataset.get(position).getCrmNumber());
        viewHolder.customerName.setText(mDataset.get(position).getCustomerCode());
        viewHolder.count.setText(mDataset.get(position).getCrmLines());


        viewHolder.budget.setText(mDataset.get(position).getCrmBudget());
        viewHolder.value.setText(mDataset.get(position).getCrmValue());

        viewHolder.budget.setText(valueFormatter.format(Double.parseDouble(mDataset.get(position).getCrmBudget())));
        viewHolder.value.setText(valueFormatter.format((Double.parseDouble(mDataset.get(position).getCrmValue()))));


        viewHolder.crmStatus.setText(mDataset.get(position).getStatus());
        viewHolder.description.setText(mDataset.get(position).getDescription());
        viewHolder.reason.setText(mDataset.get(position).getReason());

        viewHolder.crmHistoryContainer.setOnClickListener(v -> {

            //Intent viewCrmIntent = new Intent(context, ViewCrmActivity.class);
           /* //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customerCode);
            bundle.putString("login_id", login_id);
            bundle.putString("crmNo", crmNumber.getText().toString());
            bundle.putString("crmStatus", crmNoStatus.getText().toString());
            bundle.putString("crmStatusDate", crmNoStatusDate.getText().toString());
            bundle.putString("randomNumber", String.valueOf(random_num));

            Log.d("Crm--", "crmStatus: " +crmNoStatus.getText().toString());
            Log.d("Crm--", "crmStatusDate: " +crmNoStatusDate.getText().toString());

            viewCrmIntent.putExtra("crmProdList", crmProductsArrayList);
            viewCrmIntent.putExtras(bundle);*/
            //context.startActivity(viewCrmIntent);

        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView crmDate, crmNumber, customerName, count, budget, value, crmStatus, description, reason;
        LinearLayout crmHistoryContainer;

        public ViewHolder(View v) {
            super(v);
            crmHistoryContainer = v.findViewById(R.id.crmHistoryContainer);
            crmDate = v.findViewById(R.id.crmHisDateTv);
            crmNumber = v.findViewById(R.id.crmHisNumberTv);
            customerName = v.findViewById(R.id.crmHisCustomerNameTv);
            count = v.findViewById(R.id.crmHisLinesTv);
            budget = v.findViewById(R.id.crmHisBudgetTv);
            value = v.findViewById(R.id.crmHisValueTv);
            crmStatus = v.findViewById(R.id.crmHisStatusTv);
            description = v.findViewById(R.id.crmHisDescriptionTv);
            reason = v.findViewById(R.id.crmHisReasonTv);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


}