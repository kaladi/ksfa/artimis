package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.BdeReceiveProducts;

public class StockReceiptBdeReceiveAdapter extends RecyclerView.Adapter<StockReceiptBdeReceiveAdapter.ViewHolder> {

    public ArrayList<BdeReceiveProducts> mDataset;
    public Context context;
    DecimalFormat decimalFormat;
    double priceDouble, totalPriceDouble;
    String formatPrice, formatTotalPrice, conversion;
    private RecyclerViewItemListener mListener;

    public StockReceiptBdeReceiveAdapter(ArrayList<BdeReceiveProducts> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(BdeReceiveProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(BdeReceiveProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockReceiptBdeReceiveAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_receipt_bde_receive, parent, false);
        context = parent.getContext();
        decimalFormat = new DecimalFormat("#,###.00");


        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.productUom.setText(mDataset.get(position).getProductUom());
        viewHolder.productQuantity.setText(mDataset.get(position).getIssueQuantity());
        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getStockIssueDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.transferredDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }
        if (!TextUtils.isEmpty(mDataset.get(position).getIssuePrice())) {
            priceDouble = Double.parseDouble(mDataset.get(position).getIssuePrice());
            formatPrice = decimalFormat.format(priceDouble);
            viewHolder.productPrice.setText(formatPrice);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getIssueValue())) {
            totalPriceDouble = Double.parseDouble(mDataset.get(position).getIssueValue());
            formatTotalPrice = decimalFormat.format(totalPriceDouble);
            viewHolder.productTotalPrice.setText(formatTotalPrice);
        }


        if (!TextUtils.isEmpty(mDataset.get(position).getErrorStockIssueProduct())) {
            viewHolder.productName.setError(mDataset.get(position).getErrorStockIssueProduct());
        } else {
            viewHolder.productName.setError(null);
        }

        viewHolder.productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size() && mDataset.get(position).getIsBatchControlled() != null && mDataset.get(position).getIsBatchControlled().equals("Yes")) {
                    Log.d("Popup", "Listener");
                    viewHolder.mListener.onClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView transferredDate, productUom, productQuantity, productPrice, productTotalPrice;
        public Button productName;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            transferredDate = v.findViewById(R.id.stockReceiptBDEDateTv);
            productName = v.findViewById(R.id.stockReceiptBDEProdNameTv);
            productUom = v.findViewById(R.id.stockReceiptBDEProdUomTv);
            productQuantity = v.findViewById(R.id.stockReceiptBDEProdQuantityTv);
            productPrice = v.findViewById(R.id.stockReceiptBDEProPriceTv);
            productTotalPrice = v.findViewById(R.id.stockReceiptBDEProdTotalPriceTv);

        }
    }


}