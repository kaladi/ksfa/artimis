package in.kumanti.emzor.model;


public class Notes {

    public int id;
    public String productCode;
    public String companyCode;
    public String customerCode;
    public String srCode;
    public String notesText;
    public String notesType;
    public String notesReferenceNumber;
    public String timeStamp;
    public String createdAt;
    public String updatedAt;
    public String visitSequenceNumber;
    public String tabCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getProduct_id() {
        return productCode;
    }

    public void setProduct_id(String product_id) {
        this.productCode = product_id;
    }


    public String getCompany_id() {
        return companyCode;
    }

    public void setCompany_id(String company_id) {
        this.companyCode = company_id;
    }


    public String getCustomer_id() {
        return customerCode;
    }

    public void setCustomer_id(String customer_id) {
        this.customerCode = customer_id;
    }


    public String getSales_rep_id() {
        return srCode;
    }

    public void setSales_rep_id(String sales_rep_id) {
        this.srCode = sales_rep_id;
    }


    public String getNotes_data() {
        return notesText;
    }

    public void setNotes_data(String notes_data) {
        this.notesText = notes_data;
    }


    public String getNotes_type() {
        return notesType;
    }

    public void setNotes_type(String notes_type) {
        this.notesType = notes_type;
    }


    public String getNotes_reference_no() {
        return notesReferenceNumber;
    }

    public void setNotes_reference_no(String notes_reference_no) {
        this.notesReferenceNumber = notes_reference_no;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getCreated_at() {
        return createdAt;
    }

    public void setCreated_at(String created_at) {
        this.createdAt = created_at;
    }

    public String getUpdated_at() {
        return updatedAt;
    }

    public void setUpdated_at(String updated_at) {
        this.updatedAt = updated_at;
    }

    public String getVisitSeqNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSeqNumber(String visitSeqNumber) {
        this.visitSequenceNumber = visitSeqNumber;
    }

}
