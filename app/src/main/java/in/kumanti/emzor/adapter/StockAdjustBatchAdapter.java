package in.kumanti.emzor.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockIssueBatch;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class StockAdjustBatchAdapter extends RecyclerView.Adapter<StockAdjustBatchAdapter.ViewHolder> {

    public ArrayList<StockIssueBatch> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;
    private int mYear, mMonth, mDay;

    public StockAdjustBatchAdapter(ArrayList<StockIssueBatch> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(StockIssueBatch item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockIssueBatch item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockAdjustBatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_adjust_batch, parent, false);


        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getExpiryDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.expiryDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }

        //viewHolder.expiryDate.setText(mDataset.get(position).getExpiryDate());
        viewHolder.batchNumber.setText(mDataset.get(position).getBatchNumber());
        System.out.println("MMM::mDataset.get(position).getBatchQuantity()  = " + mDataset.get(position).getBatchQuantity());
        if(mDataset.get(position).getBatchQuantity() == null )
        {
            viewHolder.batchQuantity.setText("0");
        }else
        {
            viewHolder.batchQuantity.setText(String.valueOf(Integer.parseInt(mDataset.get(position).getBatchQuantity())));
        }
        viewHolder.issueQuantity.setText(mDataset.get(position).getIssueQuantity());
        //To Set Error message
//        viewHolder.issueQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, mDataset.get(position).getBatchQuantity())});
        if (!TextUtils.isEmpty(mDataset.get(position).getErrorIssueQuantity())) {
            viewHolder.issueQuantity.setError(mDataset.get(position).getErrorIssueQuantity());
        } else {
            viewHolder.issueQuantity.setError(null);
        }

        viewHolder.expiryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInspectionDatePickerView(viewHolder.expiryDate, mDataset.get(position));
            }
        });

        viewHolder.expiryDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setExpiryDate(caption.getText().toString());
                }
            }
        });


        viewHolder.issueQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setIssueQuantity(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });

        viewHolder.batchNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setBatchNumber(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void getInspectionDatePickerView(Button b, StockIssueBatch srbd) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        try {
                            /*String PATTERN = "dd MMM YY";
                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d= new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            //txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            String date = dateFormat.format(d);
                            b.setText(date);
                            srbd.setExpiryDate(date);
                            b.setError(null); */

                           /* String date = (year + "-" + (monthOfYear +1 ) + "-" + dayOfMonth);
                            b.setText(date);
                            srbd.setExpiryDate(date);
                            b.setError(null); */

                            String PATTERN = "dd MMM YY";
                            String PATTERN1 = "yyyy-MM-dd";

                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            //txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            SimpleDateFormat dateFormat1 = new SimpleDateFormat();
                            Date d1 = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat1.applyPattern(PATTERN1);
                            dateFormat1.format(d1);

                            b.setText(dateFormat.format(d1));
                            srbd.setExpiryDate(dateFormat1.format(d1));
                            b.setError(null);
                        } catch (Exception e) {

                        }
                    }
                }, mDay, mMonth, mYear);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
        datePickerDialog.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView  batchQuantity;
        public Button expiryDate;
        public EditText issueQuantity,batchNumber;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;

            expiryDate = v.findViewById(R.id.stockIssueExpiryDateTv);
            batchNumber = v.findViewById(R.id.stockIssueBatchNameTv);
            batchQuantity = v.findViewById(R.id.stockIssueBatchStockQtyTv);
            issueQuantity = v.findViewById(R.id.stockIssueBatchIssueQtyTv);

//            issueQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
        }
    }


}