package in.kumanti.emzor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.PurchaseHistoryItems;


public class PurchaseHistoryAdapter extends ArrayAdapter<PurchaseHistoryItems> {

    String customer_type1 = "";
    private LayoutInflater mInflater;
    private ArrayList<PurchaseHistoryItems> users;
    private int mViewResourceId;

    public PurchaseHistoryAdapter(Context context, int textViewResourceId, ArrayList<PurchaseHistoryItems> users, String customer_type) {
        super(context, textViewResourceId, users);
        this.users = users;

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
        customer_type1 = customer_type;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        PurchaseHistoryItems user = users.get(position);

        if (user != null) {
            TextView date = convertView.findViewById(R.id.createOrderPurHisDateTv);
            TextView quantity = convertView.findViewById(R.id.createOrderPurHisQtyTv);
            TextView price = convertView.findViewById(R.id.createOrderPurHisPriceTv);
            TextView stock = convertView.findViewById(R.id.createOrderPurHisStockTv);
            if (date != null) {
                date.setText(user.getDate());
            }
            if (quantity != null) {
                quantity.setText((user.getQuantity()));
            }
            if (price != null) {
                if ("Doctor".equals(customer_type1)) {
                    price.setText("");

                } else {
                    price.setText((user.getPrice()));

                }
            }
            if (stock != null) {
                if ("Doctor".equals(customer_type1)) {
                    stock.setText("");

                } else {
                    stock.setText((user.getStock()));

                }
            }

        }

        return convertView;
    }
}