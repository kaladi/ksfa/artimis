package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CustomerReturnBatch;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class CustomerReturnBatchAdapter extends RecyclerView.Adapter<CustomerReturnBatchAdapter.ViewHolder> {

    public ArrayList<CustomerReturnBatch> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;
    private int mYear, mMonth, mDay;

    public CustomerReturnBatchAdapter(ArrayList<CustomerReturnBatch> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(CustomerReturnBatch item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CustomerReturnBatch item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public CustomerReturnBatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_customer_return_batch, parent, false);


        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
       /* try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getExpiryDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.expiryDate.setText(dateFormat.format(d));
        }
        catch (Exception e){

        }*/

        viewHolder.expiryDate.setText(mDataset.get(position).getExpiryDate());
        viewHolder.batchNumber.setText(mDataset.get(position).getBatchNumber());
        viewHolder.batchQuantity.setText(String.valueOf(Integer.parseInt(mDataset.get(position).getBatchQuantity()) * -1));
        viewHolder.returnQuantity.setText(mDataset.get(position).getReturnQuantity());
        //To Set Error message

        if (!TextUtils.isEmpty(mDataset.get(position).getErrorReturnQuantity())) {
            viewHolder.returnQuantity.setError(mDataset.get(position).getErrorReturnQuantity());
        } else {
            viewHolder.returnQuantity.setError(null);
        }


        viewHolder.returnQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setReturnQuantity(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView expiryDate, batchNumber, batchQuantity;
        public EditText returnQuantity;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;

            expiryDate = v.findViewById(R.id.cusReturnExpiryDateTv);
            batchNumber = v.findViewById(R.id.cusReturnBatchNameTv);
            batchQuantity = v.findViewById(R.id.cusReturnBatchAvlQtyTv);
            returnQuantity = v.findViewById(R.id.cusReturnBatchReturnQtyTv);

            returnQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
        }
    }


}