package in.kumanti.emzor.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.ImageViewActivity;
import in.kumanti.emzor.activity.PDFViewActivity;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.model.Artifacts;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ArtifactsListAdapter extends BaseAdapter {

    private final Context mContext;
    private ArrayList<Artifacts> artifactsArrayList;
    String  pdfFilePath = "", pdfFileName = "PDF File";


    // 1
    public ArtifactsListAdapter(Context context, ArrayList<Artifacts> artifactsArrayList) {
        this.mContext = context;
        this.artifactsArrayList = artifactsArrayList;
    }

    // 2
    @Override
    public int getCount() {
        return artifactsArrayList.size();
    }

    // 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // 4
    @Override
    public Object getItem(int position) {
        return null;
    }

    // 5
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Artifacts artifacts = artifactsArrayList.get(position);
        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.list_artifacts, null);
        }

        ImageView imageView = convertView.findViewById(R.id.fileTypePlaceHolder);
        TextView fileName = convertView.findViewById(R.id.fileNameTV);
        LinearLayout gridContent = convertView.findViewById(R.id.gridContent);

        if (artifacts.getFileType().equals("image"))
            imageView.setImageResource(R.drawable.img);
        else if (artifacts.getFileType().equals("pdf"))
            imageView.setImageResource(R.drawable.pdf);
        else
            imageView.setImageResource(R.drawable.doc);

        fileName.setText(artifacts.getFileName());

        gridContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (artifacts.getFileType().equals("pdf")) {

                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    File file = new File(Environment.getExternalStorageDirectory() , artifacts.getFilePath());

                    System.out.println(file);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    try {
                        mContext.startActivity(intent);
                    } catch (ActivityNotFoundException e) {

                    }

//                    open pdf in app

//                    Intent myintent = new Intent(mContext, PDFViewActivity.class);
////                    //Create the bundle
//                    Bundle bundle = new Bundle();
//                    bundle.putString("pdfFilePath", artifacts.getFilePath());
//                    bundle.putString("pdfFileName", artifacts.getFileName());
//                    //Add the bundle to the intent
//                    myintent.putExtras(bundle);
//                    //Fire that second activity
////                    mContext.startActivity(myintent);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        myintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    }
//                    mContext.startActivity(myintent);

                } else if (artifacts.getFileType().equals("image")) {
                    Intent myintent = new Intent(mContext, ImageViewActivity.class);

                    //Create the bundle
                    Bundle bundle = new Bundle();
                    bundle.putString("imageFilePath", artifacts.getFilePath());
                    bundle.putString("imageFileName", artifacts.getFileName());
                    //Add the bundle to the intent
                    myintent.putExtras(bundle);
                    //Fire that second activity
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        myintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    mContext.startActivity(myintent);
                } else if (artifacts.getFileType().equals("doc")||artifacts.getFileType().equals("docx")) {
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    File file = new File(Environment.getExternalStorageDirectory() , artifacts.getFilePath());
                    System.out.println(file);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "application/msword");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    try {
                        mContext.startActivity(intent);
                    } catch (ActivityNotFoundException e) {

                    }
                } else if (artifacts.getFileType().equals("txt")) {
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    File file = new File(Environment.getExternalStorageDirectory() , artifacts.getFilePath());
                    System.out.println(file);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "text/plain");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    try {
                        mContext.startActivity(intent);
                    } catch (ActivityNotFoundException e) {

                    }
                } else {
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    File file = new File(Environment.getExternalStorageDirectory(),
                            artifacts.getFilePath());
                    Uri path = Uri.fromFile(file);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setDataAndType(path, "*/*");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                        mContext.startActivity(intent);


                }
            }
        });
        return convertView;
    }


}
