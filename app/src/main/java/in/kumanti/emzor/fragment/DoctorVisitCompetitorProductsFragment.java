package in.kumanti.emzor.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.StockReceiptBatchActivity;
import in.kumanti.emzor.adapter.DoctorVisitCompProdAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.eloquent.CompetitorProductTable;
import in.kumanti.emzor.eloquent.DoctorPrescribedProductsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CompetitorProduct;
import in.kumanti.emzor.model.DoctorPrescribedProducts;
import in.kumanti.emzor.utils.CustomSearchableSpinner;


public class DoctorVisitCompetitorProductsFragment extends Fragment {

    public RecyclerView presCompProdRecyclerView;
    CustomSearchableSpinner compProductSpinner;
    ArrayAdapter<CompetitorProduct> productSpinnerArrayAdapter;
    ArrayList<CompetitorProduct> productArrayList;
    ArrayList<CompetitorProduct> newProductList;
    CompetitorProductTable productTable;
    ArrayList<DoctorPrescribedProducts> doctorVisitPrescriptionsProdArrayList = new ArrayList<DoctorPrescribedProducts>();
    Context mContext;
    MyDBHandler dbHandler;
    String login_id = "", product_id = null, customer_id = "", product_type = "CompetitorProduct";
    Button addRow;
    RecyclerViewItemListener listener, listener1;
    DoctorPrescribedProductsTable doctorPrescribedProductsTable;
    private RecyclerView.Adapter presCompProdAdapter;
    private RecyclerView.LayoutManager presCompProdLayoutManager;


    public DoctorVisitCompetitorProductsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctor_visit_competitor_products, container, false);
        mContext = rootView.getContext();
        dbHandler = new MyDBHandler(mContext, null, null, 1);
        doctorPrescribedProductsTable = new DoctorPrescribedProductsTable(mContext);
        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
            customer_id = getArguments().getString("customer_id");
        }

        productTable = new CompetitorProductTable(mContext);

        compProductSpinner = rootView.findViewById(R.id.doctorVisitCompProdSpinner);
        presCompProdRecyclerView = rootView.findViewById(R.id.docVisitCompProdRecyclerView);

        addRow = rootView.findViewById(R.id.compProdAddRowButton);


        //Product Spinner Initialization
        updateProductSpinner();
        compProductSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                compProductSpinner.isSpinnerDialogOpen = false;

                CompetitorProduct p = (CompetitorProduct) adapterView.getItemAtPosition(pos);
                product_id = p.getProductCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                compProductSpinner.isSpinnerDialogOpen = false;

            }
        });


        presCompProdRecyclerView.setHasFixedSize(true);
        presCompProdLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        presCompProdRecyclerView.setLayoutManager(presCompProdLayoutManager);

        listener = (view, position) -> {
            updateProductSpinner();
        };
        listener1 = (view, position) -> {

            Intent intent = new Intent(getContext(), StockReceiptBatchActivity.class);
            intent.putExtra("product_id", doctorVisitPrescriptionsProdArrayList.get(position).getProductCode());
            intent.putExtra("product_name", doctorVisitPrescriptionsProdArrayList.get(position).getProductName());
            getActivity().startActivityForResult(intent, 2);
        };
        doctorVisitPrescriptionsProdArrayList = doctorPrescribedProductsTable.getPharmacyInfoProducts(customer_id, product_type);
        presCompProdAdapter = new DoctorVisitCompProdAdapter(doctorVisitPrescriptionsProdArrayList, listener, listener1);
        presCompProdRecyclerView.setAdapter(presCompProdAdapter);

        //Generate new row or item in opportunity details Recycler view
        addRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPrescribedProducts();
            }
        });


        return rootView;
    }


    /**
     * Update the Product Spinner, based upon the addition and deletion of the products in Opportunity Product Details RecyclerView List
     */
    private void updateProductSpinner() {
        //To Set the Values to the Product Spinner
        productArrayList = productTable.getCompetitorProductsList();
        //Remove the already selected product
        int k = 0;
        newProductList = new ArrayList<CompetitorProduct>();
        for (CompetitorProduct product : productArrayList) {
            boolean flag = true;
            for (DoctorPrescribedProducts srd : doctorVisitPrescriptionsProdArrayList) {
                if (product.getProductCode().equals(srd.getProductCode()))
                    flag = false;
            }
            if (flag)
                newProductList.add(product);
            k++;
        }

        CompetitorProduct p = new CompetitorProduct();
        p.setProductCode("-1");
        p.setProductName("Select");
        newProductList.add(0, p);


        compProductSpinner.setTitle("Select a Product");
        compProductSpinner.setPositiveButton("OK");

        productSpinnerArrayAdapter = new ArrayAdapter<CompetitorProduct>(mContext, android.R.layout.simple_spinner_item, newProductList);
        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        compProductSpinner.setAdapter(productSpinnerArrayAdapter);
    }


    /**
     * Adding of Products Details in Opportunity Recycler View List
     */
    private void addPrescribedProducts() {
        if (compProductSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(mContext, "Please choose a product before adding to the Product List", Toast.LENGTH_SHORT).show();
            return;
        }

        CompetitorProduct selectedProduct = newProductList.get(compProductSpinner.getSelectedItemPosition());
        DoctorPrescribedProducts srd = new DoctorPrescribedProducts();
        srd.setProductName(selectedProduct.getProductName());
        srd.setProductCode(selectedProduct.getProductCode());
        srd.setDoctorCode(customer_id);
        srd.setProductType(product_type);
        doctorVisitPrescriptionsProdArrayList.add(srd);
        presCompProdAdapter.notifyDataSetChanged();
        Log.d("Recycle", "Data Added: " + selectedProduct.getProductName());
        updateProductSpinner();
    }

    public boolean savePresciptionCompetitorProducts() {
        if (doctorVisitPrescriptionsProdArrayList.size() == 0) {
            //Toast.makeText(mContext,"Please add Prescription Products",Toast.LENGTH_SHORT).show();
            return false;
        }
        boolean validateCheck = false;
        for (DoctorPrescribedProducts dpp : doctorVisitPrescriptionsProdArrayList) {
            if (TextUtils.isEmpty(dpp.getPrescriptionsCount())) {
               // dpp.setErrorPrescriptionsCount("Please enter prescription count");
            } else{
                validateCheck = true;
                dpp.setErrorPrescriptionsCount(null);
            }

        }
        if (!validateCheck) {
            presCompProdAdapter.notifyDataSetChanged();
            return false;
        }
        doctorPrescribedProductsTable.deletePrescriptionProduct(customer_id, product_type);
        for (DoctorPrescribedProducts dpp : doctorVisitPrescriptionsProdArrayList) {
            doctorPrescribedProductsTable.create(dpp);
        }
        return true;
    }


}






