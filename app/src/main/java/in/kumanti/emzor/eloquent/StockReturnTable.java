package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.StockReturn;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_RETURN;

public class StockReturnTable extends SQLiteOpenHelper {

    private static final String COLUMN_STOCK_RETURN_ID = "stockReturnId";
    private static final String COLUMN_STOCK_RETURN_NUMBER = "stockReturnNum";
    private static final String COLUMN_STOCK_RETURN_DATE = "stockReturnDate";
    private static final String COLUMN_STOCK_RETURN_TYPE = "stockReturnType";
    private static final String COLUMN_STOCK_RETURN_SIGNATURE = "signature";
    private final static String COLUMN_STOCK_RETURN_IS_SYNC = "isSync";
    private final static String COLUMN_STOCK_RETURN_EMAIL_SENT = "emailSent";
    private final static String COLUMN_STOCK_RETURN_SMS_SENT = "smsSent";
    private final static String COLUMN_STOCK_RETURN_PRINT_DONE = "printDone";
    private final static String COLUMN_STOCK_RETURN_RANDOM_NUM = "randomNumber";
    private static final String COLUMN_STOCK_RETURN_CREATED_AT = "createdAt";
    private static final String COLUMN_STOCK_RETURN_UPDATED_AT = "updatedAt";
    private final static String COLUMN_STOCK_RETURN_SEQUENCE_NO = "visitSeqNo";
    private static final String COLUMN_STOCK_RETURN_SIGNEE_NAME = "signeeName";

    public Context context;
    String selectQuery;
    Cursor cursor;
    private SQLiteDatabase sqlWrite;

    public StockReturnTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCK_RETURN);

        String stockReturnTable = "CREATE TABLE IF NOT EXISTS " + TABLE_STOCK_RETURN + "(" +
                COLUMN_STOCK_RETURN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_STOCK_RETURN_NUMBER + " TEXT, " +
                COLUMN_STOCK_RETURN_DATE + " TEXT, " +

                COLUMN_STOCK_RETURN_TYPE + " TEXT, " +
                COLUMN_STOCK_RETURN_SIGNATURE + " TEXT, " +

                COLUMN_STOCK_RETURN_EMAIL_SENT + " TEXT, " +
                COLUMN_STOCK_RETURN_SMS_SENT + " TEXT, " +
                COLUMN_STOCK_RETURN_PRINT_DONE + " TEXT, " +
                COLUMN_STOCK_RETURN_RANDOM_NUM + " TEXT, " +
                COLUMN_STOCK_RETURN_IS_SYNC + " TEXT, " +

                COLUMN_STOCK_RETURN_SEQUENCE_NO + " TEXT, " +

                COLUMN_STOCK_RETURN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_STOCK_RETURN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_STOCK_RETURN_SIGNEE_NAME + " TEXT " +
                ");";
        sqlWrite.execSQL(stockReturnTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(StockReturn stockReturn) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_STOCK_RETURN_NUMBER, stockReturn.getStockReturnNumber());
        values.put(COLUMN_STOCK_RETURN_DATE, stockReturn.getStockReturnDate());

        values.put(COLUMN_STOCK_RETURN_TYPE, stockReturn.getStockReturnType());
        values.put(COLUMN_STOCK_RETURN_SIGNATURE, stockReturn.getSignature());

        values.put(COLUMN_STOCK_RETURN_EMAIL_SENT, stockReturn.getEmailSent());
        values.put(COLUMN_STOCK_RETURN_SMS_SENT, stockReturn.getSmsSent());
        values.put(COLUMN_STOCK_RETURN_PRINT_DONE, stockReturn.getPrintDone());
        values.put(COLUMN_STOCK_RETURN_RANDOM_NUM, stockReturn.getRandomNum());

        values.put(COLUMN_STOCK_RETURN_SEQUENCE_NO, stockReturn.getVisitSeqNumber());

        values.put(COLUMN_STOCK_RETURN_SIGNEE_NAME, stockReturn.getSigneeName());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_STOCK_RETURN, null, values);
        db.close();
        return id;
    }

    public int getStockReturnNumber() {
        int stockReturnId = 0;
        String selectQuery = "select randomNumber from xxmsales_stock_return where stockReturnDate=date('now') order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            stockReturnId = cursor.getInt(0);
            cursor.close();
        }

        return stockReturnId;
    }

    public ArrayList<StockReturn> getStockReturnDetails() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_STOCK_RETURN + " where stockReturnDate >= date('now','-5 Days') ";

        ArrayList<StockReturn> stockReturnArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {
            StockReturn stockReturn = new StockReturn();
            stockReturn.setTabCode(Constants.TAB_CODE);
            stockReturn.setCompanyCode(Constants.COMPANY_CODE);
            stockReturn.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            stockReturn.setStockReturnId(id);
            stockReturn.setStockReturnNumber(cursor.getString(1));
            stockReturn.setStockReturnDate(cursor.getString(2));
            stockReturn.setStockReturnType(cursor.getString(3));
            stockReturn.setVisitSeqNumber(cursor.getString(10));
            stockReturn.setCreatedAt(cursor.getString(11));
            stockReturn.setUpdatedAt(cursor.getString(12));

            stockReturnArrayList.add(stockReturn);
        }
        cursor.close();
        sqlWrite.close();
        return stockReturnArrayList;
    }

}
