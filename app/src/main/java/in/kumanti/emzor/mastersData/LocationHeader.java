package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class LocationHeader {

    private String location_id;
    private String location_name;

//    public LocationHeader(
//
//            String location_id,
//            String location_name
//
//
//    ) {
//
//        this.location_id = location_id;
//        this.location_name = location_name;
//
//
//    }

    public void setLocation_id(String  location_id) {
        this.location_id = location_id;
    }
    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }
    public String getLocation_name() {
        return location_name;
    }

    @Override
    public String toString() {
        return this.location_name;
    }
}

