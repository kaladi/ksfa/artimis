package in.kumanti.emzor.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.ArtifactsTable;

public class PDFViewActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    TextView actionbarTitle;
    String checkin_time = "", login_id = "";
    String customer_id = "", pdfFilePath = "", pdfFileName = "PDF File";
    SearchableSpinner fileTypeSpinner;
    ArtifactsTable artifactsTable;
    GridView artifactsGridView;
    PDFView pdfView;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_pdfview);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();


        actionbarTitle.setText(pdfFileName);
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        System.out.println("pdfFilePath = " + pdfFilePath);
        File file1 = new File(Environment.getExternalStorageDirectory() , pdfFilePath);
        pdfView.fromFile(file1)
                .defaultPage(1)
                .load();

//        String url=getIntent().getStringExtra(file1.toString());
////        Log.i("URI-URL",String.valueOf(Uri.parse(url)) );
//        pdfView.fromUri(Uri.fromFile(file1))
//                .enableSwipe(true)
//                .enableDoubletap(true)
//                .defaultPage(1)
//                .onLoad(new OnLoadCompleteListener() {
//                    @Override
//                    public void loadComplete(int nbPages) {
//
//                    }
//                })
//                .onPageChange(new OnPageChangeListener() {
//                    @Override
//                    public void onPageChanged(int page, int pageCount) {
//
//                    }
//                })
//                .enableAnnotationRendering(false)
//                .password(null)
//                .load();
        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }


    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);
        artifactsGridView = findViewById(R.id.artifactsGridView);

        fileTypeSpinner = findViewById(R.id.fileTypeSpinner);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        pdfView = findViewById(R.id.pdfView);

        artifactsTable = new ArtifactsTable(getApplicationContext());

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            pdfFilePath = bundle.getString("pdfFilePath");
            pdfFileName = bundle.getString("pdfFileName");
            //Log.d("LoginId",login_id);
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = (TextView) findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


}
