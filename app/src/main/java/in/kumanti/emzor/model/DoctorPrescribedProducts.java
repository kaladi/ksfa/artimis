package in.kumanti.emzor.model;


public class DoctorPrescribedProducts {

    public int id;
    public String doctorCode;
    public String productCode;
    public String productName;
    public String productType;
    public String prescriptionsCount;
    public String createdAt;
    public String updatedAt;
    public String errorProductName;
    public String errorPrescriptionsCount;
    public String tabCode;
    public String visitSeqNo;
    public String companyCode;
    public String srCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getErrorPrescriptionsCount() {
        return errorPrescriptionsCount;
    }

    public void setErrorPrescriptionsCount(String errorPrescriptionsCount) {
        this.errorPrescriptionsCount = errorPrescriptionsCount;
    }


    public String getPrescriptionsCount() {
        return prescriptionsCount;
    }

    public void setPrescriptionsCount(String prescriptionsCount) {
        this.prescriptionsCount = prescriptionsCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getErrorProductName() {
        return errorProductName;
    }

    public void setErrorProductName(String errorProductName) {
        this.errorProductName = errorProductName;
    }

    public String getErrorRemarks() {
        return errorPrescriptionsCount;
    }

    public void setErrorRemarks(String errorRemarks) {
        this.errorPrescriptionsCount = errorRemarks;
    }


    public String getVisitSeqNo() {
        return visitSeqNo;
    }

    public void setVisitSeqNo(String visitSeqNo) {
        this.visitSeqNo = visitSeqNo;
    }


}
