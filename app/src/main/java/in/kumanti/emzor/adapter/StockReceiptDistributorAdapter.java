package in.kumanti.emzor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.utils.DecimalDigitsInputFilter;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_ZERO;

public class StockReceiptDistributorAdapter extends RecyclerView.Adapter<StockReceiptDistributorAdapter.ViewHolder> {

    public ArrayList<StockReceipt> mDataset;
    public Context context;
    public Double totalPrice;
    public String quantity;
    DecimalFormat decimalFormat;
    Double priceDouble;
    String formatPrice;
    private RecyclerViewItemListener mListener, mListener1;


    public StockReceiptDistributorAdapter(ArrayList<StockReceipt> myDataset, RecyclerViewItemListener listener, RecyclerViewItemListener listener1) {
        mDataset = myDataset;
        mListener = listener;
        mListener1 = listener1;
    }

    public void add(StockReceipt item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockReceipt item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockReceiptDistributorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_receipt_distributor, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener, mListener1, new CustomEtListener(), new CustomPriceChangeListener());
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        decimalFormat = new DecimalFormat("#,###.00");
        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.productUom.setText(mDataset.get(position).getProductUom());
        // Update the position when focus changes
        viewHolder.customEtListener.updatePosition(position, viewHolder);
        viewHolder.priceChangeListener.updatePosition(position, viewHolder);
        viewHolder.productQuantity.setText(mDataset.get(position).getProductQuantity());
        System.out.println("TTT::mDataset.get(position).getProductPrice() = " + mDataset.get(position).getProductPrice());
        // viewHolder.productPrice.setText(mDataset.get(position).getProductPrice());
        // viewHolder.productTotalPrice.setText(mDataset.get(position).getProductTotalPrice());

       /* if (!TextUtils.isEmpty(mDataset.get(position).getProductPrice())) {
            priceDouble = Double.parseDouble(mDataset.get(position).getProductPrice());
            //formatPrice = decimalFormat.format(priceDouble);
            String value = String.valueOf(priceDouble);//formatPrice;

            if (value.equals("0.0"))
                value = null;

            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }

            System.out.println("TTT::value = " + value);
            viewHolder.productPrice.setText(value);
        }*/

        if(mDataset.get(position).getProductPrice() == null )
        {
            viewHolder.productPrice.setText("");
        }else
        {
            viewHolder.productPrice.setText(String.valueOf(mDataset.get(position).getProductPrice()));
        }

        viewHolder.productPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                  /*  mDataset.get(position).setProductPrice(caption.getText().toString());
                    caption.setError(null);*/
                }
            }
        });


        if (!TextUtils.isEmpty(mDataset.get(position).getError_productName())) {
            viewHolder.productName.setError(mDataset.get(position).getError_productName());
        } else {
            viewHolder.productName.setError(null);
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getError_productQuantity())) {
            viewHolder.productQuantity.setError(mDataset.get(position).getError_productQuantity());
        } else {
            viewHolder.productQuantity.setError(null);
        }
        if (!TextUtils.isEmpty(mDataset.get(position).getError_productPrice())) {
            viewHolder.productPrice.setError(mDataset.get(position).getError_productPrice());
        } else {
            viewHolder.productPrice.setError(null);
        }
        //If Not equals null means get the enter the batch details.....
        viewHolder.productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size()) {
                    if (mDataset.get(position).getProductQuantity() == null || TextUtils.isEmpty(mDataset.get(position).getProductQuantity())) {
                        Toast.makeText(context, "Please enter the quantity", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Log.d("DataCheck", "CurrentPosition onCLick " + position);
                    if (mDataset.get(position).getBatchControlled() != null && mDataset.get(position).getBatchControlled().equals("Yes"))
                        viewHolder.mListener1.onClick(v, position);
                }
            }
        });

        viewHolder.productQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                final EditText caption = (EditText) v;

            }
        });


        viewHolder.removeRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Product Info?");
                        //alertDialog.setMessage("your message ");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                notifyDataSetChanged();
                                viewHolder.mListener.onClick(v, position);
                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }


            }
        });

        if (position == mDataset.size() - 1)
            viewHolder.productQuantity.post(new Runnable() {
                @Override
                public void run() {
                    if (viewHolder.productQuantity.requestFocus()) {
                        ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        InputMethodManager inputMethodManager = (InputMethodManager) viewHolder.productQuantity.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(viewHolder.productQuantity, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
            });

        if (position == mDataset.size() - 1)
            viewHolder.productPrice.post(new Runnable() {
                @Override
                public void run() {
                    if (viewHolder.productPrice.requestFocus()) {
                        ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        InputMethodManager inputMethodManager = (InputMethodManager) viewHolder.productPrice.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(viewHolder.productPrice, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
            });


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextCallBackListener {

        public TextView productName, productUom, productTotalPrice;
        public ImageButton removeRow;
        public EditText productQuantity, productPrice;
        CustomEtListener customEtListener;
        CustomPriceChangeListener priceChangeListener;
        DecimalFormat decimalFormat;
        private RecyclerViewItemListener mListener, mListener1;

        public ViewHolder(View v, RecyclerViewItemListener listener, RecyclerViewItemListener listener1, CustomEtListener myCustomEditTextListener, CustomPriceChangeListener myPriceChangeListener) {
            super(v);
            mListener = listener;
            mListener1 = listener1;
            customEtListener = myCustomEditTextListener;
            priceChangeListener = myPriceChangeListener;
            productName = v.findViewById(R.id.stockReceiptDisProdNameTv);
            productUom = v.findViewById(R.id.stockReceiptDisProdUomTv);
            productQuantity = v.findViewById(R.id.stockReceiptDisProdQuantityEt);
            productPrice = v.findViewById(R.id.stockReceiptDisProPriceEt);
            productTotalPrice = v.findViewById(R.id.stockReceiptDisProdTotalPriceTv);
            removeRow = v.findViewById(R.id.stockReceiptDisRemoveButton);
            productQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
            productPrice.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_ZERO, MAX_NUMBER), new DecimalDigitsInputFilter()});
            productQuantity.addTextChangedListener(myCustomEditTextListener);
            productPrice.addTextChangedListener(priceChangeListener);
            decimalFormat = new DecimalFormat("#,###.00");
        }

        @Override
        public void updateText(String val, String val2) {
            //productQuantity.setText(val);
            if (!TextUtils.isEmpty(val2)) {
                Log.d("DataCheck", "Vlaue " + val2);
                formatPrice = decimalFormat.format(Double.parseDouble(val2));
                String value = formatPrice;
                if (value != null && !value.equals("")) {
                    if (value.startsWith(".")) {
                        value = "0" + value;
                    }
                }
                productTotalPrice.setText(value);
                if (!TextUtils.isEmpty(productQuantity.getText().toString())) {
                    productQuantity.setError(null);
                }
            } else {
                productTotalPrice.setText(val2);
            }

        }
    }

    /**
     * Custom class which implements Text Watcher
     */
    private class CustomEtListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // Change the value of array according to the position
            mDataset.get(this.position).setProductQuantity(charSequence.toString());
            //textCallBackListener.updateText(charSequence.toString(),charSequence.toString());
            // Doing the calculation

            System.out.println("charSequence.toString() = " + charSequence.toString());
            if (!mDataset.get(this.position).getProductQuantity().equals("") && !mDataset.get(this.position).getProductPrice().equals("")) {
                if (Double.parseDouble(mDataset.get(this.position).getProductPrice()) != 0d)
                    mDataset.get(this.position).setError_productPrice(null);
                mDataset.get(this.position).setError_productQuantity(null);
                Double total = Double.parseDouble(mDataset.get(this.position).getProductQuantity()) * Double.parseDouble(mDataset.get(this.position).getProductPrice());
                //formatPrice = decimalFormat.format(total);
                mDataset.get(this.position).setProductTotalPrice(String.valueOf(total));
                // triggering the callback function to update the total
                textCallBackListener.updateText(charSequence.toString(), String.valueOf(total));

            } else {
                mDataset.get(this.position).setProductTotalPrice("");
                textCallBackListener.updateText(charSequence.toString(), "");
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {


        }

    }

    /**
     * Custom class which implements Text Watcher
     */
    private class CustomPriceChangeListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String value = charSequence.toString();
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + charSequence.toString();
                }
            }
            // Change the value of array according to the position
            mDataset.get(this.position).setProductPrice(charSequence.toString());
            System.out.println("TTT::charSequence.toString() = " + charSequence.toString());
            textCallBackListener.updateText(charSequence.toString(),charSequence.toString());
            // Doing the calculation
//            if (!mDataset.get(this.position).getProductQuantity().equals("") && !mDataset.get(this.position).getProductPrice().equals("")) {
            if (!mDataset.get(this.position).getProductQuantity().equals("") && !charSequence.toString().equals("")) {
//                if (Double.parseDouble(mDataset.get(this.position).getProductPrice()) != 0d)
                if (Double.parseDouble(charSequence.toString()) != 0d)
                    mDataset.get(this.position).setError_productPrice(null);
                mDataset.get(this.position).setError_productQuantity(null);
                if(value == null){
                    value ="0";
                }
                System.out.println("TTT:pvalue = " + charSequence.toString());
//                Double total = Double.parseDouble(mDataset.get(this.position).getProductQuantity()) * Double.parseDouble(mDataset.get(this.position).getProductPrice());
                Double total = Double.parseDouble(mDataset.get(this.position).getProductQuantity()) * Double.parseDouble(charSequence.toString());
                //formatPrice = decimalFormat.format(total);
                mDataset.get(this.position).setProductTotalPrice(String.valueOf(total));
                // triggering the callback function to update the total
                textCallBackListener.updateText(charSequence.toString(), String.valueOf(total));
            } else {
                mDataset.get(this.position).setProductTotalPrice("");
                textCallBackListener.updateText(charSequence.toString(), "");
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {


        }

    }
}