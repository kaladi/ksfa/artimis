package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CrmPurchaseHisProd;


public class CrmPurchaseHistoryAdapter extends RecyclerView.Adapter<CrmPurchaseHistoryAdapter.ViewHolder> {

    public ArrayList<CrmPurchaseHisProd> mDataset;
    public Context context;
    private DecimalFormat valueFormatter, quantityFormatter;


    public CrmPurchaseHistoryAdapter(ArrayList<CrmPurchaseHisProd> myDataset) {
        mDataset = myDataset;
    }

    public void add(CrmPurchaseHisProd item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmPurchaseHisProd item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public CrmPurchaseHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_crm_purchase_history, parent, false);

        valueFormatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder viewHolder, final int position) {
        viewHolder.crmPurHisDate.setText(mDataset.get(position).getDate());
        viewHolder.crmPurHisQty.setText(valueFormatter.format(Double.parseDouble(mDataset.get(position).getQuantity())));
        viewHolder.crmPurHisPrice.setText(valueFormatter.format((Double.parseDouble(mDataset.get(position).getPrice()))));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView crmPurHisDate, crmPurHisQty, crmPurHisPrice;

        public ViewHolder(View v) {
            super(v);
            crmPurHisDate = v.findViewById(R.id.tvCreateCrmPurHisDate);
            crmPurHisQty = v.findViewById(R.id.tvCreateCrmPurHisQty);
            crmPurHisPrice = v.findViewById(R.id.tvCreateCrmPurHisPrice);
        }
    }
}