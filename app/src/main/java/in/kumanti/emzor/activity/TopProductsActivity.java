package in.kumanti.emzor.activity;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.TopProductsAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.TopProductsItems;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;

public class TopProductsActivity extends MainActivity {

    TextView actionBarTitle, marqueeText;
    ImageView actionbarBackButton, deviceInfo;

    ImageView customerImage;
    ArrayList<TopProductsItems> topProductList;
    TopProductsItems topProductList1;
    String login_id, customer_id = null, customer_type = null, trip_num = null, checkin_time, checkin_date, sr_name1, save_checkin_date;
    TextView customerName, accountNo, city, checkInTime, datetime;
    Globals globals;
    Chronometer duration;
    DecimalFormat formatter, formatter1;
    SimpleDateFormat sdfd;
    long date;
    MyDBHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_topproducts);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText(R.string.top_products_actionbar_title);
        marqueeText.setSelected(true);

        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");

        marqueeText.setText(marquee_txt);
        setDateFormat();

        getCustomerDetails();

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

        actionbarBackButton.setOnClickListener(v -> finish());


        topProductList = new ArrayList<>();
        Cursor data = null;
        if ("Secondary".equals(customer_type)) {
            try {
                Log.d("TopProductsCheck", "cusid " + customer_id);
                data = dbHandler.getInvoicedTopProductsOfYear(customer_id, "Completed");
            } catch (Exception e) {
                Log.d("TopProductsCheck", "" + e.getMessage());
            }

        } else if ("Doctor".equals(customer_type)) {
            data = dbHandler.getInvoicedTopProductsOfYear(customer_id, "FOC");

        } else {
            data = dbHandler.getorderTopProductsthisyear(customer_id);

        }

        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                String quantity_val = data.getString(1);
                String price_val = data.getString(2);
                String value_val = data.getString(3);

                double price_val1 = Double.parseDouble(price_val);
                double value_val1 = Double.parseDouble(value_val);
                int quantity_val1 = Integer.parseInt(quantity_val);

                String formatted_value = formatter.format(value_val1);
                String formatted_price = formatter.format(price_val1);
                String formatted_quantity = formatter1.format(quantity_val1);

                topProductList1 = new TopProductsItems
                        (data.getString(0),
                                formatted_quantity,
                                formatted_price,
                                formatted_value,
                                data.getString(4),
                                data.getString(5));
                topProductList.add(i, topProductList1);
                i++;
            }

            TopProductsAdapter adapter = new TopProductsAdapter(getApplicationContext(), R.layout.top_products_customer, topProductList, login_id, customer_id, customer_type);
            ListView top_product_list = findViewById(R.id.top_product_list);
            top_product_list.setAdapter(adapter);
        }


        customerImage.setOnLongClickListener(v -> {
            showcustomerLocation();
            return true;
        });

    }

    private void setDateFormat() {
        date = System.currentTimeMillis();
        sdfd = new SimpleDateFormat("ddMMMyyyy");
        SimpleDateFormat sdft = new SimpleDateFormat("hh:mm:ss");
        checkin_date = sdfd.format(date);
        checkin_time = sdft.format(date);
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        save_checkin_date = timeStampFormat.format(myDate);

    }

    private void getCustomerDetails() {

        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


            }
        }
    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        customer_id = bundle.getString("customer_id");
        login_id = bundle.getString("login_id");
        checkin_time = bundle.getString("checkin_time");

        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        checkInTime.setText(checkin_time);

    }

    private void initializeViews() {

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);


        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);


        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        globals = ((Globals) getApplicationContext());

        customer_type = globals.getCustomer_type();

        trip_num = globals.getTrip_num();
        sr_name1 = globals.getSr_name();

        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("#,###");

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

    }

}

