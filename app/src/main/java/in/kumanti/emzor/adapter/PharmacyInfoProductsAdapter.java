package in.kumanti.emzor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.model.PharmacyInfoProducts;


public class PharmacyInfoProductsAdapter extends RecyclerView.Adapter<PharmacyInfoProductsAdapter.ViewHolder> {

    public ArrayList<PharmacyInfoProducts> mDataset;
    public Context context;
    public String quantity;
    DecimalFormat decimalFormat;
    private RecyclerViewItemListener mListener;


    public PharmacyInfoProductsAdapter(ArrayList<PharmacyInfoProducts> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(PharmacyInfoProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(PharmacyInfoProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName;
        public EditText prescriptionsCount;
        public ImageButton removeRow;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            productName = v.findViewById(R.id.pharmacyProdNameTv);
            prescriptionsCount = v.findViewById(R.id.pharmacyProdPrescriptionsEt);
            removeRow = v.findViewById(R.id.pharmacyProdRemoveButton);
        }


    }


    @Override
    public PharmacyInfoProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pharmacy_info_products, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {


        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.prescriptionsCount.setText(mDataset.get(position).getPrescriptionsCount());


        //To Set Error message
        if (!TextUtils.isEmpty(mDataset.get(position).getErrorProductName())) {
            viewHolder.productName.setError(mDataset.get(position).getErrorProductName());
        } else {
            viewHolder.productName.setError(null);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getErrorPrescriptionsCount())) {
            viewHolder.prescriptionsCount.setError(mDataset.get(position).getErrorPrescriptionsCount());
        } else {
            viewHolder.prescriptionsCount.setError(null);
        }


        //To Set the values to the Availabilty Checkbox
        viewHolder.prescriptionsCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setPrescriptionsCount(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });


        viewHolder.removeRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                    View focusedView = getActivity(context).getCurrentFocus();
                    focusedView.clearFocus();
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Prescribed Product Info?");
                        //alertDialog.setMessage("your message ");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                notifyDataSetChanged();
                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }
            }
        });


    }

    private Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
    @Override
    public int getItemCount() {
        return mDataset.size();
    }



}