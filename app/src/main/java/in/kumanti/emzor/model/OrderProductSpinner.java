package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class OrderProductSpinner {

    private String product;
    private String prod_type;
    private String product_id;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product1) {
        product = product1;
    }

    public String getProd_type() {
        return prod_type;
    }

    public void setProd_type(String prod_type1) {
        prod_type = prod_type1;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id1) {
        product_id = product_id1;
    }

    @Override
    public String toString() {
        return product;
    }


}

