package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CrmQuotationProducts;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_QUOTATION_PRODUCTS;

public class CrmQuotationProductsTable extends SQLiteOpenHelper {

    private static final String COLUMN_QUOTATION_PRODUCT_ID = "quotationProductId";
    private static final String COLUMN_QUOTATION_ID = "quotationId";
    private static final String COLUMN_QUOTATION_PRODUCT_GROUP_CODE = "productGroupCode";
    private static final String COLUMN_QUOTATION_PRODUCT_CODE = "productCode";
    private static final String COLUMN_QUOTATION_PRODUCT_NAME = "productName";
    private static final String COLUMN_QUOTATION_PRODUCT_UOM = "productUom";
    private static final String COLUMN_QUOTATION_PRODUCT_QUANTITY = "productQuantity";
    private static final String COLUMN_QUOTATION_PRODUCT_UNIT_PRICE = "productUnitPrice";
    private static final String COLUMN_QUOTATION_PRODUCT_VALUE = "productValue";
    private static final String COLUMN_QUOTATION_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_QUOTATION_PRODUCT_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmQuotationProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_QUOTATION_PRODUCTS);

        String opportunityTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_QUOTATION_PRODUCTS + "(" +
                COLUMN_QUOTATION_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_QUOTATION_ID + " INTEGER, " +
                COLUMN_QUOTATION_PRODUCT_GROUP_CODE + " TEXT, " +
                COLUMN_QUOTATION_PRODUCT_CODE + " REAL, " +
                COLUMN_QUOTATION_PRODUCT_NAME + " TEXT, " +
                COLUMN_QUOTATION_PRODUCT_UOM + " TEXT, " +
                COLUMN_QUOTATION_PRODUCT_QUANTITY + " TEXT, " +
                COLUMN_QUOTATION_PRODUCT_UNIT_PRICE + " TEXT, " +
                COLUMN_QUOTATION_PRODUCT_VALUE + " TEXT, " +
                COLUMN_QUOTATION_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_QUOTATION_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(opportunityTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmQuotationProducts quotationProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_QUOTATION_ID, quotationProducts.getQuotationId());
        values.put(COLUMN_QUOTATION_PRODUCT_GROUP_CODE, quotationProducts.getProductGroupCode());
        values.put(COLUMN_QUOTATION_PRODUCT_CODE, quotationProducts.getProductCode());
        values.put(COLUMN_QUOTATION_PRODUCT_NAME, quotationProducts.getProductName());
        values.put(COLUMN_QUOTATION_PRODUCT_UOM, quotationProducts.getProductUom());
        values.put(COLUMN_QUOTATION_PRODUCT_QUANTITY, quotationProducts.getProductQuantity());
        values.put(COLUMN_QUOTATION_PRODUCT_UNIT_PRICE, quotationProducts.getProductUnitPrice());
        values.put(COLUMN_QUOTATION_PRODUCT_VALUE, quotationProducts.getProductValue());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_QUOTATION_PRODUCTS, null, values);
        db.close();
        return id;
    }


}
