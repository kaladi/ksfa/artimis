package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.IntentService;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import in.kumanti.emzor.BuildConfig;
import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.reports.CollectionReportsActivity;
import in.kumanti.emzor.activity.reports.CompetitorInfoReportsActivity;
import in.kumanti.emzor.activity.reports.CustomerReportsActivity;
import in.kumanti.emzor.activity.reports.CustomerReturnProductsReportsActivity;
import in.kumanti.emzor.activity.reports.ExpiredProductsReportsActivity;
import in.kumanti.emzor.activity.reports.FeedbackReportActivity;
import in.kumanti.emzor.activity.reports.InvoiceReportsActivity;
import in.kumanti.emzor.activity.reports.MyMessagesActivity;
import in.kumanti.emzor.activity.reports.OrderReportsActivity;
import in.kumanti.emzor.activity.reports.SurveyReportsActivity;
import in.kumanti.emzor.activity.reports.WaybillReportsActivity;
import in.kumanti.emzor.adapter.ExpandableListAdapter;
import in.kumanti.emzor.crm.CrmActivity;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.ExpenseTable;
import in.kumanti.emzor.eloquent.LedgerTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.model.CustomerDetails;
import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.Ledger;
import in.kumanti.emzor.model.ListTemplate;
import in.kumanti.emzor.model.TripDetails;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;

import static in.kumanti.emzor.utils.Constants.REQUEST_CODE_CUSTOMER_RETURN;


public class HomeActivity extends MainActivity {

    public TextView actionBarTitle;
    Spinner Customer_Name;
    Button com;
    ImageView customer_pic, sr_image, info, back;
    String customer_id = null;
    String customer_type = null;
    String trip_num = null;
    TextView toolbar_title, city, cust_order_value, cust_order_count, cust_invoice_value, cust_invoice_count, checkintime, sr_name, outstanding_val;
    TextView datetime;
    String checkin_time, checkin_date, sr_name1, save_checkin_date;
    String sr_name_details, sr_rm_name, sr_region, sr_warehouse;
    Globals globals;
    Chronometer chronometer;
    long stopTime = 0;
    ArrayList<ListTemplate> listTemplate;
    ListTemplate listTemplate1;
    ListTemplateAdapter adapter;
    SharedPreferenceManager sharedPreferenceManager;
    String nav_type;
    String format_order_value, format_invoice_value;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng,company_code1;
    DecimalFormat formatter;
    SimpleDateFormat sdfd;
    long date;
    ImageView deviceInfo;
    TextView actionbarTitle;
    CardView notesCardView, doctorvisit_btn, check_out_btn, order_btn, invoice_btn, collection_btn, ledger_btn, crmCardView, survey_btn, artifactsButton, feedback_btn, info_btn, top_products_btn;
    String lat, lng;
    MyDBHandler dbHandler;
    String login_id, email_id = "";
    TextView profile_name, profile_email;
    ExpandableListView expandableListView;
    View navHeader;
    ExpandableListAdapter expandableListAdapter;
    List<String> headerList = new ArrayList<>();
    HashMap<String, List<String>> childList = new HashMap<>();
    String user_role;
    ArrayList<Ledger> ledgerArrayList;
    ArrayList<Ledger> ledgerArrayList1;
    double invoiceTotal = 0;
    DecimalFormat decimalFormat;
    private PopupMenu popupMenu;
    private TextView marqueeText, open_invoice;


    CrmTable crmTable;
    ArrayList<Crm> crmArrayList;
    TextView  crmOppCount, crmOppValue;
    public DecimalFormat valueFormatter, quantityFormatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_home);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);

        initializeViews();
        getlocation();
        setFooterDateTime();
        populateHeaderDetails();
        getSetupDetails();
        companyType();
        getCrmOppInfo();
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        formatter = new DecimalFormat("#,###.00");
        date = System.currentTimeMillis();
        sdfd = new SimpleDateFormat("dd MMM yy");
        SimpleDateFormat sdft = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdft1 = new SimpleDateFormat("kk:mm");

        checkin_date = sdfd.format(date);
        checkin_time = sdft.format(date);
        String db_checkin_time = sdft1.format(date);
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        save_checkin_date = timeStampFormat.format(myDate);

        calculateOutstandingValue();
        // double outstanding = dbHandler.get_outstanding_value(customer_id);
        //double outstanding = dbHandler.getBalanceAmount(customer_id);


        back.setVisibility(View.GONE);

        if ("Home".equals(nav_type)) {
            Log.d("***Test***","home" +nav_type);
            chronometer.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
            chronometer.start();
        } else {
            Log.d("***Test***","Not Home" +nav_type);
            chronometer.setBase(SystemClock.elapsedRealtime() + stopTime);
            chronometer.start();
        }

        if ("Primary".equals(customer_type)) {

            invoice_btn.setVisibility(View.GONE);
            order_btn.setVisibility(View.VISIBLE);
            open_invoice.setText("0");

        }

        if ("Secondary".equals(customer_type)) {
            invoice_btn.setVisibility(View.VISIBLE);
            order_btn.setVisibility(View.GONE);

            calculateOpenInvoicesCount();

        }

        prepareListData();

        boolean flag = true;
        if ("User".equals(userrole)) {
            flag = false;
        }

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList, flag);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                String header_val = headerList.get(groupPosition);
                if ("Customer List".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), CustomerReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Journey Plan".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), NewJourneyPlanActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("View Stock".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), ViewStockActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Stock Receipt".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), StockReceiptActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Stock Return".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), StockReturnActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    startActivity(intent);

                } else if ("Customer Return".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), CustomerReturnActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("customer_id", customer_id);
                    intent.putExtra("sr_name_details", sr_name_details);
                    intent.putExtra("checkin_time", checkin_time);

                    startActivityForResult(intent,REQUEST_CODE_CUSTOMER_RETURN);

                } else if ("Waybill".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), WayBillActivity.class);
                    intent.putExtra("login_id", login_id);
                    intent.putExtra("customer_id", customer_id);
                    intent.putExtra("checkin_time", checkin_time);

                    startActivity(intent);

                } else if ("Device Info".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("My Messages".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), MyMessagesActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Logout".equals(header_val)) {
                    Intent intent = new Intent(getApplicationContext(), StartUpActivity.class);
                    globals.setLogin_email("");
                    globals.setLogin_id("");
                    startActivity(intent);

                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                String child = childList.get(
                        headerList.get(groupPosition)).get(
                        childPosition);

                if ("Orders".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), OrderReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Invoices".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), InvoiceReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Collections".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CollectionReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Waybill".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), WaybillReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }
//                else if ("Opportunities".equals(child)) {
//
//                } else if ("Enquiries".equals(child)) {
//
//                } else if ("Quotations".equals(child)) {
//
//                }
                else if ("Survey".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), SurveyReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Feedback".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), FeedbackReportActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Competitor Info".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CompetitorInfoReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Expired Products".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), ExpiredProductsReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                } else if ("Customer Return Products".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CustomerReturnProductsReportsActivity.class);
                    intent.putExtra("login_id", login_id);
                    startActivity(intent);

                }
//                else if ("Learning Stats".equals(child)) {
//
//                }
                else if ("Stock Receipt".equals(child)) {

                } else if ("Stock Ledger".equals(child)) {

                } else if ("Tab Setup".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), TabSetupActivity.class);
                    // intent.putExtra("login_id", login_id);
                    startActivity(intent);
                } else if ("User Setup".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), UserSetupActivity.class);
                    //intent.putExtra("login_id", login_id);
                    startActivity(intent);
                } else if ("Company Setup".equals(child)) {
                    Intent intent = new Intent(getApplicationContext(), CompanySetupActivity.class);
                    //intent.putExtra("login_id", login_id);
                    startActivity(intent);
                } else if ("App Setup".equals(child)) {

                } else if ("Change Password".equals(child)) {
                    //Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                    // intent.putExtra("login_id", login_id);
                    //startActivity(intent);
                }
                return false;
            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        int order_count = dbHandler.get_customer_order_count(customer_id);
        double order_value = dbHandler.get_customer_order_value(customer_id);

        int invoice_count = dbHandler.get_customer_invoice_count(customer_id);
        double invoice_value = dbHandler.get_customer_invoice_value(customer_id);


        if (order_value == 0) {
            format_order_value = String.valueOf(order_value);

        } else {
            format_order_value = formatter.format(order_value);
        }

        if (invoice_value == 0) {
            format_invoice_value = String.valueOf(invoice_value);
        } else {
            format_invoice_value = formatter.format(invoice_value);
        }

        checkintime.setText(checkin_time);

        /*cust_order_count.setText(Integer.toString(order_count));
        cust_order_value.setText(Integer.toString(order_value));
        cust_invoice_count.setText(Integer.toString(invoice_count));
        cust_invoice_value.setText(Integer.toString(invoice_value));*/

        cust_order_count.setText(Integer.toString(order_count));
        cust_order_value.setText(format_order_value + " (₦)");
        cust_invoice_count.setText(Integer.toString(invoice_count));
        cust_invoice_value.setText(format_invoice_value + " (₦)");

        // Insert CheckIN Details for that Customer

        if ("SR Landing".equals(nav_type)) {
            int random_num = dbHandler.get_checkin_id();

            SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("dd MM yy");
            Date myDate1 = new Date();
            String rec_seq_date = timeStampFormat.format(myDate1);

            random_num = random_num + 1;

            Log.d("VisitSequence Number", "Random No---" + random_num);
            String visitSeq = "";
            if (random_num <= 9) {
                visitSeq = Constants.TAB_PREFIX + "VS" + rec_seq_date + "00" + random_num;

            } else if (random_num > 9 & random_num < 99) {
                visitSeq = Constants.TAB_PREFIX + "VS" + rec_seq_date + "0" + random_num;

            } else {
                visitSeq = Constants.TAB_PREFIX + "VS" + rec_seq_date + random_num;
            }
            editor = sharedPreferences.edit();
            editor.putString("VisitSequence", visitSeq);
            editor.commit();

            String visitSequence = sharedPreferences.getString("VisitSequence", "");
            long id2 = dbHandler.insert_checkin_details
                    (
                            trip_num,
                            customer_id,
                            save_checkin_date,
                            db_checkin_time,
                            lat,
                            lng,
                            null,
                            null,
                            null,
                            null,
                            null,
                            login_id,
                            customer_type,
                            visitSequence
                    );

        }


        check_out_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog alertbox = new Dialog(v.getRootView().getContext());

                LayoutInflater mInflater = (LayoutInflater)
                        getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                final View alertLayout = mInflater.inflate(R.layout.activity_exit_popup, null);

                alertbox.setCancelable(false);
                //alertbox.setView(alertLayout);
                alertbox.setContentView(alertLayout);

                final Button Yes = alertLayout.findViewById(R.id.Yes);
                final Button No = alertLayout.findViewById(R.id.No);

                Yes.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                        sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                        chronometer.stop();

                        long date = System.currentTimeMillis();
                        SimpleDateFormat sdfd = new SimpleDateFormat("dd MMM yy");
                        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");

                        SimpleDateFormat sdft = new SimpleDateFormat("hh:mm:ss");
                        SimpleDateFormat sdft1 = new SimpleDateFormat("kk:mm");


                        int check_id = dbHandler.get_checkin_id();
                        String cout_date = timeStampFormat.format(date);
                        String cout_time = sdft.format(date);
                        String cout_time1 = sdft1.format(date);

                        if (alertbox != null && alertbox.isShowing()) {
                            alertbox.dismiss();
                        }

                        dbHandler.update_checkin_details(cout_date, cout_time1, lat, lng, Integer.toString(check_id), timeWhenStopped);
                        Intent myintent = new Intent(getApplicationContext(), StartStopActivity.class);

                        //Create the bundle
                        Bundle bundle = new Bundle();

                        //Add your data to bundle
                        bundle.putString("checkin_time", checkin_time);
                        bundle.putString("login_id", login_id);
                        bundle.putString("trip_num", trip_num);
                        bundle.putString("Home", "Home");
                        bundle.putString("user_role", user_role);


                        //Add the bundle to the intent
                        myintent.putExtras(bundle);

                        //Fire that second activity
                        startActivity(myintent);

                        //Toast.makeText(getApplicationContext(),"Check Out", Toast.LENGTH_LONG).show();
//                        postFtp();
                    }

                });

                No.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        alertbox.dismiss();
                    }

                });

                alertbox.show();

            }
        });

        order_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int sales_order_id = dbHandler.check_draft_order(customer_id);
                String sales_order_number = dbHandler.check_draft_order_number(customer_id);


                if (sales_order_id == 0) {
                    if ("Primary".equals(customer_type) || "Both".equals(customer_type)) {
                        popupMenu = new PopupMenu(HomeActivity.this, v);
                        popupMenu.setOnDismissListener(new OnDismissListener());
                        popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
                        popupMenu.inflate(R.menu.popup_menu_home_order);
                        popupMenu.show();

                    } else {
                            /*android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                            alertbox.setMessage("You can't create order for Secondary Customers");
                            alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {

                                    dialog.dismiss();

                                }

                            });

                            alertbox.show(); */
                    }
                } else {

                    Intent myintent = new Intent(getApplicationContext(), OrderProductActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putInt("order_id", sales_order_id);
                    bundle.putString("login_id", login_id);
                    bundle.putString("order_num", sales_order_number);

                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivity(myintent);

                }

            }
        });

        invoice_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ledgerArrayList1 = ledgerTable.getLedgerArrayList(customer_id, customer_type);
                int credit_limit = dbHandler.get_customer_creditLimit1(customer_id);
                System.out.println("credit_limit = " + credit_limit);
                double receiptTotal = 0;
                double outstandingTotal = 0;

                ArrayList<String> invoiceNumberList = new ArrayList<>();
                for (Ledger l : ledgerArrayList1) {
                    if (!invoiceNumberList.contains(l.getInvoiceNumber()))
                        invoiceNumberList.add(l.getInvoiceNumber());
                }

                for (Ledger l : ledgerArrayList1) {
                    invoiceTotal += TextUtils.isEmpty(l.getInvoiceAmount()) ? 0 : Double.parseDouble(l.getInvoiceAmount());
                    receiptTotal += TextUtils.isEmpty(l.getReceiptAmount()) ? 0 : Double.parseDouble(l.getReceiptAmount());
                    if (l.getTransactionType().equals("Return"))
                        outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                }
                for (String invoiceNumber : invoiceNumberList) {
                    String invoiceLastBalance = "";
                    for (Ledger l : ledgerArrayList1) {
                        if (invoiceNumber.equals(l.getInvoiceNumber()) && !l.getTransactionType().equals("Return"))
                            invoiceLastBalance = l.getOutstandingAmount();
                    }
                    outstandingTotal += TextUtils.isEmpty(invoiceLastBalance) ? 0 : Double.parseDouble(invoiceLastBalance);
                    // Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount()+" "+ outstandingTotal);
                }

                int formatOutstandingTotalAmount = outstandingTotal != 0 ? (int) outstandingTotal : 0;
//                String formatOutstandingTotalAmount = outstandingTotal != 0 ? decimalFormat.format(Math.round(outstandingTotal)) : "0.00";

                System.out.println("TTT::formatOutstandingTotalAmount = " + formatOutstandingTotalAmount);


                if (credit_limit != 0) {
                    if (formatOutstandingTotalAmount <= credit_limit) {
                        int sales_invoice_id = dbHandler.check_draft_invoice(customer_id);
                        String sales_invoice_number = dbHandler.check_draft_invoice_number(customer_id);

                        if (sales_invoice_id == 0) {
                            if ("Secondary".equals(customer_type) || "Both".equals(customer_type)) {

                                popupMenu = new PopupMenu(HomeActivity.this, v);
                                popupMenu.setOnDismissListener(new OnDismissListener());
                                popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
                                popupMenu.inflate(R.menu.popup_menu_home_invoice);
                                popupMenu.show();


                            } else {
                   /* android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    alertbox.setMessage("You can't create invoice for Primary Customers");
                    alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {

                            dialog.dismiss();

                        }

                    });

                    alertbox.show(); */
                            }

                        } else {

                            Intent myintent = new Intent(getApplicationContext(), InvoiceProductActivity.class);
                            long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                            //chronometer.stop();
                            //Create the bundle
                            Bundle bundle = new Bundle();

                            //Add your data to bundle
                            bundle.putString("checkin_time", checkin_time);
                            bundle.putString("customer_id", customer_id);
                            bundle.putString("invoice_id", String.valueOf(sales_invoice_id));
                            bundle.putString("login_id", login_id);
                            bundle.putString("invoice_num", sales_invoice_number);
                            bundle.putString("customer_type", customer_type);
                            bundle.putString("invoice_type", "SecondarySalesOrder");

                            //Add the bundle to the intent
                            myintent.putExtras(bundle);

                            //Fire that second activity
                            startActivity(myintent);

                        }


                    } else {
                        Toast.makeText(getApplicationContext(), "Your Credit limit is Exceed", Toast.LENGTH_LONG).show();
                    }
                }else{
                    int sales_invoice_id = dbHandler.check_draft_invoice(customer_id);
                    String sales_invoice_number = dbHandler.check_draft_invoice_number(customer_id);

                    if (sales_invoice_id == 0) {
                        if ("Secondary".equals(customer_type) || "Both".equals(customer_type)) {

                            popupMenu = new PopupMenu(HomeActivity.this, v);
                            popupMenu.setOnDismissListener(new OnDismissListener());
                            popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
                            popupMenu.inflate(R.menu.popup_menu_home_invoice);
                            popupMenu.show();


                        } else {
                   /* android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    alertbox.setMessage("You can't create invoice for Primary Customers");
                    alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {

                            dialog.dismiss();

                        }

                    });

                    alertbox.show(); */
                        }

                    } else {

                        Intent myintent = new Intent(getApplicationContext(), InvoiceProductActivity.class);
                        long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                        SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                        //chronometer.stop();
                        //Create the bundle
                        Bundle bundle = new Bundle();

                        //Add your data to bundle
                        bundle.putString("checkin_time", checkin_time);
                        bundle.putString("customer_id", customer_id);
                        bundle.putString("invoice_id", String.valueOf(sales_invoice_id));
                        bundle.putString("login_id", login_id);
                        bundle.putString("invoice_num", sales_invoice_number);
                        bundle.putString("customer_type", customer_type);
                        bundle.putString("invoice_type", "SecondarySalesOrder");

                        //Add the bundle to the intent
                        myintent.putExtras(bundle);

                        //Fire that second activity
                        startActivity(myintent);

                    }


                }
                }

        });

        collection_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Primary".equals(customer_type) || "Secondary".equals(customer_type)) {
                    Intent myintent = new Intent(getApplicationContext(), CollectionActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    //chronometer.stop();
                    //Create the bundle
                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putString("login_id", login_id);
                    bundle.putString("customer_type", customer_type);

                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivityForResult(myintent,101);
                } else {

                }


            }
        });

        ledger_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("Doctor".equals(customer_type)) {

                } else {
                    Intent myintent = new Intent(getApplicationContext(), LedgerActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                   // chronometer.stop();
                    //Create the bundle
                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putString("login_id", login_id);
                    bundle.putString("customer_type", customer_type);


                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivity(myintent);
                }

            }

        });

        notesCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(getApplicationContext(), NotesActivity.class);
                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                //chronometer.stop();
                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customer_id);
                bundle.putString("login_id", login_id);
                bundle.putString("notesType", "CUSTOMER");
                bundle.putString("notesReferenceNo", customer_id);

                //Add the bundle to the intent
                myintent.putExtras(bundle);
                //Fire that second activity
                startActivity(myintent);

            }
        });

        crmCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Constants.COMPANY_CODE.equals("DemoCRM") &&
//                if(customer_type.equals("Prospect") || customer_type.equals("Primary") ) {
//                    GeneralSettings gs = generalSettingsTable.getSettingByKey("enable_crm");
//                    if (gs != null && gs.getValue().equals("Yes")) {
//                        Intent startIntent = new Intent(getApplicationContext(), CrmActivity.class);
//                        long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
//                        sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
//                        chronometer.stop();
//                        //Create the bundle
//                        Bundle bundle = new Bundle();
//                        bundle.putString("checkin_time", checkin_time);
//                        bundle.putString("customer_id", customer_id);
//                        bundle.putString("login_id", login_id);
//                        bundle.putString("customerType", customer_type);
//
//                        Log.d("Crm--", "Home Customer Code: " + customer_id);
//
//                        //Add the bundle to the intent
//                        startIntent.putExtras(bundle);
//
//                        //Fire that second activity
//                        startActivity(startIntent);
//
//                    }else{
//
//                    }
//                }else{
                    if(customer_type.equals("Prospect") || customer_type.equals("Primary")) {

                        GeneralSettings gs = generalSettingsTable.getSettingByKey("enable_crm");
                        if (gs != null && gs.getValue().equals("Yes")) {

                            Intent startIntent = new Intent(getApplicationContext(), CrmActivity.class);
                            long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                            sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                            chronometer.stop();
                            //Create the bundle
                            Bundle bundle = new Bundle();
                            bundle.putString("checkin_time", checkin_time);
                            bundle.putString("customer_id", customer_id);
                            bundle.putString("login_id", login_id);
                            bundle.putString("customerType", customer_type);

                            Log.d("Crm--", "Home Customer Code: " + customer_id);

                            //Add the bundle to the intent
                            startIntent.putExtras(bundle);

                            //Fire that second activity
                            startActivity(startIntent);
                        }
                        else{

                        }
                    }
                }
//            }
        });

        survey_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(getApplicationContext(), SurveyActivity.class);
                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                //chronometer.stop();
                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customer_id);
                bundle.putString("login_id", login_id);

                //Add the bundle to the intent
                myintent.putExtras(bundle);

                //Fire that second activity
                startActivity(myintent);
            }
        });

        artifactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(getApplicationContext(), ArtifactsActivity.class);
                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                //chronometer.stop();
                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customer_id);
                bundle.putString("login_id", login_id);

                //Add the bundle to the intent
                myintent.putExtras(bundle);

                //Fire that second activity
                startActivity(myintent);
            }
        });

        feedback_btn.setOnClickListener(v -> {
            Intent myintent = new Intent(getApplicationContext(), FeedbackActivity.class);
            long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            //chronometer.stop();
            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        });

        info_btn.setOnClickListener(v -> {

            Intent myintent = new Intent(getApplicationContext(), CustomerInfoActivity.class);
            long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            //chronometer.stop();
            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("login_id", login_id);
            bundle.putString("customer_id", customer_id);
            bundle.putString("checkin_time", checkin_time);


            bundle.putString("customer_type", customer_type);


            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);

        });

        top_products_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myintent = new Intent(getApplicationContext(), TopProductsActivity.class);
                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                //chronometer.stop();
                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customer_id);
                bundle.putString("login_id", login_id);
                bundle.putString("customer_type", customer_type);

                //Add the bundle to the intent
                myintent.putExtras(bundle);

                //Fire that second activity
                startActivity(myintent);
            }
        });

        doctorvisit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Doctor".equals(customer_type)) {
                    Intent myintent = new Intent(getApplicationContext(), DoctorVisitActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    //chronometer.stop();
                    //Create the bundle
                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putString("login_id", login_id);
                    bundle.putString("customer_type", customer_type);

                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivity(myintent);
                }

            }
        });


    }

    void getSetupDetails(){
        Cursor setupDetails = dbHandler.getSetupDetails();
        int setupDetailsCount = setupDetails.getCount();
        if (setupDetailsCount == 0) {

        } else {
            int i = 0;
            while (setupDetails.moveToNext()) {

                company_code1 = Constants.COMPANY_CODE = setupDetails.getString(2);
                Constants.COMPANY_NAME = setupDetails.getString(6);
            }
        }

    }

    private void companyType(){

        if (company_code1.equals("ALS001")){
            doctorvisit_btn.setVisibility(View.VISIBLE);
        }
        else {
            doctorvisit_btn.setVisibility(View.GONE);
        }

    }

    private void getCrmOppInfo() {

        crmArrayList = crmTable.getCRMNumber(customer_id);

        if(crmArrayList != null) {
            crmOppCount.setText(String.valueOf(crmArrayList.size()));

            double totalValue = 0.0;
            String formatCrmValue = "0.0";
            for(Crm crm: crmArrayList) {
                totalValue += crm.getCrmValue() != null ? Double.parseDouble(crm.getCrmValue()) : 0.0;
            }

            if (totalValue != 0) {
                formatCrmValue = valueFormatter.format(totalValue);
            }

            crmOppValue.setText(String.format("%s(₦)", formatCrmValue));
        }




    }

    public void calculateOpenInvoicesCount() {

            ArrayList<Ledger> ledgerArrayList = ledgerTable.getLedgerArrayList(customer_id, customer_type);
            double outstandingTotal = 0;
            int count = 0;
            List<String> invoiceNumbers = new ArrayList<String>();
            ArrayList<String> invoiceNumberList = new ArrayList<>();
            for (Ledger l : ledgerArrayList) {
                if(!invoiceNumberList.contains(l.getInvoiceNumber()))
                    invoiceNumberList.add(l.getInvoiceNumber());
            }

            for (Ledger l : ledgerArrayList) {
                if(l.getTransactionType() != null) {
                    if (l.getTransactionType().equals("Return")) {
                        if (l.getOutstandingAmount() != null)
                            outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                    }
                }
            }
            for(String invoiceNumber:invoiceNumberList){
                //String invoiceLastBalance = "";
                double invoiceLastBalance = 0;
                String transactionType = "";
                for (Ledger l : ledgerArrayList) {
                    if(invoiceNumber.equals(l.getInvoiceNumber())) {
                        if(l.getOutstandingAmount() != null)
                            invoiceLastBalance += Double.parseDouble(l.getOutstandingAmount());
                        transactionType = l.getTransactionType();
                        System.out.println("TT**invoiceNumber = " + invoiceNumber);
                    }
                }
                Log.d("InvoiceVal",""+invoiceLastBalance);
                double payment_amount = dbHandler.getCollectionDueAmout(invoiceNumber);
                double invoice_amount = dbHandler.getInvoiceAmout(invoiceNumber);
                System.out.println("invoice_amount = " + invoice_amount);
                System.out.println("payment_amount = " + payment_amount);

                if(invoiceLastBalance!=0 && invoice_amount !=payment_amount && !invoiceNumber.equals("") && invoiceNumber!=null) {
                    invoiceNumbers.add(invoiceNumber);
                    count =count+1;
                }
            }

        System.out.println("TTT:::count = " + count);
        open_invoice.setText(String.valueOf(count));


    }

 /*   private void calculateOpenInvoicesCount() {

        ledgerArrayList = ledgerTable.getLedgerArrayList(customer_id, customer_type);
        double outstandingTotal = 0;
        int count = 0;
        ArrayList<String> invoiceNumberList = new ArrayList<>();
        for (Ledger l : ledgerArrayList) {
            if(!invoiceNumberList.contains(l.getInvoiceNumber()))
                invoiceNumberList.add(l.getInvoiceNumber());
        }

        for (Ledger l : ledgerArrayList) {
            if(l.getTransactionType() != null){
                if(l.getTransactionType().equals("Return")) {
                    if(l.getOutstandingAmount() != null)
                        outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                }
            }
        }
        for(String invoiceNumber:invoiceNumberList){
            //String invoiceLastBalance = "";
            double invoiceLastBalance = 0;
            for (Ledger l : ledgerArrayList) {
                if(invoiceNumber.equals(l.getInvoiceNumber())) {
                    if(l.getOutstandingAmount() != null)
                        invoiceLastBalance += Double.parseDouble(l.getOutstandingAmount());
                }
            }
            Log.d("InvoiceVal",""+invoiceLastBalance);
            count += (invoiceLastBalance == 0) ?  0 : 1;
            // Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount()+" "+ outstandingTotal);
        }

        //int collection_count = dbHandler.get_customer_collection_count(customer_id);
        open_invoice.setText(String.valueOf(count));

    }*/

    private void calculateOutstandingValue(){
        ledgerArrayList = ledgerTable.getLedgerArrayList(customer_id, customer_type);
        double receiptTotal = 0;
        double outstandingTotal = 0;

        if ("Primary".equals(customer_type)) {
            for (Ledger l : ledgerArrayList) {
                if(l.getOutstandingAmount() != null) {
                    outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                    Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount());
                }
            }
        }
        else
        {
            ArrayList<String> invoiceNumberList = new ArrayList<>();
            for (Ledger l : ledgerArrayList) {
                if(!invoiceNumberList.contains(l.getInvoiceNumber()))
                    invoiceNumberList.add(l.getInvoiceNumber());
            }

            for (Ledger l : ledgerArrayList) {
                if(l.getTransactionType() != null) {
                    if (l.getTransactionType().equals("Return")) {
                        if (l.getOutstandingAmount() != null)
                            outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                    }
                }
            }
            for(String invoiceNumber:invoiceNumberList){
                String invoiceLastBalance = "";
                for (Ledger l : ledgerArrayList) {
                    if(l.getTransactionType() != null) {
                        if (invoiceNumber.equals(l.getInvoiceNumber()) && !l.getTransactionType().equals("Return")) {
                            if (l.getOutstandingAmount() != null)
                                invoiceLastBalance = l.getOutstandingAmount();
                        }
                    }
                }
                outstandingTotal += TextUtils.isEmpty(invoiceLastBalance) ? 0 : Double.parseDouble(invoiceLastBalance);
                // Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount()+" "+ outstandingTotal);
            }
        }
        decimalFormat = new DecimalFormat("#,###.00");

        String formatOutstandingTotalAmount = outstandingTotal != 0 ? decimalFormat.format(Math.round(outstandingTotal)) : "";


      /*  if (outstanding == 0) {
            outstanding_val.setText("");

        } else {
            String outstanding1 = formatter.format(outstanding);
            outstanding_val.setText(outstanding1 + " (₦)");
        }*/

        System.out.println("TTT:::formatOutstandingTotalAmount = " + formatOutstandingTotalAmount);
      if(formatOutstandingTotalAmount==""){
          outstanding_val.setText("0");
      }else
        outstanding_val.setText(formatOutstandingTotalAmount);

    }
    private void prepareListData() {
        headerList = new ArrayList<>();
        childList = new HashMap<>();

        // Adding child data
        // headerList.add("Metrics");
        headerList.add("Customer List");
        headerList.add("Transaction List");
        if ("Primary".equals(customer_type)) {
            headerList.add("Waybill");
        }
        headerList.add("Customer Return");
        headerList.add("View Stock");
        headerList.add("Stock Receipt");
        headerList.add("Stock Return");
        headerList.add("Device Info");
        headerList.add("My Messages");

        // Adding child data
        List<String> transactionList = new ArrayList<String>();

        transactionList.add("Orders");
        transactionList.add("Invoices");
        transactionList.add("Collections");
        transactionList.add("Waybill");
//        transactionList.add("Opportunities");
//        transactionList.add("Enquiries");
//        transactionList.add("Quotations");
        transactionList.add("Survey");
        transactionList.add("Feedback");
        transactionList.add("Competitor Info");
        transactionList.add("Expired Products");
        transactionList.add("Customer Return Products");
//        transactionList.add("Learning Stats");

        childList.put(headerList.get(1), transactionList); // Header, Child data

       /* new Handler().postDelayed(() -> {
            if (BuildConfig.DEBUG) {
                Intent myintent = new Intent(getApplicationContext(), CollectionActivity.class);
                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                chronometer.stop();
                //Create the bundle
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customer_id);
                bundle.putString("login_id", login_id);
                bundle.putString("customer_type", customer_type);

                //Add the bundle to the intent
                myintent.putExtras(bundle);

                //Fire that second activity
                startActivity(myintent);
            }
        }, 1000);*/
    }


    private void initializeViews() {

        expandableListView = findViewById(R.id.expandableListView);
        actionBarTitle = findViewById(R.id.toolbar_title);
        order_btn = findViewById(R.id.order_btn);
        invoice_btn = findViewById(R.id.invoice_btn);
        collection_btn = findViewById(R.id.collection_btn);
        ledger_btn = findViewById(R.id.ledger_btn);
        survey_btn = findViewById(R.id.survey_btn);
        artifactsButton = findViewById(R.id.artifacts_btn);
        feedback_btn = findViewById(R.id.feedback_btn);
        info_btn = findViewById(R.id.info_btn);
        crmCardView = findViewById(R.id.crmCardView);
        top_products_btn = findViewById(R.id.top_products_btn);
        notesCardView = findViewById(R.id.notesCardViews);
        cust_order_count = findViewById(R.id.cust_order_count);
        cust_order_value = findViewById(R.id.cust_order_value);
        cust_invoice_value = findViewById(R.id.cust_invoice_value);
        cust_invoice_count = findViewById(R.id.cust_invoice_count);
        check_out_btn = findViewById(R.id.check_out_btn);
        checkintime = findViewById(R.id.checkintime);
        chronometer = findViewById(R.id.txt_duration);
        outstanding_val = findViewById(R.id.outstanding_val);
        notesCardView = findViewById(R.id.notesCardViews);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        back = findViewById(R.id.back);
        doctorvisit_btn = findViewById(R.id.doctorvisit_btn);
        profile_name = navHeader.findViewById(R.id.profile_name);
        profile_email = navHeader.findViewById(R.id.profile_email);
        open_invoice = findViewById(R.id.open_invoice);
        bdeProfileImage = navHeader.findViewById(R.id.navHeaderBdeImageView);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        globals = ((Globals) getApplicationContext());

        customer_type = globals.getCustomer_type();

        email_id = globals.getLogin_email();
        trip_num = globals.getTrip_num();
        sr_name1 = globals.getSr_name();

        ledgerTable = new LedgerTable(getApplicationContext());

        crmTable = new CrmTable(getApplicationContext());

        crmOppCount = findViewById(R.id.crmOppCountTv);
        crmOppValue = findViewById(R.id.crmOppValueTv);

        valueFormatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();
        customer_id = bundle.getString("customer_id");
        login_id = bundle.getString("sales_rep_id");
        nav_type = bundle.getString("nav_type");
        user_role = bundle.getString("user_role");


        //Populate Header Details

        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                actionBarTitle.setText(customerDetails.getString(2));
//                address1.setText(customerDetails.getString(6));
                // address2.setText(customerDetails.getString(7));
                // address3.setText(customerDetails.getString(8));
                //city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                // byte[] blob = customerDetails.getBlob(13);
                // Bitmap bmp= convertByteArrayToBitmap(blob);
                // customer_pic.setImageBitmap(bmp);
                //  sr_image.setImageBitmap(bmp);
                // Toast.makeText(getApplicationContext(), customerDetails.getString(2), Toast.LENGTH_LONG).show();

            }
        }

        //Populate SalesRep Details
        Cursor navdetails = dbHandler.getUserDetails(login_id);
        int navdetailscount = navdetails.getCount();
        if (navdetailscount == 0) {

        } else {
            int i = 0;
            while (navdetails.moveToNext()) {

                profile_email.setText(navdetails.getString(0));
                profile_name.setText(navdetails.getString(1));

                Bitmap bdeImageBitmap = dbHandler.getBDELogoImage(login_id);
                if (bdeImageBitmap != null) {
                    bdeProfileImage.setImageBitmap(bdeImageBitmap);
                }

            }
        }

    }

    public void showsrdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_sr_details, null);
        final TextView sr_name = alertLayout.findViewById(R.id.sr_name_details);
        final TextView sr_rm = alertLayout.findViewById(R.id.sr_rm_name);
        final TextView sr_rgn = alertLayout.findViewById(R.id.sr_region);
        final TextView sr_whouse = alertLayout.findViewById(R.id.sr_warehouse);
        final TextView sr_gps = alertLayout.findViewById(R.id.sr_gps);

        sr_name.setText(sr_name_details);
        sr_rm.setText(sr_rm_name);
        sr_rgn.setText(sr_region);
        sr_whouse.setText(sr_warehouse);
        sr_gps.setText(lat_lng);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    public void showtemplatedetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_template, null);
        //final EditText input_search = alertLayout.findViewById(R.id.inputSearch);

        //sr_name.setText(sr_name_details);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        listTemplate = new ArrayList<ListTemplate>();
        Cursor data = dbHandler.getTemplates(customer_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(alertLayout.getRootView().getContext());
            alertbox.setMessage("No Templates");
            alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {

                    dialog.dismiss();

                }

            });

            alertbox.show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                listTemplate1 = new ListTemplate(data.getString(0), data.getString(1));
                listTemplate.add(i, listTemplate1);
                i++;
            }

            adapter = new ListTemplateAdapter(getApplicationContext(), R.layout.activity_template_list, listTemplate);
            ListView list_sales_line = alertLayout.findViewById(R.id.list_view);
            list_sales_line.setAdapter(adapter);

            alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }

            });

            AlertDialog dialog = alert.create();
            dialog.show();
        }

     /*   input_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                HomeActivity.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        }); */
    }

    public void navigateInvoice() {
        Intent myintent = new Intent(getApplicationContext(), InvoiceActivity.class);
        long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
        SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
        //chronometer.stop();
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("checkin_time", checkin_time);
        bundle.putString("customer_id", customer_id);
        bundle.putString("login_id", login_id);
        bundle.putString("invoice_type", "Invoice");


        //Add the bundle to the intent
        myintent.putExtras(bundle);

        //Fire that second activity
        startActivity(myintent);
    }

    public void navigateSecondarySalesOrder() {
        Intent myintent = new Intent(getApplicationContext(), InvoiceActivity.class);
        long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
        SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
        //chronometer.stop();
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("checkin_time", checkin_time);
        bundle.putString("customer_id", customer_id);
        bundle.putString("login_id", login_id);
        bundle.putString("invoice_type", "SecondarySalesOrder");


        //Add the bundle to the intent
        myintent.putExtras(bundle);

        //Fire that second activity
        startActivity(myintent);
    }

    private void getlocation() {


        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            lat = Double.toString(latitude);
            lng = Double.toString(longitude);

            lat_lng = lat + ":" + lng;

        } else {

            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQUEST_CODE_CUSTOMER_RETURN){
            calculateOutstandingValue();
            calculateOpenInvoicesCount();
        }
    }

    private class OnDismissListener implements PopupMenu.OnDismissListener {

        @Override
        public void onDismiss(PopupMenu menu) {

        }

    }

    private class OnMenuItemClickListener implements
            PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.createorder:
                    Intent myintent = new Intent(getApplicationContext(), OrderActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    //chronometer.stop();
                    //Create the bundle
                    Bundle bundle = new Bundle();

                    //Add your data to bundle
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putString("login_id", login_id);

                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivity(myintent);
                    return true;
                case R.id.createordertemplate:
                    showtemplatedetails();
                    return true;

                case R.id.createinvoice:
                    navigateInvoice();
                    return true;

                case R.id.createsecsalesorder:
                    navigateSecondarySalesOrder();
                    return true;

                   /* Intent myintent1=new Intent(getApplicationContext(), OrderProductActivity.class);

                    //Create the bundle
                    Bundle bundle1 = new Bundle();

                    //Add your data to bundle
                    bundle1.putString("checkin_time", checkin_time);
                    bundle1.putString("customer_id",  customer_id);
                    bundle1.putInt("order_id", 73 );
                    bundle1.putString("template", "Template" );

                    //Add the bundle to the intent
                    myintent1.putExtras(bundle1);

                    //Fire that second activity
                    startActivity(myintent1); */


            }
            return false;
        }
    }



    public class ListTemplateAdapter extends ArrayAdapter<ListTemplate> {

        Activity mActivity = null;
        AlertDialog alertDialog1;
        String trip_num = "";
        Globals globals;
        MyDBHandler dbHandler;
        private LayoutInflater mInflater;
        private ArrayList<ListTemplate> users;
        private int mViewResourceId;
        private Context c;

        public ListTemplateAdapter(Context context, int textViewResourceId, ArrayList<ListTemplate> users) {

            super(context, textViewResourceId, users);
            this.users = users;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            dbHandler = new MyDBHandler(c, null, null, 1);

        }


        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            convertView = mInflater.inflate(mViewResourceId, null);
            final ListTemplate user = users.get(position);
            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.product = convertView.findViewById(R.id.product_name);


                if (viewHolder.product != null) {
                    viewHolder.product.setText(user.getTemplate_name());
                }

                viewHolder.product.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        final String id = user.getSalesorder_id();
                        Intent myintent1 = new Intent(getApplicationContext(), OrderProductActivity.class);

                        //Create the bundle
                        Bundle bundle1 = new Bundle();

                        //Add your data to bundle
                        bundle1.putString("checkin_time", checkin_time);
                        bundle1.putString("customer_id", customer_id);
                        bundle1.putInt("order_id", Integer.parseInt(id));
                        bundle1.putString("template", "Template");
                        bundle1.putString("login_id", login_id);

                        //Add the bundle to the intent
                        myintent1.putExtras(bundle1);

                        //Fire that second activity
                        startActivity(myintent1);
                    }

                });

            }

            return convertView;
        }

        public class ViewHolder {
            TextView product;
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            getCrmOppInfo();

        } catch (Exception e) {

        }

        Log.d("Crm---", "Home--onResume :Home " + nav_type);

        chronometer.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        chronometer.start();

        // if (nav_type.equals("Home") || nav_type.equals("SR Landing")) {

       /* } else {
            Log.d("Crm---", "Home--onResume :Not Home " + nav_type);
            chronometer.setBase(SystemClock.elapsedRealtime() + stopTime);
            chronometer.start();

        }*/

    }





    @Override
    protected void onPause() {
        super.onPause();

        Log.d("Crm---", "Home--onPause");

    }

}

