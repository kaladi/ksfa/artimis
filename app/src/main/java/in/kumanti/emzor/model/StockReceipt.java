package in.kumanti.emzor.model;

import java.util.ArrayList;

public class StockReceipt {


    public int id;
    ArrayList<StockReceiptBatch> batchDetailArrayList = new ArrayList<StockReceiptBatch>();
    private String tabCode;
    private String companyCode;
    private String stockReceiptCode;
    private String stockReceiptDate;
    public String srCode;
    public String invoiceCode;
    public String sourceName;
    public String stockReceiptSource;
    public String currency;
    public String productId;
    private String productName;
    private String productUom;
    private String productQuantity;
    private String productPrice;
    private String productTotalPrice;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String batchControlled;
    private String stockIssueNumber;
    private String error_productName;
    private String error_productQuantity;
    private String error_productPrice;
    private String visitSequenceNumber;

    private String invoiceNumber;
    private String invoiceDate;
    private String orderNumber;
    private String orderDate;
    private String adjustmentQuantity;
    private String remarks;
    private String receiptType;
    private boolean isSync;

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTabId() {
        return tabCode;
    }

    public void setTabId(String tabId) {
        this.tabCode = tabId;
    }

    public String getStockReceiptCode() {
        return stockReceiptCode;
    }

    public void setStockReceiptCode(String stockReceiptCode) {
        this.stockReceiptCode = stockReceiptCode;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getAdjustmentQuantity() {
        return adjustmentQuantity;
    }

    public void setAdjustmentQuantity(String quantity) {
        this.adjustmentQuantity = quantity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getStockReceiptNumber() {
        return stockReceiptCode;
    }

    public void setStockReceiptNumber(String stockReceiptNumber) {
        this.stockReceiptCode = stockReceiptNumber;
    }

    public String getStockReceiptDate() {
        return stockReceiptDate;
    }

    public void setStockReceiptDate(String stockReceiptDate) {
        this.stockReceiptDate = stockReceiptDate;
    }

    public String getStockReceiptSource() {
        return stockReceiptSource;
    }

    public void setStockReceiptSource(String stockReceiptSource) {
        this.stockReceiptSource = stockReceiptSource;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }


    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }


    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }


    public String getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(String productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }

    public String getError_productName() {
        return error_productName;
    }

    public void setError_productName(String error_productName) {
        this.error_productName = error_productName;
    }

    public ArrayList<StockReceiptBatch> getBatchDetailArrayList() {
        return batchDetailArrayList;
    }

    public void setBatchDetailArrayList(ArrayList<StockReceiptBatch> batchDetailArrayList) {
        this.batchDetailArrayList = batchDetailArrayList;
    }

    public int getBatchDetailArrayListTotalCount() {
        int count = 0;
        for (StockReceiptBatch srb : batchDetailArrayList) {
            count += Integer.parseInt(srb.getBatchQuantity());
        }
        return count;
    }

    public String getBatchControlled() {
        return batchControlled;
    }

    public void setBatchControlled(String batchControlled) {
        this.batchControlled = batchControlled;
    }

    public String getStockIssueNumber() {
        return stockIssueNumber;
    }

    public void setStockIssueNumber(String stockIssueNumber) {
        this.stockIssueNumber = stockIssueNumber;
    }

    public String getError_productQuantity() {
        return error_productQuantity;
    }

    public void setError_productQuantity(String error_productQuantity) {
        this.error_productQuantity = error_productQuantity;
    }

    public String getError_productPrice() {
        return error_productPrice;
    }

    public void setError_productPrice(String error_productPrice) {
        this.error_productPrice = error_productPrice;
    }
}
