package in.kumanti.emzor.model;

public class CrmOpportunityProducts {

    public int opportunityProductId;
    public String opportunityId;
    public String productGroupCode;
    public String productCode;
    public String productName;
    public String productUom;
    public String productQuantity;
    public String productUnitPrice;
    public String productStatus;
    public String catalogueUrl;
    public String cataloguePath;

    public boolean isSync;
    public String latestSyncDate;
    public String createdAt;
    public String updatedAt;

    public String errorProductName;
    public String errorProductQty;

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }


    public int getOpportunityProductId() {
        return opportunityProductId;
    }

    public void setOpportunityProductId(int opportunityProductId) {
        this.opportunityProductId = opportunityProductId;
    }


    public String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getProductGroupCode() {
        return productGroupCode;
    }

    public void setProductGroupCode(String productGroupCode) {
        this.productGroupCode = productGroupCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductUnitPrice() {
        return productUnitPrice;
    }

    public void setProductUnitPrice(String productUnitPrice) {
        this.productUnitPrice = productUnitPrice;
    }


    public String getCatalogueUrl() {
        return catalogueUrl;
    }

    public void setCatalogueUrl(String catalogueUrl) {
        this.catalogueUrl = catalogueUrl;
    }

    public String getCataloguePath() {
        return cataloguePath;
    }

    public void setCataloguePath(String cataloguePath) {
        this.cataloguePath = cataloguePath;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getLatestSyncDate() {
        return latestSyncDate;
    }

    public void setLatestSyncDate(String latestSyncDate) {
        this.latestSyncDate = latestSyncDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getErrorProductName() {
        return errorProductName;
    }

    public void setErrorProductName(String errorProductName) {
        this.errorProductName = errorProductName;
    }

    public String getErrorProductQty() {
        return errorProductQty;
    }

    public void setErrorProductQty(String errorProductQty) {
        this.errorProductQty = errorProductQty;
    }
}
