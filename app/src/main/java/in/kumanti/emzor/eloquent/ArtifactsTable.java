package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.Artifacts;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_ARTIFACTS;

public class ArtifactsTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_ARTIFACTS_ID = "id";
    private static final String COLUMN_COMPANY_CODE = "company_code";
    private static final String COLUMN_TAB_CODE = "tab_code";
    private static final String COLUMN_SALES_REP_CODE = "sr_code";
    private static final String COLUMN_CUSTOMER_CODE = "customer_code";
    private static final String COLUMN_ARTIFACTS_ARTIFACT_TYPE = "artifact_type";
    private static final String COLUMN_ARTIFACTS_FILE_TYPE = "file_type";
    private static final String COLUMN_ARTIFACTS_FILE_NAME = "file_name";
    private static final String COLUMN_ARTIFACTS_FILE_PATH = "file_path";
    private static final String COLUMN_ARTIFACTS_FILE_URL = "file_url";
    private static final String COLUMN_ARTIFACTS_CREATED_AT = "created_at";
    private static final String COLUMN_ARTIFACTS_UPDATED_AT = "updated_at";


    public Context context;

    public ArtifactsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTIFACTS);

        String productGroupTable = "CREATE TABLE IF NOT EXISTS " + TABLE_ARTIFACTS + "(" +
                COLUMN_ARTIFACTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_COMPANY_CODE + " TEXT, " +
                COLUMN_TAB_CODE + " TEXT, " +
                COLUMN_SALES_REP_CODE + " TEXT, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_ARTIFACTS_ARTIFACT_TYPE + " TEXT, " +
                COLUMN_ARTIFACTS_FILE_TYPE + " TEXT, " +
                COLUMN_ARTIFACTS_FILE_NAME + " TEXT, " +
                COLUMN_ARTIFACTS_FILE_PATH + " TEXT, " +
                COLUMN_ARTIFACTS_FILE_URL + " TEXT, " +
                COLUMN_ARTIFACTS_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_ARTIFACTS_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(productGroupTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(Artifacts artifacts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_COMPANY_CODE, artifacts.getCompanyCode());
        values.put(COLUMN_TAB_CODE, artifacts.getTabCode());
        values.put(COLUMN_SALES_REP_CODE, artifacts.getSrCode());
        values.put(COLUMN_CUSTOMER_CODE, artifacts.getCustomerCode());
        values.put(COLUMN_ARTIFACTS_FILE_TYPE, artifacts.getFileType());
        values.put(COLUMN_ARTIFACTS_FILE_NAME, artifacts.getFileName());
        values.put(COLUMN_ARTIFACTS_FILE_PATH, artifacts.getFilePath());
        values.put(COLUMN_ARTIFACTS_FILE_URL, artifacts.getFileURL());
        values.put(COLUMN_ARTIFACTS_ARTIFACT_TYPE, artifacts.getArtifactType());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_ARTIFACTS, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Artifacts File List details from the Master via Web Service
     */

    public void insertingArtifactsMaster(ArrayList<Artifacts> artifactsArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (artifactsArrayList != null) {
                for (int i = 0; i < artifactsArrayList.size(); i++) {
                    values.put(COLUMN_COMPANY_CODE, artifactsArrayList.get(i).getCompanyCode());
                    values.put(COLUMN_TAB_CODE, artifactsArrayList.get(i).getTabCode());
                    values.put(COLUMN_SALES_REP_CODE, artifactsArrayList.get(i).getSrCode());
                    values.put(COLUMN_CUSTOMER_CODE, artifactsArrayList.get(i).getCustomerCode());
                    values.put(COLUMN_ARTIFACTS_FILE_TYPE, artifactsArrayList.get(i).getFileType());
                    values.put(COLUMN_ARTIFACTS_FILE_NAME, artifactsArrayList.get(i).getFileName());
                    values.put(COLUMN_ARTIFACTS_FILE_PATH, "/KSFA/artifacts/" + artifactsArrayList.get(i).getFileName());
                    values.put(COLUMN_ARTIFACTS_FILE_URL, artifactsArrayList.get(i).getFileURL());
                    values.put(COLUMN_ARTIFACTS_ARTIFACT_TYPE, artifactsArrayList.get(i).getArtifactType());
                    db.insert(TABLE_ARTIFACTS, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }

    public ArrayList<Artifacts> getArtifactsByType(String fileType) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_ARTIFACTS + " WHERE artifact_type=?";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{fileType});
        Log.d("Data", "Get Product Groups  ");

        ArrayList<Artifacts> artifactsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Artifacts artifacts = new Artifacts();
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            artifacts.setId(cursor.getInt(0));
            artifacts.setArtifactType(cursor.getString(5));
            artifacts.setFileType(cursor.getString(6));
            artifacts.setFileName(cursor.getString(7));
            artifacts.setFilePath(cursor.getString(8));
            artifacts.setFileURL(cursor.getString(9));
            artifactsArrayList.add(artifacts);
        }
        cursor.close();
        sqlWrite.close();
        return artifactsArrayList;
    }

    public void deleteArtifacts() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ARTIFACTS, null, null);
        db.close();
    }

    public ArrayList<Artifacts> getArtifactsTypes() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT DISTINCT artifact_type as artifactType FROM " + TABLE_ARTIFACTS + " ORDER BY artifact_type ASC";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        Log.d("Data", "Get Product Groups  ");

        ArrayList<Artifacts> artifactsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Artifacts artifacts = new Artifacts();
            artifacts.setArtifactType(cursor.getString(0));
            artifactsArrayList.add(artifacts);
        }
        cursor.close();
        sqlWrite.close();
        return artifactsArrayList;
    }


}
