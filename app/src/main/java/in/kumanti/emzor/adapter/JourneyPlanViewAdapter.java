package in.kumanti.emzor.adapter;

/**
 * Created by Mitch on 2016-05-13.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.HomeActivity;
import in.kumanti.emzor.model.SalesRep_Customer_List;
import in.kumanti.emzor.utils.Globals;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by Mitch on 2016-05-06.
 */
public class JourneyPlanViewAdapter extends ArrayAdapter<SalesRep_Customer_List> {

    Activity mActivity = null;
    android.support.v7.app.AlertDialog alertDialog1;
    String customer_id = "";
    String sales_rep_id = "";
    String trip_num = "";
    Globals globals;
    private LayoutInflater mInflater;
    private ArrayList<SalesRep_Customer_List> users;
    private int mViewResourceId;
    private Context c;


    public JourneyPlanViewAdapter(Context context, int textViewResourceId, ArrayList<SalesRep_Customer_List> users, String sales_rep_id_val, String tripno_val) {
        super(context, textViewResourceId, users);
        this.users = users;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
        sales_rep_id = sales_rep_id_val;
        trip_num = tripno_val;
        c = context;
        globals = ((Globals) c);
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        convertView = mInflater.inflate(mViewResourceId, null);
        final SalesRep_Customer_List user = users.get(position);
        if (user != null) {
            viewHolder = new ViewHolder();
            viewHolder.serial_num = convertView.findViewById(R.id.serial_num);
            viewHolder.customer_name = convertView.findViewById(R.id.customer_name);
            viewHolder.location = convertView.findViewById(R.id.location);
            viewHolder.status = convertView.findViewById(R.id.status);
            viewHolder.customer_type = convertView.findViewById(R.id.customer_type);

            String status_val = user.getStatus();
            String customer_type1 = user.getCustomer_type();


            if (viewHolder.serial_num != null) {
                viewHolder.serial_num.setText(user.getSerial_num());
            }
            if (viewHolder.customer_name != null) {
                viewHolder.customer_name.setText((user.getCustomer_name()));
            }
            if (viewHolder.location != null) {
                viewHolder.location.setText((user.getLocation_name()));
            }
            if (viewHolder.status != null) {
                if ("true".equals(status_val)) {
                    viewHolder.status.setChecked(true);
                } else {
                    viewHolder.status.setChecked(false);
                }
            }
            if (viewHolder.customer_type != null) {
                if ("Primary".equals(customer_type1)) {
                    viewHolder.customer_type.setText(("P"));
                } else {
                    viewHolder.customer_type.setText((""));
                }
            }


        }

        return convertView;
    }

    private AlertDialog AskOption_sync() {
        // AlertDialog.Builder myQuittingDialogBox = new AlertDialog.Builder(v.getRootView().getContext());

        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(((getContext())))
                //set message, title, and icon
                .setTitle("Transact ?")
                //.setMessage("Do you want to SynDB?")

                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code

                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        //  intent.putExtra("customer_id", customer_id);
                        getContext().startActivity(intent);
                        dialog.dismiss();
                    }

                })


                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    public class ViewHolder {
        TextView serial_num, customer_name, location, customer_type;
        CheckBox status;

    }

  /*  private void showChangePasswordDialog() {

       // LayoutInflater mInflater = (LayoutInflater)
             //   c.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
       // View alertLayout = mInflater.inflate(R.layout.activity_unplanned_customers, null);        //convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_unplanned_customers, null, false);

        View myView = LayoutInflater.from(getContext()).inflate(R.layout.activity_unplanned_customers, null);

        AlertDialog.Builder alertbox = new AlertDialog.Builder(c);
        AlertDialog alertDialog = alertbox.create();
        alertDialog.setTitle("Unplanned Customers");
        alertDialog.setView(myView);

        alertDialog.getWindow().setType(WindowManager.LayoutParams.
                TYPE_SYSTEM_ALERT);
        alertDialog.show();
    } */

/*    private void showChangePasswordDialog() {

        final Dialog dialog = new Dialog(c);
        LayoutInflater mInflater = (LayoutInflater)
                c.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View alertLayout = mInflater.inflate(R.layout.activity_unplanned_customers, null);

        //View view = getLayoutInflater().inflate(R.layout.dialog_main, null);

        dialog.setContentView(alertLayout);

        dialog.show();

    } */

}