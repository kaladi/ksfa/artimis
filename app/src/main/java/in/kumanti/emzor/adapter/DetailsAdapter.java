package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.Details;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {

    public ArrayList<Details> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;

    public DetailsAdapter(ArrayList<Details> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(Details item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(Details item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_details, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        //Log.d("NotesData", mDataset.get(position).getNotesText());
        viewHolder.notesData.setText(mDataset.get(position).getNotesText());
        viewHolder.notesTimestamp.setText(mDataset.get(position).getTimeStamp());

        viewHolder.editNotesButton.setOnClickListener(view -> {
            mListener.onClick(view, position);
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView notesData, notesTimestamp;
        CardView notesCardView;
        ImageView editNotesButton;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            notesData = v.findViewById(R.id.detailsTextView);
            notesTimestamp = v.findViewById(R.id.detailsTimestampTV);
            notesCardView = v.findViewById(R.id.createDetailsCardView);
            editNotesButton = v.findViewById(R.id.editDetailsButton);
        }
    }


}