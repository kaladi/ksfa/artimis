package in.kumanti.emzor.model;

public class CustomerShippingAddress {

    public int id;
    public String customer_id;
    public String customer_name;
    public String sales_rep_id;
    public String sales_rep_name;
    public String serial_number;
    public String address1;
    public String address2;
    public String address3;
    public String city;
    public String state;
    public String mobile_number;
    public String pincode;
    public String customer_image;
    public String ship_address1;
    public String ship_address2;
    public String ship_address3;
    public String ship_state;
    public String ship_city;
    public String ship_country;
    public String customer_type;
    public String created_at;
    public String updated_at;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getSales_rep_id() {
        return sales_rep_id;
    }

    public void setSales_rep_id(String sales_rep_id) {
        this.sales_rep_id = sales_rep_id;
    }

    public String getSales_rep_name() {
        return sales_rep_name;
    }

    public void setSales_rep_name(String sales_rep_name) {
        this.sales_rep_name = sales_rep_name;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCustomer_image() {
        return customer_image;
    }

    public void setCustomer_image(String customer_image) {
        this.customer_image = customer_image;
    }

    public String getShip_address1() {
        return ship_address1;
    }

    public void setShip_address1(String ship_address1) {
        this.ship_address1 = ship_address1;
    }

    public String getShip_address2() {
        return ship_address2;
    }

    public void setShip_address2(String ship_address2) {
        this.ship_address2 = ship_address2;
    }

    public String getShip_address3() {
        return ship_address3;
    }

    public void setShip_address3(String ship_address3) {
        this.ship_address3 = ship_address3;
    }

    public String getShip_state() {
        return ship_state;
    }

    public void setShip_state(String ship_state) {
        this.ship_state = ship_state;
    }

    public String getShip_city() {
        return ship_city;
    }

    public void setShip_city(String ship_city) {
        this.ship_city = ship_city;
    }

    public String getShip_country() {
        return ship_country;
    }

    public void setShip_country(String ship_country) {
        this.ship_country = ship_country;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(String customer_type) {
        this.customer_type = customer_type;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


}
