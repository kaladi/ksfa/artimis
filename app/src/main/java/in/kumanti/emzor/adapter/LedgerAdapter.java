package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.Ledger;

public class LedgerAdapter extends RecyclerView.Adapter<LedgerAdapter.ViewHolder> {

    public ArrayList<Ledger> mDataset;
    public Context context;
    DecimalFormat decimalFormat;
    double invoiceAmountDouble, receiptAmountDouble, outstandingAmountDouble;
    String formatInvoiceAmount, formatReceiptAmount, formatOutstandingAmount;


    public LedgerAdapter(ArrayList<Ledger> myDataset) {
        mDataset = myDataset;
    }

    public void add(Ledger item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(Ledger item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public LedgerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ledger, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.invoiceNumber.setText(mDataset.get(position).getDocumentNumber());
        viewHolder.transactionType.setText(mDataset.get(position).getTransactionType());


        if (!TextUtils.isEmpty(mDataset.get(position).getInvoiceDate())) {
            viewHolder.invoiceDate.setText(mDataset.get(position).getInvoiceDate());

        }


        if (!TextUtils.isEmpty(mDataset.get(position).getInvoiceAmount())) {
            invoiceAmountDouble = Double.parseDouble(mDataset.get(position).getInvoiceAmount());
            formatInvoiceAmount = decimalFormat.format(invoiceAmountDouble);
            String value = formatInvoiceAmount;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.invoiceAmount.setText(value);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getReceiptAmount())) {
            receiptAmountDouble = Double.parseDouble(mDataset.get(position).getReceiptAmount());
            formatReceiptAmount = decimalFormat.format(receiptAmountDouble);
            String value = formatReceiptAmount;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.receiptAmount.setText(value);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getOutstandingAmount())) {
            outstandingAmountDouble = Double.parseDouble(mDataset.get(position).getOutstandingAmount());
            if(mDataset.get(position).getTransactionType().equals("Collection"))
                outstandingAmountDouble = outstandingAmountDouble<0 ? outstandingAmountDouble*(-1):outstandingAmountDouble;
            formatOutstandingAmount = decimalFormat.format(outstandingAmountDouble);
            String value = formatOutstandingAmount;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.outstandingAmount.setText(value);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView transactionType, invoiceNumber, invoiceDate, invoiceAmount, receiptAmount, outstandingAmount;

        public ViewHolder(View v) {
            super(v);

            transactionType = v.findViewById(R.id.ledgerTransTypeTv);
            invoiceNumber = v.findViewById(R.id.ledgerInvNumberTv);
            invoiceDate = v.findViewById(R.id.ledgerInvDateTv);
            invoiceAmount = v.findViewById(R.id.ledgerInvAmountTv);
            receiptAmount = v.findViewById(R.id.ledgerReceiptAmountTv);
            outstandingAmount = v.findViewById(R.id.ledgerOutstandingAmountTv);
        }
    }


}