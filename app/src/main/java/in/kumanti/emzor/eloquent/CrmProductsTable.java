package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.CrmProducts;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_PRODUCTS;

public class CrmProductsTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_CRM_PRODUCT_ID = "productId";
    private static final String COLUMN_CRM_NUMBER = "crmNo";
    private static final String COLUMN_CRM_PRODUCT_CODE = "productCode";
    private static final String COLUMN_CRM_PRODUCT_NAME = "productName";
    private static final String COLUMN_CRM_PRODUCT_UOM = "uom";
    private static final String COLUMN_CRM_PRODUCT_QUANTITY = "quantity";
    private static final String COLUMN_CRM_PRODUCT_BUDGET = "budget";
    private static final String COLUMN_CRM_PRODUCT_PRICE = "price";
    private static final String COLUMN_CRM_PRODUCT_VALUE = "value";
    private static final String COLUMN_CRM_IS_SYNC = "isSync";
    private static final String COLUMN_CRM_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_CRM_PRODUCT_UPDATED_AT = "updatedAt";


    public Context context;


    public CrmProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_PRODUCTS);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_PRODUCTS + "(" +
                COLUMN_CRM_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CRM_NUMBER + " INTEGER, " +
                COLUMN_CRM_PRODUCT_CODE + " TEXT, " +
                COLUMN_CRM_PRODUCT_NAME + " TEXT, " +
                COLUMN_CRM_PRODUCT_UOM + " TEXT, " +
                COLUMN_CRM_PRODUCT_QUANTITY + " TEXT, " +
                COLUMN_CRM_PRODUCT_BUDGET + " TEXT, " +
                COLUMN_CRM_PRODUCT_PRICE + " TEXT, " +
                COLUMN_CRM_PRODUCT_VALUE + " TEXT, " +
                COLUMN_CRM_IS_SYNC + " TEXT, " +
                COLUMN_CRM_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CRM_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmProducts crmProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CRM_NUMBER, crmProducts.getCrmNumber());
        values.put(COLUMN_CRM_PRODUCT_CODE, crmProducts.getProductCode());
        values.put(COLUMN_CRM_PRODUCT_NAME, crmProducts.getProductName());
        values.put(COLUMN_CRM_PRODUCT_UOM, crmProducts.getUom());
        values.put(COLUMN_CRM_PRODUCT_QUANTITY, crmProducts.getQuantity());
        values.put(COLUMN_CRM_PRODUCT_BUDGET, crmProducts.getBudget());
        values.put(COLUMN_CRM_PRODUCT_PRICE, crmProducts.getUnitPrice());
        values.put(COLUMN_CRM_PRODUCT_VALUE, crmProducts.getValue());
        values.put(COLUMN_CRM_IS_SYNC, crmProducts.isSync());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_PRODUCTS, null, values);
        db.close();
        return id;
    }


    public ArrayList<CrmProducts> getCrmProducts(String crmNumber) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_PRODUCTS +" WHERE "+ COLUMN_CRM_NUMBER + "=?" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {crmNumber} );

        Log.d("ViewStock--","Get Products  " );
        ArrayList<CrmProducts> products = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmProducts product = new CrmProducts();
            Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            product.setProductId(cursor.getInt(0));
            product.setCrmNumber(cursor.getString(1));
            product.setProductCode(cursor.getString(2));
            product.setProductName(cursor.getString(3));
            product.setUom(cursor.getString(4));
            product.setQuantity(cursor.getString(5));
            product.setBudget(cursor.getString(6));
            product.setUnitPrice(cursor.getString(7));
            product.setValue(cursor.getString(8));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }

    public void deleteByCrmNumber(String crmNumber){
        SQLiteDatabase db = this.getWritableDatabase();

        // Deleting all records from database table
        db.delete(TABLE_CRM_PRODUCTS, COLUMN_CRM_NUMBER+ "='" + crmNumber+"'", null);
        db.close();
    }

    public ArrayList<CrmProducts> getCrmProductsForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CRM_PRODUCTS + " WHERE DATE(datetime(createdAt,'unixepoch')) >= date('now','-5 Days')";

        ArrayList<CrmProducts> crmProductsArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            CrmProducts crmProducts = new CrmProducts();
            crmProducts.setTabCode(Constants.TAB_CODE);
            crmProducts.setCompanyCode(Constants.COMPANY_CODE);
            crmProducts.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            crmProducts.setProductId(id);
            crmProducts.setCrmNumber(cursor.getString(1));
            crmProducts.setProductCode(cursor.getString(2));
            crmProducts.setProductName(cursor.getString(3));
            crmProducts.setUom(cursor.getString(4));
            crmProducts.setQuantity(cursor.getString(5));
            crmProducts.setBudget(cursor.getString(6));
            crmProducts.setUnitPrice(cursor.getString(7));
            crmProducts.setValue(cursor.getString(8));
            crmProducts.setCreatedAt(cursor.getString(10));
            crmProducts.setUpdatedAt(cursor.getString(11));

            crmProductsArrayList.add(crmProducts);
        }
        cursor.close();
        sqlWrite.close();
        return crmProductsArrayList;
    }


    public ArrayList<CrmProducts> getOrderCrmProducts(String crmNumber) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //String selectQuery = "SELECT * FROM " + TABLE_CRM_PRODUCTS +" WHERE "+ COLUMN_CRM_NUMBER + "=?" ;
        String selectQuery = "select crmPro.productCode, crmPro.productName, crmPro.uom, crmPro.quantity, crmPro.budget,crmPro.price, crmPro.value, crmPro.crmNo from xxmsales_crm_products as crmPro left join xxmsales_item_details as item on item.product_id = crmPro.productCode  where crmPro.crmNo = ? and item.product_id is NOT null" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {crmNumber} );

        Log.d("ViewStock--","Get Products  " );
        ArrayList<CrmProducts> products = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmProducts product = new CrmProducts();
            Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            product.setProductCode(cursor.getString(0));
            product.setProductName(cursor.getString(1));
            product.setUom(cursor.getString(2));
            product.setQuantity(cursor.getString(3));
            product.setBudget(cursor.getString(4));
            product.setUnitPrice(cursor.getString(5));
            product.setValue(cursor.getString(6));
            product.setCrmNumber(cursor.getString(7));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }
}
