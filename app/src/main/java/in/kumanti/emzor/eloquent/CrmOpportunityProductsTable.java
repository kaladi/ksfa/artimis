package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.CrmOpportunityProducts;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_OPPORTUNITY_PRODUCTS;

public class CrmOpportunityProductsTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_OPPORTUNITY_PRODUCT_ID = "opportunityProductId";
    private static final String COLUMN_OPPORTUNITY_ID = "opportunityId";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_GROUP_CODE = "productGroupCode";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_CODE = "productCode";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_NAME = "productName";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_UOM = "productUom";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_QUANTITY = "productQuantity";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_UNIT_PRICE = "productUnitPrice";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_IS_SYNC = "isSync";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_OPPORTUNITY_PRODUCT_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmOpportunityProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_OPPORTUNITY_PRODUCTS);
        String opportunityTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_OPPORTUNITY_PRODUCTS + "(" +
                COLUMN_OPPORTUNITY_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_OPPORTUNITY_ID + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_GROUP_CODE + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_CODE + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_NAME + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_UOM + " REAL, " +
                COLUMN_OPPORTUNITY_PRODUCT_QUANTITY + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_UNIT_PRICE + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_IS_SYNC + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_OPPORTUNITY_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_OPPORTUNITY_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(opportunityTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmOpportunityProducts opportunityProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_OPPORTUNITY_ID, opportunityProducts.getOpportunityId());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_GROUP_CODE, opportunityProducts.getProductGroupCode());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_CODE, opportunityProducts.getProductCode());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_NAME, opportunityProducts.getProductName());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_UOM, opportunityProducts.getProductUom());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_QUANTITY, opportunityProducts.getProductQuantity());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_UNIT_PRICE, opportunityProducts.getProductUnitPrice());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_IS_SYNC, opportunityProducts.getLatestSyncDate());
        values.put(COLUMN_OPPORTUNITY_PRODUCT_LATEST_SYNC_DATE, opportunityProducts.getLatestSyncDate());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_OPPORTUNITY_PRODUCTS, null, values);
        db.close();
        return id;
    }


    public ArrayList<CrmOpportunityProducts> getOpportunityProducts(String opportunityId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_OPPORTUNITY_PRODUCTS + " where " + COLUMN_OPPORTUNITY_ID + "=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{opportunityId});

        ArrayList<CrmOpportunityProducts> opportunityProductsArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            CrmOpportunityProducts op = new CrmOpportunityProducts();
            op.setOpportunityProductId(cursor.getInt(0));
            op.setOpportunityId(cursor.getString(1));
            op.setProductGroupCode(cursor.getString(2));
            op.setProductCode(cursor.getString(3));
            op.setProductName(cursor.getString(4));
            op.setProductUom(cursor.getString(5));
            op.setProductQuantity(cursor.getString(6));
            op.setProductUnitPrice(cursor.getString(7));
            op.setProductStatus(cursor.getString(8));

            opportunityProductsArrayList.add(i, op);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return opportunityProductsArrayList;
    }


}
