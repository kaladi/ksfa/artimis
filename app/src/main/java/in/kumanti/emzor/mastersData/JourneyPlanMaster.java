package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class JourneyPlanMaster {

    private String sr_code;
    private String jptype;
    private String route;
    private String route_id;
    private String id;
    private String seq_no;
    private String customer_code;
    private String customer_name;
    private String customer_type;
    private String location;
    private String gps_latitude;
    private String gps_longitude;


    public JourneyPlanMaster(

            String sr_code,
            String jptype,
            String route,
            String route_id,
            String id,
            String seq_no,
            String customer_code,
            String customer_name,
            String customer_type,
            String location,
            String gps_latitude,
            String gps_longitude

    ) {

        this.sr_code = sr_code;
        this.jptype = jptype;
        this.route = route;
        this.route_id = route_id;
        this.id = id;
        this.seq_no = seq_no;
        this.customer_code = customer_code;
        this.customer_name = customer_name;
        this.customer_type = customer_type;
        this.location = location;
        this.gps_latitude = gps_latitude;
        this.gps_longitude = gps_longitude;


    }

    public String getSr_code() {
        return sr_code;
    }

    public String getJptype() {
        return jptype;
    }

    public String getRoute() {
        return route;
    }

    public String getRoute_id() {
        return route_id;
    }

    public String getId() {
        return id;
    }

    public String getSeq_no() {
        return seq_no;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public String getLocation() {
        return location;
    }

    public String getGps_latitude() {
        return gps_latitude;
    }

    public String getGps_longitude() {
        return gps_longitude;
    }


}

