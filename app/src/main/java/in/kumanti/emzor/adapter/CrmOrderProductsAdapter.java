package in.kumanti.emzor.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.ArtifactsActivity;
import in.kumanti.emzor.crm.CrmDetailsActivity;
import in.kumanti.emzor.model.CrmProducts;

public class CrmOrderProductsAdapter extends RecyclerView.Adapter<CrmOrderProductsAdapter.ViewHolder> {

    public ArrayList<CrmProducts> mDataset;
    public Context context;
    public DecimalFormat valueFormatter, quantityFormatter;
    double unformattedValue = 0.0;
    private String customerCode = "", productCode = "";
    private RecyclerViewItemListener mListener;
    private boolean isCrmOrdProdSaved;



    public CrmOrderProductsAdapter(ArrayList<CrmProducts> myDataset, String customerCodeVal, RecyclerViewItemListener listener, boolean isCrmOrdProdSavedVal) {
        mDataset = myDataset;
        customerCode = customerCodeVal;
        mListener = listener;
        isCrmOrdProdSaved = isCrmOrdProdSavedVal;
    }

    public void add(CrmProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CrmProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @NotNull
    @Override
    public CrmOrderProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_crm_order_products, parent, false);

        valueFormatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder viewHolder, final int position) {
        productCode = mDataset.get(position).getProductCode();

        viewHolder.tvCrmProductName.setText(mDataset.get(position).getProductName());
        viewHolder.tvCrmProductQuantity.setText(quantityFormatter.format(Double.parseDouble(mDataset.get(position).getQuantity())));

        String budgetStr = mDataset.get(position).getBudget() != null && !mDataset.get(position).getBudget().equals("") ?valueFormatter.format(Double.parseDouble(mDataset.get(position).getBudget())) : "0.0";
        String priceStr = mDataset.get(position).getUnitPrice() != null && !mDataset.get(position).getUnitPrice().equals("") ?valueFormatter.format(Double.parseDouble(mDataset.get(position).getUnitPrice())) : "0.0";
        String valueStr = mDataset.get(position).getValue() != null && !mDataset.get(position).getValue().equals("") ?valueFormatter.format(Double.parseDouble(mDataset.get(position).getValue())) : "0.0";

        viewHolder.tvCrmProductBudget.setText(budgetStr);
        viewHolder.tvCrmProductPrice.setText(priceStr);
        viewHolder.tvCrmProductValue.setText(valueStr);

//        viewHolder.tvCrmProductBudget.setText(valueFormatter.format(Double.parseDouble(mDataset.get(position).getBudget())));
//        viewHolder.tvCrmProductPrice.setText(valueFormatter.format((Double.parseDouble(mDataset.get(position).getUnitPrice()))));
//        viewHolder.tvCrmProductValue.setText(valueFormatter.format((Double.parseDouble(mDataset.get(position).getValue()))));

        if(isCrmOrdProdSaved) {
            viewHolder.crmOrderProdContainer.setEnabled(false);
        }

        viewHolder.crmOrderProdContainer.setOnClickListener(v -> {
            call_alert(v,position);

        });

    }

    private void call_alert(final View v, final int position) {
        productCode = mDataset.get(position).getProductCode();
        AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
        alertbox.setTitle("Update ?");
        alertbox.setPositiveButton("Edit", (dialog1, whichButton) -> {
            LayoutInflater inflater = LayoutInflater.from(context);
            View alertLayout = inflater.inflate(R.layout.activity_crm_update_product, null);

            TextView updateProName, updateProValue;
            EditText updateProQty, updateProBudget, updateProPrice;
            Button artifactsButton, detailsButton;
            ImageButton updateCrmProdButton;

            updateProName = alertLayout.findViewById(R.id.crmUpdateProdNameTv);
            updateProQty = alertLayout.findViewById(R.id.crmUpdateProdQtyEt);
            updateProBudget = alertLayout.findViewById(R.id.crmUpdateProdBudgetEt);
            updateProPrice = alertLayout.findViewById(R.id.crmUpdateProdPriceEt);
            updateProValue = alertLayout.findViewById(R.id.crmUpdateProdValueTv);

            artifactsButton = alertLayout.findViewById(R.id.updateCrmArtifactsBt);
            detailsButton = alertLayout.findViewById(R.id.updateCrmDetailsBt);

            updateCrmProdButton = alertLayout.findViewById(R.id.updateCrmProdIb);

            updateProName.setText(mDataset.get(position).getProductName());
            updateProQty.setText(mDataset.get(position).getQuantity());
            updateProBudget.setText(mDataset.get(position).getBudget());
            updateProPrice.setText(mDataset.get(position).getUnitPrice());
            updateProValue.setText(mDataset.get(position).getValue());

            updateProQty.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    unformattedValue = calculateProdValue(updateProQty,updateProPrice,updateProValue);

                }
            });

            updateProPrice.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    unformattedValue = calculateProdValue(updateProQty,updateProPrice,updateProValue);
                }
            });

            artifactsButton.setOnClickListener(v13 -> {
                Intent intent = new Intent(context, ArtifactsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("customer_id", customerCode);
                bundle.putString("productCode", productCode);

                intent.putExtras(bundle);
                context.startActivity(intent);
            });

            detailsButton.setOnClickListener(v12 -> {
                Intent notesIntent = new Intent(context, CrmDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("customer_id", customerCode);
                bundle.putString("productCode", productCode);
                notesIntent.putExtras(bundle);
                context.startActivity(notesIntent);
            });

            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setView(alertLayout);
            alert.setCancelable(false);

            alert.setNegativeButton("CLOSE", (dialog, which) -> {
            });

            AlertDialog dialog = alert.create();
            dialog.show();

            updateCrmProdButton.setOnClickListener(v1 -> {

                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                boolean isValid = true;

                if (TextUtils.isEmpty(updateProQty.getText().toString())) {
                    updateProQty.setError(context.getResources().getString(R.string.error_field_required));
                    isValid = false;
                } else if ("0".equals(updateProQty.getText().toString())) {
                    updateProQty.setError(context.getResources().getString(R.string.Zero_Validation));
                    isValid = false;
                }else{
                    updateProQty.setError(null);
                }
//                if (TextUtils.isEmpty(updateProBudget.getText().toString())) {
//                    updateProBudget.setError(context.getResources().getString(R.string.error_field_required));
//                    isValid = false;
//                } else if ("0".equals(updateProBudget.getText().toString())) {
//                    updateProBudget.setError(context.getResources().getString(R.string.Zero_Validation));
//                    isValid = false;
//                }else{
//                    updateProBudget.setError(null);
//                }
//                if (TextUtils.isEmpty(updateProPrice.getText().toString())) {
//                    updateProPrice.setError(context.getResources().getString(R.string.error_field_required));
//                    isValid = false;
//                } else if ("0".equals(updateProPrice.getText().toString())) {
//                    updateProPrice.setError(context.getResources().getString(R.string.Zero_Validation));
//                    isValid = false;
//                }else{
//                    updateProPrice.setError(null);
//                }

                if(isValid) {
                    mDataset.get(position).setProductName(updateProName.getText().toString());
                    mDataset.get(position).setQuantity(updateProQty.getText().toString());
                    mDataset.get(position).setBudget(updateProBudget.getText().toString());
                    mDataset.get(position).setUnitPrice(updateProPrice.getText().toString());

                    unformattedValue = calculateProdValue(updateProQty,updateProPrice,updateProValue);
                    mDataset.get(position).setValue(String.valueOf(unformattedValue));
                    notifyDataSetChanged();
                    mListener.onClick(v1, position);
                    dialog.dismiss();
                }



            });


        })

                .setNegativeButton("Delete", (dialog, which) -> {

                    AlertDialog.Builder alertbox1 = new AlertDialog.Builder(v.getRootView().getContext());
                    alertbox1.setTitle("Confirm Delete ?");
                    alertbox1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            //Toast.makeText(getContext(), id, Toast.LENGTH_LONG).show();
                            mDataset.remove(position);
                            notifyDataSetChanged();
                            mListener.onClick(v, position);

                        }

                    })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                }
                            });

                    alertbox1.show();

                });

        alertbox.show();

    }


    private double calculateProdValue(EditText crmProdQty, EditText crmProdPrice, TextView crmProdValue) {
        String prodQty = crmProdQty.getText().toString();
        String prodPrice = crmProdPrice.getText().toString();
        double unformattedValueDouble = 0;
        if(!prodQty.isEmpty() && !prodPrice.isEmpty() ) {
            int quantityInt = Integer.parseInt(prodQty);
            double priceDouble = Double.parseDouble(prodPrice);

            unformattedValueDouble = quantityInt * priceDouble;
            String formattedTotalValue = valueFormatter.format(unformattedValueDouble);

            crmProdValue.setText(formattedTotalValue);
        }
        else
        {
            unformattedValueDouble = 0.00;
            crmProdValue.setText("0.00");
        }
        return unformattedValueDouble;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCrmProductName, tvCrmProductQuantity, tvCrmProductBudget, tvCrmProductPrice, tvCrmProductValue;
        LinearLayout crmOrderProdContainer;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;

            crmOrderProdContainer = v.findViewById(R.id.crmOrderProdContainer);
            tvCrmProductName = v.findViewById(R.id.tvCrmProductName);
            tvCrmProductQuantity = v.findViewById(R.id.tvCrmProductQuantity);
            tvCrmProductBudget = v.findViewById(R.id.tvCrmProductBudget);
            tvCrmProductPrice = v.findViewById(R.id.tvCrmProductPrice);
            tvCrmProductValue = v.findViewById(R.id.tvCrmProductValue);
        }
    }




}