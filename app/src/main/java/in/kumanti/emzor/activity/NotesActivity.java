package in.kumanti.emzor.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.NotesAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.eloquent.NotesTable;
import in.kumanti.emzor.model.Notes;

public class NotesActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    private TextView actionbarTitle, marqueeText;

    public RecyclerView notesRecyclerView;
    RecyclerViewItemListener listener;
    private RecyclerView.Adapter notesAdapter;
    private RecyclerView.LayoutManager notesLayoutManager;

    public EditText notesText;
    public ImageButton saveNotesButton, cancelNotesButton;
    String login_id = "", checkin_time = "", customer_id = "", notesReferenceId = "", notesType = "CUSTOMER";
    FloatingActionButton fabIcon;

    String[] notesReferenceIdList, notesTypeList;

    NotesTable notesTable;
    ArrayList<Notes> notesArrayList;
    Notes note;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_notes);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText(R.string.notes_actionbar_title);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        notesRecyclerView.setHasFixedSize(true);
        notesLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        notesRecyclerView.setLayoutManager(notesLayoutManager);

        note = new Notes();

        setNotesData();

        fabIcon.setOnClickListener(view -> notesAdd(0, null));

        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);


        });


    }

    private void setNotesData() {
        try {
            listener = (view, position) -> notesAdd(notesArrayList.get(position).id, notesArrayList.get(position).notesText);

            if (notesType.equals("SALES_REP"))
                notesArrayList = notesTable.getNotesBySalesRep(login_id);
            else if (!TextUtils.isEmpty(notesReferenceId)) {
                notesArrayList = notesTable.getNotesByCustomer(customer_id, notesTypeList, notesReferenceIdList);
            } else
                notesArrayList = notesTable.getNotesByCustomer(customer_id, notesType);
            Log.d("CustomerData", "customer" + customer_id + " sdf " + notesArrayList.size());

            notesAdapter = new NotesAdapter(notesArrayList, listener);
            notesRecyclerView.setAdapter(notesAdapter);
        } catch (Exception e) {
            Log.d("SurveyResponse", "" + e.getMessage());
        }
    }

    public void notesAdd(int noteId, String noteData) {
        View editingNotesToolView = getLayoutInflater().inflate(R.layout.notes_layout, null, false);

        notesText = editingNotesToolView.findViewById(R.id.notesEditText);
        saveNotesButton = editingNotesToolView.findViewById(R.id.saveNotesButton);
        cancelNotesButton = editingNotesToolView.findViewById(R.id.cancelNotesButton);

        if (noteId == 0)
            notesText.setText("");
        else
            notesText.setText(noteData);
        Log.d("Notes", "ShowNotesEditor: ");

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setView(editingNotesToolView);
        final android.app.AlertDialog alert = builder.create();
        Log.d("Notes", "ShowNotesEditor: " + alert.toString());
        alert.show();

        saveNotesButton.setOnClickListener(v -> {
            hideSoftKeyboard(getApplicationContext(), v);

            if (TextUtils.isEmpty(notesText.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Please add the notes Text", Toast.LENGTH_LONG).show();
                return;
            }

            note.setId(noteId);
            note.setSales_rep_id(login_id);
            note.setCustomer_id(customer_id);
            note.setNotes_data(notesText.getText().toString());
            note.setNotes_type(notesType);

            if (!TextUtils.isEmpty(notesReferenceId))
                note.setNotes_reference_no(notesReferenceId);
            String visitSequence = sharedPreferences.getString("VisitSequence", "");
            note.setVisitSeqNumber(visitSequence);

            if (noteId == 0)
                notesTable.create(note);
            else
                notesTable.update(note);
            alert.dismiss();

            setNotesData();
        });


        cancelNotesButton.setOnClickListener(v -> {
            hideSoftKeyboard(getApplicationContext(), v);

            final Dialog alertBox = new Dialog(v.getRootView().getContext());

            LayoutInflater mInflater = (LayoutInflater) getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            final View alertLayout = mInflater.inflate(R.layout.activity_exit_notes_popup, null);

            alertBox.setCancelable(false);
            alertBox.setContentView(alertLayout);

            final Button Yes = alertLayout.findViewById(R.id.Yes);
            final Button No = alertLayout.findViewById(R.id.No);

            Yes.setOnClickListener(v1 -> {
                alertBox.dismiss();
                alert.cancel();
            });

            No.setOnClickListener(v12 -> alertBox.dismiss());

            alertBox.show();

        });
    }


    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        fabIcon = findViewById(R.id.createNotesFab);

        notesTable = new NotesTable(getApplicationContext());
        notesRecyclerView = findViewById(R.id.notesRecyclerView);
    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            notesType = bundle.getString("notesType");

            if (bundle.containsKey("notesReferenceId")) {
                notesReferenceId = bundle.getString("notesReferenceId");
                notesReferenceIdList = bundle.getStringArray("notesReferenceIdList");
                notesTypeList = bundle.getStringArray("notesTypeList");
            }
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException ignored) {
                }
            }
        };
        t.start();

    }


    @Override
    protected void onResume() {
        Log.d("Notes ", "onResume ");

        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {

        super.onRestart();
    }

    @Override
    public void onBackPressed() {

    }
}
