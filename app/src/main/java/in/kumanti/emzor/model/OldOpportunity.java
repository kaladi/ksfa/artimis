package in.kumanti.emzor.model;

public class OldOpportunity {

    public int id;
    public String customer_id;
    public String opportunity_number;
    public String opportunity_notes;
    public String opportunity_date;
    public String enquiry_number;
    public String enquiry_date;
    public String enquiry_notes;
    public String quotation_number;
    public String quotation_date;
    public String quotation_notes;
    public String order_number;
    public String order_date;
    public String order_notes;
    public String design_attachment_type;
    public String enquiry_attachment_type;
    public String custom_attachment_type;
    public String demo_date;
    public String inspection_date;
    public String delivery_date;
    public String installation_date;
    public String training_date;
    public String demo_address;
    public String inspection_address;
    public String delivery_address;
    public String installation_address;
    public String training_address;
    public String created_at;
    public String updated_at;

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getOrder_notes() {
        return order_notes;
    }

    public void setOrder_notes(String order_notes) {
        this.order_notes = order_notes;
    }

    public String getEnquiry_number() {
        return enquiry_number;
    }

    public void setEnquiry_number(String enquiry_number) {
        this.enquiry_number = enquiry_number;
    }

    public String getEnquiry_notes() {
        return enquiry_notes;
    }

    public void setEnquiry_notes(String enquiry_notes) {
        this.enquiry_notes = enquiry_notes;
    }

    public String getEnquiry_date() {
        return enquiry_date;
    }

    public void setEnquiry_date(String enquiry_date) {
        this.enquiry_date = enquiry_date;
    }

    public String getQuotation_number() {
        return quotation_number;
    }

    public void setQuotation_number(String quotation_number) {
        this.quotation_number = quotation_number;
    }

    public String getQuotation_notes() {
        return quotation_notes;
    }

    public void setQuotation_notes(String quotation_notes) {
        this.quotation_notes = quotation_notes;
    }

    public String getQuotation_date() {
        return quotation_date;
    }

    public void setQuotation_date(String quotation_date) {
        this.quotation_date = quotation_date;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getDemo_date() {
        return demo_date;
    }

    public void setDemo_date(String demo_date) {
        this.demo_date = demo_date;
    }

    public String getInspection_date() {
        return inspection_date;
    }

    public void setInspection_date(String inspection_date) {
        this.inspection_date = inspection_date;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getInstallation_date() {
        return installation_date;
    }

    public void setInstallation_date(String installation_date) {
        this.installation_date = installation_date;
    }

    public String getDesign_attachment_type() {
        return design_attachment_type;
    }

    public void setDesign_attachment_type(String design_attachment_type) {
        this.design_attachment_type = design_attachment_type;
    }

    public String getEnquiry_attachment_type() {
        return enquiry_attachment_type;
    }

    public void setEnquiry_attachment_type(String enquiry_attachment_type) {
        this.enquiry_attachment_type = enquiry_attachment_type;
    }

    public String getCustom_attachment_type() {
        return custom_attachment_type;
    }

    public void setCustom_attachment_type(String custom_attachment_type) {
        this.custom_attachment_type = custom_attachment_type;
    }

    public String getTraining_date() {
        return training_date;
    }

    public void setTraining_date(String training_date) {
        this.training_date = training_date;
    }

    public String getDemo_address() {
        return demo_address;
    }

    public void setDemo_address(String demo_address) {
        this.demo_address = demo_address;
    }

    public String getInspection_address() {
        return inspection_address;
    }

    public void setInspection_address(String inspection_address) {
        this.inspection_address = inspection_address;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getInstallation_address() {
        return installation_address;
    }

    public void setInstallation_address(String installation_address) {
        this.installation_address = installation_address;
    }

    public String getTraining_address() {
        return training_address;
    }

    public void setTraining_address(String training_address) {
        this.training_address = training_address;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }


    public String getOpportunity_notes() {
        return opportunity_notes;
    }

    public void setOpportunity_notes(String opportunity_notes) {
        this.opportunity_notes = opportunity_notes;
    }


    public String getOpportunity_number() {
        return opportunity_number;
    }

    public void setOpportunity_number(String opportunity_number) {
        this.opportunity_number = opportunity_number;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpportunity_date() {
        return opportunity_date;
    }

    public void setOpportunity_date(String opportunity_date) {
        this.opportunity_date = opportunity_date;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


}
