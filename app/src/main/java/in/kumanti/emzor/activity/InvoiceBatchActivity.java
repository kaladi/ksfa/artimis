package in.kumanti.emzor.activity;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.InvoiceBatch;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;


public class InvoiceBatchActivity extends MainActivity {

    ListView batch_list;
    String product_name, product_uom, customer_id, product_id, login_id;
    ArrayList<InvoiceBatch> productbatchLists;
    InvoiceBatch batchlist;
    int invoice_id = 0;
    int reciept_id = 0;
    int rec_num = 0;
    String salesorder_date;
    int remaining_qty = 0;
    int allocate_qty = 0;
    int order_quantity = 0;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng;
    ImageView actionbarBackButton, deviceInfo;
    MyDBHandler dbHandler;
    private TextView marqueeText, actionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_invoice_batch);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText("View Batch");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        final TextView prod_batch = findViewById(R.id.txt_product);
        final TextView uom_batch = findViewById(R.id.txt_uom);
        final TextView qty_batch = findViewById(R.id.txt_quantity);
        batch_list = findViewById(R.id.batch_list);

        final ImageView Yes = findViewById(R.id.back);
        final ImageView No = findViewById(R.id.No);

        long date = System.currentTimeMillis();
        SimpleDateFormat sdfd = new SimpleDateFormat("ddMMMyyyy");
        SimpleDateFormat sdft = new SimpleDateFormat("hh:mm");
        salesorder_date = sdfd.format(date);
        getlocation();
        //uom_batch.setFocusableInTouchMode(false);


        prod_batch.setText(product_name);
        uom_batch.setText(product_uom);
        qty_batch.setText(String.valueOf(order_quantity));
        //Toast.makeText(getApplicationContext(), "QTY" + order_quantity , Toast.LENGTH_LONG).show();

        loadproductbatchlist();
        loadproductbatchlist1();


        Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dbHandler.deletebatch(product_id);
                finish();

               /* uom_batch.setEnabled(true);
                uom_batch.requestFocus();
                uom_batch.setEnabled(false);

                int batch_value = dbHandler.get_batch_quantity(String.valueOf(invoice_id), product_id);
                //Toast.makeText(getApplicationContext(), "QTY" + order_quantity + "Batch" + batch_value, Toast.LENGTH_LONG).show();

                if (Integer.parseInt(order_quantity)
                        == batch_value) {
                    dbHandler.updatebatchstatus(invoice_id, "Inprocess");
                    finish();

                } else {
                    // Toast.makeText(getApplicationContext(), "Entered Quantity is not matched with Original Quantity", Toast.LENGTH_LONG).show();
                    android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    alertbox.setMessage("Invoiced Quantity doesn't matched with Batch Quantity");
                    alertbox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {

                            dialog.dismiss();

                        }

                    });

                    alertbox.show();
                } */
            }
        });

        No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHandler.deletebatch(product_id);
                finish();
            }
        });

    }

    private void setFooterDateTime() {

    }

    private void populateHeaderDetails() {

        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            product_name = bundle.getString("product");
            product_uom = bundle.getString("uom");
            order_quantity = bundle.getInt("quantity");
            login_id = bundle.getString("login_id");
            customer_id = bundle.getString("customer_id");
            product_id = bundle.getString("product_id");
            invoice_id = bundle.getInt("invoice_id");

        }

    }

    private void initializeViews() {
        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);
        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

    }

    private void getlocation() {


        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            String lat = Double.toString(latitude);
            String lng = Double.toString(longitude);

            lat_lng = lat + "," + lng;

        } else {

            gpsTracker.showSettingsAlert();
        }
    }

    private void loadproductbatchlist() {

        Cursor data = dbHandler.getProductBatchDetails2(product_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
        } else {
            int i = 0;
            allocate_qty = order_quantity;
            while (data.moveToNext()) {

                int onhand_qty = data.getInt(2);
                if (onhand_qty >= allocate_qty) {
                    // remaining_qty=onhand_qty-allocate_qty;

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(allocate_qty), data.getString(0), String.valueOf(invoice_id));
                    //dbHandler.updatebatchqty(String.valueOf(invoice_id), product_id, data.getString(1));

                    break;

                } else {

                    remaining_qty = allocate_qty - onhand_qty;
                    Log.i("remaining_qty", String.valueOf(remaining_qty));


                    int test = allocate_qty;
                    if (allocate_qty <= onhand_qty) {
                        test = allocate_qty;
                    } else {
                        test = onhand_qty;
                    }

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(test), data.getString(0), String.valueOf(invoice_id));
                    allocate_qty = remaining_qty;
                }
                i++;
            }

        }
    }


    private void loadproductbatchlist1() {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails2(product_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
            batch_list.setAdapter(null);
        } else {
            int i = 0;
            while (data.moveToNext()) {
               /* String inputDateStr = data.getString(1);

                DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");
                DateFormat outputFormat = new SimpleDateFormat("ddMMMyyyy");
                Date date = null;
                try {
                    date = inputFormat.parse(inputDateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputDateStr = outputFormat.format(date); */

                batchlist = new InvoiceBatch(
                        data.getString(1),
                        data.getString(0),
                        data.getInt(2),
                        data.getInt(3));
                productbatchLists.add(i, batchlist);


                i++;
            }

            InvoiceBatchAdapter adapter = new InvoiceBatchAdapter(getApplicationContext(), R.layout.list_invoice_batch, productbatchLists);
            batch_list.setAdapter(adapter);

        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    public class InvoiceBatchAdapter extends ArrayAdapter<InvoiceBatch> {

        Activity mActivity = null;
        android.support.v7.app.AlertDialog alertDialog1;
        Globals globals;
        MyDBHandler dbHandler;
        ViewHolder viewHolder = null;
        private LayoutInflater mInflater;
        private ArrayList<InvoiceBatch> productbatchLists;
        private int mViewResourceId;
        private Context c;


        public InvoiceBatchAdapter(Context context, int textViewResourceId, ArrayList<InvoiceBatch> productbatchLists) {
            super(context, textViewResourceId, productbatchLists);
            this.productbatchLists = productbatchLists;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            dbHandler = new MyDBHandler(c, null, null, 1);
            viewHolder = null;

        }

        public int getCount() {
            return productbatchLists.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public View getView(final int position, View convertView, ViewGroup parent) {


            convertView = mInflater.inflate(mViewResourceId, null);
            final InvoiceBatch user = productbatchLists.get(position);
            final int pos = position;

            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.expirydate = convertView.findViewById(R.id.batch_expirydate);
                viewHolder.batchnumber = convertView.findViewById(R.id.batch_batchnumber);
                viewHolder.quantity = convertView.findViewById(R.id.batch_quantity);
                viewHolder.invoices = convertView.findViewById(R.id.batch_invoices);


                if (viewHolder.expirydate != null) {
                    viewHolder.expirydate.setText(user.getBatchexpirydate());
                }
                if (viewHolder.batchnumber != null) {
                    viewHolder.batchnumber.setText((user.getBatchnumber()));
                }

                if (viewHolder.quantity != null) {
                    viewHolder.quantity.setText((String.valueOf(user.getBatchquantity())));
                } else {
                    viewHolder.quantity.setText("0");
                }
                if (viewHolder.invoices != null) {
                    viewHolder.invoices.setText((String.valueOf(user.getBatchinvoice())));
                } else {
                    viewHolder.invoices.setText("");
                }

                //   viewHolder.invoices.setText(productbatchLists.get(position).getBatchinvoice());
                // viewHolder.invoices.setId(position);

                //we need to update adapter once we finish with editing


            /*  viewHolder.invoices.addTextChangedListener(new TextWatcher() {

                    public void afterTextChanged(Editable s) {
                    }

                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {

                        final int position = v.getId();
                        final EditText Caption = (EditText) v;
                        String batchnumber=user.getBatchnumber();

                        dbHandler.updateInvoiceBatch(product_id,Caption.getText().toString(),batchnumber,invoice_id);
                    }

                        //dbHandler.updateInvoiceBatch(product_id,"9999");
                      /*  if (viewHolder.invoices.getText().toString().equals("")) {

                            Toast.makeText(getApplicationContext(), "Inside Null", Toast.LENGTH_LONG).show();

                        } else {

                            Integer quan = Integer.parseInt(viewHolder.invoices.getText().toString());
                            String invoice_val=String.valueOf(Integer.valueOf(quan));
                            Toast.makeText(getApplicationContext(), quan, Toast.LENGTH_LONG).show();



                            //dbHandler.updateInvoiceBatch(product_id,invoice_val);
                        }
                    }
                }); */


            }

            return convertView;
        }


        public class ViewHolder {
            TextView expirydate, batchnumber, quantity, invoices;
            EditText dummy;

        }

        class ListItem {
            String caption;
        }


    }
}
