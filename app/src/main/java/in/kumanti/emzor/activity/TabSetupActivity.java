package in.kumanti.emzor.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.R;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.mastersData.CompanyMaster;
import in.kumanti.emzor.mastersData.JSONResponse;
import in.kumanti.emzor.mastersData.SRMaster;
import in.kumanti.emzor.mastersData.SetupValidations;
import in.kumanti.emzor.mastersData.TabMaster;
import in.kumanti.emzor.utils.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.utils.Constants.IMAGE_HOME_PATH;


public class TabSetupActivity extends MainActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;
    ImageView actionbarBackButton, deviceInfo;
    MyDBHandler dbHandler;
    String otp, tabid, userid;
    String latestotp, finalotp;
    EditText txt_otp;
    Button confirm_btn;
    Button setup_button;
    ImageView info, btn_back;
    TextView user_id, tab_id;
    TextView actionbarTitle;
    TelephonyManager telephonyManager;
    ApiInterface api;
    String imel_no;
    int tabrowCount = 0;
    String login_id = "";
    private TextView marqueeText;
    private ArrayList<TabMaster> tabMastersdata;
    private ArrayList<CompanyMaster> companyMastersdata;
    private ArrayList<SRMaster> srMastersdata;

    public SharedPreferences sharedPreferences;
    public static final String USER_KEY = "User";
    public static final String PREF_NAME = "prefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_tab_setup);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();


        actionbarTitle.setText("Tab Setup");
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        if (!checkPermission()) {

            requestPermission();

        } else {


        }

        actionbarTitle.setText("Tab Setup");


        getTabDetails();

        confirm_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (tabrowCount == 0) {

                    setup_tab_confirm();

                } else {
                    update_setup_tab_confirm();
//                    Toast.makeText(getApplicationContext(), "Tab Setup is Already Done", Toast.LENGTH_SHORT).show();
                }


            }
        });


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

    }

    private void getTabDetails() {
        Cursor tabDetails = dbHandler.getTabDeatils();
        tabrowCount = tabDetails.getCount();

        if (tabrowCount == 0) {
            setup_tab();
        } else {
            int i = 0;
            while (tabDetails.moveToNext()) {

                tab_id.setText(tabDetails.getString(0));
                user_id.setText(tabDetails.getString(1));

            }
        }
    }

    private void populateHeaderDetails() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            login_id = (String) bd.get("login_id");
        }

    }

    private void initializeViews() {
        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        tab_id = findViewById(R.id.tab_id);
        user_id = findViewById(R.id.user_id);
        txt_otp = findViewById(R.id.txt_otp);
        confirm_btn = findViewById(R.id.confirm_btn);
        setup_button = findViewById(R.id.setup_button);

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

    /*    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.O){
            // Do something for lollipop and above versions
        } else{
            // do something for phones running an SDK before lollipop
        }
        imel_no = telephonyManager.getDeviceId();*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            imel_no = Constants.IMEI_NUMBER;
        }else { imel_no = telephonyManager.getDeviceId();}


        actionbarTitle = findViewById(R.id.toolbar_title);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        tab_id = findViewById(R.id.tab_id);
        user_id = findViewById(R.id.user_id);
        txt_otp = findViewById(R.id.txt_otp);
        confirm_btn = findViewById(R.id.confirm_btn);
        setup_button = findViewById(R.id.setup_button);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        sharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    private void setup_tab_confirm() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(50000, TimeUnit.SECONDS).readTimeout(50000, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit.create(ApiInterface.class);

        Call<SetupValidations> tabConfirmation = api.getTabConfirmation(imel_no);
        tabConfirmation.enqueue(new Callback<SetupValidations>() {

            @Override
            public void onResponse(Call<SetupValidations> tabConfirmation, retrofit2.Response<SetupValidations> response) {

                SetupValidations mLoginObject = response.body();
                String returnedResponse = mLoginObject.status;

                if ("1".equals(returnedResponse) && (tabMastersdata != null)) {
                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < tabMastersdata.size(); i++) {
                        //heroes[i] = heroList.get(i).getCustomer_id();

                        long id2 = dbHandler.create_tab_setup

                                (
                                        tabMastersdata.get(i).getTab_code(),
                                        tabMastersdata.get(i).getSr_code(),
                                        tabMastersdata.get(i).getTab_seq_prefix(),
                                        tabMastersdata.get(i).getWebsite_url(),
                                        tabMastersdata.get(i).getSec_price_list()

                                );

                    }
                    dbHandler.updateIMEINo(imel_no);
                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < companyMastersdata.size(); i++) {

                        new AsyncTaskLoadImage().execute(companyMastersdata.get(i).getImageUrl(), companyMastersdata.get(i).getImageName());

                        //heroes[i] = heroList.get(i).getCustomer_id();

                        long id2 = dbHandler.create_company_master

                                (
                                        companyMastersdata.get(i).getCompany_name(),
                                        companyMastersdata.get(i).getCompany_code(),
                                        companyMastersdata.get(i).getContact_person(),
                                        companyMastersdata.get(i).getEmail(),
                                        companyMastersdata.get(i).getMobile_number(),
                                        companyMastersdata.get(i).getSupport_num1(),
                                        companyMastersdata.get(i).getSupport_num2(),
                                        companyMastersdata.get(i).getSupport_email(),
                                        companyMastersdata.get(i).getAddress1(),
                                        companyMastersdata.get(i).getAddress2(),
                                        companyMastersdata.get(i).getAddress3(),
                                        companyMastersdata.get(i).getImageName(),
                                        companyMastersdata.get(i).getImageUrl()


                                );
                    }

                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < srMastersdata.size(); i++) {
                        //heroes[i] = heroList.get(i).getCustomer_id();

                        new AsyncTaskLoadSrImage().execute(srMastersdata.get(i).getImageUrl(), srMastersdata.get(i).getImageName());


                        long id2 = dbHandler.create_salesrep_details

                                (
                                        srMastersdata.get(i).getSr_code(),
                                        srMastersdata.get(i).getSr_name(),
                                        srMastersdata.get(i).getManager_name(),
                                        srMastersdata.get(i).getRegion()
                                );

                        long id3 = dbHandler.create_user_setup

                                (
                                        srMastersdata.get(i).getSr_name(),
                                        null,
                                        srMastersdata.get(i).getEmail(),
                                        tabMastersdata.get(i).getTab_password(),
                                        tabMastersdata.get(i).getTab_password(),
                                        srMastersdata.get(i).getMobile(),
                                        srMastersdata.get(i).getSr_code(),
                                        null,
                                        srMastersdata.get(i).getImageName(),
                                        srMastersdata.get(i).getImageUrl()


                                );
                    }
                    Toast.makeText(getApplicationContext(), "Tab Setup is Done", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Your Tab is Not yet Configured", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<SetupValidations> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void update_setup_tab_confirm() {

        Gson gson1 = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson1)) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit1.create(ApiInterface.class);

        Call<JSONResponse> tabMaster = api.getSetupDetails(imel_no);
        tabMaster.enqueue(new Callback<JSONResponse>() {

            @Override
            public void onResponse(Call<JSONResponse> tabMaster, retrofit2.Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();

                if (jsonResponse.getTab_master() != null) {
                    tabMastersdata = new ArrayList<>(Arrays.asList(jsonResponse.getTab_master()));
                    companyMastersdata = new ArrayList<>(Arrays.asList(jsonResponse.getCompanyMasters()));
                    srMastersdata = new ArrayList<>(Arrays.asList(jsonResponse.getSr_master()));
                    update_master_setup();

                } else {
                    Toast.makeText(getApplicationContext(), "Your Tab is Not yet Configured", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    void update_master_setup(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(50000, TimeUnit.SECONDS).readTimeout(50000, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit.create(ApiInterface.class);

        Call<SetupValidations> tabConfirmation = api.getTabConfirmation(imel_no);
        tabConfirmation.enqueue(new Callback<SetupValidations>() {

            @Override
            public void onResponse(Call<SetupValidations> tabConfirmation, retrofit2.Response<SetupValidations> response) {

                SetupValidations mLoginObject = response.body();
                String returnedResponse = mLoginObject.status;

                if ("1".equals(returnedResponse) && (tabMastersdata != null)) {
                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < tabMastersdata.size(); i++) {
                        //heroes[i] = heroList.get(i).getCustomer_id();

                        dbHandler.update_tab_setup1

                                (
                                        tabMastersdata.get(i).getTab_code(),
                                        tabMastersdata.get(i).getSr_code(),
                                        tabMastersdata.get(i).getTab_seq_prefix(),
                                        tabMastersdata.get(i).getWebsite_url(),
                                        tabMastersdata.get(i).getSec_price_list()

                                );

                    }
                    dbHandler.updateIMEINo(imel_no);

                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < companyMastersdata.size(); i++) {

                        new AsyncTaskLoadImage().execute(companyMastersdata.get(i).getImageUrl(), companyMastersdata.get(i).getImageName());

                        //heroes[i] = heroList.get(i).getCustomer_id();

                        dbHandler.update_Company_master

                                (
                                        companyMastersdata.get(i).getCompany_name(),
                                        companyMastersdata.get(i).getCompany_code(),
                                        companyMastersdata.get(i).getContact_person(),
                                        companyMastersdata.get(i).getEmail(),
                                        companyMastersdata.get(i).getMobile_number(),
                                        companyMastersdata.get(i).getSupport_num1(),
                                        companyMastersdata.get(i).getSupport_num2(),
                                        companyMastersdata.get(i).getSupport_email(),
                                        companyMastersdata.get(i).getAddress1(),
                                        companyMastersdata.get(i).getAddress2(),
                                        companyMastersdata.get(i).getAddress3(),
                                        companyMastersdata.get(i).getImageName(),
                                        companyMastersdata.get(i).getImageUrl()


                                );
                    }

                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < srMastersdata.size(); i++) {
                        //heroes[i] = heroList.get(i).getCustomer_id();

                        new AsyncTaskLoadSrImage().execute(srMastersdata.get(i).getImageUrl(), srMastersdata.get(i).getImageName());


                        dbHandler.update_salesrep_details1

                                (
                                        srMastersdata.get(i).getSr_code(),
                                        srMastersdata.get(i).getSr_name(),
                                        srMastersdata.get(i).getManager_name(),
                                        srMastersdata.get(i).getRegion()
                                );

                        dbHandler.update_user_setup1

                                (
                                        srMastersdata.get(i).getSr_name(),
                                        null,
                                        srMastersdata.get(i).getEmail(),
                                        srMastersdata.get(i).getSr_code(),
                                        null,
                                        srMastersdata.get(i).getImageName(),
                                        srMastersdata.get(i).getImageUrl()


                                );
                        editor.putString(USER_KEY, "");
                        editor.commit();
                    }


                }

            }

            @Override
            public void onFailure(Call<SetupValidations> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
        Toast.makeText(getApplicationContext(), "Tab Setup is Already Done", Toast.LENGTH_SHORT).show();
    }

    private void setup_tab() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit.create(ApiInterface.class);

        Call<JSONResponse> tabMaster = api.getSetupDetails(imel_no);
        tabMaster.enqueue(new Callback<JSONResponse>() {

            @Override
            public void onResponse(Call<JSONResponse> tabMaster, retrofit2.Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();

                if (jsonResponse.getTab_master() != null) {
                    tabMastersdata = new ArrayList<>(Arrays.asList(jsonResponse.getTab_master()));
                    companyMastersdata = new ArrayList<>(Arrays.asList(jsonResponse.getCompanyMasters()));
                    srMastersdata = new ArrayList<>(Arrays.asList(jsonResponse.getSr_master()));


                    if (tabMastersdata != null) {

                        //looping through all the heroes and inserting the names inside the string array
                        for (int i = 0; i < tabMastersdata.size(); i++) {

                            tab_id.setText(tabMastersdata.get(i).getTab_code());
                            user_id.setText(tabMastersdata.get(i).getSr_code());

                        }
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Your Tab is Not yet Configured", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void verifyOTP() {

        final String otp = txt_otp.getText().toString().trim();
        View focusView = null;
        boolean cancel = false;
        if (TextUtils.isEmpty(otp)) {
            txt_otp.setError(getString(R.string.error_field_required));
            focusView = txt_otp;
            cancel = true;
        }
        latestotp = dbHandler.getlatestOTP(tabid, userid);
        Log.i("latestotp", latestotp);
        finalotp = txt_otp.getText().toString();
        if (latestotp == "0") {
            Toast.makeText(this.getApplicationContext(), "No Records", Toast.LENGTH_LONG).show();

        } else if (latestotp.equals(finalotp)) {

            Toast.makeText(this.getApplicationContext(), "Verified Successfully", Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(this.getApplicationContext(), "Verification Failed", Toast.LENGTH_LONG).show();
        }
    }


    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();


    }

    public void generatePIN() {

        //generate a 4 digit integer 1000 <10000
        int randomPIN = (int) (Math.random() * 9000) + 1000;

        //Store integer in a string
        otp = Integer.toString(randomPIN);
        Log.i("otp", otp);
    }

    private void sendSMSMessage() {
        Log.i("Send SMS", "");

        String phoneNo = "+919176112255";
        //String phoneNo = txtphoneNo.getText().toString();
        String message = "Your OTP is  " + otp + "Kindly do not share your PIN";

        try {
            if (message.equals("")) {
                Toast.makeText(getApplicationContext(), "No OTP Found", Toast.LENGTH_LONG).show();
            } else {

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNo, null, message, null, null);
                confirm_btn.setEnabled(true);
                Toast.makeText(getApplicationContext(), "OTP  Sent!", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "SMS failed, please try again..!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.INTERNET);
        int result4 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {

        ActivityCompat.requestPermissions(TabSetupActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    public class AsyncTaskLoadImage extends AsyncTask<String, String, Bitmap> {
        private final static String TAG = "AsyncTaskLoadImage";
        private ImageView imageView;

        public AsyncTaskLoadImage() {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            Bitmap bitmap = null;
            try {
                URL url = new URL(params[0]);
                bitmap = BitmapFactory.decodeStream((InputStream) url.getContent());
                File filepath = Environment.getExternalStorageDirectory();
                String folder_main = IMAGE_HOME_PATH + "images" + File.separator + "companyImages" + File.separator;

                File dir = new File(folder_main, "");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
               /* File dir = new File(filepath.getAbsolutePath()
                        + "/mSalesCustomerImages/");
                dir.mkdirs(); */
                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = params[1];
                File file = new File(dir, fname);
                Log.i(TAG, "" + file);
                if (file.exists())
                    file.delete();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

        }
    }

    public class AsyncTaskLoadSrImage extends AsyncTask<String, String, Bitmap> {
        private final static String TAG = "AsyncTaskLoadImage";
        private ImageView imageView;

        public AsyncTaskLoadSrImage() {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            Bitmap bitmap = null;
            try {
                URL url = new URL(params[0]);
                bitmap = BitmapFactory.decodeStream((InputStream) url.getContent());
                File filepath = Environment.getExternalStorageDirectory();
                String folder_main = IMAGE_HOME_PATH + "images" + File.separator + "bdeImages" + File.separator;

                File dir = new File(folder_main, "");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
               /* File dir = new File(filepath.getAbsolutePath()
                        + "/mSalesCustomerImages/");
                dir.mkdirs(); */
                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = params[1];
                File file = new File(dir, fname);
                Log.i(TAG, "" + file);
                if (file.exists())
                    file.delete();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

        }
    }


}
