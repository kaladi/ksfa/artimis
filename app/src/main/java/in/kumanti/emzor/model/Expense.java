package in.kumanti.emzor.model;


import java.util.Comparator;

public class Expense {
    public String companyCode;
    public String tabCode;
    public String bdeCode;
    public String bdeName;
    private int expenseId;
    private  String expenseNumber;
    public String expenseDate;
    public String expenseType;
    public String expenseMileage;
    public String expenseVoucherNo;
    public String expenseVoucherDate;
    public String expenseVendor;
    public String expenseVendorType;
    public String expenseCurrency;
    public String expenseAmount;
    public String expenseRemarks;
    public String expenseSignature;
    public String expensePostDate;
    public String expensePostTime;
    public String expenseApprovedAmount;
    public String expenseStatus;
    public String expenseReason;
    public int expenseRandomNumber;
    public String tripNumber;
    public String tripStartDate;
    public String tripStartTime;
    public String tripEndDate;
    public String tripEndTime;
    public String signatureFilePath;
    private String signatureFileName;
    public boolean isSync;
    public String createdAt;
    public String updatedAt;



    public static Comparator<Expense> expenseAsc = (fruit1, fruit2) ->
            Integer.parseInt(fruit1.getExpenseAmount()) - Integer.parseInt(fruit2.getExpenseAmount());

    public static Comparator<Expense> expenseDes = (fruit1, fruit2) ->
            Integer.parseInt(fruit2.getExpenseAmount()) - Integer.parseInt(fruit1.getExpenseAmount());

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }


    public String getBdeCode() {
        return bdeCode;
    }

    public void setBdeCode(String bdeCode) {
        this.bdeCode = bdeCode;
    }

    public String getSignatureFileName() {
        return signatureFileName;
    }

    public void setSignatureFileName(String signatureFileName) {
        this.signatureFileName = signatureFileName;
    }

    public String getSignatureFilePath() {
        return signatureFilePath;
    }

    public void setSignatureFilePath(String signatureFilePath) {
        this.signatureFilePath = signatureFilePath;
    }

    public String getBdeName() {
        return bdeName;
    }

    public void setBdeName(String bdeName) {
        this.bdeName = bdeName;
    }

    public int getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(int expenseId) {
        this.expenseId = expenseId;
    }
    public String getExpenseNumber() {
        return expenseNumber;
    }

    public void setExpenseNumber(String expenseNumber) {
        this.expenseNumber = expenseNumber;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseMileage() {
        return expenseMileage;
    }

    public void setExpenseMileage(String expenseMileage) {
        this.expenseMileage = expenseMileage;
    }

    public String getExpenseVoucherNo() {
        return expenseVoucherNo;
    }

    public void setExpenseVoucherNo(String expenseVoucherNo) {
        this.expenseVoucherNo = expenseVoucherNo;
    }

    public String getExpenseVoucherDate() {
        return expenseVoucherDate;
    }

    public void setExpenseVoucherDate(String expenseVoucherDate) {
        this.expenseVoucherDate = expenseVoucherDate;
    }

    public String getExpenseVendor() {
        return expenseVendor;
    }

    public void setExpenseVendor(String expenseVendor) {
        this.expenseVendor = expenseVendor;
    }

    public String getExpenseVendorType() {
        return expenseVendorType;
    }

    public void setExpenseVendorType(String expenseVendorType) {
        this.expenseVendorType = expenseVendorType;
    }


    public String getExpenseCurrency() {
        return expenseCurrency;
    }

    public void setExpenseCurrency(String expenseCurrency) {
        this.expenseCurrency = expenseCurrency;
    }

    public String getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(String expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getExpenseSignature() {
        return expenseSignature;
    }

    public void setExpenseSignature(String expenseSignature) {
        this.expenseSignature = expenseSignature;
    }

    public String getExpensePostDate() {
        return expensePostDate;
    }

    public void setExpensePostDate(String expensePostDate) {
        this.expensePostDate = expensePostDate;
    }

    public String getExpensePostTime() {
        return expensePostTime;
    }

    public void setExpensePostTime(String expensePostTime) {
        this.expensePostTime = expensePostTime;
    }

    public String getExpenseApprovedAmount() {
        return expenseApprovedAmount;
    }

    public void setExpenseApprovedAmount(String expenseApprovedAmount) {
        this.expenseApprovedAmount = expenseApprovedAmount;
    }

    public String getExpenseStatus() {
        return expenseStatus;
    }

    public void setExpenseStatus(String expenseStatus) {
        this.expenseStatus = expenseStatus;
    }

    public int getExpenseRandomNum() {
        return expenseRandomNumber;
    }

    public void setExpenseRandomNumber(int expenseRandomNumber) {
        this.expenseRandomNumber = expenseRandomNumber;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getExpenseReason() {
        return expenseReason;
    }

    public void setExpenseReason(String expenseReason) {
        this.expenseReason = expenseReason;
    }

    public String getTripNumber() {
        return tripNumber;
    }

    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
    }


    public String getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(String tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    @Override
    public String toString() {
        return expenseAmount;
    }

}