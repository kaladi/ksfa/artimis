package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.Expense;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ViewHolder> {

    public ArrayList<Expense> mDataset;
    public Context context;
    DecimalFormat decimalFormat;
    double  priceDouble;
    String  formatPrice;
    private RecyclerViewItemListener mListener;

    public ExpenseAdapter(ArrayList<Expense> myDataset) {
        mDataset = myDataset;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView expenseNumber,expenseDate, expenseType, expenseMileage, expenseVoucherNo, expenseVendor,
                expenseCurrency, expenseAmount, expenseApprovedAmount, expenseStatus,expense_reason;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            expenseNumber = v.findViewById(R.id.expense_no);
            expenseDate = v.findViewById(R.id.expense_date);
            expenseType = v.findViewById(R.id.expense_head);
            expenseMileage = v.findViewById(R.id.expense_mileage);
            expenseVoucherNo = v.findViewById(R.id.expense_doc_no);
            expenseVendor = v.findViewById(R.id.expense_vendor);
            expenseCurrency = v.findViewById(R.id.expense_currency);
            expenseAmount = v.findViewById(R.id.expense_amount);
            expenseApprovedAmount = v.findViewById(R.id.expense_approved_amount);
            expenseStatus = v.findViewById(R.id.expense_status);
            expense_reason = v.findViewById(R.id.expense_reason);
        }
    }


    @Override
    public ExpenseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_expense_summary, parent, false);
        decimalFormat = new DecimalFormat("#,###.00");
        context = parent.getContext();
        return new ViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.expenseNumber.setText(mDataset.get(position).getExpenseNumber());
        viewHolder.expenseDate.setText(mDataset.get(position).getExpenseDate());
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
        Date date = null;
        try {
            date = inputFormat.parse(mDataset.get(position).getExpenseDate());
            String outputDateStr = outputFormat.format(date);
            viewHolder.expenseDate.setText(outputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.expenseType.setText(mDataset.get(position).getExpenseType());
        viewHolder.expenseMileage.setText(mDataset.get(position).getExpenseMileage());
        viewHolder.expenseVoucherNo.setText(mDataset.get(position).getExpenseVoucherNo());
        viewHolder.expenseVendor.setText(mDataset.get(position).getExpenseVendor());
        viewHolder.expenseCurrency.setText(mDataset.get(position).getExpenseCurrency());

        priceDouble = Double.parseDouble(mDataset.get(position).getExpenseAmount());
        formatPrice = decimalFormat.format(priceDouble);
        String value = formatPrice;
        viewHolder.expenseAmount.setText(value);

        viewHolder.expenseApprovedAmount.setText(mDataset.get(position).getExpenseApprovedAmount());
        viewHolder.expenseStatus.setText(mDataset.get(position).getExpenseStatus());
        viewHolder.expense_reason.setText(mDataset.get(position).getExpenseReason());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void add(Expense item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(Expense item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public void setItems(ArrayList<Expense> myDataset) {
        mDataset = myDataset;
    }
}