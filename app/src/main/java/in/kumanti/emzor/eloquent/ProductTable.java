package in.kumanti.emzor.eloquent;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.Product;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_PRODUCT;

public class ProductTable extends SQLiteOpenHelper {

    private static final String COLUMN_PRODUCT_ID = "id";
    private static final String COLUMN_PRODUCT_PRODUCT_ID = "product_id";
    private static final String COLUMN_PRODCUT_GROUP_ID = "product_group_id";
    private static final String COLUMN_COMPANY_ID = "company_id";
    private static final String COLUMN_TAB_ID = "tab_id";
    private static final String COLUMN_SALES_REP_ID = "sales_rep_id";
    private static final String COLUMN_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_PRODUCT_NAME = "product_name";
    private static final String COLUMN_PRODUCT_DESCRIPTION = "product_description";
    private static final String COLUMN_PRODUCT_PRICE = "product_price";
    private static final String COLUMN_PRODUCT_UOM = "product_uom";
    private static final String COLUMN_PRODUCT_TYPE = "product_type";
    private static final String COLUMN_PRODUCT_CREATED_AT = "created_at";
    private static final String COLUMN_PRODUCT_UPDATED_AT = "updated_at";

    public Context context;

    public ProductTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_PRODUCT + "(" +
                COLUMN_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PRODUCT_PRODUCT_ID + " TEXT, " +
                COLUMN_PRODCUT_GROUP_ID + " TEXT, " +
                COLUMN_COMPANY_ID + " TEXT, " +
                COLUMN_TAB_ID + " TEXT, " +
                COLUMN_SALES_REP_ID + " TEXT, " +
                COLUMN_CUSTOMER_ID + " TEXT, " +
                COLUMN_PRODUCT_NAME + " TEXT, " +
                COLUMN_PRODUCT_DESCRIPTION + " TEXT, " +
                COLUMN_PRODUCT_PRICE + " TEXT, " +
                COLUMN_PRODUCT_UOM + " TEXT, " +
                COLUMN_PRODUCT_TYPE + " TEXT, " +
                COLUMN_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<Product> getProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT + " ORDER BY product_name ASC";


        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        Log.d("ViewStock--", "Get Products  ");
        ArrayList<Product> products = new ArrayList<>();

        while (cursor.moveToNext()) {
            Product product = new Product();
            Log.d("View stock MYDATA--", "Data  id" + cursor.getInt(0));
            product.setId(cursor.getInt(0));
            product.setProduct_code(cursor.getString(1));
            product.setProduct_id(cursor.getString(1));
            product.setProduct_name(cursor.getString(2));
            product.setProduct_price(cursor.getString(5));
            product.setProduct_uom(cursor.getString(7));
            product.setBatch_controlled(cursor.getString(8));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }

    public ArrayList<Product> getDistributorProducts(String sec_price_list) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
//        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT + " ORDER BY product_name ASC";
//        String sec_price_list1 = "PRICE001";
       /* String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom,pll.price, " +
                "sid.batch_controlled FROM xxmsales_item_details as pt,xxmsales_price_list_lines as pl " +
                "LEFT JOIN xxmsales_price_list_lines as pll ON pt.product_id = pll.product_code " +
                "LEFT JOIN xxmsales_price_list_header as ph ON '"+sec_price_list1+"' =ph.price_list_code and ph.price_list_code=pl.price_id " +
                "LEFT JOIN xxmsales_item_details as sid ON pt.product_id = sid.product_id " +
                "LEFT JOIN  xxmsales_stock_reciept as sr ON pt.product_id = sr.sr_product_name GROUP by pt.product_id ORDER BY pt.product_name asc, sr.sr_receipt_number desc ";
*/

        String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom,pll.price, pt.batch_controlled FROM xxmsales_item_details as pt, xxmsales_price_list_lines as pll,xxmsales_price_list_header as ph where  pt.product_id = pll.product_code and  '"+ sec_price_list +"' = ph.price_list_code and ph.price_list_code=pll.price_id  GROUP by pt.product_id ORDER BY pt.product_name asc";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        Log.d("ViewStock--", "Get Products  ");
        ArrayList<Product> products = new ArrayList<>();

        while (cursor.moveToNext()) {
            Product product = new Product();
            Log.d("View stock MYDATA--", "Data  id" + cursor.getInt(0));
//            product.setId(cursor.getInt(0));
            product.setProduct_code(cursor.getString(0));
            product.setProduct_id(cursor.getString(0));
            product.setProduct_name(cursor.getString(1));
            product.setProduct_price(cursor.getString(3));
            product.setProduct_uom(cursor.getString(2));
            product.setBatch_controlled(cursor.getString(4));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }
    public ArrayList<Product> getDistributorProducts1() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
//        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT + " ORDER BY product_name ASC";

       /* String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom,avg(pll.price), " +
                "sid.batch_controlled FROM xxmsales_item_details as pt " +
                "LEFT JOIN xxmsales_price_list_lines as pll ON pt.product_id = pll.product_code " +
                "LEFT JOIN xxmsales_item_details as sid ON pt.product_id = sid.product_id " +
                "LEFT JOIN  xxmsales_stock_reciept as sr ON pt.product_id = sr.sr_product_name GROUP by pt.product_id ORDER BY pt.product_name asc, sr.sr_receipt_number desc ";
*/

        String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom, pt.price, pt.batch_controlled FROM xxmsales_item_details as pt, xxmsales_price_list_lines as pll,xxmsales_stock_reciept as sr where  pt.product_id = pll.product_code   GROUP by pt.product_id ORDER BY pt.product_name asc";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        Log.d("ViewStock--", "Get Products  ");
        ArrayList<Product> products = new ArrayList<>();

        while (cursor.moveToNext()) {
            Product product = new Product();
            Log.d("View stock MYDATA--", "Data  id" + cursor.getInt(0));
//            product.setId(cursor.getInt(0));
            product.setProduct_code(cursor.getString(0));
            product.setProduct_id(cursor.getString(0));
            product.setProduct_name(cursor.getString(1));
            product.setProduct_price(cursor.getString(3));
            product.setProduct_uom(cursor.getString(2));
            product.setBatch_controlled(cursor.getString(4));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }

   /* public ArrayList<Product> getProductByGroupId(String productGroupId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT +" WHERE "+COLUMN_PRODCUT_GROUP_ID+"=? ORDER BY product_name ASC";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {productGroupId} );
        Log.d("Data","Get Products  " + productGroupId);

        ArrayList<Product> products = new ArrayList<>();
        while (cursor.moveToNext()) {
            Product product = new Product();
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            product.setId(cursor.getInt(0));
            product.setProduct_id(cursor.getString(1));
            product.setProduct_name(cursor.getString(2));
            product.setProduct_price(cursor.getString(5));
            product.setProduct_uom(cursor.getString(4));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }*/


    public ArrayList<Product> getCrmProducts(String customerCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " select item.product_id, item.product_name, prList.price, item.stock_uom, item.product_type, item.batch_controlled from xxmsales_item_details as item join xxmsales_price_list_lines as prList on item.product_id = prList.product_code join xxmsales_price_list_header as phd on prList.price_id = phd.price_list_code join xxmsales_customer_details as cus on cus.price_list_code = phd.price_list_code where cus.customer_id = ? ORDER BY item.product_name ASC";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerCode});
        ArrayList<Product> products = new ArrayList<>();

        while (cursor.moveToNext()) {
            Product product = new Product();
            product.setProduct_code(cursor.getString(0));
            product.setProduct_name(cursor.getString(1));
            product.setProduct_price(cursor.getString(2));
            product.setProduct_uom(cursor.getString(3));
            product.setProduct_type(cursor.getString(4));
            product.setBatch_controlled(cursor.getString(5));
            products.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return products;
    }

}
