package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class GPSTrackerModel {

    private String gps_latitude;
    private String gps_longitude;
    private String gps_date;
    private String gps_time;
    private String gps_id;
    private String companyCode;
    private String tabCode;
    private String srCode;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }
/* public GPSTrackerModel(String gps_latitude, String gps_longitude,String gps_date,String gps_time,String gps_id) {
        this.gps_latitude = gps_latitude;
        this.gps_longitude = gps_longitude;
        this.gps_date=gps_date;
        this.gps_time=gps_time;
        this.gps_id=gps_id;

    } */

    public String getGps_latitude() {
        return gps_latitude;
    }

    public void setGps_latitude(String gps_latitude) {
        this.gps_latitude = gps_latitude;
    }

    public String getGps_longitude() {
        return gps_longitude;
    }

    public void setGps_longitude(String gps_longitude) {
        this.gps_longitude = gps_longitude;
    }


    public String getGps_date() {
        return gps_date;
    }

    public void setGps_date(String gps_date) {
        this.gps_date = gps_date;
    }


    public String getGps_time() {
        return gps_time;
    }

    public void setGps_time(String gps_time) {
        this.gps_time = gps_time;
    }


    public String getGps_id() {
        return gps_id;
    }

    public void setGps_id(String gps_id) {
        this.gps_id = gps_id;
    }
}

