package in.kumanti.emzor.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.StockViewMyStockAdapter;
import in.kumanti.emzor.adapter.ViewStockOrderAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.StockViewInvoiceList;
import in.kumanti.emzor.model.ViewStockMyStockProduct;
import in.kumanti.emzor.utils.CustomSearchableSpinner;


public class ViewStockMyStockFragment extends Fragment {

    public RecyclerView myStockRecyclerView;
    CustomSearchableSpinner productSpinner;
    TextView myStockTotalInvoiceCount, myStockTotalValue;
    ArrayAdapter<Product> productSpinnerArrayAdapter;
    Context mContext;
    MyDBHandler dbHandler;
    ArrayList<Product> productArrayList;
    ProductTable productTable;
    ArrayList<ViewStockMyStockProduct> myStockProductList;
    String login_id = "", product_id = "-1";
    RecyclerViewItemListener listener;
    private RecyclerView.Adapter myStockProductAdapter;
    private RecyclerView.LayoutManager myStockLayoutManager;


    public ViewStockMyStockFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_view_stock_my_stock, container, false);
        mContext = rootView.getContext();
        dbHandler = new MyDBHandler(mContext, null, null, 1);
        myStockTotalInvoiceCount = rootView.findViewById(R.id.myStockTotalInvoiceCountTv);
        myStockTotalValue = rootView.findViewById(R.id.myStockTotalValueTv);

        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
        }

        listener = (view, position) -> {
            showstockview(myStockProductList.get(position).getProductId(), myStockProductList.get(position).getProductUom(), myStockProductList.get(position).getProductName());
        };

        productTable = new ProductTable(mContext);
        productSpinner = rootView.findViewById(R.id.myStockProductSpinner);

        productArrayList = dbHandler.getStockReceiptProducts();
        Log.d("ViewStock--", "MyStock productArrayList  " + productArrayList.size());

        Product p = new Product();
        p.product_name = "All";
        p.product_id = "-1";
        productArrayList.add(0, p);
        productSpinnerArrayAdapter = new ArrayAdapter<Product>(mContext, android.R.layout.simple_spinner_item, productArrayList);
        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(productSpinnerArrayAdapter);

        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;

                Log.d("Products", "Selected Product" + pos);
                Product p = (Product) adapterView.getItemAtPosition(pos);
                Log.d("Products", "Selected Product" + pos + p.product_name);

                product_id = p.product_id;
                showMyStockProducts();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });

        myStockRecyclerView = rootView.findViewById(R.id.stockViewMyStockRecyclerView);
        myStockRecyclerView.setHasFixedSize(true);
        myStockLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        myStockRecyclerView.setLayoutManager(myStockLayoutManager);


        try {
            showMyStockProducts();

        } catch (Exception e) {
            Log.d("QueryDataResponse", "" + e.getMessage());
        }

        return rootView;
    }

    private void showMyStockProducts() {

        myStockRecyclerView.setAdapter(null);
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String today = timeStampFormat.format(myDate);
        myStockProductList = dbHandler.getMyStockProducts(login_id, product_id, today);
        Double stockValue = 0d;
        int invoiceCount = 0;
        for (ViewStockMyStockProduct vsmp : myStockProductList) {
            stockValue += Double.parseDouble(TextUtils.isEmpty(vsmp.getProductTotalPrice()) ? "0" : vsmp.getProductTotalPrice());
            invoiceCount += Integer.parseInt(TextUtils.isEmpty(vsmp.getSecondaryInvoiceCount()) ? "0" : vsmp.getSecondaryInvoiceCount());
        }
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        String value = decimalFormat.format(stockValue);
        if (value != null && !value.equals("")) {
            if (value.startsWith(".")) {
                value = "0" + value;
            }
        }
        myStockTotalValue.setText(value);
        myStockTotalInvoiceCount.setText(String.valueOf(invoiceCount));
        myStockProductAdapter = new StockViewMyStockAdapter(myStockProductList, listener);
        myStockRecyclerView.setAdapter(myStockProductAdapter);
    }

    public void showstockview(String product_id, String product_uom, String product_name) {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_create_view_invoice, null);
        //final TextView txt_product = alertLayout.findViewById(R.id.txt_product);
        //final TextView txt_description = alertLayout.findViewById(R.id.txt_description);
        //final ImageView close = alertLayout.findViewById(R.id.close);
        ListView stock_list = alertLayout.findViewById(R.id.stock_list);
        TextView txt_stock = alertLayout.findViewById(R.id.txt_stock);
        TextView txt_product_uom = alertLayout.findViewById(R.id.txt_product_uom);
        TextView txt_product_code = alertLayout.findViewById(R.id.txt_product_code);
        TextView txt_product_name = alertLayout.findViewById(R.id.txt_product_name);


        int stock = dbHandler.getstockonhand(product_id);
        String stockonhand = Integer.toString(stock);
        txt_stock.setText(stockonhand);
        txt_product_uom.setText(product_uom);
        txt_product_code.setText(product_id);
        txt_product_name.setText(product_name);

        //Working with List to Show Data's
        ArrayList<StockViewInvoiceList> stockList = new ArrayList<StockViewInvoiceList>();
        Cursor data = dbHandler.getstock_viewinvoice(product_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {
                Log.d("MyExpiryCount", "Exp" + data.getString(3) + " bno" + data.getString(0));
                StockViewInvoiceList stocks = new StockViewInvoiceList(
                        data.getString(0),
                        data.getString(1),
                        data.getString(2),
                        data.getString(3));
                stockList.add(i, stocks);
                //System.out.println(userList.get(i).getFirstName());
                i++;


            }

            ViewStockOrderAdapter adapter1 = new ViewStockOrderAdapter(getContext(), R.layout.list_view_stock_invoice, stockList);
            //stock_list = (ListView) findViewById(R.id.stock_list);
            stock_list.setAdapter(adapter1);
        }
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void getstock_list() {


    }
}






