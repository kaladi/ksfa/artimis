package in.kumanti.emzor.model;

public class Quarantine {

    private String productCode;
    private String batchNumber;
    private String batchQuantity;
    private String expiryDate;
    private String quaratineQuantity;
    private String quaratineDate;
    private String createdAt;
    private String updatedAt;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getBatchQuantity() {
        return batchQuantity;
    }

    public void setBatchQuantity(String batchQuantity) {
        this.batchQuantity = batchQuantity;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getQuaratineQuantity() {
        return quaratineQuantity;
    }

    public void setQuaratineQuantity(String quaratineQuantity) {
        this.quaratineQuantity = quaratineQuantity;
    }

    public String getQuaratineDate() {
        return quaratineDate;
    }

    public void setQuaratineDate(String quaratineDate) {
        this.quaratineDate = quaratineDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}

