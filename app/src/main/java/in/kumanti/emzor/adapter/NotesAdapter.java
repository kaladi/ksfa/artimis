package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.Notes;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    public ArrayList<Notes> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;

    public NotesAdapter(ArrayList<Notes> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(Notes item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(Notes item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_notes, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d("NotesData", mDataset.get(position).getNotes_data());
        viewHolder.notesData.setText(mDataset.get(position).getNotes_data());
        viewHolder.notesTimestamp.setText(mDataset.get(position).getTimeStamp());

        viewHolder.notesCardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {


                //Toast.makeText(context, "Notes deleted", Toast.LENGTH_SHORT).show();

                return true;
            }
        });

        viewHolder.editNotesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onClick(view, position);
                //mDataset.get(position).getNotes_data();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView notesData, notesTimestamp;
        CardView notesCardView;
        ImageView editNotesButton;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            notesData = v.findViewById(R.id.notesTextView);
            notesTimestamp = v.findViewById(R.id.notesTimestampTV);
            notesCardView = v.findViewById(R.id.createNotesCardView);
            editNotesButton = v.findViewById(R.id.editNotesButton);

           /* notesCardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {


                    Log.d("Notes", "Deleted");

                    return true;
                }
            });*/
        }
    }


}