package in.kumanti.emzor.model;

public class DeliveryChallanItems {
    private String sales_line_id;
    private String product;
    private String uom;
    private int quantity;
    private String price;
    private String value;
    private double price_db;
    private double value_db;
    private String prod_type;
    private String orderNumber;
    private String productId;
    private String batchControlled;
    private int waybill_quantity;


    public DeliveryChallanItems(String sales_line_id1, String product1, String uom1, int quantity1, String price1, String value1, double price_db1, double value_db1, String prod_type1, String orderNumber1, String productId1, String batchControlled1, int waybill_quantity1) {
        sales_line_id = sales_line_id1;
        product = product1;
        uom = uom1;
        quantity = quantity1;
        price = price1;
        value = value1;
        price_db = price_db1;
        value_db = value_db1;
        prod_type = prod_type1;
        orderNumber = orderNumber1;
        productId = productId1;
        batchControlled = batchControlled1;
        waybill_quantity = waybill_quantity1;

    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product1) {
        product = product1;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom1) {
        uom = uom1;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity1) {
        quantity = quantity1;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price1) {
        price = price1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value1) {
        value = value1;
    }

    public String getSales_line_id() {
        return sales_line_id;
    }

    public void setSales_line_id(String sales_line_id1) {
        sales_line_id = sales_line_id1;
    }

    public double getPrice_db() {
        return price_db;
    }

    public void setPrice_db(double price_db1) {
        price_db = price_db1;
    }

    public double getValue_db() {
        return value_db;
    }

    public void setValue_db(double value_db1) {
        value_db = value_db1;
    }

    public String getProd_type() {
        return prod_type;
    }

    public void setProd_type(String prod_type1) {
        prod_type = prod_type1;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber1) {
        orderNumber = orderNumber1;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId1) {
        productId = productId1;
    }

    public String getBatchControlled() {
        return batchControlled;
    }

    public void setBatchControlled(String batchControlled1) {
        batchControlled = batchControlled1;
    }

    public int getWaybill_quantity() {
        return waybill_quantity;
    }

    public void setWaybill_quantity(int waybill_quantity1) {
        waybill_quantity = waybill_quantity1;
    }


}

