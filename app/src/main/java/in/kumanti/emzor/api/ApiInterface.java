package in.kumanti.emzor.api;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import in.kumanti.emzor.mastersData.ActivationFlag;
import in.kumanti.emzor.mastersData.CompanyMaster;
import in.kumanti.emzor.mastersData.JSONResponse;
import in.kumanti.emzor.mastersData.JourneyPlanMaster;
import in.kumanti.emzor.mastersData.SRMaster;
import in.kumanti.emzor.mastersData.SetupValidations;
import in.kumanti.emzor.model.ApkUpgrade;
import in.kumanti.emzor.model.MasterResponseJson;
import in.kumanti.emzor.model.ResponseJson;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    //String BASE_URL = "http://182.74.154.75/websales_demo/webservices/";
   // String BASE_URL = "http://182.74.154.75/websales/webservices/";
    //String BASE_URL = "http://41.216.174.84/websales/webservices/";

    //SaiSagar Live
//    String BASE_URL = "https://kaladi.in/saisagar_dev/webservices/";
//    String BASE_URL = "https://kaladi.in/saisagar/webservices/";
//    String BASE_URL = "http://104.211.141.35:8080/Ksfa/webservices/";
//    String BASE_URL = "http://sfa.kaladi.in:8080/Ksfa/webservices/";
//    String BASE_URL = "http://54.159.96.54/Ksfa/webservices/";
//    String BASE_URL = "http://54.159.96.54/Vista_Ksfa_Test/webservices/";

     String BASE_URL = "http://54.159.96.54/Vista_Ksfa/webservices/";
    String SMS_URL = "http://54.159.96.54/Vista_Ksfa/sendsms/";
    String ACTIVATION_URL = "http://54.159.96.54/Vista_Ksfa/Transactions/src/";

  /*  String BASE_URL = "http://54.159.96.54/Vista_Ksfa_Test/webservices/";
    String SMS_URL = "http://54.159.96.54/Vista_Ksfa_Test/sendsms/";
    String ACTIVATION_URL = "http://54.159.96.54/Vista_Ksfa_Test/Transactions/src/";*/

    @GET("customer_api.php")
    Call<ResponseJson> getCustomerMaster();

    @GET("price_list.php")
    Call<JSONResponse> getPriceList();

    @GET("item_master_fetch.php")
    Call<JSONResponse> getItemMaster();

    @GET("company.php")
    Call<List<CompanyMaster>> getCompanyMaster();

    @GET("location.php")
    Call<JSONResponse> getLocation();

    @GET("sr_master.php")
    Call<List<SRMaster>> getSRMaster();

    @GET("journey_plan.php")
    Call<List<JourneyPlanMaster>> getJourneyPlan(@Query("sr_code") String sr_code, @Query("company") String company);
    //Call<List<JourneyPlanMaster>> getJourneyPlan(@Url String sr_code);

    @FormUrlEncoded
    @POST("post_test.php")
    Call<Void> insertData(@Field("name") String name, @Field("value") String value);

    @POST("transaction_api.php")
    Call<ResponseJson> insertData(@Body JsonObject user);

    @POST("invoice_save.php")
    Call<ResponseBody> insertInvoiceData(@Body ArrayList<JsonObject> user);

    @GET("admin_api.php")
    Call<SetupValidations> getValidatedLogin(@Query("user_name") String user_name, @Query("password") String password);

    @GET("setup_api.php")
    Call<JSONResponse> getSetupDetails(@Query("imei_num") String imei_num);

    @GET("tab_confirm_api.php")
    Call<SetupValidations> getTabConfirmation(@Query("imei_num") String imei_num);

    @GET("masters_api.php")
    Call<MasterResponseJson> getMasterData(@Query("sr_code") String sr_code, @Query("company") String company);

    @GET("master_rdl.php")
    Call<MasterResponseJson> getMasterRDLData(@Query("sr_code") String sr_code, @Query("company") String company);

    @POST("gps_api.php")
    Call<ResponseBody> sendGPSdata(@Body JsonObject user);

    @POST("email_send.php")
    Call<ResponseBody> postingEmailData(@Body JsonObject user);

    @POST("sendsmsorder.php")
    Call<ResponseBody> postingOrderSMSData(@Body JsonObject user);

    @POST("sendsmsinvoice.php")
    Call<ResponseBody> postingInvoiceSMSData(@Body JsonObject user);

    @POST("sendsmsstockreturn.php")
    Call<ResponseBody> postingStockReturnSMSData(@Body JsonObject user);

    @POST("sendsmscrm.php")
    Call<ResponseBody> postingCRMSMSData(@Body JsonObject user);

    @POST("sendsmscollectionInvoice.php")
    Call<ResponseBody> postingCollectionSMSData(@Body JsonObject user);

    @POST("sendsmsCustomerReturn.php")
    Call<ResponseBody> postingCustomerReturnSMSData(@Body JsonObject user);

    @POST("sendsmsWaybill.php")
    Call<ResponseBody> postingWaybillSMSData(@Body JsonObject user);

    @POST("sendsmscrmOrder.php")
    Call<ResponseBody> postingCRMOrderSMSData(@Body JsonObject user);

    @GET("apk_upgrade_api.php")
    Call<ApkUpgrade> getApkUpgradeDetails();

    @GET("8b1a9953c4.php")
    Call<ActivationFlag> getActivationFlag(@Query("companyCode") String companyCode);

    @POST("change_password.php")
    Call<ResponseBody> postingChangePassword(@Query("password") String password, @Query("username") String username, @Query("imei") String imei, @Query("companycode") String companycode);

    @POST("forgot_password.php")
    Call<ResponseBody> postingForgetPassword(@Query("username") String username,@Query("email") String email, @Query("password") String password, @Query("companycode") String companycode);


}

