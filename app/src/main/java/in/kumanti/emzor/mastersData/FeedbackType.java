package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class FeedbackType {

    private String feedback_id;
    private String feedback_type;
    private String tab_code;
    private String bde_code;
    private String customer_code;
    private String action_taken;
    private String status;

    public FeedbackType(

    ) {




        this.bde_code = bde_code;
        this.customer_code = customer_code;
        this.action_taken = action_taken;
        this.status = status;

    }

    public String getFeedback_id() {
        return feedback_id;
    }
    public void setFeedback_id(String feedback_id) {
        this.feedback_id = feedback_id;
    }

    public String getFeedback_type() { return feedback_type; }
    public void setFeedback_type(String feedback_type) {
        this.feedback_type = feedback_type;

    }

    public String getTab_code() { return tab_code; }
    public void setTab_code(String feedback_id) {
        this.tab_code = tab_code;
    }

    public String getBde_code() { return bde_code; }
    public void setBde_code(String bde_code) {
        this.bde_code = bde_code;
    }


    public String getCustomer_code() { return customer_code; }
    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getAction_taken() { return action_taken; }
    public void setAction_taken(String action_taken) {
        this.action_taken = action_taken;
    }

    public String getStatus() { return status; }
    public void setStatus(String status) {
        this.status = status;
    }


}

