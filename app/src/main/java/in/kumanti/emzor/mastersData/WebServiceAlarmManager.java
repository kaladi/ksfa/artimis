package in.kumanti.emzor.mastersData;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import in.kumanti.emzor.R;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.CompetitorStockProductTable;
import in.kumanti.emzor.eloquent.CustomerShippingAddressTable;
import in.kumanti.emzor.eloquent.LedgerTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.SurveyQuestionsTable;
import in.kumanti.emzor.eloquent.SurveyTable;
import in.kumanti.emzor.eloquent.WarehouseStockProductsTable;


public class WebServiceAlarmManager extends Service {

    private static final String TAG = "BOOMBOOMTESTGPS";
    private static final String TAG_SUCCESS = "sucess";
    private static final String TAG_MAX_GPS_TIME = "max_gps_time";
    private static final String TAG_MAX_VISIT_TIME = "max_visit_time";
    private static String url_data_transfer = "http://sfa.fmclgrp.com/locationtracker/ajax_gps_api.php";
    final Handler handler = new Handler();
    public String DEFAULT_CHANNEL_ID;
    String max_gps_time, max_visit_time;
    ApiInterface api;
    MyDBHandler dbHandler;
    CustomerShippingAddressTable customerShippingAddressTable;
    CompetitorStockProductTable competitorStockProductTable;
    SurveyTable surveyTable;
    SurveyQuestionsTable surveyQuestionsTable;
    WarehouseStockProductsTable warehouseStockProductsTable;
    LedgerTable ledgerTable;
    Timer timer = new Timer();
    TimerTask doAsynchronousTask = new TimerTask() {
        @Override
        public void run() {
            handler.post(new Runnable() {
                @SuppressWarnings("unchecked")
                public void run() {
                    try {
                        Log.e(TAG, "timercalling");
                        boolean networkConnected = isNetworkConnected();
                        boolean networking = isInternetAvailable();
                        if (networkConnected == true) {

                            if (networking == true) {
                                Log.e(TAG, "Inside MainActivity Call");
                                Toast.makeText(getApplicationContext(), "Inside Webserive Call", Toast.LENGTH_SHORT).show();

                                // generateWebservices();
                            }

                        }

                        // Intent i = new Intent(TimerService.this, MainActivity.class);
                        //  i.putExtra("msg","timercalling");

                        // startActivity(i);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                    }
                }
            });
        }
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        /*   super.onStartCommand(intent, flags, startId);;
        return START_STICKY;*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification.Builder builder = new Notification.Builder(this, DEFAULT_CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    //  .setContentText(text)
                    .setAutoCancel(true);

            Notification notification = builder.build();
            startForeground(1, notification);

        } else {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    // .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true);

            Notification notification = builder.build();

            startForeground(1, notification);
        }
        return START_NOT_STICKY;


    }

    @Override
    public void onCreate() {


        timer.schedule(doAsynchronousTask, 0, 1000 * 15 * 60);

        Log.e(TAG, "onCreate");

    }

    @Override
    public void onDestroy() {


    }


   /* public void generateWebservices() {

        Toast.makeText(getApplicationContext(), "generateWebservices", Toast.LENGTH_SHORT).show();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        api = retrofit.create(ApiInterface.class);
        Call<ResponseJson> customerMaster = api.getMasterData("11");

        dbHandler.deleteCustomerMaster();
        dbHandler.deletePriceList();
        dbHandler.deleteItemMaster();

        customerMaster.enqueue(new Callback<ResponseJson>() {

            @Override
            public void onResponse(Call<ResponseJson> customerMaster, retrofit2.Response<ResponseJson> response) {

                ResponseJson resultSet = response.body();

                String status=resultSet.status;
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();


                if (resultSet.status.equals("true")) {

                    Toast.makeText(getApplicationContext(), "true", Toast.LENGTH_SHORT).show();

                    ArrayList<CustomerMaster> customerMasterList = new Gson().fromJson(resultSet.customer, new TypeToken<ArrayList<CustomerMaster>>() {
                    }.getType());

                    ArrayList<ItemMaster> itemMastersList = new Gson().fromJson(resultSet.Product, new TypeToken<ArrayList<ItemMaster>>() {
                    }.getType());

                    ArrayList<CompetitorStockProduct> competitor_productList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CompetitorStockProduct>>() {
                    }.getType());

                    ArrayList<PriceListHeader> price_headerList = new Gson().fromJson(resultSet.price_header, new TypeToken<ArrayList<PriceListHeader>>() {
                    }.getType());

                    ArrayList<PriceListLines> price_lineList = new Gson().fromJson(resultSet.price_line, new TypeToken<ArrayList<PriceListLines>>() {
                    }.getType());

                    ArrayList<CustomerShippingAddress> shipping_addressList = new Gson().fromJson(resultSet.shipping_address, new TypeToken<ArrayList<CustomerShippingAddress>>() {
                    }.getType());

                    ArrayList<JourneyPlanMaster> journeyplanList = new Gson().fromJson(resultSet.journey_plan, new TypeToken<ArrayList<JourneyPlanMaster>>() {
                    }.getType());

                    ArrayList<ViewStockWarehouseProduct> warehouseProductsList = new Gson().fromJson(resultSet.warehouse_stock, new TypeToken<ArrayList<ViewStockWarehouseProduct>>() {
                    }.getType());


                    ArrayList<Ledger> ledgerList = new Gson().fromJson(resultSet.customer_ledgerr, new TypeToken<ArrayList<Ledger>>() {
                    }.getType());

                    ArrayList<FeedbackType> feedbackList = new Gson().fromJson(resultSet.feedback, new TypeToken<ArrayList<FeedbackType>>() {
                    }.getType());

                    ArrayList<FeedbackType> messageList = new Gson().fromJson(resultSet.message, new TypeToken<ArrayList<FeedbackType>>() {
                    }.getType());

                    String[] customerMasters = new String[resultSet.customer.size()];
                    String[] itemMasters = new String[resultSet.Product.size()];
                    String[] priceHeaderMasters = new String[resultSet.price_header.size()];
                    String[] priceListMasters = new String[resultSet.price_line.size()];
                    String[] shippingAddressMasters = new String[resultSet.shipping_address.size()];
                    String[] competitorProductMasters = new String[resultSet.competitor_product.size()];
                    String[] journeyPlanMasters = new String[resultSet.journey_plan.size()];
                    String[] wareHouseStockMasters = new String[resultSet.warehouse_stock.size()];
                    String[] ledgerMasters = new String[resultSet.customer_ledgerr.size()];
                    String[] feedbackMasters = new String[resultSet.feedback.size()];
                    String[] messageMasters = new String[resultSet.message.size()];


                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < customerMasterList.size(); i++) {
                        long id2 = dbHandler.create_customer_master

                                (
                                        customerMasterList.get(i).getCustomer_id(),
                                        customerMasterList.get(i).getCustomer_name(),
                                        customerMasterList.get(i).getBill_address_line1(),
                                        customerMasterList.get(i).getBill_address_line2(),
                                        customerMasterList.get(i).getBill_address_line3(),
                                        customerMasterList.get(i).getBill_city(),
                                        customerMasterList.get(i).getBill_state(),
                                        customerMasterList.get(i).getContact_no(),
                                        customerMasterList.get(i).getCustomer_type()

                                );
                    }

                    for (int i = 0; i < itemMastersList.size(); i++) {
                        long id3 = dbHandler.create_item_master

                                (
                                        itemMastersList.get(i).getProduct_id(),
                                        itemMastersList.get(i).getProduct_name(),
                                        itemMastersList.get(i).getLong_description(),
                                        itemMastersList.get(i).getStock_uom(),
                                        itemMastersList.get(i).getSale_uom(),
                                        itemMastersList.get(i).getBatch(),
                                        itemMastersList.get(i).getFocus()

                                );
                    }

                    for (int i = 0; i < price_headerList.size(); i++) {
                        long id4 = dbHandler.create_price_list_header

                                (
                                        price_headerList.get(i).getPrice_list_code(),
                                        price_headerList.get(i).getPrice_list_name()
                                );
                    }

                    for (int i = 0; i < price_lineList.size(); i++) {
                        long id5 = dbHandler.create_price_list_lines

                                (
                                        price_lineList.get(i).getPrice_code(),
                                        price_lineList.get(i).getProduct_name(),
                                        price_lineList.get(i).getProduct_code(),
                                        price_lineList.get(i).getPrice()


                                );
                    }

                    for (int i = 0; i < shipping_addressList.size(); i++) {

                        CustomerShippingAddress customerShippingAddress = new CustomerShippingAddress();
                        customerShippingAddress.setCustomer_id(shipping_addressList.get(i).getCustomer_id());
                        customerShippingAddressTable.create(customerShippingAddress);

                    }

                    for (int i = 0; i < competitor_productList.size(); i++) {

                        CompetitorStockProduct productCompetitor = new CompetitorStockProduct();
                        productCompetitor.setProduct_id(competitor_productList.get(i).getProduct_id());
                        productCompetitor.setCompetitor_product_id(competitor_productList.get(i).getCompetitor_product_id());
                        competitorStockProductTable.create(productCompetitor);

                    }


                    for (int i = 0; i < journeyplanList.size(); i++) {

                        long id6 = dbHandler.create_journey_plan

                                (
                                        journeyplanList.get(i).getSeq_no(),
                                        journeyplanList.get(i).getCustomer_code(),
                                        journeyplanList.get(i).getCustomer_name(),
                                        journeyplanList.get(i).getLocation(),
                                        journeyplanList.get(i).getCustomer_type()

                                );

                    }

                    for (int i = 0; i < warehouseProductsList.size(); i++) {

                        ViewStockWarehouseProduct viewStockWarehouseProduct = new ViewStockWarehouseProduct();
                        viewStockWarehouseProduct.setProductName(warehouseProductsList.get(i).getProductName());
                        warehouseStockProductsTable.create(viewStockWarehouseProduct);

                    }

                    for (int i = 0; i < ledgerList.size(); i++) {

                        Ledger ledger = new Ledger();

                        ledger.setInvoiceNumber(ledgerList.get(i).getInvoiceNumber());
                        ledger.setInvoiceDate(ledgerList.get(i).getInvoiceDate());
                        ledger.setInvoiceAmount(ledgerList.get(i).getInvoiceAmount());
                        ledger.setReceiptAmount(ledgerList.get(i).getReceiptAmount());
                        ledger.setOutstandingAmount(ledgerList.get(i).getOutstandingAmount());
                        ledger.setCustomerId(ledgerList.get(i).getCustomerId());
                        ledgerTable.create(ledger);

                    }

                    for (int i = 0; i < feedbackList.size(); i++) {

                        long id6 = dbHandler.create_feedback_type

                                (
                                        feedbackList.get(i).getFeedback_type()

                                );

                    }

                    for (int i = 0; i < messageList.size(); i++) {

                        long id6 = dbHandler.create_global_message

                                (
                                        messageList.get(i).getMessage_name()

                                );

                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseJson> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
 */


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public boolean isInternetAvailable() {

        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;

    }

}