package in.kumanti.emzor.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockViewInvoiceList;
import in.kumanti.emzor.utils.Globals;

public class ViewStockOrderAdapter extends ArrayAdapter<StockViewInvoiceList> {

    Activity mActivity = null;
    AlertDialog alertDialog1;
    Globals globals;
    private LayoutInflater mInflater;
    private ArrayList<StockViewInvoiceList> stockList;
    private int mViewResourceId;
    private Context c;

    public ViewStockOrderAdapter(Context context, int textViewResourceId, ArrayList<StockViewInvoiceList> stockList) {
        super(context, textViewResourceId, stockList);
        this.stockList = stockList;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
        c = context;

    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        convertView = mInflater.inflate(mViewResourceId, null);
        final StockViewInvoiceList stockDetail = stockList.get(position);
        if (stockDetail != null) {
            viewHolder = new ViewHolder();
            viewHolder.expirydate = convertView.findViewById(R.id.txt_expdate);
            viewHolder.batchnumber = convertView.findViewById(R.id.txt_batch_no);
            viewHolder.batchqty = convertView.findViewById(R.id.txt_stock_qty);

            if (viewHolder.expirydate != null) {
                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                Date date = null;
                try {
                    date = inputFormat.parse(stockDetail.getExpiry_date());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputDateStr = outputFormat.format(date);
                viewHolder.expirydate.setText(outputDateStr);
            } else {
                viewHolder.expirydate.setText("");
            }
            if (viewHolder.batchnumber != null) {
                viewHolder.batchnumber.setText((stockDetail.getBatch_number()));
            } else {
                viewHolder.batchnumber.setText("0");
            }
            if (!TextUtils.isEmpty(stockDetail.getIsExpired()) && stockDetail.getIsExpired().equals("TRUE")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    viewHolder.batchnumber.setBackground(c.getDrawable(R.drawable.expiry_products_border));
                }
            }
            if (viewHolder.batchqty != null) {
                viewHolder.batchqty.setText((stockDetail.getStock_quantity()));
            } else {
                viewHolder.batchqty.setText("0");
            }
        }


        return convertView;
    }

    public class ViewHolder {
        TextView expirydate, batchnumber, batchqty;

    }
}
