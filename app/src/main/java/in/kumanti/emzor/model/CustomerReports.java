package in.kumanti.emzor.model;

public class CustomerReports {

    private String serial;
    private String customerCode;
    private String customer_name;
    private String location_name;
    private String customer_type;
    private String city;
    private String mobile;
    private String email;


    public CustomerReports(String serial1,String customerCode1,String customer_name1, String customer_type1, String location1, String city1, String mobile1, String email1) {

        serial = serial1;
        customerCode = customerCode1;
        customer_name = customer_name1;
        customer_type = customer_type1;
        location_name = location1;
        city = city1;
        mobile = mobile1;
        email = email1;


    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial1) {
        serial = serial1;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode1) {
        customerCode = customerCode1;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name1) {
        customer_name = customer_name1;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation(String location1) {
        location_name = location1;
    }

    public String getCustomerType() {
        return customer_type;
    }

    public void setCustomerType(String customer_type1) {
        customer_type = customer_type1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city1) {
        city = city1;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile1) {
        mobile = mobile1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email1) {
        email1 = email1;
    }


}

