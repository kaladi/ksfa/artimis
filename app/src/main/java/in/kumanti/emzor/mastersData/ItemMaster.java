package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class ItemMaster {

    private String productId;
    private String productName;
    private String longDescription;
    private String stock_uom;
    private String sale_uom;
    private String batch;
    private String focusSchemeType;
    private String item_cost;
    private String conversion;


    public ItemMaster(String productId,
                      String productName,
                      String longDescription,
                      String stock_uom,
                      String sale_uom,
                      String batch,
                      String focusSchemeType,
                      String item_cost,
                      String conversion

    ) {

        this.productId = productId;
        this.productName = productName;
        this.longDescription = longDescription;
        this.stock_uom = stock_uom;
        this.sale_uom = sale_uom;
        this.batch = batch;
        this.focusSchemeType = focusSchemeType;
        this.item_cost = item_cost;
        this.conversion = conversion;

    }

    public String getProduct_id() {
        return productId;
    }

    public String getProduct_name() {
        return productName;
    }

    public String getLong_description() {
        return longDescription;
    }

    public String getStock_uom() {
        return stock_uom;
    }

    public String getSale_uom() {
        return sale_uom;
    }

    public String getBatch() {
        return batch;
    }

    public String getFocus() {
        return focusSchemeType;
    }

    public String getItem_cost() {
        return item_cost;
    }

    public String getConversion() {
        return conversion;
    }

    public void setConversion(String conversion) {
        this.conversion = conversion;
    }
}

