package in.kumanti.emzor.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.SalesRep_Customer_List;
import in.kumanti.emzor.utils.GPSTracker;

public class GetLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    GPSTracker gpsTracker;
    double latitude;
    double longitude;
    Button back;
    MyDBHandler dbHandler;
    String login_id, nav_type;
    ArrayList<SalesRep_Customer_List> userList;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);

        back = findViewById(R.id.back);
        getlocation();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            login_id = bundle.getString("login_id");
            nav_type = bundle.getString("nav_type");
            System.out.println("map::nav_type = " + nav_type);
        }


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        PolylineOptions lineOptions = null;
        ArrayList<LatLng> points = null;
        mMap = googleMap;
        System.out.println("map::nav_type1 = " + nav_type);
        if ("SRLanding".equals(nav_type)) {
            //Populate Header Details
            System.out.println("map::login_id1 = " + login_id);
            Cursor customerDetails = dbHandler.getJourneyplanformap(login_id);
            int numRows = customerDetails.getCount();
            System.out.println("map::numRows = " + numRows);
            if (numRows == 0) {
                 Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
            } else {
                // Toast.makeText(getApplicationContext(), "Inside Loop", Toast.LENGTH_LONG).show();

                int i = 0;
                while (customerDetails.moveToNext()) {
//                    Toast.makeText(getApplicationContext(), "Customer:" + customerDetails.getString(2)+"Latitude:" + customerDetails.getString(22) + "Longitude:" + customerDetails.getString(23), Toast.LENGTH_LONG).show();
//                     Toast.makeText(getApplicationContext(), "Status:" + customerDetails.getString(5), Toast.LENGTH_LONG).show();

                    latitude = customerDetails.getDouble(2);
                    if (latitude == 0) {


                    } else {
                        System.out.println(" (map::customerDetails.getString(1))) = " +  (customerDetails.getString(1)));
                        //  points = new ArrayList<>();
                        latitude = customerDetails.getDouble(2);
                        longitude = customerDetails.getDouble(3);
                        System.out.println("map::latitude = " + latitude);
                        System.out.println("map::longitude = " + longitude);
                        LatLng latLng = new LatLng(latitude, longitude);
                        String status = "false";
//                        if ("true".equals(customerDetails.getString(5))) {
                        if ("true".equals(status)) {
                            mMap.addMarker(
                                    new MarkerOptions()
                                            .position(latLng)

                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                            .anchor(0.5f, 0.5f)
                                            .title(customerDetails
                                                    .getString(1)));
                            float zoomLevel = 5f; //This goes up to 21
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
                        } else {
                            mMap.addMarker(
                                    new MarkerOptions()
                                            .position(latLng)

                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                            .anchor(0.5f, 0.5f)
                                            .title(customerDetails
                                                    .getString(1)));
                            float zoomLevel = 5f; //This goes up to 21
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
                        }

                        // points.add(latLng);


                    }

                }

                //lineOptions.addAll(points);
                //lineOptions.width(10);
                //lineOptions.color(Color.RED);
                // if(lineOptions != null) {
                //     mMap.addPolyline(lineOptions);
                //  }
            }

        } else {
            System.out.println("map::latitude1 = " + latitude);
            System.out.println("map::longitude1 = " + longitude);
            LatLng latLng = new LatLng(latitude, longitude);
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .anchor(0.5f, 0.5f)
                    .title("You are Here"));
//mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            float zoomLevel1 = 16f; //This goes up to 21
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel1));
        }

    }

    private void getlocation() {

        gpsTracker = new GPSTracker(this);
// check if GPS enabled
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

// \n is for new line
// Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
// can't get location
// GPS or Network is not enabled
// Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }
}
