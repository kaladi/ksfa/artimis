package in.kumanti.emzor.activity.reports;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.activity.OrderProductActivity;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.OrderDetailsList;


public class OrderReportsActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    String login_id, checkin_time, marqueeTextString;
    TextView actionBarTitle, marqueeText;
    ArrayList<OrderDetailsList> orderDetailsListArrayList;
    OrderDetailsList orderDetailsListArrayList1;
    OrderDetailsListAdapter adapter = null;
    String format_order_value;
    MyDBHandler dbHandler;
    DecimalFormat formatter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_order_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText("Order Report");
        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);

        marqueeText.setSelected(true);

        orderDetailsListArrayList = new ArrayList<OrderDetailsList>();

        getOrderReports();

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }

    private void getOrderReports() {

        System.out.println("AAA1Order::login_id = " + login_id);
        Cursor data = dbHandler.getOrderReportDetails(login_id, "");
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                String order_val = data.getString(6);
                String inputDateStr = data.getString(0);

                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                Date date = null;
                try {
                    date = inputFormat.parse(inputDateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputDateStr = outputFormat.format(date);

                if (order_val == null) {

                } else {
                    double order_value = Double.parseDouble(order_val);
                    format_order_value = formatter.format(order_value);
                }


                orderDetailsListArrayList1 = new OrderDetailsList(outputDateStr, data.getString(1), data.getString(2), data.getString(3), data.getString(4), data.getString(5), format_order_value, data.getString(7), data.getString(8), data.getString(9));
                orderDetailsListArrayList.add(i, orderDetailsListArrayList1);
                i++;
            }

            adapter = new OrderDetailsListAdapter(orderDetailsListArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(adapter);
        }

    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        login_id = bundle.getString("login_id");


    }

    private void initializeViews() {

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);
        recyclerView = findViewById(R.id.order_list);
        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        formatter = new DecimalFormat("#,###.00");

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);


    }

    private void setFooterDateTime() {

        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


    public class OrderDetailsListAdapter extends RecyclerView.Adapter<OrderDetailsListAdapter.MyViewHolder> {

        private ArrayList<OrderDetailsList> users;


        public OrderDetailsListAdapter(ArrayList<OrderDetailsList> users) {
            this.users = users;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_order_details, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            final OrderDetailsList movie = users.get(position);


            holder.date.setText(movie.getDate());
            holder.order_num.setText(movie.getOrder_num());
            holder.customer_name.setText(movie.getCustomer_name());
            holder.location.setText(movie.getLocation());
            holder.city.setText(movie.getCity());
            holder.line_items.setText(movie.getLine_items());
            holder.value.setText(movie.getValue());

            String status = dbHandler.get_order_sync(movie.getOrder_num());
            System.out.println("status = " + status);
             if(status!=null){
                 if(movie.getStatus().equals("Completed") || movie.getStatus().equals("Entered"))
                 holder.status.setText("Posted");
                 else
                     holder.status.setText(movie.getStatus());
             }else{
                 holder.status.setText(movie.getStatus());
             }




            holder.vm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String id = movie.getOrder_id();
                    // Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
                    Intent myintent = new Intent(getApplicationContext(), OrderProductActivity.class);

                    //Create the bundle
                    Bundle bundle = new Bundle();

                    //Add your data to bundle
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", movie.getCustomer_id());
                    bundle.putInt("order_id", Integer.parseInt(id));
                    bundle.putString("login_id", login_id);
                    bundle.putString("nav_type", "Reports");
                    bundle.putString("order_num", movie.getOrder_num());

                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivity(myintent);

                }
            });

        }

        @Override
        public int getItemCount() {
            return users.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView date, order_num, customer_name, location, city, line_items, value, status;

            public View vm;

            public MyViewHolder(View view) {
                super(view);
                date = view.findViewById(R.id.date);
                order_num = view.findViewById(R.id.order_num);
                customer_name = view.findViewById(R.id.customer_name);
                location = view.findViewById(R.id.location);
                city = view.findViewById(R.id.city);
                line_items = view.findViewById(R.id.line_items);
                value = view.findViewById(R.id.value);
                status = view.findViewById(R.id.status);
                vm = view;

            }
        }


    }

}
