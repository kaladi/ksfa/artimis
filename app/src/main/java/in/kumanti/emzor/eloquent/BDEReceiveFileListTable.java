package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.BDEReceiveFilesList;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_BDE_RECEIVE_FILES_LIST;

public class BDEReceiveFileListTable extends SQLiteOpenHelper {

    //Columns used for Business Development Executive Receiving File details
    private static final String COLUMN_BDE_RECEIVE_FILE_NAME_ID = "fileId";
    private static final String COLUMN_BDE_RECEIVE_FILE_NAME = "fileName";
    private static final String COLUMN_BDE_RECEIVE_CREATED_AT = "createdAt";
    private static final String COLUMN_BDE_RECEIVE_UPDATED_AT = "updatedAt";

    public Context context;
    String selectQuery;
    Cursor cursor;

    public BDEReceiveFileListTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_BDE_RECEIVE_FILES_LIST);

        String bdeListsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_BDE_RECEIVE_FILES_LIST + "(" +
                COLUMN_BDE_RECEIVE_FILE_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_BDE_RECEIVE_FILE_NAME + " TEXT, " +
                COLUMN_BDE_RECEIVE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_BDE_RECEIVE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(bdeListsTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(BDEReceiveFilesList bdeReceiveFilesList) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_BDE_RECEIVE_FILE_NAME, bdeReceiveFilesList.getFileName());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_BDE_RECEIVE_FILES_LIST, null, values);
        db.close();
        return id;
    }

    public BDEReceiveFilesList getBdeReceiveFilesListByName(String fileName) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BDE_RECEIVE_FILES_LIST + " where fileName = ?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{fileName});

        BDEReceiveFilesList bdeFile = null;
        while (cursor.moveToNext()) {
            bdeFile = new BDEReceiveFilesList();
            bdeFile.setFileId(cursor.getInt(0));
            bdeFile.setFileName(cursor.getString(1));
        }
        cursor.close();
        sqlWrite.close();
        return bdeFile;
    }

   /* public ArrayList<BDEReceiveFilesList> getBdeReceiveFilesList() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BDE_RECEIVE_FILES_LIST +" ORDER BY bdeName ASC" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<BDEReceiveFilesList> bdeListArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            BDEReceiveFilesList bdeReceiveFilesList = new BDEReceiveFilesList();
            bdeReceiveFilesList.setFileId(cursor.getInt(0));
            bdeReceiveFilesList.setFileName(cursor.getString(1));

            bdeListArrayList.add(bdeReceiveFilesList);
        }
        cursor.close();
        sqlWrite.close();
        return bdeListArrayList;
    }*/

   /* public void deleteBdeReceiveFilesList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BDE_RECEIVE_FILES_LIST, null, null);
        db.close();
    }*/

}
