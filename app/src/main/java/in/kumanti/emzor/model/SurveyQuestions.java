package in.kumanti.emzor.model;

import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SurveyQuestions {

    public int id;
    public String company_id;
    public String customer_id;
    public String sales_rep_id;
    public String survey_id;
    public String question_code;
    public String question_type;
    public String question_name;
    public ArrayList<SurveyQuestionOption> surveyQuestionOptions;
    public String created_at;
    public String updated_at;
    public ArrayList<CheckBox> checkBoxes = null;
    public RadioGroup radioGroup = null;
    public EditText editText = null;
    public TextView questionResponse;
    public int seletectedRadioGroupIndex = -1;
    public List<Integer> selectedCheckBoxIndexs = new ArrayList<Integer>();
    public String inputTextAnswer;

    public SurveyQuestions() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }


    public String getSales_rep_id() {
        return sales_rep_id;
    }

    public void setSales_rep_id(String sales_rep_id) {
        this.sales_rep_id = sales_rep_id;
    }


    public String getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(String survey_id) {
        this.survey_id = survey_id;
    }


    public String getQuestion_code() {
        return question_code;
    }

    public void setQuestion_code(String question_code) {
        this.question_code = question_code;
    }


    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }


    public String getQuestion_name() {
        return question_name;
    }

    public void setQuestion_name(String question_name) {
        this.question_name = question_name;
    }


    public ArrayList<SurveyQuestionOption> getSurveyQuestionOptions() {
        return surveyQuestionOptions;
    }

    public void setSurveyQuestionOptions(ArrayList<SurveyQuestionOption> surveyQuestionOptions) {
        this.surveyQuestionOptions = surveyQuestionOptions;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    public String getAnswer() {
        if (this.getQuestion_type().equals("Single Choice")) {
            RadioButton selectedRadioButton = this.radioGroup.findViewById(this.radioGroup.getCheckedRadioButtonId());
            if (selectedRadioButton != null)
                return selectedRadioButton.getText().toString();

        } else if (this.getQuestion_type().equals("Multiple Choice")) {
            String result = "";
            for (CheckBox ck : this.checkBoxes) {
                if (ck.isChecked()) {
                    result += ck.getText().toString() + ", ";
                }
            }
            return result;
        } else if (this.getQuestion_type().equals("Free Text")) {
            if (!this.editText.getText().toString().isEmpty()) {
                return this.editText.getText().toString();
            }
        } else if (this.getQuestion_type().equals("Both")) {
            String ans = "";
            RadioButton selectedRadioButton = this.radioGroup.findViewById(this.radioGroup.getCheckedRadioButtonId());
            if (selectedRadioButton != null)
                ans = selectedRadioButton.getText().toString();
            if (!this.editText.getText().toString().isEmpty()) {
                if (!TextUtils.isEmpty(ans))
                    ans += ", ";
                ans += this.editText.getText().toString();
            }
            return ans;
        }
        return "";
    }


    public String getAnswerIds() {
        if (this.getQuestion_type().equals("Single Choice") || this.getQuestion_type().equals("Both")) {
            RadioButton selectedRadioButton = this.radioGroup.findViewById(this.radioGroup.getCheckedRadioButtonId());
            if (selectedRadioButton != null)
                return selectedRadioButton.getTag().toString();
        } else if (this.getQuestion_type().equals("Multiple Choice")) {
            return android.text.TextUtils.join(",", this.selectedCheckBoxIndexs);
        } else if (this.getQuestion_type().equals("Free Text")) {
            if (!this.editText.getText().toString().isEmpty()) {
                return this.editText.getText().toString();
            }
        }
        return "";
    }

}
