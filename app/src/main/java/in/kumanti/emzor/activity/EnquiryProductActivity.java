package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.reports.EnquiryReportsActivity;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.InvoiceBatch;
import in.kumanti.emzor.model.InvoiceProductItems;
import in.kumanti.emzor.model.Invoices;
import in.kumanti.emzor.model.OrderDetailsList;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EnquiryProductActivity extends MainActivity {

    Spinner Customer_Name, Product;
    ImageView createSignature, viewSignature;
    Bitmap selectedImage = null;
    Button invoiceAddProductButton;
    int line_count;
    PopupMenu popupMenu;
    ImageView back, invoiceCancelButton, info, delivery_date_calendar, customerImage, sr_image, delivery_addrs, spinner_search, add_batch, invoiceSaveButton;
    TextView header_txt, total_value, net_value, accountNo;
    ImageView actionbarBackButton, deviceInfo;
    String customerNameString, customerEmailAddress;
    String customerAddress1, customerAddress2, customerAddress3;
    ArrayList<InvoiceProductItems> invoiceProductItems;
    InvoiceProductItems invoiceProductItems1;
    String sales_rep_id, invoice_id = null;
    DatePickerDialog.OnDateSetListener fdate;
    Calendar myCalendar = Calendar.getInstance();
    TextView customerName, city, checkintime1, balance, invoice_date, invoice_num, sr_name;
    EditText  total_discount, signatureName;
    int total_discount1;
    String format_paid_value;
    String format_balance_value;
    String customer_id = "", login_id;
    Globals globals;
    String checkin_time = "", invoiceTime = "";
    String bal;
    String salesinvoice_date, save_salesorder_date, sr_name1;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    String sr_name_details, sr_rm_name, sr_region, sr_warehouse;
    InvoiceProductListAdapter adapter = null;
    Chronometer chronometer;
    long stopTime = 0;
    DecimalFormat formatter, formatter1;
    String format_order_value, format_total_value, format_discount_value;
    ListView batch_list;
    ArrayList<InvoiceBatch> productbatchLists;
    InvoiceBatch batchlist;
//    CheckBox paid_change, account_change;
    SharedPreferenceManager sharedPreferenceManager;
    Double price_val_update = 0.00;
    Double order_val_update = 0.00;
    int reciept_id = 0;
//    double paid_amount = 0.00;
    double balance_amount, due_amount = 0.00;
    String change_type;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng, nav_type, invoice_number, customer_type;
    double order_val = 0.00;
    double total_val = 0.00;
    int discount_value = 0;
    LinearLayout btn_section, checkin_section;
    double price = 0.00;
    int discount = 0;
    String collection_num, collection_type, tab_prefix;
//    double pay_amount, paid_amt = 0.00;
    String emailType = "Invoice";
    String companyName, companyCode1,compAdd1, compAdd2, compAdd3, compMblNo;
//    String balanceReturned = "", balanceReturnedVal = "", balanceReturnedKey = "";
    //Variables declaration for print and sms
    Button smsButton, printButton, emailButton;
    String customerPhoneNumber = "";
    String smsMessageText = "";
    // android built in classes for bluetooth operations
    PrintBluetooth printBluetooth;
    Print print;
    String printerName = "MP80-17031810239";
    String printMsg;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    String invoiceStatus = "";
    GeneralSettingsTable generalSettingsTable;
    MyDBHandler dbHandler;
    GeneralSettings gs;
    String saveFlag;
    String invoiceType;
    int allocate_qty = 0;
    int remaining_qty = 0;
    int redCount = 0;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private TextView marqueeText, actionBarTitle;
    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";
    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_enquiry_product);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText("View Enquiry");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        getlocation();

        setInvoiceDate();




        if ("Reports".equals(nav_type)) {


            invoiceSaveButton.setVisibility(View.GONE);
            invoiceAddProductButton.setVisibility(View.GONE);
            invoiceCancelButton.setVisibility(View.GONE);

            total_discount.setEnabled(false);

            createSignature.setEnabled(false);
            checkin_section.setVisibility(View.GONE);


        }


        line_count = dbHandler.get_count_invoice_lines(invoice_id);

        final int discount_val = dbHandler.get_discount_value(invoice_id);
        //format_discount_value = formatter.format(discount_val);

        total_discount1 = discount_val;

        order_val = dbHandler.get_invoice_value(invoice_id);
        format_order_value = formatter.format(order_val);

        total_val = dbHandler.get_total_value(invoice_id);
        format_total_value = formatter.format(total_val);

        total_value.setText("10,580.00");
        total_discount.setText(String.valueOf(total_discount1));
        net_value.setText("10,580.00");


        total_discount.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (total_discount.getText().toString().equals("")) {
                    discount_value = 0;
                    order_val = total_val - discount_value;
                    format_order_value = formatter.format(order_val);
                    net_value.setText(format_order_value);

                } else {

                    discount_value = Integer.parseInt(total_discount.getText().toString());
                    order_val = total_val - discount_value;
                    format_order_value = formatter.format(order_val);
                    net_value.setText(format_order_value);
                }
            }
        });


        sr_image.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                showsrdetails();
                return true;
            }
        });

        customerImage.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                showcustomerLocation();
                return true;
            }
        });


        createSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewSignature();
            }
        });


        viewSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedImage != null) {
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.image_preview, null);
                    ImageView capturedImage, closeButton;
                    capturedImage = alertLayout.findViewById(R.id.capturedImage);
                    closeButton = alertLayout.findViewById(R.id.closeImageView);
                    capturedImage.setImageBitmap(selectedImage);
                    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    // this is set the view from XML inside AlertDialog

                    alert.setView(alertLayout);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(true);

                    android.app.AlertDialog dialog = alert.create();
                    dialog.show();


                }

            }
        });

        /*
         * Sending SMS Manager API using default SMS manager
         */


        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!invoiceStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save invoice to send sms", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(customerPhoneNumber)) {
                    Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                    return;
                }
//                calculateBalanceReturn();

                try {
                    smsMessageText = "Sale Invoice\n" +
                            "\n" +
                            "Invoice No: #" + invoice_number + "\n" +
                            "Invoice Date: " + invoice_date.getText().toString() + "\n" +
                            "Invoice Time: " + invoiceTime + "\n" +
                            "Invoice Lines: " + line_count + "\n" +


                    Log.d("SMSTEST", "TESTMESSAGE" + smsMessageText);

                    SmsManager smsManager = SmsManager.getDefault();
                    ArrayList<String> parts = smsManager.divideMessage(smsMessageText);
                    //smsManager.sendTextMessage(phoneNumber, null, message, null, null);
                    smsManager.sendMultipartTextMessage(customerPhoneNumber, null, parts,
                            null, null);
                    //smsManager.sendTextMessage(customerPhoneNumber, null, smsMessageText, null, null);

                    Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.d("Sms", "Exception" + e);
                }

            }
        });

        /*
         * Print the order
         */


        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!invoiceStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save invoice to print", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
//                    calculateBalanceReturn();
                    int totalSize = 48;
                    String ESC_NEW_LINE = "\n";
                    String horizontalLine = "-----------------------------------------------";
                    printMsg = "";
                    printMsg += print.centerString(totalSize, companyName);
                    printMsg += print.centerString(totalSize, compAdd1);
                    printMsg += print.centerString(totalSize, compAdd2);
                    printMsg += print.centerString(totalSize, compAdd3);
                    printMsg += print.centerString(totalSize, compMblNo);

                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + "Invoice Number:" + invoice_number;
                    printMsg += ESC_NEW_LINE + print.padRight("Date:" + invoice_date.getText(), 24) + print.padLeft("TIME:" + invoiceTime, 24);
                    printMsg += "BDE Name:" + sr_name_details;
                    printMsg += ESC_NEW_LINE + "Customer Name:" + customerNameString;
                    printMsg += ESC_NEW_LINE + "Address:";
                    printMsg += print.padRight(customerAddress1, totalSize);
                    printMsg += print.padRight(customerAddress2, totalSize);
                    printMsg += print.padRight(customerAddress3, totalSize);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += print.centerString(15, "Product Name") + " " + "  VOL " + print.padLeft("PRICE", 10) + "  " + print.padLeft("VALUE", 14);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    double total = 0;
                    int i = 1;
                    for (InvoiceProductItems ls : invoiceProductItems) {
                        //printMsg += print.padLeft(String.valueOf(i)+".",3)+print.padRight(ls.getProduct().substring(0,11),12)+" "+print.padLeft(String.valueOf(ls.getQuantity()),5)+" "+print.padLeft(ls.getPrice(),10)+"  "+print.padLeft(ls.getValue(),14);
                        printMsg += print.padLeft(String.valueOf(i) + ".", 3) + print.padRight(ls.getProduct().substring(0, 11), 12) + " " + print.padLeft(String.valueOf(ls.getQuantity()), 5) + " " + print.padLeft(ls.getPrice(), 10) + "  " + print.padLeft(formatter.format((ls.getPrice_db() * Double.parseDouble(ls.getQuantity()))), 14);
                        total += (ls.getPrice_db() * Double.parseDouble(ls.getQuantity()));
                        i++;
                    }

                    double vatValue = total_val - total;
                    Log.d("VatTest", "OrderVat" + vatValue + "," + total_val + "," + total);


                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                  /*  printMsg += print.centerString(34,"Total Value:")+print.padLeft(String.valueOf(total),14);
                    printMsg += print.centerString(34,"VAT:")+print.padLeft(formatter.format(vatValue),14);
                    printMsg += print.centerString(34,"Discounts:")+print.padLeft(String.valueOf(total_discount1),14);
                    printMsg += print.centerString(35,"Net Value:")+print.padLeft(net_value.getText().toString(),13);*/

                    printMsg += print.padRight("", 15) + print.padRight("Total Value:", 19) + print.padLeft(String.valueOf(formatter.format(total)), 14);
                    printMsg += print.padRight("", 15) + print.padRight("VAT:", 19) + print.padLeft(formatter.format(vatValue), 14);
                    printMsg += print.padRight("", 15) + print.padRight("Discounts:", 19) + print.padLeft(String.valueOf(total_discount1), 14);
                    printMsg += print.padRight("", 15) + print.padRight("Net Value:", 19) + print.padLeft(net_value.getText().toString(), 14);


                    printMsg += ESC_NEW_LINE + horizontalLine;


                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.padLeft("Customer Sign", totalSize);
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    print.printContent(printMsg);
                    //printBluetooth.printContent(printMsg, selectedImage);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });


        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!invoiceStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save invoice to send email", Toast.LENGTH_LONG).show();
                    return;
                }
//                calculateBalanceReturn();

                double total = 0;
                int i = 1;
                for (InvoiceProductItems ls : invoiceProductItems) {
                    total += (ls.getPrice_db() * Double.parseDouble(ls.getQuantity()));
                    i++;
                }

                double vatValue = total_val - total;

                // double vatValue = Double.parseDouble(total_value.toString())-total;


                Gson gson = new GsonBuilder().setLenient().create();
                JsonArray invoiceJsonArray = gson.toJsonTree(invoiceProductItems).getAsJsonArray();

                JsonObject jp = new JsonObject();

                jp.addProperty("emailType", emailType);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);
                jp.addProperty("invoiceNumber", invoice_number);
                jp.addProperty("date", invoice_date.getText().toString());
                jp.addProperty("time", invoiceTime);
                jp.addProperty("bdeName", sr_name_details);
                jp.addProperty("customerName", customerNameString);
                jp.addProperty("customerEmailAddress", customerEmailAddress);
                jp.addProperty("address", customerAddress1 + customerAddress2);
                jp.add("invoiceLines", invoiceJsonArray);
                jp.addProperty("totalValue", String.valueOf(total));
                jp.addProperty("vat", formatter.format(vatValue));
                jp.addProperty("discounts", String.valueOf(total_discount1));
                jp.addProperty("netValue", net_value.getText().toString());



                if (selectedImage != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                    jp.addProperty("customerSignature", encodedImage);
                }
                Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingEmailData(jp);

                Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND:", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }
                });
            }
        });


        invoiceCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog alertbox = new Dialog(v.getRootView().getContext());

                LayoutInflater mInflater = (LayoutInflater)
                        getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                final View alertLayout = mInflater.inflate(R.layout.activity_exit_invoice_popup, null);

                alertbox.setCancelable(false);
                alertbox.setContentView(alertLayout);

                final Button Yes = alertLayout.findViewById(R.id.Yes);
                final Button No = alertLayout.findViewById(R.id.No);

                Yes.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        update_invoice_status("Cancelled");
                        Toast.makeText(getApplicationContext(), "Invoice Cancelled", Toast.LENGTH_LONG).show();

                    }

                });

                No.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        alertbox.dismiss();
                    }

                });

                alertbox.show();

            }
        });


        invoiceAddProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myintent = new Intent(getApplicationContext(), InvoiceActivity.class);
                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                chronometer.stop();
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customer_id);
                bundle.putInt("invoice_id", Integer.parseInt(invoice_id));
                bundle.putString("order_type", "Lines");
                bundle.putString("login_id", login_id);
                bundle.putString("invoice_num", invoice_num.getText().toString());
                bundle.putString("customer_type", customer_type);
                bundle.putString("invoice_type", invoiceType);

                myintent.putExtras(bundle);
                startActivity(myintent);
            }
        });

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Reports".equals(nav_type)) {
                    finish();
                } else if ((invoiceStatus.equals("Completed")) || (invoiceStatus.equals("SecondarySalesOrder"))) {
                    Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    chronometer.stop();
                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("sales_rep_id", login_id);
                    bundle.putString("customer_id", customer_id);
                    bundle.putString("nav_type", "Home");

                    myintent.putExtras(bundle);
                    startActivity(myintent);
                    return;
                } else {
                    finish();
                    /*Intent myintent = new Intent(getApplicationContext(), InvoiceActivity.class);
                    long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    chronometer.stop();
                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", customer_id);
                    bundle.putInt("invoice_id", Integer.parseInt(invoice_id));
                    bundle.putString("order_type", "Lines");
                    bundle.putString("login_id", login_id);
                    bundle.putString("invoice_num", invoice_num.getText().toString());
                    bundle.putString("customer_type", customer_type);
                    bundle.putString("invoice_type", invoiceType);

                    myintent.putExtras(bundle);
                    startActivity(myintent);*/
                }

            }
        });






        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        invoiceSaveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if ("Doctor".equals(customer_type)) {
                    popupMenu = new PopupMenu(EnquiryProductActivity.this, v);
                    MenuInflater inflater = popupMenu.getMenuInflater();
                    inflater.inflate(R.menu.popup_menu_saveinvoice, popupMenu.getMenu());
                    popupMenu.setOnDismissListener(new OnDismissListener());
                    popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
                    Menu m = popupMenu.getMenu();
                    m.removeItem((R.id.saveSecondarySalesOrder));
                    popupMenu.show();


                } else {
                    popupMenu = new PopupMenu(EnquiryProductActivity.this, v);
                    MenuInflater inflater = popupMenu.getMenuInflater();
                    inflater.inflate(R.menu.popup_menu_saveinvoice, popupMenu.getMenu());
                    popupMenu.setOnDismissListener(new OnDismissListener());
                    popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
                    // popupMenu.inflate(R.menu.popup_menu_saveinvoice);
                    popupMenu.show();
                }


            }

        });

        invoiceProductItems = new ArrayList<InvoiceProductItems>();






        InvoiceProductItems s1 = new InvoiceProductItems("Dolobest", "PACK", "25", "400", "10000", "1", "A001", "0", 400,10000, 0,"No");
        InvoiceProductItems s2 = new InvoiceProductItems("Coatz AF Syrup", "PACK", "1", "580", "580", "2", "A002", "0", 580, 580,0,"No");
        invoiceProductItems.add(s1);
        invoiceProductItems.add(s2);

            System.out.println("TT::enquiryDetailsListArrayList = " + invoiceProductItems);

            adapter = new InvoiceProductListAdapter(getApplicationContext(), R.layout.list_enquiry_products, invoiceProductItems, "1", checkin_time, "CUS001");
            ListView list_sales_line = findViewById(R.id.list_sales_line);
            list_sales_line.setAdapter(adapter);
            //adapter.refreshEvents(invoiceProductItems);


    }

    private void setInvoiceDate() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdfd = new SimpleDateFormat("dd MMM yy");
        SimpleDateFormat sdft = new SimpleDateFormat("hh:mm");
        salesinvoice_date = sdfd.format(date);
        invoice_date.setText(salesinvoice_date);
        invoice_num.setText(invoice_number);
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        save_salesorder_date = timeStampFormat.format(myDate);

    }

    private void loadproductbatchlist(int invoice_id, int quantity, String product_id) {

        productbatchLists = new ArrayList<InvoiceBatch>();
        Cursor data = dbHandler.getProductBatchDetails(product_id);
        int numRows1 = data.getCount();
        if (numRows1 == 0) {

        } else {
            int i = 0;
            while (data.moveToNext()) {


                long id2 = dbHandler.insert_product_batch
                        (data.getString(0),
                                data.getString(1),
                                data.getString(2),
                                data.getString(3),
                                null,
                                null,
                                String.valueOf(invoice_id),
                                "Draft"
                        );

            }

            i++;


        }

    }

    private void loadproductbatchlist_batch(int order_qty, String product_id) {

        Cursor data = dbHandler.getProductBatchDetails2(product_id, String.valueOf(invoice_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {

        } else {
            int i = 0;
            allocate_qty = order_qty;
            while (data.moveToNext()) {

                int onhand_qty = data.getInt(2);
                if (onhand_qty >= allocate_qty) {
                    // remaining_qty=onhand_qty-allocate_qty;

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(allocate_qty), data.getString(0), String.valueOf(invoice_id));
                    //dbHandler.updatebatchqty(String.valueOf(invoice_id), product_id, data.getString(1));

                    break;

                } else {

                    remaining_qty = allocate_qty - onhand_qty;
                    Log.i("remaining_qty", String.valueOf(remaining_qty));

                    int test = allocate_qty;
                    if (allocate_qty <= onhand_qty) {
                        test = allocate_qty;
                    } else {
                        test = onhand_qty;
                    }

                    dbHandler.updateInvoiceBatch(product_id, String.valueOf(test), data.getString(0), String.valueOf(invoice_id));
                    allocate_qty = remaining_qty;
                }
                i++;
            }

        }
    }

    private void onInvoiceSave() {

        if (invoiceProductItems.size() != 0) {

            invoiceProductItems = new ArrayList<InvoiceProductItems>();
            Cursor data1 = dbHandler.getInvoiceLines(invoice_id);
            int numRows2 = data1.getCount();
            if (numRows2 == 0) {
                //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
            } else {
                int i = 0;
                while (data1.moveToNext()) {

                    String formatted_price, formatted_value;
                    if (data1.getDouble(3) == 0) {
                        formatted_price = "0";

                    } else {
                        formatted_price = formatter.format(data1.getDouble(3));

                    }

                    if (data1.getDouble(4) == 0) {
                        formatted_value = "0";

                    } else {
                        formatted_value = formatter.format(data1.getDouble(4));

                    }

                    invoiceProductItems1 = new InvoiceProductItems(data1.getString(0), data1.getString(1), data1.getString(2), formatted_price, formatted_value, data1.getString(5), data1.getString(6), data1.getString(7), data1.getDouble(3), data1.getDouble(4), data1.getInt(8), data1.getString(9));
                    invoiceProductItems.add(i, invoiceProductItems1);
                    i++;

                    if ("Invoice".equals(invoiceType)) {

                    } else {
                        dbHandler.deletebatch(data1.getString(6));
                        loadproductbatchlist(Integer.parseInt(invoice_id), data1.getInt(2), data1.getString(6));
                        loadproductbatchlist_batch(data1.getInt(2), data1.getString(6));
                        //loadproductbatchlist1();
                        dbHandler.updatebatchstatus(Integer.valueOf(invoice_id), "Inprocess");
                    }
                }

                adapter = new InvoiceProductListAdapter(getApplicationContext(), R.layout.list_invoice_products, invoiceProductItems, invoice_id, checkin_time, customer_id);
                ListView list_sales_line = findViewById(R.id.list_sales_line);
                list_sales_line.setAdapter(adapter);
                //adapter.refreshEvents(invoiceProductItems);
            }

            if ("False".equals(saveFlag)) {
                Toast.makeText(getApplicationContext(), "Please check the quantities for the rows highlighted in red", Toast.LENGTH_LONG).show();

            } else {

                GeneralSettings gs = generalSettingsTable.getSettingByKey("invoiceSignature");

                if (gs != null && gs.getValue().equals("Yes")) {


                }




                if (line_count == 0) {
                    Toast.makeText(getApplicationContext(), "Add Product to Save the Invoice", Toast.LENGTH_LONG).show();

                } else {





                    Cursor data = dbHandler.getInvoiceproductLines(invoice_id);
                    //reciept_id = dbHandler.get_reciept_id();
                    //int rec_num = reciept_id + 1;

                    int rec_random_num = dbHandler.get_receipt_random_num();

                    SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
                    Date myDate = new Date();
                    String rec_seq_date = timeStampFormat.format(myDate);
                    String receipt_num;
                    rec_random_num = rec_random_num + 1;

                    if (rec_random_num <= 9) {
                        receipt_num = tab_prefix + "SR" + rec_seq_date + "00" + rec_random_num;

                    } else if (rec_random_num > 9 & random_num < 99) {
                        receipt_num = tab_prefix + "SR" + rec_seq_date + "0" + rec_random_num;

                    } else {
                        receipt_num = tab_prefix + "SR" + rec_seq_date + rec_random_num;
                    }

                    int numRows1 = data.getCount();
                    if (numRows1 == 0) {
                    } else {
                        int i = 0;
                        while (data.moveToNext()) {

                            String visitSequence = sharedPreferences.getString("VisitSequence", "");

                            long id = dbHandler.createstockreciept
                                    (receipt_num,
                                            save_salesorder_date,
                                            "NGN",
                                            login_id,
                                            null,
                                            "Issue",
                                            "",
                                            data.getString(0),
                                            data.getString(1),
                                            data.getString(2),
                                            null,
                                            null,
                                            "",
                                            invoice_id,
                                            "Completed",
                                            rec_random_num,
                                            visitSequence,
                                            invoice_number,
                                            "");

                            if (id <= 0) {
                                Toast.makeText(getApplicationContext(), "Error.. Please try again", Toast.LENGTH_LONG).show();

                            } else {


                            }
                        }
                    }


                    dbHandler.deletebatchrow(Integer.valueOf(invoice_id));
                    dbHandler.updatebatchstatus(Integer.valueOf(invoice_id), "Completed");
                    dbHandler.updateInvoiceBatchquantity(invoice_id);
                    //dbHandler.update_invoice_sequence(Integer.valueOf(invoice_id));
                    invoiceStatus = "Completed";

                    Toast.makeText(getApplicationContext(), "Invoice Created", Toast.LENGTH_LONG).show();

                    if ("Doctor".equals(customer_type)) {
//                        dbHandler.update_invoice_header_status(invoice_id, "FOC", Double.parseDouble(paid.getText().toString()), balance_amount, change_type, order_val, signatureName.getText().toString());
                        invoiceStatus = "FOC";
                        Intent myintent = new Intent(getApplicationContext(), DoctorVisitActivity.class);
                        long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                        SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                        chronometer.stop();
                        //Create the bundle
                        Bundle bundle = new Bundle();
                        //Add your data to bundle
                        bundle.putString("checkin_time", checkin_time);
                        bundle.putString("customer_id", customer_id);
                        bundle.putString("login_id", login_id);

                        //Add the bundle to the intent
                        myintent.putExtras(bundle);

                        //Fire that second activity
                        startActivity(myintent);
                    } else {
                                /*Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                                chronometer.stop();
                                //Create the bundle
                                Bundle bundle = new Bundle();
                                //Add your data to bundle
                                bundle.putString("checkin_time", checkin_time);
                                bundle.putString("customer_id", customer_id);
                                bundle.putString("sales_rep_id", login_id);
                                bundle.putString("nav_type", "Home");

                                //Add the bundle to the intent
                                myintent.putExtras(bundle);

                                //Fire that second activity
                                startActivity(myintent);*/
                    }

                    invoiceSaveButton.setEnabled(false);
                    invoiceCancelButton.setEnabled(false);
                    invoiceAddProductButton.setEnabled(false);
                    invoiceSaveButton.setVisibility(View.GONE);
                    invoiceCancelButton.setVisibility(View.GONE);
                    invoiceAddProductButton.setVisibility(View.GONE);
                }

            }


        } else {

        }


    }

    private void createNewSignature() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);
        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                selectedImage = signatureBitmap;
                viewSignature.setImageBitmap(signatureBitmap);
                dialog.dismiss();

                //Posting the customer Signature

                //FileUtils fs = new FileUtils();
                //fs.fileFTPUpload("artifacts/BarChart.jpeg", "customerSignature/BarChart.jpeg");

            }
        });

    }

//    void calculateBalanceReturn() {
//        balanceReturned = "";
//        balanceReturnedKey = "";
//        balanceReturnedVal = "";
//        double bal_val = balance_amount;
//        if (bal_val < 0)
//            bal_val = bal_val * (-1);
//        if (paid_change.isChecked()) {
//            balanceReturned = "Change Returned: " + String.valueOf(formatter.format(bal_val));
//            balanceReturnedKey = "changeReturn";
//            balanceReturnedVal = String.valueOf(formatter.format(bal_val));
//        } else if (account_change.isChecked()) {
//            balanceReturned = "Account Adjusted: " + String.valueOf(formatter.format(bal_val));
//            balanceReturnedKey = "accountAdjusted";
//            balanceReturnedVal = String.valueOf(formatter.format(bal_val));
//        }
//    }



    private void insert_unapplied_receipts() {

        int collection_random_num = dbHandler.get_collection_random_num("Secondary");
        int collection_id = dbHandler.get_collection_sequence_value("Secondary");
        collection_id = collection_id + 1;
        collection_random_num = collection_random_num + 1;

        SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("ddMMyy");
        Date myDate1 = new Date();
        String collection_seq_date = timeStampFormat1.format(myDate1);

        if (collection_random_num <= 9) {
            collection_num = tab_prefix + "SC" + collection_seq_date + "00" + collection_random_num;

        } else if (collection_random_num > 10 & collection_random_num < 99) {
            collection_num = tab_prefix + "SC" + collection_seq_date + "0" + collection_random_num;

        } else {
            collection_num = tab_prefix + "SC" + collection_seq_date + collection_random_num;
        }


        long id1 = dbHandler.insert_collection_details
                (login_id,
                        customer_id,
                        collection_id,
                        collection_num,
                        save_salesorder_date,
                        0.00,
                        0.00,
                        0.00,
                        "On Account",
                        -1 * balance_amount,
                        null,
                        "Cash",
                        "",
                        "",
                        "",
                        "",
                        "",
                        String.valueOf(collection_random_num),
                        "",
                        "",
                        "",
                        "",
                        save_salesorder_date,
                        Constants.VISIT_SEQ_NUMBER,
                        "Invoice",
                        null
                );

        dbHandler.update_collection_sequence(collection_id);

    }

    private void getlocation() {


        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            String lat = Double.toString(latitude);
            String lng = Double.toString(longitude);

            lat_lng = lat + "," + lng;

        } else {

            gpsTracker.showSettingsAlert();
        }
    }

    private void update_invoice_status(String status) {
        dbHandler.update_invoice_header_status(String.valueOf(invoice_id), status, 0.00, 0.00, null, 0.00, null);
        dbHandler.update_invoice_detail_status(String.valueOf(invoice_id), status);
        dbHandler.updatebatchstatus(Integer.valueOf(invoice_id), status);

        if ("Doctor".equals(customer_type)) {

            Intent myintent = new Intent(getApplicationContext(), DoctorVisitActivity.class);
            //Create the bundle
            Bundle bundle = new Bundle();
            //Add your data to bundle
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        } else {
            Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);

            //Create the bundle
            Bundle bundle = new Bundle();
            //Add your data to bundle
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("sales_rep_id", login_id);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        }

    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public void paidclicked(View v) {
        //code to check if this checkbox is checked!
        CheckBox checkBox = (CheckBox) v;
        if (checkBox.isChecked()) {
            change_type = "Paid";
        }
    }

    public void accountclicked(View v) {
        //code to check if this checkbox is checked!
        CheckBox checkBox = (CheckBox) v;
        if (checkBox.isChecked()) {
            change_type = "Account";

        }
    }

    private void initializeViews() {

        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);
        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        total_value = findViewById(R.id.total_value);
        net_value = findViewById(R.id.net_value);
        total_discount = findViewById(R.id.discount);



        invoice_date = findViewById(R.id.invoice_date);
        invoice_num = findViewById(R.id.invoice_num);

        createSignature = findViewById(R.id.CreateSignImageView);
        viewSignature = findViewById(R.id.ViewSignatureImageView);

        checkintime1 = findViewById(R.id.checkintime1);
        sr_image = findViewById(R.id.sr_image);
        sr_name = findViewById(R.id.sr_name);
        chronometer = findViewById(R.id.txt_duration);

        btn_section = findViewById(R.id.btn_section);
        checkin_section = findViewById(R.id.checkin_section);

        signatureName = findViewById(R.id.signCustomerNameEditText);

        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("####.00");

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        tab_prefix = Constants.TAB_PREFIX;


        globals = ((Globals) getApplicationContext());

        printBluetooth = new PrintBluetooth(this);

        print = new Print(this);
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());

        invoiceSaveButton = findViewById(R.id.invoiceSaveButton);
        invoiceCancelButton = findViewById(R.id.invoiceCancelButton);
        invoiceAddProductButton = findViewById(R.id.invoiceAddProductButton);
        printButton = findViewById(R.id.invoicePrintButton);
        emailButton = findViewById(R.id.invoiceEmailButton);
        smsButton = findViewById(R.id.invoiceSmsButton);

        gs = generalSettingsTable.getSettingByKey("enablePrint");
        if (gs != null && gs.getValue().equals("Yes")) {
            printButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableSms");
        if (gs != null && gs.getValue().equals("Yes")) {
            smsButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableEmail");
        if (gs != null && gs.getValue().equals("Yes")) {
            emailButton.setVisibility(View.VISIBLE);
        }

        signatureName.setFilters(new InputFilter[]{filter});


    }

    private void populateHeaderDetails() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        chronometer.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        chronometer.start();
        sr_name.setText(sr_name1);


        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            invoice_id = bundle.getString("invoice_id");
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            nav_type = bundle.getString("nav_type");
            invoice_number = bundle.getString("invoice_num");
            customer_type = bundle.getString("customer_type");
            price = bundle.getDouble("price");
            invoiceType = bundle.getString("invoice_type");

        }

        checkintime1.setText(checkin_time);

        //Populate SalesRep Details
        Log.d("Invoice", "Login---" + login_id);
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                //byte[] blob = salesRepDetails.getBlob(9);
                //Bitmap bmp= convertByteArrayToBitmap(blob);
                //sr_image.setImageBitmap(bmp);
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }


        //Populate Header Details



                customerName.setText("General Hospital");
                customerNameString = ("General Hospital");
                customerEmailAddress = "General Hospital";
                city.setText("Lagos");
                accountNo.setText("CUTAB006040920001");


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                ship_address1 ="Lagos";
                ship_address2 = "Lagos";
                ship_address3 = "Lagos";
                ship_city1 = "Lagos";
                ship_state1 ="Lagos";
                ship_country1 = "Lagos";


                customerAddress1 = "Lagos";
                customerAddress2 = "Lagos";
                customerAddress3 = "Lagos";
                customerPhoneNumber = "Lagos";




        getCompanyInfo();

        Invoices invoice = dbHandler.getinvoiceHeader(invoice_number);
        try {
            String dateTime = invoice.getCreatedAt();

            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("HH:ss");
            Date myDate = new Date(Long.parseLong(dateTime) * 1000L);
            invoiceTime = dateFormat.format(myDate);
            //invoiceType=invoice.getStatus();
        } catch (Exception e) {
            // ledger.setInvoiceDate(cursor.getString(3));
        }

    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);

            }
        }

    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    public void showdeliveryaddressdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_delivery_details, null);
        final TextView ship_add1 = alertLayout.findViewById(R.id.ship_add1);
        final TextView ship_add2 = alertLayout.findViewById(R.id.ship_add2);
        final TextView ship_add3 = alertLayout.findViewById(R.id.ship_add3);
        final TextView ship_city = alertLayout.findViewById(R.id.ship_city);
        final TextView ship_state = alertLayout.findViewById(R.id.ship_state);
        final TextView ship_country = alertLayout.findViewById(R.id.ship_country);

        ship_add1.setText(ship_address1);
        ship_add2.setText(ship_address2);
        ship_add3.setText(ship_address3);
        ship_city.setText(ship_city1);
        ship_state.setText(ship_state1);
        ship_country.setText(ship_country1);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void showsrdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_sr_details, null);
        final TextView sr_name = alertLayout.findViewById(R.id.sr_name_details);
        final TextView sr_rm = alertLayout.findViewById(R.id.sr_rm_name);
        final TextView sr_rgn = alertLayout.findViewById(R.id.sr_region);
        final TextView sr_whouse = alertLayout.findViewById(R.id.sr_warehouse);
        final TextView sr_gps = alertLayout.findViewById(R.id.sr_gps);

        sr_name.setText(sr_name_details);
        sr_rm.setText(sr_rm_name);
        sr_rgn.setText(sr_region);
        sr_whouse.setText(sr_warehouse);
        sr_gps.setText(lat_lng);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);

        printBluetooth.onActivityResult(requestCode, resultCode, data, printMsg);
    }

    private void onSecondarySalesOrderSave() {

        dbHandler.update_invoice_header_status(invoice_id, "Secondary Sales Order", 0, 0, "", order_val, null);
        //dbHandler.update_invoice_sequence(Integer.valueOf(invoice_id));
        invoiceStatus = "SecondarySalesOrder";
        total_discount.setEnabled(false);

        Toast.makeText(getApplicationContext(), "Secondary Sales Order Created", Toast.LENGTH_LONG).show();

        if ("Doctor".equals(customer_type)) {

            Intent myintent = new Intent(getApplicationContext(), DoctorVisitActivity.class);
            long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            chronometer.stop();
            //Create the bundle
            Bundle bundle = new Bundle();
            //Add your data to bundle
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("login_id", login_id);

            //Add the bundle to the intent
            myintent.putExtras(bundle);

            //Fire that second activity
            startActivity(myintent);
        } else {
                                /*Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                                long timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
                                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                                chronometer.stop();
                                //Create the bundle
                                Bundle bundle = new Bundle();
                                //Add your data to bundle
                                bundle.putString("checkin_time", checkin_time);
                                bundle.putString("customer_id", customer_id);
                                bundle.putString("sales_rep_id", login_id);
                                bundle.putString("nav_type", "Home");

                                //Add the bundle to the intent
                                myintent.putExtras(bundle);

                                //Fire that second activity
                                startActivity(myintent);*/
        }

        invoiceSaveButton.setEnabled(false);
        invoiceCancelButton.setEnabled(false);
        invoiceAddProductButton.setEnabled(false);

        invoiceSaveButton.setVisibility(View.GONE);
        invoiceCancelButton.setVisibility(View.GONE);
        invoiceAddProductButton.setVisibility(View.GONE);


    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    public class InvoiceProductListAdapter extends ArrayAdapter<InvoiceProductItems> {

        InvoiceProductItems invoiceProductItems1;
        Activity mActivity = null;
        AlertDialog alertDialog1;
        String customer_id = "";
        String checkin_time = "";
        String invoice_id = "";
        String salesline_id = "";
        String trip_num = "";
        Globals globals;
        MyDBHandler dbHandler;
        String product_id = "";
        private LayoutInflater mInflater;
        private ArrayList<InvoiceProductItems> users;
        private int mViewResourceId;
        private Context c;

        public InvoiceProductListAdapter(Context context, int textViewResourceId, ArrayList<InvoiceProductItems> users, String invoice_id_val, String checkintime_val, String customer_id_val) {
            super(context, textViewResourceId, users);
            this.users = users;
            this.invoiceProductItems1 = invoiceProductItems1;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            invoice_id = invoice_id_val;
            checkin_time = checkintime_val;
            customer_id = customer_id_val;
            dbHandler = new MyDBHandler(c, null, null, 1);
            product_id = "";

        }


        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            convertView = mInflater.inflate(mViewResourceId, null);
            final InvoiceProductItems user = users.get(position);
            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.product = convertView.findViewById(R.id.product);
                viewHolder.uom = convertView.findViewById(R.id.uom);
                viewHolder.quantity = convertView.findViewById(R.id.quantity);
                viewHolder.price = convertView.findViewById(R.id.price);
                viewHolder.value = convertView.findViewById(R.id.value);
                viewHolder.vat = convertView.findViewById(R.id.vat);
                viewHolder.discount = convertView.findViewById(R.id.discount);

                if (viewHolder.product != null) {
                    viewHolder.product.setText(user.getProduct());
                }
                if (viewHolder.uom != null) {
                    viewHolder.uom.setText((user.getUom()));
                }
                if (viewHolder.quantity != null) {

                        viewHolder.quantity.setText((user.getQuantity()));


                }
                if (viewHolder.price != null) {
                    viewHolder.price.setText((user.getPrice()));
                }
                if (viewHolder.value != null) {
                    viewHolder.value.setText((user.getValue()));
                }
                if (viewHolder.vat != null) {
                    viewHolder.vat.setText((Integer.toString(user.getVat())));
                }
                if (viewHolder.discount != null) {
                    viewHolder.discount.setText((user.getDiscount()));
                }
            }











            return convertView;
        }



        public class ViewHolder {
            TextView product, uom, quantity, price, value, discount, vat;
        }

    }

    public class InvoiceBatchAdapter extends ArrayAdapter<InvoiceBatch> {

        Activity mActivity = null;
        AlertDialog alertDialog1;
        Globals globals;
        MyDBHandler dbHandler;
        ViewHolder viewHolder = null;
        String prod_id1;
        private LayoutInflater mInflater;
        private ArrayList<InvoiceBatch> productbatchLists;
        private int mViewResourceId;
        private Context c;


        public InvoiceBatchAdapter(Context context, int textViewResourceId, ArrayList<InvoiceBatch> productbatchLists, String prod_id) {
            super(context, textViewResourceId, productbatchLists);
            this.productbatchLists = productbatchLists;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            dbHandler = new MyDBHandler(c, null, null, 1);
            viewHolder = null;
            prod_id1 = prod_id;

        }

        public int getCount() {
            return productbatchLists.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public View getView(final int position, View convertView, ViewGroup parent) {


            convertView = mInflater.inflate(mViewResourceId, null);
            final InvoiceBatch user = productbatchLists.get(position);
            final int pos = position;

            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.expirydate = convertView.findViewById(R.id.batch_expirydate);
                viewHolder.batchnumber = convertView.findViewById(R.id.batch_batchnumber);
                viewHolder.quantity = convertView.findViewById(R.id.batch_quantity);
                viewHolder.invoices = convertView.findViewById(R.id.batch_invoices);


                if (viewHolder.expirydate != null) {
                    viewHolder.expirydate.setText(user.getBatchexpirydate());
                }
                if (viewHolder.batchnumber != null) {
                    viewHolder.batchnumber.setText((user.getBatchnumber()));
                }

                if (viewHolder.quantity != null) {
                    viewHolder.quantity.setText((user.getBatchquantity()));
                } else {
                    viewHolder.quantity.setText("0");
                }

                viewHolder.invoices.setText(productbatchLists.get(position).getBatchinvoice());
                viewHolder.invoices.setId(position);

                //we need to update adapter once we finish with editing
                viewHolder.invoices.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            final int position = v.getId();
                            final EditText Caption = (EditText) v;
                            String batchnumber = user.getBatchnumber();

                            //dbHandler.updateInvoiceBatch(prod_id1, Caption.getText().toString(), batchnumber, Integer.valueOf(invoice_id));


                        }
                    }
                });


            }

            return convertView;
        }


        public class ViewHolder {
            TextView expirydate, batchnumber, quantity;

            EditText invoices;

        }

        class ListItem {
            String caption;
        }


    }

    private class OnDismissListener implements PopupMenu.OnDismissListener {

        @Override
        public void onDismiss(PopupMenu menu) {

        }

    }

    private class OnMenuItemClickListener implements
            PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.saveInvoice:
                    onInvoiceSave();
                    return true;
                case R.id.saveSecondarySalesOrder:
                    onSecondarySalesOrderSave();
                    return true;

            }
            return false;
        }
    }
}
