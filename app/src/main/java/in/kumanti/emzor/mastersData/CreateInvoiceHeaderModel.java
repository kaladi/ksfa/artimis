package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class CreateInvoiceHeaderModel {

    private String company_code;
    private String tab_code;
    private String salesrep_id;
    private String customer_id;
    private String invoice_id;
    private String invoice_num;
    private String date;
    private String value;
    private String line_items;
    private String paid_amount;
    private String balance_amount;
    private String change_type;
    private String product_id;
    private String product_name;
    private String product_uom;
    private String quantity;
    private String price;
    private String line_value;
    private String discount;
    private String vat;
    private String vat_without_discount;
    private String value_with_vat;
    private String discount_value;


    public CreateInvoiceHeaderModel(String company_code1,
                                    String tab_code1,
                                    String salesrep_id1,
                                    String customer_id1,
                                    String invoice_id1,
                                    String invoice_num1,
                                    String date1,
                                    String value1,
                                    String line_items1,
                                    String paid_amount1,
                                    String balance_amount1,
                                    String change_type1,
                                    String prod_id1,
                                    String product_name1,
                                    String product_uom1,
                                    String quantity1,
                                    String price1,
                                    String line_value1,
                                    String discount1,
                                    String vat1,
                                    String vat_without_discount1,
                                    String value_with_vat1,
                                    String discount_value1
    ) {

        date = date1;
        invoice_num = invoice_num1;
        line_items = line_items1;
        value = value1;
        invoice_id = invoice_id1;
        customer_id = customer_id1;
        salesrep_id = salesrep_id1;
        paid_amount = paid_amount1;
        balance_amount = balance_amount1;
        change_type = change_type1;
        product_id = prod_id1;
        product_name = product_name1;
        product_uom = product_uom1;
        quantity = quantity1;
        price = price1;
        line_value = line_value1;
        discount = discount1;
        vat = vat1;
        vat_without_discount = vat_without_discount1;
        value_with_vat = value_with_vat1;
        discount_value = discount_value1;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code1) {
        company_code = company_code1;
    }

    public String getTab_code() {
        return tab_code;
    }

    public void setTab_code(String tab_code1) {
        tab_code = tab_code1;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date1) {
        date = date1;
    }

    public String getInvoice_num() {
        return invoice_num;
    }

    public void setInvoice_num(String invoice_num1) {
        invoice_num = invoice_num1;
    }

    public String getLine_items() {
        return line_items;
    }

    public void setLine_items(String line_items1) {
        line_items = line_items1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value1) {
        value = value1;
    }

    public String getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(String invoice_id1) {
        invoice_id = invoice_id1;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id1) {
        customer_id = customer_id1;
    }

    public String getSalesrep_id() {
        return salesrep_id;
    }

    public void setSalesrep_id(String salesrep_id1) {
        customer_id = salesrep_id1;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id1) {
        product_id = product_id1;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name1) {
        product_name = product_name1;
    }

    public String getProduct_uom() {
        return product_uom;
    }

    public void setProduct_uom(String product_uom1) {
        product_uom = product_uom1;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity1) {
        quantity = quantity1;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price1) {
        price = price1;
    }

    public String getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(String paid_amount1) {
        paid_amount = paid_amount1;
    }

    public String getBalance_amount() {
        return balance_amount;
    }

    public void setBalance_amount(String balance_amount1) {
        balance_amount = balance_amount1;
    }

    public String getChange_type() {
        return change_type;
    }

    public void setChange_type(String change_type1) {
        change_type = change_type1;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount1) {
        discount = discount1;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat1) {
        vat = vat1;
    }

    public String getVat_without_discount() {
        return vat_without_discount;
    }

    public void setVat_without_discount(String vat_without_discount1) {
        vat_without_discount = vat_without_discount1;
    }

    public String getLine_value() {
        return line_value;
    }

    public void setLine_value(String line_value1) {
        line_value = line_value1;
    }

    public String getValue_with_vat() {
        return value_with_vat;
    }

    public void setValue_with_vat(String value_with_vat1) {
        value_with_vat = value_with_vat1;
    }

    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value1) {
        discount_value = discount_value1;
    }

    @Override
    public String toString() {
        return invoice_num;
    }

}

