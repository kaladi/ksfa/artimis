package in.kumanti.emzor.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.CustomerReturnBatchAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CustomerReturnBatch;


public class CustomerReturnBatchActivity extends MainActivity {

    public RecyclerView batchRecyclerView;
    MyDBHandler myDBHandler;
    String login_id = "", product_id, product_name;
    int position = 0;
    Integer returnQuantity;
    ImageView actionbarBackButton, deviceInfo;
    ArrayList<CustomerReturnBatch> cusReturnBatchArrayList = new ArrayList<CustomerReturnBatch>();
    TextView productNameTv, productQuantityTv;
    ImageButton saveCusReturnBatchImageButton;
    private TextView marqueeText, actionbarTitle;
    private RecyclerView.Adapter batchAdapter;
    private RecyclerView.LayoutManager batchLayoutManager;

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_customer_return_batch);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Return Batch");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        //Populate the Quotation Product details using the Recycler view
        batchRecyclerView.setHasFixedSize(true);
        batchLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        batchRecyclerView.setLayoutManager(batchLayoutManager);

        try {

            batchAdapter = new CustomerReturnBatchAdapter(cusReturnBatchArrayList, null);
            batchRecyclerView.setAdapter(batchAdapter);

        } catch (Exception e) {
            Log.d("QueryDataResponse", "" + e.getMessage());
        }


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent output = new Intent();
                output.putExtra("product_id", product_id);
                output.putExtra("product_batch_details", cusReturnBatchArrayList);
                setResult(Activity.RESULT_OK, output);
                finish();
            }
        });

        saveCusReturnBatchImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(
                        Context.INPUT_METHOD_SERVICE);

                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
                if (validateInputs(0)) {
                    Log.d("InputError", "Invalid Inputs");
                    batchAdapter.notifyDataSetChanged();
                } else {
                    Intent output = new Intent();
                    output.putExtra("product_id", product_id);
                    output.putExtra("product_batch_details", cusReturnBatchArrayList);
                    setResult(Activity.RESULT_OK, output);
                    finish();
                }
            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);
            }
        });


    }

    private boolean validateInputs(int totalCountCheckFlag) {
        boolean resetflag = false;
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(
                Context.INPUT_METHOD_SERVICE);

        View focusedView = getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        for (CustomerReturnBatch crb : cusReturnBatchArrayList) {

            if (!TextUtils.isEmpty(crb.getReturnQuantity()) && (Integer.parseInt(crb.getBatchQuantity()) * -1) < Integer.parseInt(crb.getReturnQuantity())) {
                crb.setErrorReturnQuantity("Please enter return quantity less than product quantity");
                resetflag = true;
            } else
                crb.setErrorReturnQuantity("");

        }

        Integer currentTotalQuanitity = 0;

        for (CustomerReturnBatch crb : cusReturnBatchArrayList) {
            currentTotalQuanitity += Integer.parseInt(TextUtils.isEmpty(crb.getReturnQuantity()) ? "0" : crb.getReturnQuantity());
        }

        if (totalCountCheckFlag == 1 && currentTotalQuanitity == returnQuantity) {
            Toast.makeText(getApplicationContext(), "Total Quantity already Equal", Toast.LENGTH_SHORT).show();
            resetflag = true;
        }
        if (totalCountCheckFlag == 0 && currentTotalQuanitity < returnQuantity) {
            Toast.makeText(getApplicationContext(), "Sum of Batch Quantities Cannot be Less than Product Quantity", Toast.LENGTH_SHORT).show();
            resetflag = true;
        }
        if (currentTotalQuanitity > returnQuantity) {
            Toast.makeText(getApplicationContext(), "Sum of Batch Quantities Cannot be Greater than Product Quantity", Toast.LENGTH_SHORT).show();
            resetflag = true;
        }
        return resetflag;
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        batchRecyclerView = findViewById(R.id.cusReturnBatchRecyclerView);
        saveCusReturnBatchImageButton = findViewById(R.id.saveCusReturnBatchImageButton);
        productNameTv = findViewById(R.id.cusReturnBatchProductNameTv);
        productQuantityTv = findViewById(R.id.cusReturnBatchProductQtyTv);

        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            login_id = bundle.getString("login_id");
            //Log.d("LoginId",login_id);
            product_id = (String) bundle.get("product_id");
            product_name = (String) bundle.get("product_name");
            returnQuantity = Integer.parseInt((String) bundle.get("product_quantity"));
            cusReturnBatchArrayList = (ArrayList<CustomerReturnBatch>) bundle.get("product_batch_details");
            productNameTv.setText(product_name);
            productQuantityTv.setText(returnQuantity.toString());
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

}
