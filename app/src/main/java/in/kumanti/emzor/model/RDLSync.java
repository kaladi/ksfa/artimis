package in.kumanti.emzor.model;


public class RDLSync {

    public int rdlSyncId;
    public String postSyncDate;
    public String postSyncTime;
    public String createdAt;
    public String updatedAt;
    private String tabCode;
    private String companyCode;
    private String srCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public int getRdlSyncId() {
        return rdlSyncId;
    }

    public void setRdlSyncId(int rdlSyncId) {
        this.rdlSyncId = rdlSyncId;
    }

    public String getPostSyncDate() {
        return postSyncDate;
    }

    public void setPostSyncDate(String postSyncDate) {
        this.postSyncDate = postSyncDate;
    }

    public String getPostSyncTime() {
        return postSyncTime;
    }

    public void setPostSyncTime(String postSyncTime) {
        this.postSyncTime = postSyncTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
