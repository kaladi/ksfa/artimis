package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CustomerShippingAddress;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.SharedPreferenceManager;

public class CustomerInfoActivity extends MainActivity {

    MyDBHandler dbHandler;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView sr_name, actionBarTitle, marquee_txt;
    Chronometer duration;

    public EditText notesText;
    public ImageButton saveNotesButton, cancelNotesButton;

    ImageView customer_map;
//    ImageView btnAddInfo;
    String login_id = "", customerType = "", customer_id = "", checkin_time = "",credit_limit="0",creditDays="0";
    TextView customerName, credit_term, payment_term, accountNo, city, checkintime;
    TextView bill_add1, bill_add2, bill_add3, bill_city, bill_country, bill_state, ship_add1, ship_add2, ship_add3, ship_city, ship_state, ship_country;
    RadioButton Primary, Secondary, doctorRadioButton,Prospect;
    AddressViewPager shippingAddressViewPager;
    ImageButton prevAddress, nextAddress;
    LinearLayout pagingOptions;
    int shippAddressPosition = 0;
    ArrayList<CustomerShippingAddress> customerShippingAddressArrayList;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_infok);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText(R.string.customer_information_actionbar_title);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);


//        if (customerType.equals("Primary")) {
//            Primary.setChecked(true);
//            Secondary.setChecked(false);
//            doctorRadioButton.setChecked(false);
//        } else if (customerType.equals("Secondary")) {
//            Primary.setChecked(false);
//            Secondary.setChecked(true);
//            doctorRadioButton.setChecked(false);
//        } else {
//            Primary.setChecked(false);
//            Secondary.setChecked(false);
//            doctorRadioButton.setChecked(true);
//        }

//        if(Constants.COMPANY_CODE.equals("DemoCRM")) {
//            if(customerType.equals("Prospect")) {
//                Prospect.setVisibility(View.VISIBLE);
//                Prospect.setChecked(true);
//
//
//                Primary.setVisibility(View.GONE);
//                Secondary.setVisibility(View.GONE);
//                doctorRadioButton.setVisibility(View.GONE);
//
//            } else if (customerType.equals("Primary")) {
//                Primary.setVisibility(View.VISIBLE);
//                Primary.setChecked(true);
//
//
//                Prospect.setVisibility(View.GONE);
//                Secondary.setVisibility(View.GONE);
//                doctorRadioButton.setVisibility(View.GONE);
//            }else if (customerType.equals("Secondary")) {
//                Secondary.setVisibility(View.VISIBLE);
//                Secondary.setChecked(true);
//
//
//                Prospect.setVisibility(View.GONE);
//                Primary.setVisibility(View.GONE);
//                doctorRadioButton.setVisibility(View.GONE);
//            }
//
//        } else {

            if (customerType.equals("Primary")) {
                Primary.setChecked(true);
                Secondary.setChecked(false);
                doctorRadioButton.setChecked(false);

                Prospect.setVisibility(View.GONE);
                Primary.setVisibility(View.VISIBLE);
                Secondary.setVisibility(View.VISIBLE);
                doctorRadioButton.setVisibility(View.VISIBLE);

            } else if (customerType.equals("Secondary")) {
                Primary.setChecked(false);
                Secondary.setChecked(true);
                doctorRadioButton.setChecked(false);

                Prospect.setVisibility(View.GONE);
                Primary.setVisibility(View.VISIBLE);
                Secondary.setVisibility(View.VISIBLE);
                doctorRadioButton.setVisibility(View.VISIBLE);

            } else if(customerType.equals("Prospect")) {
                Prospect.setChecked(true);
                Primary.setChecked(false);
                Secondary.setChecked(false);

                Prospect.setVisibility(View.VISIBLE);
                Primary.setVisibility(View.VISIBLE);
                Secondary.setVisibility(View.VISIBLE);
                doctorRadioButton.setVisibility(View.GONE);

            }else {
                Primary.setChecked(false);
                Secondary.setChecked(false);
                doctorRadioButton.setChecked(true);

                Prospect.setVisibility(View.GONE);
                Primary.setVisibility(View.VISIBLE);
                Secondary.setVisibility(View.VISIBLE);
                doctorRadioButton.setVisibility(View.VISIBLE);
            }
//        }


        getCustomerDetails();

        getCustomerShippingAddress();

        shippingAddressViewPager.setAdapter(new AddressPageAdapter(getApplicationContext(), customerShippingAddressArrayList));
        if (customerShippingAddressArrayList.size() <= 1) {
            pagingOptions.setVisibility(View.GONE);
        }

        prevAddress.setOnClickListener(v -> {
            shippAddressPosition = shippingAddressViewPager.getCurrentItem();
            if (shippAddressPosition - 1 >= 0) {
                shippingAddressViewPager.setCurrentItem(shippAddressPosition - 1, true);
                shippAddressPosition = shippingAddressViewPager.getCurrentItem();
            }

        });

        nextAddress.setOnClickListener(v -> {
            shippAddressPosition = shippingAddressViewPager.getCurrentItem();
            if (shippAddressPosition + 1 < customerShippingAddressArrayList.size()) {
                shippingAddressViewPager.setCurrentItem(shippAddressPosition + 1, true);
                shippAddressPosition = shippingAddressViewPager.getCurrentItem();
            }

        });


        actionbarBackButton.setOnClickListener(v -> finish());

        customer_map.setOnClickListener(v -> showcustomerLocation());

//        btnAddInfo.setOnClickListener(v -> {
//            infoAdd();
//        });




        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

    }

//    public void infoAdd() {
//
//        String info = dbHandler.getCustomerInfo(customer_id);
//        View editingNotesToolView = getLayoutInflater().inflate(R.layout.additional_info_layout, null, false);
//
//        notesText = editingNotesToolView.findViewById(R.id.notesEditText);
//        saveNotesButton = editingNotesToolView.findViewById(R.id.saveNotesButton);
//        cancelNotesButton = editingNotesToolView.findViewById(R.id.cancelNotesButton);
//
//        if(info!=null && !info.equals("")){
//            notesText.setText(info);
//        }
//        else{
//            notesText.setText("");
//        }
//        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
//        builder.setView(editingNotesToolView);
//        final android.app.AlertDialog alert = builder.create();
//        alert.show();
//
//        saveNotesButton.setOnClickListener(v -> {
//            hideSoftKeyboard(getApplicationContext(), v);
//
//            if (TextUtils.isEmpty(notesText.getText().toString())) {
//                Toast.makeText(getApplicationContext(), "Please add the Info", Toast.LENGTH_LONG).show();
//                return;
//            }
//
//            dbHandler.updateCustomerDetailsAdditonalInfo(notesText.getText().toString(),customer_id);
//
//            alert.dismiss();
//        });
//
//
//        cancelNotesButton.setOnClickListener(v -> {
//            hideSoftKeyboard(getApplicationContext(), v);
//
//            final Dialog alertBox = new Dialog(v.getRootView().getContext());
//
//            LayoutInflater mInflater = (LayoutInflater) getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//            final View alertLayout = mInflater.inflate(R.layout.activity_exit_notes_popup, null);
//
//            alertBox.setCancelable(false);
//            alertBox.setContentView(alertLayout);
//
//            final Button Yes = alertLayout.findViewById(R.id.Yes);
//            final Button No = alertLayout.findViewById(R.id.No);
//
//            Yes.setOnClickListener(v1 -> {
//                alertBox.dismiss();
//                alert.cancel();
//            });
//
//            No.setOnClickListener(v12 -> alertBox.dismiss());
//
//            alertBox.show();
//
//        });
//    }

    private void getCustomerShippingAddress() {

        customerShippingAddressArrayList = new ArrayList<>();
        Cursor customerShipDetails = dbHandler.getCustomerShipDetails(customer_id);
        int numRows1 = customerShipDetails.getCount();
        if (numRows1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerShipDetails.moveToNext()) {
                Log.d("DataTest", "Test" + customerShipDetails.getString(14));
                CustomerShippingAddress csa = new CustomerShippingAddress();
                csa.setShip_address1(customerShipDetails.getString(14));
                csa.setShip_address2(customerShipDetails.getString(15));
                csa.setShip_address3(customerShipDetails.getString(16));
                csa.setShip_city(customerShipDetails.getString(17));
                csa.setShip_state(customerShipDetails.getString(18));
                csa.setShip_country("Nigeria");

                customerShippingAddressArrayList.add(csa);
            }
        }
    }

    private void getCustomerDetails() {

         credit_limit = dbHandler.get_customer_creditLimit2(customer_id);
         creditDays = dbHandler.get_customer_creditDays(customer_id);
        System.out.println("TTTT::credit_limit = " + credit_limit);
        System.out.println("TTTT::creditDays = " + creditDays);
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));

                bill_add1.setText(customerDetails.getString(6));
                bill_add2.setText(customerDetails.getString(7));
                bill_add3.setText(customerDetails.getString(8));
                bill_city.setText(customerDetails.getString(9));
                bill_country.setText(R.string.customerInformation_bill_country);
                bill_state.setText(customerDetails.getString(10));

                if(customerDetails.getString(21).equals("Primary")){
                    credit_term.setText(customerDetails.getString(25));
                    payment_term.setText(customerDetails.getString(26));
                }else {
                    if (credit_limit.equals("")) {
                        credit_term.setText("0");
                    } else {
                        credit_term.setText(credit_limit);
                    }
                    if (creditDays.equals("")) {
                        payment_term.setText("0");
                    } else {
                        payment_term.setText(creditDays);
                    }
                }




            }
        }

    }

    private void populateHeaderDetails() {
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");

            customerType = bundle.getString("customer_type");

        }


        checkintime.setText(checkin_time);


    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), CustomerLocationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("nav_type", "SRLanding");
        bundle.putString("login_id", login_id);
        bundle.putString("customer_id", customer_id);

        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void setFooterDateTime() {

        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void initializeViews() {
        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkintime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        actionBarTitle = findViewById(R.id.toolbar_title);
        Primary = findViewById(R.id.Primary);
        Secondary = findViewById(R.id.Secondary);
        bill_add1 = findViewById(R.id.bill_add1);
        bill_add2 = findViewById(R.id.bill_add2);
        bill_add3 = findViewById(R.id.bill_add3);
        bill_city = findViewById(R.id.bill_city);
        bill_country = findViewById(R.id.bill_country);
        bill_state = findViewById(R.id.bill_state);
        ship_add1 = findViewById(R.id.ship_add1);
        ship_add2 = findViewById(R.id.ship_add2);
        ship_add3 = findViewById(R.id.ship_add3);
        ship_city = findViewById(R.id.ship_city);
        ship_state = findViewById(R.id.ship_state);
        ship_country = findViewById(R.id.ship_country);

        credit_term = findViewById(R.id.credit_term);
        payment_term = findViewById(R.id.payment_term);
        shippingAddressViewPager = findViewById(R.id.shippingAddressViewPager);
        nextAddress = findViewById(R.id.nextAddress);
        prevAddress = findViewById(R.id.prevAddress);
        pagingOptions = findViewById(R.id.pagingOptions);
        customer_map = findViewById(R.id.customer_map);
//        btnAddInfo = findViewById(R.id.btnAddInfo);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        Prospect = findViewById(R.id.cusInfoProspectRb);
        doctorRadioButton = findViewById(R.id.DoctorRadioButton);

    }

    @Override
    public void onBackPressed() {

    }

}


class AddressViewPager extends ViewPager {
    private boolean enabled;

    public AddressViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onTouchEvent(event);
        }
        return false;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}


class AddressPageAdapter extends PagerAdapter {

    public ArrayList<CustomerShippingAddress> shippingAddressArrayList;
    TextView shipAddress1, shipAddress2, shipAddress3, shipCity, shipState, shipCountry;
    int numberOfAddress = 0;
    private Context mContext;

    AddressPageAdapter(Context context, ArrayList<CustomerShippingAddress> qData) {
        mContext = context;
        shippingAddressArrayList = qData;
        numberOfAddress = qData.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        Log.d("Question", "instantiateItem " + position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final ViewGroup page = (ViewGroup) inflater.inflate(R.layout.item_shipping_address, container, false);

        shipAddress1 = page.findViewById(R.id.ship_add1);
        shipAddress2 = page.findViewById(R.id.ship_add2);
        shipAddress3 = page.findViewById(R.id.ship_add3);
        shipCity = page.findViewById(R.id.ship_city);
        shipState = page.findViewById(R.id.ship_state);
        shipCountry = page.findViewById(R.id.ship_country);
        shipAddress1.setText(shippingAddressArrayList.get(position).getShip_address1());
        shipAddress2.setText(shippingAddressArrayList.get(position).getShip_address2());
        shipAddress3.setText(shippingAddressArrayList.get(position).getShip_address3());
        shipCity.setText(shippingAddressArrayList.get(position).getShip_city());
        shipState.setText(shippingAddressArrayList.get(position).getShip_state());
        shipCountry.setText(shippingAddressArrayList.get(position).getShip_country());
        container.addView(page);
        return page;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return numberOfAddress;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}
