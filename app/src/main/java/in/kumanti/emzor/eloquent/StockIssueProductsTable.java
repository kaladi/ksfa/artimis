package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.StockIssueProducts;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_ISSUE_PRODUCTS;

public class StockIssueProductsTable extends SQLiteOpenHelper {

    //Columns used for Stock Return Products Information details for a selected ordered product
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_ID = "stockIssueProductId";
    private static final String COLUMN_STOCK_ISSUE_NUMBER = "stockIssueNumber";
    // private static final String COLUMN_STOCK_ISSUE_GROUP_CODE = "productGroupCode";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_CODE = "productCode";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_NAME = "productName";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_UOM = "productUom";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_UNIT_PRICE = "unitPrice";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_STOCK_QUANTITY = "stockQuantity";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_ISSUE_QUANTITY = "issueQuantity";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_ISSUE_VALUE = "issueValue";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_IS_SYNC = "isSync";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_STOCK_ISSUE_PRODUCT_UPDATED_AT = "updatedAt";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public StockIssueProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_RETURN_PRODUCTS);

        String stockReturnProductTable = "CREATE TABLE IF NOT EXISTS " + TABLE_STOCK_ISSUE_PRODUCTS + "(" +
                COLUMN_STOCK_ISSUE_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_STOCK_ISSUE_NUMBER + " TEXT, " +
                COLUMN_STOCK_ISSUE_PRODUCT_CODE + " TEXT, " +
                COLUMN_STOCK_ISSUE_PRODUCT_NAME + " TEXT, " +
                COLUMN_STOCK_ISSUE_PRODUCT_UOM + " TEXT, " +
                COLUMN_STOCK_ISSUE_PRODUCT_UNIT_PRICE + " NUMERIC, " +
                COLUMN_STOCK_ISSUE_PRODUCT_STOCK_QUANTITY + " INTEGER, " +
                COLUMN_STOCK_ISSUE_PRODUCT_ISSUE_QUANTITY + " INTEGER, " +
                COLUMN_STOCK_ISSUE_PRODUCT_ISSUE_VALUE + " NUMERIC, " +
                COLUMN_STOCK_ISSUE_PRODUCT_IS_SYNC + " TEXT, " +
                COLUMN_STOCK_ISSUE_PRODUCT_LATEST_SYNC_DATE + " TEXT, " +

                COLUMN_STOCK_ISSUE_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_STOCK_ISSUE_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(stockReturnProductTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(StockIssueProducts stockIssueProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_STOCK_ISSUE_NUMBER, stockIssueProducts.getStockIssueNumber());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_CODE, stockIssueProducts.getProductCode());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_NAME, stockIssueProducts.getProductName());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_UOM, stockIssueProducts.getProductUom());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_UNIT_PRICE, stockIssueProducts.getUnitPrice());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_STOCK_QUANTITY, stockIssueProducts.getStockQuantity());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_ISSUE_QUANTITY, stockIssueProducts.getIssueQuantity());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_ISSUE_VALUE, stockIssueProducts.getIssueValue());
        values.put(COLUMN_STOCK_ISSUE_PRODUCT_IS_SYNC, stockIssueProducts.isSync());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_STOCK_ISSUE_PRODUCTS, null, values);
        db.close();
        return id;
    }


    public ArrayList<StockIssueProducts> getStockIssueProductsDetailsForSync() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_STOCK_ISSUE_PRODUCTS;

        ArrayList<StockIssueProducts> stockIssueProductsArrayList = new ArrayList<>();

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {
            StockIssueProducts stockIssueProducts = new StockIssueProducts();
            stockIssueProducts.setTabCode(Constants.TAB_CODE);
            stockIssueProducts.setCompanyCode(Constants.COMPANY_CODE);
            stockIssueProducts.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            stockIssueProducts.setStockIssueId(id);
            stockIssueProducts.setStockIssueNumber(cursor.getString(1));
            stockIssueProducts.setProductCode(cursor.getString(2));
            stockIssueProducts.setProductName(cursor.getString(3));
            stockIssueProducts.setProductUom(cursor.getString(4));
            stockIssueProducts.setUnitPrice(cursor.getString(5));
            stockIssueProducts.setStockQuantity(cursor.getString(6));
            stockIssueProducts.setIssueQuantity(cursor.getString(7));
            stockIssueProducts.setIssueValue(cursor.getString(8));
            stockIssueProducts.setCreatedAt(cursor.getString(11));
            stockIssueProducts.setUpdatedAt(cursor.getString(12));

            stockIssueProductsArrayList.add(stockIssueProducts);
        }
        cursor.close();
        sqlWrite.close();
        return stockIssueProductsArrayList;
    }


   /* public ArrayList<StockIssueProducts> getStockIssueProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT productCode FROM " + TABLE_STOCK_ISSUE_PRODUCTS ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<StockIssueProducts> stockIssueProductsArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            StockIssueProducts stockIssueProducts = new StockIssueProducts();
            stockIssueProducts.setStockIssueProductId(cursor.getInt(0));
            stockIssueProducts.setStockIssueNumber(cursor.getString(1));
            stockIssueProducts.setProductCode(cursor.getString(2));
            stockIssueProducts.setProductName(cursor.getString(3));
            stockIssueProducts.setStockQuantity(cursor.getString(4));
            stockIssueProducts.setIssueQuantity(cursor.getString(5));

            stockIssueProductsArrayList.add(i,stockIssueProducts);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return stockIssueProductsArrayList;
    }*/


}
