package in.kumanti.emzor.activity;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Chronometer;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.ArtifactsListAdapter;
import in.kumanti.emzor.eloquent.ArtifactsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.Artifacts;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.SharedPreferenceManager;

public class ArtifactsActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionbarTitle, checkInTime, marqueeText, customerName, accountNo, city;

    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;

    MyDBHandler dbHandler;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1, checkin_time = "", login_id = "", customer_id = "";
    ArtifactsTable artifactsTable;
    ArtifactsListAdapter artifactsListAdapter;
    GridView artifactsGridView;

    CustomSearchableSpinner artifactTypeSpinner;
    ArrayAdapter<Artifacts> artifactsSpinnerArrayAdapter;
    ArrayList<Artifacts> artifactsArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_artifacts);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText(R.string.artifacts_actionbar_title);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        artifactsArrayList = artifactsTable.getArtifactsTypes();

        Artifacts cd = new Artifacts();
        cd.setArtifactType("Select");
        artifactsArrayList.add(0, cd);

        artifactTypeSpinner.setTitle("Select a Artifact Type");
        artifactTypeSpinner.setPositiveButton("OK");

        artifactsSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, artifactsArrayList);
        artifactsSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view

        artifactTypeSpinner.setAdapter(artifactsSpinnerArrayAdapter);

        artifactTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                CustomSearchableSpinner.isSpinnerDialogOpen = false;

                Artifacts cd = (Artifacts) adapterView.getItemAtPosition(pos);
                updateArtifacts(cd.getArtifactType());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                CustomSearchableSpinner.isSpinnerDialogOpen = false;

            }
        });


        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);
        });


    }


    public void updateArtifacts(String artifactType) {
        Log.d("Artifacts", "Name" + artifactType);
        artifactsArrayList = artifactsTable.getArtifactsByType(artifactType);
        artifactsListAdapter = new ArtifactsListAdapter(getApplicationContext(), artifactsArrayList);
        artifactsGridView.setAdapter(artifactsListAdapter);
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);
        artifactTypeSpinner = findViewById(R.id.fileTypeSpinner);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        artifactsGridView = findViewById(R.id.artifactsGridView);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        artifactsTable = new ArtifactsTable(getApplicationContext());

    }

    public void populateHeaderDetails() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            Log.d("LoginId", login_id);
        }


        checkInTime.setText(checkin_time);


        //Populate Customer Information Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows != 0) {
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                String cityVal = customerDetails.getString(9) + "-" + customerDetails.getString(10);
                city.setText(cityVal);
                accountNo.setText(customerDetails.getString(1));


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                ship_address1 = customerDetails.getString(14);
                ship_address2 = customerDetails.getString(15);
                ship_address3 = customerDetails.getString(16);
                ship_city1 = customerDetails.getString(17);
                ship_state1 = customerDetails.getString(18);
                ship_country1 = customerDetails.getString(19);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(33));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }


    }

    private void setFooterDateTime() {
        Locale la= getResources().getConfiguration().locale;
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN,la);
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a",la);
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                    Log.d("Exception",e.getMessage()+"");
                }
            }
        };
        t.start();

    }

    @Override
    public void onBackPressed() {

    }


}

