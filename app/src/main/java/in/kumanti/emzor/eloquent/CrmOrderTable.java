package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CrmOrder;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_ORDER;

public class CrmOrderTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_TAB_ORDER_ID = "tabOrderId";
    private static final String COLUMN_QUOTATION_ID = "quotationId";
    private static final String COLUMN_TAB_ORDER_NUMBER = "tabOrderNumber";
    private static final String COLUMN_ORDER_DATE = "orderDate";
    private static final String COLUMN_ORDER_DEMO_DATE = "demoDate";
    private static final String COLUMN_ORDER_INSPECTION_DATE = "inspectionDate";
    private static final String COLUMN_ORDER_DELIVERY_DATE = "deliveryDate";
    private static final String COLUMN_ORDER_INSTALLATION_DATE = "installationDate";
    private static final String COLUMN_ORDER_TRAINING_DATE = "trainingDate";
    private static final String COLUMN_ORDER_DEMO_ADDRESS = "demoAddress";
    private static final String COLUMN_ORDER_INSPECTION_ADDRESS = "inspectionAddress";
    private static final String COLUMN_ORDER_DELIVERY_ADDRESS = "deliveryAddress";
    private static final String COLUMN_ORDER_INSTALLATION_ADDRESS = "installationAddress";
    private static final String COLUMN_ORDER_TRAINING_ADDRESS = "trainingAddress";
    private static final String COLUMN_ORDER_STATUS = "orderStatus";
    private static final String COLUMN_ORDER_IS_SYNC = "isSync";
    private static final String COLUMN_ORDER_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_ORDER_CREATED_AT = "createdAt";
    private static final String COLUMN_ORDER_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmOrderTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_ORDER);
        String orderTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_ORDER + "(" +
                COLUMN_TAB_ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_QUOTATION_ID + " INTEGER, " +
                COLUMN_TAB_ORDER_NUMBER + " TEXT, " +
                COLUMN_ORDER_DATE + " TEXT, " +
                COLUMN_ORDER_DEMO_DATE + " TEXT, " +
                COLUMN_ORDER_DEMO_ADDRESS + " REAL, " +
                COLUMN_ORDER_INSPECTION_DATE + " TEXT, " +
                COLUMN_ORDER_INSPECTION_ADDRESS + " TEXT, " +
                COLUMN_ORDER_DELIVERY_DATE + " TEXT, " +
                COLUMN_ORDER_DELIVERY_ADDRESS + " TEXT, " +
                COLUMN_ORDER_INSTALLATION_DATE + " TEXT, " +
                COLUMN_ORDER_INSTALLATION_ADDRESS + " TEXT, " +
                COLUMN_ORDER_TRAINING_DATE + " TEXT, " +
                COLUMN_ORDER_TRAINING_ADDRESS + " TEXT, " +
                COLUMN_ORDER_STATUS + " TEXT, " +
                COLUMN_ORDER_IS_SYNC + " TEXT, " +
                COLUMN_ORDER_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_ORDER_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_ORDER_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(orderTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmOrder order) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_QUOTATION_ID, order.getQuotationId());
        values.put(COLUMN_TAB_ORDER_NUMBER, order.getTabOrderNumber());
        values.put(COLUMN_ORDER_DATE, order.getOrderDate());
        values.put(COLUMN_ORDER_DEMO_DATE, order.getDemoDate());
        values.put(COLUMN_ORDER_DEMO_ADDRESS, order.getDemoAddress());
        values.put(COLUMN_ORDER_INSPECTION_DATE, order.getInspectionDate());
        values.put(COLUMN_ORDER_INSPECTION_ADDRESS, order.getInspectionAddress());
        values.put(COLUMN_ORDER_DELIVERY_DATE, order.getDeliveryDate());
        values.put(COLUMN_ORDER_DELIVERY_ADDRESS, order.getDeliveryAddress());
        values.put(COLUMN_ORDER_INSTALLATION_DATE, order.getInstallationDate());
        values.put(COLUMN_ORDER_INSTALLATION_ADDRESS, order.getInstallationAddress());
        values.put(COLUMN_ORDER_TRAINING_DATE, order.getTrainingDate());
        values.put(COLUMN_ORDER_TRAINING_ADDRESS, order.getTrainingAddress());
        values.put(COLUMN_ORDER_STATUS, order.getOrderStatus());
        values.put(COLUMN_ORDER_IS_SYNC, order.isSync());
        values.put(COLUMN_ORDER_LATEST_SYNC_DATE, order.getLatestSyncDate());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_ORDER, null, values);
        db.close();
        return id;
    }



    /*public ArrayList<CrmOrder> getCrmOrders() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_PRODUCTS +" ORDER BY product_name ASC" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<CrmOrder> crmOrderArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmOrder crmOrder = new CrmOrder();
            Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            crmOrder.setTabOrderId(cursor.getInt(0));
            crmOrder.setQuotationId(cursor.getString(1));
            crmOrder.setTabOrderNumber(cursor.getString(2));
            crmOrder.setOrderDate(cursor.getString(3));
            crmOrder.setDemoDate(cursor.getString(4));
            crmOrder.setDemoAddress(cursor.getString(5));
            crmOrder.setInspectionDate(cursor.getString(6));
            crmOrder.setInspectionAddress(cursor.getString(7));
            crmOrder.setDeliveryDate(cursor.getString(8));
            crmOrder.setDeliveryAddress(cursor.getString(9));
            crmOrder.setInstallationDate(cursor.getString(10));
            crmOrder.setInstallationAddress(cursor.getString(11));
            crmOrder.setTrainingDate(cursor.getString(12));
            crmOrder.setTrainingAddress(cursor.getString(13));
            crmOrder.setOrderStatus(cursor.getString(14));


            crmOrderArrayList.add(crmOrder);
        }
        cursor.close();
        sqlWrite.close();
        return crmOrderArrayList;
    }*/


}
