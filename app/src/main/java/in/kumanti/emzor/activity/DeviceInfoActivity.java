package in.kumanti.emzor.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dcastalia.localappupdate.DownloadApk;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.R;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.RDLSyncTable;
import in.kumanti.emzor.mastersData.ActivationFlag;
import in.kumanti.emzor.mastersData.SetupValidations;
import in.kumanti.emzor.model.ApkUpgrade;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.GPSTracker;
import me.shenfan.updateapp.UpdateService;
import mmsl.GetPrintableImage.GetPrintableImage;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceInfoActivity extends MainActivity {
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int Gallery_Code = 1;
    private static final int Camera_Code = 0;
    private static BluetoothSocket mbtSocket;
    private static OutputStream mbtOutputStream;
    private static InputStream mbtInputStream;

    String emailid1="",passwordOld="";
    TelephonyManager TelephonManager;
    LocationManager locationManager;
    myPhoneStateListener pslistener;
    int SignalStrength, speed;
    SeekBar bat_prog;
    ProgressBar gprs_prog, gsm_prog;
    TextView gps_prog;
    TextView txt_battery, txt_gprs, txt_gsm, txt_gps;
    Context context;
    EditText txt_lat, txt_lng, txt_appversion;
    ImageView img_return;
    TextView txt_help, datetime;
    ImageView cancel, img_map;
    GPSTracker gpsTracker;
    String lat, lng;
    TextView support, header_text;
    TextView support_number1, support_number2, txt_mail, contact_person;
    MyDBHandler dbHandler;
    Button updateAppButton,activateButton;
    TextView rdlSyncDate, mdlSyncDate,mdlSyncTime,rdlSyncTime;
    RDLSyncTable rdlSyncTable;
    Button printImageButton;
    Uri imageUri;
    int TamilfontSize = 22;
    int TamilTypeFace = Typeface.NORMAL;
    int mPrintType = 0;
    int postion;
    int[] prnRasterImg;
    int[] image_arr;
    int LINE_WIDTH = 48;
    int image_width;
    int image_height;
    byte FontStyleVal;
    String currentApkVersion = "";
    private TextView marquee_txt;
    private boolean PrintImage = false;
    private Handler handler;
    private ProgressDialog dialog;
    private Context contextForDialog = null;
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent i) {
            int level = i.getIntExtra("level", 0);
            bat_prog = findViewById(R.id.battery_progress);
            bat_prog.setProgress(level);
            //bat_prog.setThumb(getResources().getDrawable(R.drawable.tooltip_icon));
            txt_battery = findViewById(R.id.txt_battery);
            txt_battery.setText("Battery : " + Integer.toString(level) + "%");
            bat_prog.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });
        }

    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_device_info);


        initializeViews();
        setDateTime();
        getUserDetails();

        header_text.setText("Device Info");
        marquee_txt.setSelected(true);
        String marqueee_txt = sharedPreferences.getString("MarqueeText", "");
        marquee_txt.setText(marqueee_txt);

        txt_lat.setEnabled(false);
        txt_lng.setEnabled(false);

        if(dbHandler.get_activate_flag() == 0){
            activateButton.setVisibility(View.VISIBLE);
        }

        contextForDialog = this;


        PackageManager manager = getApplicationContext().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        currentApkVersion = info.versionName;

        txt_appversion.setText(currentApkVersion);

        txt_appversion.setEnabled(false);

        System.out.println("rdlSyncTable.getRDLSyncDate() = " + rdlSyncTable.getRDLSyncDate());
        System.out.println("rdlSyncTable.getRDLSyncTime() = " + rdlSyncTable.getRDLSyncTime());
        mdlSyncDate.setText(dbHandler.getMDLSyncDate());
        rdlSyncDate.setText(rdlSyncTable.getRDLSyncDate());

        mdlSyncTime.setText(dbHandler.getMDLSyncTime());
        rdlSyncTime.setText(rdlSyncTable.getRDLSyncTime());


        getlocation();
        batterystatus();
        gprsstatus();
        gsmstatus();
        gpsstatus();

        img_return.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        txt_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopup();
            }
        });

        activateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login_api();

            }
        });

        updateAppButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean networkConnected = isNetworkConnected();
                if (networkConnected == true) {
                    dialog = new ProgressDialog(contextForDialog);
                    dialog.setMessage("Downloading updated version of App, Please Wait!!");
                    dialog.setCancelable(false);
                    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    dialog.show();

                    new Thread() {
                        public void run() {
                            Gson gson = new GsonBuilder().setLenient().create();
                            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(500000, TimeUnit.SECONDS).readTimeout(500000, TimeUnit.SECONDS).build();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(ApiInterface.BASE_URL)
                                    .client(client)
                                    .addConverterFactory(GsonConverterFactory.create(gson))
                                    .build();

                            ApiInterface api = retrofit.create(ApiInterface.class);
                            Call<ApkUpgrade> apkUpgradeCall = api.getApkUpgradeDetails();
                            Log.d("Upgrading apk", "api details---" + api.getApkUpgradeDetails());

                            apkUpgradeCall.enqueue(new Callback<ApkUpgrade>() {
                                @Override
                                public void onResponse(Call<ApkUpgrade> call, Response<ApkUpgrade> response) {
                                    try {
                                        Log.d("Upgrading apk", "onSuccess---");
                                        ApkUpgrade apkUpgrade = response.body();
                                        Log.d("Upgrading apk", "api response---" + response.body().toString());
                                        Log.d("Upgrading apk", "api details--Version---" + apkUpgrade.getApkVersion() + "Apk Url---" + apkUpgrade.getApkUrl());

                                        if (!currentApkVersion.equals(apkUpgrade.getApkVersion())) {
                                            Log.d("Upgrading apk", "Version not equal---");
//                                            UpdateService.Builder.create(apkUpgrade.getApkUrl()).build(getApplicationContext());
                                            DownloadApk downloadApk = new DownloadApk(DeviceInfoActivity.this);
                                            downloadApk.startDownloadingApk(apkUpgrade.getApkUrl());
                                        } else {
                                            dialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Your App is already Up to Date", Toast.LENGTH_LONG).show();
                                            Log.d("Upgrading apk", "Version is equal---");
                                        }

                                    } catch (Exception e) {
                                        Log.d("Upgrading apk", "Exception---" + e.getMessage());
                                        //  dialog.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ApkUpgrade> call, Throwable t) {
                                    dialog.dismiss();
                                    Log.d("Upgrading apk", "onFailure---");
                                    Toast.makeText(getApplicationContext(), "Unable to connect" + t, Toast.LENGTH_LONG).show();
                                }
                            });

                        }
                    }.start();

                    handler = new Handler() {
                        public void handleMessage(android.os.Message msg) {
                            dialog.dismiss();
                        }
                    };
                } else {
                    Toast.makeText(getApplicationContext(), "Network is not Connected.Please check your internet connection!!!", Toast.LENGTH_LONG).show();
                }
            }

        });


        img_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
                startActivity(intent);
            }
        });


        printImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                Log.d("SamplePrint", "ImageButton---OnClick");
                startActivityForResult(Intent.createChooser(intent, "Select An Image"), Gallery_Code);

            }
        });
    }

    void login_api(){
        boolean networkConnected = isNetworkConnected();
        boolean networking = isInternetAvailable();
        if (networkConnected == true) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.ACTIVATION_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();

            api = retrofit.create(ApiInterface.class);
            String company_code = dbHandler.get_company_code();
            Call<ActivationFlag> validatedLogin = api.getActivationFlag(company_code);

            System.out.println("validatedLogin1 = " + validatedLogin);
            validatedLogin.enqueue(new Callback<ActivationFlag>() {

                @Override
                public void onResponse(Call<ActivationFlag> validatedLogin, retrofit2.Response<ActivationFlag> response) {
                    System.out.println("validatedLogin2 = " + response);
                    ActivationFlag mLoginObject = response.body();
                    System.out.println("mLoginObject = " + response.body());
                    System.out.println("mLoginObject.status = " + mLoginObject.flag);
                    String returnedResponse = mLoginObject.flag;

                    /*if ("N".equals(returnedResponse)) {
                        int flag = 0;
                        dbHandler.update_tab_flag(flag);
                    }*/
                    if ("Y".equals(returnedResponse)) {
                        int flag = 1;
                        dbHandler.update_tab_flag(flag);
                        Toast.makeText(getApplicationContext(), "Activation Successfully", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getApplicationContext(), "Please Contact Admin to Activate your login", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<ActivationFlag> call, Throwable t) {
                    System.out.println("validatedLogin4 = " + validatedLogin);
                    System.out.println("t.getMessage() = " + t.getMessage());
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();

        }
    }

    private void getUserDetails() {

        Cursor userDeatils = dbHandler.getUserDeatils();
        int count1 = userDeatils.getCount();
        if (count1 == 0) {
        } else {
            int i = 0;
            while (userDeatils.moveToNext()) {

                emailid1 = (userDeatils.getString(2));
                passwordOld = (userDeatils.getString(5));

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case BTWrapperActivity.REQUEST_CONNECT_BT:

                try {
                    mbtSocket = BTWrapperActivity.getSocket();
                    if (mbtSocket != null) {

                        if (PrintImage == false) {
                            Thread.sleep(100);
                            senddatatodevice();
                        } else {
                            PrintImage = false;
                            BTPrnImage bt = new BTPrnImage(image_width,
                                    image_height, prnRasterImg);

                            bt.start();

                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case Gallery_Code:
                Log.d("SamplePrint", "ImageButton---Gallery_Code");

                try {
                    Uri selectedImageURI = data.getData();
                    Bitmap selectedImage = decodeUri(selectedImageURI);

                    Log.d("SamplePrint", "ImageButton---selectedImage");

                    GetPrintableImage pt = new GetPrintableImage();

                    Log.d("SamplePrint", "ImageButton---GetPrintableImage");

                    prnRasterImg = pt.GetPrintableArray(this, 192, selectedImage);
                    image_height = pt.getPrintHeight();
                    image_width = pt.getPrintWidth();
                    try {

                        if (mbtSocket == null) {
                            Log.d("SamplePrint", "ImageButton---mbtSocket");

                            PrintImage = true;
                            Intent BTIntent = new Intent(getApplicationContext(),
                                    BTWrapperActivity.class);
                            this.startActivityForResult(BTIntent,
                                    BTWrapperActivity.REQUEST_CONNECT_BT);
                        } else {

                            BTPrnImage bt = new BTPrnImage(image_width,
                                    image_height, prnRasterImg);

                            Log.d("SamplePrint", "ImageButton---mbtSocket Connection Established");


                            bt.start();

                        }

                    } catch (Exception e) {

                        System.err.println(e.getMessage());

                    }
                    /*
                     * Bitmap modifiedimg = JPEGtoRGB888(selectedImage);
                     * ModifyImage(modifiedimg);
                     */

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
           /* case Camera_Code:
                try {
                    Uri selectedImageURI1 = imageUri;
                    Bitmap selectedImage = decodeUri(selectedImageURI1);
                    GetPrintableImage pt = new GetPrintableImage();
                    prnRasterImg = pt.GetPrintableArray(this,192, selectedImage);
                    image_height = pt.getPrintHeight();
                    image_width = pt.getPrintWidth();
                    try {

                        if (mbtSocket == null) {
                            PrintImage = true;
                            Intent BTIntent = new Intent(getApplicationContext(),
                                    BTWrapperActivity.class);
                            this.startActivityForResult(BTIntent,
                                    BTWrapperActivity.REQUEST_CONNECT_BT);
                        } else {

                            BTPrnImage bt = new BTPrnImage(image_width,
                                    image_height, prnRasterImg);

                            bt.start();

                        }

                    } catch (Exception e) {

                        System.err.println(e.getMessage());

                    }
                    *//*
             * System.out.println("Decoded image config : "+
             * selectedImage.getConfig().name()); Bitmap modifiedimg =
             * JPEGtoRGB888(selectedImage); ModifyImage(modifiedimg);
             *//*

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;*/
        }

    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null,
                options);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 150;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = options.outWidth, height_tmp = options.outHeight;
        int scale = width_tmp / 192;
        // int scale = 4;
        /*
         * while (true) { if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 <
         * REQUIRED_SIZE) break; width_tmp /= 2; height_tmp /= 2; scale *= 2; }
         */
        // Decode with inSampleSize
        BitmapFactory.Options decodeoptions = new BitmapFactory.Options();
        decodeoptions.inSampleSize = scale;

        Bitmap resizedBitmap = BitmapFactory.decodeStream(getContentResolver()
                .openInputStream(selectedImage), null, decodeoptions);

        return resizedBitmap;

    }

    private void senddatatodevice() {
        try {
            mbtOutputStream = mbtSocket.getOutputStream();
            mbtInputStream = mbtSocket.getInputStream();

            switch (mPrintType) {
              /*  case 0:
                    byte[] Command = { 0x1B, 0x21, FontStyleVal };
                    mbtOutputStream.write(Command);
                    String sendingmessage = "Name : "
                            + mNameEditText.getText().toString();
                    byte[] send = sendingmessage.getBytes();
                    mbtOutputStream.write(send);
                    mbtOutputStream.write(0x0D);
                    sendingmessage = "Amount : "
                            + mAmountEditText.getText().toString();
                    send = sendingmessage.getBytes();
                    mbtOutputStream.write(send);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.flush();
                    break;*/

                case 1:
                    byte[] TestData = {0x1D, 0x28, 0x41};
                    mbtOutputStream.write(TestData);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.flush();
                    break;

                case 2:
                    // printLogo();
                    break;

                case 3:
				/*// EAN 13
				byte[] val = { 0x1B, 0x39, 0x04, 0x30, 0x31, 0x32, 0x33, 0x34,
						0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x0A, 0x0A,
						0x0A, 0x0A };
				mbtOutputStream.write(val);
				mbtOutputStream.write(0x0D);
				mbtOutputStream.write(0x0D);
				mbtOutputStream.write(0x0D);
				mbtOutputStream.write(0x0D);
				mbtOutputStream.flush();
				*/

               /*     String Data = mNameEditText.getText().toString();
                    Data = "12345678901234567890123";*/

                  /*  try {
                        Bitmap barbmp = createBarcode128Bitmap(Data, 150, 150);	// 50, 50 - 1D
                        if (barbmp == null)
                            return;

                        Thread.sleep(50);
                        GetPrintableImage getPrintableImage = new GetPrintableImage();
                        prnRasterImg = getPrintableImage.GetPrintableArray(DeviceInfoActivity.this, 75, barbmp);	// 75 wdt - 1D
                        image_width = getPrintableImage.getPrintWidth();
                        image_height = getPrintableImage.getPrintHeight();
                        getPrintableImage = null;
                        Thread.sleep(50);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    try {
                        byte[] CommandImagePrint = new byte[prnRasterImg.length + 5];
                        CommandImagePrint[0] = 0x1B;
                        CommandImagePrint[1] = 0x23;
                        CommandImagePrint[2] = (byte) image_width;
                        CommandImagePrint[3] = (byte) (image_height / 256);
                        CommandImagePrint[4] = (byte) (image_height % 256);

                        for (int i = 0; i < prnRasterImg.length; i++) {
                            CommandImagePrint[i + 5] = (byte) (prnRasterImg[i] & 0xFF);
                        }

                        mbtOutputStream.write(CommandImagePrint, 0, CommandImagePrint.length);
                    } catch (Exception ioe) {
                        System.out.println("Problems reading from or writing to serial port."
                                + ioe.getMessage());
                    }

                    break;
                case 4:
                    byte[] prnval = new byte[prnRasterImg.length];
                    for (int i = 0; i < prnval.length; i++) {
                        prnval[i] = (byte) prnRasterImg[i];
                    }
                    mbtOutputStream.write(prnval);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.write(0x0D);
                    mbtOutputStream.flush();
                    break;

                case 5:
                    final CharSequence[] AutoOffTimers = {"3 Min", "5 Min", "9 Min", "Auto Off Disable"};
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
                    alt_bld.setIcon(R.drawable.msales_logo1);
                    alt_bld.setTitle("Select Auto Off Timer Option");
                    alt_bld.setSingleChoiceItems(AutoOffTimers, -1,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int item) {
                                    try {
                                        postion = item;

                                        if (AutoOffTimers[item].equals("3 Min")) {
                                            byte[] AutoOff = {0x1B, 0x41, 0x03};
                                            mbtOutputStream.write(AutoOff);
                                            mbtOutputStream.flush();
                                            dialog.dismiss();
                                        } else if (AutoOffTimers[item].equals("5 Min")) {
                                            byte[] AutoOff = {0x1B, 0x41, 0x05};
                                            mbtOutputStream.write(AutoOff);
                                            mbtOutputStream.flush();
                                            dialog.dismiss();
                                        } else if (AutoOffTimers[item].equals("9 Min")) {
                                            byte[] AutoOff = {0x1B, 0x41, 0x09};
                                            mbtOutputStream.write(AutoOff);
                                            mbtOutputStream.flush();
                                            dialog.dismiss();
                                        } else if (AutoOffTimers[item].equals("Auto Off Disable")) {
                                            byte[] AutoOff = {0x1B, 0x41, 0x00};
                                            mbtOutputStream.write(AutoOff);
                                            mbtOutputStream.flush();
                                            Toast.makeText(getApplicationContext(), "Auto Off Timer Disabled", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            return;
                                        }

                                        Toast.makeText(getApplicationContext(), "Auto Off Timer Disabled for " + AutoOffTimers[item], Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.getMessage();
                                    }
                                }
                            });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                    //	alert.getListView().setItemChecked(postion, true);
                    break;

                case 6:
                    byte[] BatteryStatus = {0x1B, 0x42};
                    mbtOutputStream.write(BatteryStatus, 0, BatteryStatus.length);
                    int StatusResult = mbtInputStream.read();
                    if (StatusResult == 255) {
                        Toast.makeText(getApplicationContext(), "Printer Battery Status : Charging ", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(getApplicationContext(), "Printer Battery Status : " + StatusResult + "%", Toast.LENGTH_SHORT).show();
                    break;

                case 7:
                    byte[] PrinterStatus = {0x1B, 0x76};
                    mbtOutputStream.write(PrinterStatus);
                    int PrinterStatusResult = mbtInputStream.read();

                    if (PrinterStatusResult == 65) {
                        Toast.makeText(getApplicationContext(), "Out of Paper", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (PrinterStatusResult == 66) {
                        Toast.makeText(getApplicationContext(), "Platen is Open", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (PrinterStatusResult == 128) {
                        Toast.makeText(getApplicationContext(), "Printer is Ready", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    break;

                /*case 8 :		// Tamil Print
                    int alignment = Cocos2dxBitmap.ALIGNLEFT;
                    String familyName = "KhmerOS-Bold.ttf";

                    Cocos2dxBitmap cocos2dxBitmap = new Cocos2dxBitmap(getApplicationContext());
                    Bitmap ecgBmp1 = cocos2dxBitmap.createTextBitmap(tamil, familyName, TamilTypeFace, TamilfontSize, alignment, 380, 2000);
                    printtextasimg(ecgBmp1);
                    break;*/

                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void gpsstatus() {

        gps_prog = findViewById(R.id.gps_progress);
        txt_gps = findViewById(R.id.txt_gps);
        //txt_gps.setText("GPS : " + "0"+ "%");
        txt_gps.setText("GPS");


    }


    private void gsmstatus() {

        try {
            pslistener = new myPhoneStateListener();
            TelephonManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            TelephonManager.listen(pslistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
            Log.i("AAA", Integer.toString(SignalStrength));
            /*gsm_prog = findViewById(R.id.gsm_progress);
            gsm_prog.setProgress(SignalStrength);
            txt_gsm = findViewById(R.id.txt_gsm);
            txt_gsm.setText("GSM : " +Integer.toString(SignalStrength ) + "%");
            gsm_prog.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void gprsstatus() {
        ConnectivityManager cm;

        NetworkInfo info = null;
        try {

            cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            info = cm.getActiveNetworkInfo();
            NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
            int downSpeed = nc.getLinkDownstreamBandwidthKbps();
            int upSpeed = nc.getLinkUpstreamBandwidthKbps();
            gprs_prog = findViewById(R.id.gprs_progress);
            txt_gprs = findViewById(R.id.txt_gprs);
            gprs_prog.setProgress(upSpeed);
            Log.d("Speed", String.valueOf(downSpeed));
            String Speed = null;
            if (upSpeed >= 2000) {
                Speed = "Excellent";
            }
            if (upSpeed <= 1999 && upSpeed >= 550) {
                Speed = "Good";
            }
            if (upSpeed < 550 && upSpeed >= 150) {
                Speed = "Moderate";
            }
            if (upSpeed < 150) {
                Speed = "Very Poor";
            }
            txt_gprs.setText("GPRS : " + Speed);
            gprs_prog.setOnTouchListener((view, motionEvent) -> true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //should check null because in airplane mode it will be null


    }

    private void batterystatus() {

        registerReceiver(mBatInfoReceiver, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));
    }

    private void getlocation() {


        gpsTracker = new GPSTracker(this);
        // check if GPS enabled
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            lat = Double.toString(latitude);
            lng = Double.toString(longitude);
            txt_lat.setText("6.56");
            txt_lng.setText("3.32");
            // \n is for new line
            // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
    }


    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(DeviceInfoActivity.this, android.Manifest.permission.CAMERA);
        int result1 = ContextCompat.checkSelfPermission(DeviceInfoActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int result2 = ContextCompat.checkSelfPermission(DeviceInfoActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(DeviceInfoActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int result4 = ContextCompat.checkSelfPermission(DeviceInfoActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int result5 = ContextCompat.checkSelfPermission(DeviceInfoActivity.this, Manifest.permission.INTERNET);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
                && result3 == PackageManager.PERMISSION_GRANTED && result4 == PackageManager.PERMISSION_GRANTED && result5 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {

        ActivityCompat.requestPermissions(DeviceInfoActivity.this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET,}, PERMISSION_REQUEST_CODE);

    }


    private void showpopup() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.help_info, null);
        support = alertLayout.findViewById(R.id.support);
        cancel = alertLayout.findViewById(R.id.cancel);
        support_number1 = alertLayout.findViewById(R.id.support_number1);
        support_number2 = alertLayout.findViewById(R.id.support_number2);
        contact_person = alertLayout.findViewById(R.id.contact_person);

        txt_mail = alertLayout.findViewById(R.id.txt_mail);
        support_number1.setEnabled(false);
        support_number2.setEnabled(false);
        contact_person.setEnabled(false);

        txt_mail.setEnabled(false);
        support.setText("Support Details");

        getdetails();
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


       /* cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                dialog.dismiss();
            }
        }); */

        /*if (popupWindow == null) {
            int width = 1500;
            int height = 1500;

            LayoutInflater inflater = getLayoutInflater();
            final View alertLayout = inflater.inflate(R.layout.help_info, null);

            popupWindow = new PopupWindow(alertLayout, width, height, true);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setFocusable(true);
            popupWindow.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction()==MotionEvent.ACTION_OUTSIDE){
                        popupWindow.dismiss();
                        return true;
                    }
                    return false;
                }
            });

        }
        popupWindow.showAsDropDown(txt_help);*/
    }


    private void getdetails() {
        Cursor supportDetails = dbHandler.getSupportDetails();
        int numRows = supportDetails.getCount();
        if (numRows == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (supportDetails.moveToNext()) {
                support_number1.setText(supportDetails.getString(6));
                support_number2.setText(supportDetails.getString(7));
                txt_mail.setText(supportDetails.getString(8));
                contact_person.setText(supportDetails.getString(3));

                //support_number1.setText("+919566252882");
                //support_number2.setText("+919841719658");
                //txt_mail.setText("mSales.Emzor@kumanti.in");
                //contact_person.setText("Maruntheesh");


            }
        }
    }


    private void initializeViews() {
        marquee_txt = findViewById(R.id.marquee);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        txt_lat = findViewById(R.id.txt_lat);
        txt_lng = findViewById(R.id.txt_lng);
        txt_appversion = findViewById(R.id.txt_appversion);

        txt_gprs = findViewById(R.id.txt_gprs);
        txt_gps = findViewById(R.id.txt_gps);
        img_return = findViewById(R.id.back);
        txt_help = findViewById(R.id.txt_help);
        img_map = findViewById(R.id.img_map);
        updateAppButton = findViewById(R.id.updateAppVersionButton);
        datetime = findViewById(R.id.datetime);
        header_text = findViewById(R.id.toolbar_title);

        mdlSyncDate = findViewById(R.id.mdlSyncDateTv);
        rdlSyncDate = findViewById(R.id.rdlSyncDateTv);

        mdlSyncTime = findViewById(R.id.mdlSyncTimeTv);
        rdlSyncTime = findViewById(R.id.rdlSyncTimeTv);

        rdlSyncTable = new RDLSyncTable(getApplicationContext());

        printImageButton = findViewById(R.id.printButton);
        activateButton = findViewById(R.id.activateButton);

    }


    private void setDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();


    }

    @Override
    public void onBackPressed() {

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    class BTPrnImage extends Thread {
        private String receipt;

        private int width, height;
        private int[] img;

        public BTPrnImage(int l_width, int l_height, int[]

                img_raster) {

            img = img_raster;
            width = l_width;
            height = l_height;
        }

        public void setReceipt(String r) {
            receipt = r;
        }

        public void run() {

            try {
                this.sleep(1000);
                mbtOutputStream = mbtSocket.getOutputStream();
                byte[] CommandImagePrint = new byte[254];
                int ImageYPointer = 0, ImageXPointer = 0;
                int ImageHeight = height, ImageWidth = width;
                int ImageCounter = 0;

                // byte[] CommandImagePrint1 = { 0x1B, 0x33, 0x00 }; // Command
                // to set Line Spacing to Zero (0)
                // byte[] CommandImagePrint3 = { 0x1B, 0x24, (byte)0xC4, 0x00 };
                // // Command to set printer head

                // position to 150th Dot

                CommandImagePrint[0] = 0x1B; // Command to for bit image mode
                // please refer the previous

                // document
                CommandImagePrint[1] = 0x23; // Exc #
                CommandImagePrint[2] = (byte) ImageWidth; // 8 Vertical
                // Dots(Heights) &
                // Single Width Mode

                // selected
                CommandImagePrint[3] = (byte) (ImageHeight / 256);// f8 //
                // Decimal
                // 248 since
                // the Image
                // width is

                // 248 Pixels as mentioned above
                CommandImagePrint[4] = (byte) (ImageHeight % 256);

                // Send Command to print image
                mbtOutputStream.write(CommandImagePrint, 0, 5);

                for (ImageYPointer = 0; ImageYPointer < ImageHeight; ImageYPointer++) {
                    for (ImageXPointer = 0; ImageXPointer < ImageWidth; ImageXPointer++) {
                        CommandImagePrint[ImageXPointer] = (byte) (img[ImageCounter] & 0xFF);
                        System.out.println(CommandImagePrint[ImageXPointer]);
                        ImageCounter++;
                    }
                    // Send Command to print bit image data
                    mbtOutputStream.write(CommandImagePrint, 0, ImageWidth); // CommandImagePrint.length
                    this.sleep(20);
                }

                byte[] CommandImagePrint2 = {0x1B, 0x32}; // Command to set
                // Default Line
                // Spacing

                // Send Command to set Default Line Spacing
                mbtOutputStream.write(CommandImagePrint2, 0,
                        CommandImagePrint2.length);
                CommandImagePrint2[0] = 0x0A;
                CommandImagePrint2[1] = 0x0A;
                mbtOutputStream.write(CommandImagePrint2, 0,
                        CommandImagePrint2.length);

                this.sleep(1000);

                System.out.println("written to port : " + receipt);

                mbtOutputStream.flush();
                System.out.println("Print completed");

                mbtOutputStream.write(0x0D);
                mbtOutputStream.write(0x0D);
                mbtOutputStream.write(0x0D);
                mbtOutputStream.flush();
                FontStyleVal &= 0xF7;
                // close connection
                // mbtOutputStream.close();
                // _bluetoothConnection.close();

            } catch (Exception ioe) {
                System.out.println("Problems reading from or writing to serial port." + ioe.getMessage());
            }

        }
    }

    class myPhoneStateListener extends PhoneStateListener {
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            if (signalStrength.isGsm()) {
                if (signalStrength.getGsmSignalStrength() != 99 && signalStrength.getGsmSignalStrength() != 0)
                    //SignalStrength = (signalStrength.getGsmSignalStrength() * 2) - 113;
                    //else
                    SignalStrength = signalStrength.getGsmSignalStrength();

            } else {
                SignalStrength = signalStrength.getCdmaDbm();
            }
            gsm_prog = findViewById(R.id.gsm_progress);

            SignalStrength = -(SignalStrength);
            gsm_prog.setProgress(SignalStrength);
            String Strength = null;
            if (SignalStrength <= 79 && SignalStrength >= 50) {
                Strength = "Good";
            }
            if (SignalStrength <= 99 && SignalStrength >= 80) {
                Strength = "Average";
            }
            if (SignalStrength <= 109 && SignalStrength >= 100) {
                Strength = "Poor";
            }
            if (SignalStrength <= 120 && SignalStrength >= 110) {
                Strength = "Very Poor";
            }

            txt_gsm = findViewById(R.id.txt_gsm);
            txt_gsm.setText("GSM : " + (Strength));
            gsm_prog.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });
            Log.d("Cellular", "working");


        }
    }
}
