package in.kumanti.emzor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.DoctorPrescribedProducts;


public class DoctorVisitCompProdAdapter extends RecyclerView.Adapter<DoctorVisitCompProdAdapter.ViewHolder> {

    public ArrayList<DoctorPrescribedProducts> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener, mListener1;


    public DoctorVisitCompProdAdapter(ArrayList<DoctorPrescribedProducts> myDataset, RecyclerViewItemListener listener, RecyclerViewItemListener listener1) {
        mDataset = myDataset;
        mListener = listener;
        mListener1 = listener1;
    }

    public void add(DoctorPrescribedProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(DoctorPrescribedProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public DoctorVisitCompProdAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_doctor_visit_comp_prod, parent, false);
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener, mListener1, new CustomEtListener());
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.productName.setText(mDataset.get(position).getProductName());

//        viewHolder.customEtListener.updatePosition(position, viewHolder);
        viewHolder.prescriptionsCount.setText(mDataset.get(position).getPrescriptionsCount());

        if (!TextUtils.isEmpty(mDataset.get(position).getErrorPrescriptionsCount())) {
            viewHolder.prescriptionsCount.setError(mDataset.get(position).getErrorPrescriptionsCount());
        } else {
            viewHolder.prescriptionsCount.setError(null);
        }
        //To Set the values to the Availabilty Checkbox
        viewHolder.prescriptionsCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (position < mDataset.size()) {
                    final EditText caption = (EditText) v;
                    mDataset.get(position).setPrescriptionsCount(caption.getText().toString());
                    caption.setError(null);
                }
            }
        });

        if (position == mDataset.size() - 1)
            viewHolder.prescriptionsCount.post(new Runnable() {
                @Override
                public void run() {
                    if (viewHolder.prescriptionsCount.requestFocus()) {
                        ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        InputMethodManager inputMethodManager = (InputMethodManager) viewHolder.prescriptionsCount.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(viewHolder.prescriptionsCount, InputMethodManager.SHOW_IMPLICIT);
                    }
                }
            });
        viewHolder.removeRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                View focusedView = getActivity(context).getCurrentFocus();
                focusedView.clearFocus();*/

                View focusedView = getActivity(context).getCurrentFocus();
                InputMethodManager inputManager = (InputMethodManager) context.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
//            inputManager.hideSoftInputFromWindow(focusedView.getApplicationWindowToken(), 0);
                if (focusedView != null) {
                    focusedView.clearFocus();
                }
                try {
                    if (position < mDataset.size()) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                        alertDialog.setTitle("Delete Prescribed Product Info?");
                        //alertDialog.setMessage("your message ");
                        alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                mDataset.remove(position);
                                notifyDataSetChanged();
                                viewHolder.mListener.onClick(v, position);

                            }
                        });

                        AlertDialog dialog = alertDialog.create();
                        dialog.show();

                    }

                } catch (Exception e) {

                }


            }
        });


    }

    private Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextCallBackListener {

        public TextView productName;
        public EditText prescriptionsCount;
        public ImageButton removeRow;
        CustomEtListener customEtListener;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener, RecyclerViewItemListener listener1, CustomEtListener myCustomEditTextListener) {
            super(v);
            mListener = listener;
            mListener1 = listener1;
            customEtListener = myCustomEditTextListener;
            productName = v.findViewById(R.id.doctorVisitCompProdNameTv);
            prescriptionsCount = v.findViewById(R.id.doctorVisitCompProdPrescriptionsEt);
            removeRow = v.findViewById(R.id.doctorVisitCompProdRemoveButton);
//            prescriptionsCount.addTextChangedListener(customEtListener);
        }

        @Override
        public void updateText(String val, String val2) {


        }
    }

    /**
     * Custom class which implements Text Watcher
     */
    private class CustomEtListener implements TextWatcher {
        TextCallBackListener textCallBackListener;
        private int position;

        /**
         * Updates the position according to onBindViewHolder
         *
         * @param position - position of the focused item
         * @param pa       - object of ProductListHolder Class
         */
        public void updatePosition(int position, ViewHolder pa) {
            this.position = position;
            // assigning it to the instance of the CallBackListener
            textCallBackListener = pa;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            mDataset.get(this.position).setPrescriptionsCount(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {


        }

    }

}