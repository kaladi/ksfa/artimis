package in.kumanti.emzor.crm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.activity.NotesActivity;
import in.kumanti.emzor.adapter.CrmStatusAdapter;
import in.kumanti.emzor.eloquent.CrmMeetingsTable;
import in.kumanti.emzor.eloquent.CrmStatusTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CrmStatus;
import in.kumanti.emzor.utils.GPSTracker;

public class StatusHistoryActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionbarTitle, marqueeText, customerName, accountNo, city, checkInTime;
    Chronometer duration;

    String customerCode = "", crmNoString = "", crmDateString ="", crmStatus="", crmStatusDate ="", marqueeTextString;

    TextView crmNo, crmDate;
    Button notesButton, meetingsButton;

    CrmStatusTable crmStatusTable;
    CrmMeetingsTable crmMeetingsTable;
    MyDBHandler dbHandler;

    RecyclerView statusHisDetailsRv;
    RecyclerView.Adapter statusHisDetailsAdapter;
    RecyclerView.LayoutManager statusHisDetailsLayoutManager;

    ArrayList<CrmStatus>  statusHisDetailsArrayList = new ArrayList<>();

    GPSTracker gpsTracker;
    double latitudeDouble, longitudeDouble;
    String latitude, longitude, latitudeLongitude;
    DecimalFormat formatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_crm_status);

        initializeViews();
        populateHeaderDetails();

//        getCustomerLocation();

        actionbarTitle.setText(R.string.status_history_header);

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        crmNo.setText(crmNoString);
        crmDate.setText(crmDateString);

        notesButton.setOnClickListener(v -> {
            Intent notesIntent = new Intent(getApplicationContext(), NotesActivity.class);
            //Create the bundle
            Bundle bundle = new Bundle();
            //bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customerCode);
            //bundle.putString("login_id", );
            bundle.putString("notesType", "CRM_NOTES");
            bundle.putString("notesReferenceId", crmNo.getText().toString());

            bundle.putStringArray("notesReferenceIdList", new String[]{crmNo.getText().toString()});
            bundle.putStringArray("notesTypeList", new String[]{"CRM_NOTES"});
            //Add the bundle to the intent
            notesIntent.putExtras(bundle);
            //Fire that second activity
            startActivity(notesIntent);
        });

        meetingsButton.setOnClickListener(v -> {

            Intent meetingIntent = new Intent(getApplicationContext(), CrmMeetingActivity.class);
            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("customer_id", customerCode);
            bundle.putString("crmNo", crmNoString);
            bundle.putString("crmDate", crmDateString);
            bundle.putString("crmStatus", crmStatus);
            bundle.putString("crmStatusDate", crmStatusDate);

            //Add the bundle to the intent
            meetingIntent.putExtras(bundle);
            //Fire that second activity
            startActivity(meetingIntent);

        });

        statusHisDetailsRv.setHasFixedSize(true);
        statusHisDetailsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        statusHisDetailsRv.setLayoutManager(statusHisDetailsLayoutManager);

        statusHisDetailsArrayList = crmStatusTable.getCRMStatus(customerCode, crmNo.getText().toString());

        statusHisDetailsAdapter = new CrmStatusAdapter(statusHisDetailsArrayList);
        statusHisDetailsRv.setAdapter(statusHisDetailsAdapter);

        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);
        });


    }

    private void getCustomerLocation() {

        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitudeDouble = gpsTracker.getLatitude();
            longitudeDouble = gpsTracker.getLongitude();

            latitude = formatter.format(latitudeDouble);
            longitude = formatter.format(longitudeDouble);

            latitudeLongitude = latitude + "," + longitude;
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        crmNo = findViewById(R.id.statusHisCrmNoTv);
        crmDate = findViewById(R.id.statusHisCrmDateTv);

        notesButton = findViewById(R.id.statusHisNotesBt);
        meetingsButton = findViewById(R.id.statusHisMeetingBt);

        statusHisDetailsRv = findViewById(R.id.statusHisDetailsRv);

        crmMeetingsTable = new CrmMeetingsTable(getApplicationContext());
        crmStatusTable = new CrmStatusTable(getApplicationContext());

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
    }


    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            customerCode = bundle.getString("customer_id");
            crmNoString = bundle.getString("crmNo");
            crmDateString = bundle.getString("crmDate");
            crmStatus = bundle.getString("crmStatus");
            crmStatusDate = bundle.getString("crmStatusDate");
        }

    }


}
