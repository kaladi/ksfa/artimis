package in.kumanti.emzor.model;

public class CheckInDetails {
    private String tabCode;
    private String companyCode;
    private String srCode;
    private String tripNumber;
    private String customerId;
    private String checkInDate;
    private String checkInTime;
    private String checkInGpsLat;
    private String checkInGpsLong;

    private String checkOutDate;
    private String checkOutTime;
    private String checkOutGpsLat;
    private String checkOutGpsLong;

    private String duration;
    private String customerType;
    private String visitSequenceNumber;

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getTripNumber() {
        return tripNumber;
    }

    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckInGpsLat() {
        return checkInGpsLat;
    }

    public void setCheckInGpsLat(String checkInGpsLat) {
        this.checkInGpsLat = checkInGpsLat;
    }

    public String getCheckInGpsLong() {
        return checkInGpsLong;
    }

    public void setCheckInGpsLong(String checkInGpsLong) {
        this.checkInGpsLong = checkInGpsLong;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getCheckOutGpsLat() {
        return checkOutGpsLat;
    }

    public void setCheckOutGpsLat(String checkOutGpsLat) {
        this.checkOutGpsLat = checkOutGpsLat;
    }

    public String getCheckOutGpsLong() {
        return checkOutGpsLong;
    }

    public void setCheckOutGpsLong(String checkOutGpsLong) {
        this.checkOutGpsLong = checkOutGpsLong;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
}
