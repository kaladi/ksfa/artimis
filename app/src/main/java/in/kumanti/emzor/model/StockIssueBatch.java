package in.kumanti.emzor.model;

import java.io.Serializable;

public class StockIssueBatch implements Serializable {

    public String errorIssueQuantity;
    private String companyCode;
    private String tabCode;
    private String srCode;
    private String productCode;
    private String expiryDate;
    private String batchNumber;
    private String batchQuantity;
    private String issueQuantity;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getBatchQuantity() {
        return batchQuantity;
    }

    public void setBatchQuantity(String batchQuantity) {
        this.batchQuantity = batchQuantity;
    }

    public String getIssueQuantity() {
        return issueQuantity;
    }

    public void setIssueQuantity(String issueQuantity) {
        this.issueQuantity = issueQuantity;
    }

    public String getErrorIssueQuantity() {
        return errorIssueQuantity;
    }

    public void setErrorIssueQuantity(String errorIssueQuantity) {
        this.errorIssueQuantity = errorIssueQuantity;
    }
}
