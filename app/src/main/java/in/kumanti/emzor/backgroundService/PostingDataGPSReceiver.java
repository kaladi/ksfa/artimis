package in.kumanti.emzor.backgroundService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PostingDataGPSReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.codepath.example.servicesdemo.alarm";

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent startServiceIntent = new Intent(context, PostingDataGPSService.class);
        context.startService(startServiceIntent);

        /*Intent i = new Intent(context, PostingDataGPSService.class);
        PostingDataGPSService.enqueueJobAction(context, "action.SOME_ACTION");
       *//*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(i);
        }
        else {
            context.startService(i);
        }*/
    }
}
