package in.kumanti.emzor.utils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.BitSet;
import java.util.Set;
import java.util.UUID;

public class Print {
    String printerName = "MP80-17031810239";
    String printMsg;
    Activity activity;
    int mWidth, mHeight;
    String mStatus;
    BitSet dots;
    Context mContext;
    // android built in classes for bluetooth operations
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    public Print(Activity ac) {
        activity = ac;

    }

    public Print(Context applicationContext) {
        mContext = applicationContext;

    }

    public static String padRight(String s, int n) {
        return String.format("%-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%" + n + "s", s);
    }

    public static String centerString(int width, String s) {
        return String.format("%-" + width + "s", String.format("%" + (s.length() + (width - s.length()) / 2) + "s", s));
    }

    public void printContent(String printMsg) {

        try {
            if (findBT()) {
                if (openBT()) {
                    sendData(printMsg);
                    //print_image(image);
                    closeBT();
                } else
                    Toast.makeText(mContext, "Unable to connect printer. Please check the connection.", Toast.LENGTH_LONG).show();

            } else
                Toast.makeText(mContext, "Unable to connect printer. Please check the connection.", Toast.LENGTH_LONG).show();

        } catch (IOException ex) {
            Toast.makeText(mContext, "Unable to connect printer. Please check the connection.", Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    // this will find a bluetooth printer device
    private boolean findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            //Fetching the Printer Unique Code for connecting printer alone while printing

            /*Method getUuidsMethod = BluetoothAdapter.class.getDeclaredMethod("getUuids", null);

            ParcelUuid[] uuids = (ParcelUuid[]) getUuidsMethod.invoke(mBluetoothAdapter, null);*/

            if (mBluetoothAdapter == null) {
                Log.d("BluetoothAction", "No bluetooth adapter available");
                //myLabel.setText("No bluetooth adapter available");
                return false;
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices


                    //if (device.getName().equals(printerName)) {
                    mmDevice = device;
                    Log.d("BluetoothAction", "Bluetooth device found Fesdf.");
                    break;
                    //}
                }
            } else {
                return false;
            }
            //myLabel.setText("Bluetooth device found.");
            Log.d("BluetoothAction", "Bluetooth device found.");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    // tries to open a connection to the bluetooth printer device
    private boolean openBT() throws IOException {
        try {
            Log.d("BluetoothAction", "Bluetooth Try opening");
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            Log.d("BluetoothAction", "Bluetooth Socket Created");
            if (!mmSocket.isConnected())
                mmSocket.connect();
        } catch (Exception e) {
            Log.d("BluetoothAction", "Bluetooth " + e.getMessage());
            e.printStackTrace();
            try {
                Log.e("BluetoothAction", "trying fallback...");

                mmSocket = (BluetoothSocket) mmDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).invoke(mmDevice, 1);
                mmSocket.connect();

                Log.e("BluetoothAction", "Connected");
            } catch (Exception e2) {
                Log.e("BluetoothAction", "Couldn't establish Bluetooth connection!");
                return false;
            }
        }
        Log.d("BluetoothAction", "Bluetooth Socket Connected");

        try {
            mmOutputStream = mmSocket.getOutputStream();
            Thread.sleep(100);
            mmInputStream = mmSocket.getInputStream();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            return false;
        }


        beginListenForData();
        Log.d("BluetoothAction", "Bluetooth Opened");
        //myLabel.setText("Bluetooth Opened");
        return true;
    }


    /*
     * after opening a connection to bluetooth printer device,
     * we have to listen and check if a data were sent to be printed.
     */
    private void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
                                                //myLabel.setText(data);
                                                Log.d("BluetoothAction", "Data." + data);
                                            }
                                        });

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // this will send text data to be printed by the bluetooth printer
    private void sendData(String printMsg) throws IOException {
        try {

            new Handler().postDelayed(() -> {

            }, 100);

            new Thread(() -> {
                try {
                    mmOutputStream.write(printMsg.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();


            // tell the user data were sent
            //myLabel.setText("Data sent.");
            Log.d("BluetoothAction", "Data sent.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void print_image(Bitmap bmp) throws IOException {
        //File fl = new File(file);
        //if (f1.exists()) {
        //Bitmap bmp = BitmapFactory.decodeFile(f1);
        convertBitmap(bmp);
        mmOutputStream.write(PrinterCommands.SET_LINE_SPACING_24);

        int offset = 0;
        while (offset < bmp.getHeight()) {
            mmOutputStream.write(PrinterCommands.SELECT_BIT_IMAGE_MODE);
            for (int x = 0; x < bmp.getWidth(); ++x) {

                for (int k = 0; k < 3; ++k) {

                    byte slice = 0;
                    for (int b = 0; b < 8; ++b) {
                        int y = (((offset / 8) + k) * 8) + b;
                        int i = (y * bmp.getWidth()) + x;
                        boolean v = false;
                        if (i < dots.length()) {
                            v = dots.get(i);
                        }
                        slice |= (byte) ((v ? 1 : 0) << (7 - b));
                    }
                    mmOutputStream.write(slice);
                }
            }
            offset += 24;
            mmOutputStream.write(PrinterCommands.FEED_LINE);
            mmOutputStream.write(PrinterCommands.FEED_LINE);
            mmOutputStream.write(PrinterCommands.FEED_LINE);
            mmOutputStream.write(PrinterCommands.FEED_LINE);
            mmOutputStream.write(PrinterCommands.FEED_LINE);
            mmOutputStream.write(PrinterCommands.FEED_LINE);
        }
        mmOutputStream.write(PrinterCommands.SET_LINE_SPACING_30);


//        } else {
//            Toast.makeText(activity.getApplicationContext(), "file doesn't exists", Toast.LENGTH_SHORT)
//                    .show();
//        }
    }


    // close the connection to bluetooth printer.
    private void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            Log.d("BluetoothAction", "Bluetooth Closed");
            //myLabel.setText("Bluetooth Closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String convertBitmap(Bitmap inputBitmap) {

        mWidth = inputBitmap.getWidth();
        mHeight = inputBitmap.getHeight();

        convertArgbToGrayscale(inputBitmap, mWidth, mHeight);
        mStatus = "ok";
        return mStatus;

    }

    private void convertArgbToGrayscale(Bitmap bmpOriginal, int width, int height) {
        int pixel;
        int k = 0;
        int B = 0, G = 0, R = 0;
        dots = new BitSet();
        try {

            for (int x = 0; x < height; x++) {
                for (int y = 0; y < width; y++) {
                    // get one pixel color
                    pixel = bmpOriginal.getPixel(y, x);

                    // retrieve color of all channels
                    R = Color.red(pixel);
                    G = Color.green(pixel);
                    B = Color.blue(pixel);
                    // take conversion up to one single value by calculating
                    // pixel intensity.
                    R = G = B = (int) (0.299 * R + 0.587 * G + 0.114 * B);
                    // set bit into bitset, by calculating the pixel's luma
                    if (R < 55) {
                        dots.set(k);//this is the bitset that i'm printing
                    }
                    k++;

                }


            }


        } catch (Exception e) {
            // TODO: handle exception
            Log.e("Exception", e.toString());
        }
    }
}

class PrinterCommands {
    public static final byte[] INIT = {27, 64};
    public static byte[] FEED_LINE = {10};

    public static byte[] SELECT_FONT_A = {27, 33, 0};

    public static byte[] SET_BAR_CODE_HEIGHT = {29, 104, 100};
    public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
    public static byte[] SEND_NULL_BYTE = {0x00};

    public static byte[] SELECT_PRINT_SHEET = {0x1B, 0x63, 0x30, 0x02};
    public static byte[] FEED_PAPER_AND_CUT = {0x1D, 0x56, 66, 0x00};

    public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x11};

    public static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, -128, 0};
    public static byte[] SET_LINE_SPACING_24 = {0x1B, 0x33, 24};
    public static byte[] SET_LINE_SPACING_30 = {0x1B, 0x33, 30};

    public static byte[] TRANSMIT_DLE_PRINTER_STATUS = {0x10, 0x04, 0x01};
    public static byte[] TRANSMIT_DLE_OFFLINE_PRINTER_STATUS = {0x10, 0x04, 0x02};
    public static byte[] TRANSMIT_DLE_ERROR_STATUS = {0x10, 0x04, 0x03};
    public static byte[] TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS = {0x10, 0x04, 0x04};
}