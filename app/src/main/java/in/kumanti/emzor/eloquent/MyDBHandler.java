package in.kumanti.emzor.eloquent;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.mastersData.CustomerMaster;
import in.kumanti.emzor.mastersData.FeedbackType;
import in.kumanti.emzor.mastersData.GPSTrackerModel;
import in.kumanti.emzor.mastersData.ItemMaster;
import in.kumanti.emzor.mastersData.JourneyPlanMaster;
import in.kumanti.emzor.mastersData.LocationMaster;
import in.kumanti.emzor.mastersData.MarqueeText;
import in.kumanti.emzor.mastersData.PriceListHeader;
import in.kumanti.emzor.mastersData.PriceListLines;
import in.kumanti.emzor.model.BDEIssueProductsReport;
import in.kumanti.emzor.model.CheckInDetails;
import in.kumanti.emzor.model.CollectionDetails;
import in.kumanti.emzor.model.CompanySetup;
import in.kumanti.emzor.model.CrmPriceList;
import in.kumanti.emzor.model.CrmPurchaseHisProd;
import in.kumanti.emzor.model.CustomerDetails;
import in.kumanti.emzor.model.CustomerReturnBatch;
import in.kumanti.emzor.model.CustomerReturnProductsReport;
import in.kumanti.emzor.model.Distributors;
import in.kumanti.emzor.model.DoctorVisitSponsorship;
import in.kumanti.emzor.model.ExpiredProductsReport;
import in.kumanti.emzor.model.Feedback;
import in.kumanti.emzor.model.FeedbackReport;
import in.kumanti.emzor.model.InvoiceItems;
import in.kumanti.emzor.model.InvoiceProductItems;
import in.kumanti.emzor.model.Invoices;
import in.kumanti.emzor.model.Locations;
import in.kumanti.emzor.model.MDLSync;
import in.kumanti.emzor.model.MetricsTarget;
import in.kumanti.emzor.model.NewJourneyPlanCustomers;
import in.kumanti.emzor.model.OrderItems;
import in.kumanti.emzor.model.OrderProductSpinner;
import in.kumanti.emzor.model.Orders;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.Quarantine;
import in.kumanti.emzor.model.SRPriceList;
import in.kumanti.emzor.model.SrInvoiceBatch;
import in.kumanti.emzor.model.StockAdjustment;
import in.kumanti.emzor.model.StockIssueBatch;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.model.StockReturnProductsReport;
import in.kumanti.emzor.model.TripDetails;
import in.kumanti.emzor.model.ViewStockMyStockProduct;
import in.kumanti.emzor.model.ViewStockWarehouseProduct;
import in.kumanti.emzor.model.WaybillDetails;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.IMAGE_HOME_PATH;
import static in.kumanti.emzor.utils.Constants.TABLE_BDE_LIST;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_RETURN;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_RETURN_PRODUCTS;
import static in.kumanti.emzor.utils.Constants.TABLE_DISTRIBUTORS;
import static in.kumanti.emzor.utils.Constants.TABLE_EXPENSE;
import static in.kumanti.emzor.utils.Constants.TABLE_LEDGER;
import static in.kumanti.emzor.utils.Constants.TABLE_SHIPPING_ADDRESS;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_ISSUE;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_ISSUE_PRODUCTS;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_RETURN;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_RETURN_PRODUCTS;
import static in.kumanti.emzor.utils.Constants.TABLE_WAREHOUSE;


public class MyDBHandler extends SQLiteOpenHelper {

    // Tables used for mSales App
    public static final String TABLE_LOGIN = "xxmsales_login_details";
    //private static final String DATABASE_NAME = "/mnt/sdcard/DrugField/DrugField.DB" ;
    //private static final String DATABASE_NAME = "/mnt/sdcard/Zolon/Zolon.DB" ;
    public static final String TABLE_COMPANY_SETUP = "xxmsales_company_setup";
    public static final String TABLE_USER_SETUP = "xxmsales_user_setup";
    public static final String TABLE_TAB_SETUP = "xxmsales_tab_setup";
    public static final String TABLE_CUSTOMER_DETAILS = "xxmsales_customer_details";
    public static final String TABLE_SALESREP_DETAILS = "xxmsales_salesrep_detail";
    public static final String TABLE_ITEM_DETAILS = "xxmsales_item_details";
    public static final String TABLE_TRIP_DETAILS = "xxmsales_trip_details";
    public static final String TABLE_SALESORDER_HEADER = "xxmsales_order_header";
    public static final String TABLE_SALESORDER_DETAILS = "xxmsales_order_details";
    public static final String TABLE_INVOICE_HEADER = "xxmsales_invoice_header";
    public static final String TABLE_INVOICE_DETAILS = "xxmsales_invoice_details";
    public static final String TABLE_CHECKIN_DETAILS = "xxmsales_checkin_details";
    public static final String TABLE_STOCK_DETAILS = "xxmsales_stock_details";
    //  public static final String TABLE_WAREHOUSE = "xxmsales_warehouse";
    public static final String TABLE_PRODUCT_BATCH = "xxmsales_product_batch";
    public static final String TABLE_STOCK_RECIEPT = "xxmsales_stock_reciept";
    public static final String TABLE_PRODUCT_BATCH_TEMP = "xxmsales_product_batch_temp";
    public static final String TABLE_RECEIPT_ROW_TEMP = "xxmsales_receipt_row_temp";
    public static final String TABLE_RECEIPT_BATCH_ROW_TEMP = "xxmsales_receipt_batch_row_temp";
    public static final String TABLE_CUSTOMER_IMAGE_GALLERY = "xxmsales_customer_image_gallery";
    public static final String TABLE_LPO_GALLERY = "xxmsales_lpo_gallery";
    public static final String TABLE_COLLECTION_DETAILS = "xxmsales_collection_details";
    public static final String TABLE_METRICS_TARGET = "xxmsales_metrics_target";
    public static final String TABLE_FEEDBACK = "xxmsales_feedback";
    public static final String TABLE_ACTION_TAKEN = "xxmsales_action_taken";
    public static final String TABLE_CUSTOMER = "xxmsales_customer";
    public static final String TABLE_PRICE_LIST_HEADER = "xxmsales_price_list_header";
    public static final String TABLE_PRICE_LIST_LINES = "xxmsales_price_list_lines";
    public static final String TABLE_LOCATION_HEADER = "xxmsales_location_header";
    public static final String TABLE_LOCATION_LINES = "xxmsales_location_lines";
    public static final String TABLE_SR_INVOICE_BATCH = "xxmsales_sr_invoice_batch";
    public static final String TABLE_PRIMARY_COLLECTION_DETAILS = "xxmsales_primary_collection_details";
    public static final String TABLE_JOURNEY_PLAN_MASTER = "xxmsales_journey_plan";
    public static final String TABLE_GPS_TRACKER = "xxmsales_gps_tracker";
    public static final String TABLE_GLOBAL_MESSAGE = "xxmsales_global_message";
    public static final String TABLE_FEEDBACK_TYPE = "xxmsales_feedback_type";
    public static final String TABLE_MASTER_DOWNLOAD = "xxmsales_master_download";
    public static final String TABLE_MASTER_SEQUENCE = "xxmsales_master_sequence";
    public static final String TABLE_DOCTOR_VISIT_SPONSORSHIP = "xxmsales_dr_visit_sponsorship";
    public static final String TABLE_DR_VISIT_SPONSORSHIP_TYPE = "xxmsales_dr_visit_sponsorship_type";
    public static final String TABLE_SR_PRICELIST = "xxmsales_sr_pricelist";
    public static final String TABLE_QUARANTINE_PRODUCTS = "xxmsales_quarantine_products";
    public static final String TABLE_WAYBILL_DETAILS = "xxmsales_waybill_details";
    public static final String COLUMN_CREATED_AT = "created_at";
    public static final String COLUMN_UPDATED_AT = "updated_at";
    public static final String COLUMN_SR_ID = "id";
    public static final String COLUMN_SR_PRICE_ID = "sr_price_id";
    public static final String COLUMN_SR_CODE = "sr_code";
    public static final String COLUMN_SR_IMAGE_FILE_URL = "sr_image_url";
    public static final String COLUMN_SR_IMAGE_FILE_NAME = "sr_image_name";
    public static final String COLUMN_SPONSOR_SEQ_VALUE = "sponsor_seq_value";
    public static final String COLUMN_ORDER_SEQ_VALUE = "order_seq_value";
    public static final String COLUMN_INVOICE_SEQ_VALUE = "invoice_seq_value";
    public static final String COLUMN_COLLECTION_SEQ_VALUE = "collection_seq_value";
    public static final String COLUMN_PRIMARY_COLLECTION_SEQ_VALUE = "primary_collection_seq_value";
    public static final String COLUMN_TRIP_SEQ_VALUE = "trip_seq_value";
    public static final String COLUMN_CUSTOMER_SEQ_VALUE = "customer_seq_value";
    public static final String COLUMN_RECEIPT_NUM_SEQ_VALUE = "receipt_seq_value";

    public static final String COLUMN_FEEDBACK_ID = "feedback_id";
    public static final String COLUMN_FEEDBACK_TYPE = "feedback_type";
    public static final String COLUMN_FEEDBACK_TAB_CODE = "tab_code";
    public static final String COLUMN_FEEDBACK_BDE_CODE = "bde_code";
    public static final String COLUMN_FEEDBACK_CUSTOMER_CODE = "customer_code";
    public static final String COLUMN_FEEDBACK_ACTION_TAKEN = "action_taken";
    public static final String COLUMN_FEEDBACK_STATUS = "status";

    public static final String COLUMN_GLOBAL_MESSAGE_NAME = "message_name";
    public static final String COLUMN_GLOBAL_MESSAGE_CREATION_DATE = "message_creation_date";
    public static final String COLUMN_MASTER_DOWNLOAD = "master_download";
    public static final String COLUMN_MASTER_SYNC_DATE = "sync_date";
    public static final String COLUMN_MASTER_SYNC_TIME = "sync_time";
    // Columns used for GPS Tracker Table
    public static final String COLUMN_LATITUDE = "gps_latitude";
    public static final String COLUMN_LONGITUDE = "gps_longitude";
    public static final String COLUMN_GPS_DATE = "gps_date";
    public static final String COLUMN_GPS_TIME = "gps_time";
    public static final String COLUMN_GPS_ID = "gps_id";
    public final static String COLUMN_GPS_IS_SYNC = "is_sync";
    // Columns used for Login table
    public static final String COLUMN_lOGIN_ID = "Login_ID";
    public static final String COLUMN_FULL_NAME = "Full_Name";
    public static final String COLUMN_EMAIL = "Email";
    public static final String COLUMN_PHONE_NUMBER = "Phone_Number";
    public static final String COLUMN_LOCATION = "Location";
    public static final String COLUMN_PASSWORD = "Password";
    public static final String COLUMN_CONFIRM_PASSWORD = "Confirm_password";
    // Columns used for Company Setup
    public static final String COLUMN_CID = "Id";
    public static final String COLUMN_COMPANY_NAME = "company_name";
    public static final String COLUMN_SHORT_NAME = "short_name";
    public static final String COLUMN_CONTACT_PERSON = "contact_person";
    public static final String COLUMN_EMAIL_ID = "email_id";
    public static final String COLUMN_MOBILE_NUMBER = "mobile_number";
    public static final String COLUMN_SUPPORT_NUMBER1 = "support_number1";
    public static final String COLUMN_SUPPORT_NUMBER2 = "support_number2";
    public static final String COLUMN_SUPPORT_EMAIL = "support_email";
    public final static String COLUMN_COMPANY_LOGO = "company_logo";
    public final static String COLUMN_COMPANY_ADDRESS_LINE1 = "address_line1";
    public final static String COLUMN_COMPANY_ADDRESS_LINE2 = "address_line2";
    public final static String COLUMN_COMPANY_ADDRESS_LINE3 = "address_line3";
    public final static String COLUMN_COMPANY_LOGO_IMAGE_NAME = "imageName";
    public final static String COLUMN_COMPANY_LOGO_IMAGE_URL = "imageUrl";
    // Columns used for User Setup
    public static final String COLUMN_UID = "Id";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_USER_EMAIL_ID = "email_id";
    public static final String COLUMN_USER_PASSWORD = "password";
    public static final String COLUMN_USER_CONFIRM_PASSWORD = "confirm_password";
    public static final String COLUMN_USER_MOBILE_NUMBER = "mobile_number";
    public static final String COLUMN_EMPLOYEE_ID = "employee_id";
    public static final String COLUMN_ROLE = "role";
    public final static String COLUMN_USER_IMAGE = "user_image";
    public final static String COLUMN_USER_IMAGE_NAME = "imageName";
    public final static String COLUMN_USER_IMAGE_URL = "imageUrl";
    // Columns used for Tab Setup
    public static final String COLUMN_TID = "Id";
    public static final String COLUMN_TAB_ID = "tab_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_OTP = "otp";
    public static final String COLUMN_TAB_PREFIX = "tab_prefix";
    public static final String COLUMN_BASE_URL = "base_url";
    public static final String COLUMN_SEC_PRICE_LIST = "sec_price_list";
    public static final String COLUMN_ACTIVATE_FLAG = "activate_flag";
    public static final String COLUMN_IMEINO = "imei_no";
    // Columns used for Customers Details
    public static final String COLUMN_CUID = "Id";
    public static final String COLUMN_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_CUSTOMER_NAME = "customer_name";
    public static final String COLUMN_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_SALES_REP_NAME = "sales_rep_name";
    public static final String COLUMN_SERIAL_NUM = "serial_number";
    public static final String COLUMN_ADDRESS1 = "address1";
    public static final String COLUMN_ADDRESS2 = "address2";
    public static final String COLUMN_ADDRESS3 = "address3";
    public final static String COLUMN_CITY = "city";
    public final static String COLUMN_STATE = "state";
    public final static String COLUMN_CUS_MOBILE_NUMBER = "mobile_number";
    public final static String COLUMN_CUS_CREDIT_LIMIT = "credit_limit";
    public final static String COLUMN_CUS_CREDIT_DAYS = "credit_days";

    public final static String COLUMN_PINCODE = "pincode";
    public final static String COLUMN_CUSTOMER_IMAGE_FILE_NAME = "image_file_name";
    public static final String COLUMN_SHIP_ADDRESS1 = "ship_address1";
    public static final String COLUMN_SHIP_ADDRESS2 = "ship_address2";
    public static final String COLUMN_SHIP_ADDRESS3 = "ship_address3";
    public final static String COLUMN_SHIP_CITY = "ship_city";
    public final static String COLUMN_SHIP_STATE = "ship_state";
    public final static String COLUMN_SHIP_COUNTRY = "ship_country";
    public final static String COLUMN_CUSTOMER_TYPE = "customer_type";
    public final static String COLUMN_ACC_NUMBER = "account_number";
    public final static String COLUMN_GPS_LATITUDE = "gps_latitude";
    public final static String COLUMN_GPS_LONGITUDE = "gps_longitude";
    public final static String COLUMN_CREDIT_TERM = "credit_term";
    public final static String COLUMN_PAYMENT_TERM = "payment_term";
    public final static String COLUMN_C_PRICE_LIST_NAME = "price_list_name";
    public final static String COLUMN_C_PRICE_LIST_CODE = "price_list_code";
    public final static String COLUMN_C_CREATED_FROM = "created_from";
    public final static String COLUMN_C_LOCATION_NAME = "location_name";
    public final static String COLUMN_C_LGA = "lga";
    public final static String COLUMN_C_COUNTRY = "country";
    public final static String COLUMN_C_EMAIL = "email";
    public final static String COLUMN_C_ADD_INFO = "additional_info";
    public final static String COLUMN_CU_RANDOM_NUM = "random_num";
    public final static String COLUMN_C_CREATION_DATE = "creation_date";
    public final static String COLUMN_C_IS_IMAGE_UPLOADED = "is_image_uploaded";
    public final static String COLUMN_C_CUSTOMER_IMAGE_NAME = "image_file_name";
    public final static String COLUMN_C_CUSTOMER_IMAGE_URL = "image_url";
    public final static String COLUMN_C_CUSTOMER_GROUP = "customer_group";
    public final static String COLUMN_C_CUSTOMER_VISIT_SEQUENCE_NO = "visitSeqNo";
    // Columns used for Customers  Details New
    public static final String COLUMN_CUID_NEW = "Id";
    public static final String COLUMN_CUSTOMER_ID_NEW = "customer_id";
    public static final String COLUMN_CUSTOMER_NAME_NEW = "customer_name";
    public static final String COLUMN_SALES_REP_ID_NEW = "sales_rep_id";
    public static final String COLUMN_SALES_REP_NAME_NEW = "sales_rep_name";
    public static final String COLUMN_SERIAL_NUM_NEW = "serial_number";
    public static final String COLUMN_ADDRESS1_NEW = "address1";
    public static final String COLUMN_ADDRESS2_NEW = "address2";
    public static final String COLUMN_ADDRESS3_NEW = "address3";
    public final static String COLUMN_CITY_NEW = "city";
    public final static String COLUMN_STATE_NEW = "state";
    public final static String COLUMN_CUS_MOBILE_NUMBER_NEW = "mobile_number";
    public final static String COLUMN_CUS_CREDIT_LIMIT_NEW = "credit_limit";
    public final static String COLUMN_CUS_CREDIT_DAYS_NEW = "credit_days";
    public final static String COLUMN_PINCODE_NEW = "pincode";
    public final static String COLUMN_CUSTOMER_IMAGE_NEW = "customer_image";
    public static final String COLUMN_SHIP_ADDRESS1_NEW = "ship_address1";
    public static final String COLUMN_SHIP_ADDRESS2_NEW = "ship_address2";
    public static final String COLUMN_SHIP_ADDRESS3_NEW = "ship_address3";
    public final static String COLUMN_SHIP_CITY_NEW = "ship_city";
    public final static String COLUMN_SHIP_STATE_NEW = "ship_state";
    public final static String COLUMN_SHIP_COUNTRY_NEW = "ship_country";
    public final static String COLUMN_NEW_CUSTOMER_IS_SYNC = "is_sync";
    public final static String COLUMN_NEW_CUSTOMER_VISIT_SEQUENCE_NO = "visitSeqNo";
    public final static String COLUMN_NEW_CUSTOMER_DISTRIBUTOR_NAME = "distributor_name";
    public final static String COLUMN_NEW_CUSTOMER_DISTRIBUTOR_CODE = "distributor_code";
    // Columns used for SaleRep Details
    public static final String COLUMN_SUID = "Id";
    public static final String COLUMN_S_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_S_SALES_REP_NAME = "sales_rep_name";
    public static final String COLUMN_MANAGER = "manager";
    public static final String COLUMN_REGION = "region";
    public static final String COLUMN_WAREHOUSE = "warehouse";
    public final static String COLUMN_GPS = "gps";
    // Columns used for Item Details
    public static final String COLUMN_IUID = "Id";
    public static final String COLUMN_PRODUCT_ID = "product_id";
    public static final String COLUMN_PRODUCT_NAME = "product_name";
    public static final String COLUMN_PRODUCT_DESC = "product_desc";
    public static final String COLUMN_PRODUCT_UOM = "product_uom";
    public static final String COLUMN_STOCK_UOM = "stock_uom";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_PRODUCT_TYPE = "product_type";
    public static final String COLUMN_BATCH_CONTROLLED = "batch_controlled";
    public static final String COLUMN_VAT = "vat";
    public static final String COLUMN_CONVERSION = "conversion";
    // Columns used for Trip Details
    public static final String COLUMN_TRIP_ID = "trip_id";
    public static final String COLUMN_TRIP_NUMBER = "trip_number";
    public static final String COLUMN_TRIP_DATE = "trip_date";
    public static final String COLUMN_TRIP_TIME = "trip_time";
    public static final String COLUMN_STARTING_KM = "starting_km";
    public static final String COLUMN_ODOMETER_IMAGE = "odometer_image";
    public static final String COLUMN_TRIP_FILE_NAME = "trip_file_name";
    public static final String COLUMN_TRIP_NAME = "trip_name";
    public static final String COLUMN_TR_RANDOM_NUM = "random_num";
    public static final String COLUMN_TRIP_STOP_DATE = "trip_stop_date";
    public static final String COLUMN_TRIP_STOP_TIME = "trip_stop_time";
    public static final String COLUMN_TRIP_IS_IMAGE_UPLOADED = "is_image_uploaded";
    public static final String COLUMN_TRIP_IS_SYNC = "is_sync";
    // Columns used for Sales Order Header table
    public static final String COLUMN_SOHID = "Id";
    public static final String COLUMN_O_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_O_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_SALESORDER_ID = "salesorder_id";
    public static final String COLUMN_SALESORDER_NUMBER = "salesorder_number";
    public static final String COLUMN_SALESORDER_DATE = "salesorder_date";
    public static final String COLUMN_SALESORDER_TIME = "order_time";
    public static final String COLUMN_SALESORDER_STATUS = "salesorder_status";
    public static final String COLUMN_SALESORDER_VALUE = "salesorder_value";
    public static final String COLUMN_SALESORDER_LINES = "salesorder_lines";
    public static final String COLUMN_LPO_NUMBER = "lpo_number";
    public static final String COLUMN_REMARKS = "remarks";
    public static final String COLUMN_TEMPLATE_NAME = "template_name";
    public static final String COLUMN_TEMPLATE_ID = "template_id";
    public static final String COLUMN_DELIVERY_DATE = "delivery_date";
    public static final String COLUMN_SIGNATURE = "signature";
    public final static String COLUMN_C_SHIP_ADDRESS1 = "ship_address1";
    public final static String COLUMN_C_SHIP_ADDRESS2 = "ship_address2";
    public final static String COLUMN_C_RANDOM_NUM = "random_num";
    public final static String COLUMN_ORDERS_IS_SYNC = "is_sync";
    public final static String COLUMN_ORDERS_EMAIL_SENT = "email_sent";
    public final static String COLUMN_ORDERS_SMS_SENT = "sms_sent";
    public final static String COLUMN_ORDERS_PRINT_DONE = "print_done";
    public final static String COLUMN_ORDERS_VISIT_SEQUENCE_NO = "visitSeqNo";
    public final static String COLUMN_ORDERS_SIGNATURE_NAME = "signature_name";
    public final static String COLUMN_ORDERS_IS_IMAGE_UPLOADED = "isImageUploaded";
    public final static String COLUMN_ORDERS_SIGNATURE_PATH = "signatureFilePath";
    public final static String COLUMN_ORDERS_SIGNATURE_IMAGE_NAME = "signature_imagename";
    // Columns used for Sales Order Details table
    public static final String COLUMN_SODID = "Id";
    public static final String COLUMN_OD_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_OD_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_OD_SALESORDER_ID = "salesorder_id";
    public static final String COLUMN_OD_SALESORDER_NUMBER = "salesorder_number";
    public static final String COLUMN_ODSALESORDER_LINE_ID = "salesorder_line_id";
    public static final String COLUMN_OD_SALESORDER_DATE = "salesorder_date";
    public static final String COLUMN_OD_SALESORDER_STATUS = "salesorder_status";
    public static final String COLUMN_OD_SALESORDER_VALUE = "salesorder_value";
    public static final String COLUMN_OD_SALESORDER_LINES = "salesorder_lines";
    public static final String COLUMN_OD_PRODUCT_ID = "product_id";
    public static final String COLUMN_OD_PRODUCT_NAME = "product_name";
    public static final String COLUMN_OD_PRODUCT_UOM = "product_uom";
    public static final String COLUMN_OD_QUANTITY = "quantity";
    public static final String COLUMN_OD_PRICE = "price";
    public static final String COLUMN_OD_VALUE = "value";
    public final static String COLUMN_ORDER_ITEMS_IS_SYNC = "is_sync";
    public final static String COLUMN_OD_ONHAND_STOCK = "onhand_stock";
    // Columns used for Invoice Header table
    public static final String COLUMN_IID = "Id";
    public static final String COLUMN_I_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_I_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_INVOICE_ID = "invoice_id";
    public static final String COLUMN_INVOICE_NUMBER = "invoice_number";
    public static final String COLUMN_INVOICE_DATE = "invoice_date";
    public static final String COLUMN_INVOICE_TIME = "invoice_time";
    public static final String COLUMN_INVOICE_STATUS = "invoice_status";
    public static final String COLUMN_INVOICE_VALUE = "invoice_value";
    public static final String COLUMN_INVOICE_LINES = "invoice_lines";
    public static final String COLUMN_ID_PAID_AMOUNT = "paid_amount";
    public static final String COLUMN_ID_BALANCE_AMOUNT = "balance_amount";
    public static final String COLUMN_ID_CHANGE_TYPE = "change_type";
    public static final String COLUMN_INVOICE_SIGNATURE = "signature";
    public final static String COLUMN_INVOICES_IS_SYNC = "is_sync";
    public final static String COLUMN_I_RANDOM_NUM = "random_num";
    public final static String COLUMN_INVOICE_EMAIL_SENT = "email_sent";
    public final static String COLUMN_INVOICE_SMS_SENT = "sms_sent";
    public final static String COLUMN_INVOICE_PRINT_DONE = "print_done";
    public final static String COLUMN_INVOICE_VISIT_SEQUENCE_NO = "visitSeqNo";
    public final static String COLUMN_INVOICE_RETURN_QUANTITY = "returnQuantity";
    public final static String COLUMN_INVOICE_SIGNEE_NAME = "signeeName";
    public final static String COLUMN_INVOICE_IS_IMAGE_UPLOADED = "isImageUploaded";
    public final static String COLUMN_INVOICE_SIGNATURE_PATH = "signatureFilePath";
    public final static String COLUMN_INVOICE_SIGNATURE_IMAGE_NAME = "signature_imagename";


    // Columns used for Invoice Details table
    public static final String COLUMN_IDID = "Id";
    public static final String COLUMN_ID_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_ID_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_ID_INVOICE_ID = "invoice_id";
    public static final String COLUMN_ID_INVOICE_NUMBER = "invoice_number";
    public static final String COLUMN_ID_INVOICE_LINE_ID = "invoice_line_id";
    public static final String COLUMN_ID_INVOICE_DATE = "invoice_date";
    public static final String COLUMN_ID_INVOICE_STATUS = "invoice_status";
    public static final String COLUMN_ID_INVOICE_VALUE = "invoice_value";
    public static final String COLUMN_ID_INVOICE_LINES = "invoice_lines";
    public static final String COLUMN_ID_PRODUCT_ID = "product_id";
    public static final String COLUMN_ID_PRODUCT_NAME = "product_name";
    public static final String COLUMN_ID_PRODUCT_UOM = "product_uom";
    public static final String COLUMN_ID_QUANTITY = "quantity";
    public static final String COLUMN_ID_PRICE = "price";
    public static final String COLUMN_ID_VALUE = "value";
    public static final String COLUMN_ID_DISCOUNT = "discount";
    public static final String COLUMN_ID_VAT = "vat";
    public static final String COLUMN_ID_VALUE_WITHOUT_DIS = "value_without_discount";
    public static final String COLUMN_ID_VALUE_WITHOUT_VAT = "value_without_vat";
    public static final String COLUMN_ID_VALUE_WITH_VAT = "value_with_vat";
    public static final String COLUMN_ID_DISCOUNT_VALE = "discount_value";
    public final static String COLUMN_INVOICE_ITEMS_IS_SYNC = "is_sync";
    // Columns used for CheckIN Details
    public static final String COLUMN_CHECK_ID = "check_id";
    public static final String COLUMN_C_TRIP_NUMBER = "trip_number";
    public static final String COLUMN_C_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_CHECKIN_DATE = "checkin_date";
    public static final String COLUMN_CHECKIN_TIME = "checkin_time";
    public static final String COLUMN_CHECKIN_GPS_LAT = "checkin_gps_lat";
    public static final String COLUMN_CHECKIN_GPS_LONG = "checkin_gps_long";
    public static final String COLUMN_CHECKOUT_DATE = "checkout_date";
    public static final String COLUMN_CHECKOUT_TIME = "checkout_time";
    public static final String COLUMN_CHECKOUT_GPS_LAT = "checkout_gps_lat";
    public static final String COLUMN_CHECKOUT_GPS_LONG = "checkout_gps_long";
    public static final String COLUMN_CHECKOUT_DURATION = "duration";
    public static final String COLUMN_CHECKIN_SR = "sales_rep_id";
    public static final String COLUMN_CHECKIN_CUST_TYPE = "customer_type";
    public static final String COLUMN_CHECKIN_IS_SYNC = "is_sync";
    public final static String COLUMN_CHECKIN_VISIT_SEQUENCE_NO = "visitSeqNo";
    // Columns used for Stock Reciept
    public static final String COLUMN_STOCK_RECIEPT_ID = "Id";
    public static final String COLUMN_STOCK_RECIEPT_DATE = "sr_date";
    public static final String COLUMN_STOCK_RECIEPT_CURRENCY = "sr_currency";
    public static final String COLUMN_STOCK_RECIEPT_SR_NAME = "sr_sr_name";
    public static final String COLUMN_SR_SOURCE_NAME = "sr_source_name";
    public static final String COLUMN_SR_INVOICE = "sr_invoice";
    public static final String COLUMN_SR_SOURCE_TYPE = "sr_source_type";
    public static final String COLUMN_SR_WAREHOUSE_NAME = "sr_warehouse_name";
    public static final String COLUMN_SR_PRODUCTNAME = "sr_product_name";
    public static final String COLUMN_SR_PRODUCT_UOM = "sr_product_uom";
    public static final String COLUMN_SR_PRODUCT_QUANTITY = "sr_product_quantity";
    public static final String COLUMN_SR_PRODUCT_PRICE = "sr_product_price";
    public static final String COLUMN_SR_PRODUCT_VALUE = "sr_product_value";
    public static final String COLUMN_SR_REMARKS = "sr_remarks";


    public static final String COLUMN_STOCK_RECIEPT_NUMBER = "sr_receipt_number";
    public static final String COLUMN_STOCK_INVOICE_ID = "invoice_id";
    public static final String COLUMN_STOCK_STATUS = "status";
    public final static String COLUMN_STOCK_RECEIPT_IS_SYNC = "is_sync";
    public final static String COLUMN_STOCK_RANDOM_NUM = "random_num";
    public final static String COLUMN_STOCK_RECEIPT_VISIT_SEQUENCE_NO = "visitSeqNo";
    public final static String COLUMN_STOCK_RECEIPT_REFERENCE_NUMBER = "referenceNumber";
    public final static String COLUMN_STOCK_RECEIPT_TRANSFERRED_TO = "transferredTo";
    // Columns used for Stock Details
    public static final String COLUMN_STOCK_ID = "Id";
    public static final String COLUMN_S_SR_CODE = "s_sr_code";
    public static final String COLUMN_S_PRODUCT_ID = "s_product_id";
    public static final String COLUMN_S_PRODUCT_NAME = "s_product_name";
    public static final String COLUMN_S_PRODUCT_UOM = "s_product_uom";
    public static final String COLUMN_S_PRODUCT_QUANTITY = "s_product_quantity";
    public static final String COLUMN_S_PRODUCT_WAREHOUSE = "s_product_warehouse";
    public static final String COLUMN_S_PRODUCT_VALUE = "s_product_value";
    public static final String COLUMN_S_PRODUCT_PRICE = "s_product_price";
    public static final String COLUMN_S_PRODUCT_ORDER_COUNT = "s_product_order_count";
    // Columns used for Warehouse Details
    public static final String COLUMN_WAREHOUSE_ID = "Id";
    public static final String COLUMN_W_WAREHOUSE_ID = "w_warehouse_id";
    public static final String COLUMN_W_WAREHOUSE_NAME = "w_warehouse_name";
    public static final String COLUMN_W_SR_CODE = "w_sr_code";
    // Columns used for Distributor Details
    public static final String COLUMN_D_DID = "Id";
    public static final String COLUMN_DISTRIBUTOR_ID = "d_distributor_id";
    public static final String COLUMN_DISTRIBUTO_NAME = "d_distributor_name";
    public static final String COLUMN_DISTRIBUTOR_SR_CODE = "d_sr_code";
    // Columns used for Product Batch
    public static final String COLUMN_PRODUCTBATCH_ID = "Id";
    public static final String COLUMN_B_PRODUCT_ID = "b_product_id";
    public static final String COLUMN_B_EXPIRY_DATE = "b_expiry_date";
    public static final String COLUMN_B_BATCH_NUMBER = "b_batch_number";
    public static final String COLUMN_B_BATCH_QUANTITY = "b_batch_quantity";
    public static final String COLUMN_B_BATCH_INVOICE = "b_batch_invoice";
    public static final String COLUMN_B_RECEIPT_NO = "b_receipt_no";
    public static final String COLUMN_B_INVOICE_ID = "b_invoice_id";
    public static final String COLUMN_B_TEMP_NUMBER = "b_batch_temp_number";
    public static final String COLUMN_B_STATUS = "b_batch_status";
    public static final String COLUMN_BATCH_IS_SYNC = "is_sync";
    public static final String COLUMN_BATCH_TRANSACTION_TYPE = "transactionType";
    // Columns used for Product Batch Temp
    public static final String COLUMN_PT_PRODUCTBATCH_ID = "Id";
    public static final String COLUMN_PT_PRODUCT_ID = "b_product_id";
    public static final String COLUMN_PT_EXPIRY_DATE = "b_expiry_date";
    public static final String COLUMN_PT_BATCH_NUMBER = "b_batch_number";
    public static final String COLUMN_PT_BATCH_QUANTITY = "b_batch_quantity";
    public static final String COLUMN_PT_BATCH_INVOICE = "b_batch_invoice";
    public static final String COLUMN_PT_INVOICE_ID = "b_invoice_id";
    public static final String COLUMN_PT_BATCH_STATUS = "b_batch_status";
    // Columns used for Receipt Table Temp
    public static final String COLUMN_R_TEMP_ID = "Id";
    public static final String COLUMN_R_PRODUCT_NAME = "r_product_name";
    public static final String COLUMN_R_PRODUCT_UOM = "r_product_uom";
    public static final String COLUMN_R_PRODUCT_QTY = "r_product_qty";
    public static final String COLUMN_R_PRODUCT_PRICE = "r_product_price";
    public static final String COLUMN_R_PRODUCT_VALUE = "r_product_value";
    // Columns used for Receipt Batch Temp
    public static final String COLUMN_B_TEMP_ID = "Id";
    public static final String COLUMN_B_TEMPEXPIRY_DATE = "b_temp_expiry_date";
    public static final String COLUMN_B_TEMPBATCH_NUMBER = "b_temp_batch_number";
    public static final String COLUMN_B_TEMPBATCH_QUANTITY = "b_temp_batch_quantity";
    // Columns used for Customer Image Gallery
    public static final String COLUMN_CUSTOMER_GALLERY_ID = "Id";
    public static final String COLUMN_CUSTOMER_GALLERY_IMAGE_ID = "cig_image_id";
    public static final String COLUMN_CUSTOMER_GALLERY_IMAGE_NAME = "cig_image_name";
    public static final String COLUMN_CUSTOMER_GALLERY_IMAGE_ = "cig_image";
    public static final String COLUMN_CUSTOMER_GALLERY_CUSTOMERID = "customer_id";
    public static final String COLUMN_CUSTOMER_GALLERY_IMAGE_DATE = "cig_image_date";
    public static final String COLUMN_CUSTOMER_GALLERY_IMAGE_GPS = "cig_image_gps";
    public static final String COLUMN_CUSTOMER_GALLERY_IMAGE_IS_UPLOADED = "cig_image_is_uploaded";
    public final static String COLUMN_CUSTOMER_VISIT_SEQUENCE_NO = "visitSeqNo";
    // Columns used for LPO Gallery
    public static final String COLUMN_LPO_ID = "Id";
    public static final String COLUMN_LPO_IMAGE_NAME = "lpo_image_name";
    public static final String COLUMN_LPO_IMAGE = "lpo_image";
    public static final String COLUMN_LPO_IMAGE_ID = "lpo_image_id";
    public static final String COLUMN_LPO_ORDER_ID = "lpo_order_id";
    // Columns used for Collection Details table
    public static final String COLUMN_CL_ID = "Id";
    public static final String COLUMN_CL_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_CL_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_CL_COLLECTION_ID = "collection_id";
    public static final String COLUMN_CL_COLLECTION_NUMBER = "collection_number";
    public static final String COLUMN_CL_COLLECTION_DATE = "collection_date";
    public static final String COLUMN_CL_COLLECTION_TIME = "collection_time";
    public static final String COLUMN_CL_INVOICE_AMOUNT = "invoice_amount";
    public static final String COLUMN_CL_PAID_AMOUNT = "paid_amount";
    public static final String COLUMN_CL_BALANCE_AMOUNT = "balance_amount";
    public static final String COLUMN_CL_COLLECTION_TYPE = "collection_type";
    public static final String COLUMN_CL_PAYMENT_AMOUNT = "payment_amount";
    public static final String COLUMN_CL_SIGN = "sign";
    public static final String COLUMN_CL_PAYMENT_MODE = "payment_mode";
    public static final String COLUMN_CL_CHECK_NUM = "check_number";
    public static final String COLUMN_CL_CHECK_DATE = "cheque_date";
    public static final String COLUMN_CL_BANK_BRANCH = "bank_branch";
    public static final String COLUMN_CL_INVOICE_ID = "invoice_id";
    public static final String COLUMN_CL_INVOICE_NUM = "invoice_num";
    public final static String COLUMN_CL_RANDOM_NUM = "random_num";
    public final static String COLUMN_CL_IS_SYNC = "is_sync";
    public final static String COLUMN_CL_T_BANK_BRANCH = "transfer_bank_branch";
    public final static String COLUMN_CL_T_TELLER_NUMBER = "teller_number";
    public final static String COLUMN_CL_T_ACC_NUM = "transfer_account_number";
    public final static String COLUMN_CL_T_TRANSFER_DATE = "transfer_date";
    public final static String COLUMN_CL_EMAIL_SENT = "email_sent";
    public final static String COLUMN_CL_SMS_SENT = "sms_sent";
    public final static String COLUMN_CL_PRINT_DONE = "print_done";
    public static final String COLUMN_CL_INVOICE_DATE = "invoice_date";
    public static final String COLUMN_CL_COLLECTION_CREATED_AT = "created_at";
    public static final String COLUMN_CL_COLLECTION_UPDATED_AT = "updated_at";
    public final static String COLUMN_CL_VISIT_SEQUENCE_NO = "visitSeqNo";
    public final static String COLUMN_CL_TRANSACTION_TYPE = "transactionType";
    public final static String COLUMN_CL_SIGNEE_NAME = "signeeName";
    public final static String COLUMN_CL_STATUS = "collectionStatus";
    public final static String COLUMN_CL_IS_IMAGE_UPLOADED = "isImageUploaded";
    public final static String COLUMN_CL_SIGNATURE_PATH = "signatureFilePath";
    public final static String COLUMN_CL_SIGNATURE_IMAGE_NAME = "signature_imagename";


    // Columns used for Primary Collection Details table
    public static final String COLUMN_P_CL_ID = "Id";
    public static final String COLUMN_P_CL_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_P_CL_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_P_CL_COLLECTION_ID = "collection_id";
    public static final String COLUMN_P_CL_COLLECTION_NUMBER = "collection_number";
    public static final String COLUMN_P_CL_COLLECTION_DATE = "collection_date";
    public static final String COLUMN_P_CL_INVOICE_AMOUNT = "invoice_amount";
    public static final String COLUMN_P_CL_PAID_AMOUNT = "paid_amount";
    public static final String COLUMN_P_CL_BALANCE_AMOUNT = "balance_amount";
    public static final String COLUMN_P_CL_COLLECTION_TYPE = "collection_type";
    public static final String COLUMN_P_CL_PAYMENT_AMOUNT = "payment_amount";
    public static final String COLUMN_P_CL_SIGN = "sign";
    public static final String COLUMN_P_CL_PAYMENT_MODE = "payment_mode";
    public static final String COLUMN_P_CL_CHECK_NUM = "check_number";
    public static final String COLUMN_P_CL_CHECK_DATE = "cheque_date";
    public static final String COLUMN_P_CL_BANK_BRANCH = "bank_branch";
    public static final String COLUMN_P_CL_INVOICE_ID = "invoice_id";
    public static final String COLUMN_P_CL_INVOICE_NUM = "invoice_num";
    public final static String COLUMN_P_CL_IS_SYNC = "is_sync";
    public final static String COLUMN_P_CL_VISIT_SEQUENCE_NO = "visitSeqNo";
    public final static String COLUMN_P_CL_SIGNEE_NAME = "signeeName";
    public final static String COLUMN_P_CL_STATUS = "collectionStatus";
    public final static String COLUMN_P_CL_RANDOM_NUM = "random_num";
    public final static String COLUMN_P_CL_T_BANK_BRANCH = "transfer_bank_branch";
    public final static String COLUMN_P_CL_T_TELLER_NUMBER = "teller_number";
    public final static String COLUMN_P_CL_T_ACC_NUM = "transfer_account_number";
    public final static String COLUMN_P_CL_T_TRANSFER_DATE = "transfer_date";
    public final static String COLUMN_P_CL_ORDER_NUMBER = "order_number";
    public final static String COLUMN_P_CL_EMAIL_SENT = "email_sent";
    public final static String COLUMN_P_CL_SMS_SENT = "sms_sent";
    public final static String COLUMN_P_CL_PRINT_DONE = "print_done";
    public static final String COLUMN_P_CL_INVOICE_DATE = "invoice_date";
    public static final String COLUMN_P_CL_IS_IMAGE_UPLOADED = "isImageUploaded";
    public final static String COLUMN_P_CL_SIGNATURE_PATH = "signatureFilePath";
    public final static String COLUMN_P_CL_SIGNATURE_IMAGE_NAME = "signature_imagename";


   /* // Columns used for Metrics Target table
    public static final String COLUMN_MT_ID = "Id";
    public static final String COLUMN_MT_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_MT_TARGET_ID = "targetID";
    public static final String COLUMN_MT_TARGET_CODE = "targetCode";
    public static final String COLUMN_MT_TARGET_TYPE = "targetType";
    public static final String COLUMN_MT_TARGET_VALUE = "targetValue";
    public static final String COLUMN_MT_TYPE = "type";
    public static final String COLUMN_MT_PRODUCT = "product";
    public static final String COLUMN_MT_VALUE = "value";
    public static final String COLUMN_MT_FROM_DATE = "fromDate";
    public static final String COLUMN_MT_TO_DATE = "toDate";*/

    // Columns used for Metrics Target table
    public static final String COLUMN_MT_ID = "Id";
    public static final String COLUMN_MT_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_MT_TARGET_ID = "targetID";
    public static final String COLUMN_MT_TARGET_CODE = "targetCode";
    public static final String COLUMN_MT_TARGET_TYPE = "targetType";
    public static final String COLUMN_MT_TARGET_VALUE = "targetValue";
    public static final String COLUMN_MT_TYPE = "type";
    public static final String COLUMN_MT_PRODUCT = "product";
    public static final String COLUMN_MT_VALUE = "value";
    public static final String COLUMN_MT_FROM_DATE = "fromDate";
    public static final String COLUMN_MT_TO_DATE = "toDate";
    public static final String COLUMN_MT_CUST_TYPE = "cType";



    // Columns used for Feedback table
    public static final String COLUMN_F_ID = "Id";
    public static final String COLUMN_F_COMPANY_ID = "company_id";
    public static final String COLUMN_F_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_F_TAB_ID = "tab_id";
    public static final String COLUMN_F_CUST_ID = "customer_id";
    public static final String COLUMN_FEEDBACK_CODE = "feedback_code";
    public static final String COLUMN_F_TYPE = "type";
    public static final String COLUMN_F_DATE = "feedback_date";
    public static final String COLUMN_F_ACTIONTAKEN = "action_taken";
    public static final String COLUMN_F_FEEDBACK = "feedback";
    public static final String COLUMN_F_STATUS = "status";
    public final static String COLUMN_FEEDBACK_SYNC = "is_sync";
    public final static String COLUMN_F_RANDOM_NUM = "random_num";
    public final static String COLUMN_F_VISIT_SEQUENCE_NO = "visitSeqNo";
    // Columns used for Action Taken table
    public static final String COLUMN_ACTION_ID = "Id";
    public static final String COLUMN_A_ACTION_DATE = "action_date";
    public static final String COLUMN_A_ACTIONTAKEN = "action_taken";
    public static final String COLUMN_A_FEEDBACK_CODE = "feedback_code";
    // Columns used for Price List Header
    public static final String COLUMN_P_ID = "Id";
    public static final String COLUMN_PRICE_ID = "price_id";
    public static final String COLUMN_PRICE_LIST_CODE = "price_list_code";
    public static final String COLUMN_PRICE_LIST_NAME = "price_list_name";
    public static final String COLUMN_PRICE_DESCRIPTION = "price_description";
    public static final String COLUMN_PRICE_LIST_TYPE = "price_list_type";
    public static final String COLUMN_EFFECTIVE_START_DATE = "effective_start_date";
    public static final String COLUMN_EFFECTIVE_END_DATE = "effective_end_date";
    // Columns used for Price List Lines
    public static final String COLUMN_PL_ID = "Id";
    public static final String COLUMN_PL_PRICE_ID = "price_id";
    public static final String COLUMN_PL_PRODUCT_ID = "product_id";
    public static final String COLUMN_PL_PRODUCT_CODE = "product_code";
    public static final String COLUMN_PL_PRICE = "price";
    public static final String COLUMN_PL_PRICE1 = "price2";
    public static final String COLUMN_PL_EFFECTIVE_START_DATE = "effective_start_date";
    public static final String COLUMN_PL_EFFECTIVE_END_DATE = "effective_end_date";
    // Columns used for Locations Header
    public static final String COLUMN_L_ID = "Id";
    public static final String COLUMN_LOCATION_ID = "location_id";
    public static final String COLUMN_LOCATION_NAME = "location_name";
    // Columns used for Locations Lines
    public static final String COLUMN_LI_ID = "Id";
    public static final String COLUMN_L_LOCATION_ID = "location_id";
    public static final String COLUMN_L_LOCATION_NAME = "location_name";
    public static final String COLUMN_L_LGA_ID = "lga_id";
    public static final String COLUMN_L_LGA = "lga";
    public static final String COLUMN_L_CITY_ID = "city_id";
    public static final String COLUMN_L_CITY = "city";
    public static final String COLUMN_L_STATE_ID = "state_id";
    public static final String COLUMN_L_STATE = "state";
    public static final String COLUMN_L_COUNTRY_ID = "country_id";
    public static final String COLUMN_L_COUNTRY = "country";

    //Invoice Batch

    public static final String COLUMN_SR_BATCH_ID = "Id";
    public static final String COLUMN_SR_BATCH_PRODUCT_ID = "b_product_id";
    public static final String COLUMN_SR_BATCH_PRODUCT_NAME = "b_product_name";
    public static final String COLUMN_SR_BATCH_EXPIRY_DATE = "b_expiry_date";
    public static final String COLUMN_SR_BATCH_BATCH_NUMBER = "b_batch_number";
    public static final String COLUMN_SR_BATCH_BATCH_QUANTITY = "b_batch_quantity";
    public static final String COLUMN_SR_BATCH_INVOICE_ID = "b_invoice_id";
    public static final String COLUMN_SR_BATCH_BATCH_STATUS = "b_batch_status";

    // Columns used for Journey Plan Master
    public static final String COLUMN_JP_ID = "Id";
    public static final String COLUMN_JP_SEQ_NUM = "serial_number";
    public static final String COLUMN_JP_CUSTOMER_CODE = "customer_id";
    public static final String COLUMN_JP_CUSTOMER_NAME = "customer_name";
    public static final String COLUMN_JP_LOCATION = "city";
    public static final String COLUMN_JP_CUSTOMER_TYPE = "customer_type";
    public final static String COLUMN_JP_GPS_LATITUDE = "gps_latitude";
    public final static String COLUMN_JP_GPS_LONGITUDE = "gps_longitude";
    // Columns used for Doctor Visit Sponsorship
    public static final String COLUMN_DVS_ID = "Id";
    public static final String COLUMN_DV_SPONSORSHIP_ID = "sponsorship_id";
    public static final String COLUMN_DV_SPONSORSHIP_TYPE = "type";
    public static final String COLUMN_DV_SPONSORSHIP_EVENT = "event";
    public static final String COLUMN_DV_SPONSORSHIP_FROM_DATE = "from_date";
    public static final String COLUMN_DV_SPONSORSHIP_TO_DATE = "to_date";
    public final static String COLUMN_DV_SPONSORSHIP_BUDGET_AMOUNT = "budget_amount";
    public final static String COLUMN_DV_SPONSORSHIP_CUSTOMER_ID = "customer_id";
    public final static String COLUMN_DV_SPONSORSHIP_RANDOM_NUM = "random_num";
    public final static String COLUMN_DV_SPONSORSHIP_STATUS = "status";
    public final static String COLUMN_DV_SPONSORSHIP_CURRENCY = "currency";
    public final static String COLUMN_DV_SPONSORSHIP_SEQUENCE_NO = "visitSeqNo";
    // Columns used for Doctor Visit Sponsorship Type
    public final static String COLUMN_DVS_SPONSORSHIP_TYPE_ID = "Id";
    public final static String COLUMN_DVS_SPONSORSHIP_TYPE = "type";
    // Columns used for Quarantine Products
    public static final String COLUMN_Q_ID = "Id";
    public static final String COLUMN_Q_PRODUCT_CODE = "product_code";
    public static final String COLUMN_Q_BATCH_NUMBER = "batch_number";
    public static final String COLUMN_Q_BATCH_EXPIRY_DATE = "expiry_date";
    public static final String COLUMN_Q_BATCH_QUANTITY = "batch_quantity";
    public static final String COLUMN_Q_QUARANTINE_QUANTITY = "quarantine_quantity";
    public final static String COLUMN_Q_QUARANTINE_DATE = "quarantine_date";
    public final static String COLUMN_Q_QUARANTINE_SEQUENCE_NO = "visitSeqNo";
    // Columns used for Waybill table
    public static final String COLUMN_DC_ID = "Id";
    public static final String COLUMN_DC_SALES_REP_ID = "sales_rep_id";
    public static final String COLUMN_DC_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_DC_SALESORDER_ID = "salesorder_id";
    public static final String COLUMN_DC_SALESORDER_NUMBER = "salesorder_number";
    public static final String COLUMN_DC_SALESORDER_LINE_ID = "salesorder_line_id";
    public static final String COLUMN_DC_SALESORDER_DATE = "salesorder_date";
    public static final String COLUMN_DC_SALESORDER_STATUS = "salesorder_status";
    public static final String COLUMN_DC_SALESORDER_VALUE = "salesorder_value";
    public static final String COLUMN_DC_SALESORDER_LINES = "salesorder_lines";
    public static final String COLUMN_DC_PRODUCT_ID = "product_id";
    public static final String COLUMN_DC_PRODUCT_NAME = "product_name";
    public static final String COLUMN_DC_PRODUCT_UOM = "product_uom";
    public static final String COLUMN_DC_QUANTITY = "quantity";
    public static final String COLUMN_DC_PRICE = "price";
    public static final String COLUMN_DC_VALUE = "value";
    public final static String COLUMN_DC_IS_SYNC = "is_sync";
    public final static String COLUMN_DC_WAYBILL_NUMBER = "waybill_number";
    public final static String COLUMN_DC_WAYBILL_DATE = "waybill_date";
    public final static String COLUMN_DC_WAYBILL_QUANTITY = "waybill_quantity";
    public final static String COLUMN_DC_RANDOM_NUMBER = "random_number";
    public final static String COLUMN_DC_WAYBILL_STATUS = "waybill_status";
    public final static String COLUMN_WAYBILL_SIGNEE_NAME = "signeeName";
    public final static String COLUMN_DATABASE_VERSION_ID = "Id";
    public final static String COLUMN_DATABASE_VERSION = "database_version";
    public final static String TABLE_DATABASE_VERSION = "xxmsales_database_version";
    //private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "/mnt/sdcard/KSFA/ksfaDB.DB";
    private static final String ALTER_CUSTOMER_DETAILS_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_CUSTOMER_DETAILS ADD COLUMN_NEW_CUSTOMER_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_COLLECTION_DETAILS_TABLE_ADD_COLUMN_CONVERSION =
            "ALTER TABLE TABLE_ITEM_DETAILS ADD COLUMN_CONVERSION TEXT";
    private static final String ALTER_SALESORDER_HEADER_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_SALESORDER_HEADER ADD COLUMN_ORDERS_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_INVOICE_HEADER_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_INVOICE_HEADER ADD COLUMN_INVOICE_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_INVOICE_HEADER_TABLE_ADD_RETURN_QUANTITY =
            "ALTER TABLE TABLE_INVOICE_HEADER ADD COLUMN_INVOICE_RETURN_QUANTITY TEXT";
    private static final String ALTER_CHECK_IN_DETAILS_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_CHECKIN_DETAILS ADD COLUMN_CHECKIN_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_STOCK_RECEIPT_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_STOCK_RECIEPT ADD COLUMN_STOCK_RECEIPT_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_STOCK_RECEIPT_TABLE_ADD_COLUMN_STOCK_RECEIPT_REFERENCE_NUMBER =
            "ALTER TABLE TABLE_STOCK_RECIEPT ADD COLUMN_STOCK_RECEIPT_REFERENCE_NUMBER TEXT";
    private static final String ALTER_STOCK_RECEIPT_TABLE_ADD_COLUMN_STOCK_RECEIPT_TRANSFERRED_TO =
            "ALTER TABLE TABLE_STOCK_RECIEPT ADD COLUMN_STOCK_RECEIPT_TRANSFERRED_TO TEXT";
    private static final String ALTER_COLLECTION_DETAILS_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_COLLECTION_DETAILS ADD COLUMN_CL_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_COLLECTION_DETAILS_TABLE_ADD_TRANSACTION_TYPE =
            "ALTER TABLE TABLE_COLLECTION_DETAILS ADD COLUMN_CL_TRANSACTION_TYPE TEXT";
    private static final String ALTER_PRIMARY_COLLECTION_DETAILS_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_PRIMARY_COLLECTION_DETAILS ADD COLUMN_P_CL_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_FEEDBACK_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_FEEDBACK ADD COLUMN_F_VISIT_SEQUENCE_NO TEXT";
    private static final String ALTER_DOCTOR_VISIT_SPONSORSHIP_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_DOCTOR_VISIT_SPONSORSHIP ADD COLUMN_DV_SPONSORSHIP_SEQUENCE_NO TEXT";
    private static final String ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_TRANSACTION_TYPE =
            "ALTER TABLE TABLE_CUSTOMER_STOCK_PRODUCT ADD COLUMN_CI_TRANSACTION_TYPE TEXT";
    private static final String ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_TRANSACTION_NUMBER =
            "ALTER TABLE TABLE_CUSTOMER_STOCK_PRODUCT ADD COLUMN_CI_TRANSACTION_NUMBER TEXT";
    private static final String ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_TRANSACTION_DATE =
            "ALTER TABLE TABLE_CUSTOMER_STOCK_PRODUCT ADD COLUMN_CI_TRANSACTION_DATE TEXT";
    private static final String ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_CUSTOMER_STOCK_PRODUCT ADD COLUMN_CI_SEQUENCE_NO TEXT";
    private static final String ALTER_DOCTOR_PRESCRIBED_PRODUCTS_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_DOCTOR_PRESCRIBED_PRODUCTS ADD COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_SEQUENCE_NO TEXT";
    private static final String ALTER_LPO_IMAGES_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_LPO_IMAGES ADD COLUMN_LPO_IMAGE_SEQUENCE_NO TEXT";
    private static final String ALTER_NOTES_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_NOTES ADD COLUMN_NOTES_SEQUENCE_NO TEXT";
    private static final String ALTER_PHARMACY_INFO_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_PHARMACY_INFO ADD COLUMN_PHARMACY_INFO_SEQUENCE_NO TEXT";
    private static final String ALTER_SURVEY_RESPONSE_ANSWER_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_SURVEY_RESPONSE_ANSWER ADD COLUMN_SURVEY_RESPONSE_ANSWER_SEQUENCE_NO TEXT";
    private static final String ALTER_SURVEY_RESPONSE_TABLE_ADD_SEQUENCE_NO =
            "ALTER TABLE TABLE_SURVEY_RESPONSE ADD COLUMN_SURVEY_RESPONSE_SEQUENCE_NO TEXT";
    public Context context;
    DecimalFormat formatter = new DecimalFormat("#,###.00");
    String company_code = "";
    String tab_code = "";
    String sr_code = "";
    Constants constants;
    private SQLiteDatabase sqlWrite;


    //We need to pass database information along to superclass
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {


        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        constants = new Constants();
        company_code = Constants.COMPANY_CODE;
        tab_code = Constants.TAB_CODE;
        sr_code = Constants.SR_CODE;


        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCTOR_VISIT_SPONSORSHIP);
        // Create Feedback Table
        Log.d("Test", "Database Created Cons ");

        String database_version = "CREATE TABLE IF NOT EXISTS " + TABLE_DATABASE_VERSION + "(" +

                COLUMN_DATABASE_VERSION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DATABASE_VERSION + " INTEGER ," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";

        sqlWrite.execSQL(database_version);

        String sr_pricelist = "CREATE TABLE IF NOT EXISTS " + TABLE_SR_PRICELIST + "(" +

                COLUMN_SR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SR_CODE + " INTEGER ," +
                COLUMN_SR_PRICE_ID + " TEXT ," +
                COLUMN_SR_IMAGE_FILE_NAME + " TEXT ," +
                COLUMN_SR_IMAGE_FILE_URL + " TEXT ," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";

        sqlWrite.execSQL(sr_pricelist);

        String dr_sponsorship = "CREATE TABLE IF NOT EXISTS " + TABLE_DOCTOR_VISIT_SPONSORSHIP + "(" +

                COLUMN_DVS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DV_SPONSORSHIP_ID + " INTEGER ," +
                COLUMN_DV_SPONSORSHIP_TYPE + " TEXT ," +
                COLUMN_DV_SPONSORSHIP_EVENT + " TEXT ," +
                COLUMN_DV_SPONSORSHIP_FROM_DATE + " TEXT ," +
                COLUMN_DV_SPONSORSHIP_TO_DATE + " TEXT, " +
                COLUMN_DV_SPONSORSHIP_BUDGET_AMOUNT + " TEXT, " +
                COLUMN_DV_SPONSORSHIP_CUSTOMER_ID + " TEXT, " +
                COLUMN_DV_SPONSORSHIP_RANDOM_NUM + " INTEGER, " +
                COLUMN_DV_SPONSORSHIP_STATUS + " TEXT, " +
                COLUMN_DV_SPONSORSHIP_CURRENCY + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_DV_SPONSORSHIP_SEQUENCE_NO + " TEXT " +


                ");";

        sqlWrite.execSQL(dr_sponsorship);

        String dr_sponsorship_type = "CREATE TABLE IF NOT EXISTS " + TABLE_DR_VISIT_SPONSORSHIP_TYPE + "(" +

                COLUMN_DVS_SPONSORSHIP_TYPE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DVS_SPONSORSHIP_TYPE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";

        sqlWrite.execSQL(dr_sponsorship_type);

        String master_sequence = "CREATE TABLE IF NOT EXISTS " + TABLE_MASTER_SEQUENCE + "(" +

                COLUMN_ORDER_SEQ_VALUE + " INTEGER, " +
                COLUMN_INVOICE_SEQ_VALUE + " INTEGER, " +
                COLUMN_COLLECTION_SEQ_VALUE + " INTEGER, " +
                COLUMN_PRIMARY_COLLECTION_SEQ_VALUE + " INTEGER, " +
                COLUMN_RECEIPT_NUM_SEQ_VALUE + " INTEGER, " +
                COLUMN_TRIP_SEQ_VALUE + " INTEGER, " +
                COLUMN_CUSTOMER_SEQ_VALUE + " INTEGER, " +
                COLUMN_SPONSOR_SEQ_VALUE + " INTEGER, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +


                ");";
        sqlWrite.execSQL(master_sequence);

        String master_download = "CREATE TABLE IF NOT EXISTS " + TABLE_MASTER_DOWNLOAD + "(" +

                COLUMN_MASTER_DOWNLOAD + " TEXT, " +
                COLUMN_MASTER_SYNC_DATE + " TEXT, " +
                COLUMN_MASTER_SYNC_TIME + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(master_download);

        String feedback_type = "CREATE TABLE IF NOT EXISTS " + TABLE_FEEDBACK_TYPE + "(" +

                        COLUMN_FEEDBACK_ID + " TEXT, " +
                        COLUMN_FEEDBACK_TYPE + " TEXT, " +
                        COLUMN_FEEDBACK_TAB_CODE + " TEXT, " +
                        COLUMN_FEEDBACK_BDE_CODE + " TEXT, " +
                        COLUMN_FEEDBACK_CUSTOMER_CODE + " TEXT, " +
                        COLUMN_FEEDBACK_ACTION_TAKEN + " TEXT, " +
                        COLUMN_FEEDBACK_STATUS + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";

        sqlWrite.execSQL(feedback_type);

        String global_message = "CREATE TABLE IF NOT EXISTS " + TABLE_GLOBAL_MESSAGE + "(" +

                COLUMN_GLOBAL_MESSAGE_NAME + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(global_message);


        String gps_tracker = "CREATE TABLE IF NOT EXISTS " + TABLE_GPS_TRACKER + "(" +

                COLUMN_LATITUDE + " TEXT ," +
                COLUMN_LONGITUDE + " TEXT, " +
                COLUMN_GPS_DATE + " TEXT, " +
                COLUMN_GPS_TIME + " TEXT, " +
                COLUMN_GPS_ID + " INTEGER, " +
                COLUMN_GPS_IS_SYNC + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(gps_tracker);

        String journey_plan = "CREATE TABLE IF NOT EXISTS " + TABLE_JOURNEY_PLAN_MASTER + "(" +

                COLUMN_JP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_JP_SEQ_NUM + " TEXT ," +
                COLUMN_JP_CUSTOMER_CODE + " TEXT ," +
                COLUMN_JP_CUSTOMER_NAME + " TEXT ," +
                COLUMN_JP_LOCATION + " TEXT ," +
                COLUMN_JP_CUSTOMER_TYPE + " TEXT, " +
                COLUMN_JP_GPS_LATITUDE + " TEXT, " +
                COLUMN_JP_GPS_LONGITUDE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";

        sqlWrite.execSQL(journey_plan);

        String feedback = "CREATE TABLE IF NOT EXISTS " + TABLE_FEEDBACK + "(" +

                COLUMN_F_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_F_COMPANY_ID + " TEXT ," +
                COLUMN_F_SALES_REP_ID + " TEXT ," +
                COLUMN_F_TAB_ID + " TEXT ," +
                COLUMN_F_CUST_ID + " TEXT ," +
                COLUMN_F_TYPE + " TEXT ," +
                COLUMN_F_DATE + " TEXT ," +
                COLUMN_F_ACTIONTAKEN + " TEXT ," +
                COLUMN_F_FEEDBACK + " TEXT, " +
                COLUMN_FEEDBACK_SYNC + " TEXT, " +
                COLUMN_F_RANDOM_NUM + " INTEGER, " +
                COLUMN_FEEDBACK_CODE + " TEXT, " +
                COLUMN_F_STATUS + " TEXT, " +

                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_F_VISIT_SEQUENCE_NO + " TEXT " +


                ");";

        sqlWrite.execSQL(feedback);

        String action_taken = "CREATE TABLE IF NOT EXISTS " + TABLE_ACTION_TAKEN + "(" +

                COLUMN_ACTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_A_ACTION_DATE + " TEXT ," +
                COLUMN_A_ACTIONTAKEN + " TEXT ," +
                COLUMN_A_FEEDBACK_CODE + " TEXT, " +

                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";

        sqlWrite.execSQL(action_taken);

        // Create Customer Details
        String customer_details = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER_DETAILS + "(" +
                COLUMN_CUID_NEW + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_ID_NEW + " TEXT ," +
                COLUMN_CUSTOMER_NAME_NEW + " TEXT, " +
                COLUMN_SALES_REP_ID_NEW + " INTEGER, " +
                COLUMN_SALES_REP_NAME_NEW + " TEXT, " +
                COLUMN_SERIAL_NUM_NEW + " TEXT ," +
                COLUMN_ADDRESS1_NEW + " TEXT, " +
                COLUMN_ADDRESS2_NEW + " TEXT, " +
                COLUMN_ADDRESS3_NEW + " TEXT, " +
                COLUMN_CITY_NEW + " TEXT, " +
                COLUMN_STATE_NEW + " TEXT, " +
                COLUMN_CUS_MOBILE_NUMBER_NEW + " TEXT, " +
                COLUMN_CUS_CREDIT_LIMIT_NEW + " TEXT, " +
                COLUMN_CUS_CREDIT_DAYS_NEW + " TEXT, " +

                COLUMN_PINCODE_NEW + " TEXT, " +
                COLUMN_SHIP_ADDRESS1_NEW + " TEXT, " +
                COLUMN_SHIP_ADDRESS2_NEW + " TEXT, " +
                COLUMN_SHIP_ADDRESS3_NEW + " TEXT, " +
                COLUMN_SHIP_CITY_NEW + " TEXT, " +
                COLUMN_SHIP_STATE_NEW + " TEXT, " +
                COLUMN_SHIP_COUNTRY_NEW + " TEXT, " +
                COLUMN_CUSTOMER_TYPE + " TEXT, " +
                COLUMN_ACC_NUMBER + " TEXT, " +
                COLUMN_GPS_LATITUDE + " TEXT, " +
                COLUMN_GPS_LONGITUDE + " TEXT, " +
                COLUMN_CREDIT_TERM + " TEXT, " +
                COLUMN_PAYMENT_TERM + " TEXT, " +
                COLUMN_C_PRICE_LIST_NAME + " TEXT, " +
                COLUMN_C_PRICE_LIST_CODE + " TEXT, " +
                COLUMN_C_CREATED_FROM + " TEXT, " +
                COLUMN_C_LOCATION_NAME + " TEXT, " +
                COLUMN_C_LGA + " TEXT, " +
                COLUMN_C_COUNTRY + " TEXT, " +
                COLUMN_C_EMAIL + " TEXT, " +
                COLUMN_C_ADD_INFO + " TEXT, " +
                COLUMN_NEW_CUSTOMER_IS_SYNC + " TEXT, " +
                COLUMN_C_CUSTOMER_IMAGE_NAME + " TEXT, " +
                COLUMN_C_CUSTOMER_IMAGE_URL + " TEXT, " +
                COLUMN_C_CUSTOMER_GROUP + " TEXT, " +
                COLUMN_CU_RANDOM_NUM + " INTEGER, " +
                COLUMN_C_CREATION_DATE + " TEXT, " +
                COLUMN_C_IS_IMAGE_UPLOADED + " INTEGER DEFAULT 0," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_NEW_CUSTOMER_VISIT_SEQUENCE_NO + " TEXT, " +
                COLUMN_NEW_CUSTOMER_DISTRIBUTOR_NAME + " TEXT, " +
                COLUMN_NEW_CUSTOMER_DISTRIBUTOR_CODE + " TEXT " +


                ");";
        sqlWrite.execSQL(customer_details);

        String price_list_header = "CREATE TABLE IF NOT EXISTS " + TABLE_PRICE_LIST_HEADER + "(" +
                COLUMN_P_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PRICE_ID + " INTEGER ," +
                COLUMN_PRICE_LIST_CODE + " TEXT ," +
                COLUMN_PRICE_LIST_NAME + " TEXT ," +
                COLUMN_PRICE_DESCRIPTION + " TEXT ," +
                COLUMN_PRICE_LIST_TYPE + " TEXT ," +
                COLUMN_EFFECTIVE_START_DATE + " TEXT ," +
                COLUMN_EFFECTIVE_END_DATE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(price_list_header);

        String price_list_lines = "CREATE TABLE IF NOT EXISTS " + TABLE_PRICE_LIST_LINES + "(" +
                COLUMN_PL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PL_PRICE_ID + " INTEGER ," +
                COLUMN_PL_PRODUCT_ID + " TEXT ," +
                COLUMN_PL_PRODUCT_CODE + " TEXT ," +
                COLUMN_PL_PRICE + " NUMERIC ," +
                COLUMN_PL_PRICE1 + " NUMERIC ," +
                COLUMN_PL_EFFECTIVE_START_DATE + " TEXT ," +
                COLUMN_PL_EFFECTIVE_END_DATE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(price_list_lines);

        // Create Login Table
        String login = "CREATE TABLE IF NOT EXISTS " + TABLE_LOGIN + "(" +
                COLUMN_lOGIN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_FULL_NAME + " TEXT ," +
                COLUMN_EMAIL + " TEXT, " +
                COLUMN_PHONE_NUMBER + " INTEGER, " +
                COLUMN_LOCATION + " TEXT, " +
                COLUMN_PASSWORD + " TEXT ," +
                COLUMN_CONFIRM_PASSWORD + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(login);

        // Create Company Setup Table
        String company_setup = "CREATE TABLE IF NOT EXISTS " + TABLE_COMPANY_SETUP + "(" +
                COLUMN_CID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_COMPANY_NAME + " TEXT ," +
                COLUMN_SHORT_NAME + " TEXT, " +
                COLUMN_CONTACT_PERSON + " INTEGER, " +
                COLUMN_EMAIL_ID + " TEXT, " +
                COLUMN_MOBILE_NUMBER + " TEXT ," +
                COLUMN_SUPPORT_NUMBER1 + " TEXT, " +
                COLUMN_SUPPORT_NUMBER2 + " TEXT, " +
                COLUMN_SUPPORT_EMAIL + " TEXT, " +
                COLUMN_COMPANY_LOGO + " BLOB NOT NULL, " +
                COLUMN_COMPANY_ADDRESS_LINE1 + " TEXT, " +
                COLUMN_COMPANY_ADDRESS_LINE2 + " TEXT," +
                COLUMN_COMPANY_ADDRESS_LINE3 + " TEXT, " +
                COLUMN_COMPANY_LOGO_IMAGE_NAME + " TEXT," +
                COLUMN_COMPANY_LOGO_IMAGE_URL + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(company_setup);

        // Create User Setup Table
        String user_setup = "CREATE TABLE IF NOT EXISTS " + TABLE_USER_SETUP + "(" +

                COLUMN_UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_FIRST_NAME + " TEXT ," +
                COLUMN_LAST_NAME + " TEXT, " +
                COLUMN_USER_EMAIL_ID + " INTEGER, " +
                COLUMN_USER_PASSWORD + " TEXT, " +
                COLUMN_USER_CONFIRM_PASSWORD + " TEXT ," +
                COLUMN_USER_MOBILE_NUMBER + " TEXT, " +
                COLUMN_EMPLOYEE_ID + " TEXT, " +
                COLUMN_ROLE + " TEXT, " +
                COLUMN_USER_IMAGE + " BLOB, " +
                COLUMN_USER_IMAGE_NAME + " TEXT, " +
                COLUMN_USER_IMAGE_URL + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(user_setup);

        // Create Tab Setup Table
        String tab_setup = "CREATE TABLE IF NOT EXISTS " + TABLE_TAB_SETUP + "(" +
                COLUMN_TID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_TAB_ID + " TEXT ," +
                COLUMN_USER_ID + " TEXT, " +
                COLUMN_OTP + " TEXT, " +
                COLUMN_TAB_PREFIX + " TEXT, " +
                COLUMN_BASE_URL + " TEXT, " +
                COLUMN_SEC_PRICE_LIST + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_ACTIVATE_FLAG  + " INTEGER DEFAULT 1," +
                COLUMN_IMEINO + " TEXT " +
                ");";
        sqlWrite.execSQL(tab_setup);

        // Create Salesrep Details
        String salesrep_details = "CREATE TABLE IF NOT EXISTS " + TABLE_SALESREP_DETAILS + "(" +
                COLUMN_CID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SALES_REP_ID + " INTEGER, " +
                COLUMN_SALES_REP_NAME + " TEXT, " +
                COLUMN_MANAGER + " TEXT ," +
                COLUMN_REGION + " TEXT, " +
                COLUMN_WAREHOUSE + " TEXT, " +
                COLUMN_GPS + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(salesrep_details);

        // Create Sales Order Header
        String order_header = "CREATE TABLE IF NOT EXISTS " + TABLE_SALESORDER_HEADER + "(" +
                COLUMN_SOHID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_O_SALES_REP_ID + " INTEGER, " +
                COLUMN_O_CUSTOMER_ID + " TEXT, " +
                COLUMN_SALESORDER_ID + " INTEGER ," +
                COLUMN_SALESORDER_NUMBER + " TEXT, " +
                COLUMN_SALESORDER_DATE + " TEXT," +
                COLUMN_SALESORDER_STATUS + " TEXT ," +
                COLUMN_SALESORDER_VALUE + " NUMERIC, " +
                COLUMN_SALESORDER_LINES + " TEXT," +
                COLUMN_TEMPLATE_NAME + " TEXT," +
                COLUMN_TEMPLATE_ID + " INTEGER," +
                COLUMN_SIGNATURE + " TEXT," +
                COLUMN_DELIVERY_DATE + " TEXT," +
                COLUMN_LPO_NUMBER + " TEXT," +
                COLUMN_REMARKS + " TEXT," +
                COLUMN_C_SHIP_ADDRESS1 + " TEXT," +
                COLUMN_C_SHIP_ADDRESS2 + " TEXT," +
                COLUMN_C_RANDOM_NUM + " INTEGER," +
                COLUMN_ORDERS_IS_SYNC + " TEXT, " +
                COLUMN_ORDERS_EMAIL_SENT + " TEXT, " +
                COLUMN_ORDERS_SMS_SENT + " TEXT, " +
                COLUMN_ORDERS_PRINT_DONE + " TEXT, " +

                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_ORDERS_VISIT_SEQUENCE_NO + " TEXT, " +
                COLUMN_SALESORDER_TIME + " TEXT," +
                COLUMN_ORDERS_SIGNATURE_NAME + " TEXT," +
                COLUMN_ORDERS_IS_IMAGE_UPLOADED + " INTEGER DEFAULT 0, " +
                COLUMN_ORDERS_SIGNATURE_PATH + " TEXT," +
                COLUMN_ORDERS_SIGNATURE_IMAGE_NAME + " TEXT" +


                ");";
        sqlWrite.execSQL(order_header);

        // Create Item Details
        String item_detail = "CREATE TABLE IF NOT EXISTS " + TABLE_ITEM_DETAILS + "(" +
                COLUMN_IUID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PRODUCT_ID + " INTEGER, " +
                COLUMN_PRODUCT_NAME + " TEXT, " +
                COLUMN_PRODUCT_DESC + " TEXT ," +
                COLUMN_PRODUCT_UOM + " TEXT, " +
                COLUMN_PRICE + " TEXT," +
                COLUMN_PRODUCT_TYPE + " TEXT," +
                COLUMN_STOCK_UOM + " TEXT, " +
                COLUMN_BATCH_CONTROLLED + " TEXT, " +
                COLUMN_ORDER_ITEMS_IS_SYNC + " TEXT, " +
                COLUMN_OD_ONHAND_STOCK + " INTEGER, " +
                COLUMN_VAT + " NUMERIC, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CONVERSION + " TEXT " +

                ");";
        sqlWrite.execSQL(item_detail);


        // Create Locations Header
        String location_header = "CREATE TABLE IF NOT EXISTS " + TABLE_LOCATION_HEADER + "(" +
                COLUMN_L_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_LOCATION_ID + " TEXT, " +
                COLUMN_LOCATION_NAME + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(location_header);

        // Create Locations Lines
        String location_lines = "CREATE TABLE IF NOT EXISTS " + TABLE_LOCATION_LINES + "(" +
                COLUMN_LI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_L_LOCATION_ID + " TEXT, " +
                COLUMN_L_LOCATION_NAME + " TEXT, " +
                COLUMN_L_LGA_ID + " TEXT ," +
                COLUMN_L_LGA + " TEXT ," +
                COLUMN_L_CITY_ID + " TEXT, " +
                COLUMN_L_CITY + " TEXT, " +
                COLUMN_L_STATE_ID + " TEXT," +
                COLUMN_L_STATE + " TEXT," +
                COLUMN_L_COUNTRY_ID + " TEXT," +
                COLUMN_L_COUNTRY + " TEXT," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(location_lines);


        String srbatchdetails = "CREATE TABLE IF NOT EXISTS  " + TABLE_SR_INVOICE_BATCH + "(" +
                COLUMN_SR_BATCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SR_BATCH_PRODUCT_ID + " TEXT ," +
                COLUMN_SR_BATCH_PRODUCT_NAME + " TEXT ," +
                COLUMN_SR_BATCH_EXPIRY_DATE + " TEXT ," +
                COLUMN_SR_BATCH_BATCH_NUMBER + " TEXT ," +
                COLUMN_SR_BATCH_BATCH_QUANTITY + " INTEGER ," +
                COLUMN_SR_BATCH_INVOICE_ID + " TEXT ," +
                COLUMN_SR_BATCH_BATCH_STATUS  + " TEXT " +


                ");";
        sqlWrite.execSQL(srbatchdetails);
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_COLLECTION_DETAILS);


        // Create Collection Details
        String collection_details = "CREATE TABLE IF NOT EXISTS " + TABLE_COLLECTION_DETAILS + "(" +

                COLUMN_CL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CL_SALES_REP_ID + " INTEGER, " +
                COLUMN_CL_CUSTOMER_ID + " TEXT, " +
                COLUMN_CL_COLLECTION_ID + " INTEGER ," +
                COLUMN_CL_COLLECTION_NUMBER + " TEXT, " +
                COLUMN_CL_COLLECTION_DATE + " TEXT," +
                COLUMN_CL_COLLECTION_TIME + " TEXT," +
                COLUMN_CL_INVOICE_AMOUNT + " NUMERIC ," +
                COLUMN_CL_PAID_AMOUNT + " NUMERIC, " +
                COLUMN_CL_BALANCE_AMOUNT + " NUMERIC," +
                COLUMN_CL_COLLECTION_TYPE + " TEXT," +
                COLUMN_CL_PAYMENT_AMOUNT + " NUMERIC," +
                COLUMN_CL_SIGN + " BLOB," +
                COLUMN_CL_CHECK_NUM + " TEXT," +
                COLUMN_CL_CHECK_DATE + " TEXT," +
                COLUMN_CL_BANK_BRANCH + " TEXT," +
                COLUMN_CL_INVOICE_ID + " INTEGER," +
                COLUMN_CL_INVOICE_NUM + " TEXT," +
                COLUMN_CL_PAYMENT_MODE + " TEXT," +
                COLUMN_CL_IS_SYNC + " TEXT," +
                COLUMN_CL_RANDOM_NUM + " INTEGER," +
                COLUMN_CL_T_ACC_NUM + " TEXT," +
                COLUMN_CL_T_BANK_BRANCH + " TEXT," +
                COLUMN_CL_T_TELLER_NUMBER + " TEXT," +
                COLUMN_CL_T_TRANSFER_DATE + " TEXT," +
                COLUMN_CL_EMAIL_SENT + " TEXT, " +
                COLUMN_CL_SMS_SENT + " TEXT, " +
                COLUMN_CL_INVOICE_DATE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CL_VISIT_SEQUENCE_NO + " TEXT, " +
                COLUMN_CL_TRANSACTION_TYPE + " TEXT, " +
                COLUMN_CL_SIGNEE_NAME + " TEXT, " +
                COLUMN_CL_STATUS + " TEXT, " +
                COLUMN_CL_IS_IMAGE_UPLOADED + " INTEGER DEFAULT 0, " +
                COLUMN_CL_SIGNATURE_PATH + " TEXT, " +
                COLUMN_CL_SIGNATURE_IMAGE_NAME + " TEXT " +


                ");";
        sqlWrite.execSQL(collection_details);

        // Create Collection Details
        String primary_collection_details = "CREATE TABLE IF NOT EXISTS  " + TABLE_PRIMARY_COLLECTION_DETAILS + "(" +

                COLUMN_P_CL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_P_CL_SALES_REP_ID + " INTEGER, " +
                COLUMN_P_CL_CUSTOMER_ID + " TEXT, " +
                COLUMN_P_CL_COLLECTION_ID + " INTEGER ," +
                COLUMN_P_CL_COLLECTION_NUMBER + " TEXT, " +
                COLUMN_P_CL_COLLECTION_DATE + " TEXT," +
                COLUMN_P_CL_INVOICE_AMOUNT + " NUMERIC ," +
                COLUMN_P_CL_PAID_AMOUNT + " NUMERIC, " +
                COLUMN_P_CL_BALANCE_AMOUNT + " NUMERIC," +
                COLUMN_P_CL_COLLECTION_TYPE + " TEXT," +
                COLUMN_P_CL_PAYMENT_AMOUNT + " NUMERIC," +
                COLUMN_P_CL_SIGN + " BLOB," +
                COLUMN_P_CL_CHECK_NUM + " TEXT," +
                COLUMN_P_CL_CHECK_DATE + " TEXT," +
                COLUMN_P_CL_BANK_BRANCH + " TEXT," +
                COLUMN_P_CL_INVOICE_ID + " INTEGER," +
                COLUMN_P_CL_INVOICE_NUM + " TEXT," +
                COLUMN_P_CL_PAYMENT_MODE + " TEXT," +
                COLUMN_P_CL_IS_SYNC + " TEXT," +
                COLUMN_P_CL_RANDOM_NUM + " INTEGER," +
                COLUMN_P_CL_T_ACC_NUM + " TEXT," +
                COLUMN_P_CL_T_BANK_BRANCH + " TEXT," +
                COLUMN_P_CL_T_TELLER_NUMBER + " TEXT," +
                COLUMN_P_CL_T_TRANSFER_DATE + " TEXT," +
                COLUMN_P_CL_ORDER_NUMBER + " TEXT," +
                COLUMN_P_CL_EMAIL_SENT + " TEXT, " +
                COLUMN_P_CL_SMS_SENT + " TEXT, " +
                COLUMN_P_CL_PRINT_DONE + " TEXT, " +
                COLUMN_P_CL_INVOICE_DATE + " TEXT, " +

                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_P_CL_VISIT_SEQUENCE_NO + " TEXT, " +
                COLUMN_P_CL_SIGNEE_NAME + " TEXT, " +
                COLUMN_P_CL_STATUS + " TEXT, " +
                COLUMN_P_CL_IS_IMAGE_UPLOADED + " INTEGER DEFAULT 0, " +
                COLUMN_P_CL_SIGNATURE_PATH + " TEXT, " +
                COLUMN_P_CL_SIGNATURE_IMAGE_NAME + " TEXT " +

                ");";
        sqlWrite.execSQL(primary_collection_details);

        //Create Trip Details
        String trip_details = "CREATE TABLE IF NOT EXISTS " + TABLE_TRIP_DETAILS + "(" +
                COLUMN_TRIP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_TRIP_NUMBER + " INTEGER, " +
                COLUMN_TRIP_DATE + " TEXT, " +
                COLUMN_TRIP_TIME + " TEXT ," +
                COLUMN_STARTING_KM + " INTEGER, " +
                COLUMN_ODOMETER_IMAGE + " TEXT," +
                COLUMN_TRIP_FILE_NAME + " TEXT," +
                COLUMN_TRIP_NAME + " TEXT, " +
                COLUMN_TR_RANDOM_NUM + " INTEGER, " +
                COLUMN_TRIP_STOP_DATE + " TEXT, " +
                COLUMN_TRIP_IS_SYNC + " TEXT, " +
                COLUMN_TRIP_STOP_TIME + " TEXT, " +
                COLUMN_TRIP_IS_IMAGE_UPLOADED + " INTEGER DEFAULT 0, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(trip_details);

        // Create Sales Order Details
        String order_details = "CREATE TABLE IF NOT EXISTS " + TABLE_SALESORDER_DETAILS + "(" +
                COLUMN_SODID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_O_SALES_REP_ID + " INTEGER, " +
                COLUMN_O_CUSTOMER_ID + " TEXT, " +
                COLUMN_SALESORDER_ID + " INTEGER ," +
                COLUMN_SALESORDER_NUMBER + " TEXT, " +
                COLUMN_SALESORDER_DATE + " TEXT,   " +
                COLUMN_SALESORDER_TIME + " TEXT,   " +
                COLUMN_SALESORDER_STATUS + " TEXT ," +
                COLUMN_SALESORDER_VALUE + " TEXT, " +
                COLUMN_SALESORDER_LINES + " TEXT," +
                COLUMN_ODSALESORDER_LINE_ID + " INTEGER," +
                COLUMN_OD_PRODUCT_ID + " INTEGER,   " +
                COLUMN_OD_PRODUCT_NAME + " TEXT ," +
                COLUMN_OD_PRODUCT_UOM + " TEXT, " +
                COLUMN_OD_QUANTITY + " INTEGER," +
                COLUMN_OD_PRICE + " NUMERIC," +
                COLUMN_OD_VALUE + " NUMERIC," +
                COLUMN_ORDER_ITEMS_IS_SYNC + " TEXT," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(order_details);

        // Create Invoice Header
        String invoice_header = "CREATE TABLE IF NOT EXISTS " + TABLE_INVOICE_HEADER + "(" +
                COLUMN_IID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_I_SALES_REP_ID + " INTEGER, " +
                COLUMN_I_CUSTOMER_ID + " TEXT, " +
                COLUMN_INVOICE_ID + " INTEGER ," +
                COLUMN_INVOICE_NUMBER + " TEXT, " +
                COLUMN_INVOICE_DATE + " TEXT," +
                COLUMN_INVOICE_TIME + " TEXT," +
                COLUMN_INVOICE_STATUS + " TEXT ," +
                COLUMN_INVOICE_VALUE + " TEXT, " +
                COLUMN_INVOICE_LINES + " TEXT," +
                COLUMN_ID_PAID_AMOUNT + " NUMERIC," +
                COLUMN_ID_BALANCE_AMOUNT + " NUMERIC," +
                COLUMN_ID_CHANGE_TYPE + " TEXT," +
                COLUMN_INVOICE_SIGNATURE + " TEXT," +
                COLUMN_INVOICES_IS_SYNC + " TEXT," +
                COLUMN_I_RANDOM_NUM + " INTEGER," +
                COLUMN_INVOICE_EMAIL_SENT + " TEXT, " +
                COLUMN_INVOICE_SMS_SENT + " TEXT, " +
                COLUMN_INVOICE_PRINT_DONE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_INVOICE_VISIT_SEQUENCE_NO + " TEXT, " +
                COLUMN_INVOICE_RETURN_QUANTITY + " INTEGER, " +
                COLUMN_INVOICE_SIGNEE_NAME + " TEXT, " +
                COLUMN_INVOICE_IS_IMAGE_UPLOADED + " INTEGER DEFAULT 0, " +
                COLUMN_INVOICE_SIGNATURE_PATH + " TEXT, " +
                COLUMN_INVOICE_SIGNATURE_IMAGE_NAME + " TEXT " +


                ");";
        sqlWrite.execSQL(invoice_header);


        // Create Invoice Details
        String invoice_details = "CREATE TABLE IF NOT EXISTS " + TABLE_INVOICE_DETAILS + "(" +
                COLUMN_IDID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ID_SALES_REP_ID + " INTEGER, " +
                COLUMN_ID_CUSTOMER_ID + " TEXT, " +
                COLUMN_ID_INVOICE_ID + " INTEGER ," +
                COLUMN_INVOICE_NUMBER + " TEXT, " +
                COLUMN_ID_INVOICE_DATE + " TEXT,   " +
                COLUMN_ID_INVOICE_STATUS + " TEXT ," +
                COLUMN_ID_INVOICE_VALUE + " TEXT, " +
                COLUMN_ID_INVOICE_LINES + " TEXT," +
                COLUMN_ID_INVOICE_LINE_ID + " INTEGER," +
                COLUMN_ID_PRODUCT_ID + " INTEGER,   " +
                COLUMN_ID_PRODUCT_NAME + " TEXT ," +
                COLUMN_ID_PRODUCT_UOM + " TEXT, " +
                COLUMN_ID_QUANTITY + " TEXT," +
                COLUMN_ID_PRICE + " TEXT," +
                COLUMN_ID_VALUE + " TEXT," +
                COLUMN_ID_DISCOUNT + " TEXT," +
                COLUMN_ID_VAT + " TEXT," +
                COLUMN_ID_VALUE_WITHOUT_DIS + " NUMERIC," +
                COLUMN_ID_VALUE_WITHOUT_VAT + " NUMERIC," +
                COLUMN_ID_VALUE_WITH_VAT + " NUMERIC," +
                COLUMN_ID_DISCOUNT_VALE + " NUMERIC," +
                COLUMN_INVOICE_ITEMS_IS_SYNC + " TEXT," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +


                ");";
        sqlWrite.execSQL(invoice_details);

        // Create CheckIN Details
        String checkin_deatils = "CREATE TABLE IF NOT EXISTS " + TABLE_CHECKIN_DETAILS + "(" +
                COLUMN_CHECK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_C_TRIP_NUMBER + " INTEGER, " +
                COLUMN_C_CUSTOMER_ID + " TEXT, " +
                COLUMN_CHECKIN_DATE + " TEXT ," +
                COLUMN_CHECKIN_TIME + " TEXT, " +
                COLUMN_CHECKIN_GPS_LAT + " TEXT,   " +
                COLUMN_CHECKIN_GPS_LONG + " TEXT,   " +
                COLUMN_CHECKOUT_DATE + " TEXT ," +
                COLUMN_CHECKOUT_TIME + " TEXT, " +
                COLUMN_CHECKOUT_GPS_LAT + " TEXT," +
                COLUMN_CHECKOUT_GPS_LONG + " TEXT," +
                COLUMN_CHECKOUT_DURATION + " TEXT," +
                COLUMN_CHECKIN_SR + " TEXT," +
                COLUMN_CHECKIN_IS_SYNC + " TEXT," +
                COLUMN_CHECKIN_CUST_TYPE + " TEXT," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CHECKIN_VISIT_SEQUENCE_NO + " TEXT " +


                ");";
        sqlWrite.execSQL(checkin_deatils);

       /* // Create Warehouse Details Table
        String warehousedetails = "CREATE TABLE IF NOT EXISTS  " + TABLE_WAREHOUSE + "(" +
                COLUMN_WAREHOUSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_W_WAREHOUSE_ID + " TEXT ," +
                COLUMN_W_WAREHOUSE_NAME + " TEXT ," +
                COLUMN_W_SR_CODE + " TEXT " +
                ");";
        sqlWrite.execSQL(warehousedetails);
*/

        String batchdetails = "CREATE TABLE IF NOT EXISTS  " + TABLE_PRODUCT_BATCH + "(" +
                COLUMN_PRODUCTBATCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_B_PRODUCT_ID + " TEXT ," +
                COLUMN_B_BATCH_NUMBER + " TEXT ," +
                COLUMN_B_EXPIRY_DATE + " TEXT ," +
                COLUMN_B_BATCH_QUANTITY + " INTEGER ," +
                COLUMN_B_BATCH_INVOICE + " INTEGER ," +
                COLUMN_B_RECEIPT_NO + " TEXT ," +
                COLUMN_B_INVOICE_ID + " TEXT ," +
                COLUMN_B_TEMP_NUMBER + " TEXT, " +
                COLUMN_B_STATUS + " TEXT, " +
                COLUMN_BATCH_IS_SYNC + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_BATCH_TRANSACTION_TYPE + " TEXT " +


                ");";
        sqlWrite.execSQL(batchdetails);

        // Create Batch Details Table
        String batchdetails_temp = "CREATE TABLE IF NOT EXISTS  " + TABLE_PRODUCT_BATCH_TEMP + "(" +
                COLUMN_PT_PRODUCTBATCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PT_PRODUCT_ID + " TEXT ," +
                COLUMN_PT_BATCH_NUMBER + " TEXT ," +
                COLUMN_PT_EXPIRY_DATE + " TEXT ," +
                COLUMN_PT_BATCH_QUANTITY + " TEXT ," +
                COLUMN_PT_BATCH_INVOICE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(batchdetails_temp);

        // Create Stock Reciept
        String stockreciept = "CREATE TABLE IF NOT EXISTS  " + TABLE_STOCK_RECIEPT + "(" +
                COLUMN_STOCK_RECIEPT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_STOCK_RECIEPT_DATE + " TEXT ," +
                COLUMN_STOCK_RECIEPT_CURRENCY + " TEXT ," +
                COLUMN_STOCK_RECIEPT_SR_NAME + " TEXT ," +
                COLUMN_SR_SOURCE_NAME + " TEXT ," +
                COLUMN_SR_INVOICE + " TEXT ," +
                COLUMN_SR_SOURCE_TYPE + " TEXT ," +
                COLUMN_SR_PRODUCTNAME + " TEXT ," +
                COLUMN_SR_PRODUCT_UOM + " TEXT ," +
                COLUMN_SR_PRODUCT_QUANTITY + " TEXT ," +
                COLUMN_SR_PRODUCT_PRICE + " TEXT ," +
                COLUMN_SR_PRODUCT_VALUE + " TEXT ," +
                COLUMN_SR_REMARKS + " TEXT ," +
                COLUMN_STOCK_RECIEPT_NUMBER + " TEXT, " +
                COLUMN_STOCK_INVOICE_ID + " TEXT, " +
                COLUMN_STOCK_STATUS + " TEXT, " +
                COLUMN_STOCK_RECEIPT_IS_SYNC + " TEXT," +
                COLUMN_STOCK_RANDOM_NUM + " INTEGER," +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_STOCK_RECEIPT_VISIT_SEQUENCE_NO + " TEXT " +


                ");";
        sqlWrite.execSQL(stockreciept);

        // Create Stock Reciept Temp
        String stockreciepttemp = "CREATE TABLE IF NOT EXISTS  " + TABLE_RECEIPT_ROW_TEMP + "(" +
                COLUMN_R_TEMP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_R_PRODUCT_NAME + " TEXT ," +
                COLUMN_R_PRODUCT_UOM + " TEXT ," +
                COLUMN_R_PRODUCT_QTY + " TEXT ," +
                COLUMN_R_PRODUCT_PRICE + " TEXT ," +
                COLUMN_R_PRODUCT_VALUE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(stockreciepttemp);


        // Create Customer Image Gallery Table
        String customerimagegallerydetails = "CREATE TABLE IF NOT EXISTS  " + TABLE_CUSTOMER_IMAGE_GALLERY + "(" +
                COLUMN_CUSTOMER_GALLERY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_GALLERY_IMAGE_ID + " INTEGER ," +
                COLUMN_CUSTOMER_GALLERY_IMAGE_NAME + " TEXT ," +
                COLUMN_CUSTOMER_GALLERY_IMAGE_ + " TEXT ," +
                COLUMN_CUSTOMER_GALLERY_CUSTOMERID + " TEXT ," +
                COLUMN_CUSTOMER_GALLERY_IMAGE_DATE + " TEXT ," +
                COLUMN_CUSTOMER_GALLERY_IMAGE_GPS + " TEXT, " +
                COLUMN_CUSTOMER_GALLERY_IMAGE_IS_UPLOADED + " INT DEFAULT 0, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(customerimagegallerydetails);

        // Create LPO Gallery Table
        String lpogallerydetails = "CREATE TABLE IF NOT EXISTS  " + TABLE_LPO_GALLERY + "(" +
                COLUMN_LPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_LPO_IMAGE_NAME + " TEXT ," +
                COLUMN_LPO_IMAGE + " BLOB ," +
                COLUMN_LPO_IMAGE_ID + " INTEGER ," +
                COLUMN_LPO_ORDER_ID + " INTEGER, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(lpogallerydetails);


        String metrics_target = "CREATE TABLE IF NOT EXISTS  " + TABLE_METRICS_TARGET + "(" +
                COLUMN_MT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_MT_SALES_REP_ID + " TEXT ," +
                COLUMN_MT_TARGET_ID + " TEXT ," +
                COLUMN_MT_TARGET_CODE + " TEXT ," +
                COLUMN_MT_TARGET_TYPE + " INTEGER ," +
                COLUMN_MT_TARGET_VALUE + " TEXT, " +
                COLUMN_MT_TYPE + " TEXT, " +
                COLUMN_MT_PRODUCT + " TEXT, " +
                COLUMN_MT_VALUE + " TEXT, " +
                COLUMN_MT_FROM_DATE + " TEXT, " +
                COLUMN_MT_TO_DATE + " TEXT, " +
                COLUMN_MT_CUST_TYPE + " TEXT, " +
                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(metrics_target);

        // Create Quarantine Products Table
        String quarantineProducts = "CREATE TABLE IF NOT EXISTS  " + TABLE_QUARANTINE_PRODUCTS + "(" +
                COLUMN_Q_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_Q_PRODUCT_CODE + " TEXT ," +
                COLUMN_Q_BATCH_NUMBER + " TEXT ," +
                COLUMN_Q_BATCH_EXPIRY_DATE + " TEXT ," +
                COLUMN_Q_BATCH_QUANTITY + " TEXT ," +
                COLUMN_Q_QUARANTINE_QUANTITY + " TEXT, " +
                COLUMN_Q_QUARANTINE_DATE + " TEXT, " +

                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +

                ");";
        sqlWrite.execSQL(quarantineProducts);

        // Create Sales Order Details
        String waybill_details = "CREATE TABLE IF NOT EXISTS " + TABLE_WAYBILL_DETAILS + "(" +
                COLUMN_DC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DC_SALES_REP_ID + " INTEGER, " +
                COLUMN_DC_CUSTOMER_ID + " TEXT, " +
                COLUMN_DC_SALESORDER_ID + " INTEGER ," +
                COLUMN_DC_SALESORDER_NUMBER + " TEXT, " +
                COLUMN_DC_SALESORDER_DATE + " TEXT,   " +
                COLUMN_DC_SALESORDER_STATUS + " TEXT ," +
                COLUMN_DC_SALESORDER_VALUE + " TEXT, " +
                COLUMN_DC_SALESORDER_LINES + " TEXT," +
                COLUMN_DC_SALESORDER_LINE_ID + " INTEGER," +
                COLUMN_DC_PRODUCT_ID + " INTEGER,   " +
                COLUMN_DC_PRODUCT_NAME + " TEXT ," +
                COLUMN_DC_PRODUCT_UOM + " TEXT, " +
                COLUMN_DC_QUANTITY + " INTEGER," +
                COLUMN_DC_PRICE + " NUMERIC," +
                COLUMN_DC_VALUE + " NUMERIC," +
                COLUMN_DC_IS_SYNC + " TEXT," +
                COLUMN_DC_WAYBILL_NUMBER + " TEXT," +
                COLUMN_DC_WAYBILL_DATE + " TEXT," +
                COLUMN_DC_WAYBILL_QUANTITY + " TEXT," +
                COLUMN_DC_WAYBILL_STATUS + " TEXT," +
                COLUMN_DC_RANDOM_NUMBER + " INTEGER," +

                COLUMN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_WAYBILL_SIGNEE_NAME + " TEXT" +


                ");";
        sqlWrite.execSQL(waybill_details);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("Test", "Old Version " + oldVersion + "New Version" + newVersion);


        if (newVersion > oldVersion) {

            try {
                db.execSQL("ALTER TABLE xxmsales_checkin_details ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_order_header ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_order_header ADD order_time TEXT");
                db.execSQL("ALTER TABLE xxmsales_invoice_header ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_collection_details ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_primary_collection_details ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_dr_visit_sponsorship ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_stock_reciept ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_customer_details ADD visitSeqNo TEXT");
                db.execSQL("ALTER TABLE xxmsales_feedback ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }

            try {
                db.execSQL("ALTER TABLE xxmsales_doctor_prescribed_products ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }

            try {
                db.execSQL("ALTER TABLE xxmsales_lpo_images ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }

            try {
                db.execSQL("ALTER TABLE xxmsales_notes ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }

            try {
                db.execSQL("ALTER TABLE xxmsales_pharmacy_info ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }

            try {
                db.execSQL("ALTER TABLE xxmsales_survey_response_answer ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }

            try {
                db.execSQL("ALTER TABLE xxmsales_survey_response ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }


            try {
                db.execSQL("ALTER TABLE xxmsales_customer_stock_product ADD visitSeqNo TEXT");


            } catch (Exception e) {

            }


            //db.execSQL("ALTER TABLE xxmsales_waybill_details ADD COLUMN new_column2 INTEGER ");

            /*db.execSQL(ALTER_CHECK_IN_DETAILS_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_SALESORDER_HEADER_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_INVOICE_HEADER_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_COLLECTION_DETAILS_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_PRIMARY_COLLECTION_DETAILS_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_DOCTOR_VISIT_SPONSORSHIP_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_STOCK_RECEIPT_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_CUSTOMER_DETAILS_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_FEEDBACK_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_TRANSACTION_TYPE);
            db.execSQL(ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_TRANSACTION_NUMBER);
            db.execSQL(ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_TRANSACTION_DATE);
            db.execSQL(ALTER_CUSTOMER_STOCK_PRODUCT_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_DOCTOR_PRESCRIBED_PRODUCTS_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_LPO_IMAGES_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_NOTES_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_PHARMACY_INFO_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_SURVEY_RESPONSE_ANSWER_TABLE_ADD_SEQUENCE_NO);
            db.execSQL(ALTER_SURVEY_RESPONSE_TABLE_ADD_SEQUENCE_NO); */

           /* db.execSQL("ALTER TABLE xxmsales_checkin_details ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_order_header ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_order_header ADD order_time TEXT");
            db.execSQL("ALTER TABLE xxmsales_invoice_header ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_collection_details ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_primary_collection_details ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_dr_visit_sponsorship ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_stock_reciept ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_customer_details ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_feedback ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_customer_stock_product ADD transaction_type TEXT");
            db.execSQL("ALTER TABLE xxmsales_customer_stock_product ADD transaction_number TEXT");
            db.execSQL("ALTER TABLE xxmsales_customer_stock_product ADD transaction_date TEXT");
            db.execSQL("ALTER TABLE xxmsales_customer_stock_product ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_doctor_prescribed_products ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_lpo_images ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_notes ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_pharmacy_info ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_survey_response_answer ADD visitSeqNo TEXT");
            db.execSQL("ALTER TABLE xxmsales_survey_response ADD visitSeqNo TEXT"); */


        }

        onCreate(db);
    }


    public long get_login_details(String email, String password) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("select * from " + TABLE_LOGIN + " where " + COLUMN_EMAIL + "='" + email + "' and " + COLUMN_CONFIRM_PASSWORD + "='" + password + "'", null);
        c.moveToFirst();
        if (c.getCount() > 0) {
            int login_status = 1;
            return login_status;

        } else {
            int login_status = 0;
            return login_status;
        }

    }


    //Get Product Batch Details
    public Cursor getProductBatchDetails(String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select b_product_id,b_batch_number,b_expiry_date,sum(b_batch_quantity),null b_batch_invoice from xxmsales_product_batch where b_product_id = '" + product_id + "'" + " and b_batch_status='Completed'" + " group by b_product_id,b_batch_number"
                , null);
        return data;
    }

    //Get Product Batch Details
    public Cursor getProductBatchDrillData(String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select b_expiry_date,b_batch_number,sum(b_batch_quantity),null b_batch_invoice from xxmsales_product_batch where b_product_id = '" + product_id + "'" + " and b_batch_status='Completed'" + " group by b_product_id,b_batch_number"
                , null);
        return data;
    }

    //Get Product Batch Details
    public Cursor getProductBatchDetails1(String product_id, String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select b.b_batch_number,b.b_expiry_date,(select sum(a.b_batch_quantity) quantity from xxmsales_product_batch a where 1=1  and a.b_product_id=b.b_product_id and a.b_batch_number=b.b_batch_number and a.b_batch_status='Completed' group by a.b_product_id,a.b_batch_number ) quantity,b.b_batch_invoice from xxmsales_product_batch b where b.b_expiry_date > date('now') and b.b_product_id = '" + product_id + "'" + " and b.b_invoice_id=" + invoice_id + " and b.b_batch_status='Inprocess' and (select sum(a.b_batch_quantity) quantity from xxmsales_product_batch a where 1=1  and a.b_product_id=b.b_product_id and a.b_batch_number=b.b_batch_number and a.b_batch_status='Completed' group by a.b_product_id,a.b_batch_number ) <> 0 order by b_expiry_date"
                , null);
        return data;
    }

    //Get Product Batch Details
    public Cursor getProductBatchDetails1_waybill(String product_id, String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select b.b_batch_number,b.b_expiry_date,(select sum(a.b_batch_quantity) quantity from xxmsales_product_batch a where 1=1  and a.b_product_id=b.b_product_id and a.b_batch_number=b.b_batch_number and a.b_batch_status='Completed' group by a.b_product_id,a.b_batch_number ) quantity,b.b_batch_invoice from xxmsales_product_batch b where b.b_expiry_date > date('now') and b.b_product_id = '" + product_id + "'" + " and b.b_invoice_id='" + invoice_id + "'" + " and b.b_batch_status='Inprocess' and (select sum(a.b_batch_quantity) quantity from xxmsales_product_batch a where 1=1  and a.b_product_id=b.b_product_id and a.b_batch_number=b.b_batch_number and a.b_batch_status='Completed' group by a.b_product_id,a.b_batch_number ) <> 0 order by b_expiry_date"
                , null);
        return data;
    }

    //Get Product Batch Details
    public Cursor getProductBatchDetails2(String product_id, String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select b.b_batch_number,b.b_expiry_date,(select sum(a.b_batch_quantity) quantity from xxmsales_product_batch a where 1=1  and a.b_product_id=b.b_product_id and a.b_batch_number=b.b_batch_number and a.b_batch_status='Completed' group by a.b_product_id,a.b_batch_number ) quantity,b.b_batch_invoice from xxmsales_product_batch b where b.b_expiry_date > date('now') and b.b_product_id = '" + product_id + "'" + " and b.b_invoice_id='" + invoice_id + "'" + " and b.b_batch_status='Draft' and (select sum(a.b_batch_quantity) quantity from xxmsales_product_batch a where 1=1  and a.b_product_id=b.b_product_id and a.b_batch_number=b.b_batch_number and a.b_batch_status='Completed' group by a.b_product_id,a.b_batch_number ) <> 0 order by b_expiry_date"
                , null);
        return data;
    }

    public int get_customerimage_gallery_image_id() {
        int cig_image_id = 0;
        String selectQuery = "select * from xxmsales_customer_image_gallery order by cig_image_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            cig_image_id = cursor.getInt(1);
            cursor.close();
        }

        return cig_image_id;
    }

    //Get Product Batch Details
   /* public Cursor getProductBatchDetails1(String product_id,String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select a.b_batch_number,a.b_expiry_date,sum(a.b_batch_quantity), b.b_batch_invoice from xxmsales_product_batch a,xxmsales_product_batch_temp b where 1=1 and a.b_product_id=b.b_product_id and a.b_product_id= '"+product_id+"'" + " and b.b_invoice_id=" + invoice_id + " and b.b_batch_status='Inprocess'"
                , null);
        return data;
    } */


    public String getRole(String email) {
        String userrole = null;
        String selectQuery = "select * from xxmsales_user_setup where email_id ='" + email + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            userrole = cursor.getString(8);
            cursor.close();
        }
        return userrole;
    }

    //Insert Data to Customer Details

    public long create_customer(String customer_id, String customer_name, String address1, String address2, String address3, String city, String state, String mobile_number,String credit_limit, String credit_days) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_ID_NEW, customer_id);
        values.put(COLUMN_CUSTOMER_NAME_NEW, customer_name);
        values.put(COLUMN_ADDRESS1_NEW, address1);
        values.put(COLUMN_ADDRESS2_NEW, address2);
        values.put(COLUMN_ADDRESS3_NEW, address3);
        values.put(COLUMN_CITY_NEW, city);
        values.put(COLUMN_STATE_NEW, state);
        values.put(COLUMN_CUS_MOBILE_NUMBER_NEW, mobile_number);
        values.put(COLUMN_CUS_CREDIT_LIMIT_NEW, credit_limit);
        values.put(COLUMN_CUS_CREDIT_DAYS_NEW, credit_days);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CUSTOMER, null, values);
        db.close();
        return id;
    }

    //Insert Data to Company Setup Table

    public long create_company_setup(String company_name, String short_name, String contact_person, String email_id, String mobile_number, String support_number1, String support_number2, String support_email, byte[] image) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_COMPANY_NAME, company_name);
        values.put(COLUMN_SHORT_NAME, short_name);
        values.put(COLUMN_CONTACT_PERSON, contact_person);
        values.put(COLUMN_EMAIL_ID, email_id);
        values.put(COLUMN_MOBILE_NUMBER, mobile_number);
        values.put(COLUMN_SUPPORT_NUMBER1, support_number1);
        values.put(COLUMN_SUPPORT_NUMBER2, support_number2);
        values.put(COLUMN_SUPPORT_EMAIL, support_email);
        values.put(COLUMN_COMPANY_LOGO, image);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_COMPANY_SETUP, null, values);
        db.close();
        return id;
    }

    //Insert Data to User Setup Table
    public long create_user_setup(String first_name, String last_name, String email_id, String password, String confirm_password, String mobile_number, String employee_id, String role, String imageName, String imageUrl) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRST_NAME, first_name);
        values.put(COLUMN_LAST_NAME, last_name);
        values.put(COLUMN_USER_EMAIL_ID, email_id);
        values.put(COLUMN_USER_PASSWORD, password);
        values.put(COLUMN_USER_CONFIRM_PASSWORD, confirm_password);
        values.put(COLUMN_USER_MOBILE_NUMBER, mobile_number);
        values.put(COLUMN_EMPLOYEE_ID, employee_id);
        values.put(COLUMN_ROLE, role);
        values.put(COLUMN_USER_IMAGE_NAME, imageName);
        values.put(COLUMN_USER_IMAGE_URL, imageUrl);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_USER_SETUP, null, values);
        db.close();
        return id;
    }

    public void update_user_setup1(String first_name, String last_name, String email_id,  String employee_id, String role, String imageName, String imageUrl) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRST_NAME, first_name);
        values.put(COLUMN_LAST_NAME, last_name);
        values.put(COLUMN_USER_EMAIL_ID, email_id);
//        values.put(COLUMN_USER_PASSWORD, password);
//        values.put(COLUMN_USER_CONFIRM_PASSWORD, confirm_password);
//        values.put(COLUMN_USER_MOBILE_NUMBER, mobile_number);
        values.put(COLUMN_EMPLOYEE_ID, employee_id);
        values.put(COLUMN_ROLE, role);
        values.put(COLUMN_USER_IMAGE_NAME, imageName);
        values.put(COLUMN_USER_IMAGE_URL, imageUrl);

        db.update(TABLE_USER_SETUP, values, null, null);
        db.close();
    }

    //Insert Data to User Setup Table
    public long create_feedback(String company, String sales_rep_id, String tab_id, String feedback_date, String type, String customer_id, String feedback, String action_taken, int random_num, String feedback_code, String visitSeqNo,String status) {
        ContentValues values = new ContentValues();
        Log.d("Test", "Database create_feedback " + values.toString());

        values.put(COLUMN_F_COMPANY_ID, company);
        values.put(COLUMN_F_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_F_TAB_ID, tab_id);
        values.put(COLUMN_F_DATE, feedback_date);
        values.put(COLUMN_F_TYPE, type);
        values.put(COLUMN_F_CUST_ID, customer_id);
        values.put(COLUMN_F_FEEDBACK, feedback);
        values.put(COLUMN_F_ACTIONTAKEN, action_taken);
        values.put(COLUMN_F_RANDOM_NUM, random_num);
        values.put(COLUMN_FEEDBACK_CODE, feedback_code);
        values.put(COLUMN_F_STATUS, status);
        values.put(COLUMN_F_VISIT_SEQUENCE_NO, visitSeqNo);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_FEEDBACK, null, values);
        db.close();
        return id;
    }

    public void updatemileageimage(String trip_num, String filePath, String fileName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ODOMETER_IMAGE, filePath);
        values.put(COLUMN_TRIP_FILE_NAME, fileName);
        db.update(TABLE_TRIP_DETAILS, values, COLUMN_TRIP_NUMBER + " = ?", new String[]{trip_num});
        db.close();
    }

    public void updateOrderStatusfromweb(String orderNumber, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALESORDER_STATUS, status);
        db.update(TABLE_SALESORDER_HEADER, values, COLUMN_SALESORDER_NUMBER + " = ?", new String[]{orderNumber});
        db.close();
    }

    public void uptPriCollectionStatfrweb(String collectionNumber, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_P_CL_STATUS, status);
        db.update(TABLE_PRIMARY_COLLECTION_DETAILS, values, COLUMN_P_CL_COLLECTION_NUMBER + " = ?", new String[]{collectionNumber});
        db.close();
    }

    public void uptSecCollectionStatfrweb(String collectionNumber, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CL_STATUS, status);
        db.update(TABLE_COLLECTION_DETAILS, values, COLUMN_CL_COLLECTION_NUMBER + " = ?", new String[]{collectionNumber});
        db.close();
    }

    public void updatetripstopdate(String trip_num, String stop_date, String stop_time) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TRIP_STOP_DATE, stop_date);
        values.put(COLUMN_TRIP_STOP_TIME, stop_time);

        db.update(TABLE_TRIP_DETAILS, values, COLUMN_TRIP_NUMBER + " = ?", new String[]{trip_num});
        db.close();
    }


    public void updatecustomerimage(String cid, String image_path, String fileName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_IMAGE_FILE_NAME, fileName);
        values.put(COLUMN_C_CUSTOMER_IMAGE_URL, image_path);
        db.update(TABLE_CUSTOMER_DETAILS, values, COLUMN_CID + " = ?", new String[]{cid});
        db.close();
    }

    //Insert Data to Tab Setup Table
    public long create_tab_setup(String tab_id, String user_id, String tab_prefix) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TAB_ID, tab_id);
        values.put(COLUMN_USER_ID, user_id);
        values.put(COLUMN_TAB_PREFIX, tab_prefix);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_TAB_SETUP, null, values);
        db.close();
        return id;
    }

    //Insert Data to Trip Details Table
    public long create_trip_details(String trip_number, String trip_date, String trip_time, String login_id, String stop_date, String stop_time, String starting_km) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TRIP_NUMBER, trip_number);
        values.put(COLUMN_TRIP_DATE, trip_date);
        values.put(COLUMN_TRIP_TIME, trip_time);
        values.put(COLUMN_TRIP_NAME, trip_number);
        values.put(COLUMN_TRIP_STOP_DATE, stop_date);
        values.put(COLUMN_TRIP_STOP_TIME, stop_time);
        values.put(COLUMN_STARTING_KM, starting_km);

        //values.put(COLUMN_TRIP_NAME,login_id+"18"+000+trip_number);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_TRIP_DETAILS, null, values);
        db.close();
        return id;
    }

    //Insert Data to Order Header
    public long insertToOrderHeader(String sales_rep_id, String customer_id, String salesorder_id, String salesorder_number, String salesorder_date, String salesorder_status, String salesorder_value, String salesorder_lines, String random_num, String visitSeqNo, String order_time) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_O_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_O_CUSTOMER_ID, customer_id);
        values.put(COLUMN_SALESORDER_ID, salesorder_id);
        values.put(COLUMN_SALESORDER_NUMBER, salesorder_number);
        values.put(COLUMN_SALESORDER_DATE, salesorder_date);
        values.put(COLUMN_SALESORDER_STATUS, salesorder_status);
        values.put(COLUMN_SALESORDER_VALUE, salesorder_value);
        values.put(COLUMN_SALESORDER_LINES, salesorder_lines);
        values.put(COLUMN_C_RANDOM_NUM, random_num);
        values.put(COLUMN_CHECKIN_VISIT_SEQUENCE_NO, visitSeqNo);
        values.put(COLUMN_SALESORDER_TIME, order_time);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SALESORDER_HEADER, null, values);
        db.close();
        return id;
    }

  /*  public ArrayList<CreateOrderHeaderModel> getAllOrderHeaderForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SALESORDER_HEADER ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );
        ArrayList<CreateOrderHeaderModel> orderHeaderArrayList = new ArrayList<CreateOrderHeaderModel>();

        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            CreateOrderHeaderModel orderHeaderModel = new CreateOrderHeaderModel();

            cd.setCompany_code(cursor.getInt(0));
            cd.setProduct_id(cursor.getString(1));
            cd.setCompetitor_stock_id(cursor.getString(3));
            cd.setCompetitor_product_id(cursor.getString(3));
            cd.setCompetitor_product_availability(cursor.getInt(4) > 0);
            cd.setCompetitor_product_volume(cursor.getInt(5));
            cd.setCompetitor_product_price(cursor.getDouble(6));
            cd.setCompetitor_product_promotion(cursor.getString(7));
            orderHeaderArrayList.add(i,cd);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return orderHeaderArrayList;
    }*/

    //Insert Data to Order Details
    public long insertToOrderDetail(String sales_rep_id, String customer_id, String salesorder_id, int salesorder_line_id, String salesorder_number, String salesorder_date, String salesorder_status, String product_id, String product_name, String product_uom, String order_quantity, double order_price, double order_value) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_O_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_O_CUSTOMER_ID, customer_id);
        values.put(COLUMN_SALESORDER_ID, salesorder_id);
        values.put(COLUMN_SALESORDER_NUMBER, salesorder_number);
        values.put(COLUMN_ODSALESORDER_LINE_ID, salesorder_line_id);
        values.put(COLUMN_SALESORDER_DATE, salesorder_date);
        values.put(COLUMN_SALESORDER_STATUS, salesorder_status);
        values.put(COLUMN_OD_PRODUCT_ID, product_id);
        values.put(COLUMN_OD_PRODUCT_NAME, product_name);
        values.put(COLUMN_OD_PRODUCT_UOM, product_uom);
        values.put(COLUMN_OD_QUANTITY, order_quantity);
        values.put(COLUMN_OD_PRICE, order_price);
        values.put(COLUMN_OD_VALUE, order_value);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SALESORDER_DETAILS, null, values);
        db.close();
        return id;
    }

    //Insert Data to Order Header Details
    public long insert_invoice_header(String sales_rep_id, String customer_id, String invoice_id, String invoice_number, String invoice_date, String invoice_status, String invoice_value, String invoice_lines, String random_num, String visitSeqNo) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_I_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_I_CUSTOMER_ID, customer_id);
        values.put(COLUMN_INVOICE_ID, invoice_id);
        values.put(COLUMN_INVOICE_NUMBER, invoice_number);
        values.put(COLUMN_INVOICE_DATE, invoice_date);
        values.put(COLUMN_INVOICE_STATUS, invoice_status);
        values.put(COLUMN_INVOICE_VALUE, invoice_value);
        values.put(COLUMN_INVOICE_LINES, invoice_lines);
        values.put(COLUMN_I_RANDOM_NUM, random_num);
        values.put(COLUMN_CHECKIN_VISIT_SEQUENCE_NO, visitSeqNo);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_INVOICE_HEADER, null, values);
        db.close();
        return id;
    }

    //Insert Data to Invoice Details
    public long insert_invoice_details(String sales_rep_id, String customer_id, String invoice_id, int invoice_line_id, String invoice_number, String invoice_date, String invoice_status, String product_id, String product_name, String product_uom, String quantity, double price, double value, String discount, double vat, double value_without_disc, double value_without_vat, double value_with_vat, double discount_value) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_ID_CUSTOMER_ID, customer_id);
        values.put(COLUMN_ID_INVOICE_ID, invoice_id);
        values.put(COLUMN_ID_INVOICE_NUMBER, invoice_number);
        values.put(COLUMN_ID_INVOICE_LINE_ID, invoice_line_id);
        values.put(COLUMN_ID_INVOICE_DATE, invoice_date);
        values.put(COLUMN_ID_INVOICE_STATUS, invoice_status);
        values.put(COLUMN_ID_PRODUCT_ID, product_id);
        values.put(COLUMN_ID_PRODUCT_NAME, product_name);
        values.put(COLUMN_ID_PRODUCT_UOM, product_uom);
        values.put(COLUMN_ID_QUANTITY, quantity);
        values.put(COLUMN_ID_PRICE, price);
        values.put(COLUMN_ID_VALUE, value);
        values.put(COLUMN_ID_DISCOUNT, discount);
        values.put(COLUMN_ID_VAT, vat);
        values.put(COLUMN_ID_VALUE_WITHOUT_DIS, value_without_disc);
        values.put(COLUMN_ID_VALUE_WITHOUT_VAT, value_without_vat);
        values.put(COLUMN_ID_VALUE_WITH_VAT, value_with_vat);
        values.put(COLUMN_ID_DISCOUNT_VALE, discount_value);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_INVOICE_DETAILS, null, values);
        db.close();
        return id;
    }

    //Insert Data to CheckIN Details

    public long insert_checkin_details(String trip_num, String customer_id, String checkin_date, String checkin_time, String checkin_gps_lat, String checkin_gps_long, String checkout_date, String checkout_time, String checkout_gps_lat, String checkout_gps_long, String duration, String sr_code, String cust_type, String visitSeqNo) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_C_TRIP_NUMBER, trip_num);
        values.put(COLUMN_C_CUSTOMER_ID, customer_id);
        values.put(COLUMN_CHECKIN_DATE, checkin_date);
        values.put(COLUMN_CHECKIN_TIME, checkin_time);
        values.put(COLUMN_CHECKIN_GPS_LAT, checkin_gps_lat);
        values.put(COLUMN_CHECKIN_GPS_LONG, checkin_gps_long);

        values.put(COLUMN_CHECKOUT_DATE, checkout_date);
        values.put(COLUMN_CHECKOUT_TIME, checkout_time);
        values.put(COLUMN_CHECKOUT_GPS_LAT, checkout_gps_lat);
        values.put(COLUMN_CHECKOUT_GPS_LONG, checkout_gps_long);

        values.put(COLUMN_CHECKOUT_DURATION, duration);
        values.put(COLUMN_CHECKIN_SR, sr_code);
        values.put(COLUMN_CHECKIN_CUST_TYPE, cust_type);
        values.put(COLUMN_CHECKIN_VISIT_SEQUENCE_NO, visitSeqNo);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CHECKIN_DETAILS, null, values);
        db.close();
        return id;
    }

    //Insert Data to Order Header
    public long insert_product_batch_temp(String product_id, String batch_number, String expiry_date, String batch_qty, String invoice_qty, Integer invoice_id, String batch_status) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_PT_PRODUCT_ID, product_id);
        values.put(COLUMN_PT_BATCH_NUMBER, batch_number);
        values.put(COLUMN_PT_EXPIRY_DATE, expiry_date);
        values.put(COLUMN_PT_BATCH_QUANTITY, batch_qty);
        values.put(COLUMN_PT_BATCH_INVOICE, invoice_qty);
        values.put(COLUMN_PT_INVOICE_ID, invoice_id);
        values.put(COLUMN_PT_BATCH_STATUS, batch_status);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRODUCT_BATCH_TEMP, null, values);
        db.close();
        return id;
    }

    //Insert Data to Order Header
    public long insert_product_batch(String product_id, String batch_number, String expiry_date, String batch_qty, Integer invoice_qty, String receipt_no, String invoice_id, String status) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_PRODUCT_ID, product_id);
        values.put(COLUMN_B_BATCH_NUMBER, batch_number);
        values.put(COLUMN_B_EXPIRY_DATE, expiry_date);
        values.put(COLUMN_B_BATCH_QUANTITY, batch_qty);
        values.put(COLUMN_B_BATCH_INVOICE, invoice_qty);
        values.put(COLUMN_B_RECEIPT_NO, receipt_no);
        values.put(COLUMN_B_INVOICE_ID, invoice_id);
        values.put(COLUMN_B_STATUS, status);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRODUCT_BATCH, null, values);
        db.close();
        return id;
    }

    //Insert Data to Stock Reciept Batch Table
    public long createstockrecieptbatch(String product_id, String expirydate, String batchnumber, String batchqty, String batchinvoice, String batch_recieptno, String batch_invoice_id, String batch_temp_number, String status) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_PRODUCT_ID, product_id);
        values.put(COLUMN_B_EXPIRY_DATE, expirydate);
        values.put(COLUMN_B_BATCH_NUMBER, batchnumber);
        values.put(COLUMN_B_BATCH_QUANTITY, batchqty);
        values.put(COLUMN_B_BATCH_INVOICE, batchinvoice);
        values.put(COLUMN_B_RECEIPT_NO, batch_recieptno);
        values.put(COLUMN_B_INVOICE_ID, batch_invoice_id);
        values.put(COLUMN_B_TEMP_NUMBER, batch_temp_number);
        values.put(COLUMN_B_STATUS, status);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRODUCT_BATCH, null, values);
        db.close();
        return id;
    }

    //Insert Data to Stock Reciept Table TEMP
    public long createstockrecieptbatchtemp(String expirydate, String batchnumber, String batchqty) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_B_TEMPEXPIRY_DATE, expirydate);
        values.put(COLUMN_B_TEMPBATCH_NUMBER, batchnumber);
        values.put(COLUMN_B_TEMPBATCH_QUANTITY, batchqty);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_RECEIPT_BATCH_ROW_TEMP, null, values);
        db.close();
        return id;
    }

    public long insertcustomerimagegallery(int image_id, String imagename, String customerimage, String customer_id, String date, String gps) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_GALLERY_IMAGE_ID, image_id);
        values.put(COLUMN_CUSTOMER_GALLERY_IMAGE_NAME, imagename);
        values.put(COLUMN_CUSTOMER_GALLERY_IMAGE_, customerimage);
        values.put(COLUMN_CUSTOMER_GALLERY_CUSTOMERID, customer_id);
        values.put(COLUMN_CUSTOMER_GALLERY_IMAGE_DATE, date);
        values.put(COLUMN_CUSTOMER_GALLERY_IMAGE_GPS, gps);


        long id = db.insert(TABLE_CUSTOMER_IMAGE_GALLERY, null, values);
        db.close();
        return id;
    }

    //Insert Data to Collection Details
    public long insert_collection_details(String sales_rep_id, String customer_id, int collection_id, String collection_number, String collection_date, double invoice_amount, double paid_amount, double balance_amount,
                                          String collection_type, double payment_amount, byte[] sign, String payment_mode, String check_number, String cheque_date, String bank_branch, String invoice_id, String invoice_num, String random_num, String acc_num, String transfer_date, String t_bank_branch, String teller_num, String invoice_date, String visitSeqNo, String transactionType, String signeeName) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CL_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_CL_CUSTOMER_ID, customer_id);
        values.put(COLUMN_CL_COLLECTION_ID, collection_id);
        values.put(COLUMN_CL_COLLECTION_NUMBER, collection_number);
        values.put(COLUMN_CL_COLLECTION_DATE, collection_date);
        values.put(COLUMN_CL_INVOICE_AMOUNT, invoice_amount);
        values.put(COLUMN_CL_PAID_AMOUNT, paid_amount);
        values.put(COLUMN_CL_BALANCE_AMOUNT, balance_amount);
        values.put(COLUMN_CL_COLLECTION_TYPE, collection_type);
        values.put(COLUMN_CL_PAYMENT_AMOUNT, payment_amount);
        values.put(COLUMN_CL_SIGN, sign);
        values.put(COLUMN_CL_PAYMENT_MODE, payment_mode);
        values.put(COLUMN_CL_CHECK_NUM, check_number);
        values.put(COLUMN_CL_CHECK_DATE, cheque_date);
        values.put(COLUMN_CL_BANK_BRANCH, bank_branch);
        values.put(COLUMN_CL_INVOICE_ID, invoice_id);
        values.put(COLUMN_CL_INVOICE_NUM, invoice_num);
        values.put(COLUMN_CL_RANDOM_NUM, random_num);
        values.put(COLUMN_CL_T_ACC_NUM, acc_num);
        values.put(COLUMN_CL_T_TRANSFER_DATE, transfer_date);
        values.put(COLUMN_CL_T_BANK_BRANCH, t_bank_branch);
        values.put(COLUMN_CL_T_TELLER_NUMBER, teller_num);
        values.put(COLUMN_CL_INVOICE_DATE, invoice_date);
        values.put(COLUMN_CHECKIN_VISIT_SEQUENCE_NO, visitSeqNo);
        values.put(COLUMN_CL_TRANSACTION_TYPE, transactionType);
        values.put(COLUMN_CL_SIGNEE_NAME, signeeName);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_COLLECTION_DETAILS, null, values);
        db.close();
        return id;
    }

    //Insert Data to Collection Details
    public long insert_primary_collection_details(String sales_rep_id, String customer_id, int collection_id, String collection_number, String collection_date, double invoice_amount, double paid_amount, double balance_amount,
                                                  String collection_type, double payment_amount, byte[] sign, String payment_mode, String check_number, String cheque_date, String bank_branch, String invoice_id, String invoice_num, String random_num, String acc_num, String transfer_date, String t_bank_branch, String teller_num, String order_number, String visitSeqNo, String signeeName) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_P_CL_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_P_CL_CUSTOMER_ID, customer_id);
        values.put(COLUMN_P_CL_COLLECTION_ID, collection_id);
        values.put(COLUMN_P_CL_COLLECTION_NUMBER, collection_number);
        values.put(COLUMN_P_CL_COLLECTION_DATE, collection_date);
        values.put(COLUMN_P_CL_INVOICE_AMOUNT, invoice_amount);
        values.put(COLUMN_P_CL_PAID_AMOUNT, paid_amount);
        values.put(COLUMN_P_CL_BALANCE_AMOUNT, balance_amount);
        values.put(COLUMN_P_CL_COLLECTION_TYPE, collection_type);
        values.put(COLUMN_P_CL_PAYMENT_AMOUNT, payment_amount);
        values.put(COLUMN_P_CL_SIGN, sign);
        values.put(COLUMN_P_CL_PAYMENT_MODE, payment_mode);
        values.put(COLUMN_P_CL_CHECK_NUM, check_number);
        values.put(COLUMN_P_CL_CHECK_DATE, cheque_date);
        values.put(COLUMN_P_CL_BANK_BRANCH, bank_branch);
        values.put(COLUMN_P_CL_INVOICE_ID, invoice_id);
        values.put(COLUMN_P_CL_INVOICE_NUM, invoice_num);
        values.put(COLUMN_P_CL_RANDOM_NUM, random_num);
        values.put(COLUMN_P_CL_T_ACC_NUM, acc_num);
        values.put(COLUMN_P_CL_T_TRANSFER_DATE, transfer_date);
        values.put(COLUMN_P_CL_T_BANK_BRANCH, t_bank_branch);
        values.put(COLUMN_P_CL_T_TELLER_NUMBER, teller_num);
        values.put(COLUMN_P_CL_ORDER_NUMBER, order_number);
        values.put(COLUMN_CHECKIN_VISIT_SEQUENCE_NO, visitSeqNo);
        values.put(COLUMN_P_CL_SIGNEE_NAME, signeeName);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRIMARY_COLLECTION_DETAILS, null, values);
        db.close();
        return id;
    }

    public void updateInvoiceBatch(String product_id, String invoice_val, String batch_number, String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_BATCH_QUANTITY, invoice_val);
        values.put(COLUMN_B_BATCH_INVOICE, invoice_val);
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_B_PRODUCT_ID + " = ? AND " + COLUMN_B_BATCH_NUMBER + " = ? AND " + COLUMN_B_INVOICE_ID + " = ?", new String[]{product_id, batch_number, String.valueOf(invoice_id)});
        db.close();
    }

    public void updateInvoiceBatchtemp(String product_id, String invoice_val, String batch_number, Integer invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_BATCH_QUANTITY, invoice_val);
        values.put(COLUMN_B_BATCH_INVOICE, invoice_val);
        db.update(TABLE_PRODUCT_BATCH_TEMP, values, COLUMN_B_PRODUCT_ID + " = ? AND " + COLUMN_B_BATCH_NUMBER + " = ? AND " + COLUMN_B_INVOICE_ID + " = ?", new String[]{product_id, batch_number, String.valueOf(invoice_id)});
        db.close();
    }


    public void updateDeliveryDetails(String lineId, String orderNumber, String waybillQuantity1) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_waybill_details set waybill_quantity=" + waybillQuantity1 + " where salesorder_line_id=" + lineId + " and salesorder_number='" + orderNumber + "'");
        db.close();

    }

    public void updateInvoiceBatchquantity(String invoice_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_product_batch set b_batch_quantity=-b_batch_quantity where b_invoice_id=" + invoice_id);
        db.close();

    }

    public void updateWaybillInvoiceBatchquantity(String orderNumber) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_product_batch set b_batch_quantity=-b_batch_quantity where b_invoice_id='" + orderNumber + "'");
        db.close();

    }

    public void updateTripMileage(String trip_num, String trip_date1, String start_km) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_trip_details set starting_km=" + start_km + " where trip_number= " + trip_num + " and trip_date='" + trip_date1 + "'");
        db.close();

    }

    public void updateTripMileageUploadFlag(String trip_num, String trip_date1) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_trip_details set is_image_uploaded='1' where trip_number= " + trip_num + " and trip_date='" + trip_date1 + "'");
        db.close();

    }

    public void updatebatchqty(int invoice_id, String product_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_product_batch set b_batch_invoice='0',b_batch_quantity='0' where b_invoice_id=" + invoice_id + " and b_product_id='" + product_id + "'");
        db.close();

    }

    public void updatebatchqty_waybill(String invoice_id, String product_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_product_batch set b_batch_invoice='0',b_batch_quantity='0' where b_invoice_id='" + invoice_id + "'" + " and b_product_id='" + product_id + "'");
        db.close();

    }

    public List<String> getAllWarehouse_by_srid(String sales_rep_id) {
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select w_warehouse_name from xxmsales_warehouse " +
                        "where w_sr_code = '" + sales_rep_id + "'"
                , null);

        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

// closing connection
        cursor.close();
        db.close();

// returning lables
        return labels;
    }


   /* public void updateInvoiceBatch_quantity(Integer invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(b_batch_quantity, b_batch_quantity*-1);
        values.put(COLUMN_B_BATCH_INVOICE, invoice_val);
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_B_PRODUCT_ID + " = ? AND " + COLUMN_B_BATCH_NUMBER + " = ? AND " + COLUMN_B_INVOICE_ID + " = ?", new String[]{product_id,batch_number, String.valueOf(invoice_id)});
        db.close();
    } */


    public void updatebatchstatus(Integer invoice_id, String batch_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_STATUS, batch_status);
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_B_INVOICE_ID + " = ?", new String[]{String.valueOf(invoice_id)});
        db.close();
    }

    public void updatebatchstatus_waybill(String invoice_id, String batch_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_STATUS, batch_status);
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_B_INVOICE_ID + " = ?", new String[]{invoice_id});
        db.close();
    }

    public void updatewaybillbatchstatus(String orderNumber, String batch_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_STATUS, batch_status);
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_B_INVOICE_ID + " = ?", new String[]{orderNumber});
        db.close();
    }

    public void updatebatchstatusDelivery(String orderNumber, String batch_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_STATUS, batch_status);
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_B_INVOICE_ID + " = ?", new String[]{String.valueOf(orderNumber)});
        db.close();
    }


    public void updatesponsorshipstatus(String customer_id, String status) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_dr_visit_sponsorship set status='Completed' where customer_id='" + customer_id + "'" + " and status='Draft' ");
        db.close();

    }

    public int get_count_sponsorships(String customer_id) {
        String selectQuery = "select sponsorship_id from xxmsales_dr_visit_sponsorship where customer_id='" + customer_id + "'" + " and status='Draft' ";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    //Get Stock Details in Warehouse with products
    public Cursor getWarehouseDetailswithoutProducts(String sales_rep_id, String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select s_product_name,s_product_uom,s_product_quantity,s_product_value from xxmsales_stock_details where s_sr_code = '" + sales_rep_id + "'"
                + "and s_product_id='" + product_id + "'", null);
        return data;
    }

    //Get Stock Details in stock
    public Cursor getStockDetailsmystock(String login_id, String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select sr_product_name,sr_product_uom,sum(sr_product_quantity),sum(sr_product_price),sum(sr_product_value) from xxmsales_stock_reciept where sr_sr_name = '" + login_id + "'" + "and sr_product_name='" + product_id + "'"
                , null);
        return data;
    }

    //Get Stock Details in stock
    public ArrayList<Product> getStockReceiptProducts() {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select sr_product_name,p.product_name,p.batch_controlled,avg(sr_product_price),sr_product_uom,p.price from xxmsales_stock_reciept as sr left join xxmsales_item_details as p on p.product_id = sr.sr_product_name group by sr_product_name  having sum(sr.sr_product_quantity)>0 ORDER BY p.product_name ASC";

        Cursor data = db.rawQuery(selectQuery, null);
        ArrayList<Product> productArrayList = new ArrayList<Product>();
        while (data.moveToNext()) {
            Product p = new Product();
            p.setProduct_id(data.getString(0));
            p.setProduct_code(data.getString(0));
            p.setProduct_name(data.getString(1));
            p.setBatch_controlled(data.getString(2));
            String price = TextUtils.isEmpty(data.getString(3)) ? data.getString(5) : data.getString(3);
            p.setProduct_price(price);
            p.setProduct_uom(data.getString(4));
            productArrayList.add(p);
        }
        return productArrayList;
    }

    //Get Stock Details in stock
    public ArrayList<Product> getStockReceiptProductsByType(int type, int isInvoiceId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select sr_product_name,p.product_name,p.batch_controlled,p.product_uom,avg(sr_product_price),p.price from xxmsales_stock_reciept as sr left join xxmsales_item_details as p on p.product_id = sr.sr_product_name where (p.batch_controlled='Yes' and sr.sr_product_name in (select b_product_id from xxmsales_product_batch where (Date(b_expiry_date)>Date('now'))) OR  (p.batch_controlled='No'))";
        if (isInvoiceId != 0)
            selectQuery += " and p.product_id not in (select product_id from xxmsales_invoice_details where invoice_id=" + isInvoiceId + ")";
        selectQuery += " group by sr_product_name  having sum(sr_product_quantity)>0 ORDER BY p.product_name ASC";

        if (type == 1) {
            selectQuery = "select sr_product_name,p.product_name,p.batch_controlled,p.product_uom,avg(sr_product_price),p.price from xxmsales_stock_reciept as sr left join xxmsales_item_details as p on p.product_id = sr.sr_product_name where p.batch_controlled='Yes' and sr.sr_product_name in (select b_product_id from xxmsales_product_batch where Date(b_expiry_date)<=Date('now') group by b_product_id having sum(b_batch_quantity)!=0) group by sr_product_name having sum(sr_product_quantity)>0 ORDER BY p.product_name ASC";
        }
        Cursor data = db.rawQuery(selectQuery, null);
        ArrayList<Product> productArrayList = new ArrayList<Product>();
        while (data.moveToNext()) {
            Product p = new Product();
            p.setProduct_id(data.getString(0));
            p.setProduct_code(data.getString(0));
            p.setProduct_name(data.getString(1));
            p.setBatch_controlled(data.getString(2));
            p.setProduct_uom(data.getString(3));
            String price = TextUtils.isEmpty(data.getString(4)) ? data.getString(5) : data.getString(4);
            p.setProduct_price(price);
            Log.d("StockReturnLog", "Product Name--- " + p.getProduct_name());
            productArrayList.add(p);
        }
        return productArrayList;
    }

    //Insert Data to Stock Reciept Table TEMP
    public long createstockreciepttablerow(String prod_name, String prod_uom, String prod_qty, String prod_price, String total_prod_vale) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_R_PRODUCT_NAME, prod_name);
        values.put(COLUMN_R_PRODUCT_UOM, prod_uom);
        values.put(COLUMN_R_PRODUCT_QTY, prod_qty);
        values.put(COLUMN_R_PRODUCT_PRICE, prod_price);
        values.put(COLUMN_R_PRODUCT_VALUE, total_prod_vale);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_RECEIPT_ROW_TEMP, null, values);
        db.close();
        return id;
    }

    //Get Temp table datas
    public Cursor getStockReceiptTempDatas() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select r_product_name,r_product_uom,r_product_qty,r_product_price,r_product_value from xxmsales_receipt_row_temp", null);
        return data;
    }

    //Get Temp table datas
    public Cursor getInvoiceproductLines(String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select product_id,product_uom,-quantity,-price,-value from xxmsales_invoice_details where invoice_id=" + invoice_id + " and invoice_status='Draft'", null);
        return data;
    }

    //Get Temp table datas
    public Cursor getWaybillDetails(String orderNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select product_id,product_uom,-waybill_quantity,-price,-value from xxmsales_waybill_details where salesorder_number='" + orderNumber + "'" + " and waybill_status='Draft'", null);
        return data;
    }

    //Get CompanyDetails
    public ArrayList<CompanySetup> getCompanySetup() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select * from " + TABLE_COMPANY_SETUP
                , null);
        ArrayList<CompanySetup> companySetupArrayList = new ArrayList<CompanySetup>();
        while (data.moveToNext()) {
            CompanySetup cs = new CompanySetup();
            cs.setId(data.getInt(0));
            cs.setCompany_name(data.getString(1));
            cs.setShort_name(data.getString(2));
            cs.setContact_person(data.getString(3));
            cs.setEmail_id(data.getString(4));
            cs.setMobile_number(data.getString(5));
            cs.setSupport_number1(data.getString(6));
            cs.setSupport_number2(data.getString(7));
            cs.setSupport_email(data.getString(8));
            cs.setCompany_logo(data.getBlob(9));
            companySetupArrayList.add(cs);
        }
        return companySetupArrayList;
    }

    public long create(CompanySetup companySetup) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_COMPANY_NAME, companySetup.company_name);
        values.put(COLUMN_SHORT_NAME, companySetup.short_name);
        values.put(COLUMN_CONTACT_PERSON, companySetup.contact_person);
        values.put(COLUMN_EMAIL_ID, companySetup.email_id);
        values.put(COLUMN_MOBILE_NUMBER, companySetup.mobile_number);
        values.put(COLUMN_SUPPORT_NUMBER1, companySetup.support_number1);
        values.put(COLUMN_SUPPORT_NUMBER2, companySetup.support_number2);
        values.put(COLUMN_SUPPORT_EMAIL, companySetup.support_email);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_COMPANY_SETUP, null, values);
        db.close();
        return id;

    }

    //Get CompanyDetails
    /*public ArrayList<LocationHeader> getLocationName() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select distinct location_id,location_name from " + TABLE_LOCATION_LINES
                , null);
        ArrayList<LocationHeader> locationsArrayList = new ArrayList<LocationHeader>();
        while (data.moveToNext()) {
            LocationHeader cs = new LocationHeader();

            cs.setLocation_id(data.getString(0));
            cs.setLocation_name(data.getString(1));

            locationsArrayList.add(cs);
        }
        return locationsArrayList;
    }*/

    public List<String> getLocationName() {
        List<String> locationsArrayList = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT location_name FROM " + TABLE_LOCATION_LINES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                locationsArrayList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return locationsArrayList;
    }

    public String getDistributorsCode(String distributorString) {
        String priceList_code = "";

        String selectQuery = "SELECT distributorCode FROM " + TABLE_DISTRIBUTORS +  " where distributorName ='" + distributorString + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            priceList_code = cursor.getString(0);
            cursor.close();
        }

        return priceList_code;
    }
    public List<String> getDistributorsName() {

        List<String> distributorsArrayList = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT distinct distributorName FROM " + TABLE_DISTRIBUTORS + " WHERE distributorName NOT LIKE '%_MDS%' ORDER BY distributorName ASC";


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {

            do {
                distributorsArrayList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return distributorsArrayList;
    }


    public List<String> getStateName() {
        List<String> locationsArrayList = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT state FROM " + TABLE_LOCATION_LINES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                locationsArrayList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return locationsArrayList;
    }



    public List<String> getCityName() {
        List<String> locationsArrayList = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT city FROM " + TABLE_LOCATION_LINES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                locationsArrayList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return locationsArrayList;
    }

    public List<String> getCountryName() {
        List<String> locationsArrayList = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT country FROM " + TABLE_LOCATION_LINES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                locationsArrayList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return locationsArrayList;
    }

    public List<String> getLgaName() {
        List<String> locationsArrayList = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT DISTINCT lga FROM " + TABLE_LOCATION_LINES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                locationsArrayList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return locationsArrayList;
    }


    //Get CompanyDetails
    public ArrayList<Locations> getLocations() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select * from " + TABLE_LOCATION_LINES
                , null);
        ArrayList<Locations> locationsArrayList = new ArrayList<Locations>();
        while (data.moveToNext()) {
            Locations cs = new Locations();
            cs.setId(data.getInt(0));
            cs.setLocation_id(data.getString(1));
            cs.setLocation_name(data.getString(2));
            cs.setLga_id(data.getString(3));
            cs.setLga(data.getString(4));
            cs.setCity_id(data.getString(5));
            cs.setCity(data.getString(6));
            cs.setState_id(data.getString(7));
            cs.setState(data.getString(8));
            cs.setCountry(data.getString(9));
            cs.setCountry_id(data.getString(10));
            locationsArrayList.add(cs);
        }
        return locationsArrayList;
    }

    public ArrayList<OrderProductSpinner> getMetricsProductSpinner() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

        data = db.rawQuery("select distinct a.product_code,a.product_id,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where  c.price_list_code =b.price_list_code and b.price_list_code=a.price_id ", null);


        ArrayList<OrderProductSpinner> orderProductSpinnerArrayList = new ArrayList<OrderProductSpinner>();
        while (data.moveToNext()) {
            OrderProductSpinner cs = new OrderProductSpinner();
            cs.setProduct_id(data.getString(0));
            cs.setProduct(data.getString(1));
            cs.setProd_type(data.getString(2));

            orderProductSpinnerArrayList.add(cs);
        }
        return orderProductSpinnerArrayList;
    }
    //Get CompanyDetails
    public ArrayList<OrderProductSpinner> getOrderProductSpinner(String customer_id, int order_id, String product_type) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

        if ("scheme".equals(product_type)) {
            data = db.rawQuery("select distinct a.product_code,a.product_id,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where (select product_type from xxmsales_item_details where product_id=a.product_code)='scheme' and a.product_code not in (select product_id from xxmsales_order_details where salesorder_id=" + order_id + ") and c.price_list_code =b.price_list_code and b.price_list_code=a.price_id and c.customer_id='" + customer_id + "'", null);
        } else if ("focus".equals(product_type)) {
            data = db.rawQuery("select distinct a.product_code,a.product_id,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where (select product_type from xxmsales_item_details where product_id=a.product_code)='focus' and a.product_code not in (select product_id from xxmsales_order_details where salesorder_id=" + order_id + ") and (select product_type from xxmsales_item_details where product_id=a.product_code) not in (SELECT product_type from xxmsales_item_details where product_type='scheme') and c.price_list_code =b.price_list_code and b.price_list_code=a.price_id and c.customer_id='" + customer_id + "'", null);
        } else {
            data = db.rawQuery("select distinct a.product_code,a.product_id,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where a.product_code not in (select product_id from xxmsales_order_details where salesorder_id=" + order_id + ") and c.price_list_code =b.price_list_code and b.price_list_code=a.price_id and c.customer_id='" + customer_id + "'", null);
        }


        ArrayList<OrderProductSpinner> orderProductSpinnerArrayList = new ArrayList<OrderProductSpinner>();
        while (data.moveToNext()) {
            OrderProductSpinner cs = new OrderProductSpinner();
            cs.setProduct_id(data.getString(0));
            cs.setProduct(data.getString(1));
            cs.setProd_type(data.getString(2));

            orderProductSpinnerArrayList.add(cs);
        }
        return orderProductSpinnerArrayList;
    }

    public ArrayList<OrderProductSpinner> getProductSpinner() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

            data = db.rawQuery("select distinct a.product_code,a.product_id,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where   c.price_list_code =b.price_list_code and b.price_list_code=a.price_id ", null);


         ArrayList<OrderProductSpinner> orderProductSpinnerArrayList = new ArrayList<OrderProductSpinner>();
        while (data.moveToNext()) {
            OrderProductSpinner cs = new OrderProductSpinner();
            cs.setProduct_id(data.getString(0));
            cs.setProduct(data.getString(1));
            cs.setProd_type(data.getString(2));

            orderProductSpinnerArrayList.add(cs);
        }
        return orderProductSpinnerArrayList;
    }

    public  ArrayList<StockAdjustment>  getStockProductDetails(String productname) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select distinct a.product_code,a.price,(select product_uom from xxmsales_item_details where product_id=a.product_code) product_uom from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where c.price_list_code =b.price_list_code and b.price_list_code=a.price_id and a.product_id ='" + productname + "'", null);

        ArrayList<StockAdjustment> orderProductSpinnerArrayList = new ArrayList<StockAdjustment>();
        while (data.moveToNext()) {
            StockAdjustment cs = new StockAdjustment();
            cs.setProductId(data.getString(0));
            cs.setProductPrice(data.getString(1));
            cs.setProductUom(data.getString(2));
            cs.setProductUom(data.getString(2));

            orderProductSpinnerArrayList.add(cs);
        }
        return orderProductSpinnerArrayList;
    }


    public ArrayList<Orders> getOrderNumber(String customer_id) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT salesorder_number from xxmsales_order_header where salesorder_status='Pending for Waybill' and customer_id=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customer_id});
        Log.d("Data", "Get Sales Order Number  ");
        ArrayList<Orders> ordersArrayList = new ArrayList<Orders>();

        while (cursor.moveToNext()) {
            Orders w = new Orders();
            w.setOrderCode(cursor.getString(0));
            ordersArrayList.add(w);
        }
        cursor.close();
        sqlWrite.close();
        return ordersArrayList;
    }


    //Get Stock Details in stock
    public Cursor getStockDetails(String login_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select sr_product_name,sr_product_uom,sum(sr_product_quantity),sr_product_price,sr_product_value from xxmsales_stock_reciept where sr_sr_name = '" + login_id + "' group by sr_product_name"
                , null);
        return data;
    }

    //Get Stock Details in stock
    public ArrayList<ViewStockMyStockProduct> getMyStockProducts(String login_id, String product_id, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select sr_product_name,p.product_name,sr_product_uom,sum(sr_product_quantity),avg(DISTINCT(sr_product_price)),(select count(*) from xxmsales_invoice_details as sid left join xxmsales_invoice_header as sih on sih.invoice_number = sid.invoice_number where sid.product_id = sr_product_name and sih.invoice_status='Completed' and sid.invoice_date = '" + date + "') as invoice_count,(select product_name from xxmsales_primary_invoice_product where product_code= sr_product_name) as invoice_product_name, (select avg(DISTINCT(sid.price)) from xxmsales_invoice_details as sid left join xxmsales_invoice_header as sih on sih.invoice_number = sid.invoice_number where sid.product_id = sr_product_name and sih.invoice_status='Completed' group by sid.product_id) as  invoice_product_price,p.batch_controlled,sum(sr.sr_product_quantity*sr_product_price),(select count(b_product_id) from xxmsales_product_batch where Date(b_expiry_date)<=Date('now') and b_product_id = sr.sr_product_name group by b_product_id having sum(b_batch_quantity)!=0) as expiry_count,p.price from xxmsales_stock_reciept as sr left join xxmsales_item_details as p on p.product_id = sr.sr_product_name where sr_sr_name = '" + login_id + "'";
        if (!product_id.equals("-1")) {
            selectQuery += " and sr_product_name='" + product_id + "'";
        }
        selectQuery += " group by sr_product_name  having sum(sr_product_quantity)>0 ORDER BY sr_product_name ASC";
        Cursor data = db.rawQuery(selectQuery, null);
        ArrayList<ViewStockMyStockProduct> myStockProductList = new ArrayList<ViewStockMyStockProduct>();
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            Log.d("XXXXX", "No Products Found");
        } else {
            while (data.moveToNext()) {
                ViewStockMyStockProduct stockProduct = new ViewStockMyStockProduct();
                stockProduct.setProductId(data.getString(0));
                stockProduct.setProductName((TextUtils.isEmpty(data.getString(1)) ? data.getString(6) : data.getString(1)));
                stockProduct.setProductUom(data.getString(2));
                stockProduct.setProductQuantity(data.getString(3));
                String price = (TextUtils.isEmpty(data.getString(4)) ? data.getString(11) : data.getString(4));
                //String price = data.getString(4);
                stockProduct.setProductPrice(price);

                Log.d("XXXXX", "Price--4th--" + data.getString(4) + "---11 Column--" + data.getString(11) + "==" + price);

                double totalPrice = Double.parseDouble(data.getString(3)) * Double.parseDouble(price);
                stockProduct.setProductTotalPrice(String.valueOf(totalPrice));
                //stockProduct.setProductTotalPrice(data.getString(9));
                stockProduct.setSecondaryInvoiceCount(data.getString(5));
                stockProduct.setBatchControlled(data.getString(8));
                stockProduct.setExpiredCount(data.getString(10));
                Log.d("MyExpiryCount", "Count" + data.getString(10) + " pro" + stockProduct.getProductName());
                myStockProductList.add(stockProduct);
            }
        }
        return myStockProductList;
    }

    public ArrayList<Product> getStockReceiptProductDetails(String sec_price_list) {
//    public ArrayList<Product> getStockReceiptProductDetails() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

//        String sec_price_list1 = "PRICE001";
       /* String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom, IFNULL(sum(sr.sr_product_quantity), 0) as productQuantity, avg(pll.price), " +
                "sid.batch_controlled, sr.invoice_id, max(sr.sr_receipt_number) " +
                "FROM xxmsales_item_details as pt" +
                "LEFT JOIN xxmsales_price_list_lines as pll ON pt.product_id = pll.product_code " +
                "LEFT JOIN xxmsales_price_list_header as ph ON '"+sec_price_list1+"' =ph.price_list_code and ph.price_list_code=pll.price_id " +
                "LEFT JOIN xxmsales_item_details as sid ON pt.product_id = sid.product_id " +
                "LEFT JOIN  xxmsales_stock_reciept as sr ON pt.product_id = sr.sr_product_name GROUP by pt.product_id ORDER BY pt.product_name asc, sr.sr_receipt_number desc ";
*/


//        String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom, IFNULL(sum(sr.sr_product_quantity), 0) as productQuantity, pll.price, pt.batch_controlled, sr.invoice_id FROM xxmsales_item_details as pt, xxmsales_price_list_lines as pll,xxmsales_price_list_header as ph, xxmsales_stock_reciept as sr where  pt.product_id = pll.product_code and  '"+ sec_price_list +"' = ph.price_list_code and ph.price_list_code=pll.price_id  GROUP by pt.product_id ORDER BY pt.product_name asc";
        String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom,pll.price, pt.batch_controlled FROM xxmsales_item_details as pt, xxmsales_price_list_lines as pll,xxmsales_price_list_header as ph where  pt.product_id = pll.product_code and  '"+ sec_price_list +"' = ph.price_list_code and ph.price_list_code=pll.price_id  GROUP by pt.product_id ORDER BY pt.product_name asc";

        System.out.println("RRR :: selectQuery = " + selectQuery);
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        ArrayList<Product> StockReceiptEntries = new ArrayList<>();

        while (cursor.moveToNext()) {
            Product mStock = new Product();
            mStock.setProduct_id(cursor.getString(0));
            mStock.setProduct_code(cursor.getString(0));
            mStock.setProduct_name(cursor.getString(1));
            mStock.setProduct_uom(cursor.getString(2));
//            mStock.set(cursor.getString(3));
//            mStock.setAdjustmentQuantity("0");
            mStock.setProduct_price(cursor.getString(3));
            mStock.setBatch_controlled(cursor.getString(4));
//            mStock.setInvoiceNumber(cursor.getString(6));

            System.out.println("RRR :: mStock = " + new Gson().toJson(mStock));
            StockReceiptEntries.add(mStock);
        }
        cursor.close();
        sqlWrite.close();
        return StockReceiptEntries;
    }
    public ArrayList<Product> getStockReceiptProductDetails1() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

//        String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom, IFNULL(sum(sr.sr_product_quantity), 0) as productQuantity, pt.price, pt.batch_controlled, sr.invoice_id FROM xxmsales_item_details as pt, xxmsales_price_list_lines as pll,xxmsales_stock_reciept as sr where  pt.product_id = pll.product_code   GROUP by pt.product_id ORDER BY pt.product_name asc";
        String selectQuery = "SELECT pt.product_id, pt.product_name, pt.product_uom, pt.price, pt.batch_controlled FROM xxmsales_item_details as pt, xxmsales_price_list_lines as pll,xxmsales_stock_reciept as sr where  pt.product_id = pll.product_code   GROUP by pt.product_id ORDER BY pt.product_name asc";

        System.out.println("RRR :: selectQuery = " + selectQuery);
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        ArrayList<Product> StockReceiptEntries = new ArrayList<>();

        while (cursor.moveToNext()) {
            Product mStock = new Product();
            mStock.setProduct_id(cursor.getString(0));
            mStock.setProduct_code(cursor.getString(0));
            mStock.setProduct_name(cursor.getString(1));
            mStock.setProduct_uom(cursor.getString(2));
//            mStock.set(cursor.getString(3));
//            mStock.setAdjustmentQuantity("0");
            mStock.setProduct_price(cursor.getString(4));
            mStock.setBatch_controlled(cursor.getString(5));
//            mStock.setInvoiceNumber(cursor.getString(6));

            System.out.println("RRR :: mStock = " + new Gson().toJson(mStock));
            StockReceiptEntries.add(mStock);
        }
        cursor.close();
        sqlWrite.close();
        return StockReceiptEntries;
    }

    //Get Today Order Count for the products
    public Cursor getTodayOderCount(String sales_rep_id, String product_id, String salesorder_date) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select count(*) as order_count from xxmsales_order_details where sales_rep_id = '" + sales_rep_id + "'"
                + "and product_name='" + product_id + "'" + "and salesorder_date='" + salesorder_date + "'", null);
        return data;
    }

    public void update_product_order_count(String product_id, String order_count) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_S_PRODUCT_ORDER_COUNT, order_count);
        db.update(TABLE_STOCK_DETAILS, values, COLUMN_S_PRODUCT_NAME + " = ?", new String[]{product_id});
        db.close();
    }


    public void deleteRecord() {

        SQLiteDatabase db = this.getWritableDatabase();

        // Deleting all records from database table
        db.delete(TABLE_RECEIPT_ROW_TEMP, null, null);
        db.close();

    }

    public void deleteMaster() {

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_MASTER_DOWNLOAD, null, null);

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

    }


    public String getLoginID(String user_email, String user_password) {
        String login_id = user_email;
        String login_password = user_password;

        String selectQuery = "select * from " + TABLE_USER_SETUP + " where " + COLUMN_USER_EMAIL_ID + "='" + login_id + "' and " + COLUMN_USER_PASSWORD + "='" + login_password + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            login_id = cursor.getString(7);
            cursor.close();
        } else {
            login_id = "0";
        }

        return login_id;
    }

    public String getOldPassword(String login_id) {
        String password = null;
        String selectQuery = "select * from xxmsales_login_details where login_id ='" + login_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            password = cursor.getString(5);
            cursor.close();
        }

        return password;
    }

  /*  public int getMileage(String trip_date, String trip_num) {
        int mileage = 0;
        String selectQuery = "select max(starting_km) from xxmsales_trip_details where trip_date ='" + trip_date + "'" + " and trip_number not in ('" + trip_num + "')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            mileage = cursor.getInt(0);
            cursor.close();
        }

        return mileage;
    } */

    public int getMileage(String trip_date) {
        int mileage = 0;
        String selectQuery = "select starting_km from xxmsales_trip_details where starting_km is not null ORDER by starting_km DESC LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            mileage = cursor.getInt(0);
            cursor.close();
        }

        return mileage;
    }

    public int get_template_id() {
        int template_id = 0;
        String selectQuery = "select * from xxmsales_order_header order by template_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            template_id = cursor.getInt(12);
            cursor.close();
        }

        return template_id;
    }

    public int get_activate_flag() {
        int activate_flag = 1;
        String selectQuery = "select activate_flag from xxmsales_tab_setup";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            activate_flag = cursor.getInt(0);
            System.out.println("activate_flag = " + activate_flag);
            cursor.close();
        }

        return activate_flag;
    }

    public void updatePassword(String login_id, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_PASSWORD, password);
        db.update(TABLE_LOGIN, values, COLUMN_lOGIN_ID + " = ?", new String[]{login_id});
        db.close();
    }

    //Get Sales Rep Details
    public Cursor getSalesRepDetails(String login_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_salesrep_detail where sales_rep_id = '" + login_id + "'"
                , null);

        return data;
    }

    //Get GPS Details
    public Cursor getGPSdistance() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select gps_latitude,gps_longitude from xxmsales_gps_tracker where gps_date=date('now') "
                , null);

        return data;
    }

    //Get Sales Rep Details
    public Cursor getUserDetails(String login_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select email_id,first_name from xxmsales_user_setup where employee_id = '" + login_id + "'"
                , null);

        return data;
    }

    public Cursor getSetupDetails() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select (select tab_id from xxmsales_tab_setup) tab_code,(select employee_id from xxmsales_user_setup) sr_code,(select short_name from xxmsales_company_setup) company_code,(select tab_prefix from xxmsales_tab_setup) tab_prefix ,(select base_url from xxmsales_tab_setup) base_url,(select first_name from xxmsales_user_setup) sr_name,(select company_name from xxmsales_company_setup) company_name"
                , null);

        return data;
    }

    //Get Sales Rep Details
    public Cursor getTabDeatils() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select tab_id,user_id from xxmsales_tab_setup "
                , null);

        return data;
    }

    public Cursor getUserDeatils() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select first_name,last_name,email_id,mobile_number,employee_id,password,confirm_password from xxmsales_user_setup "
                , null);

        return data;
    }

    //Get Sales Rep Details
    public Cursor getCompanyDetails() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select company_name,short_name,contact_person,email_id,mobile_number,support_number1,support_number2,support_email,address_line1, address_line2, address_line3     from xxmsales_company_setup "
                , null);

        return data;
    }

    //Get Sales Rep Details
    public Cursor getorderHeaderInfo(String order_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select lpo_number,remarks,delivery_date,ship_address1,ship_address2,salesorder_date, order_time,signature,signature_name from xxmsales_order_header where salesorder_id = '" + order_id + "'"
                , null);

        return data;
    }


    //Get Sales Rep Details
    public Cursor getCollectionTime(String collection_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select created_at from xxmsales_collection_details where id = '" + collection_id + "'", null);

        return data;
    }


    //Get Sales Rep Details
    public Cursor get_current_salescount(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {


            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where salesorder_date=date('now') and salesorder_status='Completed' ", null);
        }else {


            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where salesorder_date=date('now') and salesorder_status='Completed' ", null);
        }
            /*  }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where salesorder_date=date('now') and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/

        return data;
    }



    public Cursor get_week_salescount(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')/ 7.0 ) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days') and salesorder_status='Completed' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate) ", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')/7.0 ) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days') and salesorder_status='Completed' ", null);
        }
       /* }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days') and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/

        return data;
    }



    public Cursor get_month_salescount(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')/30.0 ) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);
        }else {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) /  (('" + days + "')/30.0 ) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);
        }
            /* }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/
        return data;
    }



    public Cursor get_quarter_salescount(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);
        }else {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);
        }
            //        }
//        else{
//            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);
//
//        }
        return data;
    }



    public Cursor get_year_salescount(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed' ", null);
        }else {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed' ", null);
        }
       /* }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/
        return data;
    }





    public Cursor get_current_salesvalue(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;



        if(targetTypeString.equals("Quantity")){


            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_lines),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where salesorder_date=date('now') and salesorder_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where salesorder_date=date('now') and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_value),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where salesorder_date=date('now') and salesorder_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where salesorder_date=date('now') and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }

        return data;
    }




    //Get Sales Rep Details
    public Cursor get_week_salesvalue(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;
        if(targetTypeString.equals("Quantity")){
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_lines),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days')  and salesorder_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days')  and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_value),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days')  and salesorder_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days')  and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }


        return data;
    }

    //Get Sales Rep Details
    public Cursor get_month_salesvalue(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;

        if(targetTypeString.equals("Quantity")){
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_lines),(select cast(sum(value) as double) / (('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_value),(select cast(sum(value) as double) / (('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select sum(value)/(('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }


        return data;
    }

    //Get Sales Rep Details
    public Cursor get_quarter_salesvalue(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;

        if(targetTypeString.equals("Quantity")){
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_lines),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')/90.0)  from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_value),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }


        return data;
    }

    //Get Sales Rep Details
    public Cursor get_year_salesvalue(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;

        if(targetTypeString.equals("Quantity")){
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_lines),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) /(('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType = 'Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(salesorder_value),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_header where strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_order_details where strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed' and product_name = '" + productNameString + "'", null);

        }


        return data;
    }
    public Cursor get_current_salescount1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;



//        if (productNameString.equals("All")) {

        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where invoice_date=date('now') and invoice_status='Completed' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where invoice_date=date('now') and invoice_status='Completed' ", null);
        } /* }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where invoice_date=date('now') and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/

        return data;
    }



    public Cursor get_week_salescount1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "", toDate = "", toDate1 = "";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
            ;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') / 7.0 )from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where DATE(invoice_date) >= DATE('now', 'weekday 0', '-7 days') and invoice_status='Completed' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') /7.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where DATE(invoice_date) >= DATE('now', 'weekday 0', '-7 days') and invoice_status='Completed' ", null);
        }/* }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where DATE(invoice_date) >= DATE('now', 'weekday 0', '-7 days') and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }
*/
        return data;
    }

    //Get Sales Rep Details
    public Cursor get_month_salescount1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') /30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);
        } else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') /30.0 ) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);
        } /*}
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_quarter_salescount1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') /90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') /90.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);
        }/* }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_year_salescount1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();



        Cursor data;

//        if (productNameString.equals("All")) {
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') /365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%Y', invoice_date) = strftime('%Y', date('now')) and invoice_status='Completed' ", null);
        }else {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(*),(select cast(sum(value) as double) / (('" + days + "') /365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%Y', invoice_date) = strftime('%Y', date('now')) and invoice_status='Completed' ", null);
        } /*  }
        else{
            data = db.rawQuery("select count(*),(select sum(value) from xxmsales_metrics_target where targetType='Sales' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%Y', invoice_date) = strftime('%Y', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }*/

        return data;
    }


    //Get Sales Rep Details
    public Cursor get_current_salesvalue1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;



        if(targetTypeString.equals("Quantity")){


            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_lines),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where invoice_date=date('now') and invoice_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where invoice_date=date('now') and invoice_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_value),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where invoice_date=date('now') and invoice_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Secondary') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where invoice_date=date('now') and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }

        return data;
    }



    //Get Sales Rep Details
    public Cursor get_week_salesvalue1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;


        if(targetTypeString.equals("Quantity")){

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_lines),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where DATE(invoice_date) >= DATE('now', 'weekday 0', '-7 days') and invoice_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where DATE(invoice_date) >= DATE('now', 'weekday 0', '-7 days') and invoice_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_value),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where DATE(invoice_date) >= DATE('now', 'weekday 0', '-7 days') and invoice_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')/7.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Secondary') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where DATE(invoice_date) >= DATE('now', 'weekday 0', '-7 days') and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_month_salesvalue1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;


        if(targetTypeString.equals("Quantity")){

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_lines),(select cast(sum(value) as double) / (('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_value),(select cast(sum(value) as double) / (('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')/30.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Secondary') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_quarter_salesvalue1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;


        if(targetTypeString.equals("Quantity")){

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_lines),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_value),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')/90.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Secondary') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%m', invoice_date) = strftime('%m', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_year_salesvalue1(String targetTypeString, String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();



        Cursor data;


        if(targetTypeString.equals("Quantity")){

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_lines),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%Y', invoice_date) = strftime('%Y', date('now')) and invoice_status='Completed' ", null);
            else
                data = db.rawQuery("select sum(quantity),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Quantity' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%Y', invoice_date) = strftime('%Y', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);
        }
        else{
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer')  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (productNameString.equals("All"))
                data = db.rawQuery("select sum(invoice_value),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_header where strftime('%Y', invoice_date) = strftime('%Y', date('now')) and invoice_status='Completed' ", null);

            else
                data = db.rawQuery("select sum(value),(select cast(sum(value) as double) / (('" + days + "')/365.0) from xxmsales_metrics_target where targetType='Sales' and targetValue='Value' and (cType ='Both' OR cType ='Primary Secondary') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_invoice_details where strftime('%Y', invoice_date) = strftime('%Y', date('now')) and invoice_status='Completed' and product_name = '" + productNameString + "'", null);

        }


        return data;
    }



    //Get Sales Rep Details
    public Cursor get_current_plancount() {
        SQLiteDatabase db = this.getWritableDatabase();

//        Cursor data = db.rawQuery("select count(distinct customer_id),(select sum(value) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where checkin_date=date('now') and customer_type = 'Primary' ", null);


//        System.out.println ("TTT::Days111: " + days);

          /*   Cursor toCur = db.rawQuery("select toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if (toCur.getCount() > 0) {
            toCur.moveToFirst();
            toDate = toCur.getString(0);
            System.out.println("TTT::toDate= " + toDate);
            toCur.close();
        }*/
        /*
        Cursor toCur1 = db.rawQuery("select sum(value) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if (toCur1.getCount() > 0) {
            toCur1.moveToFirst();
            toDate1 = toCur1.getString(0);
            System.out.println("TTT::toDate1= " + toDate1);
            toCur1.close();
        }*/

      /*  double div = Double.parseDouble(toDate1)/ days;
        System.out.println ("TTT::Days221: " + div);*/
//        (CAST((sum(value)) as double)) / (('" + days + "'))
        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select  count(customer_id),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where checkin_date=date('now') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type = 'Primary' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select  count(customer_id),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer')  and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where checkin_date=date('now') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type = 'Primary' ", null);

        }
        return data;
    }

    //Get Sales Rep Details
    public Cursor get_week_plancount() {
        SQLiteDatabase db = this.getWritableDatabase();



        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 7.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where DATE(checkin_date) >= DATE('now', 'weekday 0', '-7 days') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }else {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 7.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where DATE(checkin_date) >= DATE('now', 'weekday 0', '-7 days') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_month_plancount() {
        SQLiteDatabase db = this.getWritableDatabase();



        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer')  and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 30.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%m', checkin_date) = strftime('%m', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 30.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%m', checkin_date) = strftime('%m', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_quarter_plancount() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 90.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 90.0)  from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All'  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }

        return data;
    }


    public Cursor get_year_plancount() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) /  (('" + days + "') / 365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Primary' ", null);
        }
        return data;
    }

    public Cursor get_Customer_year_plancount() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(distinct customer_id),(select sum(value)/(('" + days + "') / 365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_type='Primary' ", null);
        }else{
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All'  and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(distinct customer_id),(select sum(value) / (('" + days + "') / 365.0)from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Primary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_type='Primary' ", null);
        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_current_plancount1() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;



        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where checkin_date=date('now') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        } else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "')) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where checkin_date=date('now') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_week_plancount1() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 7.0)  from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where DATE(checkin_date) >= DATE('now', 'weekday 0', '-7 days') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 7.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where DATE(checkin_date) >= DATE('now', 'weekday 0', '-7 days') and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }
        return data;
    }

    //Get Sales Rep Details
    public Cursor get_month_plancount1() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 30.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%m', checkin_date) = strftime('%m', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) /  (('" + days + "') / 30.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%m', checkin_date) = strftime('%m', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }

        return data;
    }

    //Get Sales Rep Details
    public Cursor get_quarter_plancount1() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {


            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 90.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }else {


            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 90.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }
             return data;
    }


    public Cursor get_year_plancount1() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }else {

            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select cast(sum(value) as double) / (('" + days + "') / 365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_id in (select customerCode from xxmsales_new_journey_plan_customers) and customer_type='Secondary' ", null);
        }
        return data;
    }

    public Cursor get_customer_year_plancount1() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data;
        Cursor data1 = db.rawQuery("select * from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR'  and  (date('now') BETWEEN fromDate AND toDate)", null);
        if(data1.getCount()>0) {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            data = db.rawQuery("select count(distinct customer_id),(select sum(value)/(('" + days + "') / 365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'SR' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_type='Secondary' ", null);
        }else {
            String fromDate = "",toDate="",toDate1="";
            Cursor fromCur = db.rawQuery("select fromDate,toDate from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer')   and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)", null);
            if (fromCur.getCount() > 0) {
                fromCur.moveToFirst();
                fromDate = fromCur.getString(0);
                toDate = fromCur.getString(1);
                System.out.println("TTT::fromDate = " + fromDate);
                fromCur.close();
            }

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");;
            String inputString1 = fromDate;
            String inputString2 = toDate;
            long days = 0;
            try {
                Date date1 = myFormat.parse(inputString1);
                Date date2 = myFormat.parse(inputString2);
                long diff = date2.getTime() - date1.getTime();
                days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
                System.out.println ("TTT::Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            data = db.rawQuery("select count(distinct customer_id),(select sum(value)/(('" + days + "') / 365.0) from xxmsales_metrics_target where targetType='Customer Visit' and targetValue='Count'  and (cType ='Both' OR cType ='Secondary Customer') and type = 'All' and  (date('now') BETWEEN fromDate AND toDate)) target from xxmsales_checkin_details where strftime('%Y', checkin_date) = strftime('%Y', date('now')) and customer_type='Secondary' ", null);
        }
        return data;
    }

    //Get Secondary Focus Product Target and achievement values
    public Cursor get_focus_product_achv(String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor data = db.rawQuery("select count(distinct product_id ) no_sold,sum(quantity) qty,sum(value) val,(select target from xxmsales_metrics_target where type='Secondary' and parameter_name='sold_count' and parameter_type='Focused') target_sold_count,(select target from xxmsales_metrics_target where type='Secondary' and parameter_name='sold_volume' and parameter_type='Focused') target_sold_volume,(select target from xxmsales_metrics_target where type='Secondary' and parameter_name='sold_value' and parameter_type='Focused') target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_invoice_details a,xxmsales_invoice_header b where a.invoice_id=b.invoice_id and b.invoice_status='Completed' ) rslt where item_type='Focused'", null);

        Cursor data;

        if (productNameString.equals("All")) {
            data = db.rawQuery("select count(distinct product_id ) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_invoice_details a,xxmsales_invoice_header b where a.invoice_id=b.invoice_id and b.invoice_status='Completed' ) rslt where item_type=''"
                    , null);
        }
        else{

            data = db.rawQuery("select count(product_id) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_invoice_details a,xxmsales_invoice_header b where a.invoice_id=b.invoice_id and b.invoice_status='Completed' and a.product_name = '" + productNameString + "') rslt where item_type=''"
                    , null);


        }

        return data;
    }

    //Get Secondary Scheme Product Target and achievement values
    public Cursor get_scheme_product_achv(String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();



        Cursor data;

        if (productNameString.equals("All")) {
            data = db.rawQuery("select count(distinct product_id ) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_invoice_details a,xxmsales_invoice_header b where a.invoice_id=b.invoice_id and b.invoice_status='Completed' ) rslt where item_type='Scheme'"
                    , null);
        }
        else{

            data = db.rawQuery("select count(product_id ) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Secondary Customer') and (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_invoice_details a,xxmsales_invoice_header b where a.invoice_id=b.invoice_id and b.invoice_status='Completed'  and a.product_name = '" + productNameString + "') rslt where item_type='Scheme'"
                    , null);


        }
        return data;
    }

    //Get Primary Focus Product Target and achievement values
    public Cursor get_order_focus_product_achv(String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;

        if (productNameString.equals("All")) {
            data = db.rawQuery("select count(distinct product_id ) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_order_details a,xxmsales_order_header b where a.salesorder_id=b.salesorder_id and b.salesorder_status='Completed' ) rslt where item_type=''"
                    , null);
        }
        else{

            data = db.rawQuery("select count(product_id ) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and  (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and  (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_order_details a,xxmsales_order_header b where a.salesorder_id=b.salesorder_id and b.salesorder_status='Completed' and a.product_name = '" + productNameString + "') rslt where item_type=''"
                    , null);


        }
        return data;
    }

    //Get Primary Scheme Product Target and achievement values
    public Cursor get_order_scheme_product_achv(String productNameString) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor data;

        if (productNameString.equals("All")) {
            data = db.rawQuery("select count(distinct product_id ) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_order_details a,xxmsales_order_header b where a.salesorder_id=b.salesorder_id and b.salesorder_status='Completed' ) rslt where item_type='Scheme'"
                    , null);
        }
        else{

            data = db.rawQuery("select count(product_id ) no_sold,sum(quantity) qty,sum(value) val,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Count' and (cType ='Both' OR cType ='Primary Customer') and (date('now') BETWEEN fromDate AND toDate)) target_sold_count,(select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Quantity' and (date('now') BETWEEN fromDate AND toDate)) target_sold_volume, (select sum(value) from xxmsales_metrics_target where targetType='Focus Products' and targetValue='Value' and (date('now') BETWEEN fromDate AND toDate)) target_sold_value from (select (select product_type from xxmsales_item_details c where c.product_id=a.product_id) item_type,product_id ,quantity,value from xxmsales_order_details a,xxmsales_order_header b where a.salesorder_id=b.salesorder_id and b.salesorder_status='Completed'  and a.product_name = '" + productNameString + "') rslt where item_type='Scheme'"
                    , null);


        }
        return data;
    }

    //Get Sales Rep Details
    public Cursor getinvoiceSectionDetails(String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select invoice_value,paid_amount,balance_amount,invoice_date from xxmsales_invoice_header where invoice_number = '" + invoice_id + "'"
                , null);
        return data;
    }

    public Invoices getinvoiceHeader(String invoice_id) {
        Log.d("InvoiceId", "Invoice Number" + invoice_id);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select invoice_value,paid_amount,balance_amount,invoice_date,created_at,invoice_status from xxmsales_invoice_header where invoice_number = '" + invoice_id + "'"
                , null);
        Invoices invoice = null;
        while (data.moveToNext()) {
            invoice = new Invoices();
            invoice.setValue(data.getString(0));
            invoice.setPaidAmount(data.getString(1));
            invoice.setBalanceAmount(data.getString(2));
            invoice.setDate(data.getString(3));
            invoice.setCreatedAt(data.getString(4));
            invoice.setStatus(data.getString(5));

        }
        return invoice;
    }

    public ArrayList<Invoices> getInvoiceHeaderForCustomerReturn(String customerId) {
        SQLiteDatabase db = this.getWritableDatabase();
        //String oldQuery = "select invoice_value,paid_amount,balance_amount,invoice_date,created_at,invoice_number,invoice_id from xxmsales_invoice_header where customer_id='"+customerId+"'";
        //String query = "select ih.invoice_value,ih.paid_amount,ih.balance_amount,ih.invoice_date,ih.created_at,ih.invoice_number,ih.invoice_id from xxmsales_invoice_header as ih where customer_id='"+customerId+"' and ih.id in (select sid.invoice_id from xxmsales_invoice_details as sid left join xxmsales_stock_reciept as sr on sr.sr_product_name = sid.product_id and sr.sr_invoice='Return' group by sid.product_id Having sum(sr.sr_product_quantity)<sid.quantity OR sr.sr_product_quantity IS NULL)";
        String query = "select ih.invoice_value,ih.paid_amount,ih.balance_amount,ih.invoice_date,ih.created_at,ih.invoice_number,ih.invoice_id from xxmsales_invoice_header as ih where ih.customer_id='" + customerId + "' and ih.invoice_status='Completed' and ih.invoice_id not in (select sr.invoice_id from xxmsales_stock_reciept as sr WHERE sr.sr_invoice='Return' and sr.invoice_id IS NOT NULL)";
        Cursor data = db.rawQuery(query, null);
        ArrayList<Invoices> invoicesArrayList = new ArrayList<Invoices>();
        while (data.moveToNext()) {
            Invoices invoice = new Invoices();
            invoice.setValue(data.getString(0));
            invoice.setPaidAmount(data.getString(1));
            invoice.setBalanceAmount(data.getString(2));
            invoice.setDate(data.getString(3));
            invoice.setCreatedAt(data.getString(4));
            invoice.setInvoiceCode(data.getString(5));
            invoice.setInvoiceId(data.getString(6));

            invoicesArrayList.add(invoice);
        }
        return invoicesArrayList;
    }

    public ArrayList<InvoiceProductItems> getinvoiceDetailsList(String invoiceNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select inv.product_id,inv.product_name,inv.product_uom,inv.quantity,inv.price,inv.value,inv.discount,inv.invoice_id,p.batch_controlled from xxmsales_invoice_details as inv left join xxmsales_item_details as p on p.product_id = inv.product_id where inv.invoice_number = '" + invoiceNumber + "'"
                , null);
        ArrayList<InvoiceProductItems> invoicesArrayList = new ArrayList<InvoiceProductItems>();
        while (data.moveToNext()) {
            InvoiceProductItems invoice = new InvoiceProductItems();
            invoice.setProduct_id(data.getString(0));
            invoice.setProduct(data.getString(1));
            invoice.setUom(data.getString(2));
            invoice.setQuantity(data.getString(3));
            invoice.setPrice(data.getString(4));
            invoice.setPrice_db(Float.parseFloat(data.getString(4)));
            invoice.setValue(data.getString(5));
            invoice.setValue_db(Float.parseFloat(data.getString(5)));
            invoice.setDiscount(data.getString(6));
            invoice.setInvoice_id(Integer.parseInt(data.getString(7)));
            invoice.setBatch_controlled(data.getString(8));
            invoice.setCustomerReturnBatchArrayList(this.getCustomerReturnBatchList(data.getString(7), data.getString(0)));
            invoicesArrayList.add(invoice);
        }
        return invoicesArrayList;
    }

    public ArrayList<CustomerReturnBatch> getCustomerReturnBatchList(String invoiceId, String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select b_product_id,b_batch_number,b_expiry_date,b_batch_quantity from xxmsales_product_batch where b_invoice_id = " + invoiceId + " and b_product_id='" + product_id + "'", null);
        ArrayList<CustomerReturnBatch> batchArrayList = new ArrayList<CustomerReturnBatch>();
        while (data.moveToNext()) {
            CustomerReturnBatch crb = new CustomerReturnBatch();
            crb.setProductCode(data.getString(0));
            crb.setBatchNumber(data.getString(1));
            crb.setExpiryDate(data.getString(2));
            crb.setBatchQuantity(data.getString(3));
            batchArrayList.add(crb);
        }
        return batchArrayList;
    }

    //Get Sales Rep Details
    public Cursor getPrimaryinvoiceSectionDetails(String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select invoice_amount,receipt_amount,outstanding_amount as balance_amount,invoice_date from xxmsales_ledger where invoice_number = '" + invoice_id + "'"
                , null);

        return data;
    }

    public String get_sec_price_list() {
        String sec_price_list = "0";
        String selectQuery = "select sec_price_list from xxmsales_tab_setup LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            sec_price_list = cursor.getString(0);
            cursor.close();
        }

        return sec_price_list;
    }

    public int get_trip_number() {
        int trip_num = 0;
        String selectQuery = "select * from xxmsales_trip_details where trip_date=date('now') order by trip_number desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            trip_num = cursor.getInt(1);
            cursor.close();
        }

        return trip_num;
    }

    public Cursor gettrip_starttime() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select trip_time from xxmsales_trip_details where trip_date=date('now') order by trip_number asc limit 1"
                , null);

        return data;
    }

    public Cursor get_latest_message() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_global_message order by message_creation_date desc limit 1"
                , null);

        return data;
    }

    public Cursor gettrip_count() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select count(*) from xxmsales_trip_details where trip_date=date('now') "
                , null);

        return data;
    }

    public Cursor getcustomer_count() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select count(distinct customer_id )from xxmsales_checkin_details where checkin_date=date('now') "
                , null);

        return data;
    }

    public Cursor getTransactionCount() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select (select count(*) from xxmsales_invoice_header  where invoice_date=date('now')) invoice_count , (select count(*) from xxmsales_order_header  where salesorder_date=date('now')) order_count,(select count(*) from xxmsales_collection_details  where collection_date=date('now')) sec_collection_count,(select count(*) from xxmsales_primary_collection_details  where collection_date=date('now')) primary_collection_count"
                , null);

        return data;
    }

    public String get_trip_name() {
        String trip_name = "";
        String selectQuery = "select trip_name from xxmsales_trip_details order by trip_number desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            trip_name = cursor.getString(0);
            cursor.close();
        }

        return trip_name;
    }

    public int get_sale_order_id() {
        int salesorder_id = 0;
        String selectQuery = "select random_num from xxmsales_order_header where salesorder_date=date('now') order by random_num desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_waybill_id() {
        int salesorder_id = 0;
        String selectQuery = "select random_number from xxmsales_waybill_details where waybill_date=date('now') order by random_number desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_receipt_random_num() {
        int salesorder_id = 0;
        String selectQuery = "select random_num from xxmsales_stock_reciept where sr_date=date('now') order by random_num desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_feedback_id() {
        int salesorder_id = 0;
        String selectQuery = "select random_num from xxmsales_feedback where feedback_date=date('now') order by random_num desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_trip_id() {
        int salesorder_id = 0;
        String selectQuery = "select random_num from xxmsales_trip_details where trip_date=date('now') order by random_num desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_customer_id() {
        int salesorder_id = 0;
        String selectQuery = "select random_num from xxmsales_customer_details where creation_date=date('now') order by random_num desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public double get_customer_creditLimit(String customer_id) {
        double credit_limit = 0;
        String selectQuery = "select credit_limit from xxmsales_customer_details where customer_id = '" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            credit_limit = cursor.getInt(0);
            cursor.close();
        }

        return credit_limit;
    }

    public int get_customer_creditLimit1(String customer_id) {
        int credit_limit = 0;
        String selectQuery = "select credit_limit from xxmsales_customer_details where customer_id = '" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            credit_limit = cursor.getInt(0);
            cursor.close();
        }

        return credit_limit;
    }

    public String get_customer_creditLimit2(String customer_id) {
        String credit_limit = "0";
        String selectQuery = "select credit_limit from xxmsales_customer_details where customer_id = '" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            credit_limit = cursor.getString(0);
            cursor.close();
        }

        return credit_limit;
    }

    public String get_customer_creditDays(String customer_id) {
        String credit_days = "0";
        String selectQuery = "select credit_days from xxmsales_customer_details where customer_id = '" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            credit_days = cursor.getString(0);
            cursor.close();
        }

        return credit_days;
    }


    public int get_collection_random_num(String customer_type) {

        int salesorder_id = 0;
        String selectQuery;

        if ("Secondary".equals(customer_type)) {

            selectQuery = "select random_num from xxmsales_collection_details where collection_date=date('now') order by random_num desc LIMIT 1";

        } else {

            selectQuery = "select random_num from xxmsales_primary_collection_details where collection_date=date('now') order by random_num desc LIMIT 1";

        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_order_sequence_value() {
        int salesorder_id = 0;
        String selectQuery = "select order_seq_value from xxmsales_master_sequence";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }


    public int get_sponsor_sequence_value() {
        int salesorder_id = 0;
        String selectQuery = "select sponsor_seq_value from xxmsales_master_sequence";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_sale_invoice_id() {
        int salesorder_id = 0;
        String selectQuery = "select random_num from xxmsales_invoice_header where invoice_date=date('now','localtime') order by random_num desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_invoice_sequence_value() {
        int salesorder_id = 0;
        String selectQuery = "select invoice_seq_value from xxmsales_master_sequence";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_collection_sequence_value(String customer_type) {
        int salesorder_id = 0;
        String selectQuery;

        if ("Secondary".equals(customer_type)) {

            selectQuery = "select collection_seq_value from xxmsales_master_sequence";

        } else {

            selectQuery = "select primary_collection_seq_value from xxmsales_master_sequence";

        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_gps_id() {
        int gps_id = 0;
        String selectQuery = "select * from xxmsales_gps_tracker order by gps_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            gps_id = cursor.getInt(4);
            cursor.close();
        }

        return gps_id;
    }

    public int getOrderLineId(int order_id) {
        int salesorder_id = 0;
        String selectQuery = "select salesorder_line_id from xxmsales_order_details where salesorder_id=" + order_id + " order by salesorder_line_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int get_sale_invoice_line_id(int invoice_id) {
        int salesinvoice_id = 0;
        String selectQuery = "select invoice_line_id from xxmsales_invoice_details where invoice_id=" + invoice_id + " order by invoice_line_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesinvoice_id = cursor.getInt(0);
            cursor.close();
        }

        return salesinvoice_id;
    }


    public int get_invoice_id() {
        int invoice_id = 0;
        String selectQuery = "select * from xxmsales_invoice_header order by invoice_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            invoice_id = cursor.getInt(3);
            cursor.close();
        }

        return invoice_id;
    }

    public int get_collection_id() {
        int collection_id = 0;
        String selectQuery = "select * from xxmsales_collection_details order by collection_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            collection_id = cursor.getInt(3);
            cursor.close();
        }

        return collection_id;
    }

    public int get_primary_collection_id() {
        int collection_id = 0;
        String selectQuery = "select * from xxmsales_primary_collection_details order by collection_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            collection_id = cursor.getInt(3);
            cursor.close();
        }

        return collection_id;
    }

    public int get_invoice_count(int inv_id) {
        int invoice_id = 0;
        String selectQuery = "select * from xxmsales_invoice_header where invoice_id=" + inv_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            invoice_id = cursor.getInt(3);
            cursor.close();
        }

        return invoice_id;
    }

    public int getFocusProductCount(String order_id, String customer_id) {
        int value = 0;
        String selectQuery = "select count(*) from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where (select product_type from xxmsales_item_details where product_id=a.product_code)='focus' and a.product_code not in (select product_id from xxmsales_order_details where salesorder_id=" + order_id + ") and c.price_list_code =b.price_list_code and b.price_list_code=a.price_id and c.customer_id='" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }


    public int getOrderLinesCount(String order_id) {
        String selectQuery = "select product_id from xxmsales_order_details where salesorder_id=" + order_id;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_count_master_sequence() {
        String selectQuery = "select order_seq_value from xxmsales_master_sequence";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_count_master_download() {

        String selectQuery = "select master_download from xxmsales_master_download";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int getmaster_count() {

        String selectQuery = "select sync_date from xxmsales_master_download where sync_date = date('now') ";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public double getInvoiceAmout(String invoice_num){
       /* SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select invoice_amount from xxmsales_collection_details WHERE invoice_num = ?", new String[]{invoice_num});
        double invoice_amount = 0;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            invoice_amount = Double.parseDouble(cursor.getString(0));
            cursor.close();
        }
        return invoice_amount;*/


        double invoice_amount = 0;
        String selectQuery = "select invoice_amount from xxmsales_collection_details WHERE invoice_num = '" + invoice_num + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && db.isOpen() && cursor.getCount()>0) {
            cursor.moveToFirst();
            if(cursor.getString(0) == null){
                invoice_amount = 0;
            }else
            invoice_amount = Double.parseDouble(cursor.getString(0));
            cursor.close();
        }

        return invoice_amount;
    }

    public double getCollectionDueAmout(String invoice_num){
        double payment_amt = 0;
        String selectQuery = "select SUM(payment_amount) as payment_amt from xxmsales_collection_details WHERE invoice_num = '" + invoice_num + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if ( cursor != null && db.isOpen() && cursor.getCount()>0 ) {
            cursor.moveToFirst();
            if(cursor.getString(0) == null){
                payment_amt = 0;
            }else
            payment_amt = Double.parseDouble(cursor.getString(0));
            cursor.close();
        }

        return payment_amt;

       /* SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select SUM(payment_amount) as payment_amt from xxmsales_collection_details WHERE invoice_num = ?", new String[]{invoice_num});

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            payment_amt = Double.parseDouble(cursor.getString(0));
            cursor.close();
        }
        return payment_amt;*/
    }


    public int get_template_count(String template_name, String customer_id) {

        String selectQuery = "select template_name from xxmsales_order_header where template_name='" + template_name + "'" + " and customer_id='" + customer_id + "'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }


  /*  public int get_current_salescount()

    {
        String selectQuery = "select salesorder_id from xxmsales_order_header where salesorder_date=date('now') and salesorder_status='Completed'" ;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    } */

  /*  public int get_week_salescount()

    {
        String selectQuery = "SELECT salesorder_id FROM xxmsales_order_header WHERE DATE(salesorder_date) >= DATE('now', 'weekday 0', '-7 days') and salesorder_status='Completed'" ;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_month_salescount()

    {
        String selectQuery = "SELECT salesorder_id FROM xxmsales_order_header WHERE strftime('%m', salesorder_date) = strftime('%m', date('now')) and salesorder_status='Completed'" ;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_quarter_salescount()

    {
        String selectQuery = "SELECT salesorder_id FROM xxmsales_order_header WHERE strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed'" ;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_year_salescount()

    {
        String selectQuery = "SELECT salesorder_id FROM xxmsales_order_header WHERE strftime('%Y', salesorder_date) = strftime('%Y', date('now')) and salesorder_status='Completed'" ;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    } */

    public int check_draft_order(String customer_id) {
        int salesorder_id = 0;
        String selectQuery = "select salesorder_id from xxmsales_order_header where customer_id='" + customer_id + "'" + " and salesorder_status='Draft'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public String check_draft_order_number(String customer_id) {
        String salesorder_id = "";
        String selectQuery = "select salesorder_number from xxmsales_order_header where customer_id='" + customer_id + "'" + " and salesorder_status='Draft'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getString(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public int check_draft_invoice(String customer_id) {
        int salesorder_id = 0;
        String selectQuery = "select invoice_id from xxmsales_invoice_header where customer_id='" + customer_id + "'" + " and invoice_status='Secondary Sales Order'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getInt(0);
            cursor.close();
        }

        return salesorder_id;
    }

    public String check_draft_invoice_number(String customer_id) {
        String salesorder_id = "";
        String selectQuery = "select invoice_number from xxmsales_invoice_header where customer_id='" + customer_id + "'" + " and invoice_status='Secondary Sales Order'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            salesorder_id = cursor.getString(0);
            cursor.close();
        }

        return salesorder_id;
    }


   /* public int check_draft_order( String customer_id)

    {
        String selectQuery = "select count(*) from xxmsales_order_header where customer_id=" + customer_id + " and salesorder_status='Draft'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        int value=0;
        if (c.getCount() > 0) {
            c.moveToFirst();
            value = c.getInt(0);
            c.close();
        }

        return value;
    } */

    public int getProductLinesCount(int order_id, String product_id) {
        String selectQuery = "select product_id from xxmsales_order_details where salesorder_id=" + order_id + " and product_id='" + product_id + "'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int getstockonhand(String product_id) {
        int value = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select sum(sr_product_quantity) from xxmsales_stock_reciept where status='Completed' and sr_product_name= '" + product_id + "'" + " group by sr_product_name" +
                        ""
                , null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public ArrayList<StockReceiptBatch> getReceiptBatchProducts(String productCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT b_expiry_date, b_batch_number, b_product_id, sum(b_batch_quantity) FROM " + TABLE_PRODUCT_BATCH
                + " WHERE b_product_id=? group by b_batch_number having sum(b_batch_quantity) > 0 ORDER BY b_expiry_date ";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{productCode});
        Log.d("Data", "Get Products  ");
        ArrayList<StockReceiptBatch> saleBatchProducts = new ArrayList<>();

        while (cursor.moveToNext()) {
            StockReceiptBatch saleBatch = new StockReceiptBatch();
            saleBatch.setExpiryDate(cursor.getString(0));
            saleBatch.setBatchNumber(cursor.getString(1));
            saleBatch.setProductCode(cursor.getString(2));
            saleBatch.setStockQuantity(cursor.getString(3));
            saleBatchProducts.add(saleBatch);
        }
        cursor.close();
        sqlWrite.close();
        return saleBatchProducts;
    }

    public Cursor getstock_viewinvoice(String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select b_expiry_date,b_batch_number,sum(b_batch_quantity), CASE WHEN Date(b_expiry_date)<=Date('now') THEN 'TRUE' ELSE 'FALSE' END isExpired from xxmsales_product_batch where b_batch_status='Completed' and b_product_id= '" + product_id + "'" + " group by b_product_id,b_batch_number having sum(b_batch_quantity)!=0", null);
        return cursor;
    }

    public ArrayList<StockIssueBatch> getStockIssueBatchList(String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select b_expiry_date,b_batch_number,sum(b_batch_quantity), CASE WHEN Date(b_expiry_date)<=Date('now') THEN 'TRUE' ELSE 'FALSE' END isExpired from xxmsales_product_batch where b_batch_status='Completed' and b_product_id= '" + product_id + "'" + " group by b_product_id,b_batch_number having sum(b_batch_quantity)!=0", null);
        ArrayList<StockIssueBatch> stockReceiptBatchArrayList = new ArrayList<StockIssueBatch>();
        while (data.moveToNext()) {
            StockIssueBatch srp = new StockIssueBatch();
            srp.setExpiryDate(data.getString(0));
            srp.setBatchNumber(data.getString(1));
            srp.setBatchQuantity(data.getString(2));
            stockReceiptBatchArrayList.add(srp);
        }
        return stockReceiptBatchArrayList;
    }

    public ArrayList<SrInvoiceBatch> getWarehouseStockBatchList(String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select b_expiry_date,b_batch_number,sum(b_batch_quantity) from xxmsales_sr_invoice_batch where b_product_id= '" + product_id + "'" + " group by b_product_id,b_batch_number having sum(b_batch_quantity)!=0", null);
        ArrayList<SrInvoiceBatch> stockReceiptBatchArrayList = new ArrayList<SrInvoiceBatch>();
        while (data.moveToNext()) {
            SrInvoiceBatch srp = new SrInvoiceBatch();
            srp.setExpiryDate(data.getString(0));
            srp.setBatchName(data.getString(1));
            srp.setBatchQuantity(data.getString(2));
            stockReceiptBatchArrayList.add(srp);
        }
        return stockReceiptBatchArrayList;
    }

    public Cursor getstock_viewinvoiceByExpiry(String product_id, boolean isExpired) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select b_expiry_date,b_batch_number,sum(b_batch_quantity) from xxmsales_product_batch where b_batch_status='Completed' and b_product_id= '" + product_id + "'";
        if (isExpired) {
            query += " and Date(b_expiry_date)<=Date('now')";
        } else {
            query += " and Date(b_expiry_date)>Date('now')";
        }
        query += " group by b_product_id,b_batch_number having sum(b_batch_quantity)>0";
        Cursor cursor = db.rawQuery(query, null);
        return cursor;
    }

    public Cursor getstock_vieworder(String sales_rep_id, String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery
                ("select  b.product_quantity," +
                        "(select a.warehouse_name from xxmsales_warehouse a \n" +
                        "where a.warehouse_code = b.warehouse_id) s_warehouse_name" +
                        " from xxmsales_warehouse_stock_product b " +
                        "where b.product_id='" + product_id + "'", null);
        return data;
    }

    public int get_count_invoiceproduct_lines(int invoice_id, String product_id) {
        String selectQuery = "select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + " and product_id='" + product_id + "'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_count_batchlines(int invoice_id, String product_id) {
        String selectQuery = "select count(*) from xxmsales_product_batch where b_invoice_id=" + invoice_id + " and b_product_id='" + product_id + "'" + " and b_batch_status='Inprocess'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        int value = 0;
        if (c.getCount() > 0) {
            c.moveToFirst();
            value = c.getInt(0);
            c.close();
        }

        return value;
    }

    public int get_count_invoice_lines(String invoice_id) {
        String selectQuery = "select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int getWaybillCount(String waybillQuantity) {
        String selectQuery = "select product_id from xxmsales_waybill_details where waybill_quantity=" + waybillQuantity;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_count_check_number(String check_number, String customer_type) {
        String selectQuery;
        if ("Secondary".equals(customer_type)) {
            selectQuery = "select collection_id from xxmsales_collection_details where check_number='" + check_number + "'";

        } else {
            selectQuery = "select collection_id from xxmsales_primary_collection_details  where check_number='" + check_number + "'";

        }
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_count_batch(String invoice_id, String product_id) {
        String selectQuery = "select b_product_id from xxmsales_product_batch where b_invoice_id=" + invoice_id + " and b_product_id='" + product_id + "'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();
        return total;
    }


    public double getOrderLinesValue(String order_id) {
        double value = 0.00;
        String selectQuery = "select sum(value) value from xxmsales_order_details where salesorder_id=" + order_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }


    public String get_mileage_data() {
        String value = "";
        String selectQuery = "select value from xxmsales_general_settings where configuration_key='mileage_data'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }

    public String get_master_download_value() {
        String value = "";
        String selectQuery = "select master_download from xxmsales_master_download";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }


    public String getMDLSyncDate() {
        String value = "";
        String selectQuery = "select sync_date from xxmsales_master_download";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(value);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");
            value = format.format(date1);
        } catch (Exception e) {

        }
        return value;
    }

    public String getMDLSyncTime() {
        String value = "";
        String selectQuery = "select sync_time from xxmsales_master_download";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }


    public double get_primary_applied_value() {
        double value = 0.00;
        String selectQuery = "select sum(payment_amount) value from xxmsales_primary_collection_details where collection_type='Against Invoice' ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public double get_applied_value(String customer_type) {
        double value = 0.00;
        String selectQuery;

        if ("Primary".equals(customer_type)) {
            selectQuery = "select sum(payment_amount) value from xxmsales_primary_collection_details where collection_type='Against Invoice' ";

        } else {
            selectQuery = "select sum(payment_amount) value from xxmsales_collection_details where collection_type='Against Invoice' ";
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public double get_unapplied_value(String customer_type) {
        double value = 0.00;
        String selectQuery;

        if ("Primary".equals(customer_type)) {
            selectQuery = "select sum(payment_amount) value from xxmsales_primary_collection_details where collection_type='onAccount' ";

        } else {
            selectQuery = "select sum(payment_amount) value from xxmsales_collection_details where collection_type='onAccount' ";
        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }


    public String get_ideal_time() {
        String value = "";
        String selectQuery = "select substr(time((trip_total_time-checkin_total_time),'unixepoch'),0,6) ||' HRS' ideal_time  from (select (select IFNULL(sum(strftime('%s', trip_stop_time) - strftime('%s', trip_time)),0) trip_difference from xxmsales_trip_details where trip_stop_time is not null and trip_time is not null and trip_date=date('now') ) trip_total_time,(select IFNULL(sum(strftime('%s', checkout_time) - strftime('%s', checkin_time)),0) checkin_difference from xxmsales_checkin_details where checkin_time is not null and checkout_time is not null and checkin_date=date('now') ) checkin_total_time)";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }

    public double get_outstanding_value(String customer_id) {
        double value = 0;
        String selectQuery = "select sum(balance_amount) value from xxmsales_invoice_header where customer_id='" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public String get_order_type(String product_id) {
        String value = "";
        String selectQuery = "select product_type from xxmsales_item_details where product_id='" + product_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }

    public boolean checkProductIsBatchControlled(String product_id) {
        String value = "";
        String selectQuery = "select batch_controlled from xxmsales_item_details where product_id='" + product_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }
        if ("Yes".equals(value)) {
            return true;
        }
        return false;
    }

    public int getYTDOrdersCount(String customer_id) {
        String selectQuery = "select salesorder_number from xxmsales_order_header where customer_id='" + customer_id + "'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int getYTDOrderProductQuantity(String customer_id, String product_id) {
        int value = 0;
        String selectQuery = "select sum(quantity) value from xxmsales_order_details where customer_id='" + customer_id + "'" + " and product_id='" + product_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public double getYTDOrderProductPrice(String customer_id, String product_id) {
        double value = 0;
        String selectQuery = "select avg_price/row_count avg_price from (select count(*) row_count,sum(price) avg_price from xxmsales_order_details where customer_id='" + customer_id + "'" + " and product_id='" + product_id + "')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public double getYTDOrderProductValue(String customer_id, String product_id) {
        double value = 0;
        String selectQuery = "select sum(value) value from xxmsales_order_details where customer_id='" + customer_id + "'" + " and product_id='" + product_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public int get_ytd_invoices_sec(String customer_id) {
        String selectQuery = "select invoice_number from xxmsales_invoice_header where customer_id='" + customer_id + "'";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public double get_ytd_price_sec(String customer_id, String product_id) {
        double value = 0;
        String selectQuery = "select avg_price/row_count avg_price from (select count(*) row_count,sum(price) avg_price from xxmsales_invoice_details where customer_id='" + customer_id + "'" + " and product_id='" + product_id + "')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public int get_ytd_quantity_sec(String customer_id, String product_id) {
        int value = 0;
        String selectQuery = "select sum(quantity) value from xxmsales_invoice_details where customer_id='" + customer_id + "'" + " and product_id='" + product_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public double get_ytd_value_sec(String customer_id, String product_id) {
        double value = 0;
        String selectQuery = "select sum(value) value from xxmsales_invoice_details where customer_id='" + customer_id + "'" + " and product_id='" + product_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }


    public double get_invoice_value(String invoice_id) {
        double value = 0;
        String selectQuery = "select sum(value) value from xxmsales_invoice_details where invoice_id=" + invoice_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public int get_database_version() {
        int value = 0;
        String selectQuery = "select database_version value from xxmsales_database_version";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public void execute_alter_script() {
        SQLiteDatabase db = this.getReadableDatabase();

        try {
            db.execSQL("ALTER TABLE xxmsales_checkin_details ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_order_header ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_order_header ADD order_time TEXT");


        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_invoice_header ADD visitSeqNo TEXT");

        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_invoice_header ADD returnQuantity INTEGER");

        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_collection_details ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_collection_details ADD transactionType TEXT");


        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_primary_collection_details ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_dr_visit_sponsorship ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_stock_reciept ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_customer_details ADD visitSeqNo TEXT");

        } catch (Exception e) {

        }

        try {

            db.execSQL("ALTER TABLE xxmsales_feedback ADD visitSeqNo TEXT");

        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_doctor_prescribed_products ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_lpo_images ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_notes ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_pharmacy_info ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_survey_response_answer ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_survey_response ADD visitSeqNo TEXT");


        } catch (Exception e) {

        }


        try {
            db.execSQL("ALTER TABLE xxmsales_customer_stock_product ADD visitSeqNo TEXT");

        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_master_download ADD sync_date TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_master_download ADD sync_time TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_item_details ADD conversion NUMERIC");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_global_message ADD message_creation_date TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_order_header ADD signature_name TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_stock_reciept ADD referenceNumber TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_stock_reciept ADD transferredTo TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_user_setup ADD imageUrl TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_user_setup ADD imageName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_stock_return_products ADD unitPrice NUMERIC");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_stock_return_products ADD returnValue NUMERIC");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_stock_return ADD signeeName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_customer_return ADD signeeName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_stock_issue ADD signeeName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_invoice_header ADD signeeName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_collection_details ADD signeeName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_primary_collection_details ADD signeeName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_waybill_details ADD signeeName TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_tab_setup ADD activate_flag INTEGER DEFAULT 1");



        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_tab_setup ADD imei_no TEXT");


        } catch (Exception e) {

        }
        try {
            db.execSQL("ALTER TABLE xxmsales_customer_details ADD distributor_name TEXT");


        } catch (Exception e) {

        }

        try {
            db.execSQL("ALTER TABLE xxmsales_customer_details ADD distributor_code TEXT");

        } catch (Exception e) {

        }

    }

    public double get_paid_value(String invoice_id) {
        double value = 0;
        String selectQuery = "select paid_amount from xxmsales_invoice_header where invoice_id=" + invoice_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public double get_balance_value(String invoice_id) {
        double value = 0;
        String selectQuery = "select balance_amount from xxmsales_invoice_header where invoice_id=" + invoice_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }




   /* public int get_invoice_id(String product_id) {
        int value = 0;
        String selectQuery = "select sum(value) value from xxmsales_invoice_details where invoice_id=" + invoice_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    } */

    public int get_discount_value(String invoice_id) {
        int value = 0;
        String selectQuery = "select sum(discount_value) value from xxmsales_invoice_details where invoice_id=" + invoice_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public double get_total_value(String invoice_id) {
        double value = 0;
        String selectQuery = "select sum(value_without_discount) value from xxmsales_invoice_details where invoice_id=" + invoice_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public Cursor getSupportDetails() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_company_setup order by Id desc limit 1 "
                , null);

        return data;
    }
    public String get_company_name() {
        String value = "";
        String selectQuery = "select company_name from xxmsales_company_setup order by Id desc limit 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }


    public String get_company_code() {
        String value = "";
        String selectQuery = "select short_name from xxmsales_company_setup order by Id desc limit 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }

    public void update_tab_flag(int flag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ACTIVATE_FLAG, flag);

        db.update(TABLE_TAB_SETUP, values, null, null);
        db.close();
    }




    public double get_customer_order_value(String customer_id) {
        double value = 0;
        String selectQuery = "select sum(salesorder_value) value from xxmsales_order_header where customer_id = '" + customer_id + "'" + " and salesorder_status IN ('Completed')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public double get_customer_invoice_value(String customer_id) {
        double value = 0;
        String selectQuery = "select sum(invoice_value) value from xxmsales_invoice_header where customer_id = '" + customer_id + "'" + " and invoice_status = 'Completed'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getDouble(0);
            cursor.close();
        }

        return value;
    }

    public int get_customer_order_count(String customer_id) {
        String selectQuery = "select salesorder_id from xxmsales_order_header where customer_id = '" + customer_id + "'" + " and salesorder_status IN ('Completed') ";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_customer_invoice_count(String customer_id) {
        String selectQuery = "select invoice_id from xxmsales_invoice_header where customer_id = '" + customer_id + "'" + " and invoice_status in ('Completed') ";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public int get_customer_collection_count(String customer_id) {
        String selectQuery = "SELECT invoice_number FROM " + TABLE_INVOICE_HEADER + " where customer_id='" + customer_id + "'" + " and invoice_status='Completed' and balance_amount <> 0 ";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor c = database.rawQuery(selectQuery, null);
        c.moveToFirst();
        int total = c.getCount();
        c.close();

        return total;
    }

    public void update_order_header(String order_id, double value, String order_lines) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALESORDER_VALUE, value);
        values.put(COLUMN_SALESORDER_LINES, order_lines);
        db.update(TABLE_SALESORDER_HEADER, values, COLUMN_SALESORDER_ID + " = ?", new String[]{order_id});
        db.close();
    }

    public void update_order_header(String order_id, double value, String order_lines, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALESORDER_VALUE, value);
        values.put(COLUMN_SALESORDER_LINES, order_lines);
        values.put(COLUMN_SIGNATURE, signature);
        db.update(TABLE_SALESORDER_HEADER, values, COLUMN_SALESORDER_ID + " = ?", new String[]{order_id});
        db.close();
    }

    public void update_order_sequence(int order_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ORDER_SEQ_VALUE, order_id);
        db.update(TABLE_MASTER_SEQUENCE, values, null, null);
        db.close();
    }

    public void update_invoice_sequence(int invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_INVOICE_SEQ_VALUE, invoice_id);
        db.update(TABLE_MASTER_SEQUENCE, values, null, null);
        db.close();
    }

    public void update_collection_sequence(int collection_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_COLLECTION_SEQ_VALUE, collection_id);
        db.update(TABLE_MASTER_SEQUENCE, values, null, null);
        db.close();
    }

    public void update_primary_collection_sequence(int collection_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_PRIMARY_COLLECTION_SEQ_VALUE, collection_id);
        db.update(TABLE_MASTER_SEQUENCE, values, null, null);
        db.close();
    }

    public void update_sponsorship_id_sequence(int sponsorship_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SPONSOR_SEQ_VALUE, sponsorship_id);
        db.update(TABLE_MASTER_SEQUENCE, values, null, null);
        db.close();
    }


    public void update_user_setup(String password, String confirm_password, String mobile_number) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_PASSWORD, password);
        values.put(COLUMN_USER_CONFIRM_PASSWORD, confirm_password);
        values.put(COLUMN_USER_MOBILE_NUMBER, mobile_number);

        db.update(TABLE_USER_SETUP, values, null, null);
        db.close();
    }

    public void update_invoice_header(String invoice_id, String value, String invoice_lines) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_INVOICE_VALUE, value);
        values.put(COLUMN_INVOICE_LINES, invoice_lines);
        db.update(TABLE_INVOICE_HEADER, values, COLUMN_INVOICE_ID + " = ?", new String[]{invoice_id});
        db.close();
    }

    public void updateInvoiceCustomerSignature(String invoice_id, double value, String invoice_lines, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_INVOICE_VALUE, value);
        values.put(COLUMN_INVOICE_LINES, invoice_lines);
        values.put(COLUMN_INVOICE_SIGNATURE, signature);
        db.update(TABLE_INVOICE_HEADER, values, COLUMN_INVOICE_ID + " = ?", new String[]{invoice_id});
        db.close();
    }


    public void update_invoice_balance(String invoice_id, double balance_amt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_BALANCE_AMOUNT, balance_amt);
        db.update(TABLE_INVOICE_HEADER, values, COLUMN_INVOICE_NUMBER + " = ?", new String[]{invoice_id});
        db.close();
    }

    public void update_order_header_status(String order_id, String lpo_number, String remarks, String status, String template_name, int template_id, String ship_address1, String ship_address2, String delivery_date, String signature_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALESORDER_STATUS, status);
        values.put(COLUMN_LPO_NUMBER, lpo_number);
        values.put(COLUMN_REMARKS, remarks);
        values.put(COLUMN_TEMPLATE_NAME, template_name);
        values.put(COLUMN_TEMPLATE_ID, template_id);
        values.put(COLUMN_C_SHIP_ADDRESS1, ship_address1);
        values.put(COLUMN_C_SHIP_ADDRESS2, ship_address2);
        values.put(COLUMN_DELIVERY_DATE, delivery_date);
        values.put(COLUMN_ORDERS_SIGNATURE_NAME, signature_name);


        db.update(TABLE_SALESORDER_HEADER, values, COLUMN_SALESORDER_ID + " = ?", new String[]{order_id});

        ContentValues order_values = new ContentValues();
        order_values.put(COLUMN_SALESORDER_STATUS, status);
        db.update(TABLE_SALESORDER_DETAILS, order_values, COLUMN_SALESORDER_ID + " = ?", new String[]{order_id});
        db.close();

    }


    public void update_order_header_status_from_waybill(String order_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_order_header set salesorder_status='Completed' where salesorder_number= '" + order_id + "'");
        db.close();

    }


    public void update_order_header_info(String order_id, String lpo_number, String remarks, String delivery_date, String ship_address1, String ship_address2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_LPO_NUMBER, lpo_number);
        values.put(COLUMN_REMARKS, remarks);
        values.put(COLUMN_DELIVERY_DATE, delivery_date);
        values.put(COLUMN_C_SHIP_ADDRESS1, ship_address1);
        values.put(COLUMN_C_SHIP_ADDRESS2, ship_address2);
        db.update(TABLE_SALESORDER_HEADER, values, COLUMN_SALESORDER_ID + " = ?", new String[]{order_id});
        db.close();
    }



    public void updateOrderSignatureimage(String order_number, String filePath, String fileName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ORDERS_SIGNATURE_PATH, filePath);
        values.put(COLUMN_ORDERS_SIGNATURE_IMAGE_NAME, fileName);
        db.update(TABLE_SALESORDER_HEADER, values, COLUMN_SALESORDER_NUMBER + " = ?", new String[]{order_number});
        db.close();
    }

    public void updateInvoiceSignatureimage(String invoice_number, String filePath, String fileName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_INVOICE_SIGNATURE_PATH, filePath);
        values.put(COLUMN_INVOICE_SIGNATURE_IMAGE_NAME, fileName);
        db.update(TABLE_INVOICE_HEADER, values, COLUMN_INVOICE_NUMBER + " = ?", new String[]{invoice_number});
        db.close();
    }

    public void updateSecCollectionSignatureimage(String collection_number, String filePath, String fileName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CL_SIGNATURE_PATH, filePath);
        values.put(COLUMN_CL_SIGNATURE_IMAGE_NAME, fileName);
        db.update(TABLE_COLLECTION_DETAILS, values, COLUMN_CL_COLLECTION_NUMBER + " = ?", new String[]{collection_number});
        db.close();
    }

    public void updatePrimCollectionSignatureimage(String collection_number, String filePath, String fileName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_P_CL_SIGNATURE_PATH, filePath);
        values.put(COLUMN_P_CL_SIGNATURE_IMAGE_NAME, fileName);
        db.update(TABLE_PRIMARY_COLLECTION_DETAILS, values, COLUMN_P_CL_COLLECTION_NUMBER + " = ?", new String[]{collection_number});
        db.close();
    }


    public void update_order_detail_status(String order_id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALESORDER_STATUS, status);
        db.update(TABLE_SALESORDER_DETAILS, values, COLUMN_SALESORDER_ID + " = ?", new String[]{order_id});
        db.close();
    }

    public void update_invoice_detail_status(String invoice_id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_INVOICE_STATUS, status);
        db.update(TABLE_INVOICE_DETAILS, values, COLUMN_ID_INVOICE_ID + " = ?", new String[]{invoice_id});
        db.close();
    }

    public void update_batch_status(String invoice_id, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_INVOICE_STATUS, status);
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_ID_INVOICE_ID + " = ?", new String[]{invoice_id});
        db.close();
    }

    public void update_invoice_header_status(String invoice_id, String status, double paid_amount, double balance_amount, String change_type, double invoice_value, String signneeName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_INVOICE_STATUS, status);
        values.put(COLUMN_ID_PAID_AMOUNT, paid_amount);
        values.put(COLUMN_ID_BALANCE_AMOUNT, balance_amount);
        values.put(COLUMN_ID_CHANGE_TYPE, change_type);
        values.put(COLUMN_ID_INVOICE_VALUE, invoice_value);
        values.put(COLUMN_INVOICE_SIGNEE_NAME, signneeName);


        db.update(TABLE_INVOICE_HEADER, values, COLUMN_INVOICE_ID + " = ?", new String[]{invoice_id});
        db.close();
    }

    public void update_waybill_status(String orderNumber, String status, String waybill_num, String waybill_date, String signeeName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_DC_WAYBILL_STATUS, status);
        values.put(COLUMN_DC_WAYBILL_NUMBER, waybill_num);
        values.put(COLUMN_DC_WAYBILL_DATE, waybill_date);
        values.put(COLUMN_WAYBILL_SIGNEE_NAME, signeeName);


        db.update(TABLE_WAYBILL_DETAILS, values, COLUMN_DC_SALESORDER_NUMBER + " = ?", new String[]{orderNumber});
        db.close();
    }

    public void update_invoice_batch_status(String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_STATUS, "Completed");
        db.update(TABLE_PRODUCT_BATCH, values, COLUMN_B_INVOICE_ID + " = ?", new String[]{invoice_id});
        db.close();
    }

    public void update_checkin_details(String cout_date, String cout_time, String cout_gps_lat, String cout_gps_long, String check_id, long duration) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CHECKOUT_DATE, cout_date);
        values.put(COLUMN_CHECKOUT_TIME, cout_time);
        values.put(COLUMN_CHECKOUT_GPS_LAT, cout_gps_lat);
        values.put(COLUMN_CHECKOUT_GPS_LONG, cout_gps_long);
        values.put(COLUMN_CHECKOUT_DURATION, duration);

        db.update(TABLE_CHECKIN_DETAILS, values, COLUMN_CHECK_ID + " = ?", new String[]{check_id});
        db.close();
    }

    //Get Customer Details
    public Cursor getCustomerDetails(String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_customer_details where customer_id = '" + customer_id + "'", null);

        return data;
    }

    //Get Customer Details
    public Cursor getCustomerShipDetails(String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_customer_shipping_address where customer_id = '" + customer_id + "'", null);

        return data;
    }


    //Get Customer Details
    public Cursor getCustomerDetailsofSalesRep(String bde_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select id,customer_id,customer_name,customer_type,location_name,city,mobile_number,email,case customer_type when 'Primary' then 1  when 'Secondary' then 2  when 'Doctor' then 3 else 4 end as customer_status from xxmsales_customer_details order by customer_status asc,customer_name asc"
                , null);

        return data;
    }

    //Get Customer Details
    public Cursor getOrderDetails(String bde_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select salesorder_date,salesorder_number,customer_id,null state,null city,null line_items,salesorder_value,salesorder_status,'B' order_data,salesorder_id from xxmsales_order_header where sales_rep_id = '" + bde_code + "'" + " order by order_data"
                , null);

        return data;
    }

    //Get Customer Details
    public Cursor getOrderReportDetails(String bde_code, String customer_id_param) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select a.salesorder_date,a.salesorder_number,(select b.customer_name from xxmsales_customer_details b where b.customer_id=a.customer_id),(select b.state from xxmsales_customer_details b where b.customer_id=a.customer_id) state,(select b.city from xxmsales_customer_details b where b.customer_id=a.customer_id) city,(select count(salesorder_id) from xxmsales_order_details c where c.salesorder_id=a.salesorder_id ) line_items,a.salesorder_value,(case a.salesorder_status WHEN 'Completed' THEN 'Entered' ELSE a.salesorder_status END) salesorder_status,a.salesorder_id,a.customer_id from xxmsales_order_header a where (:customer_id_param is null or customer_id='" + customer_id_param + "')" + " and  salesorder_status <> 'Template' " + " order by salesorder_date desc,salesorder_number desc"
                , null);

        return data;
    }

    //Get Customer Details
    public Cursor getWaybillReportDetails(String bde_code, String customer_id_param) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select a.salesorder_date,a.salesorder_number,(select b.customer_name from xxmsales_customer_details b where b.customer_id=a.customer_id),null state,null city,sum(a.waybill_quantity),sum(a.value),a.waybill_status salesorder_status,a.salesorder_id,a.customer_id from xxmsales_waybill_details a where (:customer_id_param is null or a.customer_id='" + customer_id_param + "')" + " group by a.salesorder_number order by a.salesorder_date desc,a.salesorder_number desc"
                , null);

        return data;
    }

    //Get Customer Details
    public Cursor getDoctorvisitSponsorship(String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select * from " + TABLE_DOCTOR_VISIT_SPONSORSHIP + " where customer_id = '"+customer_id+"' order by sponsorship_id desc"
                , null);

        return data;
    }
    public  void deleteDoctorvisitSponsorship (int id) {

        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        sqlWrite.delete(TABLE_DOCTOR_VISIT_SPONSORSHIP, "id="+String.valueOf(id), null);
        sqlWrite.close();
    }
    //Get Customer Details
    public Cursor getOrderService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,null,a.sales_rep_id,a.customer_id,a.salesorder_id,a.salesorder_number,a.salesorder_date,a.salesorder_value,a.salesorder_lines,a.lpo_number,a.remarks,a.delivery_date,a.ship_address1,a.ship_address2,b.product_id,b.product_name,b.product_uom,b.quantity,b.price,b.value from xxmsales_order_header a,xxmsales_order_details b where a.salesorder_id=b.salesorder_id LIMIT 2"
                , null);

        return data;
    }


    /**
     * Fetching the Total number of Orders from Order header table and returning the result as a ArrayList
     */


    public ArrayList<Orders> getOrdersService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,salesorder_id,salesorder_number,salesorder_date,salesorder_value,(case salesorder_status WHEN 'Completed' THEN 'Entered' ELSE salesorder_status END) salesorder_status,salesorder_lines,lpo_number,remarks,delivery_date,ship_address1,ship_address2,signature,visitseqno,signatureFilePath , signature_imagename from xxmsales_order_header where (salesorder_status ='Completed' OR salesorder_status='Customer Order') and salesorder_date >= date('now','-5 Days')"
                , null);
        ArrayList<Orders> ordersArrayList = new ArrayList<Orders>();
        while (data.moveToNext()) {
            Orders orders = new Orders();
            orders.setTabId(tab_code);
            orders.setCompanyCode(company_code);
            orders.setSrCode(sr_code);
            orders.setCustomerCode(data.getString(2));
            orders.setOrderId(data.getString(3));
            orders.setOrderCode(data.getString(4));
            orders.setDate(data.getString(5));
            orders.setValue(data.getString(6));
            orders.setStatus(data.getString(7));
            orders.setLineItems(data.getString(8));
            orders.setLpoNumber(data.getString(9));
            orders.setRemarks(data.getString(10));
            orders.setDeliveryDate(data.getString(11));
            orders.setShipAddress1(data.getString(12));
            orders.setShipAddress2(data.getString(13));
            orders.setCustomerSignature(data.getString(14));
            orders.setVisitSequenceNumber(data.getString(15));
            orders.setSignatureFilePath(data.getString(16));
            orders.setSignatureFileName(data.getString(17));

            ordersArrayList.add(orders);
        }
        return ordersArrayList;
    }

    public ArrayList<Orders> getOrderSignImageService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select salesorder_number,salesorder_date,signatureFilePath  from xxmsales_order_header where isImageUploaded=0 and signatureFilePath NOT NULL"
                , null);
        ArrayList<Orders> ordersArrayList = new ArrayList<Orders>();
        while (data.moveToNext()) {
            Orders orders = new Orders();
            orders.setTabId(tab_code);
            orders.setCompanyCode(company_code);
            orders.setSrCode(sr_code);

            orders.setOrderCode(data.getString(0));
            orders.setDate(data.getString(1));
            orders.setSignatureFilePath(data.getString(2));


            ordersArrayList.add(orders);

        }
        return ordersArrayList;
    }

    public void updateOrderSignUploadFlag(String salesorder_number, String salesorder_date) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_order_header set isImageUploaded='1' where salesorder_number= " + salesorder_number + " and salesorder_date='" + salesorder_date + "'");
        db.close();

    }

    /**
     * Fetching the Total number of Order Item Products from Order detail table and returning the result as a ArrayList
     */
    public ArrayList<OrderItems> getOrderItemsService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,a.sales_rep_id,a.customer_id,a.salesorder_id,a.salesorder_number,a.salesorder_date,a.salesorder_line_id,(case a.salesorder_status WHEN 'Completed' THEN 'Entered' ELSE a.salesorder_status END) salesorder_status,a.product_id,a.product_name,a.product_uom,a.quantity,a.price,a.value from xxmsales_order_details a,xxmsales_order_header b where a.salesorder_number=b.salesorder_number and b.salesorder_status='Completed' and a.salesorder_date >= date('now','-5 Days') "
                , null);
        ArrayList<OrderItems> orderItemsArrayList = new ArrayList<OrderItems>();
        while (data.moveToNext()) {
            OrderItems orderItems = new OrderItems();
            orderItems.setTabId(tab_code);
            orderItems.setCompanyCode(company_code);
            orderItems.setSrCode(sr_code);
            orderItems.setCustomerCode(data.getString(2));
            orderItems.setOrderId(data.getString(3));
            orderItems.setOrderCode(data.getString(4));
            orderItems.setOrderDate(data.getString(5));
            orderItems.setOrderLineId(data.getString(6));
            orderItems.setStatus(data.getString(7));
            orderItems.setProductCode(data.getString(8));
            orderItems.setProductName(data.getString(9));
            orderItems.setProductUom(data.getString(10));
            orderItems.setQuantity(data.getString(11));
            orderItems.setPrice(data.getString(12));
            orderItems.setValue(data.getString(13));

            orderItemsArrayList.add(orderItems);
        }
        return orderItemsArrayList;

    }

    /**
     * Fetching the Total number of Order Item Products from Order detail table and returning the result as a ArrayList
     */
    public ArrayList<WaybillDetails> getWaybillDetails() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,salesorder_id,salesorder_number,salesorder_date,salesorder_line_id,(case salesorder_status WHEN 'Completed' THEN 'Entered' ELSE salesorder_status END) salesorder_status,product_id,product_name,product_uom,quantity,price,value,waybill_number,waybill_date,waybill_quantity from xxmsales_waybill_details where waybill_status='Completed' and waybill_date >= date('now','-5 Days') "
                , null);
        ArrayList<WaybillDetails> waybillDetailsArrayList = new ArrayList<WaybillDetails>();
        while (data.moveToNext()) {
            WaybillDetails waybillDetails = new WaybillDetails();
            waybillDetails.setTabId(tab_code);
            waybillDetails.setCompanyCode(company_code);
            waybillDetails.setSrCode(sr_code);
            waybillDetails.setCustomerCode(data.getString(2));
            waybillDetails.setOrderId(data.getString(3));
            waybillDetails.setOrderCode(data.getString(4));
            waybillDetails.setOrderDate(data.getString(5));
            waybillDetails.setOrderLineId(data.getString(6));
            waybillDetails.setStatus(data.getString(7));
            waybillDetails.setProductCode(data.getString(8));
            waybillDetails.setProductName(data.getString(9));
            waybillDetails.setProductUom(data.getString(10));
            waybillDetails.setQuantity(data.getString(11));
            waybillDetails.setPrice(data.getString(12));
            waybillDetails.setValue(data.getString(13));
            waybillDetails.setWaybillNumber(data.getString(14));
            waybillDetails.setWaybillDate(data.getString(15));
            waybillDetails.setWaybillQuantity(data.getString(16));

            waybillDetailsArrayList.add(waybillDetails);
        }
        return waybillDetailsArrayList;

    }

    //Get Customer Details
    public Cursor getGPSdata() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select  null,null,null,gps_id,gps_date,gps_time,gps_latitude,gps_longitude from xxmsales_gps_tracker where is_sync is null"
                , null);

        return data;
    }

    /**
     * Fetching the Total number of Invoices from Invoice header table and returning the result as a ArrayList
     */
    public ArrayList<Invoices> getInvoicesService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,invoice_id,invoice_number,invoice_date,invoice_status,invoice_value,invoice_lines,paid_amount,balance_amount,change_type,visitseqno,signatureFilePath , signature_imagename from xxmsales_invoice_header where invoice_status='Completed' and invoice_date >= date('now','-5 Days')"
                , null);
        ArrayList<Invoices> invoicesArrayList = new ArrayList<Invoices>();
        while (data.moveToNext()) {
            Invoices invoices = new Invoices();
            invoices.setTabId(tab_code);
            invoices.setCompanyCode(company_code);
            invoices.setSrCode(sr_code);
            invoices.setCustomerCode(data.getString(2));
            invoices.setInvoiceId(data.getString(3));
            invoices.setInvoiceCode(data.getString(4));
            invoices.setDate(data.getString(5));
            invoices.setStatus(data.getString(6));
            invoices.setValue(data.getString(7));
            invoices.setLineItems(data.getString(8));
            invoices.setPaidAmount(data.getString(9));
            invoices.setBalanceAmount(data.getString(10));
            invoices.setChangeType(data.getString(11));
            invoices.setVisitSequenceNumber(data.getString(12));
            invoices.setSignatureFilePath(data.getString(13));
            invoices.setSignatureFileName(data.getString(14));

            invoicesArrayList.add(invoices);
        }
        return invoicesArrayList;
    }

    public ArrayList<Invoices> getInvoiceSignImageService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select invoice_number,invoice_date,signatureFilePath  from xxmsales_invoice_header where isImageUploaded=0 and signatureFilePath NOT NULL"
                , null);
        ArrayList<Invoices> invoicesDetailsArrayList = new ArrayList<Invoices>();
        while (data.moveToNext()) {
            Invoices invoices = new Invoices();
            invoices.setTabId(tab_code);
            invoices.setCompanyCode(company_code);
            invoices.setSrCode(sr_code);

            invoices.setInvoiceCode(data.getString(0));
            invoices.setDate(data.getString(1));
            invoices.setSignatureFilePath(data.getString(2));


            invoicesDetailsArrayList.add(invoices);

        }
        return invoicesDetailsArrayList;
    }

    public void updateInvoiceSignUploadFlag(String invoice_number, String invoice_date) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_invoice_header set isImageUploaded='1' where invoice_number= " + invoice_number + " and invoice_date='" + invoice_date + "'");
        db.close();

    }

    /**
     * Fetching the Total number of Invoice Item Products from Invoice detail table and returning the result as a ArrayList
     */
    public ArrayList<InvoiceItems> getInvoiceItemsService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,a.sales_rep_id,a.customer_id,a.invoice_id,a.invoice_line_id,a.product_id,a.product_name,a.product_uom,a.quantity,a.price,a.value,a.discount,a.vat,a.value_without_discount,a.value_without_vat,a.value_with_vat,a.discount_value,a.invoice_number,a.invoice_date from xxmsales_invoice_details a,xxmsales_invoice_header b where a.invoice_number=b.invoice_number and b.invoice_status='Completed' and a.invoice_date >= date('now','-5 Days')"
                , null);
        ArrayList<InvoiceItems> invoiceItemsArrayList = new ArrayList<InvoiceItems>();
        while (data.moveToNext()) {
            InvoiceItems invoiceItems = new InvoiceItems();
            invoiceItems.setTabId(tab_code);
            invoiceItems.setCompanyCode(company_code);
            invoiceItems.setSrCode(sr_code);
            invoiceItems.setCustomerCode(data.getString(2));
            invoiceItems.setInvoiceId(data.getString(3));
            invoiceItems.setInvoiceLineId(data.getString(4));
            invoiceItems.setProductCode(data.getString(5));
            invoiceItems.setProductName(data.getString(6));
            invoiceItems.setProductUom(data.getString(7));
            invoiceItems.setQuantity(data.getString(8));
            invoiceItems.setPrice(data.getString(9));
            invoiceItems.setValue(data.getString(10));
            invoiceItems.setDiscount(data.getString(11));
            invoiceItems.setVat(data.getString(12));
            invoiceItems.setVatWithoutDiscount(data.getString(13));
            invoiceItems.setVatWithoutVat(data.getString(14));
            invoiceItems.setValueWithVat(data.getString(15));
            invoiceItems.setDiscountValue(data.getString(16));
            invoiceItems.setInvoiceCode(data.getString(17));
            invoiceItems.setInvoiceDate(data.getString(18));

            invoiceItemsArrayList.add(invoiceItems);
        }
        return invoiceItemsArrayList;
    }

    /**
     * Fetching the Total number for stock receipts for both warehouse and distributor Products source from stock receipt table and returning the result as a ArrayList
     */
    public ArrayList<StockReceipt> getStockReceiptService() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select sr_receipt_number,sr_date,sr_sr_name,sr_invoice,sr_source_name,sr_source_type,sr_currency,sr_product_name,sr_product_name,sr_product_uom,sr_product_quantity,sr_product_price,sr_product_value,status,visitseqno from xxmsales_stock_reciept where status='Completed' and sr_date >= date('now','-5 Days')"
                , null);
        ArrayList<StockReceipt> stockReceiptArrayList = new ArrayList<StockReceipt>();
        while (data.moveToNext()) {
            StockReceipt stockReceipt = new StockReceipt();
            stockReceipt.setTabId(tab_code);
            stockReceipt.setCompanyCode(company_code);
            stockReceipt.setSrCode(sr_code);
            stockReceipt.setStockReceiptCode(data.getString(0));
            stockReceipt.setStockReceiptDate(data.getString(1));
            stockReceipt.setInvoiceCode(data.getString(3));
            stockReceipt.setSourceName(data.getString(4));
            stockReceipt.setStockReceiptSource(data.getString(5));
            stockReceipt.setCurrency(data.getString(6));
            stockReceipt.setProductId(data.getString(7));
            stockReceipt.setProductName(data.getString(8));
            stockReceipt.setProductUom(data.getString(9));
            stockReceipt.setProductQuantity(data.getString(10));
            stockReceipt.setProductPrice(data.getString(11));
            stockReceipt.setProductTotalPrice(data.getString(12));
            stockReceipt.setStatus(data.getString(13));
            stockReceipt.setVisitSequenceNumber(data.getString(14));

            stockReceiptArrayList.add(stockReceipt);
        }
        return stockReceiptArrayList;
    }

    public String getPassword(String email) {
        String userpassword = null;
        String selectQuery = "select password from xxmsales_user_setup ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            userpassword = cursor.getString(0);
            cursor.close();

        }

        return userpassword;
    }

    public Cursor getImei() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select imei_no from xxmsales_tab_setup "
                , null);

        return data;
    }

    public String getImeinumber() {
        String imei_no = "";
        String selectQuery = "select imei_no from xxmsales_tab_setup ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            if(cursor.getString(0)== null){
                imei_no ="";
            }else
                imei_no = cursor.getString(0);
            System.out.println("imei_no = " + imei_no);
            cursor.close();

        }

        return imei_no;
    }


    public void updateIMEINo(String imei_no) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_IMEINO, imei_no);
        db.update(TABLE_TAB_SETUP, values, null, null);

        db.close();
    }


    public String get_user_setup() {
        String email_id = "";
        String selectQuery = "select email_id from xxmsales_user_setup";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            email_id = cursor.getString(0);
            cursor.close();
        }

        return email_id;
    }

    public void update_user_password(String password,String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_PASSWORD, password);
        values.put(COLUMN_USER_CONFIRM_PASSWORD, password);
//        values.put(COLUMN_USER_MOBILE_NUMBER, mobile_number);

        db.update(TABLE_USER_SETUP, values, COLUMN_USER_EMAIL_ID + " = ?  ",
                new String[]{email});
        db.close();
    }


    /**
     * Fetching the Total number of batches for stock receipts  from product batch table and returning the result as a ArrayList
     */
    public ArrayList<StockReceiptBatch> getStockReceiptBatchService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select b_product_id,b_expiry_date,b_batch_number,b_batch_quantity,b_batch_status,b_invoice_id,b_receipt_no,id  from xxmsales_product_batch"
                , null);
        ArrayList<StockReceiptBatch> stockReceiptBatchArrayList = new ArrayList<StockReceiptBatch>();
        while (data.moveToNext()) {
            StockReceiptBatch stockReceiptBatch = new StockReceiptBatch();
            stockReceiptBatch.setTabId(tab_code);
            stockReceiptBatch.setCompanyCode(company_code);
            stockReceiptBatch.setSrCode(sr_code);
            stockReceiptBatch.setProductCode(data.getString(0));
            stockReceiptBatch.setExpiryDate(data.getString(1));
            stockReceiptBatch.setBatchNumber(data.getString(2));
            stockReceiptBatch.setBatchQuantity(data.getString(3));
            stockReceiptBatch.setBatchStatus(data.getString(4));
            stockReceiptBatch.setInvoiceId(data.getString(5));
            stockReceiptBatch.setStockReceiptCode(data.getString(6));
            stockReceiptBatch.setBatchId(data.getString(7));

            stockReceiptBatchArrayList.add(stockReceiptBatch);
        }
        return stockReceiptBatchArrayList;
    }


    /**
     * Fetching the Number of collection details for secondary invoices  from collection table and returning the result as a ArrayList
     */
    public ArrayList<CollectionDetails> getSecondaryCollectionsService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,collection_id,collection_number,invoice_id,invoice_num,collection_date,invoice_amount,paid_amount,balance_amount,collection_type,payment_amount,payment_mode,check_number,cheque_date,bank_branch,transfer_account_number,transfer_bank_branch,teller_number,transfer_date,visitseqno,signatureFilePath , signature_imagename from xxmsales_collection_details where collection_date >= date('now','-5 Days')"
                , null);
        ArrayList<CollectionDetails> collectionDetailsArrayList = new ArrayList<CollectionDetails>();
        while (data.moveToNext()) {
            CollectionDetails collectionDetails = new CollectionDetails();
            collectionDetails.setTabId(tab_code);
            collectionDetails.setCompanyCode(company_code);
            collectionDetails.setSrCode(sr_code);
            collectionDetails.setCustomerCode(data.getString(2));
            collectionDetails.setCollectionId(data.getString(3));
            collectionDetails.setCollectionCode(data.getString(4));
            collectionDetails.setInvoiceId(data.getString(5));
            collectionDetails.setInvoiceCode(data.getString(6));
            collectionDetails.setDate(data.getString(7));
            collectionDetails.setInvoiceAmount(data.getString(8));
            collectionDetails.setPaidAmount(data.getString(9));
            collectionDetails.setBalanceAmount(data.getString(10));
            collectionDetails.setCollectionType(data.getString(11));
            collectionDetails.setPaymentAmount(data.getString(12));
            collectionDetails.setPaymentMode(data.getString(13));
            collectionDetails.setCheckNumber(data.getString(14));
            collectionDetails.setCheckDate(data.getString(15));
            collectionDetails.setBankBranch(data.getString(16));
            collectionDetails.setTransferAccNumber(data.getString(17));
            collectionDetails.setTransferBankBranch(data.getString(18));
            collectionDetails.setTellerNumber(data.getString(19));
            collectionDetails.setTransferDate(data.getString(20));
            collectionDetails.setVisitSequenceNumber(data.getString(21));

            collectionDetails.setSignatureFilePath(data.getString(22));
            collectionDetails.setSignatureFileName(data.getString(23));

            collectionDetailsArrayList.add(collectionDetails);
        }
        return collectionDetailsArrayList;
    }


    public ArrayList<CollectionDetails> getSecCollecSignImageService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select collection_number,collection_date,signatureFilePath  from xxmsales_collection_details where isImageUploaded=0 and signatureFilePath NOT NULL"
                , null);
        ArrayList<CollectionDetails> collectionDetailsArrayList = new ArrayList<CollectionDetails>();
        while (data.moveToNext()) {
            CollectionDetails collectionDetails = new CollectionDetails();
            collectionDetails.setTabId(tab_code);
            collectionDetails.setCompanyCode(company_code);
            collectionDetails.setSrCode(sr_code);

            collectionDetails.setCollectionCode(data.getString(0));
            collectionDetails.setDate(data.getString(1));
            collectionDetails.setSignatureFilePath(data.getString(2));


            collectionDetailsArrayList.add(collectionDetails);

        }
        return collectionDetailsArrayList;
    }


    public void updateSecCollectSignUploadFlag(String collection_number, String collection_date) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_collection_details set isImageUploaded='1' where collection_number= " + collection_number + " and collection_date='" + collection_date + "'");
        db.close();

    }

    public ArrayList<CollectionDetails> getPrimaryCollectionsService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,collection_id,collection_number,invoice_id,invoice_num,collection_date,invoice_amount,paid_amount,balance_amount,collection_type,payment_amount,payment_mode,check_number,cheque_date,bank_branch,transfer_account_number,transfer_bank_branch,teller_number,transfer_date,order_number,visitseqno,signatureFilePath , signature_imagename from xxmsales_primary_collection_details where collection_date >= date('now','-5 Days')"
                , null);
        ArrayList<CollectionDetails> collectionDetailsArrayList = new ArrayList<CollectionDetails>();
        while (data.moveToNext()) {
            CollectionDetails collectionDetails = new CollectionDetails();
            collectionDetails.setTabId(tab_code);
            collectionDetails.setCompanyCode(company_code);
            collectionDetails.setSrCode(sr_code);
            collectionDetails.setCustomerCode(data.getString(2));
            collectionDetails.setCollectionId(data.getString(3));
            collectionDetails.setCollectionCode(data.getString(4));
            collectionDetails.setInvoiceId(data.getString(5));
            collectionDetails.setInvoiceCode(data.getString(6));
            collectionDetails.setDate(data.getString(7));
            collectionDetails.setInvoiceAmount(data.getString(8));
            collectionDetails.setPaidAmount(data.getString(9));
            collectionDetails.setBalanceAmount(data.getString(10));
            collectionDetails.setCollectionType(data.getString(11));
            collectionDetails.setPaymentAmount(data.getString(12));
            collectionDetails.setPaymentMode(data.getString(13));
            collectionDetails.setCheckNumber(data.getString(14));
            collectionDetails.setCheckDate(data.getString(15));
            collectionDetails.setBankBranch(data.getString(16));
            collectionDetails.setTransferAccNumber(data.getString(17));
            collectionDetails.setTransferBankBranch(data.getString(18));
            collectionDetails.setTellerNumber(data.getString(19));
            collectionDetails.setTransferDate(data.getString(20));
            collectionDetails.setOrderNumber(data.getString(21));
            collectionDetails.setVisitSequenceNumber(data.getString(22));
            collectionDetails.setSignatureFilePath(data.getString(23));
            collectionDetails.setSignatureFileName(data.getString(24));


            collectionDetailsArrayList.add(collectionDetails);
        }
        return collectionDetailsArrayList;
    }

    public ArrayList<CollectionDetails> getPrimCollecSignImageService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select collection_number,collection_date,signatureFilePath  from xxmsales_primary_collection_details where isImageUploaded=0 and signatureFilePath NOT NULL"
                , null);
        ArrayList<CollectionDetails> collectionDetailsArrayList = new ArrayList<CollectionDetails>();
        while (data.moveToNext()) {
            CollectionDetails collectionDetails = new CollectionDetails();
            collectionDetails.setTabId(tab_code);
            collectionDetails.setCompanyCode(company_code);
            collectionDetails.setSrCode(sr_code);

            collectionDetails.setCollectionCode(data.getString(0));
            collectionDetails.setDate(data.getString(1));
            collectionDetails.setSignatureFilePath(data.getString(2));


            collectionDetailsArrayList.add(collectionDetails);

        }
        return collectionDetailsArrayList;
    }

    public void updatePriCollectSignUploadFlag(String collection_number, String collection_date) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_primary_collection_details set isImageUploaded='1' where collection_number= " + collection_number + " and collection_date='" + collection_date + "'");
        db.close();

    }

    public ArrayList<CustomerDetails> getCustomerDetails() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,customer_name,address1,address2,address3,city,state,mobile_number,pincode,customer_type,email,location_name,lga,image_file_name from xxmsales_customer_details "
                , null);
        ArrayList<CustomerDetails> customerDetailsArrayList = new ArrayList<CustomerDetails>();
        while (data.moveToNext()) {
            CustomerDetails customerDetails = new CustomerDetails();
            customerDetails.setTabId(tab_code);
            customerDetails.setCompanyCode(company_code);
            customerDetails.setSrCode(sr_code);
            customerDetails.setCustomerCode(data.getString(2));
            customerDetails.setCustomerName(data.getString(3));
            customerDetails.setAddress1(data.getString(4));
            customerDetails.setAddress2(data.getString(5));
            customerDetails.setAddress3(data.getString(6));
            customerDetails.setCity(data.getString(7));
            customerDetails.setState(data.getString(8));
            customerDetails.setMobileNumber(data.getString(9));
            customerDetails.setPincode(data.getString(10));
            customerDetails.setCustomerType(data.getString(11));
            customerDetails.setEmail(data.getString(12));
            customerDetails.setLocationName(data.getString(13));
            customerDetails.setLga(data.getString(14));
            customerDetails.setImageFileName(data.getString(15));

            customerDetailsArrayList.add(customerDetails);
        }
        return customerDetailsArrayList;
    }

    /**
     * Fetching the new customer details  from customer details table and returning the result as a ArrayList
     */
    public ArrayList<CustomerDetails> getCustomerDetailsService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,customer_name,address1,address2,address3,city,state,mobile_number,pincode,customer_type,email,location_name,lga,image_file_name,additional_info,distributor_name,distributor_code from xxmsales_customer_details where created_from='Tab' and creation_date >= date('now','-5 Days')  "
                , null);
        ArrayList<CustomerDetails> customerDetailsArrayList = new ArrayList<CustomerDetails>();
        while (data.moveToNext()) {
            CustomerDetails customerDetails = new CustomerDetails();
            customerDetails.setTabId(tab_code);
            customerDetails.setCompanyCode(company_code);
            customerDetails.setSrCode(sr_code);
            customerDetails.setCustomerCode(data.getString(2));
            customerDetails.setCustomerName(data.getString(3));
            customerDetails.setAddress1(data.getString(4));
            customerDetails.setAddress2(data.getString(5));
            customerDetails.setAddress3(data.getString(6));
            customerDetails.setCity(data.getString(7));
            customerDetails.setState(data.getString(8));
            customerDetails.setMobileNumber(data.getString(9));
            customerDetails.setPincode(data.getString(10));
            customerDetails.setCustomerType(data.getString(11));
            customerDetails.setEmail(data.getString(12));
            customerDetails.setLocationName(data.getString(13));
            customerDetails.setLga(data.getString(14));
            customerDetails.setImageFileName(data.getString(15));
            customerDetails.setAdditionalInfo(data.getString(16));
            customerDetails.setDistributorName(data.getString(17));
            customerDetails.setDistributorCode(data.getString(18));

            customerDetailsArrayList.add(customerDetails);
        }
        return customerDetailsArrayList;
    }

    /**
     * Fetching the new customer details  from customer details table and returning the result as a ArrayList
     */
    public ArrayList<CustomerDetails> getCustomerDetailsImageService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,sales_rep_id,customer_id,customer_name,image_file_name,image_url from xxmsales_customer_details where created_from='Tab' and is_image_uploaded=0 and image_url NOT NULL"
                , null);
        ArrayList<CustomerDetails> customerDetailsArrayList = new ArrayList<CustomerDetails>();
        while (data.moveToNext()) {
            CustomerDetails customerDetails = new CustomerDetails();
            customerDetails.setTabId(tab_code);
            customerDetails.setCompanyCode(company_code);
            customerDetails.setSrCode(sr_code);
            customerDetails.setCustomerCode(data.getString(2));
            customerDetails.setCustomerName(data.getString(3));
            customerDetails.setImageFileName(data.getString(4));
            customerDetails.setImageURL(data.getString(5));
            customerDetailsArrayList.add(customerDetails);
        }
        return customerDetailsArrayList;
    }


    public void updateCustomerDetailsUploadFlag(String customer_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_customer_details set is_image_uploaded='1' where customer_id= '" + customer_id + "'");
        db.close();

    }

    public void updateCustomerDetailsAdditonalInfo(String info, String customer_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("update xxmsales_customer_details set additional_info= '" + info + "' where customer_id= '" + customer_id + "'");
        db.close();

    }

    public String getCustomerInfo(String customer_id) {
        String value = "";
        String selectQuery = "select additional_info from xxmsales_customer_details where customer_id= '" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }

    /**
     * Fetching the feedback data from feedback table and returning the result as a ArrayList
     */
    public ArrayList<Feedback> getFeedbackService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select company_id,sales_rep_id,customer_id,type,feedback_date,action_taken,feedback,id,feedback_code,visitseqno  from xxmsales_feedback where feedback_date >= date('now','-5 Days')"
                , null);
        ArrayList<Feedback> feedbackArrayList = new ArrayList<Feedback>();
        while (data.moveToNext()) {
            Feedback feedback = new Feedback();
            feedback.setTabId(tab_code);
            feedback.setCompanyCode(company_code);
            feedback.setSrCode(sr_code);
            feedback.setCustomerCode(data.getString(2));
            feedback.setFeedbackType(data.getString(3));
            feedback.setFeedbackDate(data.getString(4));
            feedback.setActionTaken(data.getString(5));
            feedback.setFeedbackText(data.getString(6));
            feedback.setFeedbackId(data.getString(7));
            feedback.setFeedbackCode(data.getString(8));
            feedback.setVisitSequenceNumber(data.getString(9));

            feedbackArrayList.add(feedback);

        }
        return feedbackArrayList;
    }

    public ArrayList<TripDetails> getTripDetailsService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select trip_number,trip_date,trip_time,starting_km,trip_name,random_num,trip_stop_date,trip_stop_time,odometer_image,trip_file_name  from xxmsales_trip_details"
                , null);
        ArrayList<TripDetails> tripDetailsArrayList = new ArrayList<TripDetails>();
        while (data.moveToNext()) {
            TripDetails tripDetails = new TripDetails();
            tripDetails.setTabCode(tab_code);
            tripDetails.setCompanyCode(company_code);
            tripDetails.setSrCode(sr_code);
            tripDetails.setTripNumber(data.getString(0));
            tripDetails.setTripDate(data.getString(1));
            tripDetails.setTripTime(data.getString(2));
            tripDetails.setStartingKilometer(data.getString(3));
            tripDetails.setTripName(data.getString(4));
            tripDetails.setRandomNumber(data.getString(5));
            tripDetails.setTripStopDate(data.getString(6));
            tripDetails.setTripStopTime(data.getString(7));
            tripDetails.setOdometerImage(data.getString(8));
            tripDetails.setTripFileName(data.getString(9));

            tripDetailsArrayList.add(tripDetails);

        }
        return tripDetailsArrayList;
    }

    public  ArrayList<TripDetails> getTripDetail(String tripDateString)  {

        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select * from " + TABLE_TRIP_DETAILS + " where trip_date='" + tripDateString + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);


        ArrayList<TripDetails> tripDetailsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            TripDetails tripDetails = new TripDetails();
            tripDetails.setTripNumber(cursor.getString(1));
            tripDetails.setTripName(cursor.getString(7));

            tripDetailsArrayList.add(tripDetails);
        }
        cursor.close();
        sqlWrite.close();
        return tripDetailsArrayList;
    }

    public Cursor getTripTime(String trip_no, String tripDateString) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select * from " + TABLE_TRIP_DETAILS + " where trip_number='" + trip_no + "' and trip_date='" + tripDateString + "' ";
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }


//    public  ArrayList<TripDetails> getTripTime(String trip_no, String tripDateString)  {
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        String selectQuery = "select * from " + TABLE_TRIP_DETAILS + " where trip_number='" + trip_no + "' and trip_date='" + tripDateString + "' ";
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//
//        ArrayList<TripDetails> tripDetailsArrayList = new ArrayList<>();
//        while (cursor.moveToNext()) {
//            TripDetails tripDetails = new TripDetails();
//            tripDetails.setTripTime(cursor.getString(3));
//            tripDetails.setTripStopDate(cursor.getString(9));
//            tripDetails.setTripStopTime(cursor.getString(11));
//
//            tripDetailsArrayList.add(tripDetails);
//        }
//        cursor.close();
//        sqlWrite.close();
//        return tripDetailsArrayList;
//    }


//    public ArrayList<TripDetails> getTripDetail(String tripDateString) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor data = db.rawQuery(
//                "select trip_number,trip_name  from xxmsales_trip_details where trip_date= '" + tripDateString + "'"
//                , null);
//        ArrayList<TripDetails> tripDetailsArrayList = new ArrayList<TripDetails>();
//        while (data.moveToNext()) {
//            TripDetails tripDetails = new TripDetails();
//            tripDetails.setTabCode(tab_code);
//            tripDetails.setCompanyCode(company_code);
//            tripDetails.setSrCode(sr_code);
//            tripDetails.setTripNumber(data.getString(0));
//            tripDetails.setTripName(data.getString(1));
//
//
//            tripDetailsArrayList.add(tripDetails);
//
//        }
//        return tripDetailsArrayList;
//    }


    /**
     * Fetching the trip mileage images for posting images in FTP
     */

    public ArrayList<TripDetails> getTripDetailsImageService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select trip_number,trip_date,odometer_image  from xxmsales_trip_details where is_image_uploaded=0 and odometer_image NOT NULL"
                , null);
        ArrayList<TripDetails> tripDetailsArrayList = new ArrayList<TripDetails>();
        while (data.moveToNext()) {
            TripDetails tripDetails = new TripDetails();
            tripDetails.setTabCode(tab_code);
            tripDetails.setCompanyCode(company_code);
            tripDetails.setSrCode(sr_code);
            tripDetails.setTripNumber(data.getString(0));
            tripDetails.setTripDate(data.getString(1));
            tripDetails.setOdometerMileage(data.getString(2));


            tripDetailsArrayList.add(tripDetails);

        }
        return tripDetailsArrayList;
    }


    public ArrayList<CheckInDetails> getCheckInDetailsService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select trip_number,customer_id,checkin_date,checkin_time,checkin_gps_lat,checkin_gps_long,checkout_date,checkout_time,checkout_gps_lat,checkout_gps_long,duration,customer_type,visitseqno  from xxmsales_checkin_details"
                , null);
        ArrayList<CheckInDetails> checkInDetailsArrayList = new ArrayList<CheckInDetails>();
        while (data.moveToNext()) {
            CheckInDetails checkInDetails = new CheckInDetails();
            checkInDetails.setTabCode(tab_code);
            checkInDetails.setCompanyCode(company_code);
            checkInDetails.setSrCode(sr_code);
            checkInDetails.setTripNumber(data.getString(0));
            checkInDetails.setCustomerId(data.getString(1));
            checkInDetails.setCheckInDate(data.getString(2));
            checkInDetails.setCheckInTime(data.getString(3));
            checkInDetails.setCheckInGpsLat(data.getString(4));
            checkInDetails.setCheckInGpsLong(data.getString(5));
            checkInDetails.setCheckOutDate(data.getString(6));
            checkInDetails.setCheckOutTime(data.getString(7));
            checkInDetails.setCheckOutGpsLat(data.getString(8));
            checkInDetails.setCheckOutGpsLong(data.getString(9));

            checkInDetails.setDuration(data.getString(10));
            checkInDetails.setCustomerType(data.getString(11));
            checkInDetails.setVisitSequenceNumber(data.getString(12));


            checkInDetailsArrayList.add(checkInDetails);

        }
        return checkInDetailsArrayList;
    }

    public ArrayList<DoctorVisitSponsorship> getDoctorVisitSponsorship() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select sponsorship_id,type,event,from_date,to_date,budget_amount,customer_id,random_num,status,created_at,updated_at,visitseqno  from xxmsales_dr_visit_sponsorship"
                , null);
        ArrayList<DoctorVisitSponsorship> doctorVisitSponsorshipArrayList = new ArrayList<DoctorVisitSponsorship>();
        while (data.moveToNext()) {
            DoctorVisitSponsorship doctorVisitSponsorship = new DoctorVisitSponsorship();
            doctorVisitSponsorship.setTabCode(tab_code);
            doctorVisitSponsorship.setCompanyCode(company_code);
            doctorVisitSponsorship.setSrCode(sr_code);
            doctorVisitSponsorship.setSponsorshipId(data.getString(0));
            doctorVisitSponsorship.setType(data.getString(1));
            doctorVisitSponsorship.setEvent(data.getString(2));
            doctorVisitSponsorship.setFromDate(data.getString(3));
            doctorVisitSponsorship.setToDate(data.getString(4));
            doctorVisitSponsorship.setBudgetAmount(data.getString(5));
            doctorVisitSponsorship.setCustomerId(data.getString(6));
            doctorVisitSponsorship.setRandomNumber(data.getString(7));
            doctorVisitSponsorship.setStatus(data.getString(8));
            doctorVisitSponsorship.setCreatedAt(data.getString(9));
            doctorVisitSponsorship.setUpdatedAt(data.getString(10));
            doctorVisitSponsorship.setVisitSequenceNumber(data.getString(11));


            doctorVisitSponsorshipArrayList.add(doctorVisitSponsorship);

        }
        return doctorVisitSponsorshipArrayList;
    }

    public ArrayList<GPSTrackerModel> getGPSDataService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select gps_id,gps_date,gps_time,gps_latitude,gps_longitude,(select tab_id from xxmsales_tab_setup) tab_code,(select employee_id from xxmsales_user_setup) sr_code,(select short_name from xxmsales_company_setup) company_code from xxmsales_gps_tracker where is_sync is null "
                , null);
        ArrayList<GPSTrackerModel> gpsTrackerModelArrayList = new ArrayList<GPSTrackerModel>();
        while (data.moveToNext()) {

            GPSTrackerModel gpsTrackerModel = new GPSTrackerModel();
            gpsTrackerModel.setTabCode(data.getString(5));
            gpsTrackerModel.setCompanyCode(data.getString(7));
            gpsTrackerModel.setSrCode(data.getString(6));
            gpsTrackerModel.setGps_id(data.getString(0));
            gpsTrackerModel.setGps_date(data.getString(1));
            gpsTrackerModel.setGps_time(data.getString(2));
            gpsTrackerModel.setGps_latitude(data.getString(3));
            gpsTrackerModel.setGps_longitude(data.getString(4));
            gpsTrackerModelArrayList.add(gpsTrackerModel);

        }
        return gpsTrackerModelArrayList;
    }


    //Update Sync Flag to true for the data's synced in Server
    public void updateOrdersSync(String[] orderIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_order_header set is_sync='true' where salesorder_number in (" + makePlaceholders(orderIds.length + 1) + ")", orderIds);
        db.close();
    }

    public void updateOrderItemsSync(String[] orderIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_order_details set is_sync='true' where salesorder_number in (" + makePlaceholders(orderIds.length + 1) + ")", orderIds);
        db.close();
    }

    public void updateInvoicesSync(String[] invoiceIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_invoice_header set is_sync='true' where invoice_number in (" + makePlaceholders(invoiceIds.length + 1) + ")", invoiceIds);
        db.close();
    }

    public void updateInvoiceItemsSync(String[] invoiceIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_invoice_details set is_sync='true' where invoice_number in (" + makePlaceholders(invoiceIds.length + 1) + ")", invoiceIds);
        db.close();
    }

    public void updatePrimaryCollectionsSync(String[] collectionIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_primary_collection_details set is_sync='true' where collection_number in (" + makePlaceholders(collectionIds.length + 1) + ")", collectionIds);
        db.close();
    }

    public void updateSecondaryCollectionsSync(String[] collectionIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_collection_details set is_sync='true' where collection_number in (" + makePlaceholders(collectionIds.length + 1) + ")", collectionIds);
        db.close();
    }

    public void updateCustomerDetailsSync(String[] customerIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_customer_details set is_sync='true' where customer_id in (" + makePlaceholders(customerIds.length + 1) + ")", customerIds);
        db.close();
    }

    public void updateCompetitorStockProductsSync(String[] customerIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_competitor_stock_product set is_sync='true' where competitor_stock_id in (" + makePlaceholders(customerIds.length + 1) + ")", customerIds);
        db.close();
    }

    public void updateCustomerStockProductsSync(String[] customerIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_customer_stock_product set is_sync='true' where product_id in (" + makePlaceholders(customerIds.length + 1) + ")", customerIds);
        db.close();
    }

    public void updateStockReceiptsSync(String[] customerIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_stock_reciept set is_sync='true' where sr_receipt_number in (" + makePlaceholders(customerIds.length + 1) + ")", customerIds);
        db.close();
    }

    public void updateStockReceiptBatchesSync(String[] customerIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_product_batch set is_sync='true' where b_receipt_no in (" + makePlaceholders(customerIds.length + 1) + ")", customerIds);
        db.close();
    }

    public void updateSurveyResponseSync(String[] customerIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_survey_response set is_sync='true' where id in (" + makePlaceholders(customerIds.length + 1) + ")", customerIds);
        db.close();
    }

    public void updateSurveyResponseAnswerSync(String[] customerIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_survey_response_answer set is_sync='true' where response_answer_id in (" + makePlaceholders(customerIds.length + 1) + ")", customerIds);
        db.close();
    }

    //Get Customer Details
    public Cursor getInvoiceReportDetails(String bde_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select a.invoice_date,a.invoice_number,(select b.customer_name from xxmsales_customer_details b where b.customer_id=a.customer_id),(select b.state from xxmsales_customer_details b where b.customer_id=a.customer_id) state,(select b.city from xxmsales_customer_details b where b.customer_id=a.customer_id) city,(select count(invoice_id) from xxmsales_invoice_details c where c.invoice_id=a.invoice_id ) line_items,a.invoice_value,a.invoice_status,a.invoice_id,a.customer_id from xxmsales_invoice_header a  where  invoice_status <> 'Draft' order by invoice_date desc,invoice_number desc"
                , null);

        return data;
    }

  /*  //Get Collection Details
    public Cursor getPrimaryCollectionReportDetails(String bde_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select a.collection_date,(select c.customer_name from xxmsales_customer_details c where c.customer_id=a.customer_id),a.collection_number,a.payment_amount,a.invoice_num,a.invoice_amount,(a.invoice_amount-a.payment_amount)balance_amount,a.payment_mode,a.bank_branch,a.bank_branch,'B' order_data from xxmsales_primary_collection_details a where sales_rep_id = '" + bde_code + "'" + " order by order_data,a.collection_date desc"
                , null);

        return data;
    }*/


    //New Changes

    //Get Collection Details
    public Cursor getPrimaryCollectionReportDetails(String bde_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select a.collection_date,(select c.customer_name from xxmsales_customer_details c where c.customer_id=a.customer_id),a.collection_number,a.payment_amount,a.invoice_num,(case a.collection_type WHEN 'onAccount' THEN NULL ELSE a.invoice_amount END) invoice_amount,(case a.collection_type WHEN 'onAccount' THEN (-1) * payment_amount  ELSE (a.invoice_amount-a.payment_amount) END)balance_amount,a.payment_mode,a.bank_branch,a.bank_branch,'B' order_data,a.collectionStatus from xxmsales_primary_collection_details a  order by order_data,a.collection_date desc"
                , null);
        return data;
    }

    //Get Collection Details
    public Cursor getSecondaryCollectionReportDetails(String bde_code) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select a.collection_date,(select c.customer_name from xxmsales_customer_details c where c.customer_id=a.customer_id),a.collection_number,a.payment_amount,a.invoice_num,a.invoice_amount,(a.invoice_amount-a.payment_amount) balance_amount,a.payment_mode,a.bank_branch,a.bank_branch,'B' order_data,a.transactionType,a.collectionStatus from xxmsales_collection_details a where sales_rep_id = '" + bde_code + "'" + " order by order_data,a.collection_date desc"
                , null);

        return data;
    }

    public void updateGPSSync(String[] gpsIds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_gps_tracker set is_sync='true' where gps_id in (" + makePlaceholders(gpsIds.length + 1) + ")", gpsIds);
        db.close();
    }

    public ArrayList<MarqueeText> getMessages() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT message_name ,message_creation_date from xxmsales_global_message order by message_creation_date desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        ArrayList<MarqueeText> marqueeTextArrayList = new ArrayList<MarqueeText>();
        Log.d("QueryData Array ", marqueeTextArrayList.toString());

        int i = 0;
        int serial_num = 0;

        while (cursor.moveToNext()) {
            MarqueeText marqueeText = new MarqueeText();

            serial_num = serial_num + 1;
            marqueeText.setSerail(String.valueOf(serial_num));
            marqueeText.setMsgContent(cursor.getString(0));

            String sDate1 = cursor.getString(1);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");
                String dateString = format.format(date1);
                marqueeText.setCreated_date(dateString);
            } catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }

            marqueeTextArrayList.add(i, marqueeText);

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return marqueeTextArrayList;
    }


    //Get Feedback Reports

    public ArrayList<FeedbackReport> getFeedbackReports() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT fb.feedback_date,fb.type,cust_det.customer_name,fb.feedback,fb.action_taken,fb.feedback_code FROM " + TABLE_FEEDBACK + " as fb LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON fb.customer_id = cust_det.customer_id";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("QueryData cursor ", cursor.toString());

        ArrayList<FeedbackReport> feedbackReportArrayList = new ArrayList<FeedbackReport>();
        Log.d("QueryData Array ", feedbackReportArrayList.toString());

        int i = 0;

        while (cursor.moveToNext()) {
            FeedbackReport feedbackReport = new FeedbackReport();
            // feedbackReport.setFeedback_date(cursor.getString(0));

            String sDate1 = cursor.getString(0);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                feedbackReport.setFeedback_date(dateString);
            } catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }

            feedbackReport.setFeedback_type(cursor.getString(1));
            feedbackReport.setCustomer_name(cursor.getString(2));
            feedbackReport.setCustomer_feedback(cursor.getString(3));
            feedbackReport.setAction_taken(cursor.getString(4));
            feedbackReport.setFeedback_code(cursor.getString(5));

            feedbackReportArrayList.add(i, feedbackReport);
            Log.d("QueryData", "FbText " + feedbackReport.getCustomer_feedback());

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return feedbackReportArrayList;
    }

    public ArrayList<FeedbackReport> getActionTakenDetails(String feedback_code) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();


        Cursor cursor = sqlWrite.rawQuery(
                "SELECT fb.feedback_date,fb.action_taken,fb.status from xxmsales_feedback fb where feedback_code = '" + feedback_code + "'"
                , null);

        Log.d("QueryData cursor ", cursor.toString());

        ArrayList<FeedbackReport> feedbackReportArrayList = new ArrayList<FeedbackReport>();
        Log.d("QueryData Array ", feedbackReportArrayList.toString());

        int i = 0;

        while (cursor.moveToNext()) {
            FeedbackReport feedbackReport = new FeedbackReport();
            // feedbackReport.setFeedback_date(cursor.getString(0));

            String sDate1 = cursor.getString(0);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                feedbackReport.setFeedback_date(dateString);
            } catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }


            feedbackReport.setAction_taken(cursor.getString(1));
            feedbackReport.setFeedback_status(cursor.getString(2));

            feedbackReportArrayList.add(i, feedbackReport);
            Log.d("QueryData", "FbText " + feedbackReport.getCustomer_feedback());

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return feedbackReportArrayList;
    }


    //Get Customer Details
    public Cursor getInvoiceTopproductDetails(String product_id, String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,a.invoice_date,b.invoice_number,a.quantity,a.value from xxmsales_invoice_details a,xxmsales_invoice_header b where a.invoice_id=b.invoice_id and b.invoice_status='Completed' and a.product_id = '" + product_id + "'" + " and a.customer_id = '" + customer_id + "'" + " order by a.value desc"
                , null);

        return data;
    }

    //Get Customer Details
    public Cursor getTopproductDetails(String product_id, String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select null,a.salesorder_date,b.salesorder_number,a.quantity,a.value from xxmsales_order_details a,xxmsales_order_header b where a.salesorder_id=b.salesorder_id and b.salesorder_status='Completed' and a.product_id = '" + product_id + "'" + " and a.customer_id = '" + customer_id + "'" + " order by a.value desc"
                , null);

        return data;
    }

    //Get Customer Details
    public Cursor getodometerimage(String trip_number) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_trip_details where trip_number = '" + trip_number + "'"
                , null);

        return data;
    }

    public int get_checkin_id() {
        int value = 0;
        String selectQuery = "select check_id from xxmsales_checkin_details order by check_id desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public String get_order_sync(String order_number) {
        String value = "";
        String selectQuery = "select is_sync from xxmsales_order_header where salesorder_number ='" + order_number + "' ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }

    public String get_invoice_sync(String invoice_number) {
        String value = "";
        String selectQuery = "select is_sync from xxmsales_invoice_header where invoice_number ='" + invoice_number + "' ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }


    public Cursor get_lpoimages_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_lpo_images where isUploaded= ' " + value   + " ' ",null);
        return data;
    }

    public Cursor get_tripImages_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_trip_details where is_image_uploaded= ' " + value   + " ' ",null);
        return data;
    }

    public Cursor get_expenseSign_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_expense where isImageUploaded= ' " + value   + " ' ",null);
        return data;
    }

    public Cursor get_orderSign_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_order_header where isImageUploaded= ' " + value   + " ' ",null);
        return data;
    }

    public Cursor get_invoiceSign_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_invoice_header where isImageUploaded= ' " + value   + " ' ",null);
        return data;
    }

    public Cursor get_SecCollSign_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_collection_details where isImageUploaded= ' " + value   + " ' ",null);
        return data;
    }

    public Cursor get_priCollSign_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_primary_collection_details where isImageUploaded= ' " + value   + " ' ",null);
        return data;
    }

    public Cursor get_CustomerImage_sync() {
        SQLiteDatabase db = this.getWritableDatabase();
        int  value=0;
        Cursor data = db.rawQuery("select * from xxmsales_customer_details where is_image_uploaded= ' " + value   + " ' ",null);
        return data;
    }

    //Get Customer List for the SalesRep
   /* public Cursor getSalesrepCustomers(String sales_rep_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select customer_id,serial_number,customer_name,city,customer_type from xxmsales_customer_details where sales_rep_id = '" + sales_rep_id + "'", null);
        return data;
    } */

    //Get Customer List for the SalesRep
    public Cursor getUnplannedCustomers(String sales_rep_id, String trip_num, String customer_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        String status_true = "true";
        String status_false = "false";
        String query = "select  customer_id," +
                "serial_number," +
                "customer_name," +
                "location_name," +
                "customer_type," +
                "( select CASE count(*) " +
                "when NULL THEN " + "'"
                + status_false +
                "'" + " ELSE " + "'" +
                status_true + "'" +
                "END from xxmsales_checkin_details b  " +
                "where b.customer_id=a.customer_id " +
                "and checkin_date=date('now')" +
                "group by customer_id) status, case a.customer_type when 'Primary' then 1  when 'Secondary' then 2  when 'Doctor' then 3 else 4 end as customer_status from xxmsales_customer_details a " +
                "where a.customer_type <> 'Lead' " +
                "and a.customer_id not in (select customer_id from xxmsales_journey_plan)";

        if (!TextUtils.isEmpty(customer_name))
            query += " and a.customer_name like '%" + customer_name + "%'";

        query += " order by customer_status asc,a.customer_name asc";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getSalesrepCustomers(String sales_rep_id, String trip_num) {
        SQLiteDatabase db = this.getWritableDatabase();
        String status_true = "true";
        String status_false = "false";
        Cursor data = db.rawQuery
                ("select  customer_id," +
                        "serial_number," +
                        "customer_name," +
                        "city," +
                        "customer_type," +
                        "(select CASE count(*) " +
                        "when NULL THEN " + "'"
                        + status_false +
                        "'" + " ELSE " + "'" +
                        status_true + "'" +
                        "END from xxmsales_checkin_details b  " +
                        "where b.customer_id=a.customer_id " +
                        "and checkin_date=date('now')" +
                        "group by customer_id) status from xxmsales_journey_plan a " +
                        "UNION\n" +
                        "    select '0'customer_id,'0'serial_number,'UNPLANNED CUSTOMERS' customer_name,'' city,'' customer_type,'' status  order by serial_number", null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getExpiryProducts() {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery
                ("select b_product_id,b_batch_number,b_expiry_date,quantity,quantity,date('now') from (select b_batch_number,b_product_id,b_expiry_date,sum(b_batch_quantity) quantity from xxmsales_product_batch GROUP by b_batch_number,b_expiry_date,b_product_id) where b_expiry_date <=date('now') and quantity > 0", null);
        return data;
    }


    /**
     * Fetching the Quarantine Details from Quarantine Table for showing the expired products list.
     */

    public ArrayList<ExpiredProductsReport> getExpiredProductsReports() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT product_code,batch_number,expiry_date,batch_quantity,quarantine_quantity,quarantine_date FROM " + TABLE_QUARANTINE_PRODUCTS;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});


        Log.d("Expired Cursor----", cursor.toString());

        ArrayList<ExpiredProductsReport> expiredProductsReportArrayList = new ArrayList<ExpiredProductsReport>();
        Log.d("Expired Array---- ", expiredProductsReportArrayList.toString());

        int i = 0;

        while (cursor.moveToNext()) {
            ExpiredProductsReport expiredProductsReport = new ExpiredProductsReport();
            expiredProductsReport.setProductName(cursor.getString(0));
            expiredProductsReport.setBatchName(cursor.getString(1));

            String sDate1 = cursor.getString(2);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                expiredProductsReport.setExpiredBatchDate(dateString);
            } catch (Exception e) {
            }

            expiredProductsReport.setExpiredQty(cursor.getString(3));


            String quarantineDate = cursor.getString(4);
            try {
                Date quaDate = new SimpleDateFormat("yyyy-MM-dd").parse(quarantineDate);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String quaDateString = format.format(quaDate);
                expiredProductsReport.setQuarantineDate(quaDateString);
            } catch (Exception e) {
            }

            expiredProductsReport.setQuarantineQty(cursor.getString(5));

            expiredProductsReportArrayList.add(i, expiredProductsReport);
            Log.d("Expired ", "ProductName ---- " + expiredProductsReport.getProductName());

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return expiredProductsReportArrayList;
    }


    /**
     * Fetching the Customer Return Product Details.
     */

    public ArrayList<CustomerReturnProductsReport> getCustomerReturnProductsReports() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT cr.customerReturnDate,cr.invoiceNumber,crp.customerReturnNumber,cust_det.customer_name,crp.productName,crp.returnQuantity,crp.unitPrice,crp.returnValue FROM " + TABLE_CUSTOMER_RETURN_PRODUCTS + " as crp LEFT JOIN " + TABLE_CUSTOMER_RETURN + " as cr on cr.customerReturnNum = crp.customerReturnNumber LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON cr.customerCode = cust_det.customer_id order by crp.customerReturnNumber desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});


        ArrayList<CustomerReturnProductsReport> customerReturnProductsReportArrayList = new ArrayList<CustomerReturnProductsReport>();

        int i = 0;

        while (cursor.moveToNext()) {
            CustomerReturnProductsReport customerReturnProductsReport = new CustomerReturnProductsReport();


            String sDate1 = cursor.getString(0);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                customerReturnProductsReport.setCusReturnDate(dateString);
            } catch (Exception e) {
            }
            customerReturnProductsReport.setInvoiceNum(cursor.getString(1));
            customerReturnProductsReport.setCusReturnNumber(cursor.getString(2));
            customerReturnProductsReport.setCustomerName(cursor.getString(3));

            customerReturnProductsReport.setProductName(cursor.getString(4));
            customerReturnProductsReport.setReturnQuantity(cursor.getString(5));
            customerReturnProductsReport.setReturnPrice(cursor.getString(6));
            customerReturnProductsReport.setReturnValue(cursor.getString(7));

            customerReturnProductsReportArrayList.add(i, customerReturnProductsReport);
            Log.d("CustomerReturn ", "ProductName ---- " + customerReturnProductsReport.getProductName());

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return customerReturnProductsReportArrayList;
    }

    public ArrayList<StockReturnProductsReport> getStockReturnProductsReports() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT sr.stockReturnDate,sr.stockReturnNum,sr.stockReturnType,srp.productName,srp.returnQuantity,srp.unitPrice,srp.returnValue FROM " + TABLE_STOCK_RETURN_PRODUCTS + " as srp LEFT JOIN " + TABLE_STOCK_RETURN + " as sr on sr.stockReturnNum = srp.stockReturnNumber order by sr.stockReturnNum desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});


        ArrayList<StockReturnProductsReport> stockReturnProductsReportArrayList = new ArrayList<StockReturnProductsReport>();

        int i = 0;

        while (cursor.moveToNext()) {
            StockReturnProductsReport stockReturnProductsReport = new StockReturnProductsReport();


            String sDate1 = cursor.getString(0);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                stockReturnProductsReport.setStockReturnDate(dateString);
            } catch (Exception e) {
            }

            stockReturnProductsReport.setStockReturnNumber(cursor.getString(1));
            stockReturnProductsReport.setStockReturnType(cursor.getString(2));

            stockReturnProductsReport.setProductName(cursor.getString(3));
            stockReturnProductsReport.setReturnQuantity(cursor.getString(4));

            stockReturnProductsReport.setReturnPrice(cursor.getString(5));
            stockReturnProductsReport.setReturnValue(cursor.getString(6));

            stockReturnProductsReportArrayList.add(i, stockReturnProductsReport);
            Log.d("StockReturn ", "ProductName ---- " + stockReturnProductsReport.getProductName());

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return stockReturnProductsReportArrayList;
    }


    public void deleteQuarantine() {
        SQLiteDatabase db = this.getWritableDatabase();

        // Deleting all records from database table
        db.delete(TABLE_QUARANTINE_PRODUCTS, null, null);
        db.close();
    }

    //Get Customer List for the SalesRep
    public Cursor getmap(String sales_rep_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String status_true = "true";
        String status_false = "false";
        Cursor data = db.rawQuery("select customer_id,customer_name,gps_latitude,gps_longitude from  xxmsales_customer_details where customer_id ='" + sales_rep_id + "'", null);

//        Cursor data = db.rawQuery
//                ("select  customer_id," +
//                                "customer_name," +
//                                "gps_latitude," +
//                                "gps_longitude," +
//                                "customer_type," +
//                                "(select CASE count(*) " +
//                                "when NULL THEN " + "'"
//                                + status_false +
//                                "'" + " ELSE " + "'" +
//                                status_true + "'" +
//                                "END from xxmsales_checkin_details b  " +
//                                "where b.customer_id=a.customer_id " +
//                                "and checkin_date=date('now')" +
//                                "group by customer_id) status from xxmsales_journey_plan a "
//                        , null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getJourneyplanformap(String sales_rep_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String status_true = "true";
        String status_false = "false";
        Cursor data = db.rawQuery
                ("select  customer_id," +
                                "customer_name," +
                                "gps_latitude," +
                                "gps_longitude," +
                                "customer_type," +
                                "(select CASE count(*) " +
                                "when NULL THEN " + "'"
                                + status_false +
                                "'" + " ELSE " + "'" +
                                status_true + "'" +
                                "END from xxmsales_checkin_details b  " +
                                "where b.customer_id=a.customer_id " +
                                "and checkin_date=date('now')" +
                                "group by customer_id) status from xxmsales_journey_plan a "
                        , null);
        return data;
    }



    //Get Warehouse Id using Warehouse Name
    public Cursor getWarehouseDetails(String warehousename) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_warehouse where w_warehouse_name ='" + warehousename + "'", null);
        return data;
    }

    //Get Employee Details using Employee ID
    public Cursor getEmployeeDetails(String employee_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_salesrep_detail where sales_rep_id ='" + employee_id + "'", null);
        return data;
    }

    public List<String> getAllWarehouse() {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_WAREHOUSE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

    //Get Stock Details in Warehouse
    public Cursor getStockDetailsWarehouse(String sales_rep_id, String product_id, String warehouse_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select s_product_name,s_product_uom,s_product_quantity,s_product_value from xxmsales_stock_details where s_sr_code = '" + sales_rep_id + "'"
                + "and s_product_id='" + product_id + "'" + "and s_product_warehouse='" + warehouse_id + "'", null);
        return data;
    }

    //Get Temp Receipt Batch table datas
    public Cursor getStockReceiptBatchTempDatas() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select b_temp_expiry_date,b_temp_batch_number,b_temp_batch_quantity from xxmsales_receipt_batch_row_temp", null);
        return data;
    }

    public void deleteRecieptBatchRecord() {

        SQLiteDatabase db = this.getWritableDatabase();

// Deleting all records from database table
        db.delete(TABLE_RECEIPT_BATCH_ROW_TEMP, null, null);

        db.close();

    }

    public void deleteCustomerMaster() {

        SQLiteDatabase db = this.getWritableDatabase();

// Deleting all records from database table
        db.delete(TABLE_CUSTOMER_DETAILS, COLUMN_C_CREATED_FROM + "= 'Web' ", null);

        db.close();

    }

    public void deleteJourneyplan() {

        SQLiteDatabase db = this.getWritableDatabase();

// Deleting all records from database table
        db.delete(TABLE_JOURNEY_PLAN_MASTER, null, null);

        db.close();

    }

    public void deleteItemMaster() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CUSTOMER_DETAILS, COLUMN_C_CREATED_FROM + "= 'Web' ", null);
        db.delete(TABLE_ITEM_DETAILS, null, null);
        db.delete(TABLE_PRICE_LIST_HEADER, null, null);
        db.delete(TABLE_PRICE_LIST_LINES, null, null);
        db.delete(TABLE_JOURNEY_PLAN_MASTER, null, null);
        db.delete(TABLE_FEEDBACK_TYPE, null, null);

        db.close();

    }

    public void deleteMasterData() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CUSTOMER_DETAILS, COLUMN_C_CREATED_FROM + "= 'Web' ", null);
        db.delete(TABLE_ITEM_DETAILS, null, null);
        db.delete(TABLE_PRICE_LIST_HEADER, null, null);
        db.delete(TABLE_PRICE_LIST_LINES, null, null);
        db.delete(TABLE_JOURNEY_PLAN_MASTER, null, null);
        db.delete(TABLE_FEEDBACK_TYPE, null, null);
        db.delete(TABLE_SR_PRICELIST, null, null);
        db.delete(TABLE_LOCATION_LINES, null, null);
        db.delete(TABLE_GLOBAL_MESSAGE, null, null);
        db.delete(TABLE_METRICS_TARGET, null, null);
        db.delete(TABLE_SR_INVOICE_BATCH, null, null);

        db.close();

    }

    public void deletePriceList() {

        SQLiteDatabase db = this.getWritableDatabase();

// Deleting all records from database table
        db.delete(TABLE_PRICE_LIST_HEADER, null, null);
        db.delete(TABLE_PRICE_LIST_LINES, null, null);

        db.close();

    }

    //Get Stock Details
    public Cursor getStockDetails(String sales_rep_id, String product_id, String warehouse_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select s_product_name,s_product_uom,s_product_quantity,s_product_value,s_product_price from xxmsales_stock_details where s_sr_code = '" + sales_rep_id + "'"
                + "and s_product_id='" + product_id + "'" + "and s_product_warehouse='" + warehouse_id + "'", null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getorderTopProductsthisyear(String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select a.product_name,sum(a.quantity) quantity,a.price,sum(a.value) value,(select count(*) from xxmsales_order_details c ,xxmsales_order_header d where c.product_id=a.product_id and c.salesorder_id=d.salesorder_id and c.customer_id ='" + customer_id + "'" + " and d.salesorder_status='Completed' " + " )  trx_count,a.product_id from xxmsales_order_details a,xxmsales_order_header b where a.salesorder_id=b.salesorder_id and a.customer_id ='" + customer_id + "'" + " and b.salesorder_status='Completed' " + "group by a.product_id order by sum(a.value) desc LIMIT 10 ", null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getInvoicedTopProductsOfYear(String customer_id, String invoice_status) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select b.product_name,sum(b.quantity-IFNULL(crp.returnQuantity,0)) quantity,b.price,(sum(b.quantity-IFNULL(crp.returnQuantity,0))*b.price) value,count(b.product_id) trx_count,b.product_id from xxmsales_invoice_header a,xxmsales_invoice_details b left join xxmsales_customer_return cr on cr.invoiceNumber = a.invoice_number left join xxmsales_customer_return_products crp on crp.customerReturnNumber=cr.customerReturnNum and crp.productCode=b.product_id  where a.invoice_id=b.invoice_id and b.customer_id ='" + customer_id + "'" + " and a.invoice_status = '"+invoice_status + "' " + " group by b.product_id having sum(b.quantity-IFNULL(crp.returnQuantity,0))!=0 order by value desc LIMIT 10 ", null);
        //Cursor data = db.rawQuery("select b.product_name,sum(b.quantity) quantity,b.price,(sum(b.quantity)*b.price) value,count(b.product_id) trx_count,b.product_id from xxmsales_invoice_header a,xxmsales_invoice_details b left join xxmsales_customer_return cr on cr.invoiceNumber = a.invoice_number left join xxmsales_customer_return_products crp on crp.customerReturnNumber=cr.customerReturnNum where a.invoice_id=b.invoice_id and crp.productCode=b.product_id and b.customer_id ='" + customer_id + "'" + " and a.invoice_status ='Completed' " + " group by b.product_id order by b.value desc LIMIT 10 ", null);
        return data;
    }


    public Cursor getProductDetails(String product_id1) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select id,a.product_id,a.product_name,a.product_desc,a.product_uom,(select b.price from xxmsales_price_list_lines b where b.product_code=a.product_id and b.price_id IN (select distinct c.sr_price_id from xxmsales_sr_pricelist c))price,a.product_type,a.stock_uom,a.batch_controlled,a.is_sync,a.onhand_stock,a.vat from xxmsales_item_details a where a.product_id ='" + product_id1 + "'", null);
        return data;
    }

    //Get Product Id using Product Name
    public Cursor getOrderProductDetails(String product_id1, String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select distinct a.product_code,a.product_id,a.price,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not,(select product_uom from xxmsales_item_details where product_id=a.product_code) product_uom from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where c.price_list_code =b.price_list_code and b.price_list_code=a.price_id and c.customer_id='" + customer_id + "'" + " and a.product_code ='" + product_id1 + "'", null);
        return data;
    }

    public Cursor getMetricsProductDetails(String productname) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select distinct a.product_code,a.product_id,a.price,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not,(select product_uom from xxmsales_item_details where product_id=a.product_code) product_uom from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where c.price_list_code =b.price_list_code and b.price_list_code=a.price_id  and a.product_id ='" + productname + "'", null);
        return data;
    }

    //Get Product Id using Product Name
    public Cursor getOrderProductDetails(String productname, int order_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_item_details where product_name ='" + productname + "'" + " and product_id not in (select product_id from xxmsales_order_details where salesorder_id=" + order_id + ")", null);
        return data;
    }

    //Get Product Id using Product Name
    public Cursor getInvoiceProductDetails(String productname, int invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_item_details where product_name ='" + productname + "'" + " and product_id not in (select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + ")", null);
        return data;
    }


    //Get Completed Order Product Purchased History
    public Cursor getPurchaseHistory(String product_id, String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select a.salesorder_date,a.quantity,a.price,(select distinct b.stock_volume from xxmsales_customer_stock_product b where b.product_id = a.product_id ) from xxmsales_order_details a JOIN xxmsales_order_header c on a.salesorder_number = c.salesorder_number where a.product_id ='" + product_id + "'" + "and a.customer_id='" + customer_id + "'" + " and c.salesorder_status='Completed' order by a.salesorder_date LIMIT 3", null);
        return data;
    }

    //Get Completed Invoice Product Purchased History
    public Cursor getInvoicePurchaseHistory(String product_id, String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select a.invoice_date,a.quantity,a.price,(select distinct b.stock_volume from xxmsales_customer_stock_product b where b.product_id = a.product_id ) from xxmsales_invoice_details a JOIN xxmsales_invoice_header c on a.invoice_number = c.invoice_number where a.product_id ='" + product_id + "'" + " and a.customer_id='" + customer_id + "'" + " and c.invoice_status='Completed' order by a.invoice_date LIMIT 3", null);
        return data;
    }


    //Get Customer List for the SalesRep
    public Cursor getOrderLines(String order_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select salesorder_line_id,product_name,product_uom,quantity,price,value,product_id from xxmsales_order_details where salesorder_id=" + order_id, null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getOrderDeliveryChallanDetails(String order_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select a.salesorder_line_id,a.product_name,a.product_uom,a.quantity,a.price,a.value,a.product_id ,a.salesorder_id,a.customer_id,a.salesorder_number,a.salesorder_date,a.salesorder_status,(select distinct b.batch_controlled from xxmsales_item_details b where b.product_id=a.product_id ) batch_controlled,null waybill_quantity from xxmsales_order_details a where a.salesorder_number='" + order_id + "'", null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getWaybillDeliveryChallanDetails(String order_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select a.salesorder_line_id,a.product_name,a.product_uom,a.quantity,a.price,a.value,a.product_id ,a.salesorder_id,a.customer_id,a.salesorder_number,a.salesorder_date,a.salesorder_status,(select distinct b.batch_controlled from xxmsales_item_details b where b.product_id=a.product_id ) batch_controlled,waybill_quantity from xxmsales_waybill_details a where a.salesorder_number='" + order_id + "'", null);
        return data;
    }

    //Get Customer List for the SalesRep
    public Cursor getTemplates(String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select salesorder_id,template_name from xxmsales_order_header where customer_id='" + customer_id + "'" + " and salesorder_status='Template' ", null);
        return data;
    }


    //Get Customer List for the SalesRep
    public Cursor getInvoiceLines(String invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select b.product_name,b.product_uom,b.quantity,b.price,b.value,b.invoice_line_id,b.product_id,b.discount,b.vat,(select batch_controlled from xxmsales_item_details a where a.product_id=b.product_id) batch_controlled from xxmsales_invoice_details b where b.invoice_id=" + invoice_id, null);
        return data;
    }

    //Get Employee Id using Employee Name
    public Cursor getemployeeid(String employeename) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_salesrep_detail where sales_rep_name ='" + employeename + "'", null);
        return data;
    }

    //Get Employee Id using Employee Name
    public Cursor getsalesrepimage(String login_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_user_setup where employee_id ='" + login_id + "'", null);
        return data;
    }


    public int get_length_salesrep_image(String login_id) {
        int value = 0;
        String selectQuery = "select length(user_image) value from xxmsales_user_setup where employee_id ='" + login_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public int get_length_customer_image(String customer_id) {
        int value = 0;
        String selectQuery = "select length(image_url) value from xxmsales_customer_details where customer_id ='" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            //value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public Bitmap getBDELogoImage(String employee_id) {
        String image_path = "", image_name = "";
        String selectQuery = "select imageUrl,imageName from xxmsales_user_setup where employee_id ='" + employee_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            image_path = cursor.getString(0);
            image_name = cursor.getString(1);
            cursor.close();
        }
        Log.d("BDEIMAGE", "image path" + image_name);
        Bitmap b = null;
        try {

            if (!TextUtils.isEmpty(image_name)) {

                File f = new File(IMAGE_HOME_PATH + "images" + File.separator + "bdeImages" + File.separator + image_name);
                b = BitmapFactory.decodeStream(new FileInputStream(f));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("BDEIMAGE", "image path" + e.getMessage());
            Log.d("Gallery", "error" + e.getMessage());
        }
        return b;
    }

    public Bitmap getCompanyLogoImage(String company_name) {
        String image_path = "", image_name = "";
        String selectQuery = "select imageUrl,imageName from xxmsales_company_setup where company_name ='" + company_name + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            image_path = cursor.getString(0);
            image_name = cursor.getString(1);
            cursor.close();
        }

        Log.d("Company Image", "image path" + image_name);
        Bitmap b = null;
        try {

            if (!TextUtils.isEmpty(image_name)) {
                File f = new File(IMAGE_HOME_PATH + "images" + File.separator + "companyImages" + File.separator + image_name);
                b = BitmapFactory.decodeStream(new FileInputStream(f));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("Company Image Image", "image path" + e.getMessage());
            Log.d("Company Gallery", "error" + e.getMessage());
        }
        return b;
    }


    public Bitmap getCustomerImage(String customer_id) {
        String image_path = "", type = "Tab", image_name = "";
        String selectQuery = "select image_url,image_file_name,created_from from xxmsales_customer_details where customer_id ='" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            image_path = cursor.getString(0);
            image_name = cursor.getString(1);
            type = cursor.getString(2);
            cursor.close();
        }

        Bitmap b = null;
        try {
            if (type.equals("Tab")) {
                if (!TextUtils.isEmpty(image_path)) {
                    File f = new File(IMAGE_HOME_PATH + "images" + File.separator + image_path);
                    b = BitmapFactory.decodeStream(new FileInputStream(f));
                }
            } else {
                if (!TextUtils.isEmpty(image_name)) {
                    File f = new File(IMAGE_HOME_PATH + "images" + File.separator + "customerImages" + File.separator + "web" + File.separator + image_name);
                    b = BitmapFactory.decodeStream(new FileInputStream(f));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("Gallery", "error" + e.getMessage());
        }
        return b;
    }


    public Bitmap getTripImage(String trip_num) {
        String image_path = "", type = "Tab", image_name = "";
        String selectQuery = "select odometer_image,trip_file_name from xxmsales_trip_details where trip_number ='" + trip_num + "'";        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            image_path = cursor.getString(0);
            image_name = cursor.getString(1);
            cursor.close();
        }
        System.out.println("TTT::image_name1 = " + image_name);
        Bitmap b = null;
        try {
            if (!TextUtils.isEmpty(image_name)) {
                File f = new File(IMAGE_HOME_PATH + "images" + File.separator + "tripMileageImages" + File.separator + image_name);
                b = BitmapFactory.decodeStream(new FileInputStream(f));
                System.out.println("TTT::f = " + f);
                System.out.println("TTT::b = " + b);
                System.out.println("TTT::image_name = " + image_name);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("TTT::e = " + e);

            Log.d("Gallery", "error" + e.getMessage());
        }
        return b;
    }

    public int get_length_odometer_image(String trip_num) {
        int value = 0;
        String selectQuery = "select length(odometer_image) value from xxmsales_trip_details where trip_number ='" + trip_num + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

  /*  public Globals getsalesrepimage(String login_id) throws SQLException {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select * from xxmsales_user_setup where employee_id ='" + login_id+ "'",null);
        if (cur.moveToFirst()) {
            byte[] blob = cur.getBlob(cur.getColumnIndex(COLUMN_USER_IMAGE));
            cur.close();
            return new Globals(Utility.getPhoto(blob));
        }
        cur.close();
        return null;
    } */


    public List<String> getAllEmployeeId() {
        List<String> labels = new ArrayList<String>();

// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_SALESREP_DETAILS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
// labels.add("Please Select a Ticket");

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

// closing connection
        cursor.close();
        db.close();

// returning lables
        return labels;
    }


    public List<String> getAllLabels() {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ITEM_DETAILS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }


    public List<String> getInvoicenumbers(String customer_id) {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT invoice_number FROM " + TABLE_INVOICE_HEADER + " where customer_id='" + customer_id + "'" + " and invoice_status='Completed' and balance_amount <> 0 and balance_amount > 0 ";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

    public List<String> getPrimaryInvoicenumbers(String customer_id) {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT distinct invoice_number FROM " + TABLE_LEDGER + " where customer_id='" + customer_id + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

    public List<String> getPrimaryOrdernumbers(String customer_id) {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT distinct salesorder_number FROM xxmsales_order_header where customer_id='" + customer_id + "' and salesorder_status ='Completed'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

   /* //Get Customer List for the SalesRep
    public Cursor getProductSpinner(int order_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_item_details where  product_id not in (select product_id from xxmsales_order_details where salesorder_id=" + order_id + ") order by product_type", null);
        return data;
    } */

    //Get Customer List for the SalesRep
    public Cursor getProductSpinner(int order_id, String customer_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select distinct a.product_code,a.product_id,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not from xxmsales_price_list_lines a,xxmsales_price_list_header b,xxmsales_customer_details c where c.price_list_code =b.price_list_code and b.price_list_code=a.price_id and c.customer_id='" + customer_id + "'", null);
        return data;

    }

   /* //Get Customer List for the SalesRep
    public Cursor getProductSpinnerInvoice(int invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_item_details where  product_id not in (select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + ") order by product_type", null);
        return data;
    } */

    //Get Customer List for the SalesRep
   /* public Cursor getProductSpinnerInvoice(int invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_item_details a,xxmsales_stock_reciept b where a.product_id=b.sr_product_name  and a.product_id not in (select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + ") group by a.product_name having sum(b.sr_product_quantity) > 0 order by a.product_type", null);
        return data;
    } */

    //Get Customer List for the SalesRep
    public Cursor getProductSpinnerInvoice(int invoice_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_item_details a,xxmsales_stock_reciept b where a.product_id=b.sr_product_name  and a.product_id not in (select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + ") group by a.product_name having sum(b.sr_product_quantity) > 0 order by a.product_type", null);
        return data;

    }

    //Get CompanyDetails
    public ArrayList<OrderProductSpinner> getInvoiceProductSpinner(String customer_id, int invoice_id, String product_type, String invoice_type) {

        SQLiteDatabase db = this.getWritableDatabase();
        String dataQuery;
        if ("Invoice".equals(invoice_type)) {
            dataQuery = "select * from xxmsales_item_details a,xxmsales_stock_reciept b where a.price <> 0 and a.product_id=b.sr_product_name  and a.product_id not in (select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + ")";
            if (!product_type.equals("all"))
                dataQuery += " and a.product_type='" + product_type + "'";
            dataQuery += " group by a.product_name having sum(b.sr_product_quantity) > 0 order by a.product_type";
        } else {
            dataQuery = "select null,a.product_code,a.product_id,null,null,null,(select product_type from xxmsales_item_details where product_id=a.product_code) focused_or_not from xxmsales_price_list_lines a,xxmsales_price_list_header b where a.product_code not in (select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + ") and b.price_list_code=a.price_id and a.price_id IN (select distinct c.sr_price_id from xxmsales_sr_pricelist c)";
        }


        Cursor data = db.rawQuery(dataQuery, null);
        ArrayList<OrderProductSpinner> orderProductSpinnerArrayList = new ArrayList<OrderProductSpinner>();
        while (data.moveToNext()) {
            OrderProductSpinner cs = new OrderProductSpinner();
            cs.setProduct_id(data.getString(1));
            cs.setProduct(data.getString(2));
            cs.setProd_type(data.getString(6));

            orderProductSpinnerArrayList.add(cs);
        }
        return orderProductSpinnerArrayList;
    }


    public List<String> getOrderProducts(int order_id) {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "select * from xxmsales_item_details where  product_id not in (select product_id from xxmsales_order_details where salesorder_id=" + order_id + ") order by product_type";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

    public List<String> getInvoiceProducts(int invoice_id) {
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "select * from xxmsales_item_details where  product_id not in (select product_id from xxmsales_invoice_details where invoice_id=" + invoice_id + ") order by product_type";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // labels.add("Please Select a Ticket");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                labels.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

    public String getlatestOTP(String tabid, String userid) {
        String latestOTP = null;
//String selectQuery = "select * from " + TABLE_TAB_SETUP + " where " + COLUMN_TAB_ID + "='" + tabid + "' and " + COLUMN_USER_ID + "='" + userid + "'+ order by Id desc LIMIT 1";
        String selectQuery =
                "select * from xxmsales_tab_setup where tab_id ='" + tabid + "'" + "and user_id ='" + userid + "'" + " order by Id desc LIMIT 1 ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            latestOTP = cursor.getString(3);
            cursor.close();

        }

        return latestOTP;
    }

    public long create_tab_setup(String tabid, String userid, String tab_prefix, String base_url,String sec_price_list) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TAB_ID, tabid);
        values.put(COLUMN_USER_ID, userid);
        values.put(COLUMN_TAB_PREFIX, tab_prefix);
        values.put(COLUMN_BASE_URL, base_url);
        values.put(COLUMN_SEC_PRICE_LIST, sec_price_list);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_TAB_SETUP, null, values);
        db.close();
        return id;
    }

    public void update_tab_setup1(String tabid, String userid, String tab_prefix, String base_url,String sec_price_list) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TAB_ID, tabid);
        values.put(COLUMN_USER_ID, userid);
        values.put(COLUMN_TAB_PREFIX, tab_prefix);
        values.put(COLUMN_BASE_URL, base_url);
        values.put(COLUMN_SEC_PRICE_LIST, sec_price_list);


        db.update(TABLE_TAB_SETUP, values, null, null);
        db.close();
    }

   /* public int deleteOrderLines(String id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.delete(TABLE_SALESORDER_DETAILS, COLUMN_ODSALESORDER_LINE_ID + "=?", new String[]{id});

    } */

    public void deleteOrderLines(String id, String order_id) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_SALESORDER_DETAILS, COLUMN_ODSALESORDER_LINE_ID + "=" + id + " and " + COLUMN_OD_SALESORDER_ID + "=" + order_id, null);
        database.close();
    }

    public void deleteInvoiceLines(String id, String invoice_id) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_INVOICE_DETAILS, COLUMN_ID_INVOICE_LINE_ID + "=" + id + " and " + COLUMN_ID_INVOICE_ID + "=" + invoice_id, null);
        database.close();
    }

    public String getUserPassword(String email) {
        String userpassword = null;
        String selectQuery =
                "select * from xxmsales_user_setup where email_id ='" + email + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            userpassword = cursor.getString(4);
            cursor.close();

        }

        return userpassword;
    }

    public void update_invoice_details(String invoiceline_id, String invoice_id, String qty, String price, double value, double value_without_disc, double discount_value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID_PRICE, price);
        values.put(COLUMN_ID_QUANTITY, qty);
        values.put(COLUMN_ID_VALUE, value);
        values.put(COLUMN_ID_VALUE_WITHOUT_DIS, value_without_disc);
        values.put(COLUMN_ID_DISCOUNT_VALE, discount_value);

        db.update(TABLE_INVOICE_DETAILS, values,
                COLUMN_ID_INVOICE_ID + " = ? AND " + COLUMN_ID_INVOICE_LINE_ID + " = ?",
                new String[]{invoice_id, invoiceline_id});
        db.close();
    }

    public void update_order_details(String saleline_id, String order_id, String qty, double price, double value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_OD_PRICE, price);
        values.put(COLUMN_OD_QUANTITY, qty);
        values.put(COLUMN_OD_VALUE, value);
        db.update(TABLE_SALESORDER_DETAILS, values,
                COLUMN_OD_SALESORDER_ID + " = ? AND " + COLUMN_ODSALESORDER_LINE_ID + " = ?",
                new String[]{order_id, saleline_id});
        db.close();
    }




    //Get Total Qty
    public Cursor gettotalquantity() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select sum(b_temp_batch_quantity) from xxmsales_receipt_batch_row_temp", null);
        return data;
    }

    public int get_batch_total_qty() {
        int value = 0;
        String selectQuery = "select sum(b_temp_batch_quantity) from xxmsales_receipt_batch_row_temp";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }


    public Cursor getSalesOrderDetails(int order_id, String salesline_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select product_name,product_uom,quantity,price,value from xxmsales_order_details where salesorder_id ='" + order_id + "'" + "and salesorder_line_id='" + salesline_id + "'", null);
        return data;
    }

    public Cursor getBatchDetails(int invoice_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_product_batch_temp where b_invoice_id =" + invoice_id + " and b_batch_status='Draft' ", null);
        return data;
    }


    public int get_updatedbatch_quantity(String invoice_id, String product_id) {
        int value = 0;
        String selectQuery = "select sum(b_batch_invoice) value from xxmsales_product_batch where b_invoice_id='" + invoice_id + "'" + " and b_product_id=" + "'" + product_id + "'" + " and b_batch_status='Inprocess'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }


    public int get_productbatch_quantity(String product_id) {
        int value = 0;
        String selectQuery = "select sum(b_batch_quantity) value from xxmsales_product_batch where  b_product_id=" + "'" + product_id + "'" + " and b_batch_status='Completed'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public int get_stockreceipt_quantity(String product_id) {
        int value = 0;
        String selectQuery = "select sum(sr_product_quantity) value from xxmsales_stock_reciept where  sr_product_name=" + "'" + product_id + "'" + " and status='Completed'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

    public int get_batch_quantity(String invoice_id, String product_id) {
        int value = 0;
        String selectQuery = "select sum(b_batch_invoice) value from xxmsales_product_batch_temp where b_invoice_id=" + invoice_id + " and b_product_id=" + "'" + product_id + "'" + " and b_batch_status='Draft'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getInt(0);
            cursor.close();
        }

        return value;
    }

  /*  public void deletebatch(String product_id) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_PRODUCT_BATCH,  COLUMN_B_PRODUCT_ID + "=" + "'" + product_id + "'" + " and " + COLUMN_B_STATUS + " = 'Draft' ", null);
        database.close();
    } */

    public void deletebatch(String product_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from xxmsales_product_batch where b_product_id='" + product_id + "'" + " and b_batch_status in ('Draft','Inprocess') ");
        db.close();

    }


    public void deletebatchrow(int invoice_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from xxmsales_product_batch where b_invoice_id=" + invoice_id + " and b_batch_invoice is null ");
        db.close();

    }

    public void deletewaybillbatchrow(String orderNumber) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from xxmsales_product_batch where b_invoice_id='" + orderNumber + "'" + " and b_batch_invoice is null ");
        db.close();

    }

    public void deleteDeliveryDetails() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from xxmsales_waybill_details where waybill_status='Draft' ");
        db.close();

    }

    //Get Stock in Warehouse
    public Cursor getStockinWarehouse(String sales_rep_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select s_product_name,s_product_uom,s_product_quantity,s_product_price,s_product_value,s_product_order_count from xxmsales_stock_details where s_sr_code = '" + sales_rep_id + "'"
                , null);
        return data;
    }

    //Get Stock Details in stock
    public Cursor getStockDetailsinstock(String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor data = db.rawQuery("select b_batch_number,b_expiry_date,sum(b_batch_quantity),null b_batch_invoice,null b_batch_invoice from xxmsales_product_batch where b_product_id = '" + product_id + "'" + " group by b_product_id,b_batch_number"
                , null);
        return data;
    }

    //Get Stock Details in Warehouse with products
    public Cursor getWarehouseDetailswithProducts(String sales_rep_id, String warehouse_id, String product_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select s_product_name,s_product_uom,s_product_quantity,s_product_price,s_product_value,s_product_order_count from xxmsales_stock_details where s_sr_code = '" + sales_rep_id + "' and s_product_warehouse='" + warehouse_id + "'" + "and s_product_id='" + product_id + "'";
        Cursor data = db.rawQuery(selectQuery, null);
        return data;
    }

    public ArrayList<ViewStockWarehouseProduct> getWarehouseStockProducts(String sales_rep_id, String warehouse_id, String product_id, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select product_name,product_uom,product_quantity,product_price,product_value,(select count(*) from xxmsales_order_details where product_id = xxmsales_warehouse_stock_product.product_id and salesorder_date = '" + date + "') as ord_count, (select batch_controlled from xxmsales_item_details where product_id = xxmsales_warehouse_stock_product.product_id ) as batch_controlled,product_id from xxmsales_warehouse_stock_product WHERE (sales_rep_id='" + sales_rep_id + "' OR sales_rep_id IS NULL)";
        if (!product_id.equals("-1") && warehouse_id.equals("-1")) {
            selectQuery += " and product_id='" + product_id + "'";
        } else if (product_id.equals("-1") && !warehouse_id.equals("-1")) {
            selectQuery += " and warehouse_id='" + warehouse_id + "'";
        } else if (!product_id.equals("-1") && !warehouse_id.equals("-1")) {
            selectQuery += " and warehouse_id='" + warehouse_id + "'" + " and product_id='" + product_id + "'";
        }
        ArrayList<ViewStockWarehouseProduct> warehouseStockProductList = new ArrayList<ViewStockWarehouseProduct>();
        Log.d("SelectQuery", selectQuery);
        Cursor data = db.rawQuery(selectQuery, null);
        int numRows1 = data.getCount();
        while (data.moveToNext()) {
            ViewStockWarehouseProduct warehouseStockProduct = new ViewStockWarehouseProduct();
            warehouseStockProduct.setProductName(data.getString(0));
            warehouseStockProduct.setProductUom(data.getString(1));
            warehouseStockProduct.setProductQuantity(data.getString(2));
            warehouseStockProduct.setProductPrice(data.getString(3));
            warehouseStockProduct.setProductTotalPrice(data.getString(4));
            warehouseStockProduct.setPrimaryOrderCount(data.getString(5));
            warehouseStockProduct.setBatchControlled(data.getString(6));
            warehouseStockProduct.setProductCode(data.getString(7));
            String selectedProductBatch = data.getString(6);
            System.out.println("selectedProductBatch = " + selectedProductBatch);
          /*  if (selectedProductBatch.equals("Yes") ) {
                System.out.println("selectedProductBatch1 = " + selectedProductBatch);
                String id_p= "TABTENSICARD50MG";
                String selectQuery1 = "select b_expiry_date,b_batch_number,sum(b_batch_quantity), CASE WHEN Date(b_expiry_date)<=Date('now') THEN 'TRUE' ELSE 'FALSE' END isExpired from xxmsales_product_batch where b_batch_status='Completed' and b_product_id= '" + id_p + "'" + " group by b_product_id,b_batch_number having sum(b_batch_quantity)!=0";
               *//* if (!product_id.equals("-1") && warehouse_id.equals("-1")) {
                    selectQuery1 += " and product_id='" + product_id + "'";
                } else if (product_id.equals("-1") && !warehouse_id.equals("-1")) {
                    selectQuery1 += " and warehouse_id='" + warehouse_id + "'";
                } else if (!product_id.equals("-1") && !warehouse_id.equals("-1")) {
                    selectQuery1 += " and warehouse_id='" + warehouse_id + "'" + " and product_id='" + product_id + "'";
                }*//*
                Cursor data1 = db.rawQuery(selectQuery1, null);
//                Cursor data1 = db.rawQuery("select b_expiry_date,b_batch_number,sum(b_batch_quantity), CASE WHEN Date(b_expiry_date)<=Date('now') THEN 'TRUE' ELSE 'FALSE' END isExpired from xxmsales_product_batch where b_batch_status='Completed' and b_product_id= '" + product_id + "'" + " group by b_product_id,b_batch_number having sum(b_batch_quantity)!=0", null);
                ArrayList<StockReceiptBatch> stockReceiptBatchArrayList = new ArrayList<StockReceiptBatch>();
                while (data1.moveToNext()) {
                    StockReceiptBatch srp = new StockReceiptBatch();
                    srp.setExpiryDate(data1.getString(0));
                    srp.setBatchNumber(data1.getString(1));
                    srp.setBatchQuantity(data1.getString(2));
                    stockReceiptBatchArrayList.add(srp);
                }

                warehouseStockProduct.setBatchDetailArrayList(stockReceiptBatchArrayList);
            }*/

            warehouseStockProductList.add(warehouseStockProduct);
        }
        return warehouseStockProductList;
    }

    public ArrayList<CustomerDetails> getPrimaryCustomersByGroup(String customerGroup) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select * from " + TABLE_CUSTOMER_DETAILS + " where customer_type='Primary' and customer_group='" + customerGroup + "'GROUP BY customer_id ORDER BY customer_name asc";
        ArrayList<CustomerDetails> customerDetailsArrayList = new ArrayList<CustomerDetails>();
        Cursor data = db.rawQuery(selectQuery, null);
        int numRows1 = data.getCount();
        while (data.moveToNext()) {
            CustomerDetails customerDetails = new CustomerDetails();
            customerDetails.setCustomerCode(data.getString(1));
            customerDetails.setCustomerName(data.getString(2));
            customerDetailsArrayList.add(customerDetails);
        }
        return customerDetailsArrayList;
    }

    public ArrayList<ViewStockWarehouseProduct> getWarehouseStockProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT fb.feedback_date,fb.type,cust_det.customer_name,fb.feedback,fb.action_taken FROM " + TABLE_FEEDBACK + " as fb LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON fb.customer_id = cust_det.customer_id";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("QueryData cursor ", cursor.toString());

        ArrayList<ViewStockWarehouseProduct> feedbackReportArrayList = new ArrayList<ViewStockWarehouseProduct>();
        Log.d("QueryData Array ", feedbackReportArrayList.toString());

        int i = 0;

        cursor.close();
        sqlWrite.close();
        return feedbackReportArrayList;
    }


    public ArrayList<ViewStockMyStockProduct> getMyStockProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT fb.feedback_date,fb.type,cust_det.customer_name,fb.feedback,fb.action_taken FROM " + TABLE_FEEDBACK + " as fb LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON fb.customer_id = cust_det.customer_id";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("QueryData cursor ", cursor.toString());

        ArrayList<ViewStockMyStockProduct> myStockProductArrayList = new ArrayList<ViewStockMyStockProduct>();
        Log.d("QueryData Array ", myStockProductArrayList.toString());

        int i = 0;

        /*while (cursor.moveToNext()) {
            ViewStockWarehouseProduct feedbackReport = new ViewStockWarehouseProduct();
            feedbackReport.setFeedback_date(cursor.getString(0));
            feedbackReport.setFeedback_type(cursor.getString(1));
            feedbackReport.setCustomer_name(cursor.getString(2));
            feedbackReport.setCustomer_feedback(cursor.getString(3));
            feedbackReport.setAction_taken(cursor.getString(4));

            feedbackReportArrayList.add(i,feedbackReport);
            Log.d("QueryData","FbText " + feedbackReport.getCustomer_feedback().toString());

            i++;
        }*/
        cursor.close();
        sqlWrite.close();
        return myStockProductArrayList;
    }

    //Get Distributor Id using Distributor Name
    public Cursor getDistributorDetails(String distributorname) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select * from xxmsales_distributor where d_distributor_name ='" + distributorname + "'", null);
        return data;
    }

    //Get Customer Details
    public Cursor check_reciept_number(String temp_receipt_number) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from xxmsales_product_batch where b_batch_temp_number = '" + temp_receipt_number + "'"
                , null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            cursor.close();
        }

        return cursor;
    }


    //Insert Data to Stock Reciept Batch Table
    public long insertingProductBatch(String product_id, String batchnumber, String expirydate, String batchqty, String batchinvoice, String batch_recieptno, String batch_invoice_id, String status, String transactionType) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_B_PRODUCT_ID, product_id);
        values.put(COLUMN_B_BATCH_NUMBER, batchnumber);
        values.put(COLUMN_B_EXPIRY_DATE, expirydate);
        values.put(COLUMN_B_BATCH_QUANTITY, batchqty);
        values.put(COLUMN_B_BATCH_INVOICE, batchinvoice);
        values.put(COLUMN_B_RECEIPT_NO, batch_recieptno);
        values.put(COLUMN_B_INVOICE_ID, batch_invoice_id);
        values.put(COLUMN_B_STATUS, status);
        values.put(COLUMN_BATCH_TRANSACTION_TYPE, transactionType);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRODUCT_BATCH, null, values);
        db.close();
        return id;
    }

    public int get_reciept_id() {
        int reciept_id = 0;
        String selectQuery = "select sr_receipt_number from xxmsales_stock_reciept order by sr_receipt_number desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            reciept_id = cursor.getInt(0);
            cursor.close();
        }

        return reciept_id;
    }

    //Insert Data to Stock Reciept Table
    public long createstockreciept(String sr_receipt_number, String sr_date, String sr_currency, String sr_sr_name, String sr_source_name, String sr_invoice, String sr_source_type, String sr_product_name, String sr_product_uom, String sr_product_quantity, String sr_product_price, String sr_product_value,String sr_remarks, String invoice_id, String sr_status, int random_num, String visitSeqNumb, String referenceNumber, String transferredTo) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_STOCK_RECIEPT_NUMBER, sr_receipt_number);
        values.put(COLUMN_STOCK_RECIEPT_DATE, sr_date);
        values.put(COLUMN_STOCK_RECIEPT_CURRENCY, sr_currency);
        values.put(COLUMN_STOCK_RECIEPT_SR_NAME, sr_sr_name);
        values.put(COLUMN_SR_SOURCE_NAME, sr_source_name);
        values.put(COLUMN_SR_INVOICE, sr_invoice);
        values.put(COLUMN_SR_SOURCE_TYPE, sr_source_type);
        values.put(COLUMN_SR_PRODUCTNAME, sr_product_name);
        values.put(COLUMN_SR_PRODUCT_UOM, sr_product_uom);
        values.put(COLUMN_SR_PRODUCT_QUANTITY, sr_product_quantity);
        values.put(COLUMN_SR_PRODUCT_PRICE, sr_product_price);
        values.put(COLUMN_SR_PRODUCT_VALUE, sr_product_value);
        values.put(COLUMN_SR_REMARKS, sr_remarks);
        values.put(COLUMN_INVOICE_ID, invoice_id);
        values.put(COLUMN_STOCK_STATUS, sr_status);
        values.put(COLUMN_STOCK_RANDOM_NUM, random_num);
        values.put(COLUMN_STOCK_RECEIPT_VISIT_SEQUENCE_NO, visitSeqNumb);
        values.put(COLUMN_STOCK_RECEIPT_REFERENCE_NUMBER, referenceNumber);
        values.put(COLUMN_STOCK_RECEIPT_TRANSFERRED_TO, transferredTo);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_STOCK_RECIEPT, null, values);
        db.close();
        return id;
    }


    public String getCollectionNumber(String customer_type) {

        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery;

        if ("Secondary".equals(customer_type)) {
            selectQuery = "SELECT " + COLUMN_CL_COLLECTION_NUMBER + " FROM " + TABLE_COLLECTION_DETAILS + " ORDER BY " + COLUMN_CL_ID + " DESC LIMIT 1";

        } else {
            selectQuery = "SELECT " + COLUMN_CL_COLLECTION_NUMBER + " FROM " + TABLE_PRIMARY_COLLECTION_DETAILS + " ORDER BY " + COLUMN_CL_ID + " DESC LIMIT 1";

        }

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        String number = "";
        while (cursor.moveToNext()) {
            number = cursor.getString(0);
            break;
        }

        cursor.close();
        sqlWrite.close();
        String PATTERN = "y";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        if (number == null || number.equals("")) {

            number = "COL-" + date + "-" + "1";
        } else {
            String[] values = number.split("-");
            number = values[0] + "-" + date + "-" + String.valueOf(Integer.parseInt(values[2]) + 1);
        }
        return number;

    }

    String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

    public Cursor getWeeklyOrderValues(String[] days) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select sum(value) as order_value, salesorder_date from xxmsales_order_details where salesorder_date in (" + makePlaceholders(days.length + 1) + ") GROUP BY salesorder_date", days);
        return data;
    }

    public Cursor getWeeklyInvoiceValues(String[] days) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select sum(value) as invoice_value, invoice_date from xxmsales_invoice_details where invoice_date in (" + makePlaceholders(days.length) + ") GROUP BY invoice_date", days);
        return data;
    }

    public Cursor getWeeklyCollectionValues(String[] days) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("select sum(payment_amount) as collection_value, collection_date from xxmsales_collection_details where collection_date in (" + makePlaceholders(days.length) + ") GROUP BY collection_date", days);
        return data;
    }

    public double getOrderValuesByDate(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select sum(od.value) as order_value, od.salesorder_date from xxmsales_order_details as od JOIN xxmsales_order_header oh ON od.salesorder_id = oh.salesorder_id WHERE od.salesorder_date=? AND oh.salesorder_status ='Completed' GROUP BY od.salesorder_date", new String[]{date});
        double order_value = 0;
        while (cursor.moveToNext()) {
            order_value = Double.parseDouble(cursor.getString(0));
        }
        return order_value;
    }

    public double getInvoiceValuesByDate(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select sum(ind.value) as invoice_value, ind.invoice_date from xxmsales_invoice_details as ind JOIN xxmsales_invoice_header as ih ON ind.invoice_id=ih.invoice_id where ind.invoice_date=? AND ih.invoice_status='Completed' GROUP BY ind.invoice_date", new String[]{date});
        double invoice_value = 0;
        while (cursor.moveToNext()) {
            invoice_value = Double.parseDouble(cursor.getString(0));
        }
        return invoice_value;
    }

    public double getCollectionValuesByDate(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select sum(payment_amount) as collection_value, collection_date from xxmsales_collection_details where collection_date=? GROUP BY collection_date", new String[]{date});
        double collection_value = 0;
        while (cursor.moveToNext()) {
            collection_value = Double.parseDouble(cursor.getString(0));
        }
        return collection_value;
    }

    public double getPrimaryCollectionValuesByDate(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select sum(payment_amount) as collection_value, collection_date from xxmsales_primary_collection_details where collection_date=? GROUP BY collection_date", new String[]{date});
        double collection_value = 0;
        while (cursor.moveToNext()) {
            collection_value = Double.parseDouble(cursor.getString(0));
        }
        return collection_value;
    }
    //API Master (SAP to TAB)

    //Company Master

    public long create_company_master(String company_code, String company_name, String contact_person, String email, String mobile_number, String support_num1, String support_num2, String support_email, String address1, String address2, String address3, String imageName, String imageUrl) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_COMPANY_NAME, company_code);
        values.put(COLUMN_SHORT_NAME, company_name);
        values.put(COLUMN_CONTACT_PERSON, contact_person);
        values.put(COLUMN_EMAIL_ID, email);
        values.put(COLUMN_MOBILE_NUMBER, mobile_number);
        values.put(COLUMN_SUPPORT_NUMBER1, support_num1);
        values.put(COLUMN_SUPPORT_NUMBER2, support_num2);
        values.put(COLUMN_SUPPORT_EMAIL, support_email);
        values.put(COLUMN_COMPANY_LOGO, "");
        values.put(COLUMN_COMPANY_ADDRESS_LINE1, address1);
        values.put(COLUMN_COMPANY_ADDRESS_LINE2, address2);
        values.put(COLUMN_COMPANY_ADDRESS_LINE3, address3);
        values.put(COLUMN_COMPANY_LOGO_IMAGE_NAME, imageName);
        values.put(COLUMN_COMPANY_LOGO_IMAGE_URL, imageUrl);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_COMPANY_SETUP, null, values);
        db.close();
        return id;
    }

    public void update_Company_master(String company_code, String company_name, String contact_person, String email, String mobile_number, String support_num1, String support_num2, String support_email, String address1, String address2, String address3, String imageName, String imageUrl) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_COMPANY_NAME, company_code);
        values.put(COLUMN_SHORT_NAME, company_name);
        values.put(COLUMN_CONTACT_PERSON, contact_person);
        values.put(COLUMN_EMAIL_ID, email);
        values.put(COLUMN_MOBILE_NUMBER, mobile_number);
        values.put(COLUMN_SUPPORT_NUMBER1, support_num1);
        values.put(COLUMN_SUPPORT_NUMBER2, support_num2);
        values.put(COLUMN_SUPPORT_EMAIL, support_email);
        values.put(COLUMN_COMPANY_LOGO, "");
        values.put(COLUMN_COMPANY_ADDRESS_LINE1, address1);
        values.put(COLUMN_COMPANY_ADDRESS_LINE2, address2);
        values.put(COLUMN_COMPANY_ADDRESS_LINE3, address3);
        values.put(COLUMN_COMPANY_LOGO_IMAGE_NAME, imageName);
        values.put(COLUMN_COMPANY_LOGO_IMAGE_URL, imageUrl);


        db.update(TABLE_COMPANY_SETUP, values, null, null);
        db.close();
    }


    //Customer Master

    public long create_customer_master(String customer_id, String customer_name, String address1, String address2, String address3, String city, String state, String mobile_number, String credit_limit, String credit_days, String customer_type, String price_list, String pricelist_code, String created_from, String email, String location_name, String lga, String country, String creation_date, String random_num, String image_file_name, String image_url, String Customergroup, String customer_image, String distributorString,String distributorCodeString) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_CUSTOMER_ID, customer_id);
        values.put(COLUMN_CUSTOMER_NAME, customer_name);
        values.put(COLUMN_SALES_REP_ID, "");
        values.put(COLUMN_SALES_REP_NAME, "");
        values.put(COLUMN_SERIAL_NUM, "");
        values.put(COLUMN_ADDRESS1, address1);
        values.put(COLUMN_ADDRESS2, address2);
        values.put(COLUMN_ADDRESS3, address3);
        values.put(COLUMN_CITY, city);
        values.put(COLUMN_STATE, state);
        values.put(COLUMN_CUS_MOBILE_NUMBER, mobile_number);
        values.put(COLUMN_CUS_CREDIT_LIMIT, credit_limit);
        values.put(COLUMN_CUS_CREDIT_DAYS, credit_days);

        values.put(COLUMN_PINCODE, "");
        values.put(COLUMN_CUSTOMER_IMAGE_FILE_NAME, customer_image);
        values.put(COLUMN_SHIP_ADDRESS1, "");
        values.put(COLUMN_SHIP_ADDRESS2, "");
        values.put(COLUMN_SHIP_ADDRESS3, "");
        values.put(COLUMN_SHIP_CITY, "");
        values.put(COLUMN_SHIP_STATE, "");
        values.put(COLUMN_SHIP_COUNTRY, "");
        values.put(COLUMN_CUSTOMER_TYPE, customer_type);
        values.put(COLUMN_ACC_NUMBER, "");
        values.put(COLUMN_GPS_LATITUDE, "");
        values.put(COLUMN_GPS_LONGITUDE, "");
        values.put(COLUMN_C_PRICE_LIST_NAME, price_list);
        values.put(COLUMN_C_PRICE_LIST_CODE, pricelist_code);
        values.put(COLUMN_C_CREATED_FROM, created_from);
        values.put(COLUMN_C_EMAIL, email);
        values.put(COLUMN_C_LOCATION_NAME, location_name);
        values.put(COLUMN_C_LGA, lga);
        values.put(COLUMN_C_COUNTRY, country);
        values.put(COLUMN_C_CREATION_DATE, creation_date);
        values.put(COLUMN_CU_RANDOM_NUM, random_num);
        values.put(COLUMN_C_CUSTOMER_IMAGE_NAME, image_file_name);
        values.put(COLUMN_C_CUSTOMER_IMAGE_URL, image_url);
        values.put(COLUMN_C_CUSTOMER_GROUP, Customergroup);
        values.put(COLUMN_NEW_CUSTOMER_DISTRIBUTOR_NAME, distributorString);
        values.put(COLUMN_NEW_CUSTOMER_DISTRIBUTOR_CODE, distributorCodeString);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CUSTOMER_DETAILS, null, values);
        db.close();
        return id;
    }


    public long update_customer_master(String customer_id, String customer_name, String address1, String address2, String address3, String city, String state, String mobile_number, String credit_limit, String credit_days, String customer_type, String price_list, String pricelist_code, String created_from, String email, String location_name, String lga, String country, String creation_date, String random_num, String image_file_name, String image_url, String Customergroup, String customer_image, String distributorString,String distributorCodeString) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(COLUMN_CUSTOMER_ID, customer_id);
        values.put(COLUMN_CUSTOMER_NAME, customer_name);
        values.put(COLUMN_SALES_REP_ID, "");
        values.put(COLUMN_SALES_REP_NAME, "");
        values.put(COLUMN_SERIAL_NUM, "");
        values.put(COLUMN_ADDRESS1, address1);
        values.put(COLUMN_ADDRESS2, address2);
        values.put(COLUMN_ADDRESS3, address3);
        values.put(COLUMN_CITY, city);
        values.put(COLUMN_STATE, state);
        values.put(COLUMN_CUS_MOBILE_NUMBER, mobile_number);
        values.put(COLUMN_CUS_CREDIT_LIMIT, credit_limit);
        values.put(COLUMN_CUS_CREDIT_DAYS, credit_days);

        values.put(COLUMN_PINCODE, "");
        values.put(COLUMN_CUSTOMER_IMAGE_FILE_NAME, customer_image);
        values.put(COLUMN_SHIP_ADDRESS1, "");
        values.put(COLUMN_SHIP_ADDRESS2, "");
        values.put(COLUMN_SHIP_ADDRESS3, "");
        values.put(COLUMN_SHIP_CITY, "");
        values.put(COLUMN_SHIP_STATE, "");
        values.put(COLUMN_SHIP_COUNTRY, "");
        values.put(COLUMN_CUSTOMER_TYPE, customer_type);
        values.put(COLUMN_ACC_NUMBER, "");
        values.put(COLUMN_GPS_LATITUDE, "");
        values.put(COLUMN_GPS_LONGITUDE, "");
        values.put(COLUMN_C_PRICE_LIST_NAME, price_list);
        values.put(COLUMN_C_PRICE_LIST_CODE, pricelist_code);
        values.put(COLUMN_C_CREATED_FROM, created_from);
        values.put(COLUMN_C_EMAIL, email);
        values.put(COLUMN_C_LOCATION_NAME, location_name);
        values.put(COLUMN_C_LGA, lga);
        values.put(COLUMN_C_COUNTRY, country);
        values.put(COLUMN_C_CREATION_DATE, creation_date);
        values.put(COLUMN_CU_RANDOM_NUM, "");
        values.put(COLUMN_C_CUSTOMER_IMAGE_NAME, image_file_name);
        values.put(COLUMN_C_CUSTOMER_IMAGE_URL, image_url);
        values.put(COLUMN_C_CUSTOMER_GROUP, Customergroup);
        values.put(COLUMN_NEW_CUSTOMER_DISTRIBUTOR_NAME, distributorString);
        values.put(COLUMN_NEW_CUSTOMER_DISTRIBUTOR_CODE, distributorCodeString);

        long id = db.update(TABLE_CUSTOMER_DETAILS, values, COLUMN_CUSTOMER_ID + " = ?  " , new String[]{customer_id});
        db.close();
        return id;
    }

    /*
     * Fetching the Customer details from the Master via Web Service
     */

    public String getCustomerInfo_id(String customer_id) {
        String value = "";
        String selectQuery = "select id from xxmsales_customer_details where customer_id= '" + customer_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }

    public void insertingCustomerMaster(ArrayList<CustomerMaster> customerMasterList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (customerMasterList != null) {
                for (int i = 0; i < customerMasterList.size(); i++) {

                    values.put(COLUMN_CUSTOMER_ID, customerMasterList.get(i).getCustomer_code());
                    values.put(COLUMN_CUSTOMER_NAME, customerMasterList.get(i).getCustomer_name());
                    values.put(COLUMN_SALES_REP_ID, "");
                    values.put(COLUMN_SALES_REP_NAME, "");
                    values.put(COLUMN_SERIAL_NUM, "");
                    values.put(COLUMN_ADDRESS1, customerMasterList.get(i).getBill_address_line1());
                    values.put(COLUMN_ADDRESS2, customerMasterList.get(i).getBill_address_line2());
                    values.put(COLUMN_ADDRESS3, customerMasterList.get(i).getBill_address_line3());
                    values.put(COLUMN_CITY, customerMasterList.get(i).getBill_city());
                    values.put(COLUMN_STATE, customerMasterList.get(i).getBill_state());
                    values.put(COLUMN_CUS_MOBILE_NUMBER, customerMasterList.get(i).getContact_no());
                    values.put(COLUMN_CUS_CREDIT_LIMIT, customerMasterList.get(i).getCredit_limit());
                    values.put(COLUMN_CUS_CREDIT_DAYS, customerMasterList.get(i).getCredit_days());

                    values.put(COLUMN_PINCODE, "");
                    values.put(COLUMN_CUSTOMER_IMAGE_FILE_NAME, "");
                    values.put(COLUMN_SHIP_ADDRESS1, "");
                    values.put(COLUMN_SHIP_ADDRESS2, "");
                    values.put(COLUMN_SHIP_ADDRESS3, "");
                    values.put(COLUMN_SHIP_CITY, "");
                    values.put(COLUMN_SHIP_STATE, "");
                    values.put(COLUMN_SHIP_COUNTRY, "");
                    values.put(COLUMN_CUSTOMER_TYPE, customerMasterList.get(i).getCustomer_type());
                    values.put(COLUMN_ACC_NUMBER, "");
                    values.put(COLUMN_GPS_LATITUDE, customerMasterList.get(i).getGps_latitude());
                    values.put(COLUMN_GPS_LONGITUDE, customerMasterList.get(i).getGps_longitude());
                    values.put(COLUMN_C_PRICE_LIST_NAME, customerMasterList.get(i).getPrice_list());
                    values.put(COLUMN_C_PRICE_LIST_CODE, customerMasterList.get(i).getPrice_list_code());
                    values.put(COLUMN_C_CREATED_FROM, "Web");
                    values.put(COLUMN_C_LOCATION_NAME, customerMasterList.get(i).getLocation_name());
                    values.put(COLUMN_C_EMAIL, customerMasterList.get(i).getEmail());
                    values.put(COLUMN_C_LGA, "");
                    values.put(COLUMN_C_COUNTRY, "");
                    values.put(COLUMN_C_CREATION_DATE, "");
                    values.put(COLUMN_CU_RANDOM_NUM, "");
                    values.put(COLUMN_CREDIT_TERM, customerMasterList.get(i).getCredit_term());
                    values.put(COLUMN_PAYMENT_TERM, customerMasterList.get(i).getPayment_term());
                    values.put(COLUMN_C_CUSTOMER_IMAGE_NAME, customerMasterList.get(i).getCustomer_image());
                    values.put(COLUMN_C_CUSTOMER_IMAGE_URL, customerMasterList.get(i).getImage_url());
                    values.put(COLUMN_C_CUSTOMER_GROUP, customerMasterList.get(i).getCustomerGroup());
                    long id = db.insert(TABLE_CUSTOMER_DETAILS, null, values);
                }

                db.setTransactionSuccessful();

            }

        } finally {
            db.endTransaction();
        }
    }


    public void insertingMetricsTargetMaster(ArrayList<MetricsTarget> metricsTargetList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (metricsTargetList != null) {
                for (int i = 0; i < metricsTargetList.size(); i++) {

                    values.put(COLUMN_MT_TARGET_ID, metricsTargetList.get(i).getTargetID());
                    values.put(COLUMN_MT_TARGET_CODE, metricsTargetList.get(i).getTargetCode());
                    values.put(COLUMN_MT_TARGET_TYPE, metricsTargetList.get(i).getTargetType());
                    values.put(COLUMN_MT_TARGET_VALUE, metricsTargetList.get(i).getTargetValue());
                    values.put(COLUMN_MT_TYPE, metricsTargetList.get(i).getType());
                    values.put(COLUMN_MT_PRODUCT, metricsTargetList.get(i).getProduct());
                    values.put(COLUMN_MT_VALUE, metricsTargetList.get(i).getValue());
                    values.put(COLUMN_MT_FROM_DATE, metricsTargetList.get(i).getFromDate());
                    values.put(COLUMN_MT_TO_DATE, metricsTargetList.get(i).getToDate());
                    values.put(COLUMN_MT_CUST_TYPE, metricsTargetList.get(i).getcType());

                    long id = db.insert(TABLE_METRICS_TARGET, null, values);
                }

                db.setTransactionSuccessful();

            }

        } finally {
            db.endTransaction();
        }
    }


    public long create_customer_details(String customer_id, String customer_name, String address1, String address2, String address3, String city, String state, String mobile_number, String customer_type, byte[] customer_image) {
        long id = 0;
        try {


            ContentValues values = new ContentValues();

            values.put(COLUMN_CUSTOMER_ID, customer_id);
            values.put(COLUMN_CUSTOMER_NAME, customer_name);

            SQLiteDatabase db = getWritableDatabase();
            id = db.insert(TABLE_CUSTOMER_DETAILS, null, values);
            db.close();

        } catch (Exception e) {
            Log.d("LocationData", e.getMessage());
        }
        return id;
    }
    // Price List Header

    public long create_price_list_header(String price_list_code, String price_list_name) {

        ContentValues values = new ContentValues();

        // values.put(COLUMN_PRICE_ID, price_id);
        values.put(COLUMN_PRICE_LIST_CODE, price_list_code);
        values.put(COLUMN_PRICE_LIST_NAME, price_list_name);
        // values.put(COLUMN_PRICE_DESCRIPTION, price_description);
        //values.put(COLUMN_PRICE_LIST_TYPE, price_list_type);
        // values.put(COLUMN_EFFECTIVE_START_DATE, effective_start_date);
        // values.put(COLUMN_EFFECTIVE_END_DATE, effective_end_date);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRICE_LIST_HEADER, null, values);
        db.close();
        return id;
    }



    /*
     * Fetching the Price List details from the Master via Web Service
     */

    public void insertingPriceListsHeaderMaster(ArrayList<PriceListHeader> price_headerList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (price_headerList != null) {
                for (int i = 0; i < price_headerList.size(); i++) {
                    values.put(COLUMN_PRICE_LIST_CODE, price_headerList.get(i).getPrice_list_code());
                    values.put(COLUMN_PRICE_LIST_NAME, price_headerList.get(i).getPrice_list_name());
                    db.insert(TABLE_PRICE_LIST_HEADER, null, values);
                }
                db.setTransactionSuccessful();

            }


        } finally {
            db.endTransaction();
        }
    }


    //Price List Lines
    public long create_price_list_lines(String price_id, String product_id, String product_code, String price) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_PL_PRICE_ID, price_id);
        values.put(COLUMN_PL_PRODUCT_ID, product_id);
        values.put(COLUMN_PL_PRODUCT_CODE, product_code);
        values.put(COLUMN_PL_PRICE, price);
        //values.put(COLUMN_PL_PRICE1, price2);
        //values.put(COLUMN_PL_EFFECTIVE_START_DATE, effective_start_date);
        //values.put(COLUMN_PL_EFFECTIVE_END_DATE, effective_end_date);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRICE_LIST_LINES, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Price List Lines details from the Master via Web Service
     */

    public void insertingPriceListLinesMaster(ArrayList<PriceListLines> price_lineList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (price_lineList != null) {
                for (int i = 0; i < price_lineList.size(); i++) {

                    values.put(COLUMN_PL_PRICE_ID, price_lineList.get(i).getPrice_code());
                    values.put(COLUMN_PL_PRODUCT_ID, price_lineList.get(i).getProduct_name());
                    values.put(COLUMN_PL_PRODUCT_CODE, price_lineList.get(i).getProduct_code());
                    values.put(COLUMN_PL_PRICE, price_lineList.get(i).getPrice());
                    //values.put(COLUMN_PL_PRICE1, price2);
                    //values.put(COLUMN_PL_EFFECTIVE_START_DATE, effective_start_date);
                    //values.put(COLUMN_PL_EFFECTIVE_END_DATE, effective_end_date);
                    db.insert(TABLE_PRICE_LIST_LINES, null, values);
                }

                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }

    }


    //Item Master
    public long create_item_master(String product_id, String product_name, String long_description, String stock_uom, String sale_uom, String batch, String focus, String price) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_PRODUCT_ID, product_id);
        values.put(COLUMN_PRODUCT_NAME, product_name);
        values.put(COLUMN_PRODUCT_DESC, long_description);
        values.put(COLUMN_STOCK_UOM, stock_uom);
        values.put(COLUMN_PRODUCT_UOM, sale_uom);
        values.put(COLUMN_PRODUCT_TYPE, focus);
        values.put(COLUMN_BATCH_CONTROLLED, batch);
        values.put(COLUMN_PRICE, price);

        //values.put(COLUMN_PL_PRICE1, price2);
        //values.put(COLUMN_PL_EFFECTIVE_START_DATE, effective_start_date);
        //values.put(COLUMN_PL_EFFECTIVE_END_DATE, effective_end_date);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_ITEM_DETAILS, null, values);
        db.close();
        return id;

    }


    /*
     * Fetching the Products details from the Master via Web Service
     */
    public void insertingProductsMaster(ArrayList<ItemMaster> itemMastersList) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            if (itemMastersList != null)
                for (int i = 0; i < itemMastersList.size(); i++) {
                    values.put(COLUMN_PRODUCT_ID, itemMastersList.get(i).getProduct_id());
                    values.put(COLUMN_PRODUCT_NAME, itemMastersList.get(i).getProduct_name());
                    values.put(COLUMN_PRODUCT_DESC, itemMastersList.get(i).getLong_description());
                    values.put(COLUMN_STOCK_UOM, itemMastersList.get(i).getStock_uom());
                    values.put(COLUMN_PRODUCT_UOM, itemMastersList.get(i).getSale_uom());
                    values.put(COLUMN_PRODUCT_TYPE, itemMastersList.get(i).getFocus());
                    values.put(COLUMN_BATCH_CONTROLLED, itemMastersList.get(i).getBatch());
                    values.put(COLUMN_PRICE, itemMastersList.get(i).getItem_cost());
                    values.put(COLUMN_CONVERSION, itemMastersList.get(i).getConversion());

                    db.insert(TABLE_ITEM_DETAILS, null, values);
                }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }


    }


    //Shipping Address Master
    public long create_shipping_address_master(String customer_id, String address1, String address2, String address3, String city, String state) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_ID, customer_id);
        values.put(COLUMN_SHIP_ADDRESS1, address1);
        values.put(COLUMN_SHIP_ADDRESS2, address2);
        values.put(COLUMN_SHIP_ADDRESS3, address3);
        values.put(COLUMN_SHIP_CITY, city);
        values.put(COLUMN_SHIP_STATE, state);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SHIPPING_ADDRESS, null, values);
        db.close();
        return id;
    }

    // Locations Header

    public long create_location_header(String location_id, String location_name) {

        ContentValues values = new ContentValues();

        // values.put(COLUMN_PRICE_ID, price_id);
        values.put(COLUMN_LOCATION_ID, location_id);
        values.put(COLUMN_LOCATION_NAME, location_name);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_LOCATION_HEADER, null, values);
        db.close();
        return id;
    }

    //Price Locations Lines

    public long create_location_lines(String location_id, String loc_name, String lga, String city, String state) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_L_LOCATION_ID, location_id);
        values.put(COLUMN_L_LOCATION_NAME, loc_name);
        values.put(COLUMN_L_LGA, lga);
        values.put(COLUMN_L_CITY, city);
        values.put(COLUMN_L_STATE, state);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_LOCATION_LINES, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Location List details from the Master via Web Service
     */
    public void insertingLocationsMaster(ArrayList<LocationMaster> locationList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (locationList != null) {
                for (int i = 0; i < locationList.size(); i++) {
                    values.put(COLUMN_L_LOCATION_ID, locationList.get(i).getLocation_code());
                    values.put(COLUMN_L_LOCATION_NAME, locationList.get(i).getLocation_name());
                    values.put(COLUMN_L_LGA_ID, locationList.get(i).getLga_code());
                    values.put(COLUMN_L_LGA, locationList.get(i).getLga_name() );
                    values.put(COLUMN_L_CITY_ID,locationList.get(i).getCity_code() );
                    values.put(COLUMN_L_CITY, locationList.get(i).getCity_name());
                    values.put(COLUMN_L_STATE_ID, locationList.get(i).getState_code());
                    values.put(COLUMN_L_STATE, locationList.get(i).getState_name());
                    values.put(COLUMN_L_COUNTRY_ID, locationList.get(i).getCountry_code());
                    values.put(COLUMN_L_COUNTRY, locationList.get(i).getCountry_name());

                    db.insert(TABLE_LOCATION_LINES, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }



    public void insertingSrInvoiceBacthMaster(ArrayList<SrInvoiceBatch> srInvoiceBatchList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (srInvoiceBatchList != null) {
                for (int i = 0; i < srInvoiceBatchList.size(); i++) {
                    values.put(COLUMN_SR_BATCH_PRODUCT_ID, srInvoiceBatchList.get(i).getProductCode());
                    values.put(COLUMN_SR_BATCH_PRODUCT_NAME, srInvoiceBatchList.get(i).getProductName());
                    values.put(COLUMN_SR_BATCH_EXPIRY_DATE, srInvoiceBatchList.get(i).getExpiryDate());
                    values.put(COLUMN_SR_BATCH_BATCH_NUMBER, srInvoiceBatchList.get(i).getBatchName() );
                    values.put(COLUMN_SR_BATCH_BATCH_QUANTITY,srInvoiceBatchList.get(i).getBatchQuantity() );
                    values.put(COLUMN_SR_BATCH_INVOICE_ID, srInvoiceBatchList.get(i).getInvoiceNumber());

                    db.insert(TABLE_SR_INVOICE_BATCH, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }




    // Journey Plan Master
    public long create_journey_plan(String seq_num, String customer_id, String customer_name, String location, String customer_type, String gps_latitude, String gps_longitude) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_JP_SEQ_NUM, seq_num);
        values.put(COLUMN_JP_CUSTOMER_CODE, customer_id);
        values.put(COLUMN_JP_CUSTOMER_NAME, customer_name);
        values.put(COLUMN_JP_LOCATION, location);
        values.put(COLUMN_JP_CUSTOMER_TYPE, customer_type);
        values.put(COLUMN_JP_GPS_LATITUDE, gps_latitude);
        values.put(COLUMN_JP_GPS_LONGITUDE, gps_longitude);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_JOURNEY_PLAN_MASTER, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Journey Plan Customer Lists from the Master via Web Service
     */

    public void insertingJourneyPlanMaster(ArrayList<JourneyPlanMaster> journeyplanList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (journeyplanList != null) {
                for (int i = 0; i < journeyplanList.size(); i++) {

                    values.put(COLUMN_JP_SEQ_NUM, journeyplanList.get(i).getSeq_no());
                    values.put(COLUMN_JP_CUSTOMER_CODE, journeyplanList.get(i).getCustomer_code());
                    values.put(COLUMN_JP_CUSTOMER_NAME, journeyplanList.get(i).getCustomer_name());
                    values.put(COLUMN_JP_LOCATION, journeyplanList.get(i).getLocation());
                    values.put(COLUMN_JP_CUSTOMER_TYPE, journeyplanList.get(i).getCustomer_type());
                    values.put(COLUMN_JP_GPS_LATITUDE, journeyplanList.get(i).getGps_latitude());
                    values.put(COLUMN_JP_GPS_LONGITUDE, journeyplanList.get(i).getGps_longitude());

                    long id = db.insert(TABLE_JOURNEY_PLAN_MASTER, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }


    }


    //Insert Data to Order Details
    public long insert_waybill_details(String sales_rep_id, String customer_id, String salesorder_id, int salesorder_line_id, String salesorder_number, String salesorder_date, String salesorder_status, String product_id, String product_name, String product_uom, String order_quantity, double order_price, double order_value, String waybill_number, String waybill_date, String waybill_quantity, int waybill_random_num, String waybill_status) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_DC_SALES_REP_ID, sales_rep_id);
        values.put(COLUMN_DC_CUSTOMER_ID, customer_id);
        values.put(COLUMN_DC_SALESORDER_ID, salesorder_id);
        values.put(COLUMN_DC_SALESORDER_NUMBER, salesorder_number);
        values.put(COLUMN_DC_SALESORDER_LINE_ID, salesorder_line_id);
        values.put(COLUMN_DC_SALESORDER_DATE, salesorder_date);
        values.put(COLUMN_DC_SALESORDER_STATUS, salesorder_status);
        values.put(COLUMN_DC_PRODUCT_ID, product_id);
        values.put(COLUMN_DC_PRODUCT_NAME, product_name);
        values.put(COLUMN_DC_PRODUCT_UOM, product_uom);
        values.put(COLUMN_DC_QUANTITY, order_quantity);
        values.put(COLUMN_DC_PRICE, order_price);
        values.put(COLUMN_DC_VALUE, order_value);
        values.put(COLUMN_DC_WAYBILL_NUMBER, waybill_number);
        values.put(COLUMN_DC_WAYBILL_DATE, waybill_date);
        values.put(COLUMN_DC_WAYBILL_QUANTITY, waybill_quantity);
        values.put(COLUMN_DC_WAYBILL_STATUS, waybill_status);
        values.put(COLUMN_DC_RANDOM_NUMBER, waybill_random_num);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_WAYBILL_DETAILS, null, values);
        db.close();
        return id;
    }

    public long create_sr_pricelist(String price_id, String sr_code, String fileUrl, String fileName) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_SR_PRICE_ID, price_id);
        values.put(COLUMN_SR_CODE, sr_code);
        values.put(COLUMN_SR_IMAGE_FILE_URL, fileUrl);
        values.put(COLUMN_SR_IMAGE_FILE_NAME, fileName);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SR_PRICELIST, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the BDE Price Lists details from the Master via Web Service
     */

    public void insertingBdePriceListsMaster(ArrayList<SRPriceList> srPriceListArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (srPriceListArrayList != null) {
                for (int i = 0; i < srPriceListArrayList.size(); i++) {

                    values.put(COLUMN_SR_PRICE_ID, srPriceListArrayList.get(i).getPrice_id());
                    values.put(COLUMN_SR_CODE, srPriceListArrayList.get(i).getSr_code());
                    values.put(COLUMN_SR_IMAGE_FILE_URL, srPriceListArrayList.get(i).getFileUrl());
                    values.put(COLUMN_SR_IMAGE_FILE_NAME, srPriceListArrayList.get(i).getFileName());

                    db.insert(TABLE_SR_PRICELIST, null, values);

                }

                db.setTransactionSuccessful();

            }


        } finally {
            db.endTransaction();
        }

    }


    // Journey Plan Master

    public long create_salesrep_details(String sr_code, String sr_name, String manager, String region) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_SALES_REP_ID, sr_code);
        values.put(COLUMN_SALES_REP_NAME, sr_name);
        values.put(COLUMN_MANAGER, manager);
        values.put(COLUMN_REGION, region);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SALESREP_DETAILS, null, values);
        db.close();
        return id;
    }

    public void update_salesrep_details1(String sr_code, String sr_name, String manager, String region) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALES_REP_ID, sr_code);
        values.put(COLUMN_SALES_REP_NAME, sr_name);
        values.put(COLUMN_MANAGER, manager);
        values.put(COLUMN_REGION, region);


        db.update(TABLE_SALESREP_DETAILS, values, null, null);
        db.close();
    }


    public long create_gps_tracker(Double latitude, Double longitude, String gps_date, String gps_time, int gps_id) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_LATITUDE, latitude);
        values.put(COLUMN_LONGITUDE, longitude);
        values.put(COLUMN_GPS_DATE, gps_date);
        values.put(COLUMN_GPS_TIME, gps_time);
        values.put(COLUMN_GPS_ID, gps_id);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_GPS_TRACKER, null, values);
        db.close();
        return id;
    }

    public long create_feedback_type(String feedback_type,String feedback_id,String tab_code,String bde_code,
                                     String customer_code,String action_taken,String status) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_FEEDBACK_ID, feedback_id);
        values.put(COLUMN_FEEDBACK_TYPE, feedback_type);
        values.put(COLUMN_FEEDBACK_TAB_CODE, tab_code);
        values.put(COLUMN_FEEDBACK_BDE_CODE, bde_code);
        values.put(COLUMN_FEEDBACK_CUSTOMER_CODE, customer_code);
        values.put(COLUMN_FEEDBACK_ACTION_TAKEN, action_taken);
        values.put(COLUMN_FEEDBACK_STATUS, status);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_FEEDBACK_TYPE, null, values);
        db.close();
        return id;
    }

    /*
     * Fetching the Feedback Type List details from the Master via Web Service
     */

    public void insertingFeedbackTypeMaster(ArrayList<FeedbackType> feedbackList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (feedbackList != null) {
                for (int i = 0; i < feedbackList.size(); i++) {

                    values.put(COLUMN_FEEDBACK_ID, feedbackList.get(i).getFeedback_id());
                    values.put(COLUMN_FEEDBACK_TYPE, feedbackList.get(i).getFeedback_type());
                    values.put(COLUMN_FEEDBACK_TAB_CODE, feedbackList.get(i).getTab_code());
                    values.put(COLUMN_FEEDBACK_BDE_CODE, feedbackList.get(i).getBde_code());
                    values.put(COLUMN_FEEDBACK_CUSTOMER_CODE, feedbackList.get(i).getCustomer_code());
                    values.put(COLUMN_FEEDBACK_ACTION_TAKEN, feedbackList.get(i).getAction_taken());
                    values.put(COLUMN_FEEDBACK_STATUS, feedbackList.get(i).getStatus());

                    db.insert(TABLE_FEEDBACK_TYPE, null, values);
                }
                db.setTransactionSuccessful();

            }


        } finally {
            db.endTransaction();
        }

    }


    public long create_global_message(String message) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_GLOBAL_MESSAGE_NAME, message);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_GLOBAL_MESSAGE, null, values);
        db.close();
        return id;
    }

    /*
     * Fetching the Feedback Message Lists details from the Master via Web Service
     */
    public void insertingFooterMarqueeMsgText(ArrayList<MarqueeText> marqueeTexts) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            if (marqueeTexts != null) {
                for (int i = 0; i < marqueeTexts.size(); i++) {

                    values.put(COLUMN_GLOBAL_MESSAGE_NAME, marqueeTexts.get(i).getMsgContent());
                    values.put(COLUMN_GLOBAL_MESSAGE_CREATION_DATE, marqueeTexts.get(i).getCreated_date());
                    db.insert(TABLE_GLOBAL_MESSAGE, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }


    }


    public long createMasterDownload(MDLSync mdlSync) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_MASTER_DOWNLOAD, mdlSync.getIsDownloaded());

        values.put(COLUMN_MASTER_SYNC_DATE, mdlSync.getSyncDate());
        values.put(COLUMN_MASTER_SYNC_TIME, mdlSync.getSyncTime());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_MASTER_DOWNLOAD, null, values);
        db.close();
        return id;
    }


    public long create_master_sequence() {
        ContentValues values = new ContentValues();

        values.put(COLUMN_ORDER_SEQ_VALUE, 0);
        values.put(COLUMN_INVOICE_SEQ_VALUE, 0);
        values.put(COLUMN_COLLECTION_SEQ_VALUE, 0);
        values.put(COLUMN_RECEIPT_NUM_SEQ_VALUE, 0);
        values.put(COLUMN_TRIP_SEQ_VALUE, 0);
        values.put(COLUMN_CUSTOMER_SEQ_VALUE, 0);
        values.put(COLUMN_PRIMARY_COLLECTION_SEQ_VALUE, 0);
        values.put(COLUMN_SPONSOR_SEQ_VALUE, 0);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_MASTER_SEQUENCE, null, values);
        db.close();
        return id;
    }


    //Insert Data to Order Header
    public long insert_dr_visit_sponsorship(int sponsorship_id, String type, String event, String from_date, String to_date, String budget_amount, String customer_id, String random_num, String status) {

        ContentValues values = new ContentValues();

        values.put(COLUMN_DV_SPONSORSHIP_ID, sponsorship_id);
        values.put(COLUMN_DV_SPONSORSHIP_TYPE, type);
        values.put(COLUMN_DV_SPONSORSHIP_EVENT, event);
        values.put(COLUMN_DV_SPONSORSHIP_FROM_DATE, from_date);
        values.put(COLUMN_DV_SPONSORSHIP_TO_DATE, to_date);
        values.put(COLUMN_DV_SPONSORSHIP_BUDGET_AMOUNT, budget_amount);
        values.put(COLUMN_DV_SPONSORSHIP_CUSTOMER_ID, customer_id);
        values.put(COLUMN_DV_SPONSORSHIP_RANDOM_NUM, 0);
        values.put(COLUMN_DV_SPONSORSHIP_STATUS, status);


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_DOCTOR_VISIT_SPONSORSHIP, null, values);
        db.close();
        return id;
    }

    public long createQuarantine(Quarantine quarantine) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_Q_PRODUCT_CODE, quarantine.getProductCode());
        values.put(COLUMN_Q_BATCH_NUMBER, quarantine.getBatchNumber());
        values.put(COLUMN_Q_BATCH_EXPIRY_DATE, quarantine.getExpiryDate());
        values.put(COLUMN_Q_BATCH_QUANTITY, quarantine.getBatchQuantity());
        values.put(COLUMN_Q_QUARANTINE_QUANTITY, quarantine.getQuaratineQuantity());
        values.put(COLUMN_Q_QUARANTINE_DATE, quarantine.getQuaratineDate());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_QUARANTINE_PRODUCTS, null, values);
        db.close();
        return id;
    }

    public ArrayList<DoctorVisitSponsorship> getSponsorshiptype() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT type FROM " + TABLE_DR_VISIT_SPONSORSHIP_TYPE;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        ArrayList<DoctorVisitSponsorship> doctorVisitSponsorshipArrayList = new ArrayList<DoctorVisitSponsorship>();
        while (cursor.moveToNext()) {
            DoctorVisitSponsorship doctorVisitSponsorship = new DoctorVisitSponsorship();
            doctorVisitSponsorship.setType(cursor.getString(0));
            doctorVisitSponsorshipArrayList.add(doctorVisitSponsorship);
        }
        cursor.close();
        sqlWrite.close();
        return doctorVisitSponsorshipArrayList;
    }


    /**
     * Fetching the price Lists for CRM products
     */


    public ArrayList<CrmPriceList> getCrmProductsPriceList() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT fb.feedback_date,fb.type,cust_det.customer_name,fb.feedback,fb.action_taken FROM " + TABLE_FEEDBACK + " as fb LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON fb.customer_id = cust_det.customer_id";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("QueryData cursor ", cursor.toString());

        ArrayList<CrmPriceList> crmPriceListArrayList = new ArrayList<CrmPriceList>();
        Log.d("QueryData Array ", crmPriceListArrayList.toString());

        int i = 0;

        while (cursor.moveToNext()) {
            CrmPriceList crmPriceList = new CrmPriceList();
            // feedbackReport.setFeedback_date(cursor.getString(0));

            crmPriceList.setPriceListName(cursor.getString(0));
            crmPriceList.setProductUom(cursor.getString(1));
            crmPriceList.setUnitPrice(cursor.getString(2));

            String sDate1 = cursor.getString(3);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                crmPriceList.setEffectiveDate(dateString);
            } catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }

            crmPriceList.setEffectiveDate(cursor.getString(3));
            crmPriceList.setEndDate(cursor.getString(4));

            crmPriceListArrayList.add(i, crmPriceList);

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return crmPriceListArrayList;
    }

    public void uploadOpenStock(String bdeName) throws IOException {
      /*  String filePath = Environment.getExternalStorageDirectory() + File.separator
                + "mSale" + File.separator + "csv" +File.separator + "xxmsales_warehouse";
      */

        File filePath = new File("/storage/emulated/0/KSFA/csv/OpenStock.csv");

        Log.d("Fetching csv File", "filepath------" + filePath.toString());
        Log.d("Fetching csv File", "filename------" + filePath.getName());

        FileReader file = null;
        try {
            file = new FileReader(filePath);
            BufferedReader buffer = new BufferedReader(file);

            Log.d("Fetching csv File", "file------" + file.toString());

            String line = "";
            String tableName = TABLE_STOCK_RECIEPT;
            String columns = " sr_date, sr_currency, sr_sr_name, sr_source_name,sr_source_type,sr_product_name,sr_product_uom,sr_product_quantity,sr_product_price,sr_product_value,sr_receipt_number,status ";
            String str1 = "INSERT INTO " + tableName + " (" + columns + ") values(";
            String str2 = ");";

            Log.d("Fetching csv File", "DATA  TableName" + tableName);


            int row = 0;
            buffer.readLine();
            while ((line = buffer.readLine()) != null) {
                SQLiteDatabase sqlWrite = this.getWritableDatabase();
                sqlWrite.beginTransaction();
//                if(row==0)
//                    continue;
                StringBuilder sb = new StringBuilder(str1);
                String[] str = line.split(",");
                String PATTERN = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat();
                dateFormat.applyPattern(PATTERN);
                String date = dateFormat.format(Calendar.getInstance().getTime());
                String currencyType = "NGN";
                String sourceName = "OpenStock";
                String sourceType = "Distributor";
                String receiptNumber = sourceName + "_" + bdeName;

                String productName = "";
                if (str.length >= 1)
                    productName = str[0];

                String productUOM = "";
                if (str.length >= 3)
                    productUOM = str[2];

                String product_quantity = "";
                if (str.length >= 4)
                    product_quantity = str[3];

                String product_price = "";
                if (str.length >= 5)
                    product_price = str[4];


                Double productQuantity = TextUtils.isEmpty(product_quantity) ? 0d : Double.parseDouble(product_quantity);
                Double productPrice = TextUtils.isEmpty(product_price) ? 0d : Double.parseDouble(product_price);

                String productValue = String.valueOf(productQuantity * productPrice);
                // sb.append("'" + str[0] + "',");
                sb.append('"' + date + '"' + ',');
                sb.append('"' + currencyType + '"' + ',');
                sb.append('"' + bdeName + '"' + ',');
                sb.append('"' + sourceName + '"' + ',');
                sb.append('"' + sourceType + '"' + ',');
                sb.append('"' + productName + '"' + ',');
                sb.append('"' + productUOM + '"' + ',');
                sb.append('"' + product_quantity + '"' + ',');
                sb.append('"' + product_price + '"' + ',');
                sb.append('"' + productValue + '"' + ',');
                sb.append('"' + receiptNumber + '"' + ',');
                sb.append('"' + "Completed" + '"');
                sb.append(str2);
                sqlWrite.execSQL(sb.toString());
                sqlWrite.setTransactionSuccessful();
                Log.d("Fetching csv File", "DATA  Inserted Successfully" + tableName);

                sqlWrite.endTransaction();
                /*if(!TextUtils.isEmpty(productName)&&this.checkProductIsBatchControlled(productName)){
                    this.createstockrecieptbatch(productName,"OpenStockBatch",date,product_quantity,"",receiptNumber,"","Completed");
                }*/
                row++;
            }


        } catch (FileNotFoundException e) {
            Log.d("Fetching csv File", "Exception------" + e);
            e.printStackTrace();
        }


    }

    public void uploadOpenStockBatchProducts(String bdeName) throws IOException {

        File filePath = new File("/storage/emulated/0/KSFA/csv/OpenStockBatchProducts.csv");

        Log.d("Fetching csv File", "filepath------" + filePath.toString());
        Log.d("Fetching csv File", "filename------" + filePath.getName());

        FileReader file = null;
        try {
            file = new FileReader(filePath);
            BufferedReader buffer = new BufferedReader(file);

            Log.d("Fetching csv File", "file------" + file.toString());

            String line = "";
            String tableName = TABLE_PRODUCT_BATCH;
            String columns = " b_product_id, b_batch_number, b_expiry_date, b_batch_quantity, b_receipt_no, b_batch_status ";
            String str1 = "INSERT INTO " + tableName + " (" + columns + ") values(";
            String str2 = ");";

            Log.d("Fetching csv File", "DATA  TableName" + tableName);


            int row = 0;
            buffer.readLine();
            while ((line = buffer.readLine()) != null) {
                SQLiteDatabase sqlWrite = this.getWritableDatabase();
                sqlWrite.beginTransaction();

                StringBuilder sb = new StringBuilder(str1);
                String[] str = line.split(",");

                String sourceName = "OpenStock";
                String receiptNumber = sourceName + "_" + bdeName;

                String productCode = "";
                if (str.length >= 1)
                    productCode = str[0];

                String batchName = "";
                if (str.length >= 2)
                    batchName = str[1];

                String expiryDate = "";
                if (str.length >= 3)
                    expiryDate = str[2];

                String quantity = "";
                if (str.length >= 4)
                    quantity = str[3];


                // sb.append("'" + str[0] + "',");
                sb.append('"' + productCode + '"' + ',');
                sb.append('"' + batchName + '"' + ',');
                sb.append('"' + expiryDate + '"' + ',');
                sb.append('"' + quantity + '"' + ',');
                sb.append('"' + receiptNumber + '"' + ',');
                sb.append('"' + "Completed" + '"');
                sb.append(str2);
                sqlWrite.execSQL(sb.toString());
                sqlWrite.setTransactionSuccessful();
                Log.d("Fetching csv File", "DATA  Inserted Successfully" + tableName);

                sqlWrite.endTransaction();

                row++;
            }


        } catch (FileNotFoundException e) {
            Log.d("Fetching csv File", "Exception------" + e);
            e.printStackTrace();
        }

    }


    public void uploadReconciliationProducts(String bdeName) throws IOException {
      /*  String filePath = Environment.getExternalStorageDirectory() + File.separator
                + "mSale" + File.separator + "csv" +File.separator + "xxmsales_warehouse";
      */

        File filePath = new File("/storage/emulated/0/KSFA/csv/StockAdjustment.csv");

        Log.d("Fetching csv File", "filepath------" + filePath.toString());
        Log.d("Fetching csv File", "filename------" + filePath.getName());

        FileReader file = null;
        try {
            file = new FileReader(filePath);
            BufferedReader buffer = new BufferedReader(file);

            Log.d("Fetching csv File", "file------" + file.toString());

            String line = "";
            String tableName = TABLE_STOCK_RECIEPT;
            String columns = " sr_date, sr_currency, sr_sr_name, sr_source_name,sr_source_type,sr_product_name,sr_product_uom,sr_product_quantity,sr_product_price,sr_product_value,sr_receipt_number,status ";
            String str1 = "INSERT INTO " + tableName + " (" + columns + ") values(";
            String str2 = ");";

            Log.d("Fetching csv File", "DATA  TableName" + tableName);


            int row = 0;
            buffer.readLine();
            while ((line = buffer.readLine()) != null) {
                SQLiteDatabase sqlWrite = this.getWritableDatabase();
                sqlWrite.beginTransaction();
//                if(row==0)
//                    continue;
                StringBuilder sb = new StringBuilder(str1);
                String[] str = line.split(",");
                String PATTERN = "yyyy-MM-dd";
                SimpleDateFormat dateFormat = new SimpleDateFormat();
                dateFormat.applyPattern(PATTERN);
                String date = dateFormat.format(Calendar.getInstance().getTime());
                String currencyType = "NGN";
                String sourceName = "Reconciliation";
                String sourceType = "Distributor";
                String receiptNumber = sourceName + "_" + bdeName;

                String productName = "";
                if (str.length >= 1)
                    productName = str[0];

                String productUOM = "";
                if (str.length >= 3)
                    productUOM = str[2];

                String product_quantity = "";
                if (str.length >= 4)
                    product_quantity = str[3];

                String product_price = "";
                if (str.length >= 5)
                    product_price = str[4];


                Double productQuantity = TextUtils.isEmpty(product_quantity) ? 0d : Double.parseDouble(product_quantity);
                Double productPrice = TextUtils.isEmpty(product_price) ? 0d : Double.parseDouble(product_price);

                String productValue = String.valueOf(productQuantity * productPrice);
                // sb.append("'" + str[0] + "',");
                sb.append('"' + date + '"' + ',');
                sb.append('"' + currencyType + '"' + ',');
                sb.append('"' + bdeName + '"' + ',');
                sb.append('"' + sourceName + '"' + ',');
                sb.append('"' + sourceType + '"' + ',');
                sb.append('"' + productName + '"' + ',');
                sb.append('"' + productUOM + '"' + ',');
                sb.append('"' + product_quantity + '"' + ',');
                sb.append('"' + product_price + '"' + ',');
                sb.append('"' + productValue + '"' + ',');
                sb.append('"' + receiptNumber + '"' + ',');
                sb.append('"' + "Completed" + '"');
                sb.append(str2);
                sqlWrite.execSQL(sb.toString());
                sqlWrite.setTransactionSuccessful();
                Log.d("Fetching csv File", "DATA  Inserted Successfully" + tableName);

                sqlWrite.endTransaction();
                /*if(!TextUtils.isEmpty(productName)&&this.checkProductIsBatchControlled(productName)){
                    this.createstockrecieptbatch(productName,"OpenStockBatch",date,product_quantity,"",receiptNumber,"","Completed");
                }*/
                row++;
            }


        } catch (FileNotFoundException e) {
            Log.d("Fetching csv File", "Exception------" + e);
            e.printStackTrace();
        }


    }


    public void uploadReconciliationBatchProducts(String bdeName) throws IOException {


        File filePath = new File("/storage/emulated/0/KSFA/csv/StockAdjustmentBatch.csv");

        Log.d("Fetching csv File", "filepath------" + filePath.toString());
        Log.d("Fetching csv File", "filename------" + filePath.getName());

        FileReader file = null;
        try {
            file = new FileReader(filePath);
            BufferedReader buffer = new BufferedReader(file);

            Log.d("Fetching csv File", "file------" + file.toString());

            String line = "";
            String tableName = TABLE_PRODUCT_BATCH;
            String columns = " b_product_id, b_batch_number, b_expiry_date, b_batch_quantity, b_receipt_no, b_batch_status ";
            String str1 = "INSERT INTO " + tableName + " (" + columns + ") values(";
            String str2 = ");";

            Log.d("Fetching csv File", "DATA  TableName" + tableName);


            int row = 0;
            buffer.readLine();
            while ((line = buffer.readLine()) != null) {
                SQLiteDatabase sqlWrite = this.getWritableDatabase();
                sqlWrite.beginTransaction();

                StringBuilder sb = new StringBuilder(str1);
                String[] str = line.split(",");

                String sourceName = "Reconciliation";
                String receiptNumber = sourceName + "_" + bdeName;

                String productCode = "";
                if (str.length >= 1)
                    productCode = str[0];

                String batchName = "";
                if (str.length >= 2)
                    batchName = str[1];

                String expiryDate = "";
                if (str.length >= 3)
                    expiryDate = str[2];

                String quantity = "";
                if (str.length >= 4)
                    quantity = str[3];


                // sb.append("'" + str[0] + "',");
                sb.append('"' + productCode + '"' + ',');
                sb.append('"' + batchName + '"' + ',');
                sb.append('"' + expiryDate + '"' + ',');
                sb.append('"' + quantity + '"' + ',');
                sb.append('"' + receiptNumber + '"' + ',');
                sb.append('"' + "Completed" + '"');
                sb.append(str2);
                sqlWrite.execSQL(sb.toString());
                sqlWrite.setTransactionSuccessful();
                Log.d("Fetching csv File", "DATA  Inserted Successfully" + tableName);

                sqlWrite.endTransaction();

                row++;
            }


        } catch (FileNotFoundException e) {
            Log.d("Fetching csv File", "Exception------" + e);
            e.printStackTrace();
        }

    }


    public ArrayList<NewJourneyPlanCustomers> getCustomersDetails() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMER_DETAILS + " ORDER BY customer_name ASC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList = new ArrayList<NewJourneyPlanCustomers>();
        while (cursor.moveToNext()) {

            NewJourneyPlanCustomers newJourneyPlanCustomers = new NewJourneyPlanCustomers();
            newJourneyPlanCustomers.setCustomerCode(cursor.getString(1));
            newJourneyPlanCustomers.setCustomerName(cursor.getString(2));
            newJourneyPlanCustomers.setCustomerType(cursor.getString(19));

            newJourneyPlanCustomersArrayList.add(newJourneyPlanCustomers);
        }
        cursor.close();
        sqlWrite.close();
        return newJourneyPlanCustomersArrayList;
    }


    /**
     * Fetching the Customer Return Product Details.
     */

    public ArrayList<BDEIssueProductsReport> getBDEIssueProductsReports() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT si.stockIssueDate,si.stockIssueNum,bde.bdeName,sip.productName,sip.issueQuantity,sip.unitPrice,sip.issueValue FROM " + TABLE_STOCK_ISSUE_PRODUCTS + " as sip LEFT JOIN " + TABLE_STOCK_ISSUE + " as si on si.stockIssueNum = sip.stockIssueNumber LEFT JOIN " + TABLE_BDE_LIST + " as bde ON si.bdeCode = bde.bdeCode order by sip.stockIssueNumber desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});


        ArrayList<BDEIssueProductsReport> bdeIssueProductsReportArrayList = new ArrayList<BDEIssueProductsReport>();

        int i = 0;

        while (cursor.moveToNext()) {
            BDEIssueProductsReport bdeIssueProductsReport = new BDEIssueProductsReport();


            String sDate1 = cursor.getString(0);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                bdeIssueProductsReport.setBdeIssueDate(dateString);
            } catch (Exception e) {
            }

            bdeIssueProductsReport.setBdeIssueNumber(cursor.getString(1));
            bdeIssueProductsReport.setToBdeName(cursor.getString(2));

            bdeIssueProductsReport.setProductName(cursor.getString(3));
            bdeIssueProductsReport.setIssueQuantity(cursor.getString(4));
            bdeIssueProductsReport.setIssuePrice(cursor.getString(5));
            bdeIssueProductsReport.setIssueValue(cursor.getString(6));

            bdeIssueProductsReportArrayList.add(i, bdeIssueProductsReport);
            Log.d("CustomerReturn ", "ProductName ---- " + bdeIssueProductsReport.getProductName());

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return bdeIssueProductsReportArrayList;
    }


    public ArrayList<FeedbackReport> getFeedbackHistory(String customer_id) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT fb.feedback_date,fb.type,(select customer_name from xxmsales_customer_details where customer_id=fb.customer_id) customer_name,fb.feedback,fb.action_taken,fb.feedback_code FROM xxmsales_feedback fb where fb.customer_id='" + customer_id + "'";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        Log.d("QueryData cursor ", cursor.toString());
        ArrayList<FeedbackReport> feedbackReportArrayList = new ArrayList<FeedbackReport>();
        Log.d("QueryData Array ", feedbackReportArrayList.toString());
        int i = 0;
        while (cursor.moveToNext()) {
            FeedbackReport feedbackReport = new FeedbackReport();
            // feedbackReport.setFeedback_date(cursor.getString(0));
            String sDate1 = cursor.getString(0);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");
                String dateString = format.format(date1);
                feedbackReport.setFeedback_date(dateString);
            } catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }
            feedbackReport.setFeedback_type(cursor.getString(1));
            feedbackReport.setCustomer_name(cursor.getString(2));
            feedbackReport.setCustomer_feedback(cursor.getString(3));
            feedbackReport.setAction_taken(cursor.getString(4));
            feedbackReport.setFeedback_code(cursor.getString(5));
            feedbackReportArrayList.add(i, feedbackReport);
            Log.d("QueryData", "FbText " + feedbackReport.getCustomer_feedback());
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return feedbackReportArrayList;
    }

    public void updateFeedback(String action_taken, String status,String feedback_id,String customer_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_F_ACTIONTAKEN, action_taken);
        values.put(COLUMN_F_STATUS, status);
        db.update(TABLE_FEEDBACK, values, COLUMN_F_CUST_ID + " = ? AND " + COLUMN_FEEDBACK_CODE + " = ?", new String[]{customer_id,feedback_id});
        db.close();
    }

    public ArrayList<FeedbackType> getFeedbackFromBachEnd(String customer_id) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT fb.feedback_id,fb.feedback_type,fb.tab_code,fb.bde_code,fb.customer_code,fb.action_taken,fb.status FROM xxmsales_feedback_type fb where fb.customer_code='" + customer_id + "'";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        ArrayList<FeedbackType> FeedbackTypeArrayList = new ArrayList<FeedbackType>();
        int i = 0;
        while (cursor.moveToNext()) {
            FeedbackType feedbackType = new FeedbackType();

            feedbackType.setFeedback_id(cursor.getString(0));
            feedbackType.setFeedback_type(cursor.getString(1));
            feedbackType.setTab_code(cursor.getString(2));
            feedbackType.setBde_code(cursor.getString(3));
            feedbackType.setCustomer_code(cursor.getString(4));
            feedbackType.setAction_taken(cursor.getString(5));
            feedbackType.setStatus(cursor.getString(6));
            FeedbackTypeArrayList.add(i, feedbackType);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return FeedbackTypeArrayList;
    }

    //Get PurchaseHistory
    public ArrayList<CrmPurchaseHisProd> getPurchaseHistoryForCrm(String customerCode, String productCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        Log.d("Crm--", "DB customerCode " +customerCode +" Producode: " +productCode);

        String selectQuery = " select ordItem.salesorder_date, ordItem.quantity, ordItem.price  from xxmsales_order_details as ordItem join xxmsales_order_header as ord on ord.salesorder_number = ordItem.salesorder_number where ordItem.customer_id = ?  and ordItem.product_id = ? and (ord.salesorder_status = 'Completed' OR ord.salesorder_status='Customer Order') order by ordItem.salesorder_date LIMIT 3";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerCode, productCode});

        ArrayList<CrmPurchaseHisProd> crmPurchaseList = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmPurchaseHisProd items = new CrmPurchaseHisProd();

            String sDate1 = cursor.getString(0);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                items.setDate(dateString);
            } catch (Exception e) {
            }

            items.setQuantity(cursor.getString(1));
            items.setPrice(cursor.getString(2));

            Log.d("Crm--", "cursor " +cursor.getString(2) );


            crmPurchaseList.add(items);
        }

        cursor.close();
        sqlWrite.close();
        return crmPurchaseList;
    }

}