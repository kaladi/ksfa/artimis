package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.NewJourneyPlan;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_NEW_JOURNEY_PLAN;

public class NewJourneyPlanTable extends SQLiteOpenHelper {

    private static final String COLUMN_JOURNEY_PLAN_ID = "journeyPlanId";
    private static final String COLUMN_JOURNEY_PLAN_NUMBER = "journeyPlanNumber";
    private static final String COLUMN_JOURNEY_PLAN_DATE = "journeyPlanDate";
    private static final String COLUMN_JOURNEY_PLAN_BDE_CODE = "bdeCode";
    private static final String COLUMN_JOURNEY_PLAN_BDE_REGION = "region";
    private static final String COLUMN_JOURNEY_PLAN_START_DATE = "startDate";
    private static final String COLUMN_JOURNEY_PLAN_MONDAY = "monday";
    private static final String COLUMN_JOURNEY_PLAN_TUESDAY = "tuesday";
    private static final String COLUMN_JOURNEY_PLAN_WEDNESDAY = "wednesday";
    private static final String COLUMN_JOURNEY_PLAN_THURSDAY = "thursday";
    private static final String COLUMN_JOURNEY_PLAN_FRIDAY = "friday";
    private static final String COLUMN_JOURNEY_PLAN_SATURDAY = "saturday";
    private static final String COLUMN_JOURNEY_PLAN_SUNDAY = "sunday";
    private static final String COLUMN_JOURNEY_PLAN_VISIT_SEQUENCE_NUMBER = "visitSeqNo";
    private final static String COLUMN_JOURNEY_PLAN_RANDOM_NUM = "randomNumber";
    private static final String COLUMN_JOURNEY_PLAN_IS_SYNC = "isSync";
    private static final String COLUMN_JOURNEY_PLAN_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_JOURNEY_PLAN_STATUS = "jpStatus";
    private static final String COLUMN_JOURNEY_PLAN_CREATED_AT = "createdAt";
    private static final String COLUMN_JOURNEY_PLAN_UPDATED_AT = "updatedAt";

    public Context context;
    private SQLiteDatabase sqlWrite;


    public NewJourneyPlanTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_NEW_JOURNEY_PLAN);

        String stockReturnProductTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NEW_JOURNEY_PLAN + "(" +
                COLUMN_JOURNEY_PLAN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_JOURNEY_PLAN_NUMBER + " TEXT, " +
                COLUMN_JOURNEY_PLAN_DATE + " TEXT, " +

                COLUMN_JOURNEY_PLAN_BDE_CODE + " TEXT, " +
                COLUMN_JOURNEY_PLAN_BDE_REGION + " TEXT, " +
                COLUMN_JOURNEY_PLAN_START_DATE + " TEXT, " +

                COLUMN_JOURNEY_PLAN_MONDAY + " TEXT, " +
                COLUMN_JOURNEY_PLAN_TUESDAY + " TEXT, " +
                COLUMN_JOURNEY_PLAN_WEDNESDAY + " TEXT, " +
                COLUMN_JOURNEY_PLAN_THURSDAY + " TEXT, " +
                COLUMN_JOURNEY_PLAN_FRIDAY + " TEXT, " +
                COLUMN_JOURNEY_PLAN_SATURDAY + " TEXT, " +
                COLUMN_JOURNEY_PLAN_SUNDAY + " TEXT, " +

                COLUMN_JOURNEY_PLAN_VISIT_SEQUENCE_NUMBER + " TEXT, " +
                COLUMN_JOURNEY_PLAN_RANDOM_NUM + " TEXT, " +

                COLUMN_JOURNEY_PLAN_IS_SYNC + " TEXT, " +
                COLUMN_JOURNEY_PLAN_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_JOURNEY_PLAN_STATUS + " TEXT, " +

                COLUMN_JOURNEY_PLAN_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_JOURNEY_PLAN_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(stockReturnProductTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(NewJourneyPlan newJourneyPlan) {
        ContentValues values = new ContentValues();

        values.put(COLUMN_JOURNEY_PLAN_NUMBER, newJourneyPlan.getJourneyPlanNumber());
        values.put(COLUMN_JOURNEY_PLAN_DATE, newJourneyPlan.getJourneyPlanDate());

        values.put(COLUMN_JOURNEY_PLAN_BDE_CODE, newJourneyPlan.getBdeCode());
        values.put(COLUMN_JOURNEY_PLAN_BDE_REGION, newJourneyPlan.getBdeRegion());
        values.put(COLUMN_JOURNEY_PLAN_START_DATE, newJourneyPlan.getJourneyStartDate());

        values.put(COLUMN_JOURNEY_PLAN_MONDAY, newJourneyPlan.getMonday());
        values.put(COLUMN_JOURNEY_PLAN_TUESDAY, newJourneyPlan.getTuesday());
        values.put(COLUMN_JOURNEY_PLAN_WEDNESDAY, newJourneyPlan.getWednesday());
        values.put(COLUMN_JOURNEY_PLAN_THURSDAY, newJourneyPlan.getThursday());
        values.put(COLUMN_JOURNEY_PLAN_FRIDAY, newJourneyPlan.getFriday());
        values.put(COLUMN_JOURNEY_PLAN_SATURDAY, newJourneyPlan.getSaturday());
        values.put(COLUMN_JOURNEY_PLAN_SUNDAY, newJourneyPlan.getSunday());

        values.put(COLUMN_JOURNEY_PLAN_VISIT_SEQUENCE_NUMBER, newJourneyPlan.getVisitSequenceNumber());
        values.put(COLUMN_JOURNEY_PLAN_RANDOM_NUM, newJourneyPlan.getRandomNumber());

        values.put(COLUMN_JOURNEY_PLAN_STATUS, newJourneyPlan.getJourneyPlanStatus());
        values.put(COLUMN_JOURNEY_PLAN_IS_SYNC, newJourneyPlan.getIsSync());
        values.put(COLUMN_JOURNEY_PLAN_LATEST_SYNC_DATE, newJourneyPlan.getLatestSynDate());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_NEW_JOURNEY_PLAN, null, values);
        db.close();
        return id;
    }


    public int getJourneyPlanNumber() {
        int journeyPlanId = 0;
        String selectQuery = "select randomNumber from xxmsales_new_journey_plan where journeyPlanDate=date('now') order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            journeyPlanId = cursor.getInt(0);
            cursor.close();
        }

        return journeyPlanId;
    }

    public ArrayList<NewJourneyPlan> getJourneyPlanDetailsForSync() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_NEW_JOURNEY_PLAN;

        ArrayList<NewJourneyPlan> newJourneyPlanArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            NewJourneyPlan newJourneyPlan = new NewJourneyPlan();
            newJourneyPlan.setTabCode(Constants.TAB_CODE);
            newJourneyPlan.setCompanyCode(Constants.COMPANY_CODE);
            newJourneyPlan.setBdeCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            newJourneyPlan.setJourneyPlanId(id);
            newJourneyPlan.setJourneyPlanNumber(cursor.getString(1));
            newJourneyPlan.setJourneyPlanDate(cursor.getString(2));
            newJourneyPlan.setBdeCode(cursor.getString(3));
            newJourneyPlan.setBdeRegion(cursor.getString(4));
            newJourneyPlan.setJourneyStartDate(cursor.getString(5));
            newJourneyPlan.setMonday(cursor.getString(6));
            newJourneyPlan.setTuesday(cursor.getString(7));
            newJourneyPlan.setWednesday(cursor.getString(8));
            newJourneyPlan.setThursday(cursor.getString(9));
            newJourneyPlan.setFriday(cursor.getString(10));
            newJourneyPlan.setSaturday(cursor.getString(11));
            newJourneyPlan.setSunday(cursor.getString(12));
            newJourneyPlan.setVisitSequenceNumber(cursor.getString(13));
            newJourneyPlan.setRandomNumber(cursor.getInt(14));
            newJourneyPlan.setIsSync(cursor.getString(15));
            newJourneyPlan.setLatestSynDate(cursor.getString(16));
            newJourneyPlan.setJourneyPlanStatus(cursor.getString(17));
            newJourneyPlan.setCreatedAt(cursor.getString(18));

            newJourneyPlanArrayList.add(newJourneyPlan);
        }
        cursor.close();
        sqlWrite.close();
        return newJourneyPlanArrayList;
    }

    public NewJourneyPlan getJourneyPlanByDate(String date) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NEW_JOURNEY_PLAN + " where monday = ? or tuesday = ? or wednesday=? or thursday=? or friday=? or saturday=? or sunday=? order by journeyPlanId desc limit 1";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{date, date, date, date, date, date, date});

        ArrayList<NewJourneyPlan> newJourneyPlanArrayList = new ArrayList<>();
        NewJourneyPlan newJourneyPlan = null;

        while (cursor.moveToNext()) {
            // Log.d("MYDATA","Data  id" + cursor.getInt(0));
            newJourneyPlan = new NewJourneyPlan();
            newJourneyPlan.setJourneyPlanNumber(cursor.getString(1));
            newJourneyPlan.setJourneyPlanDate(cursor.getString(2));
            newJourneyPlan.setBdeCode(cursor.getString(3));
            newJourneyPlan.setBdeRegion(cursor.getString(4));
            newJourneyPlan.setJourneyStartDate(cursor.getString(5));
            newJourneyPlan.setMonday(cursor.getString(6));
            newJourneyPlan.setTuesday(cursor.getString(7));
            newJourneyPlan.setWednesday(cursor.getString(8));
            newJourneyPlan.setThursday(cursor.getString(9));
            newJourneyPlan.setFriday(cursor.getString(10));
            newJourneyPlan.setSaturday(cursor.getString(11));
            newJourneyPlan.setSunday(cursor.getString(12));
            newJourneyPlan.setVisitSequenceNumber(cursor.getString(13));
            newJourneyPlan.setRandomNumber(cursor.getInt(14));
            newJourneyPlan.setIsSync(cursor.getString(15));
            newJourneyPlan.setLatestSynDate(cursor.getString(16));
            newJourneyPlan.setJourneyPlanStatus(cursor.getString(17));
            newJourneyPlanArrayList.add(newJourneyPlan);
        }

        cursor.close();
        sqlWrite.close();
        return newJourneyPlan;
    }

    public void updateJourneyPlanStatusFromWeb(String journeyPlanNumber, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_JOURNEY_PLAN_STATUS, status);

        db.update(TABLE_NEW_JOURNEY_PLAN, values, COLUMN_JOURNEY_PLAN_NUMBER + " = ?", new String[]{journeyPlanNumber});
        db.close();
    }

    /*public ArrayList<NewJourneyPlan> getJourneyPlan() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NEW_JOURNEY_PLAN ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<NewJourneyPlan> newJourneyPlanArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            NewJourneyPlan newJourneyPlan = new NewJourneyPlan();
            newJourneyPlan.setJourneyPlanId(cursor.getInt(0));

            newJourneyPlan.setJourneyPlanNumber(cursor.getString(1));
            newJourneyPlan.setJourneyPlanDate(cursor.getString(2));

            newJourneyPlan.setBdeCode(cursor.getString(3));
            newJourneyPlan.setBdeRegion(cursor.getString(4));

            newJourneyPlan.setJourneyStartDate(cursor.getString(5));
            newJourneyPlan.setMonday(cursor.getString(6));
            newJourneyPlan.setTuesday(cursor.getString(7));
            newJourneyPlan.setWednesday(cursor.getString(8));
            newJourneyPlan.setThursday(cursor.getString(9));
            newJourneyPlan.setFriday(cursor.getString(10));
            newJourneyPlan.setSaturday(cursor.getString(11));
            newJourneyPlan.setSunday(cursor.getString(12));

            newJourneyPlan.setVisitSequenceNumber(cursor.getString(13));

            newJourneyPlanArrayList.add(i, newJourneyPlan);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return newJourneyPlanArrayList;
    }*/
}
