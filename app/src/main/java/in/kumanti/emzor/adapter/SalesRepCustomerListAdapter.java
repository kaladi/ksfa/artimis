package in.kumanti.emzor.adapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.HomeActivity;
import in.kumanti.emzor.activity.UnplannedActivity;
import in.kumanti.emzor.model.SalesRep_Customer_List;
import in.kumanti.emzor.utils.Globals;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


public class SalesRepCustomerListAdapter extends ArrayAdapter<SalesRep_Customer_List> {

    String sales_rep_id = "";
    String trip_num = "";
    Globals globals;
    String user_role;
    private LayoutInflater mInflater;
    private ArrayList<SalesRep_Customer_List> users;
    private int mViewResourceId;
    private Context c;

    public SalesRepCustomerListAdapter(Context context, int textViewResourceId, ArrayList<SalesRep_Customer_List> users, String sales_rep_id_val, String tripno_val, String user_role1) {
        super(context, textViewResourceId, users);
        this.users = users;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
        sales_rep_id = sales_rep_id_val;
        trip_num = tripno_val;
        c = context;
        globals = ((Globals) c);
        user_role = user_role1;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        convertView = mInflater.inflate(mViewResourceId, null);
        final SalesRep_Customer_List user = users.get(position);
        if (user != null) {
            viewHolder = new ViewHolder();
            viewHolder.serial_num = convertView.findViewById(R.id.serial_num);
            viewHolder.customer_name = convertView.findViewById(R.id.customer_name);
            viewHolder.location = convertView.findViewById(R.id.location);
            viewHolder.status = convertView.findViewById(R.id.status);
            viewHolder.customer_type = convertView.findViewById(R.id.customer_type);

            String status_val = user.getStatus();
            String customer_type1 = user.getCustomer_type();

            if (viewHolder.serial_num != null) {
                viewHolder.serial_num.setText(user.getSerial_num());
            }
            if (viewHolder.customer_name != null) {
                viewHolder.customer_name.setText((user.getCustomer_name()));
            }
            if (viewHolder.location != null) {
                System.out.println("TTT::user.getLocation() = " + user.getLocation_name());
                viewHolder.location.setText((user.getLocation_name()));
            }
            if (viewHolder.status != null) {
                if ("true".equals(status_val)) {
                    viewHolder.status.setChecked(true);
                } else {
                    viewHolder.status.setChecked(false);
                }
            }
            if (viewHolder.customer_type != null) {
                if ("Primary".equals(customer_type1)) {
                    viewHolder.customer_type.setText(("P"));
                } else if ("Doctor".equals(customer_type1)) {
                    viewHolder.customer_type.setText(("D"));
                } else if ("Secondary".equals(customer_type1)) {
                    viewHolder.customer_type.setText(("S"));
                } else if("Prospect".equals(customer_type1)) {
                    viewHolder.customer_type.setText(("PT"));
                }
                else {
                    viewHolder.customer_type.setText((" "));
                }
            }
        }


        viewHolder.serial_num.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String customer_id = user.getCustomer_id();
                String customer_type = user.getCustomer_type();
                globals = ((Globals) c);
                String mileage = globals.getMileage();

                if (customer_id.equals("0")) {

                    Intent intent = new Intent(getContext(), UnplannedActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("sales_rep_id", sales_rep_id);
                    intent.putExtra("user_role", user_role);

                    getContext().startActivity(intent);

                } else {
                    final Dialog alertbox = new Dialog(v.getRootView().getContext());

                    LayoutInflater mInflater = (LayoutInflater) c.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    final View alertLayout = mInflater.inflate(R.layout.activity_checkin_popup, null);

                    alertbox.setCancelable(false);
                    alertbox.setContentView(alertLayout);

                    final Button Yes = alertLayout.findViewById(R.id.Yes);
                    final Button No = alertLayout.findViewById(R.id.No);

                    Yes.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            String customer_id = user.getCustomer_id();
                            String customer_type = user.getCustomer_type();

                            Intent intent = new Intent(getContext(), HomeActivity.class);
                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("customer_id", customer_id);
                            intent.putExtra("nav_type", "SR Landing");
                            globals.setCustomer_type(customer_type);
                            globals.setCustomer_id(customer_id);

                            intent.putExtra("sales_rep_id", sales_rep_id);
                            intent.putExtra("user_role", user_role);

                            getContext().startActivity(intent);

                        }

                    });

                    No.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            alertbox.dismiss();
                        }

                    });

                    alertbox.show();
                }

            }

        });

        viewHolder.customer_name.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String customer_id = user.getCustomer_id();
                String customer_type = user.getCustomer_type();

                globals = ((Globals) c);
                String mileage = globals.getMileage();

                if (customer_id.equals("0")) {

                    Intent intent = new Intent(getContext(), UnplannedActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("sales_rep_id", sales_rep_id);
                    intent.putExtra("user_role", user_role);

                    getContext().startActivity(intent);

                } else {
                    final Dialog alertbox = new Dialog(v.getRootView().getContext());

                    LayoutInflater mInflater = (LayoutInflater)
                            c.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    final View alertLayout = mInflater.inflate(R.layout.activity_checkin_popup, null);

                    alertbox.setCancelable(false);
                    alertbox.setContentView(alertLayout);

                    final Button Yes = alertLayout.findViewById(R.id.Yes);
                    final Button No = alertLayout.findViewById(R.id.No);

                    Yes.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            String customer_id = user.getCustomer_id();
                            String customer_type = user.getCustomer_type();

                            if (alertbox != null && alertbox.isShowing()) {
                                alertbox.dismiss();
                            }

                            Intent intent = new Intent(getContext(), HomeActivity.class);
                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("customer_id", customer_id);
                            intent.putExtra("nav_type", "SR Landing");
                            globals.setCustomer_type(customer_type);
                            intent.putExtra("user_role", user_role);

                            intent.putExtra("sales_rep_id", sales_rep_id);
                            getContext().startActivity(intent);

                        }

                    });

                    No.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            alertbox.dismiss();
                        }

                    });

                    alertbox.show();
                }

            }

        });

        viewHolder.location.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String customer_id = user.getCustomer_id();
                String customer_type = user.getCustomer_type();
                String status = user.getStatus();

                globals = ((Globals) c);
                String mileage = globals.getMileage();

                if (customer_id.equals("0")) {

                    Intent intent = new Intent(getContext(), UnplannedActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("sales_rep_id", sales_rep_id);
                    intent.putExtra("user_role", user_role);

                    getContext().startActivity(intent);

                } else {

                    final Dialog alertbox = new Dialog(v.getRootView().getContext());

                    LayoutInflater mInflater = (LayoutInflater)
                            c.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    final View alertLayout = mInflater.inflate(R.layout.activity_checkin_popup, null);

                    alertbox.setCancelable(false);
                    alertbox.setContentView(alertLayout);

                    final Button Yes = alertLayout.findViewById(R.id.Yes);
                    final Button No = alertLayout.findViewById(R.id.No);

                    Yes.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            String customer_id = user.getCustomer_id();
                            String customer_type = user.getCustomer_type();

                            Intent intent = new Intent(getContext(), HomeActivity.class);
                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("customer_id", customer_id);
                            intent.putExtra("nav_type", "SR Landing");
                            intent.putExtra("user_role", user_role);

                            globals.setCustomer_type(customer_type);

                            intent.putExtra("sales_rep_id", sales_rep_id);
                            getContext().startActivity(intent);

                        }

                    });

                    No.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {

                            alertbox.dismiss();
                        }

                    });

                    alertbox.show();
                }

            }

        });

        return convertView;
    }

    public class ViewHolder {
        TextView serial_num, customer_name, location, customer_type;
        CheckBox status;

    }

}