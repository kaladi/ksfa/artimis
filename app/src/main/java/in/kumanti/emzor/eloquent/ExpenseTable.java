package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;

import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.model.TripDetails;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_EXPENSE;
import static in.kumanti.emzor.utils.Constants.TABLE_NEW_JOURNEY_PLAN;

public class ExpenseTable extends SQLiteOpenHelper {

    private static final String COLUMN_EXPENSE_ID = "expenseId";
    private static final String COLUMN_EXPENSE_NUMBER = "expenseNumber";
    private static final String COLUMN_EXPENSE_DATE = "expenseDate";
    private static final String COLUMN_EXPENSE_TYPE = "expenseType";
    private static final String COLUMN_EXPENSE_MILEAGE = "mileage";
    private static final String COLUMN_EXPENSE_VOUCHER_NO = "voucherNo";
    private static final String COLUMN_EXPENSE_VOUCHER_DATE = "voucherDate";
    private static final String COLUMN_EXPENSE_VENDOR = "vendor";
    private static final String COLUMN_EXPENSE_VENDOR_TYPE = "vendorType";
    private static final String COLUMN_EXPENSE_CURRENCY = "currency";
    private static final String COLUMN_EXPENSE_AMOUNT = "expenseAmount";
    private static final String COLUMN_EXPENSE_REMARKS = "remarks";
    private static final String COLUMN_EXPENSE_SIGNATURE = "signature";
    private static final String COLUMN_EXPENSE_POST_DATE = "postDate";
    private static final String COLUMN_EXPENSE_POST_TIME = "postTime";
    private static final String COLUMN_EXPENSE_APPROVED_AMOUNT = "approvedAmount";
    private static final String COLUMN_EXPENSE_STATUS = "status";
    private final static String COLUMN_EXPENSE_RANDOM_NUM = "randomNumber";
    private final static String COLUMN_EXPENSE_TRIP_NUMBER = "tripNumber";
    private final static String COLUMN_EXPENSE_TRIP_START_DATE = "tripStartDate";
    private final static String COLUMN_EXPENSE_TRIP_START_TIME = "tripStartTime";
    private final static String COLUMN_EXPENSE_TRIP_END_DATE = "tripEndDate";
    private final static String COLUMN_EXPENSE_TRIP_END_TIME = "tripEndTime";
    private static final String COLUMN_EXPENSE_IS_SYNC = "is_sync";
    private static final String COLUMN_EXPENSE_SIGNATURE_PATH = "signatureFilePath";
    private static final String COLUMN_EXPENSE_SIGNATURE_NAME = "signatureFileName";
    private static final String COLUMN_EXPENSE_REASON = "expenseReason";
    private static final String COLUMN_EXPENSE_IS_IMAGE_UPLOADED = "isImageUploaded";
    private static final String COLUMN_EXPENSE_CREATED_AT = "createdAt";
    private static final String COLUMN_EXPENSE_UPDATED_AT = "updatedAt";

    public Context context;
    String selectQuery;
    Cursor cursor;

    public ExpenseTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSE);

        String expenseTable = "CREATE TABLE IF NOT EXISTS " + TABLE_EXPENSE + "(" +
                COLUMN_EXPENSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_EXPENSE_NUMBER + " TEXT, " +
                COLUMN_EXPENSE_DATE + " TEXT, " +
                COLUMN_EXPENSE_TYPE + " TEXT, " +
                COLUMN_EXPENSE_MILEAGE + " TEXT, " +
                COLUMN_EXPENSE_VOUCHER_NO + " TEXT, " +
                COLUMN_EXPENSE_VOUCHER_DATE + " TEXT, " +
                COLUMN_EXPENSE_VENDOR + " TEXT, " +
                COLUMN_EXPENSE_VENDOR_TYPE + " TEXT, " +
                COLUMN_EXPENSE_CURRENCY + " TEXT, " +
                COLUMN_EXPENSE_AMOUNT + " TEXT, " +
                COLUMN_EXPENSE_REMARKS + " TEXT, " +
                COLUMN_EXPENSE_SIGNATURE + " TEXT, " +
                COLUMN_EXPENSE_POST_DATE + " TEXT, " +
                COLUMN_EXPENSE_POST_TIME + " TEXT, " +
                COLUMN_EXPENSE_APPROVED_AMOUNT + " TEXT, " +
                COLUMN_EXPENSE_STATUS + " TEXT, " +
                COLUMN_EXPENSE_RANDOM_NUM + " TEXT, " +
                COLUMN_EXPENSE_TRIP_NUMBER + " TEXT, " +
                COLUMN_EXPENSE_TRIP_START_DATE + " TEXT, " +
                COLUMN_EXPENSE_TRIP_START_TIME + " TEXT, " +
                COLUMN_EXPENSE_TRIP_END_DATE + " TEXT, " +
                COLUMN_EXPENSE_TRIP_END_TIME + " TEXT, " +
                COLUMN_EXPENSE_SIGNATURE_PATH + " TEXT, " +
                COLUMN_EXPENSE_SIGNATURE_NAME + " TEXT, " +
                COLUMN_EXPENSE_REASON + " TEXT, " +
                COLUMN_EXPENSE_IS_IMAGE_UPLOADED + " INTEGER DEFAULT 0, " +
                COLUMN_EXPENSE_IS_SYNC + " TEXT," +
                COLUMN_EXPENSE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_EXPENSE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(expenseTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(Expense expense) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_EXPENSE_NUMBER, expense.getExpenseNumber());
        values.put(COLUMN_EXPENSE_DATE, expense.getExpenseDate());
        values.put(COLUMN_EXPENSE_TYPE, expense.getExpenseType());
        values.put(COLUMN_EXPENSE_MILEAGE, expense.getExpenseMileage());
        values.put(COLUMN_EXPENSE_VOUCHER_NO, expense.getExpenseVoucherNo());
        values.put(COLUMN_EXPENSE_VOUCHER_DATE, expense.getExpenseVoucherDate());
        values.put(COLUMN_EXPENSE_VENDOR, expense.getExpenseVendor());
        values.put(COLUMN_EXPENSE_VENDOR_TYPE, expense.getExpenseVendorType());
        values.put(COLUMN_EXPENSE_CURRENCY, expense.getExpenseCurrency());
        values.put(COLUMN_EXPENSE_AMOUNT, expense.getExpenseAmount());
        values.put(COLUMN_EXPENSE_REMARKS, expense.getExpenseRemarks());
        values.put(COLUMN_EXPENSE_SIGNATURE, expense.getExpenseSignature());
        values.put(COLUMN_EXPENSE_POST_DATE, expense.getExpensePostDate());
        values.put(COLUMN_EXPENSE_POST_TIME, expense.getExpensePostTime());
        values.put(COLUMN_EXPENSE_APPROVED_AMOUNT, expense.getExpenseApprovedAmount());
        values.put(COLUMN_EXPENSE_STATUS, expense.getExpenseStatus());
        values.put(COLUMN_EXPENSE_RANDOM_NUM, expense.getExpenseRandomNum());
        values.put(COLUMN_EXPENSE_TRIP_NUMBER, expense.getTripNumber());
        values.put(COLUMN_EXPENSE_TRIP_START_DATE, expense.getTripStartDate());
        values.put(COLUMN_EXPENSE_TRIP_START_TIME, expense.getTripStartTime());
        values.put(COLUMN_EXPENSE_TRIP_END_DATE, expense.getTripEndDate());
        values.put(COLUMN_EXPENSE_TRIP_END_TIME, expense.getTripEndTime());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_EXPENSE, null, values);
        db.close();
        return id;
    }

    /*
     * Fetching the Distributors Name List details from the Master via Web Service
     */
    public void insertingDistributorsMaster(ArrayList<Expense> expenseArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (expenseArrayList != null) {
                for (int i = 0; i < expenseArrayList.size(); i++) {
                    values.put(COLUMN_EXPENSE_NUMBER, expenseArrayList.get(i).getExpenseNumber());
                    values.put(COLUMN_EXPENSE_DATE, expenseArrayList.get(i).getExpenseDate());
                    values.put(COLUMN_EXPENSE_TYPE, expenseArrayList.get(i).getExpenseType());
                    values.put(COLUMN_EXPENSE_MILEAGE, expenseArrayList.get(i).getExpenseMileage());
                    values.put(COLUMN_EXPENSE_VOUCHER_NO, expenseArrayList.get(i).getExpenseVoucherNo());
                    values.put(COLUMN_EXPENSE_VENDOR, expenseArrayList.get(i).getExpenseVendor());
                    values.put(COLUMN_EXPENSE_CURRENCY, expenseArrayList.get(i).getExpenseCurrency());
                    values.put(COLUMN_EXPENSE_AMOUNT, expenseArrayList.get(i).getExpenseAmount());
                    values.put(COLUMN_EXPENSE_REMARKS, expenseArrayList.get(i).getExpenseRemarks());
                    values.put(COLUMN_EXPENSE_SIGNATURE, expenseArrayList.get(i).getExpenseSignature());
                    values.put(COLUMN_EXPENSE_POST_DATE, expenseArrayList.get(i).getExpensePostDate());
                    values.put(COLUMN_EXPENSE_POST_TIME, expenseArrayList.get(i).getExpensePostTime());
                    values.put(COLUMN_EXPENSE_APPROVED_AMOUNT, expenseArrayList.get(i).getExpenseApprovedAmount());
                    values.put(COLUMN_EXPENSE_STATUS, expenseArrayList.get(i).getExpenseStatus());
                    values.put(COLUMN_EXPENSE_RANDOM_NUM, expenseArrayList.get(i).getExpenseRandomNum());
                    db.insert(TABLE_EXPENSE, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }
    }

    public int getExpenseNumber() {
        int expenseId = 0;
        String selectQuery = "select randomNumber from xxmsales_expense  order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            expenseId = cursor.getInt(0);
            cursor.close();
        }

        return expenseId;
    }





    public ArrayList<Expense> getExpenseAmountSort() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT amount FROM " + TABLE_EXPENSE + " ORDER BY amount DESC ";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, null);

        ArrayList<Expense> expenseAmountArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Expense expense = new Expense();

            expense.setExpenseAmount(cursor.getString(0));

            expenseAmountArrayList.add(expense);
        }
        cursor.close();
        sqlWrite.close();
        return expenseAmountArrayList;
    }

    public ArrayList<Expense> getAllExpense() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_EXPENSE;

        Cursor cursor = sqlWrite.rawQuery(selectQuery, null);

        ArrayList<Expense> expenseArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Expense expense = new Expense();
            expense.setTabCode(Constants.TAB_CODE);
            expense.setCompanyCode(Constants.COMPANY_CODE);
            expense.setBdeCode(Constants.SR_CODE);
            expense.setBdeName(Constants.SR_NAME);

            expense.setExpenseId(cursor.getInt(0));
            expense.setExpenseNumber(cursor.getString(1));
            expense.setExpenseDate(cursor.getString(2));
            expense.setExpenseType(cursor.getString(3));
            expense.setExpenseMileage(cursor.getString(4));
            expense.setExpenseVoucherNo(cursor.getString(5));
            expense.setExpenseVoucherDate(cursor.getString(6));
            expense.setExpenseVendor(cursor.getString(7));
            expense.setExpenseVendorType(cursor.getString(8));
            expense.setExpenseCurrency(cursor.getString(9));
            expense.setExpenseAmount(cursor.getString(10));
            expense.setExpenseRemarks(cursor.getString(11));
            expense.setExpenseSignature(cursor.getString(12));
            expense.setExpensePostDate(cursor.getString(13));
            expense.setExpensePostTime(cursor.getString(14));
            expense.setExpenseApprovedAmount(cursor.getString(15));
            expense.setExpenseStatus(cursor.getString(16));
            expense.setTripNumber(cursor.getString(18));
            expense.setTripStartDate(cursor.getString(19));
            expense.setTripStartTime(cursor.getString(20));
            expense.setTripEndDate(cursor.getString(21));
            expense.setTripEndTime(cursor.getString(22));
            expense.setSignatureFilePath(cursor.getString(23));
            expense.setSignatureFileName(cursor.getString(24));
//            expense.setExpenseRandomNumber(cursor.getString(15));

            expenseArrayList.add(expense);
        }
        cursor.close();
        sqlWrite.close();
        return expenseArrayList;
    }

    public ArrayList<Expense> getExpenseByType(String type, String start, String end) {
        System.out.println("RRR :: type = [" + type + "], start = [" + start + "], end = [" + end + "]");

        String selectQuery = "select * from xxmsales_expense ";

        if (type.equals("All")) {
            if (TextUtils.isEmpty(start) && TextUtils.isEmpty(end))
                selectQuery = "select * from xxmsales_expense ";
            else selectQuery = "select * from xxmsales_expense " +
                    "where expenseDate BETWEEN date('" +(start) + "') and date('" + end + "')";
        } else {
            if (TextUtils.isEmpty(start) && TextUtils.isEmpty(end))
                selectQuery = selectQuery + " where expenseType = '" + type +
                        "' and expenseDate BETWEEN date('" + start + "') and date('" + end + "')";
            else selectQuery = selectQuery + " where expenseType = '" + type +
                    "' and expenseDate BETWEEN date('" + start + "') and date('" + end + "')";
        }

        System.out.println("RRR :: selectQuery = " + selectQuery);

        SQLiteDatabase sqlWrite = this.getReadableDatabase();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, null);
        ArrayList<Expense> expenseArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Expense expense = new Expense();
            expense.setExpenseId(cursor.getInt(0));
            expense.setExpenseNumber(cursor.getString(1));
            expense.setExpenseDate(cursor.getString(2));
            expense.setExpenseType(cursor.getString(3));
            expense.setExpenseMileage(cursor.getString(4));
            expense.setExpenseVoucherNo(cursor.getString(5));
            expense.setExpenseVendor(cursor.getString(7));
            expense.setExpenseCurrency(cursor.getString(9));
            expense.setExpenseAmount(cursor.getString(10));
            expense.setExpenseRemarks(cursor.getString(11));
            expense.setExpenseSignature(cursor.getString(12));
            expense.setExpensePostDate(cursor.getString(13));
            expense.setExpensePostTime(cursor.getString(14));
            expense.setExpenseApprovedAmount(cursor.getString(15));
            expense.setExpenseStatus(cursor.getString(16));
            expense.setExpenseReason(cursor.getString(25));

            expenseArrayList.add(expense);
        }
        cursor.close();
        sqlWrite.close();
        return expenseArrayList;
    }


    public void updateExpenseMaster(String expenseNumber, String approvedAmount,String status, String expenseReason) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_EXPENSE_APPROVED_AMOUNT, approvedAmount);
        values.put(COLUMN_EXPENSE_STATUS, status);
        values.put(COLUMN_EXPENSE_REASON, expenseReason );

        db.update(TABLE_EXPENSE, values, COLUMN_EXPENSE_NUMBER + " = ?", new String[]{expenseNumber});
        db.close();
    }

    public void updateSignatureimage(String expenseNum, String filePath, String fileName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_EXPENSE_SIGNATURE_PATH, filePath);
        values.put(COLUMN_EXPENSE_SIGNATURE_NAME, fileName);
        db.update(TABLE_EXPENSE, values, COLUMN_EXPENSE_NUMBER + " = ?", new String[]{expenseNum});
        db.close();
    }

    public void updateExpenseSignUploadFlag(String expenseNum, String postDate) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update xxmsales_expense set isImageUploaded='1' where expenseNumber= " + expenseNum + " and postDate='" + postDate + "'");
        db.close();

    }

    public ArrayList<Expense> getExpenseSignImageService() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery(
                "select expenseNumber,postDate,signatureFilePath  from xxmsales_expense where isImageUploaded=0 and signatureFilePath NOT NULL"
                , null);
        ArrayList<Expense> tripDetailsArrayList = new ArrayList<Expense>();
        while (data.moveToNext()) {
            Expense expense = new Expense();
            expense.setTabCode(Constants.TAB_CODE);
            expense.setCompanyCode(Constants.COMPANY_CODE);
            expense.setBdeCode(Constants.SR_CODE);
            expense.setBdeName(Constants.SR_NAME);

            expense.setExpenseNumber(data.getString(0));
            expense.setExpensePostDate(data.getString(1));
            expense.setSignatureFilePath(data.getString(2));


            tripDetailsArrayList.add(expense);

        }
        return tripDetailsArrayList;
    }




    public void deleteExpense() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EXPENSE, null, null);
        db.close();
    }

}
