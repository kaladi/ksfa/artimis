package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.ViewStockWarehouseProduct;

public class StockViewWarehouseAdapter extends RecyclerView.Adapter<StockViewWarehouseAdapter.ViewHolder> {

    public ArrayList<ViewStockWarehouseProduct> mDataset;
    public Context context;
    DecimalFormat decimalFormat, quantitydecimalFormat;
    double quantityDouble, priceDouble, totalPriceDouble;
    String formatQuantity, formatPrice, formatTotalPrice;
    private RecyclerViewItemListener mListener;

    public StockViewWarehouseAdapter(ArrayList<ViewStockWarehouseProduct> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(ViewStockWarehouseProduct item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(ViewStockWarehouseProduct item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockViewWarehouseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_view_warehouse, parent, false);

        context = parent.getContext();
        decimalFormat = new DecimalFormat("#,###.00");
        quantitydecimalFormat = new DecimalFormat("#,###");


        ViewHolder vh = new ViewHolder(v,mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.productUom.setText(mDataset.get(position).getProductUom());
        viewHolder.productOrderCount.setText(mDataset.get(position).getPrimaryOrderCount());
        System.out.println("mDataset.get(position).getBatchControlled() = " + mDataset.get(position).getBatchControlled());
        viewHolder.productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size() && mDataset.get(position).getBatchControlled() != null && mDataset.get(position).getBatchControlled().equals("Yes")) {
                    viewHolder.mListener.onClick(v, position);
                }
            }
        });


        if (!TextUtils.isEmpty(mDataset.get(position).getProductQuantity())) {
            quantityDouble = Double.parseDouble(mDataset.get(position).getProductQuantity());
            formatQuantity = quantitydecimalFormat.format(quantityDouble);
            String value = formatQuantity;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.productQuantity.setText(value);
        }


        if (!TextUtils.isEmpty(mDataset.get(position).getProductPrice())) {
            priceDouble = Double.parseDouble(mDataset.get(position).getProductPrice());
            formatPrice = decimalFormat.format(priceDouble);
            String value = formatPrice;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.productPrice.setText(value);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getProductTotalPrice())) {
            Log.d("MyDataDisplay", "Total Price" + mDataset.get(position).getProductTotalPrice());
            totalPriceDouble = Double.parseDouble(mDataset.get(position).getProductTotalPrice());
            formatTotalPrice = decimalFormat.format(totalPriceDouble);
            String value = formatTotalPrice;
            if (value != null && !value.equals("")) {
                if (value.startsWith(".")) {
                    value = "0" + value;
                }
            }
            viewHolder.productTotalPrice.setText(value);
        }


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName, productUom, productQuantity, productPrice, productTotalPrice, productOrderCount;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            productName = v.findViewById(R.id.warehouseProductNameTextView);
            productUom = v.findViewById(R.id.warehouseProductUomTextView);
            productQuantity = v.findViewById(R.id.warehouseProductQuantityTextView);
            productPrice = v.findViewById(R.id.warehouseProductPriceTextView);
            productTotalPrice = v.findViewById(R.id.warehouseProductTotalValueTextView);
            productOrderCount = v.findViewById(R.id.warehouseOrdersCountTextView);
        }
    }


}