package in.kumanti.emzor.model;

public class Feedback {

    private String tabCode;
    private String companyCode;
    private String srCode;
    private String customerCode;
    private String feedbackType;
    private String actionTaken;
    private String feedbackDate;
    private String feedbackText;
    private String feedbackId;
    private String feedbackCode;
    private String actionTakenDate;
    private String actionTakenStatus;
    private String visitSequenceNumber;

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }

    public String getActionTakenDate() {
        return actionTakenDate;
    }

    public void setActionTakenDate(String actionTakenDate) {
        this.actionTakenDate = actionTakenDate;
    }

    public String getFeedbackCode() {
        return feedbackCode;
    }

    public void setFeedbackCode(String feedbackCode) {
        this.feedbackCode = feedbackCode;
    }

    public String getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(String feedbackId) {
        this.feedbackId = feedbackId;
    }


    public String getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(String feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(String feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeedbackText() {
        return feedbackText;
    }

    public void setFeedbackText(String feedbackText) {
        this.feedbackText = feedbackText;
    }


    public String getTabId() {
        return tabCode;
    }

    public void setTabId(String tabId) {
        this.tabCode = tabId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }


}
