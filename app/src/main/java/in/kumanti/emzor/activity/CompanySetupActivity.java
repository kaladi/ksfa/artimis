package in.kumanti.emzor.activity;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.CompanySetupTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CompanySetup;


public class CompanySetupActivity extends MainActivity {

    public TextView marqueeText, shortName, contactPerson, emailId, mobileNumber, supportNumber1, supportNumber2, supportEmail;
    String login_id = "";
    Button confirmButton;
    ImageView actionbarBackButton, deviceInfo, companyLogoImageView;
    String checkin_time = "";
    String customer_id = "";
    TextView companyName;
    TextView actionbarTitle;
    MyDBHandler myDBHandler;
    ArrayList<CompanySetup> companySetupArrayList;
    ArrayAdapter<CompanySetup> companySetupSpinnerArrayAdapter;
    CompanySetupTable companySetupTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_company_setup);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Company Setup");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        Cursor companyDetails = myDBHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {

        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName.setText(companyDetails.getString(0));
                shortName.setText(companyDetails.getString(1));
                contactPerson.setText(companyDetails.getString(2));
                emailId.setText(companyDetails.getString(3));
                mobileNumber.setText(companyDetails.getString(4));
                supportNumber1.setText(companyDetails.getString(5));
                supportNumber2.setText(companyDetails.getString(6));
                supportEmail.setText(companyDetails.getString(7));
                // supportEmail.setText(companyDetails.getString(7));

                try {
                    Bitmap customerImageBitmap = dbHandler.getCompanyLogoImage(companyName.getText().toString());
                    if (customerImageBitmap != null) {
                        companyLogoImageView.setImageBitmap(customerImageBitmap);
                    }
                } catch (Exception e) {
                    Log.d("Company", "Exception" + e);

                }


            }
        }

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);
        companyName = findViewById(R.id.companyNameSpinner);
        contactPerson = findViewById(R.id.contactPersonTv);
        emailId = findViewById(R.id.emailIdTv);
        mobileNumber = findViewById(R.id.mobileNumberTv);
        supportNumber1 = findViewById(R.id.supportNumber1Tv);
        supportNumber2 = findViewById(R.id.supportNumber2Tv);
        supportEmail = findViewById(R.id.supportEmailTv);
        companyLogoImageView = findViewById(R.id.companyLogoImageView);
        confirmButton = findViewById(R.id.confirmImageView);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        shortName = findViewById(R.id.shortNameTv);

        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        companySetupTable = new CompanySetupTable(getApplicationContext());
    }

    public void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            Log.d("LoginId", login_id);

        }

    }


    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }


}
