package in.kumanti.emzor.backgroundService;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.mastersData.GPSTrackerModel;
import in.kumanti.emzor.model.GeneralSettings;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostingDataGPSService extends IntentService {

    public static final String MY_ACTION = "action.SOME_ACTION";
    private static final int MY_JOB_INTENT_SERVICE_ID = 500;
    GeneralSettingsTable generalSettingsTable;

    public PostingDataGPSService() {
        super("test-service");
    }

    @Override
    public void onCreate() {
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());
        super.onCreate(); // if you override onCreate(), make sure to call super().
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                String startTime = "00:00", endTime = "23.59";
                GeneralSettings gs = generalSettingsTable.getSettingByKey("enableGps");
                if (gs != null && gs.getValue().equals("Yes")) {

                    GeneralSettings gs1 = generalSettingsTable.getSettingByKey("gpsStartTime");
                    if (gs1 != null) {
                        startTime = gs1.getValue();
                    }
                    GeneralSettings gs2 = generalSettingsTable.getSettingByKey("gpsEndTime");
                    if (gs2 != null) {
                        endTime = gs2.getValue();
                    }
                    SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
                    try {
                        long date = System.currentTimeMillis();
                        String currentTime = parser.format(date);
                        Date start = parser.parse(startTime);
                        Date end = parser.parse(endTime);
                        Date userDate = parser.parse(currentTime);
                        if (userDate.after(start) && userDate.before(end)) {
                            postingData();
                        }
                    } catch (ParseException e) {
                        // Invalid date was entered
                    }
                }


            }
        }).run();
    }


    private void postingData() {

        Gson gson = new GsonBuilder().setLenient().create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL).
                        client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the Gson ConverterFactory to directly convert json data to object
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        //Fetching the orders
        ArrayList<GPSTrackerModel> gpsTrackerModelArrayList = dbHandler.getGPSDataService();
        JsonArray gpsJsonArray = gson.toJsonTree(gpsTrackerModelArrayList).getAsJsonArray();

        /*
         * Adding the all Json Array of objects into a Json Object
         */
        JsonObject jp = new JsonObject();
        jp.add("gpsDetails", gpsJsonArray);

        Log.i("onResponse: POSTGP-----", "" + jp.toString());

        try {
            //Inserting the Json object into the Api
            Call<ResponseBody> call = api.sendGPSdata(jp);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    System.out.println("TTT::gpsJsonArray = " + gpsJsonArray);
                    String[] gpsIds = new String[gpsTrackerModelArrayList.size()];
                    int i = 0;
                    for (GPSTrackerModel oh : gpsTrackerModelArrayList) {
                        gpsIds[i] = oh.getGps_id();
                        i++;
                        Log.i("onResponse: Data--", "oh " + oh.getGps_id());
                    }
                    dbHandler.updateGPSSync(gpsIds);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("onResponse: failure----", "" + t);
                    //Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });

            Log.i("MyTestService", "Service running");
        } catch (Exception e) {
            Log.i("MyTestService", "Exception GPS");

        }
    }

}
