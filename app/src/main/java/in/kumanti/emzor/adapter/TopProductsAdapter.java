package in.kumanti.emzor.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.TopProductDrillList;
import in.kumanti.emzor.model.TopProductsItems;


public class TopProductsAdapter extends ArrayAdapter<TopProductsItems> {

    DecimalFormat formatter = new DecimalFormat("#,###.00");
    ArrayList<TopProductDrillList> orderDetailsListArrayList;
    TopProductDrillList orderDetailsListArrayList1;
    OrderDetailsListAdapter adapter = null;
    MyDBHandler dbHandler;
    String login_id, customer_id, customer_type = "";
    int serial_num = 0;
    DecimalFormat formatter1;
    private LayoutInflater mInflater;
    private ArrayList<TopProductsItems> users;
    private int mViewResourceId;
    private Context c;
    private RecyclerView recyclerView;


    public TopProductsAdapter(Context context, int textViewResourceId, ArrayList<TopProductsItems> users, String login_id1, String customer_id1, String customer_type1) {
        super(context, textViewResourceId, users);
        this.users = users;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
        formatter = new DecimalFormat("#,###.00");
        c = context;
        dbHandler = new MyDBHandler(c, null, null, 1);
        login_id = login_id1;
        customer_id = customer_id1;
        customer_type = customer_type1;
        formatter1 = new DecimalFormat("#,###");

    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        convertView = mInflater.inflate(mViewResourceId, null);
        final TopProductsItems user = users.get(position);

        if (user != null) {

            viewHolder = new ViewHolder();
            viewHolder.product = convertView.findViewById(R.id.product);
            viewHolder.quantity = convertView.findViewById(R.id.quantity);
            viewHolder.price = convertView.findViewById(R.id.price);
            viewHolder.value = convertView.findViewById(R.id.value);
            viewHolder.transactional_value = convertView.findViewById(R.id.transactional_value);

            if (viewHolder.product != null) {
                viewHolder.product.setText(user.getProduct());
            }
            if (viewHolder.quantity != null) {
                viewHolder.quantity.setText((user.getQuantity()));
            }
            if (viewHolder.price != null) {
                viewHolder.price.setText((user.getPrice()));
            }
            if (viewHolder.value != null) {
                viewHolder.value.setText((user.getValue()));
            }
            if (viewHolder.transactional_value != null) {
                viewHolder.transactional_value.setText((user.getTransaction_count()));
            }
        }

        viewHolder.product.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                final String id = user.getProduct_id();
                final String prod = user.getProduct();

                prod_details(v, position, id, prod);
            }

        });

        viewHolder.quantity.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                final String id = user.getProduct_id();
                final String prod = user.getProduct();

                prod_details(v, position, id, prod);
            }

        });

        viewHolder.price.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                final String id = user.getProduct_id();
                final String prod = user.getProduct();

                prod_details(v, position, id, prod);
            }

        });

        viewHolder.value.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                final String id = user.getProduct_id();
                final String prod = user.getProduct();

                prod_details(v, position, id, prod);
            }

        });

        viewHolder.transactional_value.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                final String id = user.getProduct_id();
                final String prod = user.getProduct();

                prod_details(v, position, id, prod);
            }

        });

        return convertView;
    }

    public void prod_details(final View v, final int position, final String id, final String prod) {

        final Dialog alertbox = new Dialog(v.getRootView().getContext());

        LayoutInflater mInflater = (LayoutInflater)
                c.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View alertLayout = mInflater.inflate(R.layout.activity_top_products_popup, null);
        alertbox.setCancelable(false);
        alertbox.setContentView(alertLayout);


        final Button back = alertLayout.findViewById(R.id.back);
        final TextView prod_name = alertLayout.findViewById(R.id.prod_name);

        recyclerView = alertLayout.findViewById(R.id.order_list);
        prod_name.setText(prod);

        orderDetailsListArrayList = new ArrayList<TopProductDrillList>();
        Cursor data;
        if ("Secondary".equals(customer_type)) {
            data = dbHandler.getInvoiceTopproductDetails(id, customer_id);

        } else {
            data = dbHandler.getTopproductDetails(id, customer_id);

        }
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(c, "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {
                serial_num = serial_num + 1;

                String inputDateStr = data.getString(1);
                String quantity_val = data.getString(3);
                int quantity_val1 = Integer.parseInt(quantity_val);
                String formatted_quantity = formatter1.format(quantity_val1);


                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                Date date = null;
                try {
                    date = inputFormat.parse(inputDateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputDateStr = outputFormat.format(date);
                orderDetailsListArrayList1 = new TopProductDrillList(serial_num, outputDateStr, data.getString(2), formatted_quantity, data.getString(4));
                orderDetailsListArrayList.add(i, orderDetailsListArrayList1);
                i++;
            }
            serial_num = 0;
            adapter = new OrderDetailsListAdapter(orderDetailsListArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(c);
            // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(adapter);
        }

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                alertbox.dismiss();

            }

        });

        alertbox.show();
    }

    public class ViewHolder {
        TextView product, quantity, price, value, transactional_value;
    }

    public class OrderDetailsListAdapter extends RecyclerView.Adapter<OrderDetailsListAdapter.MyViewHolder> {

        private ArrayList<TopProductDrillList> users;

        public OrderDetailsListAdapter(ArrayList<TopProductDrillList> users) {
            this.users = users;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_top_products_drilldata, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            TopProductDrillList movie = users.get(position);
           /* if(position == 0) //this will let you know the first child item
            {
                holder.date.setBackgroundDrawable(ContextCompat.getDrawable(c, R.drawable.border_header) );
                holder.order_num.setBackgroundDrawable(ContextCompat.getDrawable(c, R.drawable.border_header) );
                holder.customer_name.setBackgroundDrawable(ContextCompat.getDrawable(c, R.drawable.border_header) );
                holder.location.setBackgroundDrawable(ContextCompat.getDrawable(c, R.drawable.border_header) );
                holder.city.setBackgroundDrawable(ContextCompat.getDrawable(c, R.drawable.border_header) );

                holder.date.setTextColor(Color.WHITE);
                holder.order_num.setTextColor(Color.WHITE);
                holder.customer_name.setTextColor(Color.WHITE);
                holder.location.setTextColor(Color.WHITE);
                holder.city.setTextColor(Color.WHITE);


                holder.date.setGravity(Gravity.CENTER);
                holder.date.setTypeface(holder.date.getTypeface(), Typeface.BOLD);

                holder.order_num.setTypeface(holder.order_num.getTypeface(), Typeface.BOLD);
                holder.order_num.setGravity(Gravity.CENTER);

                holder.customer_name.setTypeface(holder.customer_name.getTypeface(), Typeface.BOLD);
                holder.customer_name.setGravity(Gravity.CENTER);

                holder.location.setTypeface(holder.location.getTypeface(), Typeface.BOLD);
                holder.location.setGravity(Gravity.CENTER);
                holder.city.setTypeface(holder.city.getTypeface(), Typeface.BOLD);
                holder.city.setGravity(Gravity.CENTER);



            } */

            Double val = Double.parseDouble(movie.getCity());
            String val1 = formatter.format(val);

            holder.date.setText(String.valueOf(movie.getDate()));
            holder.order_num.setText(movie.getOrder_num());
            holder.customer_name.setText(movie.getCustomer_name());
            holder.location.setText(movie.getLocation());
            holder.city.setText(val1);


        }

        @Override
        public int getItemCount() {
            return users.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView date, order_num, customer_name, location, city;

            public View vm;

            public MyViewHolder(View view) {
                super(view);
                date = view.findViewById(R.id.date);
                order_num = view.findViewById(R.id.order_num);
                customer_name = view.findViewById(R.id.customer_name);
                location = view.findViewById(R.id.location);
                city = view.findViewById(R.id.city);
                vm = view;

            }
        }

    }
}