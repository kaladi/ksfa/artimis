package in.kumanti.emzor.adapter;

import android.Manifest;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.utils.Constants;

public class GPSTrackerService extends Service {
    private static final String TAG = "BOOMBOOMTESTGPS";
    private static final int LOCATION_INTERVAL = 500000;
    private static final float LOCATION_DISTANCE = 50;
    public String DEFAULT_CHANNEL_ID;
    String track_date_time = null;
    String track_time = null;
    String track_date = null;
    String user_id = null;
    MyDBHandler dbHandler;
    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER)
            //  new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    private LocationManager mLocationManager = null;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        onTaskRemoved(intent);
//        Toast.makeText(getApplicationContext(),"This is a Service running in Background",
//                Toast.LENGTH_SHORT).show();
        return START_STICKY;

    }

   /* @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        *//*   super.onStartCommand(intent, flags, startId);;
        return START_STICKY;*//*

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification.Builder builder = new Notification.Builder(this, DEFAULT_CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    //  .setContentText(text)
                    .setAutoCancel(true);

            Notification notification = builder.build();
            startForeground(1, notification);

        } else {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setContentTitle(getString(R.string.app_name))
                    // .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true);

            Notification notification = builder.build();

            startForeground(1, notification);
        }
        return START_NOT_STICKY;


    }*/

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
            Log.i(TAG, "gps available");

        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }

    }

    @Override
    public void onDestroy() {

        Calendar date = Calendar.getInstance();
        SimpleDateFormat dateFormat_timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat dateFormat_time = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat_date = new SimpleDateFormat("yyyy-MM-dd");
        track_date_time = dateFormat_timestamp.format(date.getTime());
        track_time = dateFormat_time.format(date.getTime());
        track_date = dateFormat_date.format(date.getTime());
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(GPSTrackerService.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            user_id = Constants.IMEI_NUMBER;
        }else { user_id = telephonyManager.getDeviceId();}


        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {

            Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {

                dbHandler = new MyDBHandler(GPSTrackerService.this, null, null, 1);

                int gps_id = dbHandler.get_gps_id();
                gps_id = gps_id + 1;
                //dbHandler.add_timesheet(track_date, track_time, track_date_time, user_id, "Anbu", location.getLatitude(), location.getLongitude(), "Raja", "Actual GPS STOPPED");
                dbHandler.create_gps_tracker(location.getLatitude(), location.getLongitude(), track_date, track_time, gps_id);

            }
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private class LocationListener implements android.location.LocationListener {

        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {

            Calendar date = Calendar.getInstance();
            SimpleDateFormat dateFormat_timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat dateFormat_time = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat dateFormat_date = new SimpleDateFormat("yyyy-MM-dd");

            track_date_time = dateFormat_timestamp.format(date.getTime());
            track_time = dateFormat_time.format(date.getTime());
            track_date = dateFormat_date.format(date.getTime());
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            if (ActivityCompat.checkSelfPermission(GPSTrackerService.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            //user_id = telephonyManager.getDeviceId();
            Log.e(TAG, "onLocationChanged: RAJA " + location + "rajalatiuder=" + location.getLatitude()
                    + "provider=" + location.getProvider() + track_date + track_time + track_date_time + user_id);
            if (location != null) {
               /*String value="";
               Intent intent = new Intent();
               intent.putExtra("key", location);
               sendBroadcast(intent);*/
             /*  Intent intent = new Intent();
               intent.setAction(MY_ACTION);

               intent.putExtra("DATAPASSED", location.getLatitude());

               sendBroadcast(intent);*/

                //   double lat=location.getLatitude();
                // double lon=location.getLatitude();


                //Log.e(TAG, "double "+lat);
                //Intent i = new Intent(MyService.this, MainActivity.class);
                // i.putExtra("lat",""+location.getLatitude());
                // i.putExtra("lon",""+location.getLongitude());
                // i.putExtra("net",""+location.getProvider());
                // startActivity(i);

                //Textview23.setgps(location.getLatitude(), location.getLongitude());
                // Log.e(TAG,"hi" + location.getLatitude()+location.getLongitude());

                dbHandler = new MyDBHandler(GPSTrackerService.this, null, null, 3);

                int gps_id = dbHandler.get_gps_id();
                gps_id = gps_id + 1;

                dbHandler.create_gps_tracker(location.getLatitude(), location.getLongitude(), track_date, track_time, gps_id);

            }

            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
        }


        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }
}