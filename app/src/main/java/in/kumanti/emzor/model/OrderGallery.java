package in.kumanti.emzor.model;

import android.graphics.Bitmap;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class OrderGallery {

    private Bitmap data;

    public OrderGallery(Bitmap data) {

        this.data = data;
    }

    public Bitmap getData() {
        return data;
    }

    public void setData(Bitmap data) {
        this.data = data;
    }

}

