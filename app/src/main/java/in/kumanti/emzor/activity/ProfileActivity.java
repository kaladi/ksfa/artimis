package in.kumanti.emzor.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.GPSTrackerService;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.backgroundService.FetchingDataService;
import in.kumanti.emzor.backgroundService.MasterRDLService;
import in.kumanti.emzor.backgroundService.MyService;
import in.kumanti.emzor.backgroundService.PostingDataGPSService;
import in.kumanti.emzor.eloquent.LogInOutHistoryTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.LogInOut;
import in.kumanti.emzor.utils.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ProfileActivity extends MainActivity {

    private static final String[] paths = {"Select Role", "Admin", "User"};
    MyDBHandler dbHandler;
    EditText old_password,new_password, confirm_password;
    ImageView btn_back;
    Button change_password_btn;
    TextView  email_id, mobile_number;
    TextView sr_name;
    String  login_signup,user_role;
    DrawerLayout drawer;
    ImageView info;
    TextView actionbarTitle;
    ImageView bdeImageView;
    private TextView marqueeText;

    LogInOutHistoryTable logInOutHistoryTable;
    String imel_no;
    String changePwd="",passwordOld="";
    public ImageButton tvPwdSubmit,tvPwdCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_profile);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Profile");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
        }


        if ("true".equals(login_signup)) {
            toolbar.setNavigationIcon(null);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        } else {

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProfileActivity.this,
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        getUserDetails();

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();


            }
        });


        change_password_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                getChangePasswordDialog();

            }
        });




    }

    private void stopBackgroundService() {

        stopService(new Intent(getApplicationContext(), GPSTrackerService.class));
//        stopService(new Intent(getApplicationContext(), PostingDataService.class));
        stopService(new Intent(getApplicationContext(), MyService.class));
        stopService(new Intent(getApplicationContext(), PostingDataGPSService.class));
        stopService(new Intent(getApplicationContext(), MasterRDLService.class));
        stopService(new Intent(getApplicationContext(), FetchingDataService.class));

    }

    private void getUserDetails() {

        Cursor userDeatils = dbHandler.getUserDeatils();
        int count1 = userDeatils.getCount();
        if (count1 == 0) {
        } else {
            int i = 0;
            while (userDeatils.moveToNext()) {

                sr_name.setText(userDeatils.getString(0));
                email_id.setText(userDeatils.getString(2));
                mobile_number.setText(userDeatils.getString(3));
                passwordOld = (userDeatils.getString(5));

            }
        }
    }

    private void initializeViews() {
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        sr_name = findViewById(R.id.sr_name);

        bdeImageView = findViewById(R.id.bdeImageView);

        email_id = findViewById(R.id.email_id);
        mobile_number = findViewById(R.id.mobile_number);

        btn_back = findViewById(R.id.back);
        actionbarTitle = findViewById(R.id.toolbar_title);

        // Load Products
        info = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);
        change_password_btn = findViewById(R.id.change_password_btn);
        logInOutHistoryTable = new LogInOutHistoryTable(getApplicationContext());
    }


    void getChangePasswordDialog() {



        View ChangePasswordToolView = getLayoutInflater().inflate(R.layout.dialog_change_pwd, null, false);

        old_password = ChangePasswordToolView.findViewById(R.id.old_password);
        new_password = ChangePasswordToolView.findViewById(R.id.new_password);
        confirm_password = ChangePasswordToolView.findViewById(R.id.confirm_password);
        tvPwdSubmit = ChangePasswordToolView.findViewById(R.id.tvPwdSubmit);
        tvPwdCancel = ChangePasswordToolView.findViewById(R.id.tvPwdCancel);


        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setView(ChangePasswordToolView);
        final android.app.AlertDialog alert = builder.create();
        alert.show();


        old_password.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if(!s.equals("") ) {
                    //do your work here
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            public void afterTextChanged(Editable s) { }
        });

        tvPwdSubmit.setOnClickListener(v -> {
            hideSoftKeyboard(getApplicationContext(), v);

            String old_password1 = old_password.getText().toString();
            String new_password1 = new_password.getText().toString();
            String confirm_password1 = confirm_password.getText().toString();

            if (TextUtils.isEmpty(old_password1) && TextUtils.isEmpty(new_password1) && TextUtils.isEmpty(confirm_password1) ) {
                Toast.makeText(getApplicationContext(), "Please Enter Password", Toast.LENGTH_LONG).show();
                return;
            }

            if (old_password1.equals(confirm_password1)) {
                Toast.makeText(getApplicationContext(), "The Old Password must not equal to New password", Toast.LENGTH_LONG).show();
                return;
            }

            boolean cancel = false;
            View focusView = null;


          if (new_password1.length() < 6) {
                new_password.setError("Password should contains min 6 character");
              return;
          }  else if (confirm_password1.length() < 6) {
                confirm_password.setError("Password should contains min 6 character");
              return;
          }else if (!old_password1.equals(passwordOld)) {
                old_password.setError("Old Password does not match");
              return;
          }  else if (!new_password1.equals(confirm_password1)) {
                confirm_password.setError("Password does not match");
              return;
            }else {

                String new_password2 = new_password.getText().toString();
                String confirm_password2 = confirm_password.getText().toString();
                String mobile_number1 = mobile_number.getText().toString();
                String email1 = email_id.getText().toString();

                dbHandler.update_user_setup(new_password2, confirm_password2, mobile_number1);

              Cursor tabDetails = dbHandler.getImei();
              int tabrowCou = tabDetails.getCount();

              if (tabrowCou != 0) {
                  while (tabDetails.moveToNext()) {

                      Constants.IMEI_NUMBER=tabDetails.getString(0);


                  }
              }

              Gson gson = new GsonBuilder().setLenient().create();
              Retrofit retrofit = new Retrofit.Builder()
                      .baseUrl(ApiInterface.BASE_URL)
                      .addConverterFactory(GsonConverterFactory.create(gson))
                      .build();

              ApiInterface api = retrofit.create(ApiInterface.class);
              Call<ResponseBody> call = api.postingChangePassword(confirm_password2,email1, Constants.IMEI_NUMBER, Constants.COMPANY_CODE);

              System.out.println("TT::confirm_password2 = " + confirm_password2);
              System.out.println("TT::email1 = " + email1);
              System.out.println("TT::Constants.IMEI_NUMBER = " + Constants.IMEI_NUMBER);
              System.out.println("TT::Constants.COMPANY_CODE = " + Constants.COMPANY_CODE);
              Toast.makeText(getApplicationContext(), "Password Changed successfully", Toast.LENGTH_LONG).show();

              if ("User".equals(user_role)) {
                  LogInOut logInOut = new LogInOut();
                  logInOut.setLogOutDate(logOutDate);
                  logInOut.setLogOutTime(logOutTime);
                  String logInOutId = sharedPreferences.getString("logInOutId", "");
                  logInOutHistoryTable.updaterLogout(logInOut, logInOutId);
              }
              Intent intent = new Intent(getApplicationContext(), StartUpActivity.class);
              globals.setLogin_email("");
              globals.setLogin_id("");
              intent.putExtra("user_role", user_role);
              stopBackgroundService();

              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
              startActivity(intent);


              call.enqueue(new Callback<ResponseBody>() {
                  @Override
                  public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                      try {
                          Log.d("EMAIL SEND:", "Success----" + response.body().string());
                      } catch (Exception e) {

                      }
                  }

                  @Override
                  public void onFailure(Call<ResponseBody> call, Throwable t) {

                      Log.i("EMAIL SEND: ", "failure------" + t);
                      Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                  }

              });
            }



          /*  if ((etpassword.getText().toString()).length() >= 5){
                changePwd=etpassword.getText().toString();
                String email1 = email_id.getText().toString();
                dbHandler.update_user_password(changePwd,email1);
            }else{
                Toast.makeText(getApplicationContext(), "Please enter atleast 5 Characters", Toast.LENGTH_LONG).show();
                return;
            }

            System.out.println("TTT::etpassword. = " + etpassword.getText().toString());*/


            alert.dismiss();

        });


        tvPwdCancel.setOnClickListener(v -> {
            alert.dismiss();
        });



    }

    private void populateHeaderDetails() {
        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            login_signup = bundle.getString("login_signup");
            user_role = bundle.getString("user_role");
        }

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // bdeImageView.setEnabled(true);
            }
        }
    }


    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

}
