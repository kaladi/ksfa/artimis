package in.kumanti.emzor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import in.kumanti.emzor.R;
import in.kumanti.emzor.model.CompetitorStockProduct;
import in.kumanti.emzor.model.CompetitorProduct;
import in.kumanti.emzor.utils.DecimalDigitsInputFilter;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class CompetitorStockProductAdapter extends RecyclerView.Adapter<CompetitorStockProductAdapter.ViewHolder> {

    public ArrayList<CompetitorStockProduct> mDataset;
    public Context context;
    int spinnerSize = 4;
    String[] spinnerArray = new String[spinnerSize];
    HashMap<Integer,String> spinnerMap = new HashMap<Integer, String>();
    ArrayList<CompetitorProduct> competitorListArrayProduct;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView competitor_name;
        public CheckBox competitor_availability_checkbox;
        public EditText competitor_volume_editText, competitor_price_editText, competitor_promotion_editText;
        public ImageButton competitor_details_remove_button;

        public ViewHolder(View v) {
            super(v);

            competitor_name =  v.findViewById(R.id.competitor_name_textview);
            competitor_availability_checkbox =  v.findViewById(R.id.competitor_availability_checkbox);
            competitor_volume_editText =  v.findViewById(R.id.competitor_volume_editText);
            competitor_price_editText =  v.findViewById(R.id.competitor_price_editText);
           // competitor_promotion_editText =  v.findViewById(R.id.competitor_promotion_editText);
            competitor_details_remove_button = v.findViewById(R.id.competitor_details_remove_button);
            competitor_volume_editText.setFilters(new InputFilter[]{ new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});
            competitor_price_editText.setFilters(new InputFilter[]{ new DecimalDigitsInputFilter()});
        }
    }

    public void add(CompetitorStockProduct item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(CompetitorStockProduct item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public CompetitorStockProductAdapter(ArrayList<CompetitorStockProduct> myDataset, ArrayList<CompetitorProduct> productList) {
        mDataset = myDataset;
        competitorListArrayProduct = productList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_competitor_details, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.competitor_availability_checkbox.setOnCheckedChangeListener(null);
        //To Set the Values to the Spinner
        String cpid = mDataset.get(position).getCompetitor_product_id();
        Log.d("SpinnerIndex 1",""+cpid);



        if (viewHolder.competitor_name != null) {
            Log.d("SpinnerIndex","");
            viewHolder.competitor_name.setText(mDataset.get(position).getProductName());
        }
        Log.d("BooleanCheck1",""+mDataset.get(position).getCompetitor_product_availability());

        //To Set the values to the Availabilty Checkbox
        if (viewHolder.competitor_availability_checkbox != null) {
            Log.d("BooleanCheck",""+mDataset.get(position).getCompetitor_product_availability());
            viewHolder.competitor_availability_checkbox.setChecked(mDataset.get(position).getCompetitor_product_availability());
            if(mDataset.get(position).getCompetitor_product_availability())
            {
                viewHolder.competitor_price_editText.setEnabled(true);
                viewHolder.competitor_volume_editText.setEnabled(true);
               // viewHolder.competitor_promotion_editText.setEnabled(true);
            }
        }


        //To Set the values to the Volume
        if (viewHolder.competitor_volume_editText != null && mDataset.get(position).getCompetitor_product_volume()!=null) {
            viewHolder.competitor_volume_editText.setText(String.valueOf(mDataset.get(position).getCompetitor_product_volume()));
        } else {
            viewHolder.competitor_volume_editText.setText("");
        }

        //To Set the values to the Price
        if (viewHolder.competitor_price_editText != null && mDataset.get(position).getCompetitor_product_price()!=null && mDataset.get(position).getCompetitor_product_price()!=0) {
            viewHolder.competitor_price_editText.setText(String.valueOf(mDataset.get(position).getCompetitor_product_price()));
        } else {
            viewHolder.competitor_price_editText.setText("");
        }

       /* //To Set the values to the Promotion
        if (viewHolder.competitor_promotion_editText != null && mDataset.get(position).getCompetitor_product_promotion()!=null) {
            viewHolder.competitor_promotion_editText.setText(String.valueOf(mDataset.get(position).getCompetitor_product_promotion()));
        } else {
            viewHolder.competitor_promotion_editText.setText("");
        }
*/
        //To Set Error message
        if (!TextUtils.isEmpty(mDataset.get(position).getError_competitor_product_volume())&&(mDataset.get(position).getCompetitor_product_volume()==null || mDataset.get(position).getCompetitor_product_volume()==0)) {
            viewHolder.competitor_volume_editText.setError(mDataset.get(position).getError_competitor_product_volume());
        } else {
            viewHolder.competitor_volume_editText.setError(null);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getError_competitor_product_price())&&(mDataset.get(position).getCompetitor_product_price()==null||mDataset.get(position).getCompetitor_product_price()==0)) {
            viewHolder.competitor_price_editText.setError(mDataset.get(position).getError_competitor_product_price());
        } else {
            viewHolder.competitor_price_editText.setError(null);
        }

        //we need to update adapter once we finish with editing
        viewHolder.competitor_availability_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if(position < mDataset.size()){
                    mDataset.get(position).productAvailability = isChecked;
                    if(isChecked)
                    {
                        viewHolder.competitor_price_editText.setEnabled(true);
                        viewHolder.competitor_volume_editText.setEnabled(true);
                       // viewHolder.competitor_promotion_editText.setEnabled(true);
                    }
                    else
                    {
                        mDataset.get(position).productVolume = null;
                        mDataset.get(position).productPrice = null;
                        mDataset.get(position).setError_competitor_product_price(null);
                        mDataset.get(position).setCompetitor_product_volume(null);
                       // mDataset.get(position).promotionText = "";
                        viewHolder.competitor_volume_editText.setText(null);
                        viewHolder.competitor_price_editText.setText(null);
                      //  viewHolder.competitor_promotion_editText.setText(null);
                        viewHolder.competitor_price_editText.setEnabled(false);
                        viewHolder.competitor_volume_editText.setEnabled(false);
                       // viewHolder.competitor_promotion_editText.setEnabled(false);
                        viewHolder.competitor_volume_editText.setError(null);
                        viewHolder.competitor_price_editText.setError(null);
                      //  viewHolder.competitor_promotion_editText.setError(null);
                    }
                }
            }
        });

        //we need to update adapter once we finish with editing
        viewHolder.competitor_volume_editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if(position < mDataset.size()) {
                    final EditText Caption = (EditText) v;
                    mDataset.get(position).productVolume = Caption.getText().toString().isEmpty()?null:Integer.parseInt(Caption.getText().toString());
                }
            }
        });

        //we need to update adapter once we finish with editing
        viewHolder.competitor_price_editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if(position < mDataset.size()) {
                    final EditText Caption = (EditText) v;
                    mDataset.get(position).productPrice = Caption.getText().toString().isEmpty() ? null : Double.parseDouble(Caption.getText().toString());
                }
            }
        });

     /*   //we need to update adapter once we finish with editing
        viewHolder.competitor_promotion_editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                //if (!hasFocus) {
                    //final int position = v.getId();
                    final EditText Caption = (EditText) v;
                mDataset.get(position).promotionText = Caption.getText().toString().isEmpty()?null:Caption.getText().toString();
                //}
            }
        });*/

        viewHolder.competitor_details_remove_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    try{
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                        View focusedView = getActivity(context).getCurrentFocus();
                        focusedView.clearFocus();

                        if(position < mDataset.size()) {

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                            alertDialog.setTitle("Delete Product Info?");
                            //alertDialog.setMessage("your message ");
                            alertDialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            alertDialog.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mDataset.remove(position);
                                    notifyDataSetChanged();
                                }
                            });

                            AlertDialog dialog = alertDialog.create();
                            dialog.show();

                        }

                    }catch (Exception e) {

                    }

            }
        });
    }


    private Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }




}

