package in.kumanti.emzor.model;

public class InvoiceItems {

    private String tabCode;
    private String companyCode;
    private String srCode;
    private String customerCode;
    private String invoiceId;
    private String invoiceCode;
    private String invoiceDate;
    private String invoiceLineId;
    private String status;
    private String productCode;
    private String productName;
    private String productUom;
    private String quantity;
    private String price;
    private String value;
    private String discount;
    private String vat;
    private String vatWithoutDiscount;
    private String valueWithoutVat;
    private String valueWithVat;
    private String discountValue;

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }


    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getVatWithoutVat() {
        return valueWithoutVat;
    }

    public void setVatWithoutVat(String vatWithoutVat) {
        this.valueWithoutVat = vatWithoutVat;
    }

    public String getTabId() {
        return tabCode;
    }

    public void setTabId(String tabId) {
        this.tabCode = tabId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceLineId() {
        return invoiceLineId;
    }

    public void setInvoiceLineId(String invoiceLineId) {
        this.invoiceLineId = invoiceLineId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVatWithoutDiscount() {
        return vatWithoutDiscount;
    }

    public void setVatWithoutDiscount(String vatWithoutDiscount) {
        this.vatWithoutDiscount = vatWithoutDiscount;
    }

    public String getValueWithVat() {
        return valueWithVat;
    }

    public void setValueWithVat(String valueWithVat) {
        this.valueWithVat = valueWithVat;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }
}
