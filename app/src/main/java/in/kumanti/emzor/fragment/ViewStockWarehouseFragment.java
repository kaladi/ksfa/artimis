package in.kumanti.emzor.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.StockReceiptBatchActivity;
import in.kumanti.emzor.activity.ViewStockBatchActivity;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.StockViewWarehouseAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.eloquent.WarehouseStockProductsTable;
import in.kumanti.emzor.eloquent.WarehouseTable;
import in.kumanti.emzor.model.PrimaryInvoiceProduct;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.SrInvoiceBatch;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.model.ViewStockWarehouseProduct;
import in.kumanti.emzor.model.Warehouse;
import in.kumanti.emzor.utils.CustomSearchableSpinner;


public class ViewStockWarehouseFragment extends Fragment {

    public RecyclerView warehouseProductRecyclerView;
    public RecyclerView.Adapter warehouseStockProductAdapter;
    public RecyclerView.LayoutManager warehouseStockProductLayoutManager;
    CustomSearchableSpinner productSpinner, warehouseSpinner;
    ProductTable productTable;
    ArrayList<Product> productArrayList;
    ArrayAdapter<Product> productSpinnerArrayAdapter;
    ArrayAdapter<Warehouse> warehouseSpinnerArrayAdapter;
    WarehouseTable warehouseTable;
    ArrayList<Warehouse> warehouseArrayList;
    ArrayList<ViewStockWarehouseProduct> warehouseStockProductList;
    Context mContext;
    MyDBHandler dbHandler;
    String login_id = "", warehouse_id = "-1", product_id = "-1";
    WarehouseStockProductsTable warehosueStockProductTable;
    RecyclerViewItemListener listener;

    public ViewStockWarehouseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
        }
    }


    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_view_stock_warehouse, container, false);
        mContext = rootView.getContext();
        dbHandler = new MyDBHandler(mContext, null, null, 1);
        productTable = new ProductTable(mContext);
        warehouseTable = new WarehouseTable(mContext);
        warehosueStockProductTable = new WarehouseStockProductsTable(mContext);
        productSpinner = rootView.findViewById(R.id.warehouseStockProductSpinner);
        warehouseSpinner = rootView.findViewById(R.id.warehouseSpinner);
        warehouseProductRecyclerView = rootView.findViewById(R.id.stockViewWarehouseRecyclerView);

        productSpinner.setTitle("Select a Product");
        productSpinner.setPositiveButton("OK");
        warehouseProductRecyclerView.setHasFixedSize(true);
        warehouseStockProductLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        warehouseProductRecyclerView.setLayoutManager(warehouseStockProductLayoutManager);


        try {
            showWarehouseProducts();

            getAllProducts();
            productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    productSpinner.isSpinnerDialogOpen = false;

                    Log.d("Products", "Selected Product" + pos);
                    Product p = (Product) adapterView.getItemAtPosition(pos);
                    Log.d("Products", "Selected Product" + pos + p.product_name);

                    product_id = p.product_id;
                    showWarehouseProducts();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {
                    productSpinner.isSpinnerDialogOpen = false;
                }
            });
            warehouseArrayList = warehosueStockProductTable.getWarehouseList();
            Warehouse w = new Warehouse();
            w.warehouse_name = "All";
            w.warehouse_code = "-1";
            warehouseArrayList.add(0, w);
            warehouseSpinnerArrayAdapter = new ArrayAdapter<Warehouse>(mContext, android.R.layout.simple_spinner_item, warehouseArrayList);
            warehouseSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            warehouseSpinner.setAdapter(warehouseSpinnerArrayAdapter);
            warehouseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    warehouseSpinner.isSpinnerDialogOpen = false;
                    Warehouse w = (Warehouse) adapterView.getItemAtPosition(pos);
                    Log.d("warehouse1", "Selected warehouse1" + pos + w.warehouse_name);

                    warehouse_id = w.warehouse_code;
                    if (warehouse_id.equals("-1")) {
                        getAllProducts();
                    } else {
                        productArrayList = warehosueStockProductTable.getProductsByWareshouse(warehouse_id);
                        Product p = new Product();
                        p.product_name = "All";
                        p.product_id = "-1";
                        productArrayList.add(0, p);
                        productSpinnerArrayAdapter = new ArrayAdapter<Product>(mContext, android.R.layout.simple_spinner_item, productArrayList);
                        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        productSpinner.setAdapter(productSpinnerArrayAdapter);
                    }
                    showWarehouseProducts();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {
                    warehouseSpinner.isSpinnerDialogOpen = false;
                }
            });

        } catch (Exception e) {
            Log.d("ViewStock Error--", "" + e.getMessage());
        }

        listener = (view, position) -> {
            Intent intent = new Intent(getContext(), ViewStockBatchActivity.class);
            intent.putExtra("product_id", warehouseStockProductList.get(position).getProductCode());
            intent.putExtra("product_name", warehouseStockProductList.get(position).getProductName());
            intent.putExtra("product_quantity", warehouseStockProductList.get(position).getProductQuantity());
            intent.putExtra("product_batch_details", warehouseStockProductList.get(position).getBatchDetailArrayList());
            Log.d("DataCheck", "Passed Value Count" + warehouseStockProductList.get(position).getBatchDetailArrayList().size());
            getActivity().startActivityForResult(intent, 1);

        };
        return rootView;
    }

    private void getAllProducts() {
        productArrayList = warehosueStockProductTable.getProductsByWareshouse("-1");
        Log.d("ViewStock--", "War productArrayList  " + productArrayList.size());
        Product p = new Product();
        p.product_name = "All";
        p.product_id = "-1";
        productArrayList.add(0, p);
        productSpinnerArrayAdapter = new ArrayAdapter<Product>(mContext, android.R.layout.simple_spinner_item, productArrayList);
        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(productSpinnerArrayAdapter);
    }

    private void showWarehouseProducts() {

        warehouseProductRecyclerView.setAdapter(null);
        warehouseStockProductList = new ArrayList<ViewStockWarehouseProduct>();
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String today = timeStampFormat.format(myDate);
        Log.d("XXXXX", "Login Id" + login_id + " product " + product_id + " warehouse " + warehouse_id);
        warehouseStockProductList = dbHandler.getWarehouseStockProducts(login_id, warehouse_id, product_id, today);
        System.out.println("warehouseStockProductList.size() = " + warehouseStockProductList.size());
        Log.d("SelectQuery", "Count " + warehouseStockProductList.size());
        warehouseStockProductAdapter = new StockViewWarehouseAdapter(warehouseStockProductList,listener);
        warehouseProductRecyclerView.setAdapter(warehouseStockProductAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("DataCheck", "Entered Child Fragment" + requestCode);
        //super.onActivityResult(requestCode, resultCode, data); //comment this unless you want to pass your result to the activity.
        if (requestCode == 1) {
            Log.d("DataCheck", "Entered in to Check");
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String product_id = (String) bundle.get("product_id");
                ArrayList<SrInvoiceBatch> product_batch_details = (ArrayList<SrInvoiceBatch>) bundle.get("product_batch_details");
               /* for (PrimaryInvoiceProduct pip : invoiceProducts) {
                    if (product_id.equals(pip.getPrimaryInvoiceProductCode())) {
                        Log.d("DataCheck", "Value Assigned in product");
                        pip.setBatchDetailArrayList(product_batch_details);
                        if (product_batch_details.size() != 0)
                            pip.setError_primaryInvoiceProductName(null);
                    }
                }*/
                for (ViewStockWarehouseProduct srwp :   warehouseStockProductList) {
                    if (product_id.equals(srwp.getProductCode())) {
                        Log.d("DataCheck", "Value Assigned in product");
                        srwp.setBatchDetailArrayList(product_batch_details);
                        if (product_batch_details.size() != 0)
//                            srwp.set(null);
                        Log.d("DataCheck", "Values" + srwp.getBatchDetailArrayList().size());
                    }
                }
                warehouseStockProductAdapter = new StockViewWarehouseAdapter(warehouseStockProductList,listener);
                warehouseProductRecyclerView.setAdapter(warehouseStockProductAdapter);
                warehouseStockProductAdapter.notifyDataSetChanged();
            }
        }
    }
}






