package in.kumanti.emzor.model;


public class NewJourneyPlanCustomers {

    public int jpCustomerId;
    public String journeyPlanNumber;
    public String companyCode;
    public String tabCode;
    public String bdeCode;
    public String dayCode;
    public String currentDayDate;
    public String customerCode;
    public String customerName;
    public String customerType;
    public String isSync;
    public String latestSynDate;
    public String createdAt;
    public String updatedAt;

    public int getJpCustomerId() {
        return jpCustomerId;
    }

    public void setJpCustomerId(int jpCustomerId) {
        this.jpCustomerId = jpCustomerId;
    }

    public String getJourneyPlanNumber() {
        return journeyPlanNumber;
    }

    public void setJourneyPlanNumber(String journeyPlanNumber) {
        this.journeyPlanNumber = journeyPlanNumber;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getBdeCode() {
        return bdeCode;
    }

    public void setBdeCode(String bdeCode) {
        this.bdeCode = bdeCode;
    }

    public String getDayCode() {
        return dayCode;
    }

    public void setDayCode(String dayCode) {
        this.dayCode = dayCode;
    }

    public String getCurrentDayDate() {
        return currentDayDate;
    }

    public void setCurrentDayDate(String currentDayDate) {
        this.currentDayDate = currentDayDate;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getIsSync() {
        return isSync;
    }

    public void setIsSync(String isSync) {
        this.isSync = isSync;
    }

    public String getLatestSynDate() {
        return latestSynDate;
    }

    public void setLatestSynDate(String latestSynDate) {
        this.latestSynDate = latestSynDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return customerName;
    }
}
