package in.kumanti.emzor.crm;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.GetLocationActivity;
import in.kumanti.emzor.activity.HomeActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.activity.OrderGalleryActivity;
import in.kumanti.emzor.adapter.CrmOrderProductsAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.CrmProductsTable;
import in.kumanti.emzor.eloquent.CrmStatusTable;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.CustomerShippingAddressTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.model.CrmProducts;
import in.kumanti.emzor.model.CrmStatus;
import in.kumanti.emzor.model.CustomerShippingAddress;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.OrderProductItems;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;


public class CrmOrderActivity extends MainActivity {


    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionbarTitle, marqueeText, customerName, accountNo, city, checkInTime;
    Chronometer duration;

    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    String login_id = "", checkin_time = "";
    String lat_lng, storeCrmOrderDate, storeDeliveryDate, crmOrderTime;

    TextView crmOrderNumber, crmOrderDate, crmOrderCurrency, deliveryDateValue, add1, add2;
    ImageView orderSaveButton, deliveryAddressIcon, captureLpoImage, orderCancelButton, showLpoImagesGallery;

    EditText lpoNumber, remarks ,customerSignatureName;

    Button orderAddProductButton;

    RecyclerView crmOrderProRv;
    RecyclerView.Adapter orderCrmProdAdapter;
    RecyclerView.LayoutManager orderCrmProdLayoutManager;

    ArrayList<CrmProducts> crmOrderProdArrayList = new ArrayList<>();
    double budgetDouble = 0.0, valueDouble = 0.0;
    int crmProductCount = 0;
    boolean closeParent = false;
    TextView crmOrderTotalValue;

    CrmTable crmTable;
    CrmProductsTable crmProductsTable;
    CrmStatusTable crmStatusTable;

    ArrayList<in.kumanti.emzor.model.Product> productsArrayList;
    ArrayAdapter<in.kumanti.emzor.model.Product> crmProductSpinnerArrayAdapter;
    ProductTable productTable;

    String productNameString,selectedProductCode = "", marqueeTextString, productPrice= "";
    CustomSearchableSpinner spProductName;
    TextView tvCrmOrderProName;
    EditText tvCrmOrderProQty, tvCrmOrderProBudget, tvCrmOrderProPrice, tvCrmOrderProValue;
    ImageButton addCrmOrderProdButton;

    String crmOrderNumberString = "";
    public DecimalFormat valueFormatter, quantityFormatter;
    double unformattedValue = 0.0;

    private static final int SELECT_PHOTO1 = 1;
    private static final int CAPTURE_PHOTO1 = 2;

    SharedPreferenceManager sharedPreferenceManager;
    MyDBHandler dbHandler;

    ArrayList<OrderProductItems> orderProductItems;
    int order_id = 0;
    ImageView createSignature, viewSignature, deliveryDateIcon;
    Bitmap selectedImage = null;
    String customerCode = "";
    int line_count;
    String customerNameString, customerEmailAddress;
    String customerAddress1, customerAddress2, customerAddress3;
    String  order_time;
    String sr_name_details, sr_rm_name, sr_region, sr_warehouse;
    int a[] = new int[5];
    DecimalFormat formatter, formatter1, formatter2;
    String format_order_value;
    GPSTracker gpsTracker;
    double latitude, longitude;
    LinearLayout headerDurationContainer;
    int image_id = 0;
    ViewPager addressViewPager;
    ArrayList<CustomerShippingAddress> shippingAddressesData = null;
    CustomerShippingAddressTable shippingAddressesTable;
    String emailType = "CrmOrder";
    String companyName,companyCode1, compAdd1, compAdd2, compAdd3, compMblNo;

    //Variables declaration for print and sms
    double orderTotalValue = 0;
    double vatValue;
    Button smsButton, printButton, emailButton;
    String customerPhoneNumber = "";
    String smsMessageText = "";
    String printMsg;
    Print print;
    PrintBluetooth printBluetooth;

    String crmOrderStatus = "", bdeCode="";
    GeneralSettingsTable generalSettingsTable;
    LpoImagesTable lpoImagesTable;
    String encodedImage;
    GeneralSettings gs;
    private int mYear, mMonth, mDay;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";
    Globals globals;
    RecyclerViewItemListener listener;
    private boolean isCrmOrdProSaved = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_crm_order_product);

        initializeViews();
        getFooterDateTime();
        populateHeaderDetails();

        actionbarTitle.setText(R.string.view_order_header);

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        listener = (view, position) -> {
            calculateCrmValues();
        };

//        getlocation();
        valueFormatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");
        crmOrderNumber.setText(crmOrderNumberString);
        setCrmOrderDate();

        crmOrderProRv.setHasFixedSize(true);
        orderCrmProdLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        crmOrderProRv.setLayoutManager(orderCrmProdLayoutManager);

        crmOrderProdArrayList = crmProductsTable.getOrderCrmProducts(crmOrderNumber.getText().toString());
        bindAdapter();
        calculateCrmValues();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        captureLpoImage.setOnClickListener(v -> {
            if ("".equals(lpoNumber.getText().toString())) {
                Toast.makeText(CrmOrderActivity.this, "Please Enter LPO Number", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent1, CAPTURE_PHOTO1);
            }
        });

        showLpoImagesGallery.setOnClickListener(v -> {
            int lpo_image_count = lpoImagesTable.get_lpo_order_image_count(crmOrderNumber.getText().toString());

            if (lpo_image_count == 0) {
                Toast.makeText(CrmOrderActivity.this, "No Images,Capture and Check again.", Toast.LENGTH_SHORT).show();
            } else {
                Intent galleryIntent = new Intent(getApplicationContext(), OrderGalleryActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("customer_id", customerCode);
                bundle.putString("login_id", login_id);
                bundle.putString("order_number", crmOrderNumber.getText().toString());
                galleryIntent.putExtras(bundle);

                startActivity(galleryIntent);
            }
        });


        deliveryDateIcon.setOnClickListener(v -> {
            final Calendar c = getInstance();
            mYear = c.get(YEAR);
            mMonth = c.get(MONTH);
            mDay = c.get(DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(CrmOrderActivity.this,
                    (view, year, monthOfYear, dayOfMonth) -> {

                        try {
                            String PATTERN = "dd MMM YY";
                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            deliveryDateValue.setText(dateFormat.format(d));

                            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
                            storeDeliveryDate = timeStampFormat.format(d);

                            Log.d("DateCheck","date");
                        } catch (Exception e) {
                            Log.d("DateCheck","date"+e.getMessage());
                        }

                    }, mDay, mMonth, mYear);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        });


        /*
         * addTextChangedListener for the selected product quantity EditText
         */
        deliveryDateValue.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (!deliveryDateValue.getText().toString().equals("")) {
                        deliveryDateValue.setError(null);
                }
            }
        });

        deliveryAddressIcon.setOnClickListener(v -> loadShippingAddress());

        createSignature.setOnClickListener(v -> createNewSignature());

        viewSignature.setOnClickListener(v -> {
            if (selectedImage != null) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.image_preview, null);
                ImageView capturedImage, closeButton;

                capturedImage = alertLayout.findViewById(R.id.capturedImage);
                closeButton = alertLayout.findViewById(R.id.closeImageView);
                capturedImage.setImageBitmap(selectedImage);

                android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());

                alert.setView(alertLayout);
                alert.setNegativeButton("Close", (dialog, which) -> {
                });

                alert.setCancelable(true);

                android.app.AlertDialog dialog = alert.create();
                dialog.show();


            }

        });

        /*
         * addTextChangedListener for the selected product quantity EditText
         */
        customerSignatureName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (!customerSignatureName.getText().toString().equals("")) {
                    customerSignatureName.setError(null);
                }
            }
        });

        deviceInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);
        });

        customerImage.setOnLongClickListener(v -> {
            showcustomerLocation();
            return true;
        });


        orderCancelButton.setOnClickListener(v -> {
            final Dialog alertbox = new Dialog(v.getRootView().getContext());

            LayoutInflater mInflater = (LayoutInflater)
                    getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            final View alertLayout = mInflater.inflate(R.layout.activity_exit_order_popup, null);

            alertbox.setCancelable(false);
            alertbox.setContentView(alertLayout);

            final Button Yes = alertLayout.findViewById(R.id.Yes);
            final Button No = alertLayout.findViewById(R.id.No);

            Yes.setOnClickListener(v13 -> {

                long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                duration.stop();

                Toast.makeText(getApplicationContext(), "Order Cancelled", Toast.LENGTH_LONG).show();

                navigateHomeActivity();
                alertbox.dismiss();

            });

            No.setOnClickListener(v1 -> alertbox.dismiss());

            alertbox.show();

        });

        orderAddProductButton.setOnClickListener(v -> {

            LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.activity_crm_order_add_product, null);

            spProductName = alertLayout.findViewById(R.id.spProductName);
            tvCrmOrderProName = alertLayout.findViewById(R.id.tvCrmOrderProName);
            tvCrmOrderProQty = alertLayout.findViewById(R.id.tvCrmOrderProQty);
            tvCrmOrderProBudget = alertLayout.findViewById(R.id.tvCrmOrderProBudget);
            tvCrmOrderProPrice = alertLayout.findViewById(R.id.tvCrmOrderProPrice);
            tvCrmOrderProValue = alertLayout.findViewById(R.id.tvCrmOrderProValue);
            addCrmOrderProdButton = alertLayout.findViewById(R.id.addCrmOrderProdIb);

            loadCrmProducts();

            spProductName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    in.kumanti.emzor.model.Product p = (in.kumanti.emzor.model.Product) adapterView.getItemAtPosition(pos);
                    Log.d("Products", "Selected Product" + pos + p.product_name);

                    productNameString = p.product_name;
                    selectedProductCode =p.product_code;
                    productPrice = p.product_price;
                    renderProductInfo();

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {
                }
            });

            tvCrmOrderProQty.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    unformattedValue = calculateProdValue(tvCrmOrderProQty,tvCrmOrderProPrice,tvCrmOrderProValue);

                }
            });

            tvCrmOrderProPrice.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    unformattedValue = calculateProdValue(tvCrmOrderProQty,tvCrmOrderProPrice,tvCrmOrderProValue);
                }
            });

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setView(alertLayout);
            alert.setCancelable(false);

            alert.setNegativeButton("CLOSE", (dialog, which) -> {
            });

            AlertDialog dialog = alert.create();
            dialog.show();

            addCrmOrderProdButton.setOnClickListener(v14 -> {
                boolean isValid = true;
                if(productNameString.equals("Select")) {
                    Toast.makeText(getApplicationContext(), "Select Product to View Details", Toast.LENGTH_SHORT).show();
                    isValid = false;
                }


                if (TextUtils.isEmpty(tvCrmOrderProQty.getText().toString())) {
                    tvCrmOrderProQty.setError(getString(R.string.error_field_required));
                    isValid = false;
                } else if ("0".equals(tvCrmOrderProQty.getText().toString())) {
                    tvCrmOrderProQty.setError(getString(R.string.Zero_Validation));
                    isValid = false;
                }else{
                    tvCrmOrderProQty.setError(null);
                }

                /*if (TextUtils.isEmpty(tvCrmOrderProBudget.getText().toString())) {
                    tvCrmOrderProBudget.setError(getString(R.string.error_field_required));
                    isValid = false;
                } else if ("0".equals(tvCrmOrderProBudget.getText().toString())) {
                    tvCrmOrderProBudget.setError(getString(R.string.Zero_Validation));
                    isValid = false;
                }else{
                    tvCrmOrderProBudget.setError(null);
                }*/


                if (TextUtils.isEmpty(tvCrmOrderProPrice.getText().toString())) {
                    tvCrmOrderProPrice.setError(getString(R.string.error_field_required));
                    isValid = false;
                } else if ("0".equals(tvCrmOrderProPrice.getText().toString())) {
                    tvCrmOrderProPrice.setError(getString(R.string.Zero_Validation));
                    isValid = false;
                }else{
                    tvCrmOrderProPrice.setError(null);
                }

                if(isValid) {
                    // Updated the exist view
                    CrmProducts crmProducts = new CrmProducts();
                    crmProducts.setCrmNumber(crmOrderNumber.getText().toString());
                    crmProducts.setProductCode(selectedProductCode);
                    crmProducts.setProductName(tvCrmOrderProName.getText().toString());
                    crmProducts.setQuantity(tvCrmOrderProQty.getText().toString());
                    crmProducts.setBudget(tvCrmOrderProBudget.getText().toString());
                    crmProducts.setUnitPrice(tvCrmOrderProPrice.getText().toString());
                    crmProducts.setValue(String.valueOf(unformattedValue));

                    crmOrderProdArrayList.add(crmProducts);
                    crmProductSpinnerArrayAdapter.notifyDataSetChanged();
                    crmProductsTable.deleteByCrmNumber(crmOrderNumber.getText().toString());
                    for (CrmProducts crmProduct : crmOrderProdArrayList) {
                        crmProduct.setCrmNumber(crmOrderNumber.getText().toString());
                        crmProductsTable.create(crmProduct);
                    }
                    dialog.dismiss();
                    calculateCrmValues();
                }
            });


        });

        orderSaveButton.setOnClickListener(v -> {
            boolean isValid = true;

            if(crmOrderProdArrayList.size() == 0) {
                Toast.makeText(getApplicationContext(), "Please add atleast one product to save", Toast.LENGTH_LONG).show();
                return;
            }

            if (TextUtils.isEmpty(deliveryDateValue.getText().toString())) {
                deliveryDateValue.setError(getString(R.string.delivery_date_mandatory));
                isValid = false;
            } else{
                deliveryDateValue.setError(null);
            }

            if (TextUtils.isEmpty(add1.getText().toString())) {
                add1.setError(getString(R.string.shipping_address_mandatory));
                isValid = false;
            } else{
                add1.setError(null);
            }

            gs = generalSettingsTable.getSettingByKey("orderSignature");

            if (gs != null && gs.getValue().equals("Yes")) {
                if (TextUtils.isEmpty(customerSignatureName.getText())) {
                    Toast.makeText(getApplicationContext(), "Please Enter Signee's Name", Toast.LENGTH_LONG).show();
                    isValid = false;
                } else if (selectedImage == null) {
                    Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                    isValid = false;
                }

            }


            if(isValid) {
                final Dialog alertbox = new Dialog(v.getRootView().getContext());

                LayoutInflater mInflater = (LayoutInflater)
                        getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                final View alertLayout = mInflater.inflate(R.layout.crm_order_products_save_confirmation, null);

                alertbox.setCancelable(false);
                alertbox.setContentView(alertLayout);

                final Button Yes = alertLayout.findViewById(R.id.crmOrderYesButton);
                final Button No = alertLayout.findViewById(R.id.crmOrderNoButton);

                Yes.setOnClickListener(v1 -> {

                    String visitSequence = sharedPreferences.getString("VisitSequence", "");

                    CrmStatus crmStatusInfo = new CrmStatus();
                    crmStatusInfo.setCustomerCode(customerCode);
                    crmStatusInfo.setCrmNumber(crmOrderNumber.getText().toString());
                    crmStatusInfo.setCrmStatus("Customer Order");
                    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date myDate = new Date();
                    crmStatusInfo.setCrmStatusDate(timeStampFormat.format(myDate));
                    crmStatusTable.create(crmStatusInfo);

                    // Updating CRM No Values
                    crmTable.updateCrmValues(
                            customerCode,
                            crmOrderNumber.getText().toString(),
                            crmProductCount,
                            valueDouble,
                            budgetDouble,
                            "Customer Order",
                            "");

                    order_id = dbHandler.get_order_sequence_value();
                    order_id = order_id + 1;
                    dbHandler.update_order_sequence(order_id);
                    int randomNumberInteger = dbHandler.get_sale_order_id();

                    dbHandler.insertToOrderHeader(
                            Constants.SR_CODE,
                            customerCode,
                            String.valueOf(order_id),
                            crmOrderNumber.getText().toString(),
                            storeCrmOrderDate,
                            "Customer Order",
                            String.valueOf(valueDouble),
                            String.valueOf(crmProductCount),
                            String.valueOf(order_id),
                            "",
                            ""
                    );

                    dbHandler.update_order_header_info (
                            Long.toString(order_id),
                            lpoNumber.getText().toString(),
                            remarks.getText().toString(),
                            storeDeliveryDate,
                            add1.getText().toString(),
                            add2.getText().toString());


                    if (selectedImage != null) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                        byte[] byteArrayImage = baos.toByteArray();
                        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

                        dbHandler.update_order_header(
                                Long.toString(order_id),
                                valueDouble,
                                String.valueOf(crmProductCount),
                                encodedImage
                        );

                    }

                    crmProductsTable.deleteByCrmNumber(crmOrderNumber.getText().toString());

                    int i = 0;
                    for(CrmProducts crmProduct :crmOrderProdArrayList) {
                        crmProduct.setCrmNumber(crmOrderNumber.getText().toString());
                        crmProductsTable.create(crmProduct);

                        double unitPrice = Double.parseDouble(crmProduct.getValue());
                        double value =  Double.parseDouble(crmProduct.getValue());

                        long id2 = dbHandler.insertToOrderDetail(
                                Constants.SR_CODE,
                                customerCode,
                                String.valueOf(order_id),
                                i+1,
                                crmOrderNumber.getText().toString(),
                                storeCrmOrderDate,
                                "Customer Order",
                                crmProduct.getProductCode(),
                                crmProduct.getProductName(),
                                crmProduct.getUom(),
                                crmProduct.getQuantity(),
                                unitPrice,
                                value
                        );

                    }

                    disableViews();
                    isCrmOrdProSaved = true;
                    crmOrderStatus = "Completed";
                    bindAdapter();
                    closeParent = true;
                    Toast.makeText(getApplicationContext(), "Order saved Successfully", Toast.LENGTH_LONG).show();
                    alertbox.dismiss();

                });

                No.setOnClickListener(v12 -> alertbox.dismiss());

                alertbox.show();
            }

        });


        /*
         * Print the order
         */
        printButton.setOnClickListener(v -> {
            if (!crmOrderStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save order to print", Toast.LENGTH_LONG).show();
                return;
            }
            try {
                int totalSize = 48;
                String ESC_NEW_LINE = "\n";
                String horizontalLine = "-----------------------------------------------";
                printMsg = "";
                printMsg += print.centerString(totalSize, companyName);
                printMsg += print.centerString(totalSize, compAdd1);
                printMsg += print.centerString(totalSize, compAdd2);
                printMsg += print.centerString(totalSize, compAdd3);
                printMsg += print.centerString(totalSize, compMblNo);

                printMsg += ESC_NEW_LINE + horizontalLine;
                printMsg += ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;

                printMsg += ESC_NEW_LINE + "Order Number:" + crmOrderNumber.getText().toString();
                printMsg += ESC_NEW_LINE + print.padRight("Date:" + crmOrderDate.getText().toString(), 24) + print.padLeft("TIME:" + crmOrderTime, 24);
                printMsg += "BDE Name:" + sr_name_details;
                printMsg += ESC_NEW_LINE + "Customer Name:" + customerNameString;
                printMsg += ESC_NEW_LINE + "Address:";
                printMsg += print.padRight(customerAddress1, totalSize);
                printMsg += print.padRight(customerAddress2, totalSize);
                printMsg += print.padRight(customerAddress3, totalSize);

                printMsg += ESC_NEW_LINE + horizontalLine;

                printMsg += print.centerString(15, "Product Name") + " " + "  VOL " + print.padLeft("PRICE", 10) + "  " + print.padLeft("VALUE", 14);

                printMsg += ESC_NEW_LINE + horizontalLine;
                int i = 1;
                for (CrmProducts ls : crmOrderProdArrayList) {
                    printMsg += print.padLeft(String.valueOf(i) + ".", 3) + print.padRight(ls.getProductName().substring(0, 11), 12) + " " + print.padLeft(String.valueOf(ls.getQuantity()), 5) + " " + print.padLeft(ls.getUnitPrice(), 10) + "  " + print.padLeft(ls.getValue(), 14);
                    orderTotalValue += Double.parseDouble(ls.getValue());
                    i++;
                }

                printMsg += ESC_NEW_LINE + horizontalLine;
                printMsg += print.padRight("", 15) + print.padRight("Total Value:", 19) + print.padLeft(crmOrderTotalValue.getText().toString(), 14);


                printMsg += ESC_NEW_LINE + horizontalLine;

                printMsg += ESC_NEW_LINE + "LPO Number:" + lpoNumber.getText().toString();
                printMsg += ESC_NEW_LINE + "Shipment Date:" + deliveryDateValue.getText().toString();
                printMsg += ESC_NEW_LINE + "Remarks:" + remarks.getText().toString();
                printMsg += ESC_NEW_LINE + horizontalLine;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE + horizontalLine;
                printMsg += print.padLeft("Customer Sign", totalSize);
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                printMsg += ESC_NEW_LINE;
                print.printContent(printMsg);
                //printBluetooth.printContent(printMsg, selectedImage);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });


        /**
         * Sending SMS Manager API using default SMS manager
         */
        smsButton.setOnClickListener(v -> {
            if (!crmOrderStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save order to send sms", Toast.LENGTH_LONG).show();
                return;
            }
            if (TextUtils.isEmpty(customerPhoneNumber)) {
                Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                return;
            }

            line_count = dbHandler.getOrderLinesCount(Integer.toString(order_id));

            Gson gson = new GsonBuilder().setLenient().create();
            JsonArray ordersJsonArray = gson.toJsonTree(crmOrderProdArrayList).getAsJsonArray();

            smsMessageText ="Order received\n" +
                    "\n" +
                    "Thanks for trusting " + Constants.COMPANY_NAME + " Products." +
                    "\n" +
                    "Please refer your"+
                    "\n" +
                    "Ord No: #" +
                    crmOrderNumber.getText().toString() + "\n" +
                    "Ord Date: " + crmOrderDate.getText().toString() + "\n" +
                    "Order Value: " +"NGN " + crmOrderTotalValue.getText().toString() + " NGN\n" +
                    "Delivery Date: " + deliveryDateValue.getText().toString() + "\n"+
                    "care.nigeria@artemislife.com"+ "\n" ;

            JsonObject jp = new JsonObject();
            jp.addProperty("smsType", emailType);
            jp.addProperty("smsMessageText", smsMessageText);
            jp.addProperty("companyName", companyName);
            jp.addProperty("companyCode", companyCode1);
            jp.addProperty("companyAddLine1", compAdd1);
            jp.addProperty("companyAddLine1", compAdd2);
            jp.addProperty("companyAddLine1", compAdd3);
            jp.addProperty("companyMblNo", compMblNo);
            jp.addProperty("orderNumber", crmOrderNumber.getText().toString());
            jp.addProperty("date", crmOrderDate.getText().toString());
            jp.addProperty("time", crmOrderTime);
            jp.addProperty("bdeName", sr_name_details);
            jp.addProperty("customerName", customerNameString);
            jp.addProperty("customerPhoneNumber", customerPhoneNumber);

            jp.addProperty("address", customerAddress1 + customerAddress2);
            jp.addProperty("totalValue", format_order_value);
            jp.addProperty("lpoNumber", lpoNumber.getText().toString());
            jp.addProperty("shipmentDate", deliveryDateValue.getText().toString());
            jp.addProperty("remarks", remarks.getText().toString());


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.SMS_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);
            Call<ResponseBody> call = api.postingCRMOrderSMSData(jp);
            Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.d("EMAIL SEND", "Success----" + response.body().string());

                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("EMAIL SEND: ", "failure------" + t);
                    Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });

          /*  try {
                smsMessageText = "Emzor - Order received\n" +
                        "\n" +
                        "Order No: #" +
                        crmOrderNumber.getText().toString() + "\n" +
                        "Order Date: " + crmOrderDate.getText().toString() + "\n" +
                        "Order Time: " + crmOrderTime + "\n" +
                        "Order Lines: " + crmOrderProdArrayList.size() + "\n" +
                        "Order Value: " + crmOrderTotalValue.getText().toString() + " NGN\n" +
                        "Delivery Date: " + deliveryDateValue.getText().toString() + "\n";
                Log.d("CRM***", "SMS Test" + smsMessageText);

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(customerPhoneNumber, null, smsMessageText, null, null);
                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Log.d("Sms", "Exception" + e);
            }*/

        });


        /**
         * Sending email using webService
         */
        emailButton.setOnClickListener(v -> {
            if (!crmOrderStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save order to send email", Toast.LENGTH_LONG).show();
                return;
            }
            Gson gson = new GsonBuilder().setLenient().create();
            JsonArray ordersJsonArray = gson.toJsonTree(crmOrderProdArrayList).getAsJsonArray();

            JsonObject jp = new JsonObject();
            jp.addProperty("emailType", emailType);
            jp.addProperty("companyName", companyName);
            jp.addProperty("companyCode", companyCode1);
            jp.addProperty("companyAddLine1", compAdd1);
            jp.addProperty("companyAddLine1", compAdd2);
            jp.addProperty("companyAddLine1", compAdd3);
            jp.addProperty("companyMblNo", compMblNo);
            jp.addProperty("orderNumber", crmOrderNumber.getText().toString());
            jp.addProperty("date", crmOrderDate.getText().toString());
            jp.addProperty("time", crmOrderTime);
            jp.addProperty("bdeName", sr_name_details);
            jp.addProperty("customerName", customerNameString);
            jp.addProperty("customerEmailAddress", customerEmailAddress);

            jp.addProperty("address", customerAddress1 + customerAddress2);
            jp.add("orderLines", ordersJsonArray);
            jp.addProperty("totalValue", format_order_value);
            jp.addProperty("lpoNumber", lpoNumber.getText().toString());
            jp.addProperty("shipmentDate", deliveryDateValue.getText().toString());
            jp.addProperty("remarks", remarks.getText().toString());


            if (selectedImage != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] byteArrayImage = baos.toByteArray();
                encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                jp.addProperty("customerSignature", encodedImage);
            }

            Log.d("CRM***", "Posting Data---- " + gson.toJson(jp));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);
            Call<ResponseBody> call = api.postingEmailData(jp);
            Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.d("EMAIL SEND", "Success----" + response.body().string());

                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("EMAIL SEND: ", "failure------" + t);
                    Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });
        });

        actionbarBackButton.setOnClickListener(v -> {

            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();

            if(closeParent){
                Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
                //homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("customer_id", customerCode);
                bundle.putString("sales_rep_id", login_id);

                homeIntent.putExtras(bundle);

                startActivity(homeIntent);
                closeParent = true;
            }
            else
                finish();
        });

    }

    protected void onStop() {
        if(closeParent)
            setResult(0);
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        if(closeParent)
            setResult(0);
        super.onDestroy();
    }


    public void navigateHomeActivity(){
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        //homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("checkin_time", checkin_time);
        bundle.putString("customer_id", customerCode);
        bundle.putString("crmNo", crmOrderNumberString);
        bundle.putString("sales_rep_id", login_id);
        homeIntent.putExtras(bundle);

        startActivity(homeIntent);
    }

    public void disableViews() {
        orderAddProductButton.setVisibility(View.GONE);
        orderCancelButton.setVisibility(View.GONE);
        orderSaveButton.setVisibility(View.GONE);
        lpoNumber.setEnabled(false);
        captureLpoImage.setEnabled(false);
        remarks.setEnabled(false);
        customerSignatureName.setEnabled(false);
        deliveryDateIcon.setEnabled(false);
        createSignature.setEnabled(false);
        deliveryAddressIcon.setEnabled(false);
    }

    private double calculateProdValue(EditText crmProdQty, EditText crmProdPrice, EditText crmProdValue) {
        String prodQty = crmProdQty.getText().toString();
        String prodPrice = crmProdPrice.getText().toString();
        double unformattedValueDouble = 0;
        if(!prodQty.isEmpty() && !prodPrice.isEmpty() ) {
            int quantityInt = Integer.parseInt(prodQty);
            double priceDouble = Double.parseDouble(prodPrice);

            unformattedValueDouble = quantityInt * priceDouble;
            String formattedTotalValue = valueFormatter.format(unformattedValueDouble);

            crmProdValue.setText(formattedTotalValue);
        }
        return unformattedValueDouble;
    }


    public void calculateCrmValues() {
        crmProductCount = crmOrderProdArrayList.size();
        budgetDouble = 0;
        valueDouble = 0;
        for(CrmProducts crmProduct :crmOrderProdArrayList) {
//            budgetDouble += Double.parseDouble(crmProduct.getBudget());
//            valueDouble += Double.parseDouble(crmProduct.getValue());

            budgetDouble += crmProduct.getBudget() != null && !crmProduct.getBudget().isEmpty() ? Double.parseDouble(crmProduct.getBudget()) : 0.0;
            valueDouble  += crmProduct.getValue() != null && !crmProduct.getValue().isEmpty() ? Double.parseDouble(crmProduct.getValue()) : 0.0;
            Log.d("Crm--", "crmProdValue & Budget:  " +crmProduct.getValue()+" "+crmProduct.getBudget());
        }

        String formattedBudgetValue = valueFormatter.format(budgetDouble);
        String formattedTotalValue = valueFormatter.format(valueDouble);

        crmOrderTotalValue.setText(formattedTotalValue);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_PHOTO1) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    selectedImage = getResizedBitmap(selectedImage, 50);
                    customerImage.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                }
            }

        } else if (requestCode == CAPTURE_PHOTO1) {
            if (resultCode == RESULT_OK) {

                image_id = lpoImagesTable.get_lpo_gallery_image_id();
                image_id = image_id + 1;
                String imagename = "customer_image" + image_id;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                String fileName = "lpo_" + crmOrderNumber.getText().toString() + "_" + image_id + ".png";
                fileUtils.storeImage(photo, fileName, crmOrderImageFolderPath);

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                LPOImages lpoImages = new LPOImages();
                lpoImages.setOrderCode(crmOrderNumber.getText().toString());
                lpoImages.setLpoImageName(fileName);
                lpoImages.setLpoImagePath(crmOrderImageFolderPath + File.separator + fileName);
                lpoImages.setVisitSeqNumber(visitSequence);
                lpoImagesTable.create(lpoImages);

            }
        } else

            printBluetooth.onActivityResult(requestCode, resultCode, data, printMsg);
    }

    private void renderProductInfo() {
        tvCrmOrderProName.setText(productNameString);
        tvCrmOrderProPrice.setText(productPrice);

    }

    void loadCrmProducts() {

        productsArrayList = productTable.getCrmProducts(customerCode);
        spProductName.setTitle("Select Product");
        spProductName.setPositiveButton("OK");
        ArrayList<in.kumanti.emzor.model.Product> newProductList = new ArrayList<>();
        for(in.kumanti.emzor.model.Product product: productsArrayList) {
            boolean isPresent = false;
            for (CrmProducts crmProduct : crmOrderProdArrayList) {
                if(product.product_code.equals(crmProduct.productCode))
                {
                    isPresent = true;
                    break;
                }
            }
            if(!isPresent)
                newProductList.add(product);
        }
        in.kumanti.emzor.model.Product product = new Product();
        product.setProduct_id("-1");
        product.setProduct_name("Select");
        newProductList.add(0, product);

        crmProductSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, newProductList);
        crmProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spProductName.setAdapter(crmProductSpinnerArrayAdapter);
    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void getlocation() {
        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            String lat = Double.toString(latitude);
            String lng = Double.toString(longitude);

            lat_lng = lat + "," + lng;

        } else {

            gpsTracker.showSettingsAlert();
        }
    }

    public boolean validateCustomerSignature() {
            gs = generalSettingsTable.getSettingByKey("orderSignature");

            if (gs != null && gs.getValue().equals("Yes")) {
                if (TextUtils.isEmpty(customerSignatureName.getText())) {
                    Toast.makeText(getApplicationContext(), "Please Enter Signee's Name", Toast.LENGTH_LONG).show();
                    return false;
                } else if (selectedImage == null) {
                    Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                    return false;
                }

            }
            return true;
    }



    private void initializeViews() {
        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        headerDurationContainer = findViewById(R.id.headerDurationContainer);
        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        crmOrderNumber = findViewById(R.id.crmOrderNoTv);
        crmOrderDate = findViewById(R.id.crmOrderDateTv);
        crmOrderCurrency = findViewById(R.id.crmOrderCurrencyTv);

        crmOrderProRv = findViewById(R.id.crmOrderRv);
        crmOrderTotalValue = findViewById(R.id.crmOrderTotalValueTv);

        lpoNumber = findViewById(R.id.crmOrderLpoNoEt);
        captureLpoImage = findViewById(R.id.crmOrderCaptureLpoImageView);
        showLpoImagesGallery = findViewById(R.id.crmOrderShowLpoImagesGallery);

        remarks = findViewById(R.id.crmOrderRemarksEt);
        deliveryDateValue = findViewById(R.id.crmOrderDeliveryDateTv);
        deliveryDateIcon = findViewById(R.id.crmOrderDeliverDateIv);

        createSignature = findViewById(R.id.crmOrderCreateSignIv);
        viewSignature = findViewById(R.id.crmOrderViewSignatureIv);

        customerSignatureName = findViewById(R.id.crmOrderCustSignatureNameEt);
        customerSignatureName.setFilters(new InputFilter[]{filter});

        deliveryAddressIcon = findViewById(R.id.crmOrderShippingAddIv);
        add1 = findViewById(R.id.crmOrderShipAdd1Tv);
        add2 = findViewById(R.id.crmOrderShipAdd2Tv);

        orderAddProductButton = findViewById(R.id.crmOrderAddProdButton);
        orderSaveButton = findViewById(R.id.crmOrderSaveIb);
        orderCancelButton = findViewById(R.id.crmOrderCancelIb);

        printButton = findViewById(R.id.crmOrderPrintButton);
        emailButton = findViewById(R.id.crmOrderEmailButton);
        smsButton = findViewById(R.id.crmOrderSmsButton);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());
        shippingAddressesTable = new CustomerShippingAddressTable(getApplicationContext());
        lpoImagesTable = new LpoImagesTable(getApplicationContext());
        productTable = new ProductTable(getApplicationContext());
        crmTable = new CrmTable(getApplicationContext());
        crmProductsTable = new CrmProductsTable(getApplicationContext());
        crmStatusTable = new CrmStatusTable(getApplicationContext());

        gs = generalSettingsTable.getSettingByKey("enablePrint");
        if (gs != null && gs.getValue().equals("Yes")) {
            printButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableSms");
        if (gs != null && gs.getValue().equals("Yes")) {
            smsButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableEmail");
        if (gs != null && gs.getValue().equals("Yes")) {
            emailButton.setVisibility(View.VISIBLE);
        }
        
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("#,###");
        formatter2 = new DecimalFormat("####");

        print = new Print(this);
        printBluetooth = new PrintBluetooth(this);

        globals = ((Globals) getApplicationContext());
        bdeCode = globals.getLogin_id();

    }

    private void setCrmOrderDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        crmOrderDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        storeCrmOrderDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        crmOrderTime = sdf.format(outDate);

    }

    public void bindAdapter() {
        Log.d("Crm--", "Order crmOrderProdArrayList Size: " + crmOrderProdArrayList.size());
        orderCrmProdAdapter = new CrmOrderProductsAdapter(crmOrderProdArrayList, customerCode, listener, isCrmOrdProSaved);
        crmOrderProRv.setAdapter(orderCrmProdAdapter);
    }


    public void populateHeaderDetails() {

        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            checkin_time = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            crmOrderNumberString = bundle.getString("crmNo");
            login_id = bundle.getString("login_id");
            Log.d("Crm--", "Order Crm Customer Code: " +customerCode+" loginId"+login_id);
        }

        Log.d("Crm--", "Order checkin_time: " +checkin_time);
        Log.d("Crm--", "Order crmOrderNumberString: " +crmOrderNumberString);
        Log.d("Crm--", "Order customer_id: " + customerCode);


        checkInTime.setText(checkin_time);

        //Populate SalesRep Details
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }

        //Populate Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                customerNameString = customerDetails.getString(2);
                customerEmailAddress = customerDetails.getString(33);

                String cityVal = customerDetails.getString(9);
                String stateVal = customerDetails.getString(10);
                if(cityVal != null || stateVal != null) {
                    city.setText(String.format("%s-%s", cityVal, stateVal));
                } else {
                    city.setText("");
                }

                accountNo.setText(customerDetails.getString(1));


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                // ship_address1 = customerDetails.getString(14);
                //ship_address2 = customerDetails.getString(15);
                //ship_address3 = customerDetails.getString(16);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                customerAddress1 = customerDetails.getString(6);
                customerAddress2 = customerDetails.getString(7);
                customerAddress3 = customerDetails.getString(8);
                customerPhoneNumber = customerDetails.getString(11);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/mSale/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }

        shippingAddressesData = shippingAddressesTable.getShippingAddressCompanyId(customerCode);

        if (TextUtils.isEmpty(ship_address1)) {
            Log.d("ShipAddress", "Test" + shippingAddressesData.size());

            if (shippingAddressesData != null && shippingAddressesData.size() != 0) {

                CustomerShippingAddress shippingAddress = shippingAddressesData.get(0);
                Log.d("ShipAddress", "Test" + shippingAddress.getShip_address1());
                add1.setText(shippingAddress.getShip_address1());
                add2.setText(shippingAddress.getShip_address2());
            }


        } else {
            add1.setText(ship_address1);
            add2.setText(ship_address2);
        }

        getCompanyInfo();
        getCurrencyFromSettings();

    }

    public void getCurrencyFromSettings() {
        gs = generalSettingsTable.getSettingByKey("currency");
        if(gs != null) {
            crmOrderCurrency.setText(gs.getValue());
        }
    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);
            }
        }

    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }


    private InputFilter filter = (source, start, end, dest, dstart, dend) -> {

        if (source != null && blockCharacterSet.contains(("" + source))) {
            return "";
        }
        return null;
    };

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    private void createNewSignature() {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);

        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(view -> mSignaturePad.clear());

        mSaveButton.setOnClickListener(view -> {
            Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
            selectedImage = signatureBitmap;
            viewSignature.setImageBitmap(signatureBitmap);
            dialog.dismiss();

            //Posting the customer Signature
            String fileName = "signature_" + crmOrderNumber + "_" + customerCode + ".png";
            fileUtils.storeImage(selectedImage, fileName, customerSignaturesFolderPath);

        });

    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }


    @Override
    public void onBackPressed() {

    }

    private void loadShippingAddress() {

        //ArrayList<ShippingAddress> addressList = (ArrayList<ShippingAddress>)((ShippingPageAdapter)addressViewPager.getAdapter()).shippingAddressesData;

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_delivery_details, null);
        addressViewPager = alertLayout.findViewById(R.id.addressViewPager);

        if (shippingAddressesData.size() == 0) {
            Toast.makeText(getApplicationContext(), "No Shipping address found", Toast.LENGTH_SHORT).show();
            return;
        }

        int currentItem = addressViewPager.getCurrentItem();
        addressViewPager.setAdapter(new ShippingPageAdapter(getApplicationContext(), shippingAddressesData));
        addressViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setPositiveButton("Done", (dialog, which) -> {
            CustomerShippingAddress csa = shippingAddressesData.get(addressViewPager.getCurrentItem());
            String address1 = csa.getShip_address1() != null ? csa.getShip_address1() + ", " : "" + csa.getShip_address2() != null ? csa.getShip_address2() + ", " : "" + csa.getShip_address3() != null ? csa.getShip_address3() + ", " : "";
            String address2 = csa.getShip_city() != null ? csa.getShip_city() + ", " : "" + csa.getShip_country() != null ? csa.getShip_country() + ", " : "" + csa.getShip_state() != null ? csa.getShip_state() + "." : "";
            add1.setText(address1);
            add2.setText(address2);
            add1.setError(null);
        });
        alert.setNegativeButton("Close", (dialog, which) -> {

        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

}


//Creating custom adapter for multiple shipping address

class ShippingPageAdapter extends PagerAdapter {
    int numberOfAddress = 0;
    ArrayList<CustomerShippingAddress> shippingAddressesData;
    private Context mContext;

    ShippingPageAdapter(Context context, ArrayList<CustomerShippingAddress> addressData) {
        mContext = context;
        shippingAddressesData = addressData;
        numberOfAddress = addressData.size();
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        CustomerShippingAddress shippingAddress = shippingAddressesData.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final ViewGroup page = (ViewGroup) inflater.inflate(R.layout.list_shipping_address, container, false);


        final TextView ship_add1 = page.findViewById(R.id.ship_add1);
        final TextView ship_add2 = page.findViewById(R.id.ship_add2);
        final TextView ship_add3 = page.findViewById(R.id.ship_add3);
        final TextView ship_city = page.findViewById(R.id.ship_city);
        final TextView ship_state = page.findViewById(R.id.ship_state);
        final TextView ship_country = page.findViewById(R.id.ship_country);

        ship_add1.setText(shippingAddress.getShip_address1());
        ship_add2.setText(shippingAddress.getShip_address2());
        ship_add3.setText(shippingAddress.getShip_address3());
        ship_city.setText(shippingAddress.getShip_city());
        ship_state.setText(shippingAddress.getShip_state());
        ship_country.setText(shippingAddress.getShip_country());

        Log.d("MYDATA", "Test Instance" + position);
        container.addView(page);
        return page;
    }

    @Override
    public int getCount() {
        return numberOfAddress;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);

    }


    public ArrayList<CustomerShippingAddress> getShippingAddressesDataData() {
        return shippingAddressesData;
    }

}

