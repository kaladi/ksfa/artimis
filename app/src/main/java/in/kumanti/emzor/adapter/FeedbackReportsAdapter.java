package in.kumanti.emzor.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.FeedbackReport;

public class FeedbackReportsAdapter extends RecyclerView.Adapter<FeedbackReportsAdapter.ViewHolder> {

    public ArrayList<FeedbackReport> mDataset;
    public Context context;
    MyDBHandler myDBHandler;
    public RecyclerView feedbackReportsRecyclerView;
    private RecyclerView.LayoutManager feedbackReportsLayoutManager;
    private RecyclerView.Adapter feedbackReportsAdapter;

    public FeedbackReportsAdapter(ArrayList<FeedbackReport> myDataset) {
        mDataset = myDataset;
    }

    public void add(FeedbackReport item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(FeedbackReport item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public FeedbackReportsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_feedback_reports, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.feedbackDate.setText(mDataset.get(position).getFeedback_date());
        viewHolder.feedbackType.setText(mDataset.get(position).getFeedback_type());
        viewHolder.customerName.setText(mDataset.get(position).getCustomer_name());
        viewHolder.customerFeedback.setText(mDataset.get(position).getCustomer_feedback());
        viewHolder.actionTaken.setText(mDataset.get(position).getAction_taken());

        viewHolder.vm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                show_actiontaken(v, position, mDataset.get(position).getFeedback_code());
                //Toast.makeText(context, mDataset.get(position).getFeedback_code(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void show_actiontaken(final View v, final int position, String feedback_code) {

        final Dialog alertbox = new Dialog(v.getRootView().getContext());

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View alertLayout = mInflater.inflate(R.layout.feedback_actiontaken, null);
        alertbox.setCancelable(false);
        alertbox.setContentView(alertLayout);
        myDBHandler = new MyDBHandler(context, null, null, 1);

        feedbackReportsRecyclerView = alertLayout.findViewById(R.id.feedbackReportsList);
        final Button back = alertLayout.findViewById(R.id.back);


        //Populate the Quotation Product details using the Recycler view
        feedbackReportsRecyclerView.setHasFixedSize(true);
        feedbackReportsLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        feedbackReportsRecyclerView.setLayoutManager(feedbackReportsLayoutManager);

        try {
            ArrayList<FeedbackReport> feedbackReportsList = myDBHandler.getActionTakenDetails(feedback_code);
            Log.d("QueryDataList ", "" + feedbackReportsList.size());
            feedbackReportsAdapter = new FeedbackActionTakenAdapter(feedbackReportsList);
            feedbackReportsRecyclerView.setAdapter(feedbackReportsAdapter);
        } catch (Exception e) {
            Log.d("QueryDataResponse", "" + e.getMessage());
        }

        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                alertbox.dismiss();

            }

        });


        alertbox.show();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView feedbackType, feedbackDate, customerName, customerFeedback, actionTaken;
        public View vm;
        public ViewHolder(View v) {
            super(v);

            feedbackDate = v.findViewById(R.id.feedbackDateTextView);
            feedbackType = v.findViewById(R.id.feedbackTypeTextView);
            customerName = v.findViewById(R.id.customerNameTextView);
            customerFeedback = v.findViewById(R.id.customerFeedbackTextView);
            actionTaken = v.findViewById(R.id.actionTakenTextView);

            vm = v;
        }
    }


}