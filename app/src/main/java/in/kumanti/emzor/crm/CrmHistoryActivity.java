package in.kumanti.emzor.crm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.adapter.CRMListAdapter;
import in.kumanti.emzor.eloquent.CrmMeetingsTable;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.utils.FileUtils;

public class CrmHistoryActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    TextView actionbarTitle;

    private TextView marqueeText;
    String  marqueeTextString;
    MyDBHandler dbHandler;

    public RecyclerView crmHistoryListRv;
    RecyclerView.Adapter crmHistoryListAdapter;
    RecyclerView.LayoutManager crmHistoryLayoutManager;

    ArrayList<Crm> crmHistoryArrayList = new ArrayList<>();

    CrmTable crmTable;
    CrmMeetingsTable crmMeetingsTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_crm_history_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        getFooterDateTime();

        actionbarTitle.setText("CRM History");

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        crmHistoryListRv.setHasFixedSize(true);
        crmHistoryLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        crmHistoryListRv.setLayoutManager(crmHistoryLayoutManager);

        bindAdapter();

        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);
        });

    }

    public void bindAdapter() {
        crmHistoryArrayList = crmTable.getCrmHistory();
        crmHistoryListAdapter = new CRMListAdapter(crmHistoryArrayList);
        crmHistoryListRv.setAdapter(crmHistoryListAdapter);
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        crmHistoryListRv = findViewById(R.id.rvCrmInfo);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        crmTable = new CrmTable(getApplicationContext());
        crmMeetingsTable = new CrmMeetingsTable(getApplicationContext());

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

    }

    public void getFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String date = FileUtils.getMonthYear();
        datetime.setText(date);

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            String dateString = FileUtils.getHourMin();
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException ignored) {
                }
            }
        };
        t.start();

    }

}
