package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.CustomerReturnProducts;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_RETURN_PRODUCTS;

public class CustomerReturnProductsTable extends SQLiteOpenHelper {

    //Columns used for Customer Stock Return Products Information details for a selected ordered product
    private static final String COLUMN_CUS_RETURN_PRODUCT_ID = "customerReturnProductId";
    private static final String COLUMN_CUS_RETURN_NUMBER = "customerReturnNumber";
    //private static final String COLUMN_CUS_RETURN_GROUP_CODE = "productGroupCode";
    private static final String COLUMN_CUS_RETURN_PRODUCT_CODE = "productCode";
    private static final String COLUMN_CUS_RETURN_PRODUCT_NAME = "productName";
    private static final String COLUMN_CUS_RETURN_PRODUCT_UOM = "productUom";
    private static final String COLUMN_CUS_RETURN_PRODUCT_UNIT_PRICE = "unitPrice";
    private static final String COLUMN_CUS_RETURN_PRODUCT_AVAIL_QUANTITY = "availQuantity";
    private static final String COLUMN_CUS_RETURN_PRODUCT_RETURN_QUANTITY = "returnQuantity";
    private static final String COLUMN_CUS_RETURN_PRODUCT_RETURN_VALUE = "returnValue";
    private static final String COLUMN_CUS_RETURN_PRODUCT_IS_SYNC = "isSync";
    private static final String COLUMN_CUS_RETURN_PRODUCT_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_CUS_RETURN_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_CUS_RETURN_PRODUCT_UPDATED_AT = "updatedAt";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public CustomerReturnProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_RETURN_PRODUCTS);

        String stockReturnProductTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER_RETURN_PRODUCTS + "(" +
                COLUMN_CUS_RETURN_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUS_RETURN_NUMBER + " TEXT, " +
                COLUMN_CUS_RETURN_PRODUCT_CODE + " TEXT, " +
                COLUMN_CUS_RETURN_PRODUCT_NAME + " TEXT, " +
                COLUMN_CUS_RETURN_PRODUCT_UOM + " TEXT, " +
                COLUMN_CUS_RETURN_PRODUCT_UNIT_PRICE + " NUMERIC, " +
                COLUMN_CUS_RETURN_PRODUCT_AVAIL_QUANTITY + " INTEGER, " +
                COLUMN_CUS_RETURN_PRODUCT_RETURN_QUANTITY + " INTEGER, " +
                COLUMN_CUS_RETURN_PRODUCT_RETURN_VALUE + " NUMERIC, " +
                COLUMN_CUS_RETURN_PRODUCT_IS_SYNC + " TEXT, " +
                COLUMN_CUS_RETURN_PRODUCT_LATEST_SYNC_DATE + " TEXT, " +

                COLUMN_CUS_RETURN_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CUS_RETURN_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(stockReturnProductTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CustomerReturnProducts customerReturnProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUS_RETURN_NUMBER, customerReturnProducts.getCustomerReturnNumber());
        values.put(COLUMN_CUS_RETURN_PRODUCT_CODE, customerReturnProducts.getProductCode());
        values.put(COLUMN_CUS_RETURN_PRODUCT_NAME, customerReturnProducts.getProductName());
        values.put(COLUMN_CUS_RETURN_PRODUCT_UOM, customerReturnProducts.getProductUom());
        values.put(COLUMN_CUS_RETURN_PRODUCT_UNIT_PRICE, customerReturnProducts.getReturnPrice());
        values.put(COLUMN_CUS_RETURN_PRODUCT_AVAIL_QUANTITY, customerReturnProducts.getAvailQuantity());
        values.put(COLUMN_CUS_RETURN_PRODUCT_RETURN_QUANTITY, customerReturnProducts.getReturnQuantity());
        values.put(COLUMN_CUS_RETURN_PRODUCT_RETURN_VALUE, customerReturnProducts.getReturnValue());

        values.put(COLUMN_CUS_RETURN_PRODUCT_IS_SYNC, customerReturnProducts.isSync());
        values.put(COLUMN_CUS_RETURN_PRODUCT_LATEST_SYNC_DATE, customerReturnProducts.getLatestSynDate());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CUSTOMER_RETURN_PRODUCTS, null, values);
        db.close();
        return id;
    }

    public ArrayList<CustomerReturnProducts> getCustomerReturnProductsDetailsForSync() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CUSTOMER_RETURN_PRODUCTS;

        ArrayList<CustomerReturnProducts> customerReturnProductsArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            CustomerReturnProducts customerReturnProducts = new CustomerReturnProducts();
            customerReturnProducts.setTabCode(Constants.TAB_CODE);
            customerReturnProducts.setCompanyCode(Constants.COMPANY_CODE);
            customerReturnProducts.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            customerReturnProducts.setCustomerReturnProductId(id);
            customerReturnProducts.setCustomerReturnNumber(cursor.getString(1));
            customerReturnProducts.setProductCode(cursor.getString(2));
            customerReturnProducts.setProductName(cursor.getString(3));
            customerReturnProducts.setProductUom(cursor.getString(4));
            customerReturnProducts.setReturnPrice(cursor.getString(5));
            customerReturnProducts.setAvailQuantity(cursor.getString(6));
            customerReturnProducts.setReturnQuantity(cursor.getString(7));
            customerReturnProducts.setReturnValue(cursor.getString(8));
            customerReturnProducts.setCreatedAt(cursor.getString(9));
            customerReturnProducts.setUpdatedAt(cursor.getString(10));

            customerReturnProductsArrayList.add(customerReturnProducts);
        }
        cursor.close();
        sqlWrite.close();
        return customerReturnProductsArrayList;
    }

    /*public ArrayList<StockReturnProducts> getStockReturnProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT productCode FROM " + TABLE_CUSTOMER_RETURN_PRODUCTS ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<StockReturnProducts> stockReturnProductsArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            StockReturnProducts op = new StockReturnProducts();
            op.setStockReturnProductId(cursor.getInt(0));
            op.setStockReturnNumber(cursor.getString(1));
            op.setProductCode(cursor.getString(2));
            op.setProductName(cursor.getString(3));
            op.setAvailQuantity(cursor.getString(4));
            op.setReturnQuantity(cursor.getString(5));

            stockReturnProductsArrayList.add(i,op);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return stockReturnProductsArrayList;
    }*/


}
