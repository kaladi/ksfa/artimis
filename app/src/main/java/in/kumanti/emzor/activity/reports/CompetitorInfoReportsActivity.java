package in.kumanti.emzor.activity.reports;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.adapter.MarketInfoReportsAdapter;
import in.kumanti.emzor.eloquent.CompetitorStockProductTable;
import in.kumanti.emzor.eloquent.CustomerStockProductTable;
import in.kumanti.emzor.model.CustomerStockProduct;

public class CompetitorInfoReportsActivity extends MainActivity {

    public RecyclerView marketInfoReportsRecyclerView;
    public String productName, productCode;
    public TextView date, customerName, stockProductName, stockProductAvailability, stockProductVolume;
    String login_id = "";
    ImageView deviceInfo, actionbarBack;
    String checkin_time = "", customer_id = "", marqueeTextString;
    CompetitorStockProductTable competitorStockProductTable;
    CustomerStockProductTable customerStockProductTable;
    ArrayList<CustomerStockProduct> marketInfoReportsArrayList = new ArrayList<CustomerStockProduct>();
    TextView actionbarTitle;
    private RecyclerView.Adapter marketInfoReportsAdapter;
    private RecyclerView.LayoutManager marketInfoReportsLayoutManager;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_competitor_info_reports);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Competitor Info Report");

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);


        //Populate the Quotation Product details using the Recycler view
        marketInfoReportsRecyclerView.setHasFixedSize(true);
        marketInfoReportsRecyclerView.setLayoutManager(marketInfoReportsLayoutManager);


        marketInfoReportsArrayList = customerStockProductTable.getCompetitorInfoHistory();

        marketInfoReportsAdapter = new MarketInfoReportsAdapter(marketInfoReportsArrayList);
        marketInfoReportsRecyclerView.setAdapter(marketInfoReportsAdapter);


        actionbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void initializeViews() {
        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBack = findViewById(R.id.back);


        date = findViewById(R.id.marketReportsDate);
        customerName = findViewById(R.id.marketReportsCustomerName);
        stockProductName = findViewById(R.id.marketReportsStockProductName);
        stockProductAvailability = findViewById(R.id.marketReportsStockAvailability);
        stockProductVolume = findViewById(R.id.marketReportsStockVolume);
        marketInfoReportsRecyclerView = findViewById(R.id.marketInfoReportsList);
        marketInfoReportsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        customerStockProductTable = new CustomerStockProductTable(getApplicationContext());
        competitorStockProductTable = new CompetitorStockProductTable(getApplicationContext());
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            Log.d("LoginId", login_id);
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


}


