package in.kumanti.emzor.model;

import java.util.ArrayList;

public class StockIssueProducts {

    public int stockIssueProductId;
    public int stockIssueId;
    public String stockIssueNumber;
    public String stockIssueDate;
    public String productCode;
    public String productName;
    public String productUom;
    public String unitPrice;
    public String stockQuantity;
    public String issueQuantity;
    public String remarks;
    public String issueValue;
    public String isBatchControlled;
    public String errorStockIssueQty;
    public String errorUnitPrice;
    public String errorProduct;
    public boolean isSync;
    public String createdAt;
    public String updatedAt;
    public ArrayList<StockIssueBatch> stockIssueBatchArrayList;
    private String tabCode;
    private String companyCode;
    private String srCode;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public int getStockIssueProductId() {
        return stockIssueProductId;
    }

    public void setStockIssueProductId(int stockIssueProductId) {
        this.stockIssueProductId = stockIssueProductId;
    }

    public int getStockIssueId() {
        return stockIssueId;
    }

    public void setStockIssueId(int stockIssueId) {
        this.stockIssueId = stockIssueId;
    }

    public String getStockIssueNumber() {
        return stockIssueNumber;
    }

    public void setStockIssueNumber(String stockIssueNumber) {
        this.stockIssueNumber = stockIssueNumber;
    }

    public String getStockIssueDate() {
        return stockIssueDate;
    }

    public void setStockIssueDate(String stockIssueDate) {
        this.stockIssueDate = stockIssueDate;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUom() {
        return productUom;
    }

    public void setProductUom(String productUom) {
        this.productUom = productUom;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getErrorRemarks() {
        return remarks;
    }

    public void setErrorRemarks(String remarks) {
        this.remarks = remarks;
    }


    public String getErrorUnitPrice() {
        return errorUnitPrice;
    }

    public void setErrorUnitPrice(String errorUnitPrice) {
        this.errorUnitPrice = errorUnitPrice;
    }

    public String getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(String stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getIssueQuantity() {
        return issueQuantity;
    }

    public void setIssueQuantity(String issueQuantity) {
        this.issueQuantity = issueQuantity;
    }


    public String getIssueValue() {
        return issueValue;
    }

    public void setIssueValue(String issueValue) {
        this.issueValue = issueValue;
    }

    public String getErrorStockIssueQty() {
        return errorStockIssueQty;
    }

    public void setErrorStockIssueQty(String errorStockIssueQty) {
        this.errorStockIssueQty = errorStockIssueQty;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsBatchControlled() {
        return isBatchControlled;
    }

    public void setIsBatchControlled(String isBatchControlled) {
        this.isBatchControlled = isBatchControlled;
    }

    public String getErrorProduct() {
        return errorProduct;
    }

    public void setErrorProduct(String errorProduct) {
        this.errorProduct = errorProduct;
    }

    public ArrayList<StockIssueBatch> getStockIssueBatchArrayList() {
        return stockIssueBatchArrayList;
    }

    public void setStockIssueBatchArrayList(ArrayList<StockIssueBatch> stockIssueBatchArrayList) {
        this.stockIssueBatchArrayList = stockIssueBatchArrayList;
    }
}
