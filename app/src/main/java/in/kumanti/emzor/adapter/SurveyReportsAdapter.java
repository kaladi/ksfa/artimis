package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.SurveyReport;

public class SurveyReportsAdapter extends RecyclerView.Adapter<SurveyReportsAdapter.ViewHolder> {

    public ArrayList<SurveyReport> mDataset;
    public Context context;

    public SurveyReportsAdapter(ArrayList<SurveyReport> myDataset) {
        mDataset = myDataset;
    }

    public void add(SurveyReport item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(SurveyReport item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public SurveyReportsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_survey_reports, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.surveyName.setText(mDataset.get(position).getSurvey_name());
        viewHolder.customerId.setText(mDataset.get(position).getCustomer_name());
        viewHolder.surveyDate.setText(mDataset.get(position).getDate());
        viewHolder.surveyQuestion.setText(mDataset.get(position).getQuestion());
        viewHolder.customerResponseAnswer.setText(mDataset.get(position).getAnswer());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView surveyName, surveyDate, surveyQuestion, customerResponseAnswer, customerId;

        public ViewHolder(View v) {
            super(v);

            surveyName = v.findViewById(R.id.surveyNameTv);
            customerId = v.findViewById(R.id.surveyCustomerNameTv);
            surveyDate = v.findViewById(R.id.surveyDateTv);
            surveyQuestion = v.findViewById(R.id.surveyQuestionTv);
            customerResponseAnswer = v.findViewById(R.id.surveyResponseAnswerTv);
        }
    }


}