package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.PurchaseHistoryAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.OrderProductSpinner;
import in.kumanti.emzor.model.PurchaseHistoryItems;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.InputFilterMinMax;
import in.kumanti.emzor.utils.SharedPreferenceManager;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class OrderActivity extends MainActivity implements AdapterView.OnItemSelectedListener {

    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionBarTitle, marqueeText, checkInTime;

    String loginId = "", customerCode = "", checkInTimeString = "",tab_code;
    MyDBHandler dbHandler;
    Bundle bundle;

    TextView customerName, accountNo, city;
    String shipAddress1, shipAddress2, shipAddress3, shipCity, shipState, shipCountry, latitude, longitude, latitudeLongitude;

    GPSTracker gpsTracker;
    double latitudeDouble, longitudeDouble;

    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;

    TextView notifyFocusedProdCount, orderNumber, orderDate, productName, productUom, productPrice, productValue, orderTotalProductCount, orderTotalProductValue, ytdQuantity, ytdValue, ytdPrice, ytdOrders, viewStockProdName, viewStockProdUom, schemeProductName, schemeProductDescription;

    int randomNumberInteger = 0, orderId = 0, order_line_id = 0, numRows1 = 0, numbRows = 0, serial_num = 0;

    CustomSearchableSpinner productTypeSpinner, productSpinner;

    ArrayList<OrderProductSpinner> orderProductSpinnerArrayList;
    ArrayAdapter<OrderProductSpinner> orderProductSpinnerArrayAdapter;

    EditText productQuantity;

    String orderLineIdString, productTypeString, productNameString, productUomString, productQuantityString, formattedPriceString, product_id;
    double productPriceDouble = 0.00, productValueDouble = 0.00, unformattedPriceDouble = 0.00;

    DecimalFormat formatter, quantityFormatter;

    String orderNumberString, orderDisplayDate, orderDateDB, orderTime, orderStatus, orderValue, orderProductsCount, orderTypeString = "";

    Button notificationBellButton, addProductButton, viewOrderButton, viewStockButton, schemeDetailButton, competitorInfoButton;

    String transactionType = "Order";

    LinearLayout notificationContainer, viewStockProdNameContainer, viewStockProdUomContainer;
    RelativeLayout schemeProductContainer;

    ArrayList<OrderWarehouseViewStockList> warehouseViewStockListArrayList;
    OrderWarehouseViewStockList warehouseStocks;
    OrderWarehouseViewStockAdapter orderWarehouseViewStockAdapter;

    ListView stockProductsListView, purchasedProductsListView;

    ArrayList<PurchaseHistoryItems> purchaseHistoryItemsArrayList;
    PurchaseHistoryItems purchaseHistoryItems;
    PurchaseHistoryAdapter purchaseHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_order);

        initializeViews();
        setFooterDateTime();

        actionBarTitle.setText("Order");
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        tab_code = Constants.TAB_CODE;
        fetchingBundleData();
        populateHeaderDetails();
        getCustomerLocation();

        generateOrderNumber();
        generateOrderDate();

        Random rnd = new Random();
        int ran_num = 100000 + rnd.nextInt(900000);
        orderNumberString = Integer.toString(ran_num);

        notificationContainer.setVisibility(View.VISIBLE);

        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_grey_scheme);
        productName.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        int linesCountInteger = dbHandler.getOrderLinesCount(Integer.toString(orderId));
        double linesOrderValDouble = dbHandler.getOrderLinesValue(Integer.toString(orderId));

        int focusProdCount = dbHandler.getFocusProductCount(Integer.toString(orderId), customerCode);
        notifyFocusedProdCount.setText(Integer.toString(focusProdCount));

        if (linesCountInteger == 0) {
            orderTotalProductCount.setText("");
            orderTotalProductValue.setText("");
        } else {
            orderTotalProductCount.setText(Integer.toString(linesCountInteger));
            orderTotalProductValue.setText(formatter.format(linesOrderValDouble));
        }

        productTypeSpinner.setOnItemSelectedListener(this);

        loadSpinnerType();

        /*
         * Displays the customer geo location on long press of the customer image.
         */
        customerImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                displaysCustomerLocation();
                return true;
            }
        });

        /*
         * Displays the count of the focused product for the primary customer.
         */
        notificationBellButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                alertbox.setTitle("You have Focus Products to Add");

                alertbox.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertbox.show();

            }

        });

        /*
         * addTextChangedListener for the selected product quantity EditText
         */
        productQuantity.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (productQuantity.getText().toString().equals("")) {

                    productValue.setText("");

                } else {
                    int quan = Integer.parseInt(productQuantity.getText().toString());
                    productValueDouble = Integer.valueOf(quan) * unformattedPriceDouble;

                    String ordervalue_formatted = formatter.format(productValueDouble);
                    productValue.setText(ordervalue_formatted);

                    productQuantityString = Integer.toString(quan);
                    productPriceDouble = unformattedPriceDouble;
                }
            }
        });


        /*
         * Add the selected product for the order of the corresponding customer.
         */
        addProductButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String qty1 = productQuantity.getText().toString();
                int product_count1 = dbHandler.getProductLinesCount(orderId, product_id);

                if (TextUtils.isEmpty(qty1)) {

                    productQuantity.setError(getString(R.string.error_field_required));

                } else if ("0".equals(qty1)) {

                    productQuantity.setError(getString(R.string.Zero_Validation));

                } else if (product_count1 == 1) {

                    Toast.makeText(getApplicationContext(), "Product Already Exist", Toast.LENGTH_LONG).show();

                } else {

                    orderStatus = "Draft";

                    if (orderTypeString.equals("")) {
                        String visitSequence = sharedPreferences.getString("VisitSequence", "");

                        long id1 = dbHandler.insertToOrderHeader(loginId, customerCode, Integer.toString(orderId), orderNumber.getText().toString(), orderDateDB, orderStatus, orderValue, orderProductsCount, String.valueOf(randomNumberInteger), visitSequence, orderTime);
                    }

                    if ((orderTypeString.equals("")) || (orderTypeString.equals("Lines"))) {

                        order_line_id = dbHandler.getOrderLineId(orderId);
                        order_line_id = order_line_id + 1;

                        long id2 = dbHandler.insertToOrderDetail(loginId, customerCode, Integer.toString(orderId), order_line_id, orderNumber.getText().toString(), orderDateDB, orderStatus, product_id, productNameString, productUomString, productQuantityString, productPriceDouble, productValueDouble);

                        if (id2 <= 0) {
                            Toast.makeText(getApplicationContext(), "Order Creation Failed", Toast.LENGTH_LONG).show();
                        } else {
                            // Toast.makeText(getApplicationContext(), "Product is added to cart", Toast.LENGTH_LONG).show();
                        }

                        orderTypeString = "Lines";
                    }

                    int line_count = dbHandler.getOrderLinesCount(Integer.toString(orderId));
                    double order_val = dbHandler.getOrderLinesValue(Integer.toString(orderId));

                    String format_order_value = formatter.format(order_val);
                    productQuantity.setEnabled(false);

                    orderTotalProductCount.setText(Integer.toString(line_count));
                    orderTotalProductValue.setText(format_order_value);
                    orderDate.setText(orderDisplayDate);
                    productName.setText("");
                    productUom.setText("");
                    productPrice.setText("");
                    productValue.setText("");
                    productQuantity.setText("");
                    ytdPrice.setText("0");
                    ytdQuantity.setText("0");
                    ytdOrders.setText("0");
                    ytdValue.setText("0");

                    purchaseHistoryItemsArrayList.clear();
                    purchaseHistoryAdapter.notifyDataSetChanged();

                    Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_grey_scheme);
                    productName.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

                    loadSpinnerType();
                    dbHandler.update_order_header(Integer.toString(orderId), order_val, Integer.toString(line_count));

                }

            }
        });

        /*
         * View the added products of the current order of the customer.
         */
        viewOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int line_count = dbHandler.getOrderLinesCount(Integer.toString(orderId));

                if (line_count == 0) {
                    Toast.makeText(getApplicationContext(), "Add Products to view the order", Toast.LENGTH_LONG).show();

                } else {

                    Intent orderProdIntent = new Intent(getApplicationContext(), OrderProductActivity.class);
                    long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    //duration.stop();

                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkInTimeString);
                    bundle.putString("customer_id", customerCode);
                    bundle.putInt("order_id", orderId);
                    bundle.putString("login_id", loginId);
                    bundle.putString("order_num", orderNumber.getText().toString());

                    orderProdIntent.putExtras(bundle);

                    startActivity(orderProdIntent);
                }

            }
        });

        /*
         * Navigates to the customer landing page if the user click on "Yes" button on pop up screen.
         */
        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog alertDialog = new Dialog(v.getRootView().getContext());

                LayoutInflater mInflater = (LayoutInflater) getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                final View alertLayout = mInflater.inflate(R.layout.activity_exit_order_popup, null);

                alertDialog.setCancelable(false);
                alertDialog.setContentView(alertLayout);

                final Button Yes = alertLayout.findViewById(R.id.Yes);
                final Button No = alertLayout.findViewById(R.id.No);

                Yes.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        if (orderId == 0) {
                            Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);

                            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                            //duration.stop();

                            Bundle bundle = new Bundle();
                            bundle.putString("checkin_time", checkInTimeString);
                            bundle.putString("sales_rep_id", loginId);
                            bundle.putString("customer_id", customerCode);
                            bundle.putString("nav_type", "Home");

                            homeIntent.putExtras(bundle);
                            startActivity(homeIntent);

                        } else {
                            update_order_status("Cancelled");
                            Toast.makeText(getApplicationContext(), "Order Cancelled", Toast.LENGTH_LONG).show();
                        }
                    }

                });

                No.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(final View v) {

                        alertDialog.dismiss();
                    }

                });

                alertDialog.show();

            }
        });

        /*
         * Provides the Device information like the connected device battery level, gprs, gsm, current location of the user as latitude and longtitude co- ordinates, current version of the application, recent master downloaded date, recent posting data time to web and help.
         */
        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        /*
         * Displays the available stock for the particular product.
         */
        viewStockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("Select Product".equals(productNameString)) {
                    Toast.makeText(OrderActivity.this, "Select Product to View Stock", Toast.LENGTH_SHORT).show();

                } else {
                    displayViewStockProductInfo();
                }

            }

        });

        /*
         * Displays the scheme information for the selected product.
         */
        schemeDetailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("Select Product".equals(productNameString)) {
                    Toast.makeText(OrderActivity.this, "Select Product to View Scheme", Toast.LENGTH_SHORT).show();
                } else {
                    displaysSchemeDetail();
                }
            }
        });

        /*
         * Get the competitor products information of the customer used products.
         */
        competitorInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ("Select Product".equals(productNameString)) {
                    Toast.makeText(OrderActivity.this, "Select Product to View Competitor Info", Toast.LENGTH_SHORT).show();

                } else {

                    Intent competitorInfoIntent = new Intent(getApplicationContext(), CompetitorInfoActivity.class);

                    long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                    //duration.stop();

                    Bundle bundle = new Bundle();
                    bundle.putString("checkin_time", checkInTimeString);
                    bundle.putString("customer_id", customerCode);
                    bundle.putDouble("price", unformattedPriceDouble);
                    bundle.putString("login_id", loginId);
                    bundle.putString("product_name", productNameString);
                    bundle.putString("product_id", product_id);
                    bundle.putString("product_uom", productUomString);
                    bundle.putString("order_id", Integer.toString(orderId));
                    bundle.putString("transactionType", transactionType);
                    bundle.putString("transactionDate", orderDateDB);
                    bundle.putString("transactionNumber", orderNumber.getText().toString());


                    competitorInfoIntent.putExtras(bundle);
                    startActivity(competitorInfoIntent);
                }

            }

        });

    }

    /*
     * Displays the customer information and check-in time and its duration value.
     */
    private void populateHeaderDetails() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        checkInTime.setText(checkInTimeString);

        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {

        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                shipAddress1 = customerDetails.getString(15);
                shipAddress2 = customerDetails.getString(16);
                shipAddress3 = customerDetails.getString(17);
                shipCity = customerDetails.getString(18);
                shipState = customerDetails.getString(19);
                shipCountry = customerDetails.getString(20);

            }
        }

    }

    /*
     * Fetch the bundle data from the home activity
     */
    public void fetchingBundleData() {
        bundle = getIntent().getExtras();

        if (bundle != null) {
            checkInTimeString = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            orderId = bundle.getInt("order_id");
            orderLineIdString = bundle.getString("salesline_id");
            loginId = bundle.getString("login_id");
            orderNumberString = bundle.getString("order_num");
        }

    }

    private void update_order_status(String status) {

        dbHandler.update_order_header_status(Integer.toString(orderId), null, null, status, null, 0, null, null, null, null);
        dbHandler.update_order_detail_status(Integer.toString(orderId), status);

        Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);

        Bundle bundle = new Bundle();
        bundle.putString("checkin_time", checkInTimeString);
        bundle.putString("customer_id", customerCode);
        bundle.putString("sales_rep_id", loginId);

        myintent.putExtras(bundle);
        startActivity(myintent);
    }

    /*
     * Displays the values of the product types.
     */
    public void loadSpinnerType() {

        String[] labels = {"All", "Focus", "Scheme"};

        productTypeSpinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, labels));

        productTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productTypeString = adapterView.getItemAtPosition(pos).toString();

                loadSpinnerProduct(productTypeString);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

    }

    @Override
    public void onBackPressed() {

    }


    public void loadSpinnerProduct(String product_type) {

        productSpinner.setTitle("Select Product");
        productSpinner.setPositiveButton("OK");

        if (orderProductSpinnerArrayList == null) {
            orderProductSpinnerArrayList = dbHandler.getOrderProductSpinner(customerCode, orderId, product_type.toLowerCase());

            OrderProductSpinner cs = new OrderProductSpinner();
            cs.setProduct("Select Product");
            cs.setProd_type("-1");
            cs.setProduct_id("-1");
            orderProductSpinnerArrayList.add(0, cs);

            orderProductSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, orderProductSpinnerArrayList);
            orderProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            productSpinner.setAdapter(orderProductSpinnerArrayAdapter);
        } else {
            orderProductSpinnerArrayList.clear();
            orderProductSpinnerArrayList = dbHandler.getOrderProductSpinner(customerCode, orderId, product_type.toLowerCase());

            OrderProductSpinner cs = new OrderProductSpinner();
            cs.setProduct("Select Product");
            cs.setProd_type("-1");
            cs.setProduct_id("-1");
            orderProductSpinnerArrayList.add(0, cs);

            orderProductSpinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, orderProductSpinnerArrayList);
            orderProductSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            productSpinner.setAdapter(orderProductSpinnerArrayAdapter);
        }

        /*
         * Functionality of setOnItemSelectedListener of selected product and its corresponding type.
         */
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;
                productNameString = adapterView.getItemAtPosition(pos).toString();
                OrderProductSpinner cs1 = (OrderProductSpinner) adapterView.getItemAtPosition(pos);
                String product_id1 = cs1.getProduct_id();
                System.out.println("product_id1ordeer = " + product_id1);

                if ("Select Product".equals(productNameString)) {
                    resetfields();

                } else {
                    dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
//                    Cursor productDetails = dbHandler.getOrderProductDetails(productNameString, customerCode);
                    Cursor productDetails = dbHandler.getOrderProductDetails(product_id1, customerCode);
                    numbRows = productDetails.getCount();
                    if (numbRows == 0) {

                    } else {

                        while (productDetails.moveToNext()) {

                            unformattedPriceDouble = productDetails.getDouble(2);
                            formattedPriceString = formatter.format(unformattedPriceDouble);

                            productName.setText(productDetails.getString(1));
                            productUom.setText(productDetails.getString(4));
                            productQuantity.setText("");
                            productValue.setText("");
                            productPrice.setText(formattedPriceString);

                            product_id = productDetails.getString(0);
                            System.out.println("product_iddborder2 = " + product_id);
                            productUomString = productDetails.getString(4);
                            Log.i("product_id", product_id);
                            String prod_type = productDetails.getString(3);

                            if ("Focused".equals(prod_type)) {
                                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_green_scheme);
                                productName.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);


                            } else if ("Scheme".equals(prod_type)) {
                                Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_yellow_scheme);
                                productName.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
                            }


                            Cursor data = dbHandler.getPurchaseHistory(product_id, customerCode);
                            purchaseHistoryItemsArrayList = new ArrayList<PurchaseHistoryItems>();

                            int numRows1 = data.getCount();
                            if (numRows1 == 0) {
                                int j = 0;
                                while (data.moveToNext()) {
                                    String price_val = data.getString(2);
                                    double price_val1 = Double.parseDouble(price_val);
                                    String formatted_price = formatter.format(price_val1);

                                    String inputDateStr = data.getString(0);

                                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                                    Date date = null;
                                    try {
                                        date = inputFormat.parse(inputDateStr);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    String outputDateStr = outputFormat.format(date);

                                    purchaseHistoryItems = new PurchaseHistoryItems(outputDateStr, data.getString(1), formatted_price, data.getString(3), data.getString(3));
                                    purchaseHistoryItemsArrayList.add(j, purchaseHistoryItems);
                                    j++;
                                }

                                purchaseHistoryAdapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItemsArrayList, null);
                                purchasedProductsListView.setAdapter(purchaseHistoryAdapter);

                            } else {

                                int j = 0;
                                while (data.moveToNext()) {
                                    String price_val = data.getString(2);
                                    double price_val1 = Double.parseDouble(price_val);
                                    String formatted_price = formatter.format(price_val1);
                                    String inputDateStr = data.getString(0);

                                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                                    Date date = null;
                                    try {
                                        date = inputFormat.parse(inputDateStr);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    String outputDateStr = outputFormat.format(date);

                                    purchaseHistoryItems = new PurchaseHistoryItems(outputDateStr, data.getString(1), formatted_price, data.getString(3), data.getString(3));
                                    purchaseHistoryItemsArrayList.add(j, purchaseHistoryItems);
                                    j++;
                                }

                                purchaseHistoryAdapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItemsArrayList, null);
                                purchasedProductsListView.setAdapter(purchaseHistoryAdapter);
                            }
                        }

                        productQuantity.setEnabled(true);
                        getYTDValues();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        productNameString = parent.getItemAtPosition(position).toString();
        OrderProductSpinner cs1 = (OrderProductSpinner) parent.getItemAtPosition(position);
        String product_id1 = cs1.getProduct_id();
        System.out.println("product_id1ordeer1 = " + product_id1);
        if ("Select Product".equals(productNameString)) {
            resetfields();

        } else {
            dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
            Cursor productDetails = dbHandler.getOrderProductDetails(product_id1, customerCode);
            numbRows = productDetails.getCount();
            if (numbRows == 0) {
            } else {

                while (productDetails.moveToNext()) {

                    unformattedPriceDouble = productDetails.getDouble(2);
                    formattedPriceString = formatter.format(unformattedPriceDouble);

                    productName.setText(productDetails.getString(1));
                    productUom.setText(productDetails.getString(4));
                    productQuantity.setText("");
                    productValue.setText("");
                    productPrice.setText(formattedPriceString);

                    product_id = productDetails.getString(0);
                    System.out.println("product_iddborder = " + product_id);
                    productUomString = productDetails.getString(4);
                    Log.i("product_id", product_id);
                    String prod_type = productDetails.getString(3);

                    if ("Focused".equals(prod_type)) {
                        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_green_scheme);
                        productName.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);


                    } else if ("Scheme".equals(prod_type)) {
                        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_yellow_scheme);
                        productName.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
                    }


                    Cursor data = dbHandler.getPurchaseHistory(product_id, customerCode);
                    purchaseHistoryItemsArrayList = new ArrayList<PurchaseHistoryItems>();

                    int numRows1 = data.getCount();
                    if (numRows1 == 0) {
                        int j = 0;
                        while (data.moveToNext()) {
                            String price_val = data.getString(2);
                            double price_val1 = Double.parseDouble(price_val);
                            String formatted_price = formatter.format(price_val1);

                            String inputDateStr = data.getString(0);

                            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                            DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                            Date date = null;
                            try {
                                date = inputFormat.parse(inputDateStr);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String outputDateStr = outputFormat.format(date);

                            purchaseHistoryItems = new PurchaseHistoryItems(outputDateStr, data.getString(1), formatted_price, data.getString(3), data.getString(3));
                            purchaseHistoryItemsArrayList.add(j, purchaseHistoryItems);
                            j++;
                        }

                        purchaseHistoryAdapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItemsArrayList, null);
                        purchasedProductsListView.setAdapter(purchaseHistoryAdapter);

                    } else {

                        int j = 0;
                        while (data.moveToNext()) {
                            String price_val = data.getString(2);
                            double price_val1 = Double.parseDouble(price_val);
                            String formatted_price = formatter.format(price_val1);
                            String inputDateStr = data.getString(0);

                            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                            DateFormat outputFormat = new SimpleDateFormat("dd MMM yy");
                            Date date = null;
                            try {
                                date = inputFormat.parse(inputDateStr);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String outputDateStr = outputFormat.format(date);

                            purchaseHistoryItems = new PurchaseHistoryItems(outputDateStr, data.getString(1), formatted_price, data.getString(3), data.getString(3));
                            purchaseHistoryItemsArrayList.add(j, purchaseHistoryItems);
                            j++;
                        }

                        purchaseHistoryAdapter = new PurchaseHistoryAdapter(getApplicationContext(), R.layout.list_purchase_history, purchaseHistoryItemsArrayList, null);
                        purchasedProductsListView.setAdapter(purchaseHistoryAdapter);
                    }
                }

                productQuantity.setEnabled(true);
                getYTDValues();
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    /*
     * Displays the current check-in customer geo location.
     */
    public void displaysCustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    /*
     * Year-to-date (YTD) is a period, starting from the beginning of the current year (either the calendar year) and continuing up to the present day of the selected product.
     */
    private void getYTDValues() {

        int ytdQtyInteger = dbHandler.getYTDOrderProductQuantity(customerCode, product_id);
        double ytdValueDouble = dbHandler.getYTDOrderProductValue(customerCode, product_id);
        int ytdOrderInteger = dbHandler.getYTDOrdersCount(customerCode);
        double ytdPriceDouble = dbHandler.getYTDOrderProductPrice(customerCode, product_id);

        String formattedYTDValue = formatter.format(ytdValueDouble);
        //changed quantity double value into integer value- thaiyal.
        //String formattedYTDQuantity =   formatter.format(ytdQtyInteger);
        String formattedYTDPrice = formatter.format(ytdPriceDouble);

        if (ytdValueDouble == 0) {
            ytdValue.setText("0");
        } else {
            ytdValue.setText(formattedYTDValue);
        }

        if (ytdPriceDouble == 0) {
            ytdPrice.setText("0");
        } else {
            ytdPrice.setText(formattedYTDPrice);
        }
        //changed quantity double value into integer value- thaiyal.
        ytdQuantity.setText(Integer.toString(ytdQtyInteger));
        ytdValue.setText(formattedYTDValue);
        ytdPrice.setText(formattedYTDPrice);
        ytdOrders.setText(Integer.toString(ytdOrderInteger));

    }

    /*
     * Generate the order date and time.
     */
    public void generateOrderDate() {
        String PATTERN = "yyyy-MM-dd";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);

        String date = dateFormat.format(Calendar.getInstance().getTime());
        String PATTERN_DISPLAY = "dd MMM YY";

        SimpleDateFormat dateFormatDisplay = new SimpleDateFormat();
        dateFormatDisplay.applyPattern(PATTERN_DISPLAY);

        orderDisplayDate = dateFormatDisplay.format(Calendar.getInstance().getTime());
        orderDateDB = date;

        orderDate.setText(orderDisplayDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        orderTime = sdf.format(outDate);
    }

    /*
     * Generate the unique order sequence number for the current order based upon the corresponding BDE Code and its Tab code.
     */
    public void generateOrderNumber() {

        if (orderId == 0) {

            randomNumberInteger = dbHandler.get_sale_order_id();
            orderId = dbHandler.get_order_sequence_value();

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
            Date myDate = new Date();
            String order_seq_date = timeStampFormat.format(myDate);

            orderId = orderId + 1;
            dbHandler.update_order_sequence(orderId);

            randomNumberInteger = randomNumberInteger + 1;

            System.out.println("randomNumberInteger = " + randomNumberInteger);
            if (randomNumberInteger <= 9) {
                orderNumber.setText(tab_code + "OR"  + order_seq_date + "00" + randomNumberInteger);

            } else if (randomNumberInteger > 9 & randomNumberInteger < 99) {
                orderNumber.setText(tab_code + "OR"  +  order_seq_date + "0" + randomNumberInteger);

            } else {
                orderNumber.setText(tab_code + "OR"  +  order_seq_date + randomNumberInteger);
            }

        } else {
            orderTypeString = bundle.getString("order_type");
            orderNumber.setText(orderNumberString);
        }
    }

    /*
     * Initialize the widget views of the create order page
     */
    private void initializeViews() {

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        deviceInfo = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        orderNumber = findViewById(R.id.createOrderNumberTv);
        orderDate = findViewById(R.id.createOrderDateTv);

        notificationContainer = findViewById(R.id.notificationContainer);
        notificationBellButton = findViewById(R.id.notificationBellButton);
        notifyFocusedProdCount = findViewById(R.id.focusedProdCountTv);

        productTypeSpinner = findViewById(R.id.createOrderProductTypeSpinner);
        productSpinner = findViewById(R.id.createOrderProductSpinner);

        productName = findViewById(R.id.createOrderProdNameTv);
        productUom = findViewById(R.id.createOrderProdUomTv);
        productQuantity = findViewById(R.id.createOrderProdQuantityEt);

        productPrice = findViewById(R.id.createOrderProdPriceTv);
        productValue = findViewById(R.id.createOrderProdValueTv);

        addProductButton = findViewById(R.id.createOrderAddProdButton);
        viewOrderButton = findViewById(R.id.createOrderViewProdButton);

        orderTotalProductCount = findViewById(R.id.createOrderTotalProdCountTv);
        orderTotalProductValue = findViewById(R.id.createOrderTotalProdValueTv);

        viewStockButton = findViewById(R.id.createOrderViewStockButton);
        schemeDetailButton = findViewById(R.id.createOrderSchemeDetailButton);
        competitorInfoButton = findViewById(R.id.createOrderCompetitorInfoButton);

        ytdQuantity = findViewById(R.id.createOrderYTDQuantityTv);
        ytdValue = findViewById(R.id.createOrderYTDValueTv);
        ytdPrice = findViewById(R.id.createOrderYTDPriceTv);
        ytdOrders = findViewById(R.id.createOrderYTDOrdersTv);

        purchasedProductsListView = findViewById(R.id.createOrderPurchasedProdListView);

        productQuantity.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});

        formatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

    }

    /*
     * Setting the footer date and time of the order page.
     */
    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    /*
     * Displays the scheme information of the selected product.
     */
    public void displaysSchemeDetail() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.scheme_product, null);

        schemeProductContainer = alertLayout.findViewById(R.id.schemeProductContainer);

        schemeProductName = alertLayout.findViewById(R.id.schemeProductNameTv);
        schemeProductDescription = alertLayout.findViewById(R.id.schemeProductDescriptionTv);

        schemeProductName.setText("Product  " + productNameString);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    /*
     * Displays the Stock Product Information for the selected product.
     */
    public void displayViewStockProductInfo() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_create_view_order, null);

        viewStockProdNameContainer = alertLayout.findViewById(R.id.createOrderProductNameContainer);
        viewStockProdUomContainer = alertLayout.findViewById(R.id.createOrderProductUomContainer);

        viewStockProdName = alertLayout.findViewById(R.id.createOrderViewStockProdNameTv);
        viewStockProdUom = alertLayout.findViewById(R.id.createOrderViewStockProdUomTv);

        stockProductsListView = alertLayout.findViewById(R.id.createOrderViewStockProdListView);

        getStockProdListItems();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    /*
     * Get the current location of the check in the primary customer.
     */
    private void getCustomerLocation() {

        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitudeDouble = gpsTracker.getLatitude();
            longitudeDouble = gpsTracker.getLongitude();

            latitude = formatter.format(latitudeDouble);
            longitude = formatter.format(longitudeDouble);

            latitudeLongitude = latitude + "," + longitude;
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void resetfields() {
        productName.setText("");
        productUom.setText("");
        productPrice.setText("");
        productValue.setText("");
        productQuantity.setText("");
        ytdPrice.setText("0");
        ytdQuantity.setText("0");
        ytdOrders.setText("0");
        ytdValue.setText("0");
        if (numbRows == 0) {
        } else {
            purchaseHistoryItemsArrayList.clear();
            purchaseHistoryAdapter.notifyDataSetChanged();
        }

        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.product_grey_scheme);
        productName.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);


    }

    public void getStockProdListItems() {

        warehouseViewStockListArrayList = new ArrayList<OrderWarehouseViewStockList>();

        viewStockProdUom.setText(productUomString);
        viewStockProdName.setText(productNameString);

        Cursor data = dbHandler.getstock_vieworder(loginId, product_id);
        numRows1 = data.getCount();

        if (numRows1 == 0) {
        } else {
            int i = 0;
            while (data.moveToNext()) {
                serial_num = serial_num + 1;

                String onhand_qty = data.getString(0);
                String serialno = Integer.toString(serial_num);

                warehouseStocks = new OrderWarehouseViewStockList(serialno, data.getString(1), quantityFormatter.format(Double.parseDouble(onhand_qty)));
                warehouseViewStockListArrayList.add(i, warehouseStocks);
                i++;

            }

            orderWarehouseViewStockAdapter = new OrderWarehouseViewStockAdapter(getApplicationContext(), R.layout.list_view_stock_order, warehouseViewStockListArrayList);
            stockProductsListView.setAdapter(orderWarehouseViewStockAdapter);
            serial_num = 0;
        }
    }

    public class OrderWarehouseViewStockList {
        private String serial_num;
        private String warehouse_name;
        private String stock_quantity;


        public OrderWarehouseViewStockList(String snum, String wname, String sqty) {
            serial_num = snum;
            warehouse_name = wname;
            stock_quantity = sqty;
        }

        public String getSerial_num() {
            return serial_num;
        }

        public void setSerial_num(String snum) {
            serial_num = snum;
        }

        public String getWarehouse_name() {
            return warehouse_name;
        }

        public void setWarehouse_name(String wname) {
            warehouse_name = wname;
        }

        public String getStock_quantity() {
            return stock_quantity;
        }

        public void setStock_quantity(String sqty) {
            stock_quantity = sqty;
        }

    }

    public class OrderWarehouseViewStockAdapter extends ArrayAdapter<OrderWarehouseViewStockList> {

        Activity mActivity = null;
        AlertDialog alertDialog1;
        Globals globals;
        private LayoutInflater mInflater;
        private ArrayList<OrderWarehouseViewStockList> stockList;
        private int mViewResourceId;
        private Context c;

        public OrderWarehouseViewStockAdapter(Context context, int textViewResourceId, ArrayList<OrderWarehouseViewStockList> stockList) {
            super(context, textViewResourceId, stockList);
            this.stockList = stockList;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;

        }


        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            convertView = mInflater.inflate(mViewResourceId, null);
            final OrderWarehouseViewStockList stockDetail = stockList.get(position);
            if (stockDetail != null) {
                viewHolder = new ViewHolder();
                viewHolder.serial_num = convertView.findViewById(R.id.txt_serial_no);
                viewHolder.warehousename = convertView.findViewById(R.id.txt_wh_name);
                viewHolder.productqty = convertView.findViewById(R.id.txt_stock_qty);

                if (viewHolder.warehousename != null) {
                    viewHolder.warehousename.setText(stockDetail.getWarehouse_name());
                } else {
                    viewHolder.warehousename.setText("");
                }
                if (viewHolder.productqty != null) {
                    viewHolder.productqty.setText((stockDetail.getStock_quantity()));
                } else {
                    viewHolder.productqty.setText("0");
                }
                if (viewHolder.serial_num != null) {
                    viewHolder.serial_num.setText((stockDetail.getSerial_num()));
                } else {
                    viewHolder.serial_num.setText("0");
                }

            }


            return convertView;
        }

        public class ViewHolder {
            TextView serial_num, warehousename, productqty;

        }
    }

}
