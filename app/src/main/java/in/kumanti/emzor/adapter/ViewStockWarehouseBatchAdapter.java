package in.kumanti.emzor.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.SrInvoiceBatch;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.utils.InputFilterMinMax;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;

public class ViewStockWarehouseBatchAdapter extends RecyclerView.Adapter<ViewStockWarehouseBatchAdapter.ViewHolder> {

    public ArrayList<SrInvoiceBatch> mDataset;
    public Context context;
    View alertLayout;
    private RecyclerViewItemListener mListener;
    private int mYear, mMonth, mDay;

    public ViewStockWarehouseBatchAdapter(ArrayList<SrInvoiceBatch> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(SrInvoiceBatch item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(SrInvoiceBatch item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public ViewStockWarehouseBatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_warehouse_batch, parent, false);


        alertLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_receipt_batch_delete_confirmation, parent, false);


        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getExpiryDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.expiryDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }
        viewHolder.batchNumber.setText(mDataset.get(position).getBatchName());
        viewHolder.batchQuantity.setText(mDataset.get(position).getBatchQuantity());




    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView batchNumber, batchQuantity;
        public Button expiryDate;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            expiryDate = v.findViewById(R.id.batchExpiryDateButton);
            batchNumber = v.findViewById(R.id.batchNumberEt);
            batchQuantity = v.findViewById(R.id.batchQuantityEt);
        }
    }


}