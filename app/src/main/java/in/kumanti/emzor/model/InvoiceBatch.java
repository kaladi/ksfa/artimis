package in.kumanti.emzor.model;

public class InvoiceBatch {
    private String batchexpirydate;
    private String batchnumber;
    private int batchquantity;
    private int batchinvoice;

    public InvoiceBatch(String bexpirydate, String bnumber, int bqty, int binvoice) {
        batchexpirydate = bexpirydate;
        batchnumber = bnumber;
        batchquantity = bqty;
        batchinvoice = binvoice;

    }

    public String getBatchexpirydate() {
        return batchexpirydate;
    }

    public void setBatchexpirydate(String bexpirydate) {
        batchexpirydate = bexpirydate;
    }

    public String getBatchnumber() {
        return batchnumber;
    }

    public void setBatchnumber(String bnumber) {
        batchnumber = bnumber;
    }

    public int getBatchquantity() {
        return batchquantity;
    }

    public void setBatchquantity(int bqty) {
        batchquantity = bqty;
    }

    public int getBatchinvoice() {
        return batchinvoice;
    }

    public void setBatchinvoice(int binvoice) {
        batchinvoice = binvoice;
    }

}

