package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM;

public class CrmTable extends SQLiteOpenHelper {

    private static final String COLUMN_CRM_ID = "crmId";
    private static final String COLUMN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_CRM_NUMBER = "crmNo";
    private static final String COLUMN_CRM_DATE = "crmDate";
    private static final String COLUMN_CRM_CURRENCY = "currency";
    private static final String COLUMN_CRM_STATUS = "status";
    private static final String COLUMN_CRM_DESCRIPTION = "description";
    private static final String COLUMN_CRM_REASON = "reason";
    private static final String COLUMN_CRM_LINES = "crmLines";
    private static final String COLUMN_CRM_BUDGET = "crmBudget";
    private static final String COLUMN_CRM_VALUE = "crmValue";
    private static final String COLUMN_CRM_IS_SYNC = "isSync";
    private static final String COLUMN_CRM_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_CRM_RANDOM_NUMBER = "randomNumber";
    private static final String COLUMN_CRM_CREATED_AT = "createdAt";
    private static final String COLUMN_CRM_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        
        
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM);

        String crmTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM + "(" +
                COLUMN_CRM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_CRM_NUMBER + " TEXT, " +
                COLUMN_CRM_DATE + " TEXT, " +
                COLUMN_CRM_CURRENCY + " TEXT, " +
                COLUMN_CRM_STATUS + " TEXT, " +
                COLUMN_CRM_DESCRIPTION + " TEXT, " +
                COLUMN_CRM_REASON + " TEXT, " +
                COLUMN_CRM_LINES + " TEXT, " +
                COLUMN_CRM_BUDGET + " TEXT, " +
                COLUMN_CRM_VALUE + " TEXT, " +
                COLUMN_CRM_IS_SYNC + " TEXT, " +
                COLUMN_CRM_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_CRM_RANDOM_NUMBER + " INTEGER, " +
                COLUMN_CRM_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CRM_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(crmTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(Crm crm) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_CODE, crm.getCustomerCode());
        values.put(COLUMN_CRM_NUMBER, crm.getCrmNumber());
        values.put(COLUMN_CRM_DATE, crm.getCrmDate());
        values.put(COLUMN_CRM_CURRENCY, crm.getCurrency());
        
        values.put(COLUMN_CRM_STATUS, crm.getStatus());
        values.put(COLUMN_CRM_DESCRIPTION, crm.getDescription());
        values.put(COLUMN_CRM_REASON, crm.getReason());

        values.put(COLUMN_CRM_LINES, crm.getCrmLines());
        values.put(COLUMN_CRM_BUDGET, crm.getCrmBudget());
        values.put(COLUMN_CRM_VALUE, crm.getCrmValue());

        values.put(COLUMN_CRM_IS_SYNC, crm.isSync());
        values.put(COLUMN_CRM_LATEST_SYNC_DATE, crm.getLatestSyncDate());
        values.put(COLUMN_CRM_RANDOM_NUMBER, crm.getRandomNumber());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM, null, values);
        db.close();
        return id;
    }


    public int getRandomNumber() {
        int crmId = 0;

        String selectQuery = "select randomNumber from xxmsales_crm where crmDate=strftime('%Y-%m-%d','now', 'localtime') order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            crmId = cursor.getInt(0);
            cursor.close();
        }

        return crmId;
    }

    public ArrayList<Crm> getCRMNumber(String customerCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT DISTINCT crmNo, customerCode, crmId, status, crmValue  FROM " + TABLE_CRM +  " WHERE "+COLUMN_CUSTOMER_CODE+"=? AND status IN ('New Opportunity','Prospect') ORDER BY crmNo DESC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerCode});

        Log.d("Crm--", "Crm Numbers  ");
        ArrayList<Crm> crmArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            Crm crm = new Crm();
            crm.setCrmNumber(cursor.getString(0));
            crm.setCustomerCode(cursor.getString(1));
            crm.setCrmId(cursor.getInt(2));
            crm.setStatus(cursor.getString(3));
            crm.setCrmValue(cursor.getString(4));
            crmArrayList.add(crm);
        }
        cursor.close();
        sqlWrite.close();
        return crmArrayList;
    }

    public Crm getCrmBYNumber(String crmNumber, String customerCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT *  FROM " + TABLE_CRM +  " WHERE "+COLUMN_CRM_NUMBER+"=? AND "+COLUMN_CUSTOMER_CODE+"=? ORDER BY crmNo DESC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{crmNumber,customerCode});

        Log.d("Crm--", "Crm Numbers  ");
        Crm crm = null;
        while (cursor.moveToNext()) {
            crm = new Crm();
            crm.setCrmNumber(cursor.getString(2));
            crm.setCustomerCode(cursor.getString(1));
            crm.setCrmId(cursor.getInt(0));
        }
        cursor.close();
        sqlWrite.close();
        return crm;
    }

    public void updateCrmValues(String customerCode, String crmNumber, int crmLines, double crmValue, double crmBudget, String crmStatus, String reason) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CRM_LINES, crmLines);
        cv.put(COLUMN_CRM_VALUE, crmValue);
        cv.put(COLUMN_CRM_BUDGET, crmBudget);
        cv.put(COLUMN_CRM_STATUS, crmStatus);
        cv.put(COLUMN_CRM_REASON, reason);

        sqlWrite.update(TABLE_CRM, cv, COLUMN_CUSTOMER_CODE + " =? AND " + COLUMN_CRM_NUMBER + "=?", new String [] {customerCode, crmNumber});
        sqlWrite.close();
    }


    public ArrayList<Crm> getCrmForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CRM + " WHERE DATE(datetime(updatedAt,'unixepoch')) >= date('now','-5 Days')";

        ArrayList<Crm> crmArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            Crm crm = new Crm();
            crm.setTabCode(Constants.TAB_CODE);
            crm.setCompanyCode(Constants.COMPANY_CODE);
            crm.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            crm.setCrmId(id);
            crm.setCustomerCode(cursor.getString(1));
            crm.setCrmNumber(cursor.getString(2));

            crm.setCrmDate(cursor.getString(3));
            crm.setCurrency(cursor.getString(4));
            crm.setStatus(cursor.getString(5));
            crm.setDescription(cursor.getString(6));
            crm.setReason(cursor.getString(7));

            crm.setCrmLines(cursor.getString(8));
            crm.setCrmBudget(cursor.getString(9));
            crm.setCrmValue(cursor.getString(10));


            crm.setCreatedAt(cursor.getString(13));
            crm.setUpdatedAt(cursor.getString(14));

            crmArrayList.add(crm);
        }
        cursor.close();
        sqlWrite.close();
        return crmArrayList;
    }


    public ArrayList<Crm> getCrmHistory() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
       // String selectQuery = " SELECT crmDate, crmNo, customerCode, crmLines, crmBudget, crmValue, status, description FROM " + TABLE_CRM + " ORDER BY createdAt";

        String selectQuery = "select crm.crmDate, crm.crmNo, cus.customer_name, crm.crmLines, crm.crmBudget, crm.crmValue, crm.status, crm.description, crm.reason from xxmsales_crm as crm left join xxmsales_customer_details as cus on crm.customerCode = cus.customer_id order by createdAt desc";
        ArrayList<Crm> crmArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            Crm crm = new Crm();
            crm.setCrmDate(cursor.getString(0));
            crm.setCrmNumber(cursor.getString(1));
            crm.setCustomerCode(cursor.getString(2));
            crm.setCrmLines(cursor.getString(3));
            crm.setCrmBudget(cursor.getString(4));
            crm.setCrmValue(cursor.getString(5));
            crm.setStatus(cursor.getString(6));
            crm.setDescription(cursor.getString(7));
            crm.setReason(cursor.getString(8));
            crmArrayList.add(crm);
        }
        cursor.close();
        sqlWrite.close();
        return crmArrayList;
    }

}
