package in.kumanti.emzor.crm;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.HomeActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.activity.OrderGalleryActivity;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.ViewCrmProductsAdapter;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.CrmProductsTable;
import in.kumanti.emzor.eloquent.CrmStatusTable;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.model.CrmProducts;
import in.kumanti.emzor.model.CrmStatus;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewCrmActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionbarTitle, marqueeText, customerName, accountNo, city, checkInTime;
    Chronometer duration;

    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1, customerType;
    String customerAddress1, customerAddress2, customerAddress3;
    String customerPhoneNumber = "";
    String smsMessageText = "";
    String crm_number = "", crmStatusSpString = "", descriptionString = "", marqueeTextString;

    Bitmap selectedImage = null,selectedImage1 = null;
    int image_id = 0,image_id1 = 0, randomNumber = 0, crmProductCount = 0;
    double budgetDouble = 0.0, valueDouble = 0.0;
    boolean closeParent = false;
    private boolean isCrmProSaved = false;
    String loginId = "",crmNumberString = "", crmStatusString="", crmStatusDateString= "", checkInTimeString = "", customerCode = "", crmDropString = "";

    String customerNameString, customerEmailAddress;
    SharedPreferenceManager sharedPreferenceManager;
    MyDBHandler dbHandler;
    LpoImagesTable lpoImagesTable;
    RecyclerViewItemListener listener;
    ImageButton crmSaveButton, crmCancelButton;
    Button detailsButton, crmAddProdButton, statusHistoryButton, attachEnquiryButton, attachQuotationButton, enterOrderButton, emailButton, smsButton;

    TextView crmNo, currency, crmDate, crmStatus, crmStatusDate, totalBudgetValue, totalProdValue;

    EditText reasonForDrop;
    LinearLayout reasonForDropContainer;
    CustomSearchableSpinner crmStatusSpinner;

    CrmTable crmTable;
    CrmProductsTable crmProductsTable;
    CrmStatusTable crmStatusTable;

    ArrayList<CrmProducts> viewCrmProdArrayList = new ArrayList<>();
    RecyclerView viewCrmProdRecyclerView;
    RecyclerView.Adapter<ViewCrmProductsAdapter.ViewHolder> viewCrmProdAdapter;
    RecyclerView.LayoutManager viewCrmProdLayoutManager;

    String storeCrmDate, crmSavedStatus = "", crmTime;
    DecimalFormat valueFormatter, quantityFormatter;

    private static final int SELECT_ENQUIRY = 1;
    private static final int CAPTURE_ENQUIRY = 2;
    private static final int SELECT_QUOTATION = 3;
    private static final int CAPTURE_QUOTATION = 4;

    GeneralSettings gs;
    GeneralSettingsTable generalSettingsTable;
    Globals globals;

    boolean isQuotationCap = false;

    String emailType = "Crm";
    String companyName, companyCode1,compAdd1, compAdd2, compAdd3, compMblNo;
    String sr_name_details, sr_rm_name, sr_region, sr_warehouse;

    GPSTracker gpsTracker;
    double latitudeDouble, longitudeDouble;
    String latitude, longitude, latitudeLongitude;
    DecimalFormat formatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_view_crm);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();
        setCrmDate();
//        getCustomerLocation();

        actionbarTitle.setText(R.string.view_crm_header);

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        listener = (view, position) -> {
            calculateCrmValues();
        };

        crmStatus.setText(crmStatusString);
        crmStatusDate.setText(crmStatusDateString);

        viewCrmProdRecyclerView.setHasFixedSize(true);
        viewCrmProdLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        viewCrmProdRecyclerView.setLayoutManager(viewCrmProdLayoutManager);

        bindAdapter();
        calculateCrmValues();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        crmAddProdButton.setOnClickListener(v -> {
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();
            Intent intent = new Intent();
            intent.putExtra("crmProdList", viewCrmProdArrayList);
            setResult(RESULT_OK, intent);
            finish();
        });

        statusHistoryButton.setOnClickListener(v -> {
            Intent statusHisIntent = new Intent(getApplicationContext(), StatusHistoryActivity.class);

            Bundle bundle = new Bundle();
            bundle.putString("customer_id", customerCode);
            bundle.putString("crmNo", crmNumberString);
            bundle.putString("crmDate", crmDate.getText().toString());
            bundle.putString("crmStatus", crmStatusString);
            bundle.putString("crmStatusDate", crmStatusDateString);
            statusHisIntent.putExtras(bundle);

            startActivity(statusHisIntent);
        });

        enterOrderButton.setOnClickListener(v -> {

            Log.d("Crm--", "customerType " +customerType);

            if(customerType != null && customerType.equals("Primary")) {
                Crm isExistingCRM = crmTable.getCrmBYNumber(crmNo.getText().toString(),customerCode);
                if(isExistingCRM == null) {
                    Toast.makeText(ViewCrmActivity.this, "Please save CRM Products", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent orderIntent = new Intent(getApplicationContext(), CrmOrderActivity.class);

                long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                //duration.stop();

                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkInTimeString);
                bundle.putString("customer_id", customerCode);
                bundle.putString("login_id", loginId);
                bundle.putString("crmNo", crmNo.getText().toString());

                orderIntent.putExtras(bundle);
                startActivityForResult(orderIntent,99);
            } else {
                Toast.makeText(ViewCrmActivity.this, "Customer is not approved for Order", Toast.LENGTH_SHORT).show();
            }



        });


        crmCancelButton.setOnClickListener(v -> {
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();
            navigateHomeActivity();
        });

       actionbarBackButton.setOnClickListener(v -> {

           long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
           sharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
           duration.stop();

           if(closeParent){
               navigateHomeActivity();
               closeParent = true;
           }
           else
               finish();
       });

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(ViewCrmActivity.this.getApplicationContext(), DeviceInfoActivity.class);
            ViewCrmActivity.this.startActivity(intent);

        });

        attachEnquiryButton.setOnClickListener(v -> {
            LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.dialog_attach_enquiry, null);
            ImageView ivCaptureEnquiryImage, ivViewEnquiryImage;

            ivCaptureEnquiryImage = alertLayout.findViewById(R.id.ivCaptureEnquiryImage);
            ivViewEnquiryImage = alertLayout.findViewById(R.id.ivViewEnquiryImage);

            ivCaptureEnquiryImage.setOnClickListener(v1 -> {
                Intent enquiryCapIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(enquiryCapIntent, CAPTURE_ENQUIRY);
            });

            ivViewEnquiryImage.setOnClickListener(v12 -> {
                int lpo_image_count = lpoImagesTable.get_lpo_order_image_count(crm_number);

                if (lpo_image_count == 0) {
                    Toast.makeText(ViewCrmActivity.this, "No Images,Capture and Check again.", Toast.LENGTH_SHORT).show();
                } else {
                    Intent myintent = new Intent(getApplicationContext(), OrderGalleryActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("customer_id", customerCode);
                    bundle.putString("login_id", loginId);
                    bundle.putString("order_number", crm_number);
                    bundle.putString("imageType", "Enquiry");

                    myintent.putExtras(bundle);
                    startActivity(myintent);
                }


            });

            android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
            alert.setView(alertLayout);
            alert.setNegativeButton("Close", (dialog, which) -> {
            });

            alert.setCancelable(true);

            android.app.AlertDialog dialog = alert.create();
            dialog.show();
        });

        attachQuotationButton.setOnClickListener(v -> {
            LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.dialog_attach_quotation, null);
            ImageView ivCaptureQuotationImage, ivViewQuotationImage;

            ivCaptureQuotationImage = alertLayout.findViewById(R.id.ivCaptureQuotationImage);
            ivViewQuotationImage = alertLayout.findViewById(R.id.ivViewQuotationImage);

            ivCaptureQuotationImage.setOnClickListener(v13 -> {
                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent1, CAPTURE_QUOTATION);
            });

            ivViewQuotationImage.setOnClickListener(v14 -> {
                int lpo_image_count = lpoImagesTable.get_lpo_order_image_count(crm_number);

                if (lpo_image_count == 0) {
                    Toast.makeText(ViewCrmActivity.this, "No Images,Capture and Check again.", Toast.LENGTH_SHORT).show();
                } else {
                    Intent myintent = new Intent(getApplicationContext(), OrderGalleryActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("customer_id", customerCode);
                    bundle.putString("login_id", loginId);
                    bundle.putString("order_number", crm_number);
                    bundle.putString("imageType", "Quotation");

                    myintent.putExtras(bundle);
                    startActivity(myintent);
                }


            });

            android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());

            alert.setView(alertLayout);
            alert.setNegativeButton("Close", (dialog, which) -> {
            });

            alert.setCancelable(true);

            android.app.AlertDialog dialog = alert.create();
            dialog.show();
        });


        crmSaveButton.setOnClickListener(v -> {

            if(crmStatusString.equals("Dropped")||crmStatusString.equals("Lost")) {
                if (TextUtils.isEmpty(reasonForDrop.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Please Enter Reason", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            if(viewCrmProdArrayList.size() == 0) {
                Toast.makeText(getApplicationContext(), "Please add atleast one product to save", Toast.LENGTH_LONG).show();
                return;
            }


            final Dialog alertbox = new Dialog(v.getRootView().getContext());

            LayoutInflater mInflater = (LayoutInflater)
                    getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            final View alertLayout = mInflater.inflate(R.layout.crm_products_save_confirmation, null);

            TextView crmProdSaveConfirmText= alertLayout.findViewById(R.id.crmProdSaveConfirmTv);
            
            if(isQuotationCap) {
                crmProdSaveConfirmText.setText("Submit Quotation?");
            }

            alertbox.setCancelable(false);
            alertbox.setContentView(alertLayout);

            final Button Yes = alertLayout.findViewById(R.id.crmYesButton);
            final Button No = alertLayout.findViewById(R.id.crmNoButton);

            Yes.setOnClickListener(v1 -> {

                if(isQuotationCap) {
                    crmStatusString = "Prospect";
                    crmStatus.setText(crmStatusString);
                }

                String visitSequence = sharedPreferences.getString("VisitSequence", "");
                Crm isExistingCRM = crmTable.getCrmBYNumber(crmNo.getText().toString(),customerCode);
                if(isExistingCRM == null) {
                    createCrmStatus();

                    Crm crm = new Crm();
                    crm.setCrmNumber(crmNo.getText().toString());
                    crm.setRandomNumber(randomNumber);
                    crm.setCrmDate(storeCrmDate);
                    crm.setCustomerCode(customerCode);
                    crm.setCurrency(currency.getText().toString());
                    crm.setStatus(crmStatusString);
                    calculateCrmValues();
                    crm.setCrmLines(String.valueOf(crmProductCount));
                    crm.setCrmBudget(String.valueOf(budgetDouble));
                    crm.setCrmValue(String.valueOf(valueDouble));
                    crm.setDescription(descriptionString);

                    Log.d("Crm---", "new CRM crmStatusString:  " +crmStatusString);
                    crmTable.create(crm);
                } else {
                    // Updating CRM No Values
                    createCrmStatus();
                    Log.d("Crm---", "isExistingCRM crmStatusString:  " +crmStatusString);

                    crmDropString = reasonForDrop.getText().toString();
                    crmTable.updateCrmValues(customerCode, crmNo.getText().toString(), crmProductCount, valueDouble, budgetDouble, crmStatusString, crmDropString);
                }

                crmProductsTable.deleteByCrmNumber(crmNo.getText().toString());

                for(CrmProducts crmProduct :viewCrmProdArrayList) {
                    crmProduct.setCrmNumber(crmNo.getText().toString());
                    crmProductsTable.create(crmProduct);
                }

                disableViews();

                Toast.makeText(getApplicationContext(), "Crm Products saved Successfully", Toast.LENGTH_LONG).show();
                alertbox.dismiss();
                closeParent = true;
                if(crmStatusString.equals("Dropped")||crmStatusString.equals("Lost")) {
                    navigateHomeActivity();
                }

                isCrmProSaved = true;
                crmSavedStatus = "Completed";
                bindAdapter();
            });

            No.setOnClickListener(v12 -> alertbox.dismiss());
            alertbox.show();

        });


        /**
         * Sending SMS Manager API using default SMS manager
         */
        smsButton.setOnClickListener(v -> {
            if (!crmSavedStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save crm to send sms", Toast.LENGTH_LONG).show();
                return;
            }
            if (TextUtils.isEmpty(customerPhoneNumber)) {
                Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                return;
            }

            Gson gson = new GsonBuilder().setLenient().create();
            JsonArray crmProductsJsonArray = gson.toJsonTree(viewCrmProdArrayList).getAsJsonArray();

            smsMessageText = "CRM \n" +
                    "\n" +
                    "Thanks for trusting " + Constants.COMPANY_NAME + " Products." +
                    "\n" +
                    "Please refer your"+
                    "\n" +
                    "Crm No: #" +
                    crmNo.getText().toString() + "\n" +
                    "Crm Date: " + crmDate.getText().toString() + "\n" +
                    "Total Product Value: " +"NGN " +  totalProdValue.getText().toString() + " NGN\n" +
                    "Total Budget Value: " + "NGN " + totalBudgetValue.getText().toString() + "\n"+
                    "care.nigeria@artemislife.com"+ "\n" ;

            JsonObject jp = new JsonObject();
            jp.addProperty("smsType", emailType);
            jp.addProperty("smsMessageText", smsMessageText);
            jp.addProperty("companyName", companyName);
            jp.addProperty("companyCode", companyCode1);
            jp.addProperty("companyAddLine1", compAdd1);
            jp.addProperty("companyAddLine1", compAdd2);
            jp.addProperty("companyAddLine1", compAdd3);
            jp.addProperty("companyMblNo", compMblNo);
            jp.addProperty("orderNumber", crmNo.getText().toString());
            jp.addProperty("date", crmDate.getText().toString());
            jp.addProperty("time", crmTime);
            jp.addProperty("bdeName", sr_name_details);
            jp.addProperty("customerName", customerNameString);
            jp.addProperty("customerPhoneNumber", customerPhoneNumber);

            jp.addProperty("address", customerAddress1 + customerAddress2);
            jp.addProperty("totalProductValue", totalProdValue.getText().toString());
            jp.addProperty("totalBudgetValue", totalBudgetValue.getText().toString());
            jp.addProperty("description", descriptionString);

            Log.d("CRM***", "Email Posting Data---- " + gson.toJson(jp));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.SMS_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);
            Call<ResponseBody> call = api.postingCRMSMSData(jp);
            Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.d("EMAIL SEND", "Success----" + response.body().string());

                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("EMAIL SEND: ", "failure------" + t);
                    Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });


          /*  try {
                smsMessageText = "Emzor - CRM \n" +
                        "\n" +
                        "Crm No: #" +
                        crmNo.getText().toString() + "\n" +
                        "Crm Date: " + crmDate.getText().toString() + "\n" +
                        "Crm Time: " + crmTime + "\n" +
                        "Crm Lines: " + viewCrmProdArrayList.size() + "\n" +
                        "Total Product Value: " + totalProdValue.getText().toString() + " NGN\n" +
                        "Total Budget Value: " + totalBudgetValue.getText().toString() + "\n";

                Log.d("CRM***", "SMS Test" + smsMessageText);

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(customerPhoneNumber, null, smsMessageText, null, null);
                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Log.d("Sms", "Exception" + e);
            }*/

        });

        /*
         * Sending email using webService
         */
        emailButton.setOnClickListener(v -> {
            if (!crmSavedStatus.equals("Completed")) {
                Toast.makeText(getApplicationContext(), "Please save crm to send email", Toast.LENGTH_LONG).show();
                return;
            }
            Gson gson = new GsonBuilder().setLenient().create();
            JsonArray crmProductsJsonArray = gson.toJsonTree(viewCrmProdArrayList).getAsJsonArray();

            JsonObject jp = new JsonObject();
            jp.addProperty("emailType", emailType);
            jp.addProperty("companyName", companyName);
            jp.addProperty("companyCode", companyCode1);
            jp.addProperty("companyAddLine1", compAdd1);
            jp.addProperty("companyAddLine1", compAdd2);
            jp.addProperty("companyAddLine1", compAdd3);
            jp.addProperty("companyMblNo", compMblNo);
            jp.addProperty("orderNumber", crmNo.getText().toString());
            jp.addProperty("date", crmDate.getText().toString());
            jp.addProperty("time", crmTime);
            jp.addProperty("bdeName", sr_name_details);
            jp.addProperty("customerName", customerNameString);
            jp.addProperty("customerEmailAddress", customerEmailAddress);

            jp.addProperty("address", customerAddress1 + customerAddress2);
            jp.add("crmLines", crmProductsJsonArray);
            jp.addProperty("totalProductValue", totalProdValue.getText().toString());
            jp.addProperty("totalBudgetValue", totalBudgetValue.getText().toString());
            jp.addProperty("description", descriptionString);

            Log.d("CRM***", "Email Posting Data---- " + gson.toJson(jp));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);
            Call<ResponseBody> call = api.postingEmailData(jp);
            Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();


            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.d("EMAIL SEND", "Success----" + response.body().string());

                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    Log.i("EMAIL SEND: ", "failure------" + t);
                    Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });
        });

    }

    private void getCustomerLocation() {

        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitudeDouble = gpsTracker.getLatitude();
            longitudeDouble = gpsTracker.getLongitude();

            latitude = formatter.format(latitudeDouble);
            longitude = formatter.format(longitudeDouble);

            latitudeLongitude = latitude + "," + longitude;
        } else {
            gpsTracker.showSettingsAlert();
        }
    }


    public void createCrmStatus() {
        CrmStatus crmStatusInfo = new CrmStatus();
        crmStatusInfo.setCustomerCode(customerCode);
        crmStatusInfo.setCrmNumber(crmNumberString);
        crmStatusInfo.setCrmStatus(crmStatusString);
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        crmStatusInfo.setCrmStatusDate(timeStampFormat.format(myDate));
        crmStatusTable.create(crmStatusInfo);
    }

    public void navigateHomeActivity(){
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        //homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("checkin_time", checkInTimeString);
        bundle.putString("customer_id", customerCode);
        bundle.putString("crmNo", crmNumberString);
        bundle.putString("sales_rep_id", loginId);
        homeIntent.putExtras(bundle);

        startActivity(homeIntent);
    }

    protected void onStop() {
        Log.d("ResultReceived","Restsend"+crmStatusString+" bb"+closeParent);
        if(crmStatusString.equals("Dropped")||crmStatusString.equals("Lost") || closeParent)
            setResult(RESULT_CANCELED);
        else
            setResult(RESULT_OK);
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        Log.d("ResultReceived","Restsend"+crmStatusString+" bb"+closeParent);
        if(crmStatusString.equals("Dropped")||crmStatusString.equals("Lost") || closeParent)
            setResult(RESULT_CANCELED);
        else
            setResult(RESULT_OK);
        super.onDestroy();
    }

    public void disableViews() {
        crmSaveButton.setVisibility(View.GONE);
        crmCancelButton.setVisibility(View.GONE);
        crmAddProdButton.setVisibility(View.GONE);
        attachEnquiryButton.setEnabled(false);
        attachQuotationButton.setEnabled(false);
        enterOrderButton.setEnabled(false);
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        crmNo = findViewById(R.id.viewCrmNoTv);
        currency = findViewById(R.id.viewCrmCurrencyTv);
        crmDate = findViewById(R.id.viewCrmDateTv);

        crmStatus = findViewById(R.id.viewCrmStatusTv);
        crmStatusDate = findViewById(R.id.viewCrmStatusDateTv);
        crmStatusSpinner = findViewById(R.id.viewCrmStatusSp);

        reasonForDropContainer = findViewById(R.id.viewCrmReasonContainer);
        reasonForDrop = findViewById(R.id.viewCrmReasonEt);

        totalBudgetValue = findViewById(R.id.viewCrmTotalBudgetValueTv);
        totalProdValue = findViewById(R.id.viewCrmTotalProdValueTv);

        crmAddProdButton = findViewById(R.id.viewCrmAddProductBt);
        statusHistoryButton = findViewById(R.id.viewCrmStatusHistoryBt);
        attachEnquiryButton = findViewById(R.id.viewCrmAttachEnquiryBt);
        attachQuotationButton = findViewById(R.id.viewCrmAttachQuotationBt);
        enterOrderButton = findViewById(R.id.viewCrmEnterOrderBt);

        crmSaveButton = findViewById(R.id.viewCrmSaveIb);
        crmCancelButton = findViewById(R.id.viewCrmCancelIb);
        emailButton = findViewById(R.id.viewCrmEmailButton);
        smsButton = findViewById(R.id.viewCrmSmsButton);

        viewCrmProdRecyclerView = findViewById(R.id.viewCrmProdsRv);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        crmTable = new CrmTable(getApplicationContext());
        crmProductsTable = new CrmProductsTable(getApplicationContext());
        crmStatusTable = new CrmStatusTable(getApplicationContext());
        lpoImagesTable = new LpoImagesTable(getApplicationContext());
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());

        globals = ((Globals) getApplicationContext());
        customerType = globals.getCustomer_type();

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        valueFormatter = new DecimalFormat("#,###.00");
        quantityFormatter = new DecimalFormat("#,###");
    }

    public void bindAdapter() {
        viewCrmProdAdapter = new ViewCrmProductsAdapter(viewCrmProdArrayList, customerCode, listener, isCrmProSaved);
        viewCrmProdRecyclerView.setAdapter(viewCrmProdAdapter);
    }

    public void calculateCrmValues() {
        crmProductCount = viewCrmProdArrayList.size();
        budgetDouble = 0;
        valueDouble = 0;
        for(CrmProducts crmProduct :viewCrmProdArrayList) {

            budgetDouble += crmProduct.getBudget() != null && !crmProduct.getBudget().isEmpty() ? Double.parseDouble(crmProduct.getBudget()) : 0.0;
            valueDouble  += crmProduct.getValue() != null && !crmProduct.getValue().isEmpty() ? Double.parseDouble(crmProduct.getValue()) : 0.0;
//            budgetDouble += Double.parseDouble(crmProduct.getBudget());
//            valueDouble += Double.parseDouble(crmProduct.getValue());
//            Log.d("Crm--", "crmProdValue & Budget:  " +crmProduct.getValue()+" "+crmProduct.getBudget());
        }

        String formattedTotalBudgetValue = valueFormatter.format(budgetDouble);
        totalBudgetValue.setText(formattedTotalBudgetValue);

        String formattedTotalValue = valueFormatter.format(valueDouble);
        totalProdValue.setText(formattedTotalValue);
    }

    public void setCrmStatusSpinner() {

        String[] statusArrayList = {"Prospect","Dropped", "Lost"};

        crmStatusSpinner.setAdapter(new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, statusArrayList));
        crmStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                crmStatusString = adapterView.getItemAtPosition(pos).toString();

                if(crmStatusString.equals("Dropped") || crmStatusString.equals("Lost")) {
                    reasonForDrop.setVisibility(View.VISIBLE);
                }
                else if(crmStatusString.equals("Prospect")) {
                    //reasonForDropContainer.setVisibility(View.GONE);
                    reasonForDrop.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkInTimeString = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            loginId = bundle.getString("login_id");
            crmNumberString = bundle.getString("crmNo");
            crmStatusString = bundle.getString("crmStatus");
            crmStatusDateString = bundle.getString("crmStatusDate");
            randomNumber = Integer.parseInt(bundle.getString("randomNumber"));
            descriptionString = bundle.getString("description");
            Log.d("Crm--", "View Crm Customer Code: " +customerCode+" loginId"+loginId);

            crm_number = crmNumberString;
            viewCrmProdArrayList = (ArrayList<CrmProducts>) getIntent().getSerializableExtra("crmProdList");

            if(crmStatusString.equals("Prospect")) {
                crmStatus.setVisibility(View.GONE);
                crmStatusSpinner.setVisibility(View.VISIBLE);
                setCrmStatusSpinner();
            }

            Log.d("Crm--", "View checkInTimeString: " +checkInTimeString);
            Log.d("Crm--", "View customerCode : " + customerCode);
            Log.d("Crm--", "View crmNo: " + crmNumberString);


            //Log.d("Crm--", "View crmStatus: " +crmStatusString);
            Log.d("Crm--", "View crmStatusArray SIze: " +viewCrmProdArrayList.size());
        }

        checkInTime.setText(checkInTimeString);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + sharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        crmNo.setText(crmNumberString);

        //Populate Customer Information Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                customerNameString = customerDetails.getString(2);
                customerEmailAddress = customerDetails.getString(33);

                String cityVal = customerDetails.getString(9);
                String stateVal = customerDetails.getString(10);
                if(cityVal != null || stateVal != null) {
                    city.setText(String.format("%s-%s", cityVal, stateVal));
                } else {
                    city.setText("");
                }

                accountNo.setText(customerDetails.getString(1));


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                // ship_address1 = customerDetails.getString(14);
                //ship_address2 = customerDetails.getString(15);
                //ship_address3 = customerDetails.getString(16);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                customerAddress1 = customerDetails.getString(6);
                customerAddress2 = customerDetails.getString(7);
                customerAddress3 = customerDetails.getString(8);
                customerPhoneNumber = customerDetails.getString(11);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/mSale/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }

        getCurrencyFromSettings();
        getCompanyInfo();
        getBdeInfo();

    }

    public void getBdeInfo() {
        //Populate SalesRep Details
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(loginId);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }
    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);
            }
        }

    }

    public void getCurrencyFromSettings() {
        gs = generalSettingsTable.getSettingByKey("currency");
        if(gs != null) {
            currency.setText(gs.getValue());
        }
    }

    private void setCrmDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        crmDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        storeCrmDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        crmTime = sdf.format(outDate);

    }

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }


    private void setFooterDateTime() {
        final TextView datetime = (TextView) findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_ENQUIRY) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    selectedImage = getResizedBitmap(selectedImage, 50);
                    customerImage.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                }
            }

        } else if (requestCode == CAPTURE_ENQUIRY) {
            if (resultCode == RESULT_OK) {

                image_id = lpoImagesTable.get_lpo_gallery_image_id();
                image_id = image_id + 1;
                String imagename = "customer_image" + image_id;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                String fileName = "lpo_" + crm_number + "_" + image_id + ".png";
                fileUtils.storeImage(photo, fileName, crmEnquiryImageFolderPath);

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                LPOImages lpoImages = new LPOImages();
                lpoImages.setOrderCode(crm_number);
                lpoImages.setLpoImageName(fileName);
                lpoImages.setLpoImagePath(crmEnquiryImageFolderPath + File.separator + fileName);
                lpoImages.setImageType("Enquiry");
                lpoImages.setVisitSeqNumber(visitSequence);
                lpoImagesTable.create(lpoImages);

            }
        }


        if (requestCode == SELECT_QUOTATION) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    selectedImage1 = BitmapFactory.decodeStream(imageStream);
                    selectedImage1 = getResizedBitmap(selectedImage1, 50);
                    customerImage.setImageBitmap(selectedImage1);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                }
            }

        } else if (requestCode == CAPTURE_QUOTATION) {
            if (resultCode == RESULT_OK) {

                image_id1 = lpoImagesTable.get_lpo_gallery_image_id();
                image_id1 = image_id1 + 1;
                String imagename = "customer_image" + image_id1;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                String fileName = "lpo_" + crm_number + "_" + image_id1 + ".png";
                fileUtils.storeImage(photo, fileName, crmQuotationImageFolderPath);

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                LPOImages lpoImages = new LPOImages();
                lpoImages.setOrderCode(crmNo.getText().toString());
                lpoImages.setLpoImageName(fileName);
                lpoImages.setLpoImagePath(crmQuotationImageFolderPath + File.separator + fileName);
                lpoImages.setImageType("Quotation");
                lpoImages.setVisitSeqNumber(visitSequence);
                lpoImagesTable.create(lpoImages);

                isQuotationCap = true;

            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


}
