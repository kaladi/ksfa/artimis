package in.kumanti.emzor.activity;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.ExpenseTable;
import in.kumanti.emzor.eloquent.ExpenseTypeTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CustomerDetails;
import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.model.ExpenseType;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.PrimaryInvoice;
import in.kumanti.emzor.model.TripDetails;
import in.kumanti.emzor.model.Warehouse;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class NewSRExpenseActivity extends MainActivity implements AdapterView.OnItemSelectedListener {

    private static final int SELECT_PHOTO1 = 1;
    private static final int CAPTURE_PHOTO1 = 2;
    private static final int CAPTURE_PHOTO2 = 3;
    String login_id = "",expenseNum=  "";
    ImageView actionbarBackButton, deviceInfo, saveButton;
    TextView expenseDateButton,voucherDateButton,expensePostDateTv,expensePostTimeTv,tripDateButton;
    TextView expenseNumber;
    ImageView createSignature, viewSignature;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private int mYear, mMonth, mDay;
    ImageView captureCustomerImage, showCustomerImage;
    String expenseDateString,voucherDateString,tripDateString,postDateFtp;

    String checkin_time1 = "";
    String customer_id = "";
    int image_id = 0;
    TextView actionbarTitle, customerCode, checkInTime;
    Chronometer chronometer;
    MyDBHandler myDBHandler;
    ExpenseTypeTable expenseTypeTable;
    TextView expense_currencyTv;
    EditText etexpense_amount,etexpense_mileage,etexpense_voucher_no,etexpense_remarks;
    TextView tvexpense_mileage;
    String expenseDate,voucherDate,tripDate,tripNoString ="",vendorString="";
    CustomSearchableSpinner tripNoSpinner;
    ArrayAdapter<TripDetails> tripNoSpinnerArrayAdapter;
    SearchableSpinner expense_typeSpinner,expense_vendorSpinner;
    SharedPreferenceManager sharedPreferenceManager;

    ArrayList<ExpenseType> expenseTypeArrayList;
    ArrayAdapter<ExpenseType> expenseTypeSpinnerArrayAdapter;
    ArrayList<CustomerDetails> customerNameArrayList;
    ArrayList<TripDetails> tripNoArrayList;
    ArrayList<TripDetails> tripDetailArrayList;
    ArrayAdapter<CustomerDetails> customerNameSpinnerArrayAdapter;

    ExpenseTable expenseTable;
    LpoImagesTable lpoImagesTable;

    GeneralSettings gs;
    GeneralSettingsTable generalSettingsTable;

    String trip_num = null,trip_no,customer_name,customer_type,expenseTypeString="",startTime,stopDate,stopTime;
    Globals globals;
    int random_num = 0 ;
    Constants constants;
    String company_code, tab_code, sr_code, tab_prefix;
    Bitmap selectedImage;
    private TextView marqueeText;
    LinearLayout layout_mileage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_new_expense);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        setFooterDateTime();
        generateExpenseSeqNumber();

        trip_num = globals.getTrip_num();


        actionbarTitle.setText("New Expense Booking");
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        company_code = Constants.COMPANY_CODE;
        tab_code = Constants.TAB_CODE;
        sr_code = Constants.SR_CODE;
        tab_prefix = Constants.TAB_PREFIX;


        expense_vendorSpinner.setOnItemSelectedListener(this);
        customerNameArrayList = myDBHandler.getCustomerDetails();
        CustomerDetails cd= new CustomerDetails();
        cd.setCustomerCode("-1");
        cd.setCustomerName("Select") ;
        customerNameArrayList.add(0, cd);
        expense_vendorSpinner.setAdapter(new ArrayAdapter<CustomerDetails>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, customerNameArrayList));
        expense_vendorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                if(pos !=0)
                vendorString = adapterView.getItemAtPosition(pos).toString();
//                customer_name = adapterView.getItemAtPosition(pos).toString();
//                customer_type = myDBHandler.getCustomerDetails();
                CustomerDetails cd = (CustomerDetails) adapterView.getItemAtPosition(pos);
                customer_name = cd.getCustomerName();
                System.out.println("MM::expenseTypeString1 = " + expenseTypeString);
                System.out.println("MM::(cd.getCustomerName()) = " + (cd.getCustomerName()));
                System.out.println("MM::customer_name = " + customer_name);
                System.out.println("MM::getCustomerCode = " + cd.getCustomerCode());
                System.out.println("MM::getCustomerType = " + cd.getCustomerType());

                customer_type = cd.getCustomerType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {

                System.out.println("FFFFFFF");
                System.out.println("MM::(cd.getCustomerName()) = " + (cd.getCustomerName()));
                System.out.println("MM::customer_name = " + customer_name);
                System.out.println("MM::getCustomerCode = " + cd.getCustomerCode());
                System.out.println("MM::getCustomerType = " + cd.getCustomerType());
            }
        });

        gs = generalSettingsTable.getSettingByKey("currency");
        if(gs != null) {
            expense_currencyTv.setText(gs.getValue());
        }

        tripDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewSRExpenseActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String PATTERN = "dd MMM YY";
                                SimpleDateFormat dateFormat = new SimpleDateFormat();
                                Date d = null;
                                try {
                                    d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                dateFormat.applyPattern(PATTERN);
                                dateFormat.format(d);
                                tripDateButton.setText(dateFormat.format(d));
                                tripDate = tripDateButton.getText().toString();
                                //expenseDateButton.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
//                                System.out.println("TT::tripDate = " + tripDate);

                                SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
                                tripDateString = sdfd.format(d);
//                                System.out.println("TT::tripDateString = " + tripDateString);

                                tripNoArrayList = myDBHandler.getTripDetail(tripDateString);
//                                System.out.println("TT::tripNoArrayList1 = " + tripNoArrayList);
                                TripDetails w = new TripDetails();
                                w.setTripNumber("-1");
                                w.setTripName("Select");
                                tripNoArrayList.add(0, w);
//                                System.out.println("TT::tripNoArrayList2 = " + tripNoArrayList);
                                tripNoSpinnerArrayAdapter = new ArrayAdapter<TripDetails>(getApplicationContext(), android.R.layout.simple_spinner_item, tripNoArrayList);
                                tripNoSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                tripNoSpinner.setAdapter(tripNoSpinnerArrayAdapter);
                                tripNoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                                               int pos, long id) {
                                        tripNoSpinner.isSpinnerDialogOpen = false;

                                        TripDetails w = (TripDetails) adapterView.getItemAtPosition(pos);
                                        trip_no = w.getTripNumber();
                                        tripNoString = trip_no;
                                       /* tripDetailArrayList = myDBHandler.getTripTime(trip_no,tripDateString);
                                        if(tripDetailArrayList!=null)
                                       { startTime =tripDetailArrayList.get(0).getTripTime();
                                        stopDate =tripDetailArrayList.get(0).getTripStopDate();
                                        stopTime = tripDetailArrayList.get(0).getTripStopTime();
                                        System.out.println("TTTTTT: = " + startTime);
                                        System.out.println("TTTTTT: = " + stopDate);
                                        System.out.println("TTTTTT: = " + stopTime);}*/

                                        Cursor tripDetailArrayList = myDBHandler.getTripTime(trip_no,tripDateString);

                                        int tabrowCount = tripDetailArrayList.getCount();

                                        if (tabrowCount != 0) {

                                            int i = 0;
                                            while (tripDetailArrayList.moveToNext()) {

                                                startTime =tripDetailArrayList.getString(3);
                                                stopDate =tripDetailArrayList.getString(9);
                                                stopTime = tripDetailArrayList.getString(11);
                                                System.out.println("TTTTTT: = " + startTime);
                                                System.out.println("TTTTTT: = " + stopDate);
                                                System.out.println("TTTTTT: = " + stopTime);

                                            }
                                        }

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapter) {
                                        tripNoSpinner.isSpinnerDialogOpen = false;
                                    }
                                });

//                                System.out.println("tripDateString = " + tripDateString);
//                                tripNoArrayList = myDBHandler.getTripDetail(tripDateString);
//                                TripDetails cd1 = new TripDetails();
//                                cd1.setTripName("-1");
//                                cd1.setTripNumber("Select");
//                                tripNoArrayList.add(0, cd1);
//                                tripNoSpinner.setAdapter(new ArrayAdapter<TripDetails>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, tripNoArrayList));
//                                tripNoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                    @Override
//                                    public void onItemSelected(AdapterView<?> adapterView, View view,
//                                                               int pos, long id) {
//
//                                        TripDetails cd1 = (TripDetails) adapterView.getItemAtPosition(pos);
//                                        trip_no = cd1.getTripNumber();
//                                        System.out.println("trip_no = " + trip_no);
//                                    }
//
//                                    @Override
//                                    public void onNothingSelected(AdapterView<?> adapter) {
//
//                                    }
//                                });

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        expenseDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewSRExpenseActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String PATTERN = "dd MMM YY";
                                SimpleDateFormat dateFormat = new SimpleDateFormat();
                                Date d = null;
                                try {
                                    d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                dateFormat.applyPattern(PATTERN);
                                dateFormat.format(d);
                                expenseDateButton.setText(dateFormat.format(d));
                                expenseDate = expenseDateButton.getText().toString();
                                //expenseDateButton.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);


                                SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
                                expenseDateString = sdfd.format(d);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        voucherDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewSRExpenseActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String PATTERN = "dd MMM YY";
                                SimpleDateFormat dateFormat = new SimpleDateFormat();
                                Date d = null;
                                try {
                                    d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                dateFormat.applyPattern(PATTERN);
                                dateFormat.format(d);
                                voucherDateButton.setText(dateFormat.format(d));
                                voucherDate = voucherDateButton.getText().toString();
                                //expenseDateButton.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);


                                SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
                                voucherDateString = sdfd.format(d);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY");
        SimpleDateFormat stf = new SimpleDateFormat("hh:mm a");
        String dateString = sdf.format(date);
        String timeString = stf.format(date);
        expensePostDateTv.setText(dateString);
        expensePostTimeTv.setText(timeString);

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String postDateString = dateFormat1.format(date);
        postDateFtp = postDateString;

        captureCustomerImage.setOnClickListener(v -> {
            Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent1, CAPTURE_PHOTO1);
        });

        showCustomerImage.setOnClickListener(v -> {
            int lpo_image_count = lpoImagesTable.get_lpo_order_image_count(expenseNum);

            if (lpo_image_count == 0) {
                Toast.makeText(NewSRExpenseActivity.this, "No Images,Capture and Check again.", Toast.LENGTH_SHORT).show();
            } else {
                Intent myintent = new Intent(getApplicationContext(), OrderGalleryActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("customer_id", customer_id);
                bundle.putString("login_id", login_id);
                bundle.putString("order_number", expenseNum);
                myintent.putExtras(bundle);

                startActivity(myintent);
            }


        });



        createSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopupsignature();
            }
        });


        viewSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedImage != null) {
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.image_preview, null);
                    ImageView capturedImage, closeButton;
                    capturedImage = alertLayout.findViewById(R.id.capturedImage);
                    closeButton = alertLayout.findViewById(R.id.closeImageView);
                    capturedImage.setImageBitmap(selectedImage);
                    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());
                    // this is set the view from XML inside AlertDialog

                    alert.setView(alertLayout);
                    alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(true);

                    android.app.AlertDialog dialog = alert.create();
                    dialog.show();


                }

            }
        });

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

        saveButton.setOnClickListener(v -> {
            boolean validForm = true;
            if (TextUtils.isEmpty(expenseDateButton.getText().toString())) {
                validForm = false;
                expenseDateButton.setError("Please enter value");
            } else
                expenseDateButton.setError(null);

            if (expense_typeSpinner.getSelectedItem().toString().trim().equals("Select")) {
                validForm = false;
                Toast.makeText(getApplicationContext(), "Please choose a expense type", Toast.LENGTH_SHORT).show();
            }
/*
            if (TextUtils.isEmpty(expense_typeSpinner.getSelectedItem().toString())) {
                validForm = false;
                Toast.makeText(getApplicationContext(), "Please choose a expense type", Toast.LENGTH_SHORT).show();
            }*/
            if (TextUtils.isEmpty(etexpense_amount.getText().toString())) {
                validForm = false;
                etexpense_amount.setError("Please enter value");
            } else
                etexpense_amount.setError(null);


            //If not Valid form return empty
            if (!validForm)
                return;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate = new Date();
            String creation_date = timeStampFormat.format(myDate);

            Expense expense = new Expense();
            expense.setExpenseNumber(expenseNum);
            expense.setExpenseDate(expenseDateString);
            expense.setExpenseType(expense_typeSpinner.getSelectedItem().toString());
            expense.setExpenseMileage(etexpense_mileage.getText().toString());
            expense.setExpenseVoucherNo(etexpense_voucher_no.getText().toString());
            expense.setExpenseVoucherDate(voucherDateString);
            expense.setExpenseVendor(vendorString);
            expense.setExpenseVendorType(customer_type);
            expense.setExpenseCurrency(expense_currencyTv.getText().toString());
            expense.setExpenseAmount(etexpense_amount.getText().toString());
            expense.setExpenseRemarks(etexpense_remarks.getText().toString());

            if (selectedImage != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] byteArrayImage = baos.toByteArray();
                String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                expense.setExpenseSignature(encodedImage);
            }

            expense.setExpensePostDate(postDateString);
            expense.setExpensePostTime(expensePostTimeTv.getText().toString());
            expense.setExpenseStatus("Pending");
            expense.setExpenseRandomNumber(random_num);
            System.out.println("TTT::tripNoString = " + tripNoString);
            System.out.println("TTT::vendorString = " + vendorString);
            expense.setTripNumber(tripNoString);
            expense.setTripStartDate(tripDateString);
            expense.setTripStartTime(startTime);
            expense.setTripEndDate(stopDate);
            expense.setTripEndTime(stopTime);
            expenseTable.create(expense);





            if (selectedImage != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.PNG, 100, baos);
                System.out.println("MMM::selectedImage = " + selectedImage);

                String fileName = "sign_" + expenseNum + "_" + postDateFtp + ".png";
                System.out.println("MMM::fileName = " + fileName);
                fileUtils.storeImage(selectedImage, fileName, expenseSignatureFolderPath);
                System.out.println("expenseSignatureFolderPath = " + expenseSignatureFolderPath);
                expenseTable.updateSignatureimage(expenseNum, expenseSignatureFolderPath + File.separator + fileName, fileName);
            }

            Intent myintent = new Intent(getApplicationContext(), ExpenseSummaryActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("sales_rep_id", login_id);
            bundle.putString("trip_num", trip_num);
            myintent.putExtras(bundle);
            startActivity(myintent);

        });

        expense_typeSpinner.setOnItemSelectedListener(this);


        loadSpinnerType();

    }



    private void showpopupsignature() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);
        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                selectedImage = signatureBitmap;
                viewSignature.setImageBitmap(signatureBitmap);

//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                selectedImage.compress(Bitmap.CompressFormat.PNG, 100, baos);
//                System.out.println("MMM::selectedImage = " + selectedImage);
//
//                String fileName = "sign_" + expenseNum + "_" + postDateFtp + ".png";
//                System.out.println("MMM::fileName = " + fileName);
//                fileUtils.storeImage(selectedImage, fileName, expenseSignatureFolderPath);
//                System.out.println("expenseSignatureFolderPath = " + expenseSignatureFolderPath);
//                expenseTable.updateSignatureimage(expenseNum, expenseSignatureFolderPath + File.separator + fileName, fileName);

                dialog.dismiss();

            }
        });

    }

    public void loadSpinnerType() {

        expenseTypeArrayList = expenseTypeTable.getExpenseType();
        ExpenseType et= new ExpenseType();
        et.setCode("-1");
        et.setValue("Select"); ;
        expenseTypeArrayList.add(0, et);
        expense_typeSpinner.setAdapter(new ArrayAdapter<ExpenseType>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, expenseTypeArrayList));
        expense_typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                if(pos !=0)
                    expenseTypeString = adapterView.getItemAtPosition(pos).toString();

                if(expenseTypeString.equals("Fuel")){
                    layout_mileage.setVisibility(View.VISIBLE);
                    tvexpense_mileage.setVisibility(View.VISIBLE);
                    etexpense_mileage.setVisibility(View.VISIBLE);
                }else{
                    layout_mileage.setVisibility(View.GONE);
                    tvexpense_mileage.setVisibility(View.GONE);
                    etexpense_mileage.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {

            }
        });


      /*  String[] labels = {"Select","Boarding","Lodging","Communication","Entertainment","Fuel","Vehicle Repair","Toll","Parking","Miscellaneous"};

        expense_typeSpinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, labels));

        expense_typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                expenseTypeString = adapterView.getItemAtPosition(pos).toString();
                System.out.println("MM::expenseTypeString = " + expenseTypeString);

                if(expenseTypeString.equals("Fuel")){
                    layout_mileage.setVisibility(View.VISIBLE);
                    tvexpense_mileage.setVisibility(View.VISIBLE);
                    etexpense_mileage.setVisibility(View.VISIBLE);
                }else{
                    layout_mileage.setVisibility(View.GONE);
                    tvexpense_mileage.setVisibility(View.GONE);
                    etexpense_mileage.setVisibility(View.GONE);
                }



                *//*   loadSpinnerProduct(expenseTypeString);*//*

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });*/

    }

    private void generateExpenseSeqNumber() {

        random_num = expenseTable.getExpenseNumber();
        System.out.println("Random"+random_num);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        Log.d("Expense", "Random No---" + random_num);

        if (random_num <= 9) {
            expenseNumber.setText(Constants.SR_CODE + "EXP" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            expenseNumber.setText(Constants.SR_CODE + "EXP" + rec_seq_date + "0" + random_num);

        } else {
            expenseNumber.setText(Constants.SR_CODE + "EXP" + rec_seq_date + random_num);
        }

        expenseNum = expenseNumber.getText().toString();

    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        expenseNumber = findViewById(R.id.expense_NumberTv);
        expenseDateButton = findViewById(R.id.expneseDateButton);
        voucherDateButton = findViewById(R.id.voucherDateButton);
        tripDateButton = findViewById(R.id.tripDateButton);
        tripNoSpinner = findViewById(R.id.trip_noSpinner);
        captureCustomerImage = findViewById(R.id.captureImageView);
        showCustomerImage = findViewById(R.id.showImageView);
        expensePostDateTv = findViewById(R.id.expensePostDateTv);
        expensePostTimeTv = findViewById(R.id.expensePostTimeTv);
        expense_typeSpinner = findViewById(R.id.expense_typeSpinner);
        etexpense_amount= findViewById(R.id.etexpense_amount);
        expense_vendorSpinner= findViewById(R.id.expense_vendorSpinner);
        expense_currencyTv= findViewById(R.id.expense_currencyTv);
        etexpense_voucher_no= findViewById(R.id.etexpense_voucher_no);
        etexpense_remarks =findViewById(R.id.etexpense_remarks);
        etexpense_mileage = findViewById(R.id.etexpense_mileage);
        tvexpense_mileage = findViewById(R.id.expense_mileage);
        layout_mileage =  findViewById(R.id.input_layout_mileage);


        createSignature = findViewById(R.id.createSignImageView);
        viewSignature = findViewById(R.id.viewSignImageView);

        saveButton = findViewById(R.id.saveImageView);
        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        globals = ((Globals) getApplicationContext());
        constants = new Constants();

        lpoImagesTable = new LpoImagesTable(getApplicationContext());
        expenseTypeTable = new ExpenseTypeTable(getApplicationContext());
        expenseTable = new ExpenseTable(getApplicationContext());
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());

    }

    public void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time1 = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            Log.d("LoginId", login_id);
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                showCustomerImage.setEnabled(true);

            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_PHOTO1) {
            if (resultCode == RESULT_OK) {

                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

            }
        }
        else if (requestCode == CAPTURE_PHOTO1) {
            if (resultCode == RESULT_OK) {

                image_id = lpoImagesTable.get_lpo_gallery_image_id();
                image_id = image_id + 1;
                String imagename = "customer_image" + image_id;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                String fileName = "lpo_" + expenseNum + "_" + image_id + ".png";
                fileUtils.storeImage(photo, fileName, orderGalleryImageFolderPath);

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                LPOImages lpoImages = new LPOImages();
                lpoImages.setOrderCode(expenseNum);
                lpoImages.setLpoImageName(fileName);
                lpoImages.setLpoImagePath(orderGalleryImageFolderPath + File.separator + fileName);
                lpoImages.setVisitSeqNumber(visitSequence);
                lpoImagesTable.create(lpoImages);

            }
        }
//        else if (requestCode == CAPTURE_PHOTO2) {
//            if (resultCode == RESULT_OK) {
////                onCaptureImageResult(data);
//                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
//                selectedImage = signatureBitmap;
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                selectedImage.compress(Bitmap.CompressFormat.PNG, 100, baos);
//
//                String fileName = "sign_" + expenseNum + "_" + postDateFtp + ".png";
//                fileUtils.storeImage(selectedImage, fileName, expenseSignatureFolderPath);
//                dbHandler.updatemileageimage(expenseNum, expenseSignatureFolderPath + File.separator + fileName, fileName);
//
//            }
//        }
    }



    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
