package in.kumanti.emzor.model;


import android.text.TextUtils;

public class Age {

    public int ageId;
    public String customerId;
    public String belowThirtyVal;
    public String belowThirtyPer;
    public String thirtyOneToSixtyVal;
    public String thirtyOneToSixtyPer;
    public String sixtyOneToNinetyVal;
    public String sixtyOneToNinetyPer;
    public String aboveNinetyVal;
    public String aboveNinetyPer;
    public String createdAt;
    public String updatedAt;

    public String getBelowThirtyVal() {
        return TextUtils.isEmpty(belowThirtyVal) ? "0" : belowThirtyVal;
    }

    public void setBelowThirtyVal(String belowThirtyVal) {
        this.belowThirtyVal = belowThirtyVal;
    }


    public String getBelowThirtyPer() {
        return belowThirtyPer;
    }

    public void setBelowThirtyPer(String belowThirtyPer) {
        this.belowThirtyPer = belowThirtyPer;
    }


    public String getThirtyOneToSixtyVal() {
        return TextUtils.isEmpty(thirtyOneToSixtyVal) ? "0" : thirtyOneToSixtyVal;
    }

    public void setThirtyOneToSixtyVal(String thirtyOneToSixtyVal) {
        this.thirtyOneToSixtyVal = thirtyOneToSixtyVal;
    }


    public String getThirtyOneToSixtyPer() {
        return thirtyOneToSixtyPer;
    }

    public void setThirtyOneToSixtyPer(String thirtyOneToSixtyPer) {
        this.thirtyOneToSixtyPer = thirtyOneToSixtyPer;
    }


    public String getSixtyOneToNinetyVal() {
        return TextUtils.isEmpty(sixtyOneToNinetyVal) ? "0" : sixtyOneToNinetyVal;
    }

    public void setSixtyOneToNinetyVal(String sixtyOneToNinetyVal) {
        this.sixtyOneToNinetyVal = sixtyOneToNinetyVal;
    }


    public String getSixtyOneToNinetyPer() {
        return sixtyOneToNinetyPer;
    }

    public void setSixtyOneToNinetyPer(String sixtyOneToNinetyPer) {
        this.sixtyOneToNinetyPer = sixtyOneToNinetyPer;
    }


    public String getAboveNinetyVal() {
        return TextUtils.isEmpty(aboveNinetyVal) ? "0" : aboveNinetyVal;
    }

    public void setAboveNinetyVal(String aboveNinetyVal) {
        this.aboveNinetyVal = aboveNinetyVal;
    }


    public String getAboveNinetyPer() {
        return aboveNinetyPer;
    }

    public void setAboveNinetyPer(String aboveNinetyPer) {
        this.aboveNinetyPer = aboveNinetyPer;
    }


    public int getAgeId() {
        return ageId;
    }

    public void setAgeId(int ageId) {
        this.ageId = ageId;
    }


    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }


    public String getCreated_at() {
        return createdAt;
    }

    public void setCreated_at(String created_at) {
        this.createdAt = created_at;
    }

    public String getUpdated_at() {
        return updatedAt;
    }

    public void setUpdated_at(String updated_at) {
        this.updatedAt = updated_at;
    }


    @Override
    public String toString() {
        return belowThirtyVal;
    }


}
