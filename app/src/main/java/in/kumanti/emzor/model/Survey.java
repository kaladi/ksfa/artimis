package in.kumanti.emzor.model;

import java.util.ArrayList;

public class Survey {

    public int id;
    public String product_id;
    public String company_id;
    public String customer_id;
    public String sales_rep_id;
    public String survey_name;
    public String survey_code;
    public String survey_type;
    public String survey_date;
    public String survey_description;
    public ArrayList<SurveyQuestions> survey_questions;
    public String created_at;
    public String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }


    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }


    public String getSales_rep_id() {
        return sales_rep_id;
    }

    public void setSales_rep_id(String sales_rep_id) {
        this.sales_rep_id = sales_rep_id;
    }

    public String getSurvey_name() {
        return survey_name;
    }

    public void setSurvey_name(String survey_name) {
        this.survey_name = survey_name;
    }

    public String getSurvey_code() {
        return survey_code;
    }

    public void setSurvey_code(String survey_code) {
        this.survey_code = survey_code;
    }


    public String getSurvey_type() {
        return survey_type;
    }

    public void setSurvey_type(String survey_type) {
        this.survey_type = survey_type;
    }


    public String getSurvey_description() {
        return survey_description;
    }

    public void setSurvey_description(String survey_description) {
        this.survey_description = survey_description;
    }

    public ArrayList<SurveyQuestions> getSurvey_questions() {
        return survey_questions;
    }

    public void setSurvey_questions(ArrayList<SurveyQuestions> survey_questions) {
        this.survey_questions = survey_questions;
    }

    public String getSurvey_date() {
        return survey_date;
    }

    public void setSurvey_date(String survey_date) {
        this.survey_date = survey_date;
    }


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return survey_name;
    }


}
