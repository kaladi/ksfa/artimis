package in.kumanti.emzor.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.LedgerAdapter;
import in.kumanti.emzor.eloquent.AgeTable;
import in.kumanti.emzor.eloquent.LedgerTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.Age;
import in.kumanti.emzor.model.Ledger;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class LedgerActivity extends MainActivity {

    public RecyclerView ledgerRecyclerView;
    String login_id, customer_type = "", customer_id = "", checkin_time1 = "";
    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView customerName, accountNo, city;
    MyDBHandler dbHandler;
    TextView actionbarTitle, checkInTime;
    LedgerTable ledgerTable;
    AgeTable ageTable;
    ArrayList<Ledger> ledgerArrayList;
    ArrayList<Age> ageArrayList;
    Button ageButton;
    LinearLayout ageValuesContainer;
    TextView ageBelowThirtyValue, ageBelowThirtyPercentage, ageThirtyOneToSixtyValue, ageThirtyOneToSixtyPercentage, ageSixtyOneToNinetyValue, ageSixtyOneToNinetyPercentage, ageAboveNinetyValue, ageAboveNinetyPercentage, totalAgeValue, totalAgePer, ageNotesInfo;
    TextView ledgerTotalInvoiceValTv, ledgerTotalReceiptValTv, ledgerTotalOutstandingValTv, ledgerNetOutstandingTv;
    DecimalFormat decimalFormat;
    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    double invoiceTotal = 0;
    double ageBelow30 = 0;
    double age31To60 = 0;
    double age61To90 = 0;
    double above90 = 0;
    private RecyclerView.Adapter ledgerAdapter;
    private RecyclerView.LayoutManager ledgerLayoutManager;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_ledger);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText(R.string.ledger_actionbar_title);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        ledgerRecyclerView.setHasFixedSize(true);
        ledgerLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ledgerRecyclerView.setLayoutManager(ledgerLayoutManager);

        try {

            ledgerArrayList = ledgerTable.getLedgerArrayList(customer_id, customer_type);
            double receiptTotal = 0;
            double outstandingTotal = 0;
            double netOutstanding = 0;
            if ("Primary".equals(customer_type)) {
                for (Ledger l : ledgerArrayList) {
                    invoiceTotal += TextUtils.isEmpty(l.getInvoiceAmount()) ? 0 : Double.parseDouble(l.getInvoiceAmount());
                    receiptTotal += TextUtils.isEmpty(l.getReceiptAmount()) ? 0 : Double.parseDouble(l.getReceiptAmount());

                    outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                    Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount()+" "+ outstandingTotal);
                }
            }
            else
            {
                ArrayList<String> invoiceNumberList = new ArrayList<>();
                for (Ledger l : ledgerArrayList) {
                    if(!invoiceNumberList.contains(l.getInvoiceNumber()))
                        invoiceNumberList.add(l.getInvoiceNumber());
                }

                for (Ledger l : ledgerArrayList) {
                    invoiceTotal += TextUtils.isEmpty(l.getInvoiceAmount()) ? 0 : Double.parseDouble(l.getInvoiceAmount());
                    receiptTotal += TextUtils.isEmpty(l.getReceiptAmount()) ? 0 : Double.parseDouble(l.getReceiptAmount());
                    if(l.getTransactionType().equals("Return"))
                        outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
                }
                for(String invoiceNumber:invoiceNumberList){
                    String invoiceLastBalance = "";
                    for (Ledger l : ledgerArrayList) {
                        if(invoiceNumber.equals(l.getInvoiceNumber())&&!l.getTransactionType().equals("Return"))
                            invoiceLastBalance = l.getOutstandingAmount();
                    }
                    outstandingTotal += TextUtils.isEmpty(invoiceLastBalance) ? 0 : Double.parseDouble(invoiceLastBalance);
                    // Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount()+" "+ outstandingTotal);
                }
            }


            netOutstanding = receiptTotal - outstandingTotal;
            decimalFormat = new DecimalFormat("#,###.00");
            String formatInvoiceAmount = invoiceTotal != 0 ? decimalFormat.format(Math.round(invoiceTotal)) : "";
            String formatReceiptTotalAmount = receiptTotal != 0 ? decimalFormat.format(Math.round(receiptTotal)) : "0";
            String formatOutstandingTotalAmount = outstandingTotal != 0 ? decimalFormat.format(Math.round(outstandingTotal)) : "0.00";
            String formatNetOutstandingAmount = netOutstanding != 0 ? decimalFormat.format(Math.round(netOutstanding)) : "";

            ledgerTotalInvoiceValTv.setText(formatInvoiceAmount);
            ledgerTotalReceiptValTv.setText(formatReceiptTotalAmount);
            ledgerTotalOutstandingValTv.setText(formatOutstandingTotalAmount);
            ledgerNetOutstandingTv.setText(formatNetOutstandingAmount);
            Log.i("QueryDataList ", "" + ledgerArrayList.size());
            ledgerAdapter = new LedgerAdapter(ledgerArrayList);
            ledgerRecyclerView.setAdapter(ledgerAdapter);

        } catch (Exception e) {
            Log.d("QueryDataResponse", "" + e.getMessage());
        }


        ageButton.setOnClickListener(v -> showCustomerAge());


        actionbarBackButton.setOnClickListener(v -> finish());


        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });


    }

    public double calculateOutstandingfromLedger(String customer_id,String ageCount){
        double outstandingTotal = 0;
        ArrayList<Ledger> ledgerArrayList = ledgerTable.getLedgerArrayByDataAgesList(customer_id,ageCount);
        ArrayList<String> invoiceNumberList = new ArrayList<>();
        for (Ledger l : ledgerArrayList) {
            if(!invoiceNumberList.contains(l.getInvoiceNumber()))
                invoiceNumberList.add(l.getInvoiceNumber());
        }

        for (Ledger l : ledgerArrayList) {
            if(l.getTransactionType().equals("Return"))
                outstandingTotal += TextUtils.isEmpty(l.getOutstandingAmount()) ? 0 : Double.parseDouble(l.getOutstandingAmount());
        }
        for(String invoiceNumber:invoiceNumberList){
            String invoiceLastBalance = "";
            for (Ledger l : ledgerArrayList) {
                if(invoiceNumber.equals(l.getInvoiceNumber())&&!l.getTransactionType().equals("Return"))
                    invoiceLastBalance = l.getOutstandingAmount();
            }
            outstandingTotal += TextUtils.isEmpty(invoiceLastBalance) ? 0 : Double.parseDouble(invoiceLastBalance);
            // Log.d("QueryDataList", l.getInvoiceAmount() + " " + l.getReceiptAmount() + " " + l.getOutstandingAmount()+" "+ outstandingTotal);
        }
        return outstandingTotal;
    }
    public void showCustomerAge() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_customer_age, null);

        ageTable = new AgeTable(getApplicationContext());
        ageArrayList = ageTable.getAgeList(customer_id, customer_type);
        Log.d("AgeArrayList ", "" + ageArrayList.size());

        ageBelowThirtyValue = alertLayout.findViewById(R.id.ageBelowThirtyValueTv);
        ageBelowThirtyPercentage = alertLayout.findViewById(R.id.ageBelowThirtyPerTv);
        ageThirtyOneToSixtyValue = alertLayout.findViewById(R.id.ageThirtyOneToSixtyValueTv);
        ageThirtyOneToSixtyPercentage = alertLayout.findViewById(R.id.ageThirtyOneToSixtyPerTv);
        ageSixtyOneToNinetyValue = alertLayout.findViewById(R.id.ageSixtyOneToNinetyValueTv);
        ageSixtyOneToNinetyPercentage = alertLayout.findViewById(R.id.ageSixtyOneToNinetyPerTv);
        ageAboveNinetyValue = alertLayout.findViewById(R.id.ageAboveNinetyValueTv);
        ageAboveNinetyPercentage = alertLayout.findViewById(R.id.ageAboveNinetyPerTv);
        totalAgeValue = alertLayout.findViewById(R.id.totalAgeValueTv);
        totalAgePer = alertLayout.findViewById(R.id.totalPercentageTV);
        ageNotesInfo = alertLayout.findViewById(R.id.ledgerAgeValueInfoTv);
        ageValuesContainer = alertLayout.findViewById(R.id.ageValueContainer);

        if ("Primary".equals(customer_type)) {
            ageNotesInfo.setText(R.string.age_calculation_receiving_notes);
        } else {
            ageValuesContainer.setVisibility(View.GONE);
        }


        for (Age a : ageArrayList) {
            decimalFormat = new DecimalFormat("#,###.00");
            if ("Primary".equals(customer_type)) {
                ageBelow30 = Double.parseDouble(a.getBelowThirtyVal());
            }
            else
            {
                ageBelow30 = calculateOutstandingfromLedger(customer_id,"30");
            }
            String formatAgeBelowThirtyVal = ageBelow30 != 0 ? decimalFormat.format(Math.round(ageBelow30)) : "";
            ageBelowThirtyValue.setText(formatAgeBelowThirtyVal);
            if ("Primary".equals(customer_type)) {
                age31To60 = Double.parseDouble(a.getThirtyOneToSixtyVal());
            }
            else
            {
                age31To60 = calculateOutstandingfromLedger(customer_id,"60");
            }
            String formatAgeOneToSixtyVal = age31To60 != 0 ? decimalFormat.format(Math.round(age31To60)) : "";
            ageThirtyOneToSixtyValue.setText(formatAgeOneToSixtyVal);
            if ("Primary".equals(customer_type)) {
                age61To90 = Double.parseDouble(a.getSixtyOneToNinetyVal());
            }
            else
            {
                age61To90 = calculateOutstandingfromLedger(customer_id,"90");
            }
            String formatSixtyOneToNinetyVal = age61To90 != 0 ? decimalFormat.format(Math.round(age61To90)) : "";
            ageSixtyOneToNinetyValue.setText(formatSixtyOneToNinetyVal);
            if ("Primary".equals(customer_type)) {
                above90 = Double.parseDouble(a.getSixtyOneToNinetyVal());
            }
            else
            {
                above90 = calculateOutstandingfromLedger(customer_id,"90+");
            }
            String formatAbove90Val = above90 != 0 ? decimalFormat.format(Math.round(above90)) : "";
            ageAboveNinetyValue.setText(formatAbove90Val);

            double totalAgeDoubleValue = ageBelow30 + age31To60 + age61To90 + above90;

            int belowThirtyPer = (int) ((ageBelow30 / totalAgeDoubleValue) * 100);
            ageBelowThirtyPercentage.setText(String.valueOf(belowThirtyPer));

            int thirtyOneToSixtyPer = (int) ((age31To60 / totalAgeDoubleValue) * 100);
            ageThirtyOneToSixtyPercentage.setText(String.valueOf(thirtyOneToSixtyPer));

            int sixtyOneToNinetyPer = (int) ((age61To90 / totalAgeDoubleValue) * 100);
            ageSixtyOneToNinetyPercentage.setText(String.valueOf(sixtyOneToNinetyPer));

            int aboveNinetyPer = (int) ((above90 / totalAgeDoubleValue) * 100);
            ageAboveNinetyPercentage.setText(String.valueOf(aboveNinetyPer));

            int totalAgePerInt = (belowThirtyPer + thirtyOneToSixtyPer + sixtyOneToNinetyPer + aboveNinetyPer);


            String totalAgeDoubleValueText = "";
            if (totalAgeDoubleValue > 1000000 || totalAgeDoubleValue < -1000000) {
                totalAgeDoubleValue = totalAgeDoubleValue / 1000000;
                totalAgeDoubleValueText = Math.round(totalAgeDoubleValue) + "M";
                totalAgeValue.setText(totalAgeDoubleValueText);


            } else {
                totalAgeDoubleValueText = (String.valueOf(totalAgeDoubleValue));
                String formatTotalAgeDoubleValueText = totalAgeDoubleValue != 0 ? decimalFormat.format(Math.round(totalAgeDoubleValue)) : "";
                totalAgeValue.setText(formatTotalAgeDoubleValueText);


            }
            totalAgePer.setText(String.valueOf(totalAgePerInt));
        }


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);

        alert.setNegativeButton("Close", (dialog, which) -> {
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        ledgerRecyclerView = findViewById(R.id.ledgerRecyclerView);
        ledgerTotalInvoiceValTv = findViewById(R.id.ledgerTotalInvoiceValTv);
        ledgerTotalReceiptValTv = findViewById(R.id.ledgerTotalReceiptValTv);
        ledgerTotalOutstandingValTv = findViewById(R.id.ledgerTotalOutstandingValTv);
        ledgerNetOutstandingTv = findViewById(R.id.ledgerNetOutstandingTv);
        ageButton = findViewById(R.id.ageButton);

        ledgerTable = new LedgerTable(getApplicationContext());

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

    }

    public void populateHeaderDetails() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time1 = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            customer_type = bundle.getString("customer_type");
            Log.d("CheckInTime ", checkin_time1);
        }

        checkInTime.setText(checkin_time1);


        //Populate Customer Information Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }


    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    @Override
    public void onBackPressed() {

    }

}
