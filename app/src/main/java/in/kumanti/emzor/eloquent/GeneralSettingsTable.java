package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.GeneralSettings;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_GENERAL_SETTINGS;

public class GeneralSettingsTable extends SQLiteOpenHelper {

    private static final String COLUMN_SETTINGS_ID = "settingsId";
    private static final String COLUMN_SETTINGS_KEY = "configuration_key";
    private static final String COLUMN_SETTINGS_VALUE = "value";
    private static final String COLUMN_SETTINGS_CREATED_AT = "created_at";
    private static final String COLUMN_SETTINGS_UPDATED_AT = "updated_at";

    public Context context;

    public GeneralSettingsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTINGS);

        String settingsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_GENERAL_SETTINGS + "(" +
                COLUMN_SETTINGS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SETTINGS_KEY + " TEXT, " +
                COLUMN_SETTINGS_VALUE + " TEXT, " +
                COLUMN_SETTINGS_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SETTINGS_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(settingsTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(GeneralSettings generalSettings) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SETTINGS_KEY, generalSettings.getConfigKey());
        values.put(COLUMN_SETTINGS_VALUE, generalSettings.getValue());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_GENERAL_SETTINGS, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the General Settings from the Master via Web Service
     */
    public void insertingSettingsMaster(ArrayList<GeneralSettings> generalSettingsArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (generalSettingsArrayList != null) {
                for (int i = 0; i < generalSettingsArrayList.size(); i++) {
                    values.put(COLUMN_SETTINGS_KEY, generalSettingsArrayList.get(i).getConfigKey());
                    values.put(COLUMN_SETTINGS_VALUE, generalSettingsArrayList.get(i).getValue());

                    db.insert(TABLE_GENERAL_SETTINGS, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }

    }

    public GeneralSettings getSettingByKey(String configKey) {
        GeneralSettings generalSetting = null;
        try {
            SQLiteDatabase sqlWrite = this.getWritableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_GENERAL_SETTINGS + " WHERE " + COLUMN_SETTINGS_KEY + "=?";
            Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{configKey});
            Log.d("Data", "Get Configuration  ");

            while (cursor.moveToNext()) {
                generalSetting = new GeneralSettings();
                generalSetting.setSettingsId(cursor.getInt(0));
                generalSetting.setConfigKey(cursor.getString(1));
                generalSetting.setValue(cursor.getString(2));
            }
            cursor.close();
            sqlWrite.close();
        } catch (Exception e) {
            Log.d("Exception--", "Error--" + e.getMessage());
        }
        return generalSetting;
    }

   /* public ArrayList<GeneralSettings> getSettingsByKey(String configKey) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_GENERAL_SETTINGS +" WHERE "+COLUMN_SETTINGS_KEY+"=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {configKey} );
        Log.d("Data","Get Configuration  " );
        ArrayList<GeneralSettings> generalSettingsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            GeneralSettings generalSettings = new GeneralSettings();
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            generalSettings.setSettingsId(cursor.getInt(0));
            generalSettings.setConfigKey(cursor.getString(1));
            generalSettings.setValue(cursor.getString(2));

            generalSettingsArrayList.add(generalSettings);
        }
        cursor.close();
        sqlWrite.close();
        return generalSettingsArrayList;
    }*/

    public void deleteGeneralSettings() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GENERAL_SETTINGS, null, null);
        db.close();
    }

}
