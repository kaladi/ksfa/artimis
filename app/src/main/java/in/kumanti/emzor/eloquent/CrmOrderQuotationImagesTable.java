package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import in.kumanti.emzor.model.CrmOrderQuotationImages;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_ORDER_QUOTE_IMAGES;

public class CrmOrderQuotationImagesTable extends SQLiteOpenHelper {

    private static final String COLUMN_QUOTE_IMAGE_ID = "quoteImageId";
    private static final String COLUMN_TAB_ORDER_ID = "tabOrderId";
    private static final String COLUMN_QUOTE_SEQ = "quoteSeqNo";
    private static final String COLUMN_QUOTE_IMAGE_NAME = "quoteImageName";
    private static final String COLUMN_QUOTE_IMAGE_PATH = "quoteImagePath";
    private static final String COLUMN_QUOTE_IMAGE_IS_SYNC = "isSync";
    private static final String COLUMN_QUOTE_IMAGE_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_ORDER_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_ORDER_PRODUCT_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmOrderQuotationImagesTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_ORDER_QUOTE_IMAGES);
        String opportunityTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_ORDER_QUOTE_IMAGES + "(" +
                COLUMN_QUOTE_IMAGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_TAB_ORDER_ID + " INTEGER, " +
                COLUMN_QUOTE_SEQ + " TEXT, " +
                COLUMN_QUOTE_IMAGE_NAME + " REAL, " +
                COLUMN_QUOTE_IMAGE_PATH + " TEXT, " +
                COLUMN_QUOTE_IMAGE_IS_SYNC + " TEXT, " +
                COLUMN_QUOTE_IMAGE_SYNC_DATE + " TEXT, " +
                COLUMN_ORDER_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_ORDER_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(opportunityTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmOrderQuotationImages quotationImages) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TAB_ORDER_ID, quotationImages.getTabOrderId());
        values.put(COLUMN_QUOTE_SEQ, quotationImages.getQuoteSeqNo());
        values.put(COLUMN_QUOTE_IMAGE_NAME, quotationImages.getQuoteImageName());
        values.put(COLUMN_QUOTE_IMAGE_PATH, quotationImages.getQuoteImagePath());
        values.put(COLUMN_QUOTE_IMAGE_IS_SYNC, quotationImages.isSync());
        values.put(COLUMN_QUOTE_IMAGE_SYNC_DATE, quotationImages.getLatestSyncDate());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_ORDER_QUOTE_IMAGES, null, values);
        db.close();
        return id;
    }


}
