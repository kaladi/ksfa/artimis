package in.kumanti.emzor.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class FeedbackActivity extends MainActivity implements AdapterView.OnItemSelectedListener {

    MyDBHandler dbHandler;
    TextView actionbarTitle, feedback_date, marqueeText, checkInTime, customerName, city, accountNo;
    ImageView info, btn_back, btnConfirm, customerImage;
    String login_id, checkin_time, customer_id, save_f_fate, feedback_type;
    EditText customer_message, customer_action;
    Spinner type;
    Chronometer duration;
    Button btnHistory;
    String feedback_code;
    int random_num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_feedback);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");

        marqueeText.setText(marquee_txt);
        actionbarTitle.setText(R.string.feedback_actionbar_title);

        setFeedbackDate();

        type.setOnItemSelectedListener(this);
        loadSpinnerType();

        btn_back.setOnClickListener(v -> finish());

        info.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

        btnHistory.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), FeedbackHistoryActivity.class);
            intent.putExtra("login_id", login_id);
            intent.putExtra("customer_id", customer_id);
            intent.putExtra("feedback_type", "History");

            startActivity(intent);

        });


        btnConfirm.setOnClickListener(v -> {

            if ("Select".equals(feedback_type)) {
                Toast.makeText(getApplicationContext(), "Please select Type", Toast.LENGTH_SHORT).show();
            } else if ("".equals(customer_message.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Please Enter Feedback", Toast.LENGTH_SHORT).show();
            } else {

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                dbHandler.create_feedback(null,
                        login_id,
                        null,
                        save_f_fate,
                        feedback_type,
                        customer_id,
                        customer_message.getText().toString(),
                        customer_action.getText().toString(),
                        random_num,
                        feedback_code,
                        visitSequence,"Open" );

                Toast.makeText(getApplicationContext(), "Feedback Submitted", Toast.LENGTH_SHORT).show();
                loadSpinnerType();
                customer_message.setText("");
                customer_action.setText("");
                generate_feedback_code();

            }
        });

    }

    private void setFeedbackDate() {
        long date = System.currentTimeMillis();
        SimpleDateFormat sdfd = new SimpleDateFormat("dd MMM yy");
        String f_fate = sdfd.format(date);
        feedback_date.setText(f_fate);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        save_f_fate = timeStampFormat.format(myDate);

    }

    private void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");

        }

        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();
        checkInTime.setText(checkin_time);

        generate_feedback_code();

        //Populate Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }


    }


    private void generate_feedback_code() {
        random_num = dbHandler.get_feedback_id();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String feedback_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        if (random_num <= 9) {
            feedback_code = tab_prefix + "FB" + feedback_seq_date + "00" + random_num;

        } else if (random_num > 9 & random_num < 99) {
            feedback_code = tab_prefix + "FB" + feedback_seq_date + "0" + random_num;

        } else {
            feedback_code = tab_prefix + "FB" + feedback_seq_date + random_num;
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();


    }

    private void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        btn_back = findViewById(R.id.back);
        info = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);


        btnConfirm = findViewById(R.id.btnConfirm);
        btnHistory = findViewById(R.id.btnHistory);
        customer_message = findViewById(R.id.customer_message);
        customer_action = findViewById(R.id.customer_action);
        feedback_date = findViewById(R.id.feedback_date);
        type = findViewById(R.id.type);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);


    }

    @Override
    public void onBackPressed() {

    }


    public void loadSpinnerType() {

        String[] labels = {"Select", "BDE", "General", "Product Quality", "Price", "Promotion", "Stock Availability"};

        type.setAdapter(new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                labels));

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        feedback_type = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
