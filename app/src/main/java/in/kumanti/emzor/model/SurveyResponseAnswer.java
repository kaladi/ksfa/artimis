package in.kumanti.emzor.model;

public class SurveyResponseAnswer {

    public String tabCode;
    public String srCode;
    public String companyCode;
    public int id;
    public String surveyCode;
    public String surveyResponseId;
    public String surveyQuestionId;
    public String optionValue;
    public String optionsValuesIds;
    public String createdAt;
    public String updatedAt;
    public String visitSequenceNumber;

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurvey_id() {
        return surveyCode;
    }

    public void setSurvey_id(String survey_id) {
        this.surveyCode = survey_id;
    }

    public String getOption_value() {
        return optionValue;
    }

    public void setOption_value(String option_value) {
        this.optionValue = option_value;
    }

    public String getSurvey_question_id() {
        return surveyQuestionId;
    }

    public void setSurvey_question_id(String survey_question_id) {
        this.surveyQuestionId = survey_question_id;
    }

    public String getSurvey_response_answer_id() {
        return surveyResponseId;
    }

    public void setSurvey_response_answer_id(String survey_response_answer_id) {
        this.surveyResponseId = survey_response_answer_id;
    }

    public String getOptionsValuesIds() {
        return optionsValuesIds;
    }

    public void setOptionsValuesIds(String optionsValuesIds) {
        this.optionsValuesIds = optionsValuesIds;
    }

    public String getCreated_at() {
        return createdAt;
    }

    public void setCreated_at(String created_at) {
        this.createdAt = created_at;
    }


    public String getUpdated_at() {
        return updatedAt;
    }

    public void setUpdated_at(String updated_at) {
        this.updatedAt = updated_at;
    }


}
