package in.kumanti.emzor.model;

public class CollectionReports {

    private String date;
    private String customer_name;
    private String receipt_num;
    private String value;
    private String invoice_num;
    private String invoice_amount;
    private String due_amount;
    private String mode;
    private String bank;
    private String branch;
    private String transactionType;
    private String status;


    public CollectionReports(String date1, String customer_name1, String receipt_num1, String value1, String invoice_num1, String invoice_amount1, String due_amount1, String mode1, String bank1, String branch1, String transactionsType,String status1) {

        date = date1;
        customer_name = customer_name1;
        receipt_num = receipt_num1;
        value = value1;
        invoice_num = invoice_num1;
        invoice_amount = invoice_amount1;
        due_amount = due_amount1;
        mode = mode1;
        bank = bank1;
        branch = branch1;
        transactionType = transactionsType;
        status = status1;

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date1) {
        date = date1;
    }


    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name1) {
        customer_name = customer_name1;
    }

    public String getReceipt_num() {
        return receipt_num;
    }

    public void setReceipt_num(String receipt_num1) {
        receipt_num = receipt_num1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value1) {
        value = value1;
    }

    public String getInvoice_num() {
        return invoice_num;
    }

    public void setInvoice_num(String invoice_num1) {
        invoice_num = invoice_num1;
    }

    public String getInvoice_amount() {
        return invoice_amount;
    }

    public void setInvoice_amount(String invoice_amount1) {
        invoice_amount = invoice_amount1;
    }

    public String getDue_amount() {
        return due_amount;
    }

    public void setDue_amount(String due_amount1) {
        due_amount = due_amount1;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode1) {
        mode = mode1;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status1) {
        status = status1;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank1) {
        bank = bank1;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch1) {
        branch = branch1;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

}

