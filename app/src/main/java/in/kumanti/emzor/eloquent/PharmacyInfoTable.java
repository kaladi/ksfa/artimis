package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.PharmacyInfo;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_PHARMACY_INFO;

public class PharmacyInfoTable extends SQLiteOpenHelper {

    //Columns used for Pharmacy Information details based upon the type of pharmacy
    private static final String COLUMN_PHARMACY_INFO_ID = "id";
    private static final String COLUMN_SR_CODE = "sr_code";
    private static final String COLUMN_CUSTOMER_CODE = "customer_code";
    private static final String COLUMN_PHARMACY_TYPE = "pharmacy_type";
    private static final String COLUMN_PHARMACY_NAME = "pharmacy_name";
    private static final String COLUMN_PHARMACY_DATE = "pharmacy_date";
    private static final String COLUMN_PHARMACY_CONTACT_NAME = "contact_name";
    private static final String COLUMN_PHARMACY_CONTACT_NUMBER = "contact_number";
    private static final String COLUMN_PHARMACY_INFO_CREATED_AT = "created_at";
    private static final String COLUMN_PHARMACY_INFO_UPDATED_AT = "updated_at";
    private final static String COLUMN_PHARMACY_INFO_SEQUENCE_NO = "visitSeqNo";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public PharmacyInfoTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_PHARMACY_INFO);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_PHARMACY_INFO + "(" +
                COLUMN_PHARMACY_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SR_CODE + " TEXT, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_PHARMACY_TYPE + " TEXT, " +
                COLUMN_PHARMACY_NAME + " TEXT, " +
                COLUMN_PHARMACY_DATE + " TEXT, " +
                COLUMN_PHARMACY_CONTACT_NAME + " TEXT, " +
                COLUMN_PHARMACY_CONTACT_NUMBER + " TEXT, " +
                COLUMN_PHARMACY_INFO_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_PHARMACY_INFO_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_PHARMACY_INFO_SEQUENCE_NO + " TEXT " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(PharmacyInfo pharmacyInfo) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SR_CODE, pharmacyInfo.getSrCode());
        values.put(COLUMN_CUSTOMER_CODE, pharmacyInfo.getCustomerCode());
        values.put(COLUMN_PHARMACY_TYPE, pharmacyInfo.getPharmacyType());
        values.put(COLUMN_PHARMACY_NAME, pharmacyInfo.getPharmacyName());
        values.put(COLUMN_PHARMACY_DATE, pharmacyInfo.getPharmacyDate());
        values.put(COLUMN_PHARMACY_CONTACT_NAME, pharmacyInfo.getContactName());
        values.put(COLUMN_PHARMACY_CONTACT_NUMBER, pharmacyInfo.getContactNumber());
        values.put(COLUMN_PHARMACY_INFO_SEQUENCE_NO, pharmacyInfo.getVisitSequenceNumber());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PHARMACY_INFO, null, values);
        db.close();
        return id;
    }

    public ArrayList<PharmacyInfo> getPharmacyInfo(String pharmacyType, String customerCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PHARMACY_INFO + " WHERE " + COLUMN_PHARMACY_TYPE + "=? AND " + COLUMN_CUSTOMER_CODE + "=? ORDER BY id asc";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{pharmacyType, customerCode});
        Log.d("Data", "Get Products  ");
        ArrayList<PharmacyInfo> pharmacyInfos = new ArrayList<>();

        while (cursor.moveToNext()) {
            PharmacyInfo product = new PharmacyInfo();
            Log.d("PPPPP", "Data  id" + cursor.getInt(0));
            product.setId(cursor.getInt(0));
            product.setSrCode(cursor.getString(1));
            product.setCustomerCode(cursor.getString(2));
            product.setPharmacyType(cursor.getString(3));
            product.setPharmacyName(cursor.getString(4));
            product.setPharmacyDate(cursor.getString(5));
            product.setContactName(cursor.getString(6));
            product.setContactNumber(cursor.getString(7));
            pharmacyInfos.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return pharmacyInfos;
    }

    public ArrayList<PharmacyInfo> getPharmacyInfo() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_PHARMACY_INFO;

        ArrayList<PharmacyInfo> pharmacyInfoArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            PharmacyInfo pharmacyInfo = new PharmacyInfo();
            pharmacyInfo.setTabCode(Constants.TAB_CODE);
            pharmacyInfo.setCompanyCode(Constants.COMPANY_CODE);
            pharmacyInfo.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            pharmacyInfo.setId(id);
            pharmacyInfo.setCustomerCode(cursor.getString(2));
            pharmacyInfo.setPharmacyType(cursor.getString(3));
            pharmacyInfo.setPharmacyName(cursor.getString(4));
            pharmacyInfo.setPharmacyDate(cursor.getString(5));
            pharmacyInfo.setContactName(cursor.getString(6));
            pharmacyInfo.setContactNumber(cursor.getString(7));
            pharmacyInfo.setCreatedAt(cursor.getString(8));
            pharmacyInfo.setUpdatedAt(cursor.getString(9));
            pharmacyInfo.setVisitSequenceNumber(cursor.getString(10));


            pharmacyInfoArrayList.add(pharmacyInfo);
        }
        cursor.close();
        sqlWrite.close();
        return pharmacyInfoArrayList;
    }


    public boolean checkPharmacyNameIsExist(String pharmacyName) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_PHARMACY_INFO + " WHERE " + COLUMN_PHARMACY_NAME + "=?";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{pharmacyName});
        while (cursor.moveToNext()) {
            cursor.close();
            sqlWrite.close();
            return true;
        }
        cursor.close();
        sqlWrite.close();
        return false;
    }

}
