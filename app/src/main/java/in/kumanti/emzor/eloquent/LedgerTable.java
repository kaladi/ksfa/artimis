package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.model.Ledger;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_LEDGER;

public class LedgerTable extends SQLiteOpenHelper {

    private static final String COLUMN_LEDGER_ID = "id";
    private static final String COLUMN_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_INVOICE_NUMBER = "invoice_number";
    private static final String COLUMN_INVOICE_DATE = "invoice_date";
    private static final String COLUMN_INVOICE_AMOUNT = "invoice_amount";
    private static final String COLUMN_RECEIPT_AMOUNT = "receipt_amount";
    private static final String COLUMN_OUTSTANDING_AMOUNT = "outstanding_amount";
    private static final String COLUMN_LEDGER_CREATED_AT = "created_at";
    private static final String COLUMN_LEDGER_UPDATED_AT = "updated_at";

    public Context context;
    String selectQuery;
    Cursor cursor;
    private SQLiteDatabase sqlWrite;

    public LedgerTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_LEDGER);

        String surveyTable = "CREATE TABLE IF NOT EXISTS " + TABLE_LEDGER + "(" +
                COLUMN_LEDGER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_ID + " TEXT, " +
                COLUMN_INVOICE_NUMBER + " TEXT, " +
                COLUMN_INVOICE_DATE + " TEXT, " +
                COLUMN_INVOICE_AMOUNT + " TEXT, " +
                COLUMN_RECEIPT_AMOUNT + " TEXT, " +
                COLUMN_OUTSTANDING_AMOUNT + " TEXT, " +
                COLUMN_LEDGER_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_LEDGER_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(surveyTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public ArrayList<Ledger> getLedgerArrayList(String customer_id, String customer_type) {
        sqlWrite = this.getWritableDatabase();

        if ("Primary".equals(customer_type)) {
            selectQuery = "SELECT id,customer_id,invoice_number,invoice_date,invoice_amount,receipt_amount,outstanding_amount,'Invoice' as type,invoice_number FROM " + TABLE_LEDGER + " WHERE " + COLUMN_CUSTOMER_ID + "=?";
            cursor = sqlWrite.rawQuery(selectQuery, new String[]{customer_id});
        } else {
            // selectQuery = "SELECT null,a.customer_id,a.invoice_num,(select b.invoice_date from xxmsales_invoice_header b where b.invoice_number=a.invoice_num) invoice_date,(select b.invoice_value from xxmsales_invoice_header b where b.invoice_number=a.invoice_num) invoice_value,sum(a.payment_amount),((select b.invoice_value from xxmsales_invoice_header b where b.invoice_number=a.invoice_num) - sum(a.payment_amount) ) balance_amount FROM xxmsales_collection_details a where collection_type='Against Invoice' and customer_id='" + customer_id + "' group by a.invoice_num UNION select null,customer_id,collection_number,collection_date,invoice_value,pay1,pay2 from (select null,a.customer_id,a.collection_number,a.collection_date,null invoice_value,sum(a.payment_amount) pay1,sum(a.payment_amount)*-1 pay2 FROM xxmsales_collection_details a where collection_type='onAccount' and customer_id='" + customer_id + "') where pay1 > 0";
            //selectQuery = "SELECT NULL, a.customer_id, a.invoice_num, (SELECT b.invoice_date FROM   xxmsales_invoice_header b WHERE  b.invoice_number = a.invoice_num) invoice_date, (SELECT b.invoice_value FROM   xxmsales_invoice_header b WHERE  b.invoice_number = a.invoice_num) invoice_value, Sum(a.payment_amount), ( (SELECT b.invoice_value FROM   xxmsales_invoice_header b WHERE  b.invoice_number = a.invoice_num) - Sum(a.payment_amount) )balance_amount, transactiontype FROM   xxmsales_collection_details a WHERE  collection_type = 'Against Invoice' AND customer_id = '" + customer_id + "' GROUP  BY a.invoice_num UNION SELECT NULL, customer_id, collection_number, collection_date, invoice_value, pay1, pay2, transactiontype FROM   (SELECT NULL, a.customer_id, a.collection_number, a.collection_date, NULL                       invoice_value, Sum(a.payment_amount)      pay1, Sum(a.payment_amount) *- 1 pay2, transactiontype FROM   xxmsales_collection_details a WHERE  collection_type = 'onAccount'AND customer_id = '" + customer_id + "') WHERE  pay1 > 0 ";
            //selectQuery = "select null,customer_id,document_number,document_date,total_amount,payment_amount,ifnull(total_amount-payment_amount,0) balance_amount, transaction_type from (select a.customer_id,a.invoice_number document_number,a.invoice_date document_date,ifnull(sum(b.value),0) total_amount,ifnull((select sum(payment_amount) from xxmsales_collection_details c where c.invoice_num=a.invoice_number),0) payment_amount,'Invoice' transaction_type from xxmsales_invoice_header a,xxmsales_invoice_details b where a.customer_id='" + customer_id + "'" + " and a.invoice_number=b.invoice_number AND a.invoice_status='Completed' GROUP by a.invoice_number UNION select a.customer_id,a.collection_number document_number,a.collection_date document_date,0, ifnull(a.payment_amount,0),transactiontype  from xxmsales_collection_details a where a.customer_id='" + customer_id + "'" + " and a.collection_type = 'onAccount' or payment_mode='Credit' ) ORDER by document_date,document_number DESC";
            //selectQuery = "select null,customer_id,document_number,document_date,total_amount,payment_amount,ifnull(total_amount-payment_amount,0) balance_amount, transaction_type from (select a.customer_id,a.invoice_number document_number,a.invoice_date document_date,ifnull(sum(b.value),0) total_amount,ifnull((select sum(payment_amount) from xxmsales_collection_details c where c.invoice_num=a.invoice_number and c.transactionType='Invoice'),0) payment_amount,'Invoice' transaction_type from xxmsales_invoice_header a,xxmsales_invoice_details b where a.customer_id='" + customer_id + "'" + " and a.invoice_number=b.invoice_number AND a.invoice_status='Completed' GROUP by a.invoice_number UNION select a.customer_id,a.collection_number document_number,a.collection_date document_date,0, ifnull(a.payment_amount,0),transactiontype  from xxmsales_collection_details a where a.customer_id='" + customer_id + "'" + " and a.transactionType!='Invoice') ORDER by document_date,document_number DESC";
            selectQuery = "select null,a.customer_id,case when (a.transactionType=='Invoice' and a.invoice_num!='') then a.invoice_num when a.transactionType=='Return' then b.customerReturnNum else a.collection_number end as document_number,a.collection_date document_date,case when a.transactionType=='Invoice' then a.invoice_amount else 0 end as total_amount, ifnull(a.payment_amount,0),case when a.transactionType=='Return' and (ifnull(a.invoice_amount-a.payment_amount,0))<>0 then (a.invoice_amount-a.payment_amount)*-1 when a.transactionType=='Collection' and a.collection_type = 'Against Invoice' then (a.invoice_amount-(select sum(case when c.transactionType=='Return' then 0 else c.payment_amount end) as amnt from xxmsales_collection_details c where c.invoice_num=a.invoice_num and c.created_at < a.created_at and c.collection_type='Against Invoice' group by c.invoice_num)-a.payment_amount) when a.transactionType=='Collection' and a.invoice_num ='' and a.payment_mode='Cash' then a.payment_amount*-1 else ifnull(a.invoice_amount-a.payment_amount,0) end balance_amount,a.transactiontype,a.invoice_num  from xxmsales_collection_details a left join xxmsales_customer_return b on a.invoice_num = b.invoiceNumber where a.customer_id='" + customer_id + "' and (a.collection_type='Against Invoice' OR ((a.collection_type='On Account' OR a.collection_type='onAccount') and a.invoice_num='' and a.payment_mode='Cash'))";
            cursor = sqlWrite.rawQuery(selectQuery, null);
        }
        Log.d("Data", "Get Ledger Lists  ");
        ArrayList<Ledger> ledgerList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Ledger ledger = new Ledger();
            int id = cursor.getInt(0);
            ledger.setId(id);
            ledger.setCustomerId(cursor.getString(1));
            ledger.setDocumentNumber(cursor.getString(2));
            String sDate1 = cursor.getString(3);

            Log.d("LedgerTable***", "DB Date---" +sDate1);


            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                ledger.setInvoiceDate(dateString);
                Log.d("LedgerTable***", "Formatted Date---" +date1.toString());
            }
            catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }

            ledger.setInvoiceAmount(cursor.getString(4));
            ledger.setReceiptAmount(cursor.getString(5));

            ledger.setOutstandingAmount(cursor.getString(6));
            if ("Primary".equals(customer_type))
                ledger.setTransactionType("Invoice");
            else
                ledger.setTransactionType(cursor.getString(7));
            ledger.setInvoiceNumber(cursor.getString(8));
            ledgerList.add(ledger);

        }
        cursor.close();
        sqlWrite.close();
        return ledgerList;
    }

    public ArrayList<Ledger> getLedgerArrayByDataAgesList(String customer_id, String ageCount) {
        sqlWrite = this.getWritableDatabase();

        selectQuery = "select null,a.customer_id,case when a.transactionType=='Invoice' and a.invoice_num!='' then a.invoice_num when a.transactionType=='Return' then b.customerReturnNum else a.collection_number end as document_number,a.collection_date document_date,case when a.transactionType=='Invoice' then a.invoice_amount else 0 end as total_amount, ifnull(a.payment_amount,0),case when a.transactionType=='Return' and (ifnull(a.invoice_amount-a.payment_amount,0))<>0 then (a.invoice_amount-a.payment_amount)*-1 when a.transactionType=='Collection' and a.collection_type = 'Against Invoice' then (a.invoice_amount-(select sum(case when c.transactionType=='Return' then 0 else c.payment_amount end) as amnt from xxmsales_collection_details c where c.invoice_num=a.invoice_num and c.created_at < a.created_at and c.collection_type='Against Invoice' group by c.invoice_num)-a.payment_amount) when a.transactionType=='Collection' and a.invoice_num ='' and a.payment_mode='Cash' then a.payment_amount*-1 else ifnull(a.invoice_amount-a.payment_amount,0) end balance_amount,a.transactiontype,a.invoice_num  from xxmsales_collection_details a left join xxmsales_customer_return b on a.invoice_num = b.invoiceNumber where a.customer_id='" + customer_id + "' and (a.collection_type='Against Invoice' OR ((a.collection_type='On Account' OR a.collection_type='onAccount') and a.invoice_num='' and a.payment_mode='Cash')) ";
        if(ageCount.equals("30"))
        {
            selectQuery += " AND a.collection_date >= Date( Julianday(Date('now')) - 30)";
        }
        else if(ageCount.equals("60"))
        {
            selectQuery += " AND a.collection_date <  date( julianday(date('now')) - 30) AND a.collection_date >= date( julianday(date('now')) - 60)";
        }
        else if(ageCount.equals("90"))
        {
            selectQuery += " AND a.collection_date <  date( julianday(date('now')) - 60) AND a.collection_date >= date( julianday(date('now')) - 90)";
        }
        else if(ageCount.equals("90+"))
        {
            selectQuery += " AND a.collection_date < date( julianday(date('now')) - 90)";
        }

        cursor = sqlWrite.rawQuery(selectQuery, null);
        Log.d("Data", "Get Ledger Lists  ");
        ArrayList<Ledger> ledgerList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Ledger ledger = new Ledger();
            int id = cursor.getInt(0);
            ledger.setId(id);
            ledger.setCustomerId(cursor.getString(1));
            ledger.setDocumentNumber(cursor.getString(2));
            String sDate1 = cursor.getString(3);
            try {
                @SuppressLint("SimpleDateFormat") Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                ledger.setInvoiceDate(dateString);
            } catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }

            ledger.setInvoiceAmount(cursor.getString(4));
            ledger.setReceiptAmount(cursor.getString(5));

            ledger.setOutstandingAmount(cursor.getString(6));
            ledger.setTransactionType(cursor.getString(7));
            ledger.setInvoiceNumber(cursor.getString(8));
            ledgerList.add(ledger);

        }
        cursor.close();
        sqlWrite.close();
        return ledgerList;
    }
    public ArrayList<Ledger> getSecondaryInvoiceLedgerArrayList(String customer_id, String invoiceNumber) {
        sqlWrite = this.getWritableDatabase();

        //selectQuery = "select null,a.customer_id,case when a.transactionType=='Invoice' then a.invoice_num when a.transactionType=='Return' then b.customerReturnNum else a.collection_number end as document_number,a.collection_date document_date,case when a.transactionType=='Invoice' then a.invoice_amount else 0 end as total_amount, ifnull(a.payment_amount,0),case when a.transactionType=='Return' and (ifnull(a.invoice_amount-a.payment_amount,0))<>0 then (a.invoice_amount-a.payment_amount)*-1 when a.transactionType=='Collection' then (a.invoice_amount-(select sum(case when c.transactionType=='Return' then 0 else c.payment_amount end) as amnt from xxmsales_collection_details c where c.invoice_num=a.invoice_num and c.created_at < a.created_at and c.collection_type='Against Invoice' group by c.invoice_num)-a.payment_amount) else ifnull(a.invoice_amount-a.payment_amount,0) end balance_amount,a.transactiontype,a.invoice_num  from xxmsales_collection_details a left join xxmsales_customer_return b on a.invoice_num = b.invoiceNumber where a.customer_id='" + customer_id + "' and a.invoice_num='"+ invoiceNumber+"' and collection_type='Against Invoice'";
        selectQuery = "select null,a.customer_id,case when (a.transactionType=='Invoice' and a.invoice_num!='') then a.invoice_num when a.transactionType=='Return' then b.customerReturnNum else a.collection_number end as document_number,a.collection_date document_date,case when a.transactionType=='Invoice' then a.invoice_amount else 0 end as total_amount, ifnull(a.payment_amount,0),case when a.transactionType=='Return' and (ifnull(a.invoice_amount-a.payment_amount,0))<>0 then (a.invoice_amount-a.payment_amount)*-1 when a.transactionType=='Collection' and a.collection_type = 'Against Invoice' then (a.invoice_amount-(select sum(case when c.transactionType=='Return' then 0 else c.payment_amount end) as amnt from xxmsales_collection_details c where c.invoice_num=a.invoice_num and c.created_at < a.created_at and c.collection_type='Against Invoice' group by c.invoice_num)-a.payment_amount) when a.transactionType=='Collection' and a.invoice_num ='' and a.payment_mode='Cash' then a.payment_amount*-1 else ifnull(a.invoice_amount-a.payment_amount,0) end balance_amount,a.transactiontype,a.invoice_num  from xxmsales_collection_details a left join xxmsales_customer_return b on a.invoice_num = b.invoiceNumber where a.customer_id='" + customer_id + "' and ((a.collection_type='Against Invoice' and a.invoice_num='"+ invoiceNumber+"') OR ((a.collection_type='On Account' OR a.collection_type='onAccount') and a.invoice_num='' and a.payment_mode='Cash'))";
        cursor = sqlWrite.rawQuery(selectQuery, null);
        Log.d("Data", "Get Ledger Lists  ");
        ArrayList<Ledger> ledgerList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Ledger ledger = new Ledger();
            int id = cursor.getInt(0);
            ledger.setId(id);
            ledger.setCustomerId(cursor.getString(1));
            ledger.setDocumentNumber(cursor.getString(2));
            String sDate1 = cursor.getString(3);
            try {
                @SuppressLint("SimpleDateFormat") Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                ledger.setInvoiceDate(dateString);
            } catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }

            ledger.setInvoiceAmount(cursor.getString(4));
            ledger.setReceiptAmount(cursor.getString(5));

            ledger.setOutstandingAmount(cursor.getString(6));

            ledger.setTransactionType(cursor.getString(7));
            ledger.setInvoiceNumber(cursor.getString(8));
            ledgerList.add(ledger);

        }
        cursor.close();
        sqlWrite.close();
        return ledgerList;
    }
    public long create(Ledger ledger) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_ID, ledger.customerId);
        values.put(COLUMN_INVOICE_NUMBER, ledger.invoiceNumber);
        values.put(COLUMN_INVOICE_DATE, ledger.invoiceDate);
        values.put(COLUMN_INVOICE_AMOUNT, ledger.invoiceAmount);
        values.put(COLUMN_RECEIPT_AMOUNT, ledger.receiptAmount);
        values.put(COLUMN_OUTSTANDING_AMOUNT, ledger.outstandingAmount);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_LEDGER, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Ledger Details List from the Master via Web Service
     */
    public void insertingLedgersMaster(ArrayList<Ledger> ledgerList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            if (ledgerList != null) {
                for (int i = 0; i < ledgerList.size(); i++) {
                    values.put(COLUMN_CUSTOMER_ID, ledgerList.get(i).getCustomerId());
                    values.put(COLUMN_INVOICE_NUMBER, ledgerList.get(i).getInvoiceNumber());
                    values.put(COLUMN_INVOICE_DATE, ledgerList.get(i).getInvoiceDate());
                    values.put(COLUMN_INVOICE_AMOUNT, ledgerList.get(i).getInvoiceAmount());
                    values.put(COLUMN_RECEIPT_AMOUNT, ledgerList.get(i).getReceiptAmount());
                    values.put(COLUMN_OUTSTANDING_AMOUNT, ledgerList.get(i).getOutstandingAmount());
                    db.insert(TABLE_LEDGER, null, values);
                }
                db.setTransactionSuccessful();

            }

        } finally {
            db.endTransaction();
        }
    }


    public void deleteLedger() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LEDGER, null, null);
        db.close();
    }


}
