package in.kumanti.emzor.adapter;

import android.view.View;

public interface RecyclerViewItemListener {
    void onClick(View view, int position);
}
