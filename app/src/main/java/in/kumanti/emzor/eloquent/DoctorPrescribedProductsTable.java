package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.DoctorPrescribedProducts;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_DOCTOR_PRESCRIBED_PRODUCTS;

public class DoctorPrescribedProductsTable extends SQLiteOpenHelper {

    private static final String COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_ID = "id";
    private static final String COLUMN_DOCTOR_CODE = "doctor_code";
    private static final String COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_CODE = "product_code";
    private static final String COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_NAME = "product_name";
    private static final String COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_TYPE = "product_type";
    private static final String COLUMN_DOCTOR_VISIT_PRODUCT_PRESCRIPTIONS_COUNT = "prescriptions_count";
    private static final String COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_CREATED_AT = "created_at";
    private static final String COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_UPDATED_AT = "updated_at";
    private final static String COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_SEQUENCE_NO = "visitSeqNo";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public DoctorPrescribedProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCTOR_PRESCRIBED_PRODUCTS);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_DOCTOR_PRESCRIBED_PRODUCTS + "(" +
                COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DOCTOR_CODE + " TEXT, " +
                COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_CODE + " TEXT, " +
                COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_NAME + " TEXT, " +
                COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_TYPE + " TEXT, " +
                COLUMN_DOCTOR_VISIT_PRODUCT_PRESCRIPTIONS_COUNT + " TEXT, " +
                COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_SEQUENCE_NO + " TEXT " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(DoctorPrescribedProducts prescriptionsProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_DOCTOR_CODE, prescriptionsProducts.getDoctorCode());
        values.put(COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_CODE, prescriptionsProducts.getProductCode());
        values.put(COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_NAME, prescriptionsProducts.getProductName());
        values.put(COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_TYPE, prescriptionsProducts.getProductType());
        values.put(COLUMN_DOCTOR_VISIT_PRODUCT_PRESCRIPTIONS_COUNT, prescriptionsProducts.getPrescriptionsCount());
        values.put(COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCTS_SEQUENCE_NO, prescriptionsProducts.getVisitSeqNo());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_DOCTOR_PRESCRIBED_PRODUCTS, null, values);
        db.close();
        return id;
    }

    public ArrayList<DoctorPrescribedProducts> getPharmacyInfoProducts(String doctorCode, String productType) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DOCTOR_PRESCRIBED_PRODUCTS + " WHERE " + COLUMN_DOCTOR_CODE + "=? AND " + COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_TYPE + "=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{doctorCode, productType});

        ArrayList<DoctorPrescribedProducts> prescriptionsProductsArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DoctorPrescribedProducts product = new DoctorPrescribedProducts();
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            product.setId(cursor.getInt(0));
            product.setDoctorCode(cursor.getString(1));
            product.setProductCode(cursor.getString(2));
            product.setProductName(cursor.getString(3));
            product.setProductType(cursor.getString(4));
            product.setPrescriptionsCount(cursor.getString(5));
            prescriptionsProductsArrayList.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return prescriptionsProductsArrayList;
    }


    public void deletePrescriptionProduct(String doctorCode, String productType) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_DOCTOR_PRESCRIBED_PRODUCTS, COLUMN_DOCTOR_CODE + "=? AND " + COLUMN_DOCTOR_VISIT_PRESCRIPTIONS_PRODUCT_TYPE + "=?", new String[]{doctorCode, productType});
            db.close();
        } catch (Exception e) {
            Log.d("DeleteProduct", "Error" + e.getMessage());
        }

    }

    public ArrayList<DoctorPrescribedProducts> getDoctorPrescribedProducts() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_DOCTOR_PRESCRIBED_PRODUCTS;

        ArrayList<DoctorPrescribedProducts> doctorPrescribedProductsArrayList = new ArrayList<>();

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {
            DoctorPrescribedProducts doctorPrescribedProducts = new DoctorPrescribedProducts();
            doctorPrescribedProducts.setTabCode(Constants.TAB_CODE);
            doctorPrescribedProducts.setCompanyCode(Constants.COMPANY_CODE);
            doctorPrescribedProducts.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            doctorPrescribedProducts.setId(id);
            doctorPrescribedProducts.setDoctorCode(cursor.getString(1));
            doctorPrescribedProducts.setProductCode(cursor.getString(2));
            doctorPrescribedProducts.setProductName(cursor.getString(3));
            doctorPrescribedProducts.setProductType(cursor.getString(4));
            doctorPrescribedProducts.setPrescriptionsCount(cursor.getString(5));

            doctorPrescribedProducts.setCreatedAt(cursor.getString(6));
            doctorPrescribedProducts.setUpdatedAt(cursor.getString(7));
            doctorPrescribedProducts.setVisitSeqNo(cursor.getString(8));

            doctorPrescribedProductsArrayList.add(doctorPrescribedProducts);
        }
        cursor.close();
        sqlWrite.close();
        return doctorPrescribedProductsArrayList;
    }


}
