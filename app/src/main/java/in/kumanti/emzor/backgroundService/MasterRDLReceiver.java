package in.kumanti.emzor.backgroundService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MasterRDLReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.codepath.example.servicesdemo.alarm";
    String login_id, company_code;

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        login_id = bundle.getString("login_id");
        company_code = bundle.getString("companyCode");

        Intent startServiceIntent = new Intent(context, MasterRDLService.class);
        startServiceIntent.putExtra("login_id", login_id);
        startServiceIntent.putExtra("companyCode", company_code);
        context.startService(startServiceIntent);


//        Intent i = new Intent(context, MasterRDLService.class);
//        i.putExtra("login_id", login_id);
//        i.putExtra("companyCode", company_code);
//
//        Log.d("PostingData", "Receiver:---Company:--" + company_code);
//
//
//        MasterRDLService.enqueueJobAction(context, "action.SOME_ACTION", login_id, company_code);
       /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(i);
        }
        else {
            context.startService(i);
        }*/
    }
}
