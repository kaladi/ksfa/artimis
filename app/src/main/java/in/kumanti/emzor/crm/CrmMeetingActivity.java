package in.kumanti.emzor.crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.adapter.CrmMeetingStatusAdapter;
import in.kumanti.emzor.eloquent.CrmMeetingsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CrmMeeting;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class CrmMeetingActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView actionbarTitle, marqueeText, customerName, accountNo, city, checkInTime;
    Chronometer duration;

    String customerCode = "", crmNoString = "", crmDateString ="", crmStatusString="", crmStatusDateString ="", storeCrmDate, marqueeTextString;

    SharedPreferenceManager sharedPreferenceManager;

    TextView crmNo, crmDate, crmStatus, crmStatusDate;
    ImageButton addRowImageButton;
    EditText description;
    ImageButton saveMeetingButton;

    ArrayList<CrmMeeting> meetingStatusArrayList = new ArrayList<>();
    RecyclerView meetingStatusRv;
    RecyclerView.Adapter meetingStatusAdapter;
    RecyclerView.LayoutManager meetingStatusLayoutManager;

    CrmMeetingsTable crmMeetingsTable;

    MyDBHandler dbHandler;

    GPSTracker gpsTracker;
    double latitudeDouble, longitudeDouble;
    String latitude, longitude, latitudeLongitude;
    DecimalFormat formatter;

   /* public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }
*/
    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_crm_meeting);

        initializeViews();
        populateHeaderDetails();
//        getCustomerLocation();
        setCrmDate();

        actionbarTitle.setText(R.string.crm_meeting_header);

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        crmNo.setText(crmNoString);
        crmStatus.setText(crmStatusString);
        crmStatusDate.setText(crmStatusDateString);

        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

        meetingStatusRv.setHasFixedSize(true);
        meetingStatusLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        meetingStatusRv.setLayoutManager(meetingStatusLayoutManager);

        try {
            meetingStatusArrayList = crmMeetingsTable.getCrmMeetings(customerCode,crmNoString);
            meetingStatusAdapter = new CrmMeetingStatusAdapter(meetingStatusArrayList);
            meetingStatusRv.setAdapter(meetingStatusAdapter);

        } catch (Exception e) {
            Log.d("QueryDataResponse", "" + e.getMessage());
        }

        addRowImageButton.setOnClickListener(v -> addMeetingStatusRow());

        saveMeetingButton.setOnClickListener(v -> {
            InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(
                    Context.INPUT_METHOD_SERVICE);

            View focusedView = getCurrentFocus();
            if (focusedView != null) {
                inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }


            if (validateInputs()) {
                Log.d("InputError", "Invalid Inputs");
                meetingStatusAdapter.notifyDataSetChanged();
            }
            else {
                for (CrmMeeting cm : meetingStatusArrayList) {
                    if(cm.getMeetingId()==0) {
                        CrmMeeting crmMeeting = new CrmMeeting();
                        crmMeeting.setCustomerCode(customerCode);
                        crmMeeting.setCrmNumber(crmNo.getText().toString());
                        crmMeeting.setCrmDate(storeCrmDate);
                        crmMeeting.setMeetingDate(cm.getMeetingDate());
                        crmMeeting.setMeetingType(cm.getMeetingType());
                        crmMeeting.setMeetingStatus(cm.getMeetingStatus());
                        crmMeeting.setRemarks(cm.getRemarks());
                        crmMeetingsTable.create(crmMeeting);
                    }
                }
                finish();
            }
        });
    }

    private void getCustomerLocation() {

        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitudeDouble = gpsTracker.getLatitude();
            longitudeDouble = gpsTracker.getLongitude();

            latitude = formatter.format(latitudeDouble);
            longitude = formatter.format(longitudeDouble);

            latitudeLongitude = latitude + "," + longitude;
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void addMeetingStatusRow() {
        if (validateInputs())
        {
            Log.d("InputError", "Invalid Inputs");
        } else {
          /*  View current = getCurrentFocus();
            hideSoftKeyboard(getApplicationContext(), current);
            current.clearFocus();*/

            View focusedView = getCurrentFocus();
            InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
//            inputManager.hideSoftInputFromWindow(focusedView.getApplicationWindowToken(), 0);
            if (focusedView != null) {
                focusedView.clearFocus();
            }
            CrmMeeting cm = new CrmMeeting();
            cm.setMeetingType("");
            meetingStatusArrayList.add(0,cm);
            meetingStatusAdapter = new CrmMeetingStatusAdapter(meetingStatusArrayList);
            meetingStatusRv.setAdapter(meetingStatusAdapter);
        }
        //meetingStatusAdapter.notifyDataSetChanged();
    }

    private boolean validateInputs() {
        boolean resetflag = false;
       /* View current = getCurrentFocus();
        hideSoftKeyboard(getApplicationContext(), current);
        current.clearFocus();*/
        View focusedView = getCurrentFocus();
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(
                Context.INPUT_METHOD_SERVICE);
//        inputManager.hideSoftInputFromWindow(focusedView.getApplicationWindowToken(), 0);
        if (focusedView != null) {
            focusedView.clearFocus();
        }
        for (CrmMeeting cm : meetingStatusArrayList) {

            if (TextUtils.isEmpty(cm.getMeetingDate())) {
                cm.setError_MeetingDate("Please enter value");
                resetflag = true;
            } else
                cm.setError_MeetingDate("");
            if (TextUtils.isEmpty(cm.getMeetingType())) {
                cm.setError_MeetingType("Please enter value");
                resetflag = true;
            } else
                cm.setError_MeetingType("");
            if (TextUtils.isEmpty(cm.getMeetingStatus())) {
                cm.setError_MeetingStatus("Please enter value");
                resetflag = true;
            } else
                cm.setError_MeetingStatus("");

            if (TextUtils.isEmpty(cm.getRemarks())) {
                cm.setError_Remarks("Please enter value");
                resetflag = true;
            } else
                cm.setError_Remarks("");
        }

        return resetflag;
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        crmNo = findViewById(R.id.crmMeetingCrmNoTv);
        crmDate = findViewById(R.id.crmMeetingCrmDateTv);

        crmStatus = findViewById(R.id.crmMeetingCrmStatus);
        crmStatusDate = findViewById(R.id.crmMeetingCrmStatusDateTv);

        addRowImageButton = findViewById(R.id.addRowImageButton);
        meetingStatusRv = findViewById(R.id.crmMeetingStatusInfoRv);
        saveMeetingButton = findViewById(R.id.crmMeetingSaveIb);

        crmMeetingsTable = new CrmMeetingsTable(getApplicationContext());


        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            customerCode = bundle.getString("customer_id");
            crmNoString = bundle.getString("crmNo");
            crmDateString = bundle.getString("crmDate");
            crmStatusString = bundle.getString("crmStatus");
            crmStatusDateString = bundle.getString("crmStatusDate");
        }

    }

    private void setCrmDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        crmDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        storeCrmDate = timeStampFormat.format(myDate);

    }


}
