package in.kumanti.emzor.model;


public class PrimaryInvoice {

    public int primary_invoice_id;
    public String warehouseCode;
    public String primaryInvoiceNumber;
    public String created_at;
    public String updated_at;


    public String getWarehouseId() {
        return warehouseCode;
    }

    public void setWarehouseId(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }


    public int getPrimary_invoice_id() {
        return primary_invoice_id;
    }

    public void setPrimary_invoice_id(int primary_invoice_id) {
        this.primary_invoice_id = primary_invoice_id;
    }


    public String getPrimary_invoice_number() {
        return primaryInvoiceNumber;
    }

    public void setPrimary_invoice_number(String primaryInvoiceNumber) {
        this.primaryInvoiceNumber = primaryInvoiceNumber;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public String toString() {
        return primaryInvoiceNumber;
    }
}
