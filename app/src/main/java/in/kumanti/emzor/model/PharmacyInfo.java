package in.kumanti.emzor.model;


import java.io.Serializable;

public class PharmacyInfo implements Serializable {

    public int id;
    public String srCode;
    public String customerCode;
    public String pharmacyType;
    public String pharmacyName;
    public String pharmacyDate;
    public String contactName;
    public String contactNumber;
    public String createdAt;
    public String updatedAt;
    public String tabCode;
    public String companyCode;
    private String visitSequenceNumber;

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getPharmacyType() {
        return pharmacyType;
    }

    public void setPharmacyType(String pharmacyType) {
        this.pharmacyType = pharmacyType;
    }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }

    public String getPharmacyDate() {
        return pharmacyDate;
    }

    public void setPharmacyDate(String pharmacyDate) {
        this.pharmacyDate = pharmacyDate;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    @Override
    public String toString() {
        return pharmacyName;
    }


}
