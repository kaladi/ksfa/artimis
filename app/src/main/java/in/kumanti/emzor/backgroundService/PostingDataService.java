package in.kumanti.emzor.backgroundService;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.CompetitorStockProductTable;
import in.kumanti.emzor.eloquent.CrmDetailsTable;
import in.kumanti.emzor.eloquent.CrmMeetingsTable;
import in.kumanti.emzor.eloquent.CrmNewProductsTable;
import in.kumanti.emzor.eloquent.CrmProductsTable;
import in.kumanti.emzor.eloquent.CrmStatusTable;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.CustomerReturnProductsTable;
import in.kumanti.emzor.eloquent.CustomerReturnTable;
import in.kumanti.emzor.eloquent.CustomerStockProductTable;
import in.kumanti.emzor.eloquent.DoctorPrescribedProductsTable;
import in.kumanti.emzor.eloquent.DoctorVisitInfoTable;
import in.kumanti.emzor.eloquent.ExpenseTable;
import in.kumanti.emzor.eloquent.LogInOutHistoryTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.NewJourneyPlanCustomerTable;
import in.kumanti.emzor.eloquent.NewJourneyPlanTable;
import in.kumanti.emzor.eloquent.NotesTable;
import in.kumanti.emzor.eloquent.PharmacyInfoProductsTable;
import in.kumanti.emzor.eloquent.PharmacyInfoTable;
import in.kumanti.emzor.eloquent.RDLSyncTable;
import in.kumanti.emzor.eloquent.StockIssueProductsTable;
import in.kumanti.emzor.eloquent.StockIssueTable;
import in.kumanti.emzor.eloquent.StockReturnProductsTable;
import in.kumanti.emzor.eloquent.StockReturnTable;
import in.kumanti.emzor.eloquent.SurveyResponseAnswerTable;
import in.kumanti.emzor.eloquent.SurveyResponseTable;
import in.kumanti.emzor.model.CheckInDetails;
import in.kumanti.emzor.model.CollectionDetails;
import in.kumanti.emzor.model.CompetitorStockProduct;
import in.kumanti.emzor.model.Crm;
import in.kumanti.emzor.model.CrmMeeting;
import in.kumanti.emzor.model.CrmNewProducts;
import in.kumanti.emzor.model.CrmProducts;
import in.kumanti.emzor.model.CrmStatus;
import in.kumanti.emzor.model.CustomerDetails;
import in.kumanti.emzor.model.CustomerReturn;
import in.kumanti.emzor.model.CustomerReturnProducts;
import in.kumanti.emzor.model.CustomerStockProduct;
import in.kumanti.emzor.model.Details;
import in.kumanti.emzor.model.DoctorPrescribedProducts;
import in.kumanti.emzor.model.DoctorVisitInfo;
import in.kumanti.emzor.model.DoctorVisitSponsorship;
import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.model.Feedback;
import in.kumanti.emzor.model.InvoiceItems;
import in.kumanti.emzor.model.Invoices;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.LogInOut;
import in.kumanti.emzor.model.NewJourneyPlan;
import in.kumanti.emzor.model.NewJourneyPlanCustomers;
import in.kumanti.emzor.model.Notes;
import in.kumanti.emzor.model.OrderItems;
import in.kumanti.emzor.model.Orders;
import in.kumanti.emzor.model.PharmacyInfo;
import in.kumanti.emzor.model.PharmacyInfoProducts;
import in.kumanti.emzor.model.RDLSync;
import in.kumanti.emzor.model.ResponseJson;
import in.kumanti.emzor.model.StockIssue;
import in.kumanti.emzor.model.StockIssueProducts;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.model.StockReturn;
import in.kumanti.emzor.model.StockReturnProducts;
import in.kumanti.emzor.model.SurveyResponse;
import in.kumanti.emzor.model.SurveyResponseAnswer;
import in.kumanti.emzor.model.TripDetails;
import in.kumanti.emzor.model.WaybillDetails;
import in.kumanti.emzor.utils.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.activity.MainActivity.PREF_NAME;

public class PostingDataService extends JobIntentService {

    String postSyncDate, postSyncTime;
    RDLSyncTable rdlSyncTable;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    public PostingDataService() {
    }

    // Helper Methods to start this JobIntentService.
    public static void enqueueJobAction(Context context, String action) {
        Intent intent = new Intent(context, PostingDataService.class);
        intent.setAction(Constants.MY_ACTION);

        enqueueWork(context, PostingDataService.class, Constants.MY_JOB_INTENT_SERVICE_ID, intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("NEw")
                    .setContentText("Background Upload Running").build();

            startForeground(1, notification);
        }


    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                postingData();

            }
        }).run();


    }

    private void postingData() {
        String login_id = sharedPreferences.getString("logInId", "");
        Gson gson = new GsonBuilder().setLenient().create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL).
                        client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the Gson ConverterFactory to directly convert json data to object
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        //Fetching the orders
        ArrayList<Orders> ordersArrayList = dbHandler.getOrdersService();
        JsonArray ordersJsonArray = gson.toJsonTree(ordersArrayList).getAsJsonArray();

        //Fetching the order items
        ArrayList<OrderItems> orderItemsArrayList = dbHandler.getOrderItemsService();
        JsonArray orderItemsJsonArray = gson.toJsonTree(orderItemsArrayList).getAsJsonArray();

        //Fetching the invoices
        ArrayList<Invoices> invoicesArrayList = dbHandler.getInvoicesService();
        JsonArray invoicesJsonArray = gson.toJsonTree(invoicesArrayList).getAsJsonArray();

        //Fetching the invoice items
        ArrayList<InvoiceItems> invoiceItemsArrayList = dbHandler.getInvoiceItemsService();
        JsonArray invoiceItemsJsonArray = gson.toJsonTree(invoiceItemsArrayList).getAsJsonArray();

        //Fetching Primary Collection Details
        ArrayList<CollectionDetails> primaryCollectionDetailsArrayList = dbHandler.getPrimaryCollectionsService();
        JsonArray primaryCollectionsJsonArray = gson.toJsonTree(primaryCollectionDetailsArrayList).getAsJsonArray();

        //Fetching Secondary Collections
        ArrayList<CollectionDetails> secondaryCollectionDetailsArrayList = dbHandler.getSecondaryCollectionsService();
        JsonArray secondaryCollectionsJsonArray = gson.toJsonTree(secondaryCollectionDetailsArrayList).getAsJsonArray();

        //Fetching Stock Receipts
        ArrayList<StockReceipt> stockReceiptArrayList = dbHandler.getStockReceiptService();
        JsonArray stockReceiptsJsonArray = gson.toJsonTree(stockReceiptArrayList).getAsJsonArray();

        //Fetching Stock Receipt Batches
        ArrayList<StockReceiptBatch> stockReceiptBatchArrayList = dbHandler.getStockReceiptBatchService();
        JsonArray stockReceiptBatchesJsonArray = gson.toJsonTree(stockReceiptBatchArrayList).getAsJsonArray();

        //Fetching New customer details
        ArrayList<CustomerDetails> customerDetailsArrayList = dbHandler.getCustomerDetailsService();
        JsonArray customerDetailsJsonArray = gson.toJsonTree(customerDetailsArrayList).getAsJsonArray();

        //Fetching Competitor Stock Products
        CompetitorStockProductTable pct = new CompetitorStockProductTable(getApplicationContext());
        ArrayList<CompetitorStockProduct> competitorStockProductArrayList = pct.getAllCompetitorStockProductsForSync();
        JsonArray competitorStockProductsJsonArray = gson.toJsonTree(competitorStockProductArrayList).getAsJsonArray();

        //Fetching Customer Stock Products
        CustomerStockProductTable sp = new CustomerStockProductTable(getApplicationContext());
        ArrayList<CustomerStockProduct> customerStockProductArrayList = sp.getAllCustomerStockProductForSync();
        JsonArray customerStockProductsJsonArray = gson.toJsonTree(customerStockProductArrayList).getAsJsonArray();


        //Fetching Survey Response
        SurveyResponseTable responseTable = new SurveyResponseTable(getApplicationContext());
        ArrayList<SurveyResponse> surveyResponseArrayList = responseTable.getAllSurveyResponseForSync();
        JsonArray surveyResponseJsonArray = gson.toJsonTree(surveyResponseArrayList).getAsJsonArray();

        //Fetching Survey Answer Response
        SurveyResponseAnswerTable responseAnswerTable = new SurveyResponseAnswerTable(getApplicationContext());
        ArrayList<SurveyResponseAnswer> surveyResponseAnswerArrayList = responseAnswerTable.getAllSurveyResponseAnswerForSync();
        JsonArray surveyResponseAnswerJsonArray = gson.toJsonTree(surveyResponseAnswerArrayList).getAsJsonArray();


        //Fetching Feedback data
        ArrayList<Feedback> feedbackArrayList = dbHandler.getFeedbackService();
        JsonArray feedbackJsonArray = gson.toJsonTree(feedbackArrayList).getAsJsonArray();

        //Fetching Trip Details data
        ArrayList<TripDetails> tripDetailsArrayList = dbHandler.getTripDetailsService();
        JsonArray tripDetailsJsonArray = gson.toJsonTree(tripDetailsArrayList).getAsJsonArray();

        //Fetching Check In data
        ArrayList<CheckInDetails> checkInDetailsArrayList = dbHandler.getCheckInDetailsService();
        JsonArray checkInDetailsJsonArray = gson.toJsonTree(checkInDetailsArrayList).getAsJsonArray();

        //Fetching Doctor Visit Info
        DoctorVisitInfoTable doctorVisitInfoTable = new DoctorVisitInfoTable(getApplicationContext());
        ArrayList<DoctorVisitInfo> doctorVisitInfoArrayList = doctorVisitInfoTable.getDoctorVisitInfo();
        JsonArray doctorVisitInfoJsonArray = gson.toJsonTree(doctorVisitInfoArrayList).getAsJsonArray();

        //Fetching Doctor Visit Prescribed Info
        DoctorPrescribedProductsTable doctorPrescribedProductsTable = new DoctorPrescribedProductsTable(getApplicationContext());
        ArrayList<DoctorPrescribedProducts> doctorPrescribedProductsArrayList = doctorPrescribedProductsTable.getDoctorPrescribedProducts();
        JsonArray doctorPrescribedProductsJsonArray = gson.toJsonTree(doctorPrescribedProductsArrayList).getAsJsonArray();

        //Fetching Pharmacy Info
        PharmacyInfoTable pharmacyInfoTable = new PharmacyInfoTable(getApplicationContext());
        ArrayList<PharmacyInfo> pharmacyInfoArrayList = pharmacyInfoTable.getPharmacyInfo();
        JsonArray pharmacyInfoArrayJsonArray = gson.toJsonTree(pharmacyInfoArrayList).getAsJsonArray();

        //Fetching Pharmacy Info Products
        PharmacyInfoProductsTable pharmacyInfoProductsTable = new PharmacyInfoProductsTable(getApplicationContext());
        ArrayList<PharmacyInfoProducts> pharmacyInfoProductsArrayList = pharmacyInfoProductsTable.getPharmacyInfoProducts();
        JsonArray pharmacyInfoProductsJsonArray = gson.toJsonTree(pharmacyInfoProductsArrayList).getAsJsonArray();

        //Fetching Check In data
        ArrayList<DoctorVisitSponsorship> doctorVisitSponsorshipArrayList = dbHandler.getDoctorVisitSponsorship();
        JsonArray doctorVisitSponsorshipJsonArray = gson.toJsonTree(doctorVisitSponsorshipArrayList).getAsJsonArray();

        //Fetching Notes
        NotesTable notesTable = new NotesTable(getApplicationContext());
        ArrayList<Notes> notesArrayList = notesTable.getNotesData();
        JsonArray notesJsonArray = gson.toJsonTree(notesArrayList).getAsJsonArray();

        //Fetching the wayBillDetails
        ArrayList<WaybillDetails> waybillDetailsArrayList = dbHandler.getWaybillDetails();
        JsonArray WaybillDetailsJsonArray = gson.toJsonTree(waybillDetailsArrayList).getAsJsonArray();

        //Fetching Stock Return
        StockReturnTable stockReturnTable = new StockReturnTable(getApplicationContext());
        ArrayList<StockReturn> stockReturnInfoArrayList = stockReturnTable.getStockReturnDetails();
        JsonArray stockReturnInfoJsonArray = gson.toJsonTree(stockReturnInfoArrayList).getAsJsonArray();

        //Fetching Stock Return Products
        StockReturnProductsTable stockReturnProductsTable = new StockReturnProductsTable(getApplicationContext());
        ArrayList<StockReturnProducts> stockReturnProductsInfoArrayList = stockReturnProductsTable.getStockReturnDetails();
        JsonArray stockReturnProductsInfoJsonArray = gson.toJsonTree(stockReturnProductsInfoArrayList).getAsJsonArray();

        //Fetching Stock Return Products
        LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
        ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLPOImageDetails();
        JsonArray lpoImagesInfoJsonArray = gson.toJsonTree(lpoImagesArrayList).getAsJsonArray();

        //Fetching Stock Return Products
        LogInOutHistoryTable logInOutHistoryTable = new LogInOutHistoryTable(getApplicationContext());
        ArrayList<LogInOut> logInOutArrayList = logInOutHistoryTable.getLoginOutHistory();
        JsonArray logInOutInfoJsonArray = gson.toJsonTree(logInOutArrayList).getAsJsonArray();


        //Fetching Customer Stock Return Products
        CustomerReturnTable customerReturnTable = new CustomerReturnTable(getApplicationContext());
        ArrayList<CustomerReturn> customerReturnArrayList = customerReturnTable.getCustomerReturnDetailsForSync();
        JsonArray customerReturnJsonArray = gson.toJsonTree(customerReturnArrayList).getAsJsonArray();


        //Fetching Customer Stock Return Products
        CustomerReturnProductsTable customerReturnProductsTable = new CustomerReturnProductsTable(getApplicationContext());
        ArrayList<CustomerReturnProducts> customerReturnProductsArrayList = customerReturnProductsTable.getCustomerReturnProductsDetailsForSync();
        JsonArray customerReturnProductsJsonArray = gson.toJsonTree(customerReturnProductsArrayList).getAsJsonArray();


        //Posting Journey Plan Details
        NewJourneyPlanTable newJourneyPlanTable = new NewJourneyPlanTable(getApplicationContext());
        ArrayList<NewJourneyPlan> newJourneyPlanArrayList = newJourneyPlanTable.getJourneyPlanDetailsForSync();
        JsonArray newJourneyPlanJsonArray = gson.toJsonTree(newJourneyPlanArrayList).getAsJsonArray();


        //Posting Journey Plan Customer Details
        NewJourneyPlanCustomerTable newJourneyPlanCustomerTable = new NewJourneyPlanCustomerTable(getApplicationContext());
        ArrayList<NewJourneyPlanCustomers> newJourneyPlanCustomersArrayList = newJourneyPlanCustomerTable.getJourneyPlanCustomersDetailsForSync();
        JsonArray newJourneyPlanCustomersJsonArray = gson.toJsonTree(newJourneyPlanCustomersArrayList).getAsJsonArray();

        //Posting Stock Issue
        StockIssueTable stockIssueTable = new StockIssueTable(getApplicationContext());
        ArrayList<StockIssue> stockIssueArrayList = stockIssueTable.getStockIssueDetailsForSync();
        JsonArray stockIssueJsonArray = gson.toJsonTree(stockIssueArrayList).getAsJsonArray();

        //Posting Stock Issue
        StockIssueProductsTable stockIssueProductsTable = new StockIssueProductsTable(getApplicationContext());
        ArrayList<StockIssueProducts> stockIssueProductsArrayList = stockIssueProductsTable.getStockIssueProductsDetailsForSync();
        JsonArray stockIssueProductsJsonArray = gson.toJsonTree(stockIssueProductsArrayList).getAsJsonArray();


        //Details Info
        CrmDetailsTable crmDetailsTable = new CrmDetailsTable(getApplicationContext());
        ArrayList<Details> crmDetailsArrayList = crmDetailsTable.getDetailsForSync();
        JsonArray crmDetailsJsonArray = gson.toJsonTree(crmDetailsArrayList).getAsJsonArray();

        //Crm Header Info
        CrmTable crmTable = new CrmTable(getApplicationContext());
        ArrayList<Crm> crmArrayList = crmTable.getCrmForSync();
        JsonArray crmJsonArray = gson.toJsonTree(crmArrayList).getAsJsonArray();

        //Crm Products Info
        CrmProductsTable crmProductsTable = new CrmProductsTable(getApplicationContext());
        ArrayList<CrmProducts> crmProductsArrayList = crmProductsTable.getCrmProductsForSync();
        JsonArray crmProductsJsonArray = gson.toJsonTree(crmProductsArrayList).getAsJsonArray();

        //Crm New Products Info
        CrmNewProductsTable crmNewProductsTable = new CrmNewProductsTable(getApplicationContext());
        ArrayList<CrmNewProducts> crmNewProductsArrayList = crmNewProductsTable.getCrmNewProductsForSync();
        JsonArray crmNewProductsJsonArray = gson.toJsonTree(crmNewProductsArrayList).getAsJsonArray();

        //Crm Meetings Info
        CrmMeetingsTable crmMeetingsTable = new CrmMeetingsTable(getApplicationContext());
        ArrayList<CrmMeeting> crmMeetingArrayList = crmMeetingsTable.getCrmMeetingsForSync();
        JsonArray crmMeetingsJsonArray = gson.toJsonTree(crmMeetingArrayList).getAsJsonArray();

        //Crm Status Info
        CrmStatusTable crmStatusTable = new CrmStatusTable(getApplicationContext());
        ArrayList<CrmStatus> crmStatusArrayList = crmStatusTable.getCrmStatusForSync();
        JsonArray crmStatusJsonArray = gson.toJsonTree(crmStatusArrayList).getAsJsonArray();


        ExpenseTable expenseTable = new ExpenseTable(getApplicationContext());
        ArrayList<Expense> expenseArrayList = expenseTable.getAllExpense();
        JsonArray expenseJsonArray = gson.toJsonTree(expenseArrayList).getAsJsonArray();

        /*
         * Adding the all Json Array of objects into a Json Object
         */
        JsonObject jp = new JsonObject();
        jp.add("orders", ordersJsonArray);
        jp.add("orderItems", orderItemsJsonArray);
        jp.add("invoices", invoicesJsonArray);
        jp.add("invoiceItems", invoiceItemsJsonArray);
        jp.add("primaryCollections", primaryCollectionsJsonArray);
        jp.add("secondaryCollections", secondaryCollectionsJsonArray);
        jp.add("stockReceipts", stockReceiptsJsonArray);
        jp.add("stockReceiptBatches", stockReceiptBatchesJsonArray);
        jp.add("competitorStockProducts", competitorStockProductsJsonArray);
        jp.add("customerStockProducts", customerStockProductsJsonArray);
        jp.add("customerDetails", customerDetailsJsonArray);
        jp.add("surveys", surveyResponseJsonArray);
        jp.add("surveyResponseAnswers", surveyResponseAnswerJsonArray);
        jp.add("feedback", feedbackJsonArray);
        jp.add("tripDetails", tripDetailsJsonArray);
        jp.add("planVisitDetails", checkInDetailsJsonArray);
        jp.add("pharmacyInfo", pharmacyInfoArrayJsonArray);
        jp.add("pharmacyInfoProducts", pharmacyInfoProductsJsonArray);
        jp.add("doctorVisitPatientsInfo", doctorVisitInfoJsonArray);
        jp.add("doctorVisitPrescriptionProducts", doctorPrescribedProductsJsonArray);
        jp.add("sponsorShipDetails", doctorVisitSponsorshipJsonArray);
        jp.add("notesDetails", notesJsonArray);
        jp.add("waybillDetails", WaybillDetailsJsonArray);
        jp.add("stockReturn", stockReturnInfoJsonArray);
        jp.add("stockReturnProducts", stockReturnProductsInfoJsonArray);
        jp.add("lpoImages", lpoImagesInfoJsonArray);
        jp.add("loginLogout", logInOutInfoJsonArray);
        jp.add("customerReturn", customerReturnJsonArray);
        jp.add("customerReturnProducts", customerReturnProductsJsonArray);
        jp.add("newJourneyPlan", newJourneyPlanJsonArray);
        jp.add("newJourneyPlanCustomers", newJourneyPlanCustomersJsonArray);
        jp.add("stockIssue", stockIssueJsonArray);
        jp.add("stockIssueProducts", stockIssueProductsJsonArray);

        jp.add("details", crmDetailsJsonArray);
        jp.add("crm", crmJsonArray);
        jp.add("crmProducts", crmProductsJsonArray);
        jp.add("crmNewProducts", crmNewProductsJsonArray);
        jp.add("crmMeeting", crmMeetingsJsonArray);
        jp.add("crmStatus", crmStatusJsonArray);

        jp.add("expense", expenseJsonArray);

        Log.i("onResponse: POST-----", "" + jp.toString());


        try {
            //Inserting the Json object into the Api
            Call<ResponseJson> call = api.insertData(jp);

            call.enqueue(new Callback<ResponseJson>() {
                @Override
                public void onResponse(Call<ResponseJson> call, Response<ResponseJson> response) {

                    ResponseJson resultSet = response.body();

                    if (resultSet != null && resultSet.status.equals("true")) {

                        String[] orderIds = new String[ordersArrayList.size()];
                        int i = 0;
                        for (Orders oh : ordersArrayList) {
                            orderIds[i] = oh.getOrderCode();
                            i++;
                            // Log.d("onResponse: Data--", "oh " + oh.getOrderCode());
                        }
                        dbHandler.updateOrdersSync(orderIds);
                        dbHandler.updateOrderItemsSync(orderIds);

                        String[] invoiceIds = new String[invoicesArrayList.size()];
                        i = 0;
                        for (Invoices oh : invoicesArrayList) {
                            invoiceIds[i] = oh.getInvoiceCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getInvoiceCode());
                        }
                        dbHandler.updateInvoicesSync(invoiceIds);
                        dbHandler.updateInvoiceItemsSync(invoiceIds);


                        String[] primaryCollectionIds = new String[primaryCollectionDetailsArrayList.size()];
                        i = 0;
                        for (CollectionDetails oh : primaryCollectionDetailsArrayList) {
                            primaryCollectionIds[i] = oh.getCollectionCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getCollectionCode());
                        }
                        dbHandler.updatePrimaryCollectionsSync(primaryCollectionIds);

                        String[] secondaryCollectionIds = new String[secondaryCollectionDetailsArrayList.size()];
                        i = 0;
                        for (CollectionDetails oh : secondaryCollectionDetailsArrayList) {
                            secondaryCollectionIds[i] = oh.getCollectionCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getCollectionCode());
                        }
                        dbHandler.updateSecondaryCollectionsSync(secondaryCollectionIds);

                        String[] customerIds = new String[customerDetailsArrayList.size()];
                        i = 0;
                        for (CustomerDetails oh : customerDetailsArrayList) {
                            customerIds[i] = oh.getCustomerCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getCustomerCode());
                        }
                        dbHandler.updateCustomerDetailsSync(customerIds);

                        String[] stockReceiptIds = new String[stockReceiptArrayList.size()];
                        i = 0;
                        for (StockReceipt oh : stockReceiptArrayList) {
                            stockReceiptIds[i] = oh.getStockReceiptCode();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getStockReceiptCode());
                        }
                        dbHandler.updateStockReceiptsSync(stockReceiptIds);

                        String[] stockReceiptBatchIds = new String[stockReceiptBatchArrayList.size()];
                        i = 0;
                        for (StockReceiptBatch oh : stockReceiptBatchArrayList) {
                            stockReceiptBatchIds[i] = oh.getStockReceiptCode();
                            i++;
                            // Log.d("onResponse: Data--", "oh " + oh.getStockReceiptCode());
                        }
                        dbHandler.updateStockReceiptBatchesSync(stockReceiptBatchIds);

                        String[] competitorStockId = new String[competitorStockProductArrayList.size()];
                        i = 0;
                        for (CompetitorStockProduct oh : competitorStockProductArrayList) {
                            competitorStockId[i] = oh.getCompetitor_stock_id();
                            i++;
                            // Log.d("onResponse: Data--", "oh " + oh.getCompetitor_stock_id());
                        }
                        dbHandler.updateCompetitorStockProductsSync(competitorStockId);


                        String[] customerStockId = new String[customerStockProductArrayList.size()];
                        i = 0;
                        for (CustomerStockProduct oh : customerStockProductArrayList) {
                            customerStockId[i] = oh.getProduct_id();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getProduct_id());
                        }
                        dbHandler.updateCustomerStockProductsSync(customerStockId);


                        String[] surveyId = new String[surveyResponseArrayList.size()];
                        i = 0;
                        for (SurveyResponse oh : surveyResponseArrayList) {
                            surveyId[i] = oh.getSurvey_id();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getSurvey_id());
                        }
                        dbHandler.updateSurveyResponseSync(surveyId);


                        String[] surveyResponseAnswerId = new String[surveyResponseAnswerArrayList.size()];
                        i = 0;
                        for (SurveyResponseAnswer oh : surveyResponseAnswerArrayList) {
                            surveyResponseAnswerId[i] = oh.getSurvey_response_answer_id();
                            i++;
                            //Log.d("onResponse: Data--", "oh " + oh.getSurvey_response_answer_id());
                        }
                        dbHandler.updateSurveyResponseAnswerSync(surveyResponseAnswerId);

                        generatePostSyncDateTime();

                        RDLSync rdlSync = new RDLSync();
                        rdlSync.setPostSyncDate(postSyncDate);
                        rdlSync.setPostSyncTime(postSyncTime);
                        rdlSyncTable = new RDLSyncTable(getApplicationContext());
                        rdlSyncTable.create(rdlSync);
                    }


                    /*
                     * Posting Trip Mileage images, Lpo Gallery images and Secondary Customer Images on FTP in certain time duration limit..
                     */
//
//                    FTPClient con = null;
//                    try {
//                        LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
//                        ArrayList<TripDetails> tripDetailsImageArrayList = dbHandler.getTripDetailsImageService();
//                        ArrayList<CustomerDetails> customerDetailsImageArrayList = dbHandler.getCustomerDetailsImageService();
//                        ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLpoImageService();
//                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                                .permitAll()
//                                .build();
//                        StrictMode.setThreadPolicy(policy);
//                        Log.d("FileUpload", "FilePath");
//                        if (tripDetailsImageArrayList.size() != 0 || customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
//                            con = new FTPClient();
//                            con.connect(Constants.FTP_HOST_NAME);
//                            Log.d("FileUpload", "FilePath Conneted");
//                            if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {
//
//                                System.out.println("AAA:::con = " + con);
//
//                                Log.d("FileUpload", "FilePath Login");
//                                con.enterLocalPassiveMode(); // important!
//                                con.setFileType(FTP.BINARY_FILE_TYPE);
//                                for (TripDetails tp : tripDetailsImageArrayList) {
//                                    if (tp.getOdometerMileage() != null) {
//                                        try {
//                                            System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);
//
//                                            String data = Environment.getExternalStorageDirectory() + File.separator
//                                                    + "KSFA" + File.separator + "images" + File.separator + tp.getOdometerMileage();
//                                            Log.d("FileUpload", "FilePath " + data);
//                                            FileInputStream in = new FileInputStream(new File(data));
//                                            boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getOdometerMileage(), in);
//
//                                            System.out.println("AAA:::result " + result);
//
//                                            in.close();
//                                            if (result) {
//                                                System.out.println("AAA:::FileUpload::::Success " + result);
//                                                dbHandler.updateTripMileageUploadFlag(tp.getTripNumber(), tp.getTripDate());
//                                                Log.d("FileUpload", "Success");
//
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                            Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
//                                        }
//                                    }
//
//                                }
//                                for (CustomerDetails cd : customerDetailsImageArrayList) {
//                                    if (cd.getImageURL() != null) {
//                                        try {
//                                            String data = Environment.getExternalStorageDirectory() + File.separator
//                                                    + "KSFA" + File.separator + "images" + File.separator + cd.getImageURL();
//                                            Log.d("FileUpload", "FilePath " + data);
//                                            FileInputStream in = new FileInputStream(new File(data));
//                                            boolean result = con.storeFile("/home/vistagftp/vistagftp/" + cd.getImageURL(), in);
//                                            in.close();
//                                            if (result) {
//                                                Log.d("FileUpload", "Success");
//                                                dbHandler.updateCustomerDetailsUploadFlag(cd.getCustomerCode());
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                            Log.d("FileUpload", "Customer Details Error" + e.getMessage());
//                                        }
//
//                                    }
//
//                                }
//                                for (LPOImages lp : lpoImagesArrayList) {
//                                    if (lp.getLpoImagePath() != null) {
//                                        try {
//                                            String data = Environment.getExternalStorageDirectory() + File.separator
//                                                    + "KSFA" + File.separator + "images" + File.separator + lp.getLpoImagePath();
//                                            Log.d("FileUpload", "FilePath " + data);
//                                            FileInputStream in = new FileInputStream(new File(data));
//                                            boolean result = con.storeFile("/home/vistagftp/vistagftp/" + lp.getLpoImagePath(), in);
//                                            in.close();
//                                            if (result) {
//                                                lpoImagesTable.updatelpoGalleryUploadFlag(String.valueOf(lp.getImageId()));
//                                                System.out.println("TTT::String.valueOf(lp.imageId) = " + String.valueOf(lp.getImageId()));
//                                                System.out.println("TTT::Sucesss= ");
//                                                Log.d("FileUpload", "Success");
//
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                            Log.d("FileUpload", "Lpo Error" + e.getMessage());
//                                        }
//                                    }
//
//                                }
//
//
//                                con.logout();
//                                con.disconnect();
//                            }
//                        }
//
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Log.d("FileUpload", "Error" + e.getMessage());
//                    }


                    System.out.println("FTpppppppp = ");
                        FTPClient con = null;
                        try {
                            System.out.println("Enterrrrr = ");
                            LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
                            ExpenseTable expenseTable = new ExpenseTable(getApplicationContext());
                            ArrayList<TripDetails> tripDetailsImageArrayList = dbHandler.getTripDetailsImageService();
//                            ArrayList<Expense> expenseSignImageArrayList = expenseTable.getExpenseSignImageService();
                            ArrayList<CustomerDetails> customerDetailsImageArrayList = dbHandler.getCustomerDetailsImageService();
                            ArrayList<LPOImages> lpoImagesArrayList = lpoImagesTable.getLpoImageService();
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                    .permitAll()
                                    .build();
                            StrictMode.setThreadPolicy(policy);
                            Log.d("FileUpload", "FilePath");

                            System.out.println("FileUploadddddddd = ");
                            if (tripDetailsImageArrayList.size() != 0 ||  customerDetailsImageArrayList.size() != 0 || lpoImagesArrayList.size() != 0) {
                                con = new FTPClient();
                                con.connect(Constants.FTP_HOST_NAME);
                                Log.d("FileUpload", "FilePath Conneted");
                                System.out.println("FilePath Conneteddddddddd = ");
                                if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {

                                    System.out.println("AAA:::con = ");

                                    Log.d("FileUpload", "FilePath Login");
                                    con.enterLocalPassiveMode(); // important!
                                    con.setFileType(FTP.BINARY_FILE_TYPE);
                                    for (TripDetails tp : tripDetailsImageArrayList) {
                                        if (tp.getOdometerMileage() != null) {
                                            try {
                                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                                String data = Environment.getExternalStorageDirectory() + File.separator
                                                        + "KSFA" + File.separator + "images" + File.separator + tp.getOdometerMileage();
                                                Log.d("FileUpload", "FilePath " + data);
                                                FileInputStream in = new FileInputStream(new File(data));
                                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getOdometerMileage(), in);

                                                System.out.println("AAA:::result " + result);

                                                in.close();
                                                if (result) {
                                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                                    dbHandler.updateTripMileageUploadFlag(tp.getTripNumber(), tp.getTripDate());
                                                    Log.d("FileUpload", "Success");

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                                            }
                                        }

                                    }

                                    /*for (Expense tp : expenseSignImageArrayList) {
                                        if (tp.getSignatureFilePath() != null) {
                                            try {
                                                System.out.println("AAA:::FileUpload " + Constants.FTP_USER_NAME);

                                                String data = Environment.getExternalStorageDirectory() + File.separator
                                                        + "KSFA" + File.separator + "images" + File.separator + tp.getSignatureFilePath();
                                                Log.d("FileUpload", "FilePath " + data);
                                                FileInputStream in = new FileInputStream(new File(data));
                                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + tp.getSignatureFilePath(), in);

                                                System.out.println("AAA:::result " + result);

                                                in.close();
                                                if (result) {
                                                    System.out.println("AAA:::FileUpload::::Success " + result);
                                                    expenseTable.updateExpenseSignUploadFlag(tp.getExpenseNumber(), tp.getExpensePostDate());
                                                    Log.d("FileUpload", "Success");

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Log.d("FileUpload", "Trip Mileage Error" + e.getMessage());
                                            }
                                        }

                                    }*/


                                    for (CustomerDetails cd : customerDetailsImageArrayList) {
                                        if (cd.getImageURL() != null) {
                                            try {
                                                String data = Environment.getExternalStorageDirectory() + File.separator
                                                        + "KSFA" + File.separator + "images" + File.separator + cd.getImageURL();
                                                Log.d("FileUpload", "FilePath " + data);
                                                FileInputStream in = new FileInputStream(new File(data));
                                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + cd.getImageURL(), in);
                                                in.close();
                                                if (result) {
                                                    Log.d("FileUpload", "Success");
                                                    dbHandler.updateCustomerDetailsUploadFlag(cd.getCustomerCode());
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Log.d("FileUpload", "Customer Details Error" + e.getMessage());
                                            }

                                        }

                                    }
                                    for (LPOImages lp : lpoImagesArrayList) {
                                        if (lp.getLpoImagePath() != null) {
                                            try {
                                                String data = Environment.getExternalStorageDirectory() + File.separator
                                                        + "KSFA" + File.separator + "images" + File.separator + lp.getLpoImagePath();
                                                Log.d("FileUpload", "FilePath " + data);
                                                FileInputStream in = new FileInputStream(new File(data));
                                                boolean result = con.storeFile("/home/vistagftp/vistagftp/" + lp.getLpoImagePath(), in);
                                                in.close();
                                                System.out.println("ftp.getReplyString() = " + con.getReplyString());
                                                if (result) {
                                                    lpoImagesTable.updatelpoGalleryUploadFlag(String.valueOf(lp.getImageId()));
                                                    System.out.println("TTT::String.valueOf(lp.imageId) = " + String.valueOf(lp.getImageId()));
                                                    System.out.println("TTT::Sucesss= ");
                                                    Log.d("FileUpload", "Success");

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                System.out.println("e.getMessage() = " + e.getMessage());
                                                Log.d("FileUpload", "Lpo Error" + e.getMessage());
                                            }
                                        }

                                    }


                                    con.logout();
                                    con.disconnect();
                                }
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("FileUpload", "Error" + e.getMessage());
                        }
                }

                @Override
                public void onFailure(Call<ResponseJson> call, Throwable t) {


                    Log.i("onResponse: failure----", "" + t);
                    //Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                }
            });

            Log.i("MyTestService", "Service running");
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Throwable" + e, Toast.LENGTH_LONG).show();

        }
    }

    private void postingDatabaseFtp(String login_id) {
        FTPClient con = null;
        con = new FTPClient();


        try {
            con.connect(Constants.FTP_HOST_NAME);
            if (con.login(Constants.FTP_USER_NAME, Constants.FTP_PASSWORD)) {
                Log.d("FileUpload", "FilePath Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d("FileUpload", "FilePath Connected");
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String date = timeStampFormat.format(myDate);
        String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/dbBackup/" + Constants.TAB_CODE + "_" + login_id + "_" + date + "_" + "ksfaDB.DB";

        String file_name = "ksfaDB.DB";
        try {
            String data = Environment.getExternalStorageDirectory() + File.separator
                    + "KSFA" + File.separator + file_name;
            Log.d("FileUpload", "FilePath " + data);

            FileInputStream in = new FileInputStream(new File(destinationPath));

            boolean result = con.storeFile("/home/vistagftp/vistagftp/Db/ksfaDB.DB", in);
            Log.d("FileUpload", "File Result--" + result);

            in.close();

            if (result) {
                Log.d("FileUpload", "Success");

            }


            con.logout();
            con.disconnect();

            Log.d("FileUpload", "File Connection Closed--");


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Database Upload Error" + e.getMessage());
        }
    }


    private void generatePostSyncDateTime() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        postSyncDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        postSyncTime = sdf.format(outDate);
    }

}
