package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.model.CrmColdCall;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_COLD_CALL;

public class CrmColdCallTable extends SQLiteOpenHelper {

    private static final String COLUMN_COLD_CALL_ID = "coldCallId";
    private static final String COLUMN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_COLD_CALL_NUMBER = "coldCallNumber";
    private static final String COLUMN_COLD_CALL_DATE = "coldCallDate";
    private static final String COLUMN_COLD_CALL_PERSON_MET1 = "personMet1";
    private static final String COLUMN_COLD_CALL_PERSON_MET2 = "personMet2";
    private static final String COLUMN_COLD_CALL_SUBJECT = "subject";
    private static final String COLUMN_COLD_CALL_OUTCOME_TYPE = "outcomeType";
    private static final String COLUMN_COLD_CALL_OUTCOME_DESCRIPTION = "outcomeDescription";
    private static final String COLUMN_COLD_CALL_VISIT_DATE = "visitDate";
    private static final String COLUMN_COLD_CALL_VISIT_TIME = "visitTime";
    private static final String COLUMN_COLD_CALL_VISIT_ADDRESS = "visitAddress";
    private static final String COLUMN_COLD_CALL_IS_SYNC = "isSync";
    private static final String COLUMN_COLD_CALL_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_COLD_CALL_RANDOM_NUMBER = "randomNumber";
    private static final String COLUMN_COLD_CALL_CREATED_AT = "createdAt";
    private static final String COLUMN_COLD_CALL_UPDATED_AT = "updatedAt";

    public Context context;
    String selectQuery;
    Cursor cursor;

    public CrmColdCallTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_COLD_CALL);

        String coldCallTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_COLD_CALL + "(" +
                COLUMN_COLD_CALL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_COLD_CALL_NUMBER + " TEXT, " +
                COLUMN_COLD_CALL_OUTCOME_TYPE + " TEXT, " +
                COLUMN_COLD_CALL_DATE + " TEXT, " +
                COLUMN_COLD_CALL_PERSON_MET1 + " TEXT, " +
                COLUMN_COLD_CALL_PERSON_MET2 + " TEXT, " +
                COLUMN_COLD_CALL_SUBJECT + " TEXT, " +
                COLUMN_COLD_CALL_OUTCOME_DESCRIPTION + " TEXT, " +
                COLUMN_COLD_CALL_VISIT_DATE + " TEXT, " +
                COLUMN_COLD_CALL_VISIT_TIME + " TEXT, " +
                COLUMN_COLD_CALL_VISIT_ADDRESS + " TEXT, " +
                COLUMN_COLD_CALL_IS_SYNC + " TEXT, " +
                COLUMN_COLD_CALL_LATEST_SYNC_DATE + " TEXT, " +
                COLUMN_COLD_CALL_RANDOM_NUMBER + " INTEGER, " +
                COLUMN_COLD_CALL_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_COLD_CALL_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(coldCallTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmColdCall coldCall) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_CODE, coldCall.getCustomerCode());
        values.put(COLUMN_COLD_CALL_NUMBER, coldCall.getColdCallNumber());
        values.put(COLUMN_COLD_CALL_OUTCOME_TYPE, coldCall.getOutcomeType());
        values.put(COLUMN_COLD_CALL_DATE, coldCall.getColdCallDate());
        values.put(COLUMN_COLD_CALL_PERSON_MET1, coldCall.getPersonsMet1());
        values.put(COLUMN_COLD_CALL_PERSON_MET2, coldCall.getPersonsMet2());
        values.put(COLUMN_COLD_CALL_SUBJECT, coldCall.getSubject());
        values.put(COLUMN_COLD_CALL_OUTCOME_DESCRIPTION, coldCall.getOutcomeDescription());
        values.put(COLUMN_COLD_CALL_VISIT_DATE, coldCall.getVisitDate());
        values.put(COLUMN_COLD_CALL_VISIT_TIME, coldCall.getVisitTime());
        values.put(COLUMN_COLD_CALL_VISIT_ADDRESS, coldCall.getVisitAddress());
        values.put(COLUMN_COLD_CALL_IS_SYNC, coldCall.isSync());
        values.put(COLUMN_COLD_CALL_LATEST_SYNC_DATE, coldCall.getLatestSyncDate());
        values.put(COLUMN_COLD_CALL_RANDOM_NUMBER, coldCall.getRandomNumber());


        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_COLD_CALL, null, values);
        db.close();
        return id;
    }


    public int getColdCallNumber() {
        int coldCallId = 0;
        String selectQuery = "select randomNumber from xxmsales_crm_cold_call where coldCallDate=date('now') order by randomNumber desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            coldCallId = cursor.getInt(0);
            cursor.close();
        }

        return coldCallId;
    }



   /* public ArrayList<CrmColdCall> getOutcomeTypes() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_COLD_CALL +" ORDER BY outcomeType ASC" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        Log.d("ViewStock--","Get Products  " );
        ArrayList<CrmColdCall> coldCallArrayList = new ArrayList<CrmColdCall>();
        while (cursor.moveToNext()) {
            CrmColdCall coldCall = new CrmColdCall();
            Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            coldCall.setColdCallId(cursor.getInt(0));
            coldCall.setOutcomeType(cursor.getString(3));

            coldCallArrayList.add(coldCall);
        }
        cursor.close();
        sqlWrite.close();
        return coldCallArrayList;
    }*/


    public ArrayList<CrmColdCall> getColdCallHistoryByCustomer(String customerId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_COLD_CALL + " WHERE " + COLUMN_CUSTOMER_CODE + "=? ORDER BY coldCallId desc";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerId});

        ArrayList<CrmColdCall> coldCallArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            CrmColdCall cd = new CrmColdCall();
            cd.setColdCallId(cursor.getInt(0));
            cd.setCustomerCode(cursor.getString(1));
            cd.setColdCallNumber(cursor.getString(2));
            cd.setOutcomeType(cursor.getString(3));

            String ccDate = cursor.getString(4);
            try {
                @SuppressLint("SimpleDateFormat") Date coldCallFormatDate = new SimpleDateFormat("yyyy-MM-dd").parse(ccDate);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(coldCallFormatDate);
                cd.setColdCallDate(dateString);
            } catch (Exception ignored) {
            }

            cd.setPersonsMet1(cursor.getString(5));
            cd.setPersonsMet2(cursor.getString(6));
            cd.setSubject(cursor.getString(7));
            cd.setOutcomeDescription(cursor.getString(8));
            cd.setVisitDate(cursor.getString(9));
            cd.setVisitTime(cursor.getString(10));
            cd.setVisitAddress(cursor.getString(11));

            coldCallArrayList.add(i, cd);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return coldCallArrayList;
    }

   /* public ArrayList<CrmColdCall> getColdCallHistory() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        String selectQuery = "SELECT fb.feedback_date,fb.type,cust_det.customer_name,fb.feedback,fb.action_taken FROM " + TABLE_CRM_COLD_CALL + " as fb LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON fb.customer_id = cust_det.customer_id";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        Log.d("QueryData cursor ", cursor.toString());

        ArrayList<CrmColdCall> coldCallArrayList = new ArrayList<CrmColdCall>();
        Log.d("QueryData Array ", coldCallArrayList.toString());

        int i = 0;

        while (cursor.moveToNext()) {
            CrmColdCall coldCall = new CrmColdCall();
            // feedbackReport.setFeedback_date(cursor.getString(0));

            String sDate1 = cursor.getString(0);
            try {
                Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format( date1 );
                coldCall.setColdCallDate(dateString);
            }
            catch (Exception e) {
                // ledger.setInvoiceDate(cursor.getString(3));
            }

            coldCall.setPersonsMet1(cursor.getString(1));
            coldCall.setPersonsMet2(cursor.getString(2));
            coldCall.setOutcomeType(cursor.getString(3));
            coldCall.setOutcomeDescription(cursor.getString(4));

            coldCallArrayList.add(i, coldCall);

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return coldCallArrayList;
    }*/

}
