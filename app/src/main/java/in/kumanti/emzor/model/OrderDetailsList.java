package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class OrderDetailsList {

    private String date;
    private String order_num;
    private String customer_name;
    private String location;
    private String city;
    private String line_items;
    private String value;
    private String status;
    private String order_id;
    private String customer_id;


    public OrderDetailsList(String date1, String order_num1,
                            String customer_name1, String location1,
                            String city1, String line_items1,
                            String value1, String status1, String order_id1, String customer_id1
    ) {

        date = date1;
        order_num = order_num1;
        customer_name = customer_name1;
        location = location1;
        city = city1;
        line_items = line_items1;
        value = value1;
        status = status1;
        order_id = order_id1;
        customer_id = customer_id1;


    }

    public String getDate() {
        return date;
    }

    public void setDate(String date1) {
        date = date1;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num1) {
        order_num = order_num1;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name1) {
        customer_name = customer_name1;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location1) {
        location = location1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city1) {
        city = city1;
    }

    public String getLine_items() {
        return line_items;
    }

    public void setLine_items(String line_items1) {
        line_items = line_items1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value1) {
        value = value1;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status1) {
        status = status1;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id1) {
        order_id = order_id1;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id1) {
        customer_id = customer_id1;
    }

}

