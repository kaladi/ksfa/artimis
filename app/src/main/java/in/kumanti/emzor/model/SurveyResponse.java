package in.kumanti.emzor.model;

import java.util.ArrayList;

public class SurveyResponse {

    public String tabId;
    public int surveyResponseId;
    public String productCode;
    public String companyCode;
    public String customerCode;
    public String srCode;
    public String surveyCode;
    public String status;
    public ArrayList<SurveyResponseAnswer> responseAnswers;
    public String createdAt;
    public String updatedAt;
    public String visitSequenceNumber;


    public int getId() {
        return surveyResponseId;
    }

    public void setId(int id) {
        this.surveyResponseId = id;
    }

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String tabId) {
        this.tabId = tabId;
    }

    public String getProduct_id() {
        return productCode;
    }

    public void setProduct_id(String product_id) {
        this.productCode = product_id;
    }

    public String getCompany_id() {
        return companyCode;
    }

    public void setCompany_id(String company_id) {
        this.companyCode = company_id;
    }


    public String getCustomer_id() {
        return customerCode;
    }

    public void setCustomer_id(String customer_id) {
        this.customerCode = customer_id;
    }


    public String getSales_rep_id() {
        return srCode;
    }

    public void setSales_rep_id(String sales_rep_id) {
        this.srCode = sales_rep_id;
    }

    public String getSurvey_id() {
        return surveyCode;
    }

    public void setSurvey_id(String survey_id) {
        this.surveyCode = survey_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<SurveyResponseAnswer> getResponseAnswers() {
        return responseAnswers;
    }

    public void setResponseAnswers(ArrayList<SurveyResponseAnswer> responseAnswers) {
        this.responseAnswers = responseAnswers;
    }

    public String getCreated_at() {
        return createdAt;
    }

    public void setCreated_at(String created_at) {
        this.createdAt = created_at;
    }


    public String getUpdated_at() {
        return updatedAt;
    }

    public void setUpdated_at(String updated_at) {
        this.updatedAt = updated_at;
    }

    public String getVisitSequenceNumber() {
        return visitSequenceNumber;
    }

    public void setVisitSequenceNumber(String visitSequenceNumber) {
        this.visitSequenceNumber = visitSequenceNumber;
    }
}
