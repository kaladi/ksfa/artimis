package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.Age;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_AGE;

public class AgeTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_AGE_ID = "id";
    private static final String COLUMN_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_BELOW_THIRTY_VAL = "below_thirty_val";
    private static final String COLUMN_BELOW_THIRTY_PER = "below_thirty_per";
    private static final String COLUMN_THIRTY_ONE_TO_SIXTY_VAL = "thirty_one_to_sixty_val";
    private static final String COLUMN_THIRTY_ONE_TO_SIXTY_PER = "thirty_one_to_sixty_per";
    private static final String COLUMN_SIXTY_ONE_TO_NINETY_VAL = "sixty_one_to_ninety_val";
    private static final String COLUMN_SIXTY_ONE_TO_NINETY_PER = "sixty_one_to_ninety_per";
    private static final String COLUMN_ABOVE_NINETY_VAL = "above_ninety_val";
    private static final String COLUMN_ABOVE_NINETY_PER = "above_ninety_per";
    private static final String COLUMN_AGE_CREATED_AT = "created_at";
    private static final String COLUMN_AGE_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public AgeTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_AGE);

        String surveyTable = "CREATE TABLE IF NOT EXISTS " + TABLE_AGE + "(" +
                COLUMN_AGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_ID + " TEXT, " +
                COLUMN_BELOW_THIRTY_VAL + " TEXT, " +
                COLUMN_BELOW_THIRTY_PER + " TEXT, " +
                COLUMN_THIRTY_ONE_TO_SIXTY_VAL + " TEXT, " +
                COLUMN_THIRTY_ONE_TO_SIXTY_PER + " TEXT, " +
                COLUMN_SIXTY_ONE_TO_NINETY_VAL + " TEXT, " +
                COLUMN_SIXTY_ONE_TO_NINETY_PER + " TEXT, " +
                COLUMN_ABOVE_NINETY_VAL + " TEXT, " +
                COLUMN_ABOVE_NINETY_PER + " TEXT, " +
                COLUMN_AGE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_AGE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(surveyTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<Age> getAgeList(String customer_id, String customer_type) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery;
        Cursor cursor;

        if ("Primary".equals(customer_type)) {
            selectQuery = "SELECT * FROM " + TABLE_AGE + " WHERE " + COLUMN_CUSTOMER_ID + "=?";

        } else {
            selectQuery = "select null,null,thirty_val,null thirty_val_per,thirty_to_sixty,null thirty_to_sixty_per,sixty_to_ninty,null sixty_to_ninty_per,greater_than_ninty,null greater_than_ninty_per from ((select sum(outstanding_amount) thirty_val FROM (select invoice_amount,invoice_num,invoice_date,sum(payment_amount) payment_amount,(invoice_amount - sum(payment_amount)) outstanding_amount from xxmsales_collection_details where collection_type='Against Invoice' and  customer_id='" + customer_id + "' and invoice_date >= date( julianday(date('now')) - 30) GROUP BY INVOICE_NUM ) ),(select sum(outstanding_amount) thirty_to_sixty FROM (select invoice_amount,invoice_num,invoice_date,sum(payment_amount) payment_amount,(invoice_amount - sum(payment_amount)) outstanding_amount from xxmsales_collection_details where collection_type='Against Invoice' and  customer_id='" + customer_id + "' and invoice_date < date( julianday(date('now')) - 30) and invoice_date >= date( julianday(date('now')) - 60) GROUP BY INVOICE_NUM )),(select sum(outstanding_amount) sixty_to_ninty FROM (select invoice_amount,invoice_num,invoice_date,sum(payment_amount) payment_amount,(invoice_amount - sum(payment_amount)) outstanding_amount from xxmsales_collection_details where collection_type='Against Invoice' and  customer_id='" + customer_id + "' and invoice_date < date( julianday(date('now')) - 60) and invoice_date >= date( julianday(date('now')) - 90) GROUP BY INVOICE_NUM )),(select sum(outstanding_amount) greater_than_ninty FROM (select invoice_amount,invoice_num,invoice_date,sum(payment_amount) payment_amount,(invoice_amount - sum(payment_amount)) outstanding_amount from xxmsales_collection_details where collection_type='Against Invoice' and  customer_id='" + customer_id + "' and invoice_date < date( julianday(date('now')) - 90) GROUP BY INVOICE_NUM )))";
        }

        if ("Primary".equals(customer_type)) {
            cursor = sqlWrite.rawQuery(selectQuery, new String[]{customer_id});

        } else {
            cursor = sqlWrite.rawQuery(selectQuery, null);
        }

        Log.d("Data", "Get Age Lists  ");
        ArrayList<Age> ageArrayList = new ArrayList<>();

        while (cursor.moveToNext()) {
            Age age = new Age();
            int id = cursor.getInt(0);
            age.setAgeId(id);
            age.setCustomerId(cursor.getString(1));
            age.setBelowThirtyVal(cursor.getString(2));
            age.setBelowThirtyPer(cursor.getString(3));
            age.setThirtyOneToSixtyVal(cursor.getString(4));
            age.setThirtyOneToSixtyPer(cursor.getString(5));
            age.setSixtyOneToNinetyVal(cursor.getString(6));
            age.setSixtyOneToNinetyPer(cursor.getString(7));
            age.setAboveNinetyVal(cursor.getString(8));
            age.setAboveNinetyPer(cursor.getString(9));
            ageArrayList.add(age);
        }

        cursor.close();
        sqlWrite.close();
        return ageArrayList;
    }


    public long create(Age age) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_ID, age.customerId);
        values.put(COLUMN_BELOW_THIRTY_VAL, age.belowThirtyVal);
        values.put(COLUMN_THIRTY_ONE_TO_SIXTY_VAL, age.thirtyOneToSixtyVal);
        values.put(COLUMN_SIXTY_ONE_TO_NINETY_VAL, age.sixtyOneToNinetyVal);
        values.put(COLUMN_ABOVE_NINETY_VAL, age.aboveNinetyVal);
        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_AGE, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Age List details from the Master via Web Service
     */

    public void insertingAgeDetailsMaster(ArrayList<Age> ageList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (ageList != null) {
                for (int i = 0; i < ageList.size(); i++) {

                    values.put(COLUMN_CUSTOMER_ID, ageList.get(i).getCustomerId());
                    values.put(COLUMN_BELOW_THIRTY_VAL, ageList.get(i).getBelowThirtyVal());
                    values.put(COLUMN_THIRTY_ONE_TO_SIXTY_VAL, ageList.get(i).getThirtyOneToSixtyVal());
                    values.put(COLUMN_SIXTY_ONE_TO_NINETY_VAL, ageList.get(i).getSixtyOneToNinetyVal());
                    values.put(COLUMN_ABOVE_NINETY_VAL, ageList.get(i).getAboveNinetyVal());
                    db.insert(TABLE_AGE, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }

    public void deleteAge() {
        SQLiteDatabase db = this.getWritableDatabase();

        // Deleting all records from database table
        db.delete(TABLE_AGE, null, null);
        db.close();
    }
}
