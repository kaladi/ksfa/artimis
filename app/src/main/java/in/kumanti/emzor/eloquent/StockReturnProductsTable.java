package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.StockReturnProducts;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_STOCK_RETURN_PRODUCTS;

public class StockReturnProductsTable extends SQLiteOpenHelper {

    //Columns used for Stock Return Products Information details for a selected ordered product
    private static final String COLUMN_STOCK_RETURN_PRODUCT_ID = "stockReturnProductId";
    private static final String COLUMN_STOCK_RETURN_NUMBER = "stockReturnNumber";
    // private static final String COLUMN_STOCK_RETURN_GROUP_CODE = "productGroupCode";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_CODE = "productCode";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_NAME = "productName";
    //private static final String COLUMN_STOCK_RETURN_PRODUCT_UOM = "productUom";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_AVAIL_QUANTITY = "availQuantity";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_RETURN_QUANTITY = "returnQuantity";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_UNIT_PRICE = "unitPrice";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_RETURN_VALUE = "returnValue";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_IS_SYNC = "isSync";
    // private static final String COLUMN_STOCK_RETURN_PRODUCT_LATEST_SYNC_DATE = "latestSyncDate";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_CREATED_AT = "createdAt";
    private static final String COLUMN_STOCK_RETURN_PRODUCT_UPDATED_AT = "updatedAt";

    public Context context;
    private SQLiteDatabase sqlWrite;


    public StockReturnProductsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCK_RETURN_PRODUCTS);

        String stockReturnProductTable = "CREATE TABLE IF NOT EXISTS " + TABLE_STOCK_RETURN_PRODUCTS + "(" +
                COLUMN_STOCK_RETURN_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_STOCK_RETURN_NUMBER + " TEXT, " +
                COLUMN_STOCK_RETURN_PRODUCT_CODE + " TEXT, " +
                COLUMN_STOCK_RETURN_PRODUCT_NAME + " TEXT, " +
                COLUMN_STOCK_RETURN_PRODUCT_AVAIL_QUANTITY + " TEXT, " +
                COLUMN_STOCK_RETURN_PRODUCT_RETURN_QUANTITY + " REAL, " +
                COLUMN_STOCK_RETURN_PRODUCT_UNIT_PRICE + " NUMERIC, " +
                COLUMN_STOCK_RETURN_PRODUCT_RETURN_VALUE + " NUMERIC, " +
                COLUMN_STOCK_RETURN_PRODUCT_IS_SYNC + " TEXT, " +
                COLUMN_STOCK_RETURN_PRODUCT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_STOCK_RETURN_PRODUCT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(stockReturnProductTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(StockReturnProducts stockReturnProducts) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_STOCK_RETURN_NUMBER, stockReturnProducts.getStockReturnNumber());
        values.put(COLUMN_STOCK_RETURN_PRODUCT_CODE, stockReturnProducts.getProductCode());
        values.put(COLUMN_STOCK_RETURN_PRODUCT_NAME, stockReturnProducts.getProductName());
        values.put(COLUMN_STOCK_RETURN_PRODUCT_AVAIL_QUANTITY, stockReturnProducts.getAvailQuantity());
        values.put(COLUMN_STOCK_RETURN_PRODUCT_RETURN_QUANTITY, stockReturnProducts.getReturnQuantity());
        values.put(COLUMN_STOCK_RETURN_PRODUCT_RETURN_VALUE, stockReturnProducts.getReturnValue());
        values.put(COLUMN_STOCK_RETURN_PRODUCT_UNIT_PRICE, stockReturnProducts.getReturnPrice());
        values.put(COLUMN_STOCK_RETURN_PRODUCT_IS_SYNC, stockReturnProducts.isSync());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_STOCK_RETURN_PRODUCTS, null, values);
        db.close();
        return id;
    }

    public ArrayList<StockReturnProducts> getStockReturnDetails() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_STOCK_RETURN_PRODUCTS;

        ArrayList<StockReturnProducts> stockReturnArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            StockReturnProducts stockReturn = new StockReturnProducts();
            stockReturn.setTabCode(Constants.TAB_CODE);
            stockReturn.setCompanyCode(Constants.COMPANY_CODE);
            stockReturn.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            stockReturn.setStockReturnProductId(id);
            stockReturn.setStockReturnNumber(cursor.getString(1));
            stockReturn.setProductCode(cursor.getString(2));
            stockReturn.setProductName(cursor.getString(3));
            stockReturn.setAvailQuantity(cursor.getString(4));
            stockReturn.setReturnQuantity(cursor.getString(5));
            stockReturn.setReturnPrice(cursor.getString(6));
            stockReturn.setReturnValue(cursor.getString(7));
            stockReturn.setCreatedAt(cursor.getString(8));
            stockReturn.setUpdatedAt(cursor.getString(9));

            stockReturnArrayList.add(stockReturn);
        }
        cursor.close();
        sqlWrite.close();
        return stockReturnArrayList;
    }

    /*public ArrayList<StockReturnProducts> getStockReturnProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT productCode FROM " + TABLE_STOCK_RETURN_PRODUCTS ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {} );

        ArrayList<StockReturnProducts> stockReturnProductsArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            StockReturnProducts op = new StockReturnProducts();
            op.setStockReturnProductId(cursor.getInt(0));
            op.setStockReturnNumber(cursor.getString(1));
            op.setProductCode(cursor.getString(2));
            op.setProductName(cursor.getString(3));
            op.setAvailQuantity(cursor.getString(4));
            op.setReturnQuantity(cursor.getString(5));

            stockReturnProductsArrayList.add(i,op);
            i++;
        }

        cursor.close();
        sqlWrite.close();
        return stockReturnProductsArrayList;
    }*/


}
