package in.kumanti.emzor.utils;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ToStringBuilder {
    private Object object;
    private Map<String, Object> fieldMap = new LinkedHashMap<>();
    private ToStringBuilder(Object object) {
        this.object = object;
    }

    public static ToStringBuilder from(Object object) {
        return new ToStringBuilder(object);
    }

    public ToStringBuilder field(String name, Object value) {
        fieldMap.put(name, value);
        return this;
    }

    public ToStringBuilder parent(ToStringBuilder parent) {
        fieldMap.putAll(parent.fieldMap);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(object.getClass().getSimpleName()).append(" {\n");
        for (Map.Entry<String, Object> field : fieldMap.entrySet()) {
            sb.append("  ").append(field.getKey()).append(": ");
            formatValue(sb, field.getValue());
            sb.append("\n");
        }
        sb.append("}\n");
        return sb.toString();
    }

    private void formatValue(StringBuilder sb, Object value) {
        if (value instanceof List<?>) {
            List list = (List) value;
            sb.append("[");
            int size = list.size();
            for (int i = 0; i < size; i++) {
                formatValue(sb, list.get(i));
                if (i != size - 1) {
                    sb.append(", ");
                }
            }
            sb.append("]");
        } else if (value instanceof Map) {
            Map map = (Map) value;
            sb.append("{");
            Iterator<Map.Entry> itr = map.entrySet().iterator();
            while (itr.hasNext()) {
                Map.Entry entry = itr.next();
                formatValue(sb, entry.getKey());
                sb.append(": ");
                formatValue(sb, entry.getValue());
                if (itr.hasNext()) {
                    sb.append(", ");
                }
            }
            sb.append("}");
        } else {
            sb.append(value);
        }
    }
}
