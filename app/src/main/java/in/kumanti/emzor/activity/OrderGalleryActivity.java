package in.kumanti.emzor.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.OrderGallery;

public class OrderGalleryActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    private TextView marqueeText, actionBarTitle;

    String loginIdString, orderNumberString;

    GridView gridView;
    ArrayList<OrderGallery> galleryImagesArrayList;
    OrderGallery orderGallery;
    OrderGalleryListAdapter galleryListAdapter;
    Bitmap selectedImage;

    MyDBHandler dbHandler;
    LpoImagesTable lpoImagesTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_order_gallery);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText("Gallery");
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        galleryImagesArrayList = new ArrayList<>();

        getOrderImages();

        actionbarBackButton.setOnClickListener(v -> finish());

    }

    private void getOrderImages() {
        Cursor data = lpoImagesTable.getOrderImages(loginIdString, orderNumberString);

        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {
                String filePath = data.getString(3);
                try {
                    File f = new File(homePath + File.separator + "images" + File.separator + filePath);
                    Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                    orderGallery = new OrderGallery(b);
                    galleryImagesArrayList.add(i, orderGallery);
                    i++;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            galleryListAdapter = new OrderGalleryListAdapter(galleryImagesArrayList);
            gridView.setAdapter(galleryListAdapter);

            gridView.setOnItemClickListener((parent, v, position, id) -> {

                OrderGallery item = galleryImagesArrayList.get(position);
                selectedImage = item.getData();

                if (selectedImage != null) {
                    Intent i1 = new Intent(getApplicationContext(), ImageViewActivity.class);
                    i1.putExtra("image", selectedImage);
                    startActivity(i1);

                }
            });
        }

    }

    private void populateHeaderDetails() {

        Bundle bundle = getIntent().getExtras();
        loginIdString = bundle.getString("login_id");
        orderNumberString = bundle.getString("order_number");

    }

    private void initializeViews() {

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        gridView = findViewById(R.id.gallery);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        lpoImagesTable = new LpoImagesTable(getApplicationContext());

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    @Override
    public void onBackPressed() {

    }

    public class OrderGalleryListAdapter extends BaseAdapter {

        private ArrayList<OrderGallery> users;

        @Override
        public int getCount() {
            return users.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            ViewHolder holder = null;

            if (row == null) {
                row = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_order_gallery, parent, false);
                holder = new ViewHolder();
                holder.image = row.findViewById(R.id.photo);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            OrderGallery item = users.get(position);

            holder.image.setImageBitmap(item.getData());
            return row;

        }

        public OrderGalleryListAdapter(ArrayList<OrderGallery> users) {
            this.users = users;
        }


        class ViewHolder {

            ImageView image;
        }

    }

}
