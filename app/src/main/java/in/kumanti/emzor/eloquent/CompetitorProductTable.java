package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.CompetitorProduct;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_COMPETITOR_PRODUCT;

public class CompetitorProductTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_CI_ID = "id";
    private static final String COLUMN_CI_PRODUCT_CODE = "productCode";
    private static final String COLUMN_CI_PRODUCT_NAME = "productName";
    private static final String COLUMN_CI_PRODUCT_MASTER_CODE = "productMasterCode";

    public Context context;

    public CompetitorProductTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPETITOR_PRODUCT);

        String competitorInfoDetailTable = "CREATE TABLE IF NOT EXISTS " + TABLE_COMPETITOR_PRODUCT + "(" +
                COLUMN_CI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CI_PRODUCT_CODE + " TEXT, " +
                COLUMN_CI_PRODUCT_NAME + " TEXT, " +
                COLUMN_CI_PRODUCT_MASTER_CODE + " TEXT " +
                ");";

        sqlWrite.execSQL(competitorInfoDetailTable);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(CompetitorProduct product) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CI_PRODUCT_CODE, product.getProductCode());
        values.put(COLUMN_CI_PRODUCT_NAME, product.getProductName());
        values.put(COLUMN_CI_PRODUCT_MASTER_CODE, product.getProductMasterCode());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_COMPETITOR_PRODUCT, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Competitor Products List details from the Master via Web Service
     */
    public void insertingCompetitorProductsMaster(ArrayList<CompetitorProduct> competitor_productList) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (competitor_productList != null) {
                for (int i = 0; i < competitor_productList.size(); i++) {
                    values.put(COLUMN_CI_PRODUCT_CODE, competitor_productList.get(i).getProductCode());
                    values.put(COLUMN_CI_PRODUCT_NAME, competitor_productList.get(i).getProductName());
                    values.put(COLUMN_CI_PRODUCT_MASTER_CODE, competitor_productList.get(i).getProductMasterCode());
                    db.insert(TABLE_COMPETITOR_PRODUCT, null, values);
                }
                db.setTransactionSuccessful();
            }


        } finally {
            db.endTransaction();
        }

    }

    public ArrayList<CompetitorProduct> getCompetitorProductsList() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT productCode,productName,productMasterCode FROM " + TABLE_COMPETITOR_PRODUCT;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        ArrayList<CompetitorProduct> productCompetitorArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            //Log.d("MYDATA","Data  id" + cursor.getInt(0));
            CompetitorProduct cd = new CompetitorProduct();
            cd.setProductCode(cursor.getString(0));
            cd.setProductName(cursor.getString(1));
            cd.setProductMasterCode(cursor.getString(2));
            productCompetitorArrayList.add(i, cd);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return productCompetitorArrayList;
    }

    public ArrayList<CompetitorProduct> getCompetitorProductsListByProductCode(String productCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT productCode,productName,productMasterCode FROM " + TABLE_COMPETITOR_PRODUCT + " WHERE " + COLUMN_CI_PRODUCT_MASTER_CODE + "=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{productCode});

        ArrayList<CompetitorProduct> productCompetitorArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            CompetitorProduct cd = new CompetitorProduct();
            cd.setProductCode(cursor.getString(0));
            cd.setProductName(cursor.getString(1));
            cd.setProductMasterCode(cursor.getString(2));
            productCompetitorArrayList.add(i, cd);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return productCompetitorArrayList;
    }

    public void deleteCompetitorProducts() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_COMPETITOR_PRODUCT, null, null);
        db.close();
    }

}
