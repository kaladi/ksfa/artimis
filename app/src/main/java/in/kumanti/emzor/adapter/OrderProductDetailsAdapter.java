package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.OpportunityProductDetails;

public class OrderProductDetailsAdapter extends RecyclerView.Adapter<OrderProductDetailsAdapter.ViewHolder> {

    public ArrayList<OpportunityProductDetails> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;

    public OrderProductDetailsAdapter(ArrayList<OpportunityProductDetails> myDataset) {
        mDataset = myDataset;
    }

    public void add(OpportunityProductDetails item1, int position) {
        mDataset.add(position, item1);
        Log.d("Recycle", "Data set Add" + position + "--" + mDataset.size());
        notifyItemInserted(position);
    }

    public void remove(OpportunityProductDetails item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_order_products_details, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productName, productUom, productQuantity, productPrice, productValue, productDesign;

        public ViewHolder(View v) {
            super(v);

            productName = v.findViewById(R.id.orderProductNameTextView);
            productUom = v.findViewById(R.id.orderProductUomTextView);
            productQuantity = v.findViewById(R.id.orderProductQuantityTextView);
            productPrice = v.findViewById(R.id.orderProductPriceTextView);
            productValue = v.findViewById(R.id.orderProductValueTextView);
            productDesign = v.findViewById(R.id.orderProductDesignTextView);
        }
    }


}