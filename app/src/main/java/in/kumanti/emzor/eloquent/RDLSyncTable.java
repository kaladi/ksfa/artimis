package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import in.kumanti.emzor.model.RDLSync;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_RDL_SYNC_HISTORY;

public class RDLSyncTable extends SQLiteOpenHelper {

    //Columns used for RDL POSTING DATA in duration of every 15 minutes to Web service
    private static final String COLUMN_RDL_SYNC_ID = "rdlSyncId";
    private static final String COLUMN_RDL_SYNC_DATE = "syncDate";
    private static final String COLUMN_RDL_SYNC_TIME = "syncTime";
    private static final String COLUMN_RDL_SYNC_CREATED_AT = "createdAt";
    private static final String COLUMN_RDL_SYNC_UPDATED_AT = "updatedAt";

    public Context context;

    public RDLSyncTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_RDL_SYNC_HISTORY);

        String logInOutTable = "CREATE TABLE IF NOT EXISTS " + TABLE_RDL_SYNC_HISTORY + "(" +
                COLUMN_RDL_SYNC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_RDL_SYNC_DATE + " TEXT, " +
                COLUMN_RDL_SYNC_TIME + " TEXT, " +

                COLUMN_RDL_SYNC_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_RDL_SYNC_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(logInOutTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(RDLSync rdlSync) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_RDL_SYNC_DATE, rdlSync.getPostSyncDate());
        values.put(COLUMN_RDL_SYNC_TIME, rdlSync.getPostSyncTime());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_RDL_SYNC_HISTORY, null, values);
        db.close();
        return id;

    }


    public String getRDLSyncDate() {
        String value = "";
        String selectQuery = "select syncDate from xxmsales_rdl_sync_history order by rdlSyncId desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(value);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");
            value = format.format(date1);
        } catch (Exception e) {

        }
        return value;
    }

    public String getRDLSyncTime() {
        String value = "";
        String selectQuery = "select syncTime from xxmsales_rdl_sync_history order by rdlSyncId desc LIMIT 1";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            value = cursor.getString(0);
            cursor.close();
        }

        return value;
    }


   /* public ArrayList<RDLSync> getRDLHistory() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_RDL_SYNC_HISTORY;

        ArrayList<RDLSync> rdlSyncArrayList = new ArrayList<>();
        int i = 0;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        while (cursor.moveToNext()) {

            RDLSync rdlSync = new RDLSync();
            rdlSync.setTabCode(Constants.TAB_CODE);
            rdlSync.setCompanyCode(Constants.COMPANY_CODE);
            rdlSync.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            rdlSync.setRdlSyncId(id);
            rdlSync.setPostSyncDate(cursor.getString(1));
            rdlSync.setPostSyncTime(cursor.getString(2));

            rdlSync.setCreatedAt(cursor.getString(4));
            rdlSync.setUpdatedAt(cursor.getString(5));

            rdlSyncArrayList.add(rdlSync);
        }
        cursor.close();
        sqlWrite.close();
        return rdlSyncArrayList;
    }*/

   /* public void updaterLogout(RDLSync rdlSync, String loginId){
        ContentValues values = new ContentValues();
        values.put(COLUMN_RDL_SYNC_DATE, rdlSync.getPostSyncDate());
        values.put(COLUMN_RDL_SYNC_TIME, rdlSync.getPostSyncTime());

        SQLiteDatabase db = getWritableDatabase();
        db.update(TABLE_RDL_SYNC_HISTORY, values, COLUMN_RDL_SYNC_ID + " = ?", new String[]{loginId});
        db.close();
    }*/

    /*public void deleteRDLHistory() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RDL_SYNC_HISTORY, null, null);
        db.close();
    }*/


}
