package in.kumanti.emzor.utils;

import android.os.Environment;

import java.io.File;

public class Constants {

    public static final String MY_ACTION = "action.SOME_ACTION";
    public static final int MY_JOB_INTENT_SERVICE_ID = 500;

//    //SaiSagar Live
//    public static final String FTP_HOST_NAME = "sfa.fmclgrp.com";
//    public static final String FTP_USER_NAME = "tts";
//    public static final String FTP_PASSWORD = "*tts*2010#";

  /*  public static final String FTP_HOST_NAME = "104.211.141.35";
    public static final String FTP_USER_NAME = "kaladiftp";
    public static final String FTP_PASSWORD = "Kaladiconserv@2020";*/

    public static final String FTP_HOST_NAME = "54.159.96.54";
    public static final String FTP_USER_NAME = "vistagftp";
    public static final String FTP_PASSWORD = "Vistag@2020";

    //SaiSagar Live
//    public static final String FTP_HOST_NAME = "kaladi.sg";
//    public static final String FTP_USER_NAME = "saisagar@kaladi.in";
//    public static final String FTP_PASSWORD = "ie]x]MH86uHC";

    /*public static final String FTP_HOST_NAME = "198.12.152.180";
    public static final String FTP_USER_NAME = "Emzor@emzorlan.com";
    public static final String FTP_PASSWORD = "Kum*emz#123";*/

    /*public static final String FTP_HOST_NAME = "182.74.154.75";
    public static final String FTP_USER_NAME = "emzor";
    public static final String FTP_PASSWORD = "Kum*emz#123";*/

    /*public static final String FTP_HOST_NAME = "41.216.174.84";
    public static final String FTP_USER_NAME = "Emzor";
    public static final String FTP_PASSWORD = "Kum*emz#123"; */

    public static final String UPGRADE_APK_URL = "http://182.74.154.75/websales/apk/mSales_4.0.apk";
    public static final String DATABASE_NAME = "/mnt/sdcard/KSFA/ksfaDB.DB";
    public static final int DATABASE_VERSION = 2;
    public static final String TABLE_CUSTOMER_STOCK_PRODUCT = "xxmsales_customer_stock_product";
    public static final String TABLE_COMPETITOR_STOCK_PRODUCT = "xxmsales_competitor_stock_product";
    public static final String TABLE_COMPETITOR_PRODUCT = "xxmsales_competitor_product";
    public static final String TABLE_SHIPPING_ADDRESS = "xxmsales_customer_shipping_address";
    public static final String TABLE_NOTES = "xxmsales_notes";
    // public static final String DATABASE_NAME = "/mnt/sdcard/DrugField/DrugField.DB";
    public static final String TABLE_PRODUCT_GROUP = "xxmsales_product_group";
    public static final String TABLE_PRODUCT = "xxmsales_item_details";
    public static final String TABLE_PRIMARY_INVOICE_PRODUCT = "xxmsales_primary_invoice_product";
    public static final String TABLE_PRIMARY_INVOICE = "xxmsales_primary_invoice";
    public static final String TABLE_QUOTATION = "xxmsales_quotation";
    public static final String TABLE_SURVEY_QUESTIONS_OPTION = "xxmsales_survey_questions_option";
    public static final String TABLE_SURVEY_QUESTIONS = "xxmsales_survey_questions";
    public static final String TABLE_SURVEY_RESPONSE_ANSWER = "xxmsales_survey_response_answer";
    public static final String TABLE_SURVEY_RESPONSE = "xxmsales_survey_response";
    public static final String TABLE_SURVEY = "xxmsales_survey";
    public static final String TABLE_WAREHOUSE = "xxmsales_warehouse";
    public static final String TABLE_WAREHOUSE_STOCK_PRODUCT = "xxmsales_warehouse_stock_product";
    public static final String TABLE_OPPORTUNITY = "xxmsales_opportunity";
    public static final String TABLE_OPPORTUNITY_PRODUCT_DETAILS = "xxmsales_opportunity_product_details";
    public static final String TABLE_ITEM_DETAILS = "xxmsales_item_details";
    public static final String TABLE_CUSTOMER_DETAILS = "xxmsales_customer_details";
    public static final String TABLE_OPPORTUNITY_ENQUIRY = "xxmsales_opportunity_enquiry";
    public static final String TABLE_OPPORTUNITY_ENQUIRY_PRODUCT_DETAILS = "xxmsales_opportunity_enquiry_product_details";
    public static final String TABLE_OPPORTUNITY_ATTACHMENT_TYPE = "xxmsales_opportunity_attachment_type";
    public static final String TABLE_LEDGER = "xxmsales_ledger";
    public static final String TABLE_AGE = "xxmsales_age";
    public static final String TABLE_EXPENSE_TYPE = "xxmsales_expense_type";
    public static final String TABLE_COMPANY_SETUP = "xxmsales_company_setup";
    public static final String TABLE_DISTRIBUTORS = "xxmsales_distributors";
    public static final String TABLE_LPO_IMAGES = "xxmsales_lpo_images";
    public static final String COLUMN_IS_SYNC = "is_sync";
    public static final String FOLDER_PATH = Environment.getExternalStorageDirectory() + File.separator
            + "KSFA" + File.separator + "OldOpportunity"
            + File.separator;
    public static final String MIN_ZERO = "0";
    public static final String MIN_NUMBER = "1";
    public static final String MAX_NUMBER = "9999999";
    public static final String TABLE_PHARMACY_INFO = "xxmsales_pharmacy_info";
    public static final String TABLE_PHARMACY_INFO_PRODUCTS = "xxmsales_pharmacy_info_products";
    public static final String TABLE_DOCTOR_VISIT_INFO = "xxmsales_doctor_visit_info";
    public static final String TABLE_DOCTOR_PRESCRIBED_PRODUCTS = "xxmsales_doctor_prescribed_products";
    public static final String TABLE_ARTIFACTS = "xxmsales_artifacts";
    public static final String TABLE_GENERAL_SETTINGS = "xxmsales_general_settings";
    public static final String TABLE_LOG_IN_OUT_HISTORY = "xxmsales_login_out_history";
    public static final String TABLE_RDL_SYNC_HISTORY = "xxmsales_rdl_sync_history";


    public static final String TABLE_CRM = "xxmsales_crm";
    public static final String TABLE_CRM_PRODUCTS = "xxmsales_crm_products";
    public static final String TABLE_CRM_NEW_PRODUCTS = "xxmsales_crm_new_products";
    public static final String TABLE_CRM_MEETINGS = "xxmsales_crm_meetings";
    public static final String TABLE_CRM_DETAILS = "xxmsales_crm_details";
    public static final String TABLE_CRM_STATUS = "xxmsales_crm_status";

    public static final String TABLE_CRM_COLD_CALL = "xxmsales_crm_cold_call";
    public static final String TABLE_CRM_OPPORTUNITY = "xxmsales_crm_opportunity";
    public static final String TABLE_CRM_OPPORTUNITY_PRODUCTS = "xxmsales_crm_opportunity_products";
    public static final String TABLE_CRM_QUOTATION = "xxmsales_crm_quotation";
    public static final String TABLE_CRM_QUOTATION_PRODUCTS = "xxmsales_crm_quotation_products";
    public static final String TABLE_CRM_ORDER = "xxmsales_crm_order";
    public static final String TABLE_CRM_ORDER_PRODUCTS = "xxmsales_crm_order_products";
    public static final String TABLE_CRM_ORDER_QUOTE_IMAGES = "xxmsales_crm_order_quote_images";
    public static final String TABLE_CRM_KEY_PERSONS = "xxmsales_key_persons";
    public static final String TABLE_CRM_KEY_PERSONS_ALERT = "xxmsales_key_persons_alert";
    public static final String TABLE_CRM_PRODUCT_GROUP = "xxmsales_crm_product_group";
    public static final String TABLE_EXPENSE = "xxmsales_expense";
    public static final String IMAGE_HOME_PATH = Environment.getExternalStorageDirectory() + File.separator
            + "KSFA" + File.separator;
    public static final String TABLE_SAMPLE_TEST = "xxmsales_sample_test";
    public static final String TABLE_STOCK_RETURN = "xxmsales_stock_return";
    public static final String TABLE_STOCK_RETURN_PRODUCTS = "xxmsales_stock_return_products";
    public static final String TABLE_CUSTOMER_RETURN = "xxmsales_customer_return";
    public static final String TABLE_CUSTOMER_RETURN_PRODUCTS = "xxmsales_customer_return_products";
    public static final String TABLE_STOCK_ISSUE = "xxmsales_stock_issue";
    public static final String TABLE_STOCK_ISSUE_PRODUCTS = "xxmsales_stock_issue_products";
    public static final String TABLE_BDE_LIST = "xxmsales_bde_list";
    public static final String TABLE_BDE_RECEIVE_FILES_LIST = "xxmsales_bde_receive_files_list";
    public static final String TABLE_BDE_RECEIVE_PRODUCTS = "xxmsales_bde_receive_products";
    public static final String TABLE_BDE_RECEIVE_BATCH_PRODUCTS = "xxmsales_bde_receive_batch_products";
    public static final String TABLE_NEW_JOURNEY_PLAN = "xxmsales_new_journey_plan";
    public static final String TABLE_NEW_JOURNEY_PLAN_CUSTOMERS = "xxmsales_new_journey_plan_customers";
    public static String COMPANY_NAME = "";
    public static String COMPANY_CODE = "";
    public static String TAB_CODE = "";
    public static String SR_CODE = "";
    public static String SR_NAME = "";
    public static String TAB_PREFIX = "";
    public static String BASE_URL = "";
    public static String VISIT_SEQ_NUMBER = "";
    public static String IMEI_NUMBER = "";

    public static int REQUEST_CODE_CUSTOMER_RETURN = 101;




}
