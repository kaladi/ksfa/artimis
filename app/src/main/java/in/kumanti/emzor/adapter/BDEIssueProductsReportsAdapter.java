package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.BDEIssueProductsReport;

public class BDEIssueProductsReportsAdapter extends RecyclerView.Adapter<BDEIssueProductsReportsAdapter.ViewHolder> {

    public ArrayList<BDEIssueProductsReport> mDataset;
    public Context context;

    public BDEIssueProductsReportsAdapter(ArrayList<BDEIssueProductsReport> myDataset) {
        mDataset = myDataset;
    }

    public void add(BDEIssueProductsReport item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(BDEIssueProductsReport item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public BDEIssueProductsReportsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_bde_issue_products_reports, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.issueDate.setText(mDataset.get(position).getBdeIssueDate());
        viewHolder.issueNumber.setText(mDataset.get(position).getBdeIssueNumber());
        viewHolder.toBdeName.setText(mDataset.get(position).getToBdeName());
        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.issueQuantity.setText(mDataset.get(position).getIssueQuantity());
        viewHolder.issuePrice.setText(mDataset.get(position).getIssuePrice());
        viewHolder.issueValue.setText(mDataset.get(position).getIssueValue());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView issueDate, issueNumber, toBdeName, productName, issueQuantity, issuePrice, issueValue;

        public ViewHolder(View v) {
            super(v);
            issueDate = v.findViewById(R.id.bdeIssueReportsDateTv);
            issueNumber = v.findViewById(R.id.bdeIssueReportsNumberTv);
            toBdeName = v.findViewById(R.id.toBdeNameReportsTv);
            productName = v.findViewById(R.id.bdeIssueReportsProductNameTv);
            issueQuantity = v.findViewById(R.id.bdeIssueReportsQtyTv);
            issuePrice = v.findViewById(R.id.bdeIssueReportsPriceTv);
            issueValue = v.findViewById(R.id.bdeIssueReportsValueTv);
        }
    }


}