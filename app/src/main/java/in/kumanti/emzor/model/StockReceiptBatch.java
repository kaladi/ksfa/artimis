package in.kumanti.emzor.model;

import java.io.Serializable;

public class StockReceiptBatch implements Serializable {

    public String error_batchNumber;
    public String error_batchQuantity;
    public String error_expiryDate;
    public String invoiceId;
    private String tabCode;
    private String productCode;
    private String expiryDate;
    private String batchNumber;
    private String batchQuantity;
    private String batchStatus;
    private String stockReceiptCode;
    private String companyCode;
    private String srCode;
    private String batchId;
    private String isExpired;
    private String stockQuantity;



    public String getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(String stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getStockReceiptCode() {
        return stockReceiptCode;
    }

    public void setStockReceiptCode(String stockReceiptCode) {
        this.stockReceiptCode = stockReceiptCode;
    }

    public String getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(String isExpired) {
        this.isExpired = isExpired;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getTabId() {
        return tabCode;
    }

    public void setTabId(String tabId) {
        this.tabCode = tabId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }


    public String getBatchQuantity() {
        return batchQuantity;
    }

    public void setBatchQuantity(String batchQuantity) {
        this.batchQuantity = batchQuantity;
    }

    public String getBatchStatus() {
        return batchStatus;
    }

    public void setBatchStatus(String batchStatus) {
        this.batchStatus = batchStatus;
    }

    public String getError_batchNumber() {
        return error_batchNumber;
    }

    public void setError_batchNumber(String error_batchNumber) {
        this.error_batchNumber = error_batchNumber;
    }

    public String getError_batchQuantity() {
        return error_batchQuantity;
    }

    public void setError_batchQuantity(String error_batchQuantity) {
        this.error_batchQuantity = error_batchQuantity;
    }

    public String getError_expiryDate() {
        return error_expiryDate;
    }

    public void setError_expiryDate(String error_expiryDate) {
        this.error_expiryDate = error_expiryDate;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

}
