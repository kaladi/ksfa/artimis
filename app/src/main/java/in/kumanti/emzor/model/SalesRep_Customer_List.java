package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class SalesRep_Customer_List {
    private String serial_num;
    private String customer_name;
    private String location_name;
    private String customer_id;
    private String customer_type;
    private String status;

    public SalesRep_Customer_List(String snum, String cname, String loc, String cust_id, String cust_type, String status1) {
        serial_num = snum;
        customer_name = cname;
        location_name = loc;
        customer_id = cust_id;
        customer_type = cust_type;
        status = status1;
    }

    public String getSerial_num() {
        return serial_num;
    }

    public void setSerial_num(String snum) {
        serial_num = snum;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String cname) {
        customer_name = cname;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation(String loc) {
        location_name = loc;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String cust_id) {
        customer_id = cust_id;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(String cust_type) {
        customer_type = cust_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status1) {
        status = status1;
    }


}

