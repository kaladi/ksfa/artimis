package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.model.CrmMeeting;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_MEETINGS;

public class CrmMeetingsTable extends SQLiteOpenHelper {

    //Columns used for Competitor Information details for a selected ordered product
    private static final String COLUMN_CRM_MEETING_ID = "meetingId";
    private static final String COLUMN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_CRM_NUMBER = "crmNo";
    private static final String COLUMN_CRM_DATE = "crmDate";
    private static final String COLUMN_CRM_MEETING_DATE = "meetingDate";
    private static final String COLUMN_CRM_MEETING_TYPE = "meetingType";
    private static final String COLUMN_CRM_MEETING_STATUS = "meetingStatus";
    private static final String COLUMN_CRM_MEETING_REMARKS = "remarks";
    private static final String COLUMN_CRM_MEETING_IS_SYNC = "isSync";
    private static final String COLUMN_CRM_MEETING_CREATED_AT = "createdAt";
    private static final String COLUMN_CRM_MEETING_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmMeetingsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

       // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_MEETINGS);
        String crmMeetingTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_MEETINGS + "(" +
                COLUMN_CRM_MEETING_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_CRM_NUMBER + " TEXT, " +
                COLUMN_CRM_DATE + " INTEGER, " +
                COLUMN_CRM_MEETING_DATE + " INTEGER, " +
                COLUMN_CRM_MEETING_TYPE + " INTEGER, " +
                COLUMN_CRM_MEETING_STATUS + " INTEGER, " +
                COLUMN_CRM_MEETING_REMARKS + " TEXT, " +
                COLUMN_CRM_MEETING_IS_SYNC + " TEXT, " +
                COLUMN_CRM_MEETING_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CRM_MEETING_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(crmMeetingTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(CrmMeeting crmMeeting) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_CODE, crmMeeting.getCustomerCode());
        values.put(COLUMN_CRM_NUMBER, crmMeeting.getCrmNumber());
        values.put(COLUMN_CRM_DATE, crmMeeting.getCrmDate());
        values.put(COLUMN_CRM_MEETING_DATE, crmMeeting.getMeetingDate());
        values.put(COLUMN_CRM_MEETING_TYPE, crmMeeting.getMeetingType());
        values.put(COLUMN_CRM_MEETING_STATUS, crmMeeting.getMeetingStatus());
        values.put(COLUMN_CRM_MEETING_REMARKS, crmMeeting.getRemarks());
        values.put(COLUMN_CRM_MEETING_IS_SYNC, crmMeeting.isSync());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_MEETINGS, null, values);
        db.close();
        return id;
    }

    public ArrayList<CrmMeeting> getCrmMeetings(String customerCode,String crmNumber) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_MEETINGS +" WHERE " + COLUMN_CUSTOMER_CODE + "=? AND " + COLUMN_CRM_NUMBER + "=? ORDER BY "+COLUMN_CRM_MEETING_ID+" DESC" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {customerCode,crmNumber} );

        ArrayList<CrmMeeting> crmOrderArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            CrmMeeting crmMeeting = new CrmMeeting();

            Log.d("View stock MYDATA--","Data  id" + cursor.getInt(0));
            crmMeeting.setMeetingId(cursor.getInt(0));
            crmMeeting.setCustomerCode(cursor.getString(1));
            crmMeeting.setCrmNumber(cursor.getString(2));
            crmMeeting.setCrmDate(cursor.getString(3));
            crmMeeting.setMeetingDate(cursor.getString(4));
            crmMeeting.setMeetingType(cursor.getString(5));
            crmMeeting.setMeetingStatus(cursor.getString(6));
            crmMeeting.setRemarks(cursor.getString(7));


            crmOrderArrayList.add(crmMeeting);
        }
        cursor.close();
        sqlWrite.close();
        return crmOrderArrayList;
    }


    public ArrayList<CrmMeeting> getCrmMeetingsForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CRM_MEETINGS + " WHERE DATE(datetime(createdAt,'unixepoch')) >= date('now','-5 Days')";

        ArrayList<CrmMeeting> crmMeetingsArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            CrmMeeting crmMeeting = new CrmMeeting();
            crmMeeting.setTabCode(Constants.TAB_CODE);
            crmMeeting.setCompanyCode(Constants.COMPANY_CODE);
            crmMeeting.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            crmMeeting.setMeetingId(id);
            crmMeeting.setCustomerCode(cursor.getString(1));
            crmMeeting.setCrmNumber(cursor.getString(2));
            crmMeeting.setCrmDate(cursor.getString(3));
            crmMeeting.setMeetingDate(cursor.getString(4));
            crmMeeting.setMeetingType(cursor.getString(5));
            crmMeeting.setMeetingStatus(cursor.getString(6));
            crmMeeting.setRemarks(cursor.getString(7));
            crmMeeting.setCreatedAt(cursor.getString(9));
            crmMeeting.setUpdatedAt(cursor.getString(10));

            crmMeetingsArrayList.add(crmMeeting);
        }
        cursor.close();
        sqlWrite.close();
        return crmMeetingsArrayList;
    }

    public ArrayList<CrmMeeting> getCrmMeetingHistory() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "select cm.crmNo, cm.crmDate, cus.customer_name, cm.remarks, cm.meetingDate, cm.meetingStatus, cm.meetingType from xxmsales_crm_meetings as cm left join xxmsales_customer_details as cus on cm.customerCode = cus.customer_id order by createdAt";

        //String selectQuery = "SELECT cm.crmNo, cm.crmDate, cm.customerCode, cm.remarks, cm.meetingDate, cm.meetingStatus FROM " + TABLE_CRM_MEETINGS + " as cm ";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});


        ArrayList<CrmMeeting> meetingHistoryArrayList = new ArrayList<CrmMeeting>();

        int i = 0;

        while (cursor.moveToNext()) {
            CrmMeeting crmMeeting = new CrmMeeting();
            crmMeeting.setCrmNumber(cursor.getString(0));
            crmMeeting.setCrmDate(cursor.getString(1));

            crmMeeting.setCustomerCode(cursor.getString(2));
            crmMeeting.setRemarks(cursor.getString(3));

            String sDate1 = cursor.getString(4);
            try {
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yy");

                String dateString = format.format(date1);
                crmMeeting.setMeetingDate(dateString);
            } catch (Exception e) {
            }
            crmMeeting.setMeetingStatus(cursor.getString(5));
            crmMeeting.setMeetingType(cursor.getString(6));

            meetingHistoryArrayList.add(i, crmMeeting);

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return meetingHistoryArrayList;
    }
}
