package in.kumanti.emzor.model;


import java.io.Serializable;

public class CrmColdCall implements Serializable {

    public int coldCallId;
    public String customerCode;
    public String coldCallNumber;
    public String coldCallDate;
    public String personsMet1;
    public String personsMet2;
    public String personsMet3;
    public String subject;
    public String outcomeType;
    public String outcomeDescription;
    public String visitDate;
    public String visitTime;
    public String visitAddress;
    public boolean isSync;
    public String latestSyncDate;
    public int randomNumber;
    public String createdAt;
    public String updatedAt;

    public String getColdCallNumber() {
        return coldCallNumber;
    }

    public void setColdCallNumber(String coldCallNumber) {
        this.coldCallNumber = coldCallNumber;
    }


    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public int getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(int randomNumber) {
        this.randomNumber = randomNumber;
    }

    public int getColdCallId() {
        return coldCallId;
    }

    public void setColdCallId(int coldCallId) {
        this.coldCallId = coldCallId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getColdCallDate() {
        return coldCallDate;
    }

    public void setColdCallDate(String coldCallDate) {
        this.coldCallDate = coldCallDate;
    }

    public String getPersonsMet1() {
        return personsMet1;
    }

    public void setPersonsMet1(String personsMet1) {
        this.personsMet1 = personsMet1;
    }

    public String getPersonsMet2() {
        return personsMet2;
    }

    public void setPersonsMet2(String personsMet2) {
        this.personsMet2 = personsMet2;
    }

    public String getPersonsMet3() {
        return personsMet3;
    }

    public void setPersonsMet3(String personsMet3) {
        this.personsMet3 = personsMet3;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getOutcomeType() {
        return outcomeType;
    }

    public void setOutcomeType(String outcomeType) {
        this.outcomeType = outcomeType;
    }

    public String getOutcomeDescription() {
        return outcomeDescription;
    }

    public void setOutcomeDescription(String outcomeDescription) {
        this.outcomeDescription = outcomeDescription;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    public String getVisitAddress() {
        return visitAddress;
    }

    public void setVisitAddress(String visitAddress) {
        this.visitAddress = visitAddress;
    }


    public String getLatestSyncDate() {
        return latestSyncDate;
    }

    public void setLatestSyncDate(String latestSyncDate) {
        this.latestSyncDate = latestSyncDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    @Override
    public String toString() {
        return outcomeType;
    }


}
