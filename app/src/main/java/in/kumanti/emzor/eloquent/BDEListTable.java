package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import in.kumanti.emzor.model.BDEList;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_BDE_LIST;
import static in.kumanti.emzor.utils.Constants.TABLE_BDE_RECEIVE_PRODUCTS;

public class BDEListTable extends SQLiteOpenHelper {

    //Columns used for Business Development Executive details
    private static final String COLUMN_BDE_ID = "bdeId";
    private static final String COLUMN_BDE_NAME = "bdeName";
    private static final String COLUMN_BDE_CODE = "bdeCode";
    private static final String COLUMN_BDE_CREATED_AT = "createdAt";
    private static final String COLUMN_BDE_UPDATED_AT = "updatedAt";

    public Context context;
    String selectQuery;
    Cursor cursor;

    public BDEListTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();

        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_BDE_LISTS);

        String bdeListsTable = "CREATE TABLE IF NOT EXISTS " + TABLE_BDE_LIST + "(" +
                COLUMN_BDE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_BDE_CODE + " TEXT, " +
                COLUMN_BDE_NAME + " TEXT, " +
                COLUMN_BDE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_BDE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(bdeListsTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(BDEList bdeList) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_BDE_CODE, bdeList.getBdeCode());
        values.put(COLUMN_BDE_NAME, bdeList.getBdeName());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_BDE_LIST, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Business Development Executive Name List details from the Master via Web Service
     */

    public void insertingBDEListMaster(ArrayList<BDEList> bdeListArrayList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (bdeListArrayList != null) {
                for (int i = 0; i < bdeListArrayList.size(); i++) {
                    values.put(COLUMN_BDE_CODE, bdeListArrayList.get(i).getBdeCode());
                    values.put(COLUMN_BDE_NAME, bdeListArrayList.get(i).getBdeName());
                    db.insert(TABLE_BDE_LIST, null, values);
                }
                db.setTransactionSuccessful();
            }

        } finally {
            db.endTransaction();
        }
    }

    public ArrayList<BDEList> getBDEList(String login_id) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BDE_LIST + " where bdeCode != ?  ORDER BY bdeName ASC";

        //String selectQuery = "SELECT * FROM " + TABLE_BDE_LIST +" where bdeCode != '"+bdeCode+"' ORDER BY bdeName ASC" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{login_id});

        ArrayList<BDEList> bdeListArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            BDEList bdeList = new BDEList();
            bdeList.setBdeId(cursor.getInt(0));
            bdeList.setBdeCode(cursor.getString(1));
            bdeList.setBdeName(cursor.getString(2));

            bdeListArrayList.add(bdeList);
        }
        cursor.close();
        sqlWrite.close();
        return bdeListArrayList;
    }

    public ArrayList<BDEList> getBdeByReceiveProducts() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BDE_LIST + " where bdeCode in (select DISTINCT fromBdeCode from " + TABLE_BDE_RECEIVE_PRODUCTS + ") ORDER BY bdeName ASC";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        ArrayList<BDEList> bdeListArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            BDEList bdeList = new BDEList();
            bdeList.setBdeId(cursor.getInt(0));
            bdeList.setBdeCode(cursor.getString(1));
            bdeList.setBdeName(cursor.getString(2));

            bdeListArrayList.add(bdeList);
        }
        cursor.close();
        sqlWrite.close();
        return bdeListArrayList;
    }

    public void deleteBdeList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BDE_LIST, null, null);
        db.close();
    }

}
