package in.kumanti.emzor.model;

public class TripDetails {
    private String tabCode;
    private String companyCode;
    private String srCode;
    private String tripNumber;
    private String tripDate;
    private String tripTime;
    private String startingKilometer;
    private String odometerMileage;
    private String tripName;
    private String randomNumber;
    private String tripStopDate;
    private String tripStopTime;
    private String odometerImage;
    private String tripFileName;

    public String getOdometerImage() {
        return odometerImage;
    }

    public void setOdometerImage(String odometerImage) {
        this.odometerImage = odometerImage;
    }

    public String getTripFileName() {
        return tripFileName;
    }

    public void setTripFileName(String tripFileName) {
        this.tripFileName = tripFileName;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getTripNumber() {
        return tripNumber;
    }

    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripTime() {
        return tripTime;
    }

    public void setTripTime(String tripTime) {
        this.tripTime = tripTime;
    }

    public String getStartingKilometer() {
        return startingKilometer;
    }

    public void setStartingKilometer(String startingKilometer) {
        this.startingKilometer = startingKilometer;
    }

    public String getOdometerMileage() {
        return odometerMileage;
    }

    public void setOdometerMileage(String odometerMileage) {
        this.odometerMileage = odometerMileage;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(String randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getTripStopDate() {
        return tripStopDate;
    }

    public void setTripStopDate(String tripStopDate) {
        this.tripStopDate = tripStopDate;
    }

    public String getTripStopTime() {
        return tripStopTime;
    }

    public void setTripStopTime(String tripStopTime) {
        this.tripStopTime = tripStopTime;
    }

    @Override
    public String toString() {
        return tripName;
    }
}
