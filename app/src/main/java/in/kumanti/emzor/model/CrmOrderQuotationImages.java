package in.kumanti.emzor.model;

public class CrmOrderQuotationImages {

    public boolean isSync;
    public String latestSyncDate;
    public String createdAt;
    public String updatedAt;
    private String quotationImageId;
    private String tabOrderId;
    private String quoteSeqNo;
    private String quoteImageName;
    private String quoteImagePath;

    public String getQuotationImageId() {
        return quotationImageId;
    }

    public void setQuotationImageId(String quotationImageId) {
        this.quotationImageId = quotationImageId;
    }

    public String getTabOrderId() {
        return tabOrderId;
    }

    public void setTabOrderId(String tabOrderId) {
        this.tabOrderId = tabOrderId;
    }

    public String getQuoteSeqNo() {
        return quoteSeqNo;
    }

    public void setQuoteSeqNo(String quoteSeqNo) {
        this.quoteSeqNo = quoteSeqNo;
    }

    public String getQuoteImageName() {
        return quoteImageName;
    }

    public void setQuoteImageName(String quoteImageName) {
        this.quoteImageName = quoteImageName;
    }

    public String getQuoteImagePath() {
        return quoteImagePath;
    }

    public void setQuoteImagePath(String quoteImagePath) {
        this.quoteImagePath = quoteImagePath;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getLatestSyncDate() {
        return latestSyncDate;
    }

    public void setLatestSyncDate(String latestSyncDate) {
        this.latestSyncDate = latestSyncDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
