package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class ShippingAddressMaster {

    private String customer_id;
    private String address_line1;
    private String address_line2;
    private String address_line3;
    private String city;
    private String state;


    public ShippingAddressMaster(
            String customer_id,
            String address_line1,
            String address_line2,
            String address_line3,
            String city,
            String state
    ) {

        this.customer_id = customer_id;
        this.address_line1 = address_line1;
        this.address_line2 = address_line2;
        this.address_line3 = address_line3;
        this.city = city;
        this.state = state;
    }

    public String getCustomer_id() {
        return customer_id;
    }


    public String getShip_address_line1() {
        return address_line1;
    }

    public String getShip_address_line2() {
        return address_line2;
    }

    public String getShip_address_line3() {
        return address_line3;
    }

    public String getShip_city() {
        return city;
    }

    public String getShip_state() {
        return state;
    }

}

