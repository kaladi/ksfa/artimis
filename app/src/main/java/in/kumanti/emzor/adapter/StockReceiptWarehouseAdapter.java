package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockReceipt;

public class StockReceiptWarehouseAdapter extends RecyclerView.Adapter<StockReceiptWarehouseAdapter.ViewHolder> {

    public ArrayList<StockReceipt> mDataset;
    public Context context;
    DecimalFormat decimalFormat;
    double priceDouble, totalPriceDouble;
    String formatPrice, formatTotalPrice, conversion;
    private RecyclerViewItemListener mListener;

    public StockReceiptWarehouseAdapter(ArrayList<StockReceipt> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(StockReceipt item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockReceipt item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockReceiptWarehouseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_receipt_warehouse, parent, false);
        context = parent.getContext();
        decimalFormat = new DecimalFormat("#,###.00");


        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.productUom.setText(mDataset.get(position).getProductUom());
        viewHolder.productQuantity.setText(mDataset.get(position).getProductQuantity());


        if (!TextUtils.isEmpty(mDataset.get(position).getProductPrice())) {
            priceDouble = Double.parseDouble(mDataset.get(position).getProductPrice());
            formatPrice = decimalFormat.format(priceDouble);
            viewHolder.productPrice.setText(formatPrice);
        }

        if (!TextUtils.isEmpty(mDataset.get(position).getProductTotalPrice())) {
            totalPriceDouble = Double.parseDouble(mDataset.get(position).getProductTotalPrice());
            formatTotalPrice = decimalFormat.format(totalPriceDouble);
            viewHolder.productTotalPrice.setText(formatTotalPrice);
        }


        if (!TextUtils.isEmpty(mDataset.get(position).getError_productName())) {
            viewHolder.productName.setError(mDataset.get(position).getError_productName());
        } else {
            viewHolder.productName.setError(null);
        }

        viewHolder.productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position < mDataset.size() && mDataset.get(position).getBatchControlled() != null && mDataset.get(position).getBatchControlled().equals("Yes")) {
                    viewHolder.mListener.onClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView productUom, productQuantity, productPrice, productTotalPrice;
        public Button productName;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;
            productName = v.findViewById(R.id.stockReceiptWarProdNameTv);
            productUom = v.findViewById(R.id.stockReceiptWarProdUomTv);
            productQuantity = v.findViewById(R.id.stockReceiptWarProdQuantityTv);
            productPrice = v.findViewById(R.id.stockReceiptWarProPriceTv);
            productTotalPrice = v.findViewById(R.id.stockReceiptWarProdTotalPriceTv);

        }
    }


}