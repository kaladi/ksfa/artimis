package in.kumanti.emzor.model;

public class CustomerReturnProductsReport {

    public int cusReturnProdId;
    public String cusReturnDate;
    public String cusReturnNumber;
    public String customerName;
    public String productCode;
    public String productName;
    public String returnQuantity;
    public String returnPrice;
    public String returnValue;
    public String createdAt;
    public String updatedAt;
    private String invoiceNum;

    public int getCusReturnProdId() {
        return cusReturnProdId;
    }

    public void setCusReturnProdId(int cusReturnProdId) {
        this.cusReturnProdId = cusReturnProdId;
    }

    public String getCusReturnDate() {
        return cusReturnDate;
    }

    public void setCusReturnDate(String cusReturnDate) {
        this.cusReturnDate = cusReturnDate;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getCusReturnNumber() {
        return cusReturnNumber;
    }

    public void setCusReturnNumber(String cusReturnNumber) {
        this.cusReturnNumber = cusReturnNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }


    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
