package in.kumanti.emzor.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.FeedbackHistoryAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.mastersData.FeedbackType;
import in.kumanti.emzor.model.FeedbackReport;


public class FeedbackHistoryActivity extends MainActivity {

    public RecyclerView feedbackReportsRecyclerView;
    String login_id, feedback_type = "";
    ImageView actionbarBackButton, deviceInfo;
    String checkin_time = "";
    String customer_id = "";
    TextView actionbarTitle, feedbackDateTextView,
            feedbackTypeTextView,
            actionTakenTextView,
            customerNameTextView,
            customerFeedbackTextView;
    MyDBHandler myDBHandler;
    private RecyclerView.Adapter feedbackReportsAdapter;
    private RecyclerView.LayoutManager feedbackReportsLayoutManager;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_feedback_history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("History");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        //Populate the Quotation Product details using the Recycler view
        feedbackReportsRecyclerView.setHasFixedSize(true);
        feedbackReportsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        feedbackReportsRecyclerView.setLayoutManager(feedbackReportsLayoutManager);

        try {
            ArrayList<FeedbackType> FeedbackTypeArrayList = myDBHandler.getFeedbackFromBachEnd(customer_id);
//            System.out.println("FeedbackTypeArrayList = " + FeedbackTypeArrayList);
            if(FeedbackTypeArrayList.size()==0) {
                ArrayList<FeedbackReport> feedbackReportsList = myDBHandler.getFeedbackHistory(customer_id);
                Log.d("QueryDataList ", "" + feedbackReportsList.size());
//                System.out.println("feedbackReportsList.size() = " + feedbackReportsList.size());
                feedbackReportsAdapter = new FeedbackHistoryAdapter(feedbackReportsList);
                feedbackReportsRecyclerView.setAdapter(feedbackReportsAdapter);
            }
            else{

                if (FeedbackTypeArrayList != null) {
                    for (int i = 0; i < FeedbackTypeArrayList.size(); i++) {
//                        System.out.println("FeedbackTypeArrayListAction_taken() = " + FeedbackTypeArrayList.get(i).getAction_taken());
//                        System.out.println("FeedbackTypeArrayListgetStatus() = " + FeedbackTypeArrayList.get(i).getStatus());
                        dbHandler.updateFeedback(FeedbackTypeArrayList.get(i).getAction_taken(), FeedbackTypeArrayList.get(i).getStatus(),FeedbackTypeArrayList.get(i).getFeedback_id(),customer_id);
                    }
                }


                ArrayList<FeedbackReport> feedbackReportsList = myDBHandler.getFeedbackHistory(customer_id);
                Log.d("QueryDataList ", "" + feedbackReportsList.size());
                feedbackReportsAdapter = new FeedbackHistoryAdapter(feedbackReportsList);
                feedbackReportsRecyclerView.setAdapter(feedbackReportsAdapter);
            }
        } catch (Exception e) {
            Log.d("QueryDataResponse", "" + e.getMessage());
        }


        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }


    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        feedbackReportsRecyclerView = findViewById(R.id.feedbackReportsList);
        myDBHandler = new MyDBHandler(getApplicationContext(), null, null, 1);


        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        feedbackDateTextView = findViewById(R.id.feedbackDateTextView);
        feedbackTypeTextView = findViewById(R.id.feedbackTypeTextView);
        actionTakenTextView = findViewById(R.id.actionTakenTextView);
        customerNameTextView = findViewById(R.id.customerNameTextView);
        customerFeedbackTextView = findViewById(R.id.customerFeedbackTextView);


    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            feedback_type = bundle.getString("feedback_type");

            Log.d("LoginId", login_id);
        }
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

}
