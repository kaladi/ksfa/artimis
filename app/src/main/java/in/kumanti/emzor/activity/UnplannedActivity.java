package in.kumanti.emzor.activity;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.SalesRepCustomerListAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.SalesRep_Customer_List;
import in.kumanti.emzor.utils.Globals;


public class UnplannedActivity extends MainActivity {

    ImageView info, back;
    MyDBHandler dbHandler;
    String login_id = "", tripno1;
    ImageView new_customer;
    String trip_num = null;
    int serial_num = 0;

    Globals globals;
    int numRows1 = 0;
    ArrayList<SalesRep_Customer_List> userList;
    SalesRepCustomerListAdapter adapter;
    SalesRep_Customer_List user;

    TextView actionbarTitle, marqueeText;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    String user_role;
    EditText searchCustomerName;
    String customerName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_unplanned);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("Unplanned Customers");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        fetchUnPlannedCustomers();


        searchCustomerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //UnplannedActivity.this.adapter.getFilter().filter(s);
                customerName = s.toString();
                // Log.d("Unplanned Customers", "CustomerName---" +customerName);
                fetchUnPlannedCustomers();

                //adapter.notifyDataSetChanged();


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        new_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myintent = new Intent(getApplicationContext(), NewCustomerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("login_id", login_id);
                myintent.putExtras(bundle);
                startActivity(myintent);


            }
        });

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myintent = new Intent(getApplicationContext(), StartStopActivity.class);

                //Create the bundle
                Bundle bundle = new Bundle();

                //Add your data to bundle
                //bundle.putString("checkin_time", checkin_time);
                bundle.putString("login_id", login_id);
                bundle.putString("trip_num", trip_num);
                bundle.putString("Home", "Home");


                //Add the bundle to the intent
                myintent.putExtras(bundle);

                //Fire that second activity
                startActivity(myintent);
            }
        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

    }

    private void fetchUnPlannedCustomers() {
        //Working with List to Show Data's


        //
        userList = new ArrayList<SalesRep_Customer_List>();
        Cursor data = dbHandler.getUnplannedCustomers(login_id, trip_num, customerName);


        numRows1 = data.getCount();
        if (numRows1 == 0) {
            // Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                //serial_num = serial_num + 1;
                user = new SalesRep_Customer_List(data.getString(5), data.getString(2), data.getString(3), data.getString(0), data.getString(4), data.getString(5));
                userList.add(i, user);
                System.out.println("TTT::data.getString(3) = " + data.getString(2));
                System.out.println("TTT::data.getString(3) = " + data.getString(3));
                Log.d("Unplanned Customers", "Customer Name" + user.getCustomer_name() + " ---" + data.getString(6));
                //System.out.println(userList.get(i).getFirstName());
                i++;

            }

            adapter = new SalesRepCustomerListAdapter(getApplicationContext(), R.layout.list_unplannedcustomer_start_stop, userList, login_id, tripno1, user_role);
            ListView customerTable = findViewById(R.id.customer_list);
            customerTable.setAdapter(adapter);
            serial_num = 0;

        }
    }

    private void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        globals = ((Globals) getApplicationContext());
        tripno1 = globals.getTrip_num();

        new_customer = findViewById(R.id.new_customer);

        searchCustomerName = findViewById(R.id.customerNameEditText);


    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();

        login_id = bundle.getString("sales_rep_id");
        user_role = bundle.getString("user_role");


    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();


    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == 3) {

            //Create the bundle
            Bundle bundle = new Bundle();

            bundle.putString("login_id", login_id);
            bundle.putString("trip_num", trip_num);
            bundle.putString("Home", "Home");


        }
    }
}
