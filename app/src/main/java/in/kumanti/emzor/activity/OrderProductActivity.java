package in.kumanti.emzor.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.CustomerShippingAddressTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.model.CustomerShippingAddress;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.LPOImages;
import in.kumanti.emzor.model.OrderProductItems;
import in.kumanti.emzor.model.ResponseJson;
import in.kumanti.emzor.signature.SignaturePad;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.GPSTracker;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.InputFilterMinMax;
import in.kumanti.emzor.utils.Print;
import in.kumanti.emzor.utils.PrintBluetooth;
import in.kumanti.emzor.utils.SharedPreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.utils.Constants.MAX_NUMBER;
import static in.kumanti.emzor.utils.Constants.MIN_NUMBER;


public class OrderProductActivity extends MainActivity implements View.OnClickListener {

    private static final int SELECT_PHOTO1 = 1;
    private static final int CAPTURE_PHOTO1 = 2;
    EditText customerSignatureName;
    Spinner Customer_Name, Product;
    Button orderAddProductButton;
    ImageView back, deviceInfo, delivery_date_calendar, orderSaveButton, customerImage, sr_image, delivery_addrs, captureLpoImage, orderCancelButton, showLpoImagesGallery;
    TextView total_value;
    ArrayList<OrderProductItems> orderProductItems;
    OrderProductItems listSalesLines1;
    String sales_rep_id = null;
    int order_id = 0;
    int random_num = 0;
    ImageView img_date, createSignature, viewSignature, img_calender;
    Bitmap selectedImage = null;
    TextView txt_date, header_txt;
    String selected_date;
    String template_name;
    int template_id = 0;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    DatePickerDialog.OnDateSetListener sdate;
    Calendar myCalendar = Calendar.getInstance();
    Bitmap bmp = null;
    String customer_id = "", login_id;
    String checkin_time = "";
    int line_count;
    TextView customerName, city, checkInTimeText, order_date, order_num, sr_name, txt_calender, add1, add2;
    String customerNameString, customerEmailAddress;
    String customerAddress1, customerAddress2, customerAddress3;
    String salesorder_date, sr_name1, template, tab_prefix;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1, order_time;
    String sr_name_details, sr_rm_name, sr_region, sr_warehouse, save_salesorder_date, save_salesorder_time;
    EditText remarks, lpo_number;
    Chronometer duration;
    long stopTime = 0;
    String order_type = "";
    String salesorder_status, salesorder_value, salesorder_lines;
    int a[] = new int[5];
    double order_val = 0.00;
    DecimalFormat formatter, formatter1, formatter2;
    String format_order_value;
    Double price_val_update = 0.00;
    Double order_val_update = 0.00;
    SharedPreferenceManager sharedPreferenceManager;
    String order_count;
    GPSTracker gpsTracker;
    double latitude, longitude;
    String lat_lng, nav_type, order_number;
    int order_line_id = 0;
    LinearLayout headerDurationContainer, btn_section;
    int image_id = 0;
    ViewPager addressViewPager;
    ArrayList<CustomerShippingAddress> shippingAddressesData = null;
    CustomerShippingAddressTable shippingAddressesTable;
    TextView actionbarTitle;
    SimpleDateFormat deliveryDateFormat;
    TextView notification_value;
    LinearLayout notification;
    Button notification_bell;
    String emailType = "Order";
    String companyName, companyCode1,compAdd1, compAdd2, compAdd3, compMblNo;
    //Variables declaration for print and sms
    double orderTotalValue = 0;
    double vatValue;
    double netValue;
    Button smsButton, printButton, emailButton;
    String customerPhoneNumber = "";
    String smsMessageText = "";
    // android built in classes for bluetooth operations
    String printerName = "MP80-17031810239";
    String printMsg;
    Print print;
    PrintBluetooth printBluetooth;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    String orderStatus = "";
    GeneralSettingsTable generalSettingsTable;
    MyDBHandler dbHandler;
    Globals globals;
    LpoImagesTable lpoImagesTable;
    String encodedImage;
    GeneralSettings gs;
    private int mYear, mMonth, mDay;
    private PopupMenu popupMenu;
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    private TextView marqueeText, accountNo;
    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";
    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    public static void hideSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_order_product);

        initializeViews();
        populateHeaderDetails();


        header_txt.setText("View Order");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        notification.setVisibility(View.VISIBLE);
        Print print = new Print(getApplicationContext());

        SimpleDateFormat deliveryDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getlocation();
        setDateTime();


        formatter = new DecimalFormat("#,###.00");
        formatter1 = new DecimalFormat("#,###");
        formatter2 = new DecimalFormat("####");


        long date = System.currentTimeMillis();
        SimpleDateFormat sdfd = new SimpleDateFormat("dd MMM yy");
        SimpleDateFormat sdft = new SimpleDateFormat("hh : mm");
        salesorder_date = sdfd.format(date);
        order_date.setText(salesorder_date);
        order_num.setText(order_number);
        checkInTimeText.setText(checkin_time);
        order_val = dbHandler.getOrderLinesValue(Integer.toString(order_id));
        format_order_value = formatter.format(order_val);

        // total_value.setText(Integer.toString(order_val));
        total_value.setText(format_order_value);

        if ("Reports".equals(nav_type)) {

            orderSaveButton.setVisibility(View.GONE);
            orderCancelButton.setVisibility(View.GONE);
            orderAddProductButton.setVisibility(View.GONE);
            // btn_section.setVisibility(View.GONE);
            // back_section.setVisibility(View.GONE);
            captureLpoImage.setEnabled(false);
            delivery_addrs.setEnabled(false);
            img_calender.setEnabled(false);
            createSignature.setEnabled(false);
            lpo_number.setEnabled(false);
            remarks.setEnabled(false);
            headerDurationContainer.setVisibility(View.GONE);
            //gallery.setEnabled(false);
            orderStatus = "Completed";
        }


        delivery_addrs.setOnClickListener(v -> loadShippingAddress());

        notification_bell.setOnClickListener(v -> {

            android.app.AlertDialog.Builder alertbox = new android.app.AlertDialog.Builder(v.getRootView().getContext());
            alertbox.setTitle("You have Focus Products to Add");

            alertbox.setNegativeButton("Close", (dialog, which) -> dialog.dismiss());

            alertbox.show();

        });

        sr_image.setOnLongClickListener(v -> {
            showsrdetails();
            return true;
        });

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);

        });

        customerImage.setOnLongClickListener(v -> {
            showcustomerLocation();
            return true;
        });

        captureLpoImage.setOnClickListener(v -> {

            if ("".equals(lpo_number.getText().toString())) {

            } else {
                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent1, CAPTURE_PHOTO1);
            }

        });

        /*
         * Print the order
         */
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!orderStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save order to print", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    int totalSize = 48;
                    String ESC_NEW_LINE = "\n";
                    String horizontalLine = "-----------------------------------------------";
                    printMsg = "";
                    printMsg += print.centerString(totalSize, companyName);
                    printMsg += print.centerString(totalSize, compAdd1);
                    printMsg += print.centerString(totalSize, compAdd2);
                    printMsg += print.centerString(totalSize, compAdd3);
                    printMsg += print.centerString(totalSize, compMblNo);

                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE + ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + "Order Number:" + order_number;
                    printMsg += ESC_NEW_LINE + print.padRight("Date:" + order_date.getText(), 24) + print.padLeft("TIME:" + order_time, 24);
                    printMsg += "BDE Name:" + sr_name_details;
                    printMsg += ESC_NEW_LINE + "Customer Name:" + customerNameString;
                    printMsg += ESC_NEW_LINE + "Address:";
                    printMsg += print.padRight(customerAddress1, totalSize);
                    printMsg += print.padRight(customerAddress2, totalSize);
                    printMsg += print.padRight(customerAddress3, totalSize);

                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += print.centerString(15, "Product Name") + " " + "  VOL " + print.padLeft("PRICE", 10) + "  " + print.padLeft("VALUE", 14);

                    printMsg += ESC_NEW_LINE + horizontalLine;
                    int i = 1;
                    for (OrderProductItems ls : orderProductItems) {
                        printMsg += print.padLeft(String.valueOf(i) + ".", 3) + print.padRight(ls.getProduct().substring(0, 11), 12) + " " + print.padLeft(String.valueOf(ls.getQuantity()), 5) + " " + print.padLeft(ls.getPrice(), 10) + "  " + print.padLeft(ls.getValue(), 14);
                        orderTotalValue += ls.getValue_db();
                        i++;
                    }

                    // vatValue = order_val*5/100;
                    // netValue = order_val+vatValue;
                    Log.d("VatTest", "OrderVat" + vatValue);
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.padRight("", 15) + print.padRight("Total Value:", 19) + print.padLeft(format_order_value, 14);
                    // printMsg += print.padRight("",15)+print.padRight("VAT:",19)+print.padLeft(formatter.format(vatValue),14);
                    // printMsg += print.padRight("",15)+print.padRight("Net Value:",19)+print.padLeft(formatter.format(netValue),14);


                    printMsg += ESC_NEW_LINE + horizontalLine;

                    printMsg += ESC_NEW_LINE + "LPO Number:" + lpo_number.getText();
                    printMsg += ESC_NEW_LINE + "Shipment Date:" + txt_date.getText();
                    printMsg += ESC_NEW_LINE + "Remarks:" + remarks.getText();
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE + horizontalLine;
                    printMsg += print.padLeft("Customer Sign", totalSize);
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    printMsg += ESC_NEW_LINE;
                    print.printContent(printMsg);
                    //printBluetooth.printContent(printMsg, selectedImage);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });


        /**
         * Sending SMS Manager API using default SMS manager
         */
        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!orderStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save order to send sms", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(customerPhoneNumber)) {
                    Toast.makeText(getApplicationContext(), "Customer Phone number is not available", Toast.LENGTH_LONG).show();
                    return;
                }
                line_count = dbHandler.getOrderLinesCount(Integer.toString(order_id));


                smsMessageText = "Order received\n" +
                        "\n" +
                        "Thanks for trusting " + Constants.COMPANY_NAME + " Products." +
                        "\n" +
                        "Please refer your"+
                        "\n" +
                        "Order No: #" +
                        order_number + "\n" +
                        "Ord Date: " + order_date.getText().toString() + "\n" +
                        "Ord Value: " + "NGN " + format_order_value + "\n" +
                        "Delivery Date: " + order_date.getText().toString() + "\n" +
                        "care.nigeria@artemislife.com"+ "\n" ;
                System.out.println("smsMessageTextoo = " + smsMessageText);

                Gson gson = new GsonBuilder().setLenient().create();
                JsonArray ordersJsonArray = gson.toJsonTree(orderProductItems).getAsJsonArray();

                JsonObject jp = new JsonObject();
                jp.addProperty("smsType", emailType);
                jp.addProperty("smsMessageText", smsMessageText);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);
                jp.addProperty("orderNumber", order_number);
                jp.addProperty("date", order_date.getText().toString());
                jp.addProperty("time", order_time);
                jp.addProperty("bdeName", sr_name_details);
                jp.addProperty("customerName", customerNameString);
                jp.addProperty("customerPhoneNumber", customerPhoneNumber);
                jp.addProperty("address", customerAddress1 + customerAddress2);
                jp.addProperty("orderLines", line_count);
                jp.addProperty("totalValue", format_order_value);
                jp.addProperty("shipmentDate", txt_date.getText().toString());

                System.out.println("TTT::gson.toJson(jp) = " + gson.toJson(jp));

                Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.SMS_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);

                System.out.println("TTTT:::smsjp = " + jp.toString());
                System.out.println("TTTT:::smsjp1 = " + jp);
                Call<ResponseBody> call = api.postingInvoiceSMSData(jp);
                Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND", "Success----" + response.body().string());
                            System.out.println("TTTT::response.body().string() = " + response.body().string());
                            ResponseBody resultSet = response.body();
                            System.out.println("resultSet = " + resultSet);

                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }
                });

               /* try {
                    smsMessageText = "Order received\n" +
                            "\n" +
                            "Order No: #" +
                            order_number + "\n" +
                            "Order Date: " + order_date.getText().toString() + "\n" +
                            "Order Time: " + order_time + "\n" +
                            "Order Lines: " + line_count + "\n" +
                            "Order Value: " + format_order_value + " NGN\n" +
                            "Delivery Date: " + order_date.getText().toString() + "\n";
                    Log.d("SMSTEST", "TESTMESSAGE" + smsMessageText);

                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(customerPhoneNumber, null, smsMessageText, null, null);
                    Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.d("Sms", "Exception" + e);
                }*/

            }
        });


        /**
         * Sending email using webService
         */
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!orderStatus.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), "Please save order to send email", Toast.LENGTH_LONG).show();
                    return;
                }
                Gson gson = new GsonBuilder().setLenient().create();
                JsonArray ordersJsonArray = gson.toJsonTree(orderProductItems).getAsJsonArray();

                JsonObject jp = new JsonObject();
                jp.addProperty("emailType", emailType);
                jp.addProperty("companyName", companyName);
                jp.addProperty("companyCode", companyCode1);
                jp.addProperty("companyAddLine1", compAdd1);
                jp.addProperty("companyAddLine1", compAdd2);
                jp.addProperty("companyAddLine1", compAdd3);
                jp.addProperty("companyMblNo", compMblNo);
                jp.addProperty("orderNumber", order_number);
                jp.addProperty("date", order_date.getText().toString());
                jp.addProperty("time", order_time);
                jp.addProperty("bdeName", sr_name_details);
                jp.addProperty("customerName", customerNameString);
                jp.addProperty("customerEmailAddress", customerEmailAddress);
                jp.addProperty("address", customerAddress1 + customerAddress2);
                jp.add("orderLines", ordersJsonArray);
                jp.addProperty("totalValue", format_order_value);
                // jp.addProperty("vat", formatter.format(vatValue));
                // jp.addProperty("netValue", formatter.format(netValue));
                jp.addProperty("lpoNumber", lpo_number.getText().toString());
                jp.addProperty("shipmentDate", txt_date.getText().toString());
                jp.addProperty("remarks", remarks.getText().toString());


                if (selectedImage != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                    jp.addProperty("customerSignature", encodedImage);
                }

                Log.d("EMAIL SEND", "Posting Data---- " + gson.toJson(jp));


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);

                System.out.println("TTTT:::jp = " + jp.toString());
                System.out.println("TTTT:::jp1 = " + jp);
                Call<ResponseBody> call = api.postingEmailData(jp);
                Toast.makeText(getApplicationContext(), "Mail Sent", Toast.LENGTH_LONG).show();


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND", "Success----" + response.body().string());
                            System.out.println("TTTT::response.body().string() = " + response.body().string());
                            ResponseBody resultSet = response.body();
                            System.out.println("resultSet = " + resultSet);

                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

        back.setOnClickListener(v -> {

            if ("Reports".equals(nav_type)) {
                finish();
            } else if ((orderStatus.equals("Completed")) || (orderStatus.equals("Pending for Waybill"))) {
                Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                duration.stop();
                Bundle bundle = new Bundle();
                bundle.putString("checkin_time", checkin_time);
                bundle.putString("sales_rep_id", login_id);
                bundle.putString("customer_id", customer_id);
                bundle.putString("nav_type", "Home");

                myintent.putExtras(bundle);
                startActivity(myintent);
                return;
            } else {
                finish();
//                    Intent myintent = new Intent(getApplicationContext(), OrderActivity.class);
//                    long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
//                    SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
//                    duration.stop();
//                    //Create the bundle
//                    Bundle bundle = new Bundle();
//
//                    //Add your data to bundle
//                    bundle.putString("checkin_time", checkin_time);
//                    bundle.putString("customer_id", customer_id);
//                    bundle.putInt("order_id", order_id);
//                    bundle.putString("order_type", "Lines");
//                    bundle.putString("login_id", login_id);
//                    bundle.putString("order_num", order_num.getText().toString());
//
//                    //Add the bundle to the intent
//                    myintent.putExtras(bundle);
//
//                    //Fire that second activity
//                    startActivity(myintent);
            }


        });

        orderCancelButton.setOnClickListener(v -> {
            final Dialog alertbox = new Dialog(v.getRootView().getContext());

            LayoutInflater mInflater = (LayoutInflater)
                    getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            final View alertLayout = mInflater.inflate(R.layout.activity_exit_order_popup, null);

            alertbox.setCancelable(false);
            alertbox.setContentView(alertLayout);

            final Button Yes = alertLayout.findViewById(R.id.Yes);
            final Button No = alertLayout.findViewById(R.id.No);

            Yes.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(final View v) {

                    if (order_id == 0) {
                        Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
                        long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
                        SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
                        duration.stop();
                        Bundle bundle = new Bundle();
                        bundle.putString("checkin_time", checkin_time);
                        bundle.putString("sales_rep_id", login_id);
                        bundle.putString("customer_id", customer_id);
                        bundle.putString("nav_type", "Home");

                        myintent.putExtras(bundle);
                        startActivity(myintent);
                    } else {
                        update_order_status("Cancelled");
                        Toast.makeText(getApplicationContext(), "Order Cancelled", Toast.LENGTH_LONG).show();
                    }

                }

            });

            No.setOnClickListener(v1 -> alertbox.dismiss());

            alertbox.show();

        });

        createSignature.setOnClickListener(v -> createNewSignature());

        viewSignature.setOnClickListener(v -> {
            if (selectedImage != null) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.image_preview, null);
                ImageView capturedImage, closeButton;

                capturedImage = alertLayout.findViewById(R.id.capturedImage);
                closeButton = alertLayout.findViewById(R.id.closeImageView);
                capturedImage.setImageBitmap(selectedImage);

                android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(v.getRootView().getContext());

                alert.setView(alertLayout);
                alert.setNegativeButton("Close", (dialog, which) -> {
                });

                alert.setCancelable(true);

                android.app.AlertDialog dialog = alert.create();
                dialog.show();


            }

        });

        showLpoImagesGallery.setOnClickListener(v -> {

            int lpo_image_count = lpoImagesTable.get_lpo_order_image_count(order_number);

            if (lpo_image_count == 0) {
                Toast.makeText(OrderProductActivity.this, "No Images,Capture and Check again.", Toast.LENGTH_SHORT).show();
            } else {
                Intent myintent = new Intent(getApplicationContext(), OrderGalleryActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("customer_id", customer_id);
                bundle.putString("login_id", login_id);
                bundle.putString("order_number", order_number);
                myintent.putExtras(bundle);

                startActivity(myintent);
            }


        });

        orderAddProductButton.setOnClickListener(v -> {

            dbHandler.update_order_header_info(Integer.toString(order_id), lpo_number.getText().toString(), remarks.getText().toString(), txt_calender.getText().toString(), add1.getText().toString(), add2.getText().toString());

            Intent myintent = new Intent(getApplicationContext(), OrderActivity.class);
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();

            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putInt("order_id", order_id);
            bundle.putString("order_type", "Lines");
            bundle.putString("login_id", login_id);
            bundle.putString("order_num", order_num.getText().toString());
            myintent.putExtras(bundle);

            startActivity(myintent);
        });

        orderSaveButton.setOnClickListener(v -> {
            popupMenu = new PopupMenu(OrderProductActivity.this, v);
            popupMenu.setOnDismissListener(new OnDismissListener());
            popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
            popupMenu.inflate(R.menu.popup_menu);
            popupMenu.show();

        });

        orderProductItems = new ArrayList<>();
        Cursor data = dbHandler.getOrderLines(Integer.toString(order_id));
        int numRows1 = data.getCount();
        if (numRows1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (data.moveToNext()) {

                String formatted_price = formatter.format(data.getDouble(4));
                String formatted_value = formatter.format(data.getDouble(5));
                final String item_type = dbHandler.get_order_type(data.getString(6));

                listSalesLines1 = new OrderProductItems(data.getString(0), data.getString(1), data.getString(2), data.getInt(3), formatted_price, formatted_value, data.getDouble(4), data.getDouble(5), item_type);
                orderProductItems.add(i, listSalesLines1);

                if ("Template".equals(template)) {

                    salesorder_status = "Draft";
                    long date1 = System.currentTimeMillis();
                    SimpleDateFormat sdfd1 = new SimpleDateFormat("ddMMMyyyy");
                    SimpleDateFormat sdft1 = new SimpleDateFormat("hh:mm");
                    salesorder_date = sdfd1.format(date1);
                    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date myDate = new Date();
                    save_salesorder_date = timeStampFormat.format(myDate);
                    save_salesorder_time = sdft1.format(myDate);
                    order_time = save_salesorder_time;
                    int sale_order_id = dbHandler.get_sale_order_id();

                    a[0] = a[0] + 1;
                    sale_order_id = a[0];

                    if (order_type.equals("")) {

                        random_num = dbHandler.get_sale_order_id();
                        order_id = dbHandler.get_order_sequence_value();

                        SimpleDateFormat timeStampFormat1 = new SimpleDateFormat("ddMMyy");
                        Date myDate1 = new Date();
                        String order_seq_date = timeStampFormat1.format(myDate1);
                        order_id = order_id + 1;
                        dbHandler.update_order_sequence(order_id);

                        random_num = random_num + 1;

                        if (order_id <= 9) {
                            order_num.setText(tab_prefix + "OR" + order_seq_date + "00" + random_num);

                        } else if (order_id > 9 & random_num < 99) {
                            order_num.setText(tab_prefix + "OR" + order_seq_date + "0" + random_num);

                        } else {
                            order_num.setText(tab_prefix + "OR" + order_seq_date + random_num);
                        }
                        order_number = order_num.getText().toString();
                        String visitSequence = sharedPreferences.getString("VisitSequence", "");

                        long id1 = dbHandler.insertToOrderHeader(
                                login_id,
                                customer_id,
                                Integer.toString(order_id),
                                order_num.getText().toString(),
                                save_salesorder_date,
                                salesorder_status,
                                salesorder_value,
                                salesorder_lines,
                                String.valueOf(random_num),
                                visitSequence,
                                save_salesorder_time);

                        order_date.setText(salesorder_date);
                        // order_num.setText(login_id + "18" + Integer.toString(order_id));
                        checkInTimeText.setText(checkin_time);
                        //int order_val1 = dbHandler.get_order_value(Integer.toString(order_id));
                        total_value.setText(format_order_value);
                        //formatter.format(Integer.toString(order_val));

                    }

                    if ((order_type.equals("")) || (order_type.equals("Lines"))) {

                        order_line_id = dbHandler.getOrderLineId(order_id);
                        order_line_id = order_line_id + 1;
                        long id2 = dbHandler.insertToOrderDetail
                                (login_id,
                                        customer_id,
                                        Integer.toString(order_id),
                                        order_line_id,
                                        order_num.getText().toString(),
                                        save_salesorder_date,
                                        salesorder_status,
                                        data.getString(6),
                                        data.getString(1),
                                        data.getString(2),
                                        data.getString(3),
                                        data.getDouble(4),
                                        data.getDouble(5)
                                );


                        if (id2 <= 0) {
                            Toast.makeText(getApplicationContext(), "Order Creation Failed", Toast.LENGTH_LONG).show();
                        } else {
                            // Toast.makeText(getApplicationContext(), "Product is added to cart", Toast.LENGTH_LONG).show();
                        }

                        order_type = "Lines";
                    }

                } else {

                }

                i++;
            }

            ListSalesLineAdapter adapter = new ListSalesLineAdapter(getApplicationContext(), R.layout.list_sales_lines, orderProductItems, Integer.toString(order_id), checkin_time, customer_id);
            ListView list_sales_line = findViewById(R.id.list_sales_line);
            list_sales_line.setAdapter(adapter);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_PHOTO1) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    selectedImage = BitmapFactory.decodeStream(imageStream);
                    selectedImage = getResizedBitmap(selectedImage, 50);
                    customerImage.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                }
            }

        } else if (requestCode == CAPTURE_PHOTO1) {
            if (resultCode == RESULT_OK) {

                image_id = lpoImagesTable.get_lpo_gallery_image_id();
                image_id = image_id + 1;
                String imagename = "customer_image" + image_id;
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] customerimage = stream.toByteArray();
                String fileName = "lpo_" + order_number + "_" + image_id + ".png";
                fileUtils.storeImage(photo, fileName, orderGalleryImageFolderPath);

                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                LPOImages lpoImages = new LPOImages();
                lpoImages.setOrderCode(order_number);
                lpoImages.setLpoImageName(fileName);
                lpoImages.setLpoImagePath(orderGalleryImageFolderPath + File.separator + fileName);
                lpoImages.setVisitSeqNumber(visitSequence);
                lpoImagesTable.create(lpoImages);

            }
        } else

            printBluetooth.onActivityResult(requestCode, resultCode, data, printMsg);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void getlocation() {
        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            String lat = Double.toString(latitude);
            String lng = Double.toString(longitude);

            lat_lng = lat + "," + lng;

        } else {

            gpsTracker.showSettingsAlert();
        }
    }

    public boolean validateCustomerSignature() {
        gs = generalSettingsTable.getSettingByKey("orderSignature");

        if (gs != null && gs.getValue().equals("Yes")) {
            if (TextUtils.isEmpty(customerSignatureName.getText())) {
                Toast.makeText(getApplicationContext(), "Please Enter Signee's Name", Toast.LENGTH_LONG).show();
                return false;
            } else if (selectedImage == null) {
                Toast.makeText(getApplicationContext(), "Please add the Customer Signature", Toast.LENGTH_LONG).show();
                return false;
            }

        }
        return true;
    }

    public void showtemplatedetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_save_template, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        EditText edit_template_name = alertLayout.findViewById(R.id.template_name);

        alert.setPositiveButton("Save", (dialog, which) -> {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(alertLayout.getWindowToken(), 0);

            int template_count = dbHandler.get_template_count(edit_template_name.getText().toString(), customer_id);

            if (template_count > 0) {
                Toast.makeText(getApplicationContext(), "Template Name Already Exist", Toast.LENGTH_LONG).show();
                edit_template_name.setError("Template Name Already Exist");
            } else {
                template_id = dbHandler.get_template_id();
                template_id = template_id + 1;
                template_name = edit_template_name.getText().toString();

                update_order_status("Template");

                Toast.makeText(getApplicationContext(), "Template is Created", Toast.LENGTH_LONG).show();
            }

        });

        alert.setNegativeButton("Close", (dialog, which) -> {
        });

        AlertDialog dialog = alert.create();
        dialog.show();

    }

    private void update_order_status(String status) {

        int line_count = dbHandler.getOrderLinesCount(Integer.toString(order_id));
        order_val = dbHandler.getOrderLinesValue(Integer.toString(order_id));
        if (selectedImage != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();
            String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

            dbHandler.update_order_header(Integer.toString(order_id), order_val, Integer.toString(line_count), encodedImage);
        } else {
            dbHandler.update_order_header(Integer.toString(order_id), order_val, Integer.toString(line_count));
        }

        dbHandler.update_order_header_status(Integer.toString(order_id), lpo_number.getText().toString(), remarks.getText().toString(), status, template_name, template_id, add1.getText().toString(), add2.getText().toString(), txt_calender.getText().toString(), customerSignatureName.getText().toString());

        if (("Completed".equals(status)) || ("Pending for Waybill".equals(status))) {

        } else {
            Intent myintent = new Intent(getApplicationContext(), HomeActivity.class);
            long timeWhenStopped = duration.getBase() - SystemClock.elapsedRealtime();
            SharedPreferenceManager.instance().persistTimeSpentOnLevel(timeWhenStopped);
            duration.stop();

            Bundle bundle = new Bundle();
            bundle.putString("checkin_time", checkin_time);
            bundle.putString("customer_id", customer_id);
            bundle.putString("sales_rep_id", login_id);
            bundle.putString("nav_type", "Home");
            myintent.putExtras(bundle);

            startActivity(myintent);
        }
    }

    private void initializeViews() {

        header_txt = findViewById(R.id.toolbar_title);
        total_value = findViewById(R.id.total_value);
        createSignature = findViewById(R.id.orderCreateSignImageView);
        viewSignature = findViewById(R.id.orderViewSignatureImageView);

        txt_date = findViewById(R.id.txt_calender);
        img_calender = findViewById(R.id.img_calender);

        customerImage = findViewById(R.id.customerImageView);
        customerName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        checkInTimeText = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);


        txt_calender = findViewById(R.id.txt_calender);
        order_date = findViewById(R.id.order_date);
        order_num = findViewById(R.id.order_num);
        sr_image = findViewById(R.id.sr_image);
        txt_calender.setOnClickListener(this);
        sr_name = findViewById(R.id.sr_name);
        delivery_addrs = findViewById(R.id.delivery_addrs);
        lpo_number = findViewById(R.id.lpo_number);
        remarks = findViewById(R.id.remarks);
        img_calender.setOnClickListener(this);
        back = findViewById(R.id.back);
        btn_section = findViewById(R.id.btn_section);
        headerDurationContainer = findViewById(R.id.headerDurationContainer);

        add1 = findViewById(R.id.add1);
        add2 = findViewById(R.id.add2);

        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        captureLpoImage = findViewById(R.id.captureLpoImageView);
        showLpoImagesGallery = findViewById(R.id.showLpoImagesGallery);


        globals = ((Globals) getApplicationContext());
        tab_prefix = Constants.TAB_PREFIX;

        sr_name1 = globals.getSr_name();

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        notification = findViewById(R.id.notificationContainer);
        notification_value = findViewById(R.id.focusedProdCountTv);
        notification_bell = findViewById(R.id.notificationBellButton);

        orderSaveButton = findViewById(R.id.orderSaveImageButton);
        orderCancelButton = findViewById(R.id.orderCancelImageButton);
        orderAddProductButton = findViewById(R.id.orderAddProductButton);

        printButton = findViewById(R.id.orderPrintButton);
        emailButton = findViewById(R.id.orderEmailButton);
        smsButton = findViewById(R.id.orderSmsButton);

        print = new Print(this);
        printBluetooth = new PrintBluetooth(this);
        generalSettingsTable = new GeneralSettingsTable(getApplicationContext());
        lpoImagesTable = new LpoImagesTable(getApplicationContext());

        gs = generalSettingsTable.getSettingByKey("enablePrint");
        if (gs != null && gs.getValue().equals("Yes")) {
            printButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableSms");
        if (gs != null && gs.getValue().equals("Yes")) {
            smsButton.setVisibility(View.VISIBLE);
        }

        gs = generalSettingsTable.getSettingByKey("enableEmail");
        if (gs != null && gs.getValue().equals("Yes")) {
            emailButton.setVisibility(View.VISIBLE);
        }

        customerSignatureName = findViewById(R.id.customerSignatureNameEt);

        customerSignatureName.setFilters(new InputFilter[]{filter});


    }

    public void populateHeaderDetails() {

        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        shippingAddressesTable = new CustomerShippingAddressTable(getApplicationContext());


        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            order_id = bundle.getInt("order_id");
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            template = bundle.getString("template");
            login_id = bundle.getString("login_id");
            nav_type = bundle.getString("nav_type");
            order_number = bundle.getString("order_num");

        }

        int focus_order_count = dbHandler.getFocusProductCount(Integer.toString(order_id), customer_id);
        notification_value.setText(Integer.toString(focus_order_count));

        //Populate SalesRep Image
        Cursor salesimage = dbHandler.getsalesrepimage(login_id);
        int count = salesimage.getCount();
        if (count == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesimage.moveToNext()) {
                byte[] blob = salesimage.getBlob(9);
                int salesimage_length = dbHandler.get_length_salesrep_image(login_id);
                if (salesimage_length == 0) {

                } else {
                    Bitmap bmp = convertByteArrayToBitmap(blob);
                    sr_image.setImageBitmap(bmp);
                }
            }
        }

        //Populate SalesRep Details
        Cursor salesRepDetails = dbHandler.getSalesRepDetails(login_id);
        int count1 = salesRepDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (salesRepDetails.moveToNext()) {
                //byte[] blob = salesRepDetails.getBlob(9);
                //Bitmap bmp= convertByteArrayToBitmap(blob);
                //sr_image.setImageBitmap(bmp);
                sr_name_details = salesRepDetails.getString(2);
                sr_rm_name = salesRepDetails.getString(3);
                sr_region = salesRepDetails.getString(4);
                sr_warehouse = salesRepDetails.getString(5);

            }
        }

        //Populate Header Details

        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                customerNameString = customerDetails.getString(2);
                customerEmailAddress = customerDetails.getString(33);
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                // ship_address1 = customerDetails.getString(14);
                //ship_address2 = customerDetails.getString(15);
                //ship_address3 = customerDetails.getString(16);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

                customerAddress1 = customerDetails.getString(6);
                customerAddress2 = customerDetails.getString(7);
                customerAddress3 = customerDetails.getString(8);
                customerPhoneNumber = customerDetails.getString(11);

                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }

        shippingAddressesData = shippingAddressesTable.getShippingAddressCompanyId(customer_id);
        //Populate LPO,Delivery and Remarks
        Cursor orderHeaderInfo = dbHandler.getorderHeaderInfo(String.valueOf(order_id));
        int count2 = orderHeaderInfo.getCount();
        if (count2 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (orderHeaderInfo.moveToNext()) {
                lpo_number.setText(orderHeaderInfo.getString(0));
                remarks.setText(orderHeaderInfo.getString(1));
                txt_calender.setText(orderHeaderInfo.getString(2));
                ship_address1 = orderHeaderInfo.getString(3);
                ship_address2 = orderHeaderInfo.getString(4);
                order_time = orderHeaderInfo.getString(6);
                if (!TextUtils.isEmpty(orderHeaderInfo.getString(7))) {
                    byte[] byteArrayImage = Base64.decode(orderHeaderInfo.getString(7), Base64.DEFAULT);
                    Bitmap bmp = convertByteArrayToBitmap(byteArrayImage);
                    viewSignature.setImageBitmap(bmp);
                    selectedImage = bmp;
                }
                customerSignatureName.setText(orderHeaderInfo.getString(8));
                /*SimpleDateFormat dateFormat = new SimpleDateFormat();
                dateFormat.applyPattern("HH:ss");
                Date myDate = new java.util.Date(Long.parseLong(dateTime) * 1000L);
                order_time = dateFormat.format(myDate);*/
            }
        }
        Log.d("ShipAddress", "Test" + ship_address1);
        if (TextUtils.isEmpty(ship_address1)) {
            Log.d("ShipAddress", "Test" + shippingAddressesData.size());

            if (shippingAddressesData != null && shippingAddressesData.size() != 0) {

                CustomerShippingAddress shippingAddress = shippingAddressesData.get(0);
                Log.d("ShipAddress", "Test" + shippingAddress.getShip_address1());
                add1.setText(shippingAddress.getShip_address1());
                add2.setText(shippingAddress.getShip_address2());
            }


        } else {
            add1.setText(ship_address1);
            add2.setText(ship_address2);
        }

        // sr_name.setText(sr_name1);

        getCompanyInfo();


    }

    public void getCompanyInfo() {
        //Populate SalesRep Details
        Cursor companyDetails = dbHandler.getCompanyDetails();
        int count1 = companyDetails.getCount();
        if (count1 == 0) {
            //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (companyDetails.moveToNext()) {

                companyName = companyDetails.getString(0);
                companyCode1 = companyDetails.getString(1);
                compAdd1 = companyDetails.getString(8);
                compAdd2 = companyDetails.getString(9);
                compAdd3 = companyDetails.getString(10);
                compMblNo = companyDetails.getString(4);


            }
        }

    }

    public void showcustomerLocation() {

        Intent intent = new Intent(getApplicationContext(), GetLocationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v == img_calender) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,

                    (view, year, monthOfYear, dayOfMonth) -> {
                        try {
                            String PATTERN = "dd MMM YY";
                            SimpleDateFormat dateFormat = new SimpleDateFormat();
                            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            dateFormat.applyPattern(PATTERN);
                            dateFormat.format(d);
                            //txt_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            txt_date.setText(dateFormat.format(d));

                        } catch (Exception e) {

                        }

                    }, mYear, mMonth, mDay);

            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

            datePickerDialog.show();
        }

    }

    private void createNewSignature() {

        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.signature, null);
        mSignaturePad = alertLayout.findViewById(R.id.signature_pad);
        mClearButton = alertLayout.findViewById(R.id.clear_button);
        mSaveButton = alertLayout.findViewById(R.id.save_button);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//Toast.makeText(MainActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
// this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
// disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);
        final android.app.AlertDialog dialog = alert.create();
        dialog.show();

        mClearButton.setOnClickListener(view -> mSignaturePad.clear());

        mSaveButton.setOnClickListener(view -> {
            Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
            selectedImage = signatureBitmap;
            viewSignature.setImageBitmap(signatureBitmap);
            dialog.dismiss();

            //Posting the customer Signature
            String fileName = "signature_" + order_number + "_" + customer_id + ".png";
            fileUtils.storeImage(selectedImage, fileName, orderSignaturesFolderPath);

        });

    }

    private void setDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(() -> {
                            long date1 = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                            String dateString = sdf.format(date1);
                            datetime.setText(dateString);
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }

    private Bitmap convertByteArrayToBitmap(byte[] bytes) {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public void showdeliveryaddressdetails() {

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_delivery_details, null);
        final TextView ship_add1 = alertLayout.findViewById(R.id.ship_add1);
        final TextView ship_add2 = alertLayout.findViewById(R.id.ship_add2);
        final TextView ship_add3 = alertLayout.findViewById(R.id.ship_add3);
        final TextView ship_city = alertLayout.findViewById(R.id.ship_city);
        final TextView ship_state = alertLayout.findViewById(R.id.ship_state);
        final TextView ship_country = alertLayout.findViewById(R.id.ship_country);

        ship_add1.setText(ship_address1);
        ship_add2.setText(ship_address2);
        ship_add3.setText(ship_address3);
        ship_city.setText(ship_city1);
        ship_state.setText(ship_state1);
        ship_country.setText(ship_country1);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();

        dialog.show();
    }

    @Override
    public void onBackPressed() {

    }

    private void loadShippingAddress() {

        //ArrayList<ShippingAddress> addressList = (ArrayList<ShippingAddress>)((ShippingPageAdapter)addressViewPager.getAdapter()).shippingAddressesData;

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_delivery_details, null);
        addressViewPager = alertLayout.findViewById(R.id.addressViewPager);
        Log.d("MYDATA", "Customer Data  id" + customer_id);
        if (shippingAddressesData.size() == 0) {
            Toast.makeText(getApplicationContext(), "No Shipping address found", Toast.LENGTH_SHORT).show();
            return;
        }

        int currentItem = addressViewPager.getCurrentItem();
        addressViewPager.setAdapter(new ShippingPageAdapter(getApplicationContext(), shippingAddressesData));
        addressViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setPositiveButton("Done", (dialog, which) -> {
            CustomerShippingAddress csa = shippingAddressesData.get(addressViewPager.getCurrentItem());
            String address1 = csa.getShip_address1() != null ? csa.getShip_address1() + ", " : "" + csa.getShip_address2() != null ? csa.getShip_address2() + ", " : "" + csa.getShip_address3() != null ? csa.getShip_address3() + ", " : "";
            String address2 = csa.getShip_city() != null ? csa.getShip_city() + ", " : "" + csa.getShip_country() != null ? csa.getShip_country() + ", " : "" + csa.getShip_state() != null ? csa.getShip_state() + "." : "";
            add1.setText(address1);
            add2.setText(address2);
        });
        alert.setNegativeButton("Close", (dialog, which) -> {

        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

    public void showsrdetails() {


    }

    private class OnDismissListener implements PopupMenu.OnDismissListener {

        @Override
        public void onDismiss(PopupMenu menu) {

        }

    }

    private class OnMenuItemClickListener implements
            PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.saveorder:

                    if(!validateCustomerSignature())
                        return true;
                    line_count = dbHandler.getOrderLinesCount(Integer.toString(order_id));


                    if ("".equals(txt_calender.getText().toString())) {
                        txt_calender.setError(getString(R.string.delivery_date_mandatory));

                    } else if ("".equals(add1.getText().toString())) {
                        add1.setError(getString(R.string.shipping_address_mandatory));

                    } else if (line_count == 0) {
                        Toast.makeText(getApplicationContext(), "Add Product to Save the Order", Toast.LENGTH_LONG).show();

                    } else {
                        update_order_status("Completed");
                        orderStatus = "Completed";
                        lpo_number.setEnabled(false);
                        remarks.setEnabled(false);
                        img_calender.setEnabled(false);
                        createSignature.setEnabled(false);
                        delivery_addrs.setEnabled(false);
                        orderSaveButton.setEnabled(false);
                        orderAddProductButton.setEnabled(false);
                        orderCancelButton.setEnabled(false);
                        txt_date.setError(null);
                        orderSaveButton.setVisibility(View.GONE);
                        orderCancelButton.setVisibility(View.GONE);
                        orderAddProductButton.setVisibility(View.GONE);
                        customerSignatureName.setEnabled(false);
                        captureLpoImage.setVisibility(View.GONE);

                        if (selectedImage != null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            selectedImage.compress(Bitmap.CompressFormat.PNG, 100, baos);
                            String fileName = "signature_" + order_number + "_" + customer_id + ".png";
                            dbHandler.updateOrderSignatureimage(order_number, orderSignaturesFolderPath + File.separator + fileName, fileName);
                        }

                        Toast.makeText(getApplicationContext(), "Order is Placed", Toast.LENGTH_LONG).show();
                        // Toast.makeText(getApplicationContext(), Integer.toString(line_count), Toast.LENGTH_LONG).show();

                    }


                    return true;
                case R.id.savedraft:
                    if(!validateCustomerSignature())
                        return true;
                    update_order_status("Draft");
                    Toast.makeText(getApplicationContext(), "Order Saved as Draft", Toast.LENGTH_LONG).show();

                    return true;
                case R.id.savetemplate:

                    showtemplatedetails();


                    return true;

                case R.id.savewaybill:
                    line_count = dbHandler.getOrderLinesCount(Integer.toString(order_id));


                    if ("".equals(txt_calender.getText().toString())) {
                        txt_calender.setError(getString(R.string.delivery_date_mandatory));

                    } else if ("".equals(add1.getText().toString())) {
                        add1.setError(getString(R.string.shipping_address_mandatory));

                    } else if (line_count == 0) {
                        Toast.makeText(getApplicationContext(), "Add Product to Save the Order", Toast.LENGTH_LONG).show();

                    } else {
                        update_order_status("Pending for Waybill");
                        orderStatus = "Pending for Waybill";
                        lpo_number.setEnabled(false);
                        remarks.setEnabled(false);
                        img_calender.setEnabled(false);
                        createSignature.setEnabled(false);
                        delivery_addrs.setEnabled(false);
                        orderSaveButton.setEnabled(false);
                        orderAddProductButton.setEnabled(false);
                        orderCancelButton.setEnabled(false);
                        txt_date.setError(null);
                        orderSaveButton.setVisibility(View.GONE);
                        orderCancelButton.setVisibility(View.GONE);
                        orderAddProductButton.setVisibility(View.GONE);
                        customerSignatureName.setEnabled(false);
                        captureLpoImage.setVisibility(View.GONE);

                        Toast.makeText(getApplicationContext(), "Waybill is Created", Toast.LENGTH_LONG).show();

                    }


                    return true;

            }
            return false;
        }
    }

    public class ListSalesLineAdapter extends ArrayAdapter<OrderProductItems> {

        Activity mActivity = null;
        android.support.v7.app.AlertDialog alertDialog1;
        String customer_id = "";
        String checkin_time = "";
        String order_id = "";
        String salesline_id = "";
        String trip_num = "";
        Globals globals;
        MyDBHandler dbHandler;
        private LayoutInflater mInflater;
        private ArrayList<OrderProductItems> users;
        private int mViewResourceId;
        private Context c;

        public ListSalesLineAdapter(Context context, int textViewResourceId, ArrayList<OrderProductItems> users, String order_id_val, String checkintime_val, String customer_id_val) {
            super(context, textViewResourceId, users);
            this.users = users;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mViewResourceId = textViewResourceId;
            c = context;
            order_id = order_id_val;
            checkin_time = checkintime_val;
            customer_id = customer_id_val;
            dbHandler = new MyDBHandler(c, null, null, 1);


        }


        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;
            convertView = mInflater.inflate(mViewResourceId, null);
            final OrderProductItems user = users.get(position);
            if (user != null) {
                viewHolder = new ViewHolder();
                viewHolder.product = convertView.findViewById(R.id.product);
                viewHolder.uom = convertView.findViewById(R.id.uom);
                viewHolder.quantity = convertView.findViewById(R.id.quantity);
                viewHolder.price = convertView.findViewById(R.id.price);
                viewHolder.value = convertView.findViewById(R.id.value);
                viewHolder.item_type = convertView.findViewById(R.id.item_type);

                String qty_formatted = formatter2.format(user.getQuantity());

                if (viewHolder.product != null) {
                    viewHolder.product.setText(user.getProduct());
                }
                if (viewHolder.uom != null) {
                    viewHolder.uom.setText((user.getUom()));
                }
                if (viewHolder.quantity != null) {
                    viewHolder.quantity.setText(qty_formatted);
                }
                if (viewHolder.price != null) {
                    viewHolder.price.setText((user.getPrice()));
                }
                if (viewHolder.value != null) {
                    viewHolder.value.setText((user.getValue()));
                }

                if (viewHolder.item_type != null) {

                    String item_type_val = user.getProd_type();
                    if ("focus".equals(item_type_val)) {
                        viewHolder.item_type.setImageResource(R.drawable.product_green_scheme);

                    } else if ("scheme".equals(item_type_val)) {
                        viewHolder.item_type.setImageResource(R.drawable.product_yellow_scheme);

                    } else {
                        viewHolder.item_type.setImageResource(0);
                        // viewHolder.item_type.setVisibility(View.INVISIBLE);
                        // viewHolder.item_type.setBackgroundColor(Color.parseColor("#d6d6d6"));

                    }

                }
            }


            viewHolder.product.setOnClickListener(v -> {

                if ("Reports".equals(nav_type) || orderStatus.equals("Completed") || orderStatus.equals("Pending for Waybill")) {

                } else {
                    final String id = user.getSales_line_id();
                    call_alert(v, position, id);
                }
            });

            viewHolder.uom.setOnClickListener(v -> {

                if ("Reports".equals(nav_type) || orderStatus.equals("Completed") || orderStatus.equals("Pending for Waybill")) {

                } else {
                    final String id = user.getSales_line_id();
                    call_alert(v, position, id);
                }
            });

            viewHolder.quantity.setOnClickListener(v -> {

                if ("Reports".equals(nav_type) || orderStatus.equals("Completed") || orderStatus.equals("Pending for Waybill")) {

                } else {
                    final String id = user.getSales_line_id();
                    call_alert(v, position, id);
                }
            });

            viewHolder.price.setOnClickListener(v -> {

                if ("Reports".equals(nav_type) || orderStatus.equals("Completed") || orderStatus.equals("Pending for Waybill")) {

                } else {
                    final String id = user.getSales_line_id();
                    call_alert(v, position, id);
                }
            });

            viewHolder.value.setOnClickListener(v -> {

                if ("Reports".equals(nav_type) || orderStatus.equals("Completed") || orderStatus.equals("Pending for Waybill")) {

                } else {
                    final String id = user.getSales_line_id();
                    call_alert(v, position, id);
                }


            });

            return convertView;
        }

        public void call_alert(final View v, final int position, final String id) {

            final OrderProductItems user = users.get(position);

            // final String id = user.getInvoice_line_id();
            final String prod = user.getProduct();
            final String uom = user.getUom();
            final int qty = user.getQuantity();
            final double price = user.getPrice_db();
            final double value = user.getValue_db();

            AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
            alertbox.setTitle("Update ?");
            alertbox.setPositiveButton("Edit", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int whichButton) {

                    showsrdetails(v, prod, uom, qty, price, value, id);
                    //Toast.makeText(getApplicationContext(), "Qty :" + qty + "Price :" + price + "Value :" + value, Toast.LENGTH_LONG).show();
                    //Toast.makeText(c, id + order_id, Toast.LENGTH_LONG).show();

                }

            })

                    .setNegativeButton("Delete", (dialog, which) -> {

                        AlertDialog.Builder alertbox1 = new AlertDialog.Builder(v.getRootView().getContext());
                        alertbox1.setTitle("Confirm Delete ?");
                        alertbox1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                //Toast.makeText(getContext(), id, Toast.LENGTH_LONG).show();
                                MyDBHandler dbHandler = new MyDBHandler(c, null, null, 1);
                                dbHandler.deleteOrderLines(id, order_id);
                                users.remove(position);
                                // your remaining code
                                notifyDataSetChanged();
                                order_val = dbHandler.getOrderLinesValue(order_id);
                                format_order_value = formatter.format(order_val);
                                // total_value.setText(Integer.toString(order_val));
                                total_value.setText(format_order_value);

                            }

                        })

                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                });

                        alertbox1.show();

                    });

            alertbox.show();

        }

        public void showsrdetails(View v, String prod1, String uom1, final int qty1, final double price1, final double value1, final String sale_line_id) {

            LayoutInflater mInflater = (LayoutInflater)
                    c.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            View alertLayout = mInflater.inflate(R.layout.activity_update_order, null);

            final TextView prod_name = alertLayout.findViewById(R.id.prod_name);
            final TextView uom = alertLayout.findViewById(R.id.uom);
            final EditText qty = alertLayout.findViewById(R.id.quantity);
            final TextView price = alertLayout.findViewById(R.id.price);
            final TextView value = alertLayout.findViewById(R.id.value);
            qty.setFilters(new InputFilter[]{new InputFilterMinMax(MIN_NUMBER, MAX_NUMBER)});


            price_val_update = price1;

            String price_format = formatter.format(price1);
            String value_format = formatter.format(value1);
            String qty_format = formatter2.format(qty1);

            prod_name.setText(prod1);
            uom.setText(uom1);
            qty.setText(qty_format);
            price.setText(price_format);
            value.setText(value_format);

            qty.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (qty.getText().toString().equals("")) {
                        value.setText("");

                    } else {

                        int quan = Integer.parseInt(qty.getText().toString());
                        order_val_update = quan * price1;
                        String orderval_formatted = formatter.format(order_val_update);
                        value.setText(orderval_formatted);

                    }
                }
            });

            price.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (price.getText().toString().equals("")) {

                        value.setText("");

                    } else {

                        // double order_val=qty1*price1;
                        // String orderval_formatted=formatter.format(order_val);
                        //value.setText(orderval_formatted);
                        // Toast.makeText(getApplicationContext(), orderval_formatted, Toast.LENGTH_LONG).show();


                    }
                }
            });

            AlertDialog.Builder alert = new AlertDialog.Builder(v.getRootView().getContext());
            alert.setTitle("Edit Product Quantity");
            alert.setView(alertLayout);
            alert.setPositiveButton("Update", (dialog, whichButton) -> {

                if (TextUtils.isEmpty(qty.getText().toString())) {
                    qty.setError(getString(R.string.error_field_required));

                } else if ("0".equals(qty.getText().toString())) {
                    qty.setError(getString(R.string.Zero_Validation));
                } else {
                    int quan = Integer.parseInt(qty.getText().toString());
                    order_val_update = quan * price1;
                    dbHandler.update_order_details(sale_line_id, order_id, qty.getText().toString(), price_val_update, order_val_update);
                    orderProductItems = new ArrayList<OrderProductItems>();
                    Cursor data = dbHandler.getOrderLines(order_id);
                    int numRows1 = data.getCount();
                    if (numRows1 == 0) {
                        //Toast.makeText(getApplicationContext(), "The Database is empty  :(.", Toast.LENGTH_LONG).show();
                    } else {
                        int i = 0;
                        while (data.moveToNext()) {

                            String formatted_price = formatter.format(data.getDouble(4));
                            String formatted_value = formatter.format(data.getDouble(5));
                            final String item_type = dbHandler.get_order_type(data.getString(6));
                            listSalesLines1 = new OrderProductItems(data.getString(0), data.getString(1), data.getString(2), data.getInt(3), formatted_price, formatted_value, data.getDouble(4), data.getDouble(5), item_type);
                            orderProductItems.add(i, listSalesLines1);
                            //System.out.println(userList.get(i).getFirstName());
                            i++;

                        }

                        ListSalesLineAdapter adapter = new ListSalesLineAdapter(getApplicationContext(), R.layout.list_sales_lines, orderProductItems, order_id, checkin_time, customer_id);
                        ListView list_sales_line = findViewById(R.id.list_sales_line);
                        list_sales_line.setAdapter(adapter);
                    }

                    order_val = dbHandler.getOrderLinesValue(order_id);
                    format_order_value = formatter.format(order_val);
                    total_value.setText(format_order_value);
                }

            })

                    .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

            // disallow cancel of AlertDialog on click of back button and outside touch
            alert.setCancelable(false);
            alert.show();
        }

        public class ViewHolder {
            TextView product, uom, quantity, price, value;
            ImageView item_type;
        }

    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        private String resp;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                int time = Integer.parseInt(params[0]) * 1000;

                Thread.sleep(time);
                resp = "Slept for " + params[0] + " seconds";
            } catch (InterruptedException e) {
                e.printStackTrace();
                resp = e.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            //finalResult.setText(result);
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(OrderProductActivity.this,
                    "ProgressDialog",
                    "Wait for  seconds");
        }


        @Override
        protected void onProgressUpdate(String... text) {
            //finalResult.setText(text[0]);

        }
    }
}


//Creating custom adapter for multiple shipping address

class ShippingPageAdapter extends PagerAdapter {
    int numberOfAddress = 0;
    ArrayList<CustomerShippingAddress> shippingAddressesData;
    private Context mContext;

    ShippingPageAdapter(Context context, ArrayList<CustomerShippingAddress> addressData) {
        mContext = context;
        shippingAddressesData = addressData;
        numberOfAddress = addressData.size();
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        CustomerShippingAddress shippingAddress = shippingAddressesData.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final ViewGroup page = (ViewGroup) inflater.inflate(R.layout.list_shipping_address, container, false);


        final TextView ship_add1 = page.findViewById(R.id.ship_add1);
        final TextView ship_add2 = page.findViewById(R.id.ship_add2);
        final TextView ship_add3 = page.findViewById(R.id.ship_add3);
        final TextView ship_city = page.findViewById(R.id.ship_city);
        final TextView ship_state = page.findViewById(R.id.ship_state);
        final TextView ship_country = page.findViewById(R.id.ship_country);

        ship_add1.setText(shippingAddress.getShip_address1());
        ship_add2.setText(shippingAddress.getShip_address2());
        ship_add3.setText(shippingAddress.getShip_address3());
        ship_city.setText(shippingAddress.getShip_city());
        ship_state.setText(shippingAddress.getShip_state());
        ship_country.setText(shippingAddress.getShip_country());

        Log.d("MYDATA", "Test Instance" + position);
        container.addView(page);
        return page;
    }

    @Override
    public int getCount() {
        return numberOfAddress;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);

    }


    public ArrayList<CustomerShippingAddress> getShippingAddressesDataData() {
        return shippingAddressesData;
    }

}

