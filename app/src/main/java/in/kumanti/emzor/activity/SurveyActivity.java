package in.kumanti.emzor.activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.SurveyQuestionOptionTable;
import in.kumanti.emzor.eloquent.SurveyResponseAnswerTable;
import in.kumanti.emzor.eloquent.SurveyResponseTable;
import in.kumanti.emzor.eloquent.SurveyTable;
import in.kumanti.emzor.model.Survey;
import in.kumanti.emzor.model.SurveyQuestionOption;
import in.kumanti.emzor.model.SurveyQuestions;
import in.kumanti.emzor.model.SurveyResponse;
import in.kumanti.emzor.model.SurveyResponseAnswer;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.SharedPreferenceManager;

public class SurveyActivity extends MainActivity implements View.OnClickListener {

    public int numberOfQuestions;
    public int mScore = 0;
    String login_id = "";
    MyDBHandler dbHandler;
    TextView checkInTime, actionBarTitle, marqueeText;
    ImageView deviceInfo, actionBarBack, customerImage;
    Chronometer duration;

    CustomSearchableSpinner survey_name_spinner;
    ImageButton previousButton, nextButton;
    Button surveyReportButton;
    TextView scoreValue, surveyDescription, surveyDate, accountNo, customerName, city;
    View thankYouLayout;
    QuestionsViewPager surveyQusOptionsPager;
    LinearLayout buttonContainer, thankYouContainer;
    ArrayList<SurveyQuestions> questionData;
    String checkin_time = "", customer_id = "", surveyDateString, surveyId;

    SurveyQuestionOptionTable surveyQuestionOptionTable;
    SurveyTable surveyTable;
    SurveyResponseTable surveyResponseTable;
    SurveyResponseAnswerTable surveyResponseAnswerTable;
    SharedPreferenceManager sharedPreferenceManager;
    SurveyResponse currentSurveyResponse = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_survey);

        initializeViews();
        setFooterDateTime();
        populateHeaderDetails();


        actionBarTitle.setText("Survey");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        try {
            surveyDescription.setText("");
            surveyDate.setText("");

            loadSurveyName();
            survey_name_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view,
                                           int pos, long id) {
                    survey_name_spinner.isSpinnerDialogOpen = false;

                    Survey s = (Survey) adapterView.getItemAtPosition(pos);
                    if (s != null && !s.survey_code.equals("-1")) {
                        Survey survey = surveyTable.getSurveyById(s.survey_code);
                        surveyDescription.setText(survey.getSurvey_description() != null ? survey.getSurvey_description() : "");
                        Log.d("Survey-- ", "Description: " + survey.getSurvey_description());

                        surveyDate.setText(survey.getSurvey_date() != null ? survey.getSurvey_date() : "");
                        setSurveyDate();
                        Log.d("Survey-- ", "Date: " + s.survey_code + " cust" + customer_id);
                        ArrayList<SurveyQuestions> questionData = survey.getSurvey_questions();
                        currentSurveyResponse = surveyResponseTable.getSurveyResponseById(s.survey_code, customer_id);
                        if (currentSurveyResponse != null) {
                            Log.d("SurveyData", "" + s.survey_code);
                            for (SurveyResponseAnswer sra : currentSurveyResponse.getResponseAnswers()) {
                                Log.d("SurveyData", "" + s.survey_code + " " + sra.getId() + " Q" + sra.getSurvey_question_id());
                                for (SurveyQuestions sq : questionData) {
                                    if (sra.getSurvey_question_id().equals(String.valueOf(sq.id))) {
                                        Log.d("SurveyData", "" + s.survey_code + " Question" + sq.id);
                                        if (sq.getQuestion_type().equals("Single Choice")) {
                                            sq.seletectedRadioGroupIndex = TextUtils.isEmpty(sra.optionsValuesIds) ? -1 : Integer.parseInt(sra.optionsValuesIds);
                                        } else if (sq.getQuestion_type().equals("Multiple Choice")) {
                                            Log.d("SurveyData", "" + s.survey_code + " Question Value Multiple" + sq.id);
                                            if (!TextUtils.isEmpty(sra.optionsValuesIds)) {
                                                ArrayList<Integer> options = new ArrayList<Integer>();
                                                String[] items = sra.optionsValuesIds.split(",");
                                                for (String i : items) {
                                                    options.add(Integer.parseInt(i));
                                                }
                                                sq.selectedCheckBoxIndexs = options;
                                            }
                                        } else if (sq.getQuestion_type().equals("Both")) {
                                            sq.seletectedRadioGroupIndex = TextUtils.isEmpty(sra.optionsValuesIds) ? -1 : Integer.parseInt(sra.optionsValuesIds);
                                            String val[] = sra.optionValue.split(", ", 2);
                                            if (val.length == 2)
                                                sq.inputTextAnswer = val[1];
                                        } else {
                                            Log.d("SurveyData", "" + s.survey_code + " Question Value" + sq.id);
                                            sq.inputTextAnswer = sra.optionValue;
                                        }
                                    }
                                }
                            }
                        }
                        Log.d("Survey-- ", "Question Data: " + survey.getSurvey_questions());

                        numberOfQuestions = questionData.size();
                        setQuestionData(questionData);
                        Log.d("Survey-- ", "No of Questions: " + numberOfQuestions);

                        surveyId = survey.survey_code;
                        if (questionData != null) {
                            loadQuestion(questionData);

                        }
                    } else {
                        surveyId = "-1";
                        surveyDescription.setText("");
                        surveyDate.setText("");
                        previousButton.setBackgroundColor(Color.TRANSPARENT);
                        nextButton.setBackgroundColor(Color.TRANSPARENT);
                        ArrayList<SurveyQuestions> questionData = new ArrayList<SurveyQuestions>();
                        numberOfQuestions = questionData.size();
                        setQuestionData(questionData);
                        loadQuestion(questionData);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapter) {
                    survey_name_spinner.isSpinnerDialogOpen = false;
                }
            });


        } catch (Exception e) {
            Log.d("Survey--", "Exception--" + e.getMessage());
        }


        surveyQusOptionsPager.setPagingEnabled(false);

        previousButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);

        actionBarBack.setOnClickListener(v -> finish());

        surveyQusOptionsPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //updateButtonState(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

    }

    private void loadSurveyName() {
        ArrayList<Survey> surveyArrayList = surveyTable.getSurveyList(customer_id);
        Survey s = new Survey();
        s.survey_name = "Select";
        s.survey_code = "-1";
        surveyArrayList.add(0, s);

        ArrayAdapter<Survey> spinnerArrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, surveyArrayList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        survey_name_spinner.setAdapter(spinnerArrayAdapter);
    }

    private void loadQuestion(ArrayList<SurveyQuestions> question) {

        surveyQusOptionsPager.setAdapter(new QuestionsPageAdapter(getApplicationContext(), questionData));
        Log.d("Survey-- ", "loadQuestion: Data " + questionData.size());

        surveyQuestionOptionTable.clearAllQuestions();
        updateButtonState(0);
    }

    public boolean validateAnswer(SurveyQuestions surveyQuestion) {
        if (surveyQuestion.getQuestion_type().equals("Single Choice") || surveyQuestion.getQuestion_type().equals("Both")) {
            return surveyQuestion.radioGroup.getCheckedRadioButtonId() != -1;
        } else if (surveyQuestion.getQuestion_type().equals("Multiple Choice")) {
            for (CheckBox ck : surveyQuestion.checkBoxes) {
                if (ck.isChecked()) {
                    return true;
                }
            }
        } else if (surveyQuestion.getQuestion_type().equals("Free Text")) {
            return !surveyQuestion.editText.getText().toString().isEmpty();
        }
        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.surveyNextButton:
                ArrayList<SurveyQuestions> sqList = ((QuestionsPageAdapter) surveyQusOptionsPager.getAdapter()).questionData;
                int currentItem = surveyQusOptionsPager.getCurrentItem();
                //if (validateAnswer(sqList.get(currentItem))) {//Uncomment this if validation required
                if (currentItem + 1 < numberOfQuestions) {
                    surveyQusOptionsPager.setCurrentItem(currentItem + 1, true);
                    updateButtonState(currentItem + 1);

                } else {
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.survey_save_confirmation, null);
                    Button surveyNoBtn = alertLayout.findViewById(R.id.surveyNoBtn),
                            surveyYesBtn = alertLayout.findViewById(R.id.surveyYesBtn);

                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(false);

                    AlertDialog dialog = alert.create();
                    surveyYesBtn.setOnClickListener(v1 -> {
                        saveSurvey("completed", sqList);
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Survey Completed Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    });

                    surveyNoBtn.setOnClickListener(v12 -> {
                        saveSurvey("inprogress", sqList);
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Survey saved in draft", Toast.LENGTH_SHORT).show();
                        finish();
                    });
                    dialog.show();

                }
                //Uncomment this if validation required
                /*} else {
                    String requiredMessage = "Please select atleast one option";
                    if (sqList.get(currentItem).getQuestion_type().equals("Free Text"))
                        requiredMessage = "Please enter your inputs";
                    Toast.makeText(getApplicationContext(), requiredMessage, Toast.LENGTH_LONG).show();
                }*/

                break;

            case R.id.surveyPreviousButton:
                int currentItem1 = surveyQusOptionsPager.getCurrentItem();
                if (currentItem1 > 0) {
                    surveyQusOptionsPager.setCurrentItem(currentItem1 - 1, true);
                    updateButtonState(currentItem1 - 1);
                }
                break;
        }

    }

    private void saveSurvey(String status, ArrayList<SurveyQuestions> sqList) {
        long responseId = 0;
        //Once Every Questions are answered, we will log the survey response here.
        if (currentSurveyResponse == null) {
            String visitSequence = sharedPreferences.getString("VisitSequence", "");

            SurveyResponse surveyResponse = new SurveyResponse();
            surveyResponse.setSurvey_id(surveyId);
            surveyResponse.setSales_rep_id(login_id);
            surveyResponse.setCustomer_id(customer_id);
            surveyResponse.setStatus(status);
            surveyResponse.setVisitSequenceNumber(visitSequence);
            responseId = surveyResponseTable.create(surveyResponse);
        } else {
            responseId = currentSurveyResponse.getId();
            currentSurveyResponse.setStatus(status);
            surveyResponseTable.update(String.valueOf(responseId), currentSurveyResponse);
            surveyResponseAnswerTable.deleteAnswers(String.valueOf(responseId));
        }
        for (SurveyQuestions cd : sqList) {

            String visitSequence = sharedPreferences.getString("VisitSequence", "");

            SurveyResponseAnswer surveyResponseAnswer = new SurveyResponseAnswer();
            surveyResponseAnswer.setSurvey_id(surveyId);
            surveyResponseAnswer.setSurvey_response_answer_id(String.valueOf(responseId));
            surveyResponseAnswer.setSurvey_question_id(String.valueOf(cd.id));
            String val = cd.getAnswer();
            surveyResponseAnswer.setOption_value(val);
            surveyResponseAnswer.setOptionsValuesIds(cd.getAnswerIds());
            surveyResponseAnswer.setVisitSequenceNumber(visitSequence);
            Log.d("AnswerOptions", "Value Id =" + val);

            Log.d("AnswerOptions", "Value =" + val);
            surveyResponseAnswerTable.create(surveyResponseAnswer);
        }
    }


    private void showResult() {
        //Broadcasting score and user id
        Log.d("Survey QuizData", "showResult " + mScore);
        surveyQusOptionsPager.setVisibility(View.INVISIBLE);
        thankYouContainer.setVisibility(View.VISIBLE);
        scoreValue.setText("");
        buttonContainer.setVisibility(View.INVISIBLE);
        loadQuestionSubmit();
      /*  LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_scheme_allocation, null);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();*/
    }


    public ArrayList<SurveyQuestions> getQuestionData() {
        return questionData;
    }

    public void setQuestionData(ArrayList<SurveyQuestions> questionData) {

        this.questionData = questionData;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public void updateButtonState(int currentItem) {
        previousButton.setEnabled(currentItem > 0);
        if (currentItem == 0)
            previousButton.setBackground(getResources().getDrawable(R.drawable.round_corner_grey));
        else
            previousButton.setBackground(getResources().getDrawable(R.drawable.round_corner));
        if (currentItem == questionData.size() - 1 || questionData.isEmpty()) {
            nextButton.setBackground(getResources().getDrawable(R.drawable.round_corner_orange));
            nextButton.setImageResource(R.drawable.verification_mark_dark);

        } else {
            nextButton.setBackground(getResources().getDrawable(R.drawable.round_corner));
            nextButton.setImageResource(R.drawable.next_arrow);
        }
        if (surveyId.equals("-1")) {
            previousButton.setBackgroundColor(Color.TRANSPARENT);
            nextButton.setBackgroundColor(Color.TRANSPARENT);
            previousButton.setEnabled(false);
            nextButton.setEnabled(false);
        } else {
            previousButton.setEnabled(true);
            nextButton.setEnabled(true);
        }
    }

    private void loadQuestionSubmit() {
        previousButton.setVisibility(View.GONE);
        nextButton.setVisibility(View.GONE);

        buttonContainer.setVisibility(View.VISIBLE);
        thankYouContainer.setVisibility(View.VISIBLE);

        surveyQusOptionsPager.setVisibility(View.INVISIBLE);

    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkin_time = bundle.getString("checkin_time");
            customer_id = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
        }

//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        checkInTime.setText(checkin_time);
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();


        //Populate Customer Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customer_id);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                customerName.setText(customerDetails.getString(2));
                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));


                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customer_id);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }


                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File(filepath.getAbsolutePath()
                        + "/KSFA/CustomerImages/" + customerDetails.getString(36));

                File imgFile = new File(String.valueOf(dir));

                if (imgFile.exists()) {

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView myImage = (ImageView) findViewById(R.id.customer_pic);
                    myImage.setImageBitmap(myBitmap);

                }

            }
        }

    }


    private void setSurveyDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        surveyDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        surveyDateString = timeStampFormat.format(myDate);
    }

    private void initializeViews() {
        actionBarTitle = findViewById(R.id.toolbar_title);
        actionBarBack = findViewById(R.id.back);
        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        customerName = findViewById(R.id.customerNameTv);
        city = findViewById(R.id.customerCityTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        customerImage = findViewById(R.id.customerImageView);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        survey_name_spinner = findViewById(R.id.survey_name_spinner);
        surveyDescription = findViewById(R.id.survey_description);
        surveyDate = findViewById(R.id.survey_date);
        previousButton = findViewById(R.id.surveyPreviousButton);
        nextButton = findViewById(R.id.surveyNextButton);
        buttonContainer = findViewById(R.id.button_container);
        thankYouContainer = findViewById(R.id.thank_you_container);
        thankYouLayout = findViewById(R.id.quiz_score);
        scoreValue = thankYouContainer.findViewById(R.id.score);
        surveyReportButton = findViewById(R.id.surveyReports);
        surveyQusOptionsPager = findViewById(R.id.optionsPager);


        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        surveyTable = new SurveyTable(getApplicationContext());
        surveyQuestionOptionTable = new SurveyQuestionOptionTable(getApplicationContext());
        surveyResponseTable = new SurveyResponseTable(getApplicationContext());
        surveyResponseAnswerTable = new SurveyResponseAnswerTable(getApplicationContext());


    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

}


class QuestionsViewPager extends ViewPager {
    private boolean enabled;

    public QuestionsViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}


class QuestionsPageAdapter extends PagerAdapter {

    public ArrayList<SurveyQuestions> questionData;
    TextView questionTopic, questionText, optionTitle;
    EditText answerText;
    RadioGroup radioGroup;
    RadioButton radioButton;
    CheckBox checkBoxBtn;
    ScrollView scrollView, checkBoxScrollView;
    LinearLayout checkBoxLayout;
    String correctAnswer;
    int numberOfQuestions = 0;
    private Context mContext;

    QuestionsPageAdapter(Context context, ArrayList<SurveyQuestions> qData) {
        mContext = context;
        questionData = qData;
        numberOfQuestions = qData.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        Log.d("Question", "instantiateItem " + position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final ViewGroup page = (ViewGroup) inflater.inflate(R.layout.survey_questions, container, false);

        scrollView = page.findViewById(R.id.options_scrollView);
        checkBoxScrollView = page.findViewById(R.id.options_checkbox_scrollView);
        checkBoxLayout = page.findViewById(R.id.optionCheckboxes);
        questionTopic = page.findViewById(R.id.question_topic);
        questionText = page.findViewById(R.id.question);
        optionTitle = page.findViewById(R.id.optionTitle);
        radioGroup = page.findViewById(R.id.radio_group);
        answerText = page.findViewById(R.id.answer_text);

        answerText.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        checkBoxScrollView.setVisibility(View.GONE);
        questionText.setText("");
        questionText.setText(questionData.get(position).getQuestion_name());

        String questionType = questionData.get(position).getQuestion_type();
        Log.d("QText", "qname " + questionData.get(position).getQuestion_name() + " QTYPE" + questionType);
        ArrayList<CheckBox> checkBoxes = null;
        if (questionType != null && (questionType.equals("Single Choice") || questionType.equals("Both") || questionType.equals("Multiple Choice"))) {
            optionTitle.setText("Options:");
            optionTitle.setTextSize(16);
            if (questionType.equals("Single Choice") || questionType.equals("Both")) {
                scrollView.setVisibility(View.VISIBLE);
            } else {
                checkBoxScrollView.setVisibility(View.VISIBLE);
                checkBoxes = new ArrayList<>();
            }
            final List<SurveyQuestionOption> opt = questionData.get(position).getSurveyQuestionOptions();
            final ArrayList<Integer> correctAnswersArray = new ArrayList<>();
            int counter = 0;
            LinearLayout linearLayout = new LinearLayout(mContext);
            //  Log.d("QText", "qname " + questionData.get(position).getQuestion_name() + " QTYPE" + questionType + " optsize "+opt.size() );
            int radioCheckedIndex = -1;

            try {
                for (int i = 0; i < opt.size(); i++) {
                    radioGroup.setOrientation(LinearLayout.VERTICAL);
                    if (questionType.equals("Single Choice") || questionType.equals("Both")) {
                        radioButton = new RadioButton(mContext);
                        radioButton.setText(opt.get(i).getOptionText());
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT, 1F);
                        radioButton.setTextColor(Color.BLACK);
                        //radioButton.setButtonTintMode(R.drawable.my_compound_button_color_selector);
                        radioButton.setLayoutParams(lp);
//                        final int sdk = android.os.Build.VERSION.SDK_INT;
//                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                            radioButton.setBackgroundDrawable(ContextCompat.getDrawable(mContext.getApplicationContext(), R.drawable.border_layout_header));
//                        } else {
//                            radioButton.setBackground(ContextCompat.getDrawable(mContext.getApplicationContext(), R.drawable.border_layout_header));
//                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            radioButton.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.orange)));
                        }
//                        float scale = mContext.getResources().getDisplayMetrics().density;
//                        int dpAsPixels = (int) (5 * scale + 0.5f);
                        radioButton.setPadding(0, 10, 0, 10);
                        radioButton.setSingleLine(true);
                        radioButton.setTextColor(Color.parseColor("#000000"));
                        radioButton.setTextSize(15);
                        //radioButton.setBackground(R.drawable.);
                        radioButton.setTag(opt.get(i).id);
                        //radioButton.setGravity(Gravity.LEFT);
                        radioButton.setEms(15);
                        //radioButton.setButtonDrawable(null);
                        //Drawable img = mContext.getResources().getDrawable(android.R.drawable.btn_radio);
                        //radioButton.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                        if (questionData.get(position).seletectedRadioGroupIndex != -1 && opt.get(i).id == questionData.get(position).seletectedRadioGroupIndex) {
                            radioCheckedIndex = i;
                        }
                        radioGroup.addView(radioButton);
                        if (questionType.equals("Both")) {
                            optionTitle.setText("Enter Text:");
                            optionTitle.setTextSize(16);
                            answerText.setVisibility(View.VISIBLE);
                            answerText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                            if (questionData.get(position).inputTextAnswer != null)
                                answerText.setText(questionData.get(position).inputTextAnswer);
                            questionData.get(position).editText = answerText;

                            answerText.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    questionData.get(position).inputTextAnswer = s.toString();
                                }

                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count,
                                                              int after) {
                                    // TODO Auto-generated method stub
                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    // TODO Auto-generated method stub

                                }
                            });
                        }
                    } else {
                        checkBoxBtn = new CheckBox(mContext);
                        checkBoxBtn.setText(opt.get(i).getOptionText());
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT, 1F);
                        checkBoxBtn.setTextColor(Color.BLACK);
                        checkBoxBtn.setLayoutParams(lp);
                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            checkBoxBtn.setBackgroundDrawable(ContextCompat.getDrawable(mContext.getApplicationContext(), R.drawable.border_layout_header));
                        } else {
                            checkBoxBtn.setBackground(ContextCompat.getDrawable(mContext.getApplicationContext(), R.drawable.border_layout_header));
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            checkBoxBtn.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.orange)));
                        }
                        float scale = mContext.getResources().getDisplayMetrics().density;
                        //  int dpAsPixels = (int) (5 * scale + 0.5f);
                        // checkBoxBtn.setPadding(0, dpAsPixels, 0, 10);
                        checkBoxBtn.setPadding(0, 10, 0, 10);
                        checkBoxBtn.setSingleLine(true);
                        checkBoxBtn.setTextColor(Color.parseColor("#000000"));
                        checkBoxBtn.setTextSize(15);
                        checkBoxBtn.setTag(opt.get(i).id);
                        checkBoxBtn.setEms(15);
                        checkBoxBtn.setBackgroundColor(Color.parseColor("#faf8f8"));
                        checkBoxBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                int s = (int) buttonView.getTag();
                                if (isChecked) {
                                    questionData.get(position).selectedCheckBoxIndexs.add(s);
                                } else {
                                    questionData.get(position).selectedCheckBoxIndexs.remove(s);

                                }
                            }
                        });
                /*checkBoxBtn.setButtonDrawable(null);

                Drawable img = mContext.getResources().getDrawable(android.R.drawable.btn_default);
                checkBoxBtn.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);*/
                        if (questionData.get(position).selectedCheckBoxIndexs.indexOf(opt.get(i).id) != -1)
                            checkBoxBtn.setChecked(true);
                        Log.d("QText", "qname " + questionData.get(position).getQuestion_name() + " QANS" + opt.get(i).getOptionText());
                        checkBoxLayout.addView(checkBoxBtn);
                        checkBoxes.add(checkBoxBtn);
                    }
                }

            } catch (Exception e) {

            }

            if (questionType.equals("Single Choice") || questionType.equals("Both")) {
                radioGroup.setOnCheckedChangeListener(null);
                if (questionData.get(position).seletectedRadioGroupIndex != -1 && radioCheckedIndex != -1)
                    ((RadioButton) radioGroup.getChildAt(radioCheckedIndex)).setChecked(true);
                questionData.get(position).radioGroup = radioGroup;

                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        String selectedAnswerId = String.valueOf(checkedId);
                        Log.d("QuizData", "selectedId " + selectedAnswerId);
                        // find the radiobutton by returned id
                        RadioButton radioSelectedButton = questionData.get(position).radioGroup.findViewById(checkedId);
                        if (radioSelectedButton != null) {
                            questionData.get(position).seletectedRadioGroupIndex = Integer.parseInt(radioSelectedButton.getTag().toString());//questionData.get(position).radioGroup.indexOfChild();
                        }

                    }
                });
            } else {

                questionData.get(position).checkBoxes = checkBoxes;
            }
        } else if (questionType.equals("Free Text")) {
            optionTitle.setText("Enter Text:");
            optionTitle.setTextSize(16);
            answerText.setVisibility(View.VISIBLE);
            answerText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            if (questionData.get(position).inputTextAnswer != null)
                answerText.setText(questionData.get(position).inputTextAnswer);
            questionData.get(position).editText = answerText;

            answerText.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    questionData.get(position).inputTextAnswer = s.toString();
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }
            });
        }
        container.addView(page);
        return page;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return numberOfQuestions;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public ArrayList<SurveyQuestions> getQuestionData() {
        return questionData;
    }

}
