package in.kumanti.emzor.backgroundService;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.ExpenseTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.NewJourneyPlanTable;
import in.kumanti.emzor.model.CollectionDetails;
import in.kumanti.emzor.model.Expense;
import in.kumanti.emzor.model.MasterResponseJson;
import in.kumanti.emzor.model.NewJourneyPlan;
import in.kumanti.emzor.model.Orders;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MasterRDLService extends IntentService {

    public static final String MY_ACTION = "action.SOME_ACTION";
    private static final int MY_JOB_INTENT_SERVICE_ID = 500;
    String login_id, companyCode = "";
    ArrayList<Orders> ordersArrayList1;
    ArrayList<CollectionDetails> collectionPrArraylist1;
    ArrayList<CollectionDetails> collectionSecArraylist1;
    ArrayList<NewJourneyPlan> newJourneyPlanArrayList1;
    NewJourneyPlanTable newJourneyPlanTable1;

    ArrayList<Expense> expenseArrayList1;
    ExpenseTable expenseTable1;


    public MasterRDLService() {
        super("test-service");
    }

    @Override
    public void onCreate() {
        super.onCreate(); // if you override onCreate(), make sure to call super().
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Bundle bundle= intent.getExtras();
        if (bundle!= null) {
            login_id = bundle.getString("login_id");
            companyCode = bundle.getString("companyCode");
        }

        new Thread(new Runnable() {
            @Override
            public void run() {

                postingData();

            }
        }).run();


    }


    private void postingData() {

        Gson gson = new GsonBuilder().setLenient().create();

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL).
                        client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the Gson ConverterFactory to directly convert json data to object
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Call<MasterResponseJson> orderStatusUpdate = api.getMasterRDLData(login_id, companyCode);

        Log.d("PostingData", "Service Status---Company:--" + companyCode);

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        newJourneyPlanTable1 = new NewJourneyPlanTable(getApplicationContext());

        expenseTable1 = new ExpenseTable(getApplicationContext());

        orderStatusUpdate.enqueue(new Callback<MasterResponseJson>() {

            @Override
            public void onResponse(Call<MasterResponseJson> customerMaster, retrofit2.Response<MasterResponseJson> response) {

                MasterResponseJson resultSet = response.body();

                if (resultSet.status.equals("true")) {

                    ordersArrayList1 = new Gson().fromJson(resultSet.order_status_update, new TypeToken<ArrayList<Orders>>() {
                    }.getType());


                    Log.d("PostingData", "Status---OrderArrayList:--" + ordersArrayList1.size());


                    String[] orders = new String[resultSet.order_status_update.size()];

                    //looping through all the heroes and inserting the names inside the string array
                    for (int i = 0; i < ordersArrayList1.size(); i++) {
                        Log.i("PostingData", "OrderUpdateService---" + ordersArrayList1.get(i).getOrderCode() + ordersArrayList1.get(i).getStatus());
                        dbHandler.updateOrderStatusfromweb(ordersArrayList1.get(i).getOrderCode(), ordersArrayList1.get(i).getStatus());

                    }

                    newJourneyPlanArrayList1 = new Gson().fromJson(resultSet.journeyPlanStatus, new TypeToken<ArrayList<NewJourneyPlan>>() {
                    }.getType());

                    if (newJourneyPlanArrayList1 != null)
                        for (int i = 0; i < newJourneyPlanArrayList1.size(); i++) {
                            Log.i("NewJourneyPlan Status", "JP Array Size---" + newJourneyPlanArrayList1.size());
                            Log.i("NewJourneyPlan Status", "JP Array First Element---" + newJourneyPlanArrayList1.get(0).toString());
                            Log.i("NewJourneyPlan Status", "NewJourneyPlan ID---" + newJourneyPlanArrayList1.get(i).getJourneyPlanNumber() + "  Status--" + newJourneyPlanArrayList1.get(i).getStatus());
                            newJourneyPlanTable1.updateJourneyPlanStatusFromWeb(newJourneyPlanArrayList1.get(i).getJourneyPlanNumber(), newJourneyPlanArrayList1.get(i).getStatus());

                        }


                    expenseArrayList1 = new Gson().fromJson(resultSet.expense, new TypeToken<ArrayList<Expense>>() {
                    }.getType());

                    if (expenseArrayList1 != null)
                        for (int i = 0; i < expenseArrayList1.size(); i++) {
                            System.out.println("expenseList.size() = " + expenseArrayList1.size());
                            System.out.println("expenseList.expenseArrayList1() = " + expenseArrayList1);
                            System.out.println("expenseList.getExpenseNumber() = " + expenseArrayList1.get(i).getExpenseNumber());
                            System.out.println("expenseList.getExpenseApprovedAmount() = " + expenseArrayList1.get(i).getExpenseApprovedAmount());
                            System.out.println("expenseList.getExpenseStatus() = " + expenseArrayList1.get(i).getExpenseStatus());
                            System.out.println("expenseList.getExpenseStatus() = " + expenseArrayList1.get(i).getExpenseReason());

                            expenseTable1.updateExpenseMaster(expenseArrayList1.get(i).getExpenseNumber(), expenseArrayList1.get(i).getExpenseApprovedAmount(),expenseArrayList1.get(i).getExpenseStatus(),expenseArrayList1.get(i).getExpenseReason());

                        }



                    collectionPrArraylist1 = new Gson().fromJson(resultSet.prstatus, new TypeToken<ArrayList<CollectionDetails>>() {
                    }.getType());


                    System.out.println("collectionPrArraylist1.size() = " + collectionPrArraylist1.size());
                    System.out.println("collectionPrArraylist1.size() = " + resultSet.prstatus.size());
                    String[] primaryCollectionDetails = new String[resultSet.prstatus.size()];
                    if (collectionPrArraylist1 != null) {
                        for (int i = 0; i < collectionPrArraylist1.size(); i++) {
                            System.out.println("collectionPrArraylist1 = " + collectionPrArraylist1);
                            System.out.println("collectionPrArraylist1.get(i).getCollectionCode() = " + collectionPrArraylist1.get(i).getCollectionCode());
                            System.out.println("collectionPrArraylist1.get(i).getCollectionCode() = " + collectionPrArraylist1.get(i).getCollectionStatus());
                            Log.i("PostingData", "OrderUpdateService---" + collectionPrArraylist1.get(i).getCollectionCode() + collectionPrArraylist1.get(i).getCollectionStatus());
                            System.out.println("CollectionPrimary= " + collectionPrArraylist1.get(i).getCollectionCode() + collectionPrArraylist1.get(i).getCollectionStatus());
                            dbHandler.uptPriCollectionStatfrweb(collectionPrArraylist1.get(i).getCollectionCode(), collectionPrArraylist1.get(i).getCollectionStatus());

                        }
                    }

                    collectionSecArraylist1 = new Gson().fromJson(resultSet.secstatus, new TypeToken<ArrayList<CollectionDetails>>() {
                    }.getType());


                    String[] secCollectionDetails = new String[resultSet.secstatus.size()];

                    for (int i = 0; i < collectionSecArraylist1.size(); i++) {
                        Log.i("PostingData", "OrderUpdateService---" + collectionSecArraylist1.get(i).getCollectionCode() + collectionSecArraylist1.get(i).getCollectionStatus());
                        System.out.println("CollectionSecondary= " +collectionSecArraylist1.get(i).getCollectionCode() + collectionSecArraylist1.get(i).getCollectionStatus() );
                        dbHandler.uptSecCollectionStatfrweb(collectionSecArraylist1.get(i).getCollectionCode(), collectionSecArraylist1.get(i).getCollectionStatus());

                    }


                }
            }

            @Override
            public void onFailure(Call<MasterResponseJson> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        Log.i("OrderUpdateService", "Service running");

    }

}
