package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.model.SurveyReport;
import in.kumanti.emzor.model.SurveyResponse;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_DETAILS;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY_QUESTIONS;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY_RESPONSE;
import static in.kumanti.emzor.utils.Constants.TABLE_SURVEY_RESPONSE_ANSWER;

public class SurveyResponseTable extends SQLiteOpenHelper {

    private static final String COLUMN_SURVEY_RESPONSE_ID = "id";
    private static final String COLUMN_PRODUCT_ID = "product_id";
    private static final String COLUMN_COMPANY_ID = "company_id";
    private static final String COLUMN_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_SALES_REP_ID = "sales_rep_id";
    private static final String COLUMN_SURVEY_ID = "survey_id";
    private static final String COLUMN_SURVEY_STATUS = "status";
    private static final String COLUMN_SURVEY_CREATED_AT = "created_at";
    private static final String COLUMN_SURVEY_UPDATED_AT = "updated_at";
    private static final String COLUMN_SURVEY_RESPONSE_IS_SYNC = "is_sync";
    private final static String COLUMN_SURVEY_RESPONSE_SEQUENCE_NO = "visitSeqNo";

    public Context context;
    private SQLiteDatabase sqlWrite;
    private SurveyResponseAnswerTable surveyResponseAnswerTable;

    public SurveyResponseTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY_RESPONSE);

        String surveyTable = "CREATE TABLE IF NOT EXISTS " + TABLE_SURVEY_RESPONSE + "(" +
                COLUMN_SURVEY_RESPONSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PRODUCT_ID + " TEXT, " +
                COLUMN_COMPANY_ID + " TEXT, " +
                COLUMN_CUSTOMER_ID + " TEXT, " +
                COLUMN_SALES_REP_ID + " TEXT, " +
                COLUMN_SURVEY_ID + " TEXT, " +
                COLUMN_SURVEY_STATUS + " TEXT, " +
                COLUMN_SURVEY_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SURVEY_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_SURVEY_RESPONSE_IS_SYNC + " TEXT, " +
                COLUMN_SURVEY_RESPONSE_SEQUENCE_NO + " TEXT " +
                ");";
        sqlWrite.execSQL(surveyTable);

        surveyResponseAnswerTable = new SurveyResponseAnswerTable(context);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(SurveyResponse surveyResponse) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_COMPANY_ID, surveyResponse.getCompany_id());
        values.put(COLUMN_CUSTOMER_ID, surveyResponse.getCustomer_id());
        values.put(COLUMN_PRODUCT_ID, surveyResponse.getProduct_id());
        values.put(COLUMN_SALES_REP_ID, surveyResponse.getSales_rep_id());
        values.put(COLUMN_SURVEY_ID, surveyResponse.getSurvey_id());
        values.put(COLUMN_SURVEY_STATUS, surveyResponse.getStatus());
        values.put(COLUMN_SURVEY_RESPONSE_SEQUENCE_NO, surveyResponse.getVisitSequenceNumber());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_SURVEY_RESPONSE, null, values);
        db.close();
        return id;
    }

    public SurveyResponse getSurveyResponseById(String surveyCode, String customerId) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_SURVEY_RESPONSE + " WHERE " + COLUMN_SURVEY_ID + "= ? AND " + COLUMN_CUSTOMER_ID + "=?";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{surveyCode, customerId});

        SurveyResponse survey = null;
        while (cursor.moveToNext()) {
            survey = new SurveyResponse();
            int id = cursor.getInt(0);
            survey.setId(id);
            survey.setCompany_id(cursor.getString(1));
            survey.setResponseAnswers(surveyResponseAnswerTable.getSurveyResponseById(String.valueOf(id)));
        }
        cursor.close();
        sqlWrite.close();
        return survey;
    }

    public void update(String id, SurveyResponse surveyResponse) {
        sqlWrite = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SURVEY_STATUS, surveyResponse.getStatus());
        sqlWrite.update(TABLE_SURVEY_RESPONSE, values, COLUMN_SURVEY_RESPONSE_ID + "=" + id, null);
        sqlWrite.close();
    }

    public ArrayList<SurveyReport> getSurveyReportsBySalesRep(String salesRepId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT s.survey_name,sr.created_at,(select question_name FROM " + TABLE_SURVEY_QUESTIONS + " as tsq WHERE tsq.id = sra.survey_question_id) as question,sra.option_value,cust_det.customer_name FROM " + TABLE_SURVEY_RESPONSE + " as sr"
                + " JOIN " + TABLE_SURVEY_RESPONSE_ANSWER + " as sra ON sra.response_answer_id = sr.id "
                + " JOIN " + TABLE_SURVEY + " as s ON sr.survey_id = s.survey_code "
                + " LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON sr.customer_id = cust_det.customer_id ";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{salesRepId});

        ArrayList<SurveyReport> surveyReportArrayList = new ArrayList<>();
        int i = 0;
        Log.d("SurveyResponse", "ResponseGet " + salesRepId);
        while (cursor.moveToNext()) {
            SurveyReport sr = new SurveyReport();
            sr.setSurvey_name(cursor.getString(0));
            String dateTime = cursor.getString(1);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd MMM yy");

            Date myDate = new java.util.Date(Long.parseLong(dateTime) * 1000L);
            String date = dateFormat.format(myDate);
            sr.setDate(date);

            sr.setQuestion(cursor.getString(2));
            sr.setAnswer(cursor.getString(3));
            sr.setCustomer_name(cursor.getString(4));
            surveyReportArrayList.add(i, sr);
            i++;
            Log.d("SurveyResponse", "survey_id " + cursor.getString(0) + " date:" + date + " ques:" + cursor.getString(2) + " ans:" + cursor.getString(3));
        }
        cursor.close();
        sqlWrite.close();
        return surveyReportArrayList;
    }

    public ArrayList<SurveyResponse> getAllSurveyResponseForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_SURVEY_RESPONSE;

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        Log.d("Data", "getCompetitorDetailsByProductId  ");

        ArrayList<SurveyResponse> surveyResponseArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            SurveyResponse sr = new SurveyResponse();
            sr.setTabId("1");
            sr.setId(cursor.getInt(0));
            sr.setCompany_id(cursor.getString(2));
            sr.setCustomer_id(cursor.getString(2));
            sr.setSales_rep_id(cursor.getString(2));
            sr.setSurvey_id(cursor.getString(2));
            surveyResponseArrayList.add(sr);
        }
        cursor.close();
        sqlWrite.close();
        return surveyResponseArrayList;
    }


}
