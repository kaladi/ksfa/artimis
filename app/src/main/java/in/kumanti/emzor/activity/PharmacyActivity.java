package in.kumanti.emzor.activity;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.adapter.PharmacyInfoProductsAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.PharmacyInfoProductsTable;
import in.kumanti.emzor.eloquent.PharmacyInfoTable;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.model.PharmacyInfo;
import in.kumanti.emzor.model.PharmacyInfoProducts;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.utils.CustomSearchableSpinner;
import in.kumanti.emzor.utils.Globals;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class PharmacyActivity extends MainActivity {

    public RecyclerView pharmacyProductsRecyclerView;
    String pharmacyType = "InHouse";
    TextView actionbarTitle, marqueeText, checkInTime;
    ImageView actionbarBackButton, deviceInfo, customerImage;
    TextView doctorName, accountNo, address1, address2, address3, city, cust_order_value, cust_order_count, cust_invoice_value, cust_invoice_count, sr_name, outstanding_val;
    String ship_address1, ship_address2, ship_address3, ship_city1, ship_state1, ship_country1;
    MyDBHandler dbHandler;
    Chronometer duration;
    SharedPreferenceManager sharedPreferenceManager;
    String checkin_date, sr_name1, save_checkin_date;
    String customer_id = null, pharmacyDateString = null;
    String customer_type = null;
    Globals globals;
    LinearLayout createPharmacyLayout;
    TextView pharmacyDate;
    EditText newPharmacyName;
    CustomSearchableSpinner productSpinner, pharmacyNameSpinner;
    Button addPharmacyNameButton;
    ArrayAdapter<Product> productSpinnerArrayAdapter;
    ArrayAdapter<PharmacyInfo> pharmacySpinnerArrayAdapter;
    ArrayList<Product> productArrayList;
    ArrayList<Product> newProductList;
    ArrayList<PharmacyInfo> pharmacyInfoArrayList;
    ArrayList<PharmacyInfo> newPharmacyInfoArrayList;
    Button addRowButton;
    RecyclerViewItemListener listener;
    ImageButton saveProductsButton;
    String checkInTimeString = "";
    String customerCode = "", productCodeString = null;
    int pharmacyId = -1;
    PharmacyInfoTable pharmacyInfoTable;
    ProductTable productTable;
    PharmacyInfoProductsTable pharmacyInfoProductsTable;
    ArrayList<PharmacyInfoProducts> pharmacyInfoProductsArrayList = new ArrayList<PharmacyInfoProducts>();
    String login_id = "";
    private RecyclerView.Adapter pharmacyProductsAdapter;
    private RecyclerView.LayoutManager pharmacyProductsLayoutManager;
    private String blockCharacterSet = "~#^|$%&*!@(){}[]<>,.'?-+=_/:;";
    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_pharmacy);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();
        setPharmacyDate();

        if (pharmacyType.equals("InHouse")) {
            actionbarTitle.setText("In-House Pharmacy");
        } else {
            actionbarTitle.setText("Neighbourhood Pharmacy");
        }

        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        //Populate the Pharmacy Product details using the Recycler view
        pharmacyProductsRecyclerView.setHasFixedSize(true);
        pharmacyProductsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        pharmacyProductsRecyclerView.setLayoutManager(pharmacyProductsLayoutManager);

        updatePharmacyNameSpinner();
        //Pharmacy Name Spinner
        pharmacyNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                pharmacyNameSpinner.isSpinnerDialogOpen = false;

                PharmacyInfo p = (PharmacyInfo) adapterView.getItemAtPosition(pos);
                pharmacyId = p.getId();
                if (pharmacyId == 0) {
                    createPharmacyLayout.setVisibility(View.VISIBLE);
                } else {
                    createPharmacyLayout.setVisibility(View.GONE);
                    if (pharmacyId != -1) {
                        pharmacyInfoProductsArrayList = pharmacyInfoProductsTable.getPharmacyInfoProducts(String.valueOf(pharmacyId));
                        pharmacyProductsAdapter = new PharmacyInfoProductsAdapter(pharmacyInfoProductsArrayList, listener);
                        pharmacyProductsRecyclerView.setAdapter(pharmacyProductsAdapter);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                pharmacyNameSpinner.isSpinnerDialogOpen = false;
            }
        });


        //Product Spinner Initialization
        updateProductSpinner();
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;

                Product p = (Product) adapterView.getItemAtPosition(pos);
                productCodeString = p.getProduct_code();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;

            }
        });

        listener = (view, position) -> {
            updateProductSpinner();
        };


        //Generate new row or item in opportunity details Recycler view
        addRowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPharmacyProducts();
            }
        });

        addPharmacyNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(newPharmacyName.getText().toString())) {
                    newPharmacyName.setError("Please enter Name");
                    return;
                }
                if (pharmacyInfoTable.checkPharmacyNameIsExist(newPharmacyName.getText().toString())) {
                    newPharmacyName.setError("Pharmacy Name already Exist");
                    return;
                }

                String visitSequence = sharedPreferences.getString("VisitSequence", "");
                newPharmacyName.setError(null);
                PharmacyInfo pharmacyInfo = new PharmacyInfo();
                pharmacyInfo.setSrCode(login_id);
                pharmacyInfo.setCustomerCode(customerCode);
                pharmacyInfo.setPharmacyType(pharmacyType);
                pharmacyInfo.setPharmacyName(newPharmacyName.getText().toString());
                pharmacyInfo.setVisitSequenceNumber(visitSequence);
                long id = pharmacyInfoTable.create(pharmacyInfo);
                Toast.makeText(getApplicationContext(), "New Pharmacy Info Added", Toast.LENGTH_SHORT).show();
                updatePharmacyNameSpinner();
                int index = 0;
                int spinnerIndex = 0;
                for (PharmacyInfo pi : pharmacyInfoArrayList) {
                    if (pi.getId() == id) {
                        spinnerIndex = index;
                    }
                    index++;
                }
                newPharmacyName.setText("");
                createPharmacyLayout.setVisibility(View.GONE);
                pharmacyNameSpinner.setSelection(spinnerIndex);
            }
        });


        saveProductsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String visitSequence = sharedPreferences.getString("VisitSequence", "");

                hideSoftKeyboard(getApplicationContext(), v);
                View current = getCurrentFocus();
                current.clearFocus();

                PharmacyInfo pharmacyInfo = (PharmacyInfo) pharmacyNameSpinner.getSelectedItem();
                if (pharmacyInfo.getId() == -1 || pharmacyInfo.getId() == 0) {
                    Toast.makeText(getApplicationContext(), "Please select Pharmacy or Create Pharmacy", Toast.LENGTH_SHORT).show();
                    return;
                }
                boolean isValid = true;
                for (PharmacyInfoProducts pip : pharmacyInfoProductsArrayList) {
                    if (TextUtils.isEmpty(pip.getPrescriptionsCount()) || pip.getPrescriptionsCount().equals("0")) {
                        pip.setErrorPrescriptionsCount("Please enter prescription count");
                        isValid = false;
                    } else
                        pip.setErrorPrescriptionsCount(null);
                }
                if (!isValid) {
                    pharmacyProductsAdapter.notifyDataSetChanged();
                    return;
                }

                pharmacyInfo.setVisitSequenceNumber(visitSequence);
                //pharmacyInfoTable.create(pharmacyInfo);

                if (pharmacyInfoProductsArrayList.size() != 0) {
                    pharmacyInfoProductsTable.deletePrescriptionProduct(String.valueOf(pharmacyInfo.getId()));
                    for (PharmacyInfoProducts pip : pharmacyInfoProductsArrayList) {
                        try {
                            long id = pharmacyInfoProductsTable.create(pip);
                            //Log.d("QueryResponse", pip.getProductName() + " id:" + id);
                        } catch (Exception e) {
                            Log.d("QueryResponse", e.getMessage());
                        }
                    }
                    Toast.makeText(getApplicationContext(), "Pharmacy Created Successfully", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "There is no products added", Toast.LENGTH_SHORT).show();
                }
            }
        });

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }

    @Override
    public void onPause() {
        //onDetachedFromWindow();
        super.onPause();

    }

    /**
     * Update the Product Spinner, based upon the addition and deletion of the products in Opportunity Product Details RecyclerView List
     */
    private void updatePharmacyNameSpinner() {
        //To Set the Values to the Product Spinner
        pharmacyInfoArrayList = pharmacyInfoTable.getPharmacyInfo(pharmacyType, customerCode);
        PharmacyInfo pi = new PharmacyInfo();
        pi.setId(-1);
        pi.setPharmacyName("Select");
        pharmacyInfoArrayList.add(0, pi);
        PharmacyInfo pi1 = new PharmacyInfo();
        pi1.setId(0);
        pi1.setPharmacyName("Create New Pharmacy");
        pharmacyInfoArrayList.add(1, pi1);

        pharmacySpinnerArrayAdapter = new ArrayAdapter<PharmacyInfo>(getApplicationContext(), android.R.layout.simple_spinner_item, pharmacyInfoArrayList);
        pharmacySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        pharmacyNameSpinner.setAdapter(pharmacySpinnerArrayAdapter);
    }


    /**
     * Update the Product Spinner, based upon the addition and deletion of the products in Opportunity Product Details RecyclerView List
     */
    private void updateProductSpinner() {
        //To Set the Values to the Product Spinner
        productArrayList = productTable.getProducts();
        //Remove the already selected product
        int k = 0;
        newProductList = new ArrayList<Product>();
        for (Product product : productArrayList) {
            boolean flag = true;
            for (PharmacyInfoProducts products : pharmacyInfoProductsArrayList) {
                if (product.product_id.equals(products.getProductCode()))
                    flag = false;
            }
            if (flag)
                newProductList.add(product);
            k++;
        }

        Product p = new Product();
        p.setProduct_id("-1");
        p.setProduct_name("Select");
        newProductList.add(0, p);

        productSpinner.setTitle("Select a Product");
        productSpinner.setPositiveButton("OK");

        productSpinnerArrayAdapter = new ArrayAdapter<Product>(getApplicationContext(), android.R.layout.simple_spinner_item, newProductList);
        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(productSpinnerArrayAdapter);
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    /**
     * Adding of Products Details in Opportunity Recycler View List
     */
    private void addPharmacyProducts() {


        Product selectedProduct = (Product) productSpinner.getSelectedItem();//newProductList.get(productSpinner.getSelectedItemPosition());
        PharmacyInfo pharmacyInfo = (PharmacyInfo) pharmacyNameSpinner.getSelectedItem();
        if (pharmacyInfo != null && (pharmacyInfo.getId() == 0 || pharmacyInfo.getId() == -1)) {
            Toast.makeText(getApplicationContext(), "Please choose a Pharmacy Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if (selectedProduct != null && productSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(getApplicationContext(), "Please choose a product before adding to the Product List", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("ProductData", "" + selectedProduct.product_code);
        PharmacyInfoProducts product = new PharmacyInfoProducts();
        product.setProductName(selectedProduct.product_name);
        product.setProductCode(selectedProduct.product_id);
        product.setPharmacyInfoId(String.valueOf(pharmacyInfo.getId()));
        // product.setRemarks(selectedProduct.product_uom);
        // product.setStatus(selectedProduct.product_id);
        pharmacyInfoProductsArrayList.add(product);
        // pharmacyProductsAdapter.notifyDataSetChanged();

        pharmacyProductsAdapter = new PharmacyInfoProductsAdapter(pharmacyInfoProductsArrayList, listener);
        pharmacyProductsRecyclerView.setAdapter(pharmacyProductsAdapter);
        Log.d("Recycle", "Data Added: " + selectedProduct.product_name);
        updateProductSpinner();
    }


    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        checkInTime = findViewById(R.id.checkInTimeValueTextView);
        duration = findViewById(R.id.durationValueTextView);

        customerImage = findViewById(R.id.customerImageView);
        doctorName = findViewById(R.id.customerNameTv);
        accountNo = findViewById(R.id.customerAccountNoTv);
        city = findViewById(R.id.customerCityTv);

        pharmacyDate = findViewById(R.id.pharmacyDateTv);
        newPharmacyName = findViewById(R.id.pharmacyNameEditText);
        addPharmacyNameButton = findViewById(R.id.addPharmacyNameButton);
        pharmacyNameSpinner = findViewById(R.id.pharmacyNameSpinner);
        createPharmacyLayout = findViewById(R.id.createPharmacyLayout);
        productSpinner = findViewById(R.id.pharmacyProdSpinner);
        pharmacyProductsRecyclerView = findViewById(R.id.pharmacyProdRecyclerView);
        addRowButton = findViewById(R.id.addPharmacyProdRowButton);
        saveProductsButton = findViewById(R.id.savePharmacyProdButton);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        pharmacyInfoTable = new PharmacyInfoTable(getApplicationContext());
        productTable = new ProductTable(getApplicationContext());
        pharmacyInfoProductsTable = new PharmacyInfoProductsTable(getApplicationContext());
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        globals = ((Globals) getApplicationContext());

        newPharmacyName.setFilters(new InputFilter[]{filter});


    }

    public void populateHeaderDetails() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        duration.setBase(SystemClock.elapsedRealtime() + SharedPreferenceManager.instance().getTimeSpentOnLevel());
        duration.start();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            checkInTimeString = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            pharmacyType = bundle.getString("pharmacyType");
            customer_type = bundle.getString("customer_type");

            Log.d("LoginId", login_id);
        }

        //checkInTime.setText(checkInTimeString);
        Log.d("Pharmacy ---", "CheckInTime--" + checkInTimeString);


        customer_type = globals.getCustomer_type();
        checkInTime.setText(checkInTimeString);


        //Populate Header Details
        Cursor customerDetails = dbHandler.getCustomerDetails(customerCode);
        int numRows = customerDetails.getCount();
        if (numRows == 0) {
            // Toast.makeText(getApplicationContext(), "No Records Found  :(.", Toast.LENGTH_LONG).show();
        } else {
            int i = 0;
            while (customerDetails.moveToNext()) {
                doctorName.setText(customerDetails.getString(2));
                Log.d("Pharmacy ---", "DoctorName--" + doctorName);

                city.setText(customerDetails.getString(9) + "-" + customerDetails.getString(10));
                accountNo.setText(customerDetails.getString(1));

                Bitmap customerImageBitmap = dbHandler.getCustomerImage(customerCode);
                if (customerImageBitmap != null) {
                    customerImage.setImageBitmap(customerImageBitmap);
                }

                ship_address1 = customerDetails.getString(15);
                ship_address2 = customerDetails.getString(16);
                ship_address3 = customerDetails.getString(17);
                ship_city1 = customerDetails.getString(18);
                ship_state1 = customerDetails.getString(19);
                ship_country1 = customerDetails.getString(20);

            }
        }

    }

    private void setPharmacyDate() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        pharmacyDate.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        pharmacyDateString = timeStampFormat.format(myDate);
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }


}
