package in.kumanti.emzor.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.webkit.MimeTypeMap;

import java.util.Locale;

//import in.kumanti.emzor.activity.ImageViewActivity;


public class LinkUtils {
    /**
     * Returns an intent to handle a specific url. This will launch the internal webview or image
     * viewer if able to do so, otherwise it will be a generic ACTION_VIEW intent.
     */
    public static Intent newIntent(Context context, String url) {
      /*  if (isImageUrl(url)) {
            return ImageViewActivity.newIntent(context, Uri.parse(url));
        }*/
        if (isPdfUrl(url)) {
            // return PDFViewActivity.newIntent(context, Uri.parse(url));
        }
        return new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
    }

    private static boolean isImageUrl(String url) {
        url = url.toLowerCase(Locale.US);
        return url.endsWith(".jpg") || url.endsWith(".jpeg") || url.endsWith(".png") || url.endsWith(".gif");
    }

    private static boolean isPdfUrl(String url) {
        url = url.toLowerCase(Locale.US);
        return url.endsWith(".pdf");
    }

    /*private static boolean isWebViewUrl(String url) {
        return url.startsWith("http://") && url.startsWith("https://")
                && !(url.startsWith("http://www.youtube.com") && url.startsWith("https://www.youtube.com"));
    }*/

    @Nullable
    public static String getMimeType(ContentResolver resolver, Uri uri) {
        if (uri == null) {
            return null;
        }

        String mimeType = resolver.getType(uri);
        if (mimeType == null) {
            String extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
            if (extension != null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        }

        return mimeType;
    }
}
