package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.mastersData.MarqueeText;

public class MyMessageAdapter extends RecyclerView.Adapter<MyMessageAdapter.ViewHolder> {

    public ArrayList<MarqueeText> mDataset;
    public Context context;

    public MyMessageAdapter(ArrayList<MarqueeText> myDataset) {
        mDataset = myDataset;
    }

    public void add(MarqueeText item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(MarqueeText item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyMessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mymessages, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.messageSerial.setText(mDataset.get(position).getSerail());
        viewHolder.messageDate.setText(mDataset.get(position).getCreated_date());
        viewHolder.messageView.setText(mDataset.get(position).getMsgContent());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView messageSerial, messageDate, messageView;

        public ViewHolder(View v) {
            super(v);

            messageSerial = v.findViewById(R.id.messageSerialTextView);
            messageDate = v.findViewById(R.id.messageDateTextView);
            messageView = v.findViewById(R.id.messageTextView);

        }
    }


}