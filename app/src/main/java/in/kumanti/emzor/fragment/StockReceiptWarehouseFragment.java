package in.kumanti.emzor.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.StockReceiptBatchActivity;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.adapter.StockReceiptWarehouseAdapter;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.PrimaryInvoiceProductTable;
import in.kumanti.emzor.eloquent.PrimaryInvoiceTable;
import in.kumanti.emzor.eloquent.WarehouseTable;
import in.kumanti.emzor.model.PrimaryInvoice;
import in.kumanti.emzor.model.PrimaryInvoiceProduct;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.model.StockReceipt;
import in.kumanti.emzor.model.StockReceiptBatch;
import in.kumanti.emzor.model.Warehouse;
import in.kumanti.emzor.utils.Constants;
import in.kumanti.emzor.utils.CustomSearchableSpinner;

import static android.content.Context.MODE_PRIVATE;
import static in.kumanti.emzor.activity.MainActivity.PREF_NAME;


public class StockReceiptWarehouseFragment extends Fragment {

    public RecyclerView warInvoiceProRecyclerView;
    public RecyclerView.Adapter warInvoiceProductAdapter;
    public RecyclerView.LayoutManager warInvoiceProductLayoutManager;
    CustomSearchableSpinner productSpinner, warehouseSpinner, invoiceSpinner;
    ImageButton saveButton;
    TextView receipt_num, receipt_date;
    ArrayList<Product> productArrayList;
    ArrayAdapter<PrimaryInvoiceProduct> productSpinnerArrayAdapter;
    ArrayAdapter<Warehouse> warehouseSpinnerArrayAdapter;
    ArrayAdapter<PrimaryInvoice> invoiceSpinnerArrayAdapter;
    WarehouseTable warehouseTable;
    PrimaryInvoiceTable primaryInvoiceTable;
    PrimaryInvoiceProductTable primaryInvoiceProductTable;
    ArrayList<Warehouse> warehouseArrayList;
    ArrayList<PrimaryInvoice> invoiceArrayList;
    ArrayList<PrimaryInvoiceProduct> invoiceProducts;
    ArrayList<StockReceipt> stockReceiptWarProductArrayList;
    Context mContext;
    MyDBHandler dbHandler;
    String login_id = "", warehouse_id = null, invoice_id = null, product_id = null, str_receipt_id = null, str_receip_date = null, distributor_id = null, distributor_name = "";
    int receipt_id;
    ArrayList<StockReceipt> stockReceiptList = new ArrayList<StockReceipt>();
    RecyclerViewItemListener listener;
    int random_num = 0;
    String company_code, tab_code, sr_code, tab_prefix;
    Constants constants;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    public StockReceiptWarehouseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
        }
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_stock_receipt_warehouse, container, false);
        mContext = rootView.getContext();
        dbHandler = new MyDBHandler(mContext, null, null, 1);


        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();


        warehouseTable = new WarehouseTable(mContext);
        primaryInvoiceTable = new PrimaryInvoiceTable(mContext);
        primaryInvoiceProductTable = new PrimaryInvoiceProductTable(mContext);
        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
        }
        warehouseSpinner = rootView.findViewById(R.id.stockReceiptWarehouseSpinner);
        invoiceSpinner = rootView.findViewById(R.id.invoiceSpinner);
        productSpinner = rootView.findViewById(R.id.stockReceiptProductSpinner);
        warInvoiceProRecyclerView = rootView.findViewById(R.id.stockReceiptWarehouseRecyclerView);
        saveButton = rootView.findViewById(R.id.saveStockReceiptWarImageButton);
        receipt_num = rootView.findViewById(R.id.receipt_num);
        receipt_date = rootView.findViewById(R.id.receipt_date);
        //To Generate the Recepit number
        constants = new Constants();
        tab_prefix = Constants.TAB_PREFIX;
        company_code = Constants.COMPANY_CODE;
        tab_code = Constants.TAB_CODE;
        sr_code = Constants.SR_CODE;

        //To Generate the Recepit number
        receipt_id = dbHandler.get_reciept_id();
        //int rec_num = receipt_id + 1;
        //str_receipt_id = Integer.toString(rec_num);
        //receipt_num.setText(str_receipt_id);
        generateStockReceiptNumber();

        setRecieptDateTime();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (invoiceProducts.size() != 1) {
                    boolean validCheck = true;
                    for (PrimaryInvoiceProduct pip : invoiceProducts) {
                        Log.d("StockReceipt****", "InvoiceProduct Name---" +pip.getPrimaryInvoiceProductName() +" ProductCode--" +pip.getPrimaryInvoiceProductCode());
                        Log.d("StockReceipt****", "Batch Controlled---" +pip.getBatchControlled());

                        if (!TextUtils.isEmpty(pip.getBatchControlled()) && !pip.getPrimaryInvoiceProductCode().equals("-1") && pip.getBatchControlled().equals("Yes") && pip.getBatchDetailArrayList().size() == 0) {
                            Log.d("StockReceipt****", "Batch Size--" + pip.getBatchDetailArrayList().size() + pip.getPrimaryInvoiceProductName() + " productName---" +pip.getPrimaryInvoiceProductName() + "BatchControlled---" +pip.getBatchControlled());

                            pip.setError_primaryInvoiceProductName("Please add Batch Details");
                            validCheck = false;
                        } else {
                            pip.setError_primaryInvoiceProductName(null);
                        }
                    }
                    if (!validCheck) {
                        updateProductsInAdapter();
                        Toast.makeText(mContext, "Please add Batch Details", Toast.LENGTH_LONG).show();
                        return;
                    }
                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.stock_receipt_save_confirmation, null);
                    Button saveStockReceiptNo = alertLayout.findViewById(R.id.stockReceiptNoButton),
                            saveStockReceiptYes = alertLayout.findViewById(R.id.stockReceiptYesButton);

                    AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    // disallow cancel of AlertDialog on click of back button and outside touch
                    alert.setCancelable(false);

                    AlertDialog dialog = alert.create();
                    saveStockReceiptYes.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {
                            for (PrimaryInvoiceProduct pip : invoiceProducts) {
                                if (!pip.getPrimaryInvoiceProductCode().equals("-1")) {
                                    try {

                                        String visitSequence = sharedPreferences.getString("VisitSequence", "");
                                       /* int conversion = 1;
                                        if(!TextUtils.isEmpty(pip.getConversion())) {
                                            conversion = Integer.parseInt(pip.getConversion());
                                            conversion = conversion==0?1:conversion;
                                        }
                                        int volume = conversion*Integer.parseInt(pip.getPrimaryInvoiceProductQuantity());*/

                                        long id = dbHandler.createstockreciept(
                                                str_receipt_id,
                                                str_receip_date,
                                                "NGN",
                                                login_id,
                                                distributor_id,
                                                "Stock Receipt",
                                                "warehouse",
                                                pip.getPrimaryInvoiceProductCode(),
                                                pip.getPrimaryInvoiceProductUom(),
                                                pip.getPrimaryInvoiceProductQuantity(),
                                                pip.getPrimaryInvoiceProductPrice(),
                                                pip.getPrimaryInvoiceProductTotalPrice(),
                                                "",
                                                pip.getPrimaryInvoiceNumber(),
                                                "Completed",
                                                random_num,
                                                visitSequence,
                                                receipt_num.getText().toString(),
                                                "");
                                        for (StockReceiptBatch srbd : pip.getBatchDetailArrayList()) {
                                            dbHandler.insertingProductBatch(pip.getPrimaryInvoiceProductCode(), srbd.getBatchNumber(), srbd.getExpiryDate(), srbd.getBatchQuantity(), invoice_id, str_receipt_id, invoice_id, "Completed", "StockReceipt");
                                        }
                                        Log.d("QueryResponse", pip.getPrimaryInvoiceProductName() + " id:" + id);
                                    } catch (Exception e) {
                                        Log.d("QueryResponse", e.getMessage());
                                    }
                                }
                            }
                            Toast.makeText(getContext(), "Stock Receipt Created Successfully", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            getActivity().finish();

                        }

                    });

                    saveStockReceiptNo.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(final View v) {
                            dialog.dismiss();
                        }

                    });
                    dialog.show();

                } else
                    Toast.makeText(getContext(), "There is no products selected", Toast.LENGTH_LONG).show();


            }
        });

        listener = (view, position) -> {
            Intent intent = new Intent(getContext(), StockReceiptBatchActivity.class);
            intent.putExtra("product_id", stockReceiptList.get(position).getProductId());
            intent.putExtra("product_name", stockReceiptList.get(position).getProductName());
            intent.putExtra("product_quantity", stockReceiptList.get(position).getProductQuantity());
            intent.putExtra("product_batch_details", stockReceiptList.get(position).getBatchDetailArrayList());
            Log.d("DataCheck", "Passed Value Count" + stockReceiptList.get(position).getBatchDetailArrayList().size());
            getActivity().startActivityForResult(intent, 1);

        };

        productSpinner.setTitle("Select a Product");
        productSpinner.setPositiveButton("OK");
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
//                productSpinner.isSpinnerDialogOpen = false;

                PrimaryInvoiceProduct p = (PrimaryInvoiceProduct) adapterView.getItemAtPosition(pos);
                product_id = p.getPrimaryInvoiceProductCode();
                if (!product_id.equals("-1")) {
                    stockReceiptList = new ArrayList<StockReceipt>();
                    StockReceipt srwp = new StockReceipt();
                    srwp.setProductId(p.getPrimaryInvoiceProductCode());
                    srwp.setProductName(p.getPrimaryInvoiceProductName());
                    srwp.setProductPrice(p.getPrimaryInvoiceProductPrice());
                    int i = (int) Float.parseFloat(p.getPrimaryInvoiceProductQuantity());
                    srwp.setProductQuantity(String.valueOf(i));
                    srwp.setProductTotalPrice(p.getPrimaryInvoiceProductTotalPrice());
                    srwp.setProductUom(p.getPrimaryInvoiceProductUom());
                    srwp.setBatchControlled(p.getBatchControlled());
                    srwp.setInvoiceCode(p.getPrimaryInvoiceNumber());
                    stockReceiptList.add(srwp);
                    warInvoiceProductAdapter = new StockReceiptWarehouseAdapter(stockReceiptList, listener);
                    warInvoiceProRecyclerView.setAdapter(warInvoiceProductAdapter);
                    warInvoiceProductAdapter.notifyDataSetChanged();
                } else {
                    updateProductsInAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
//                productSpinner.isSpinnerDialogOpen = false;
            }
        });

        //Warehouse Spinner Initialization
        warehouseSpinner.setTitle("Select");
        warehouseArrayList = warehouseTable.getWarehouseListByPrimaryInvoice();
        Warehouse w = new Warehouse();
        w.warehouse_name = "Select";
        w.warehouse_code = "-1";
        warehouseArrayList.add(0, w);
        warehouseSpinnerArrayAdapter = new ArrayAdapter<Warehouse>(mContext, android.R.layout.simple_spinner_item, warehouseArrayList);
        warehouseSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        warehouseSpinner.setAdapter(warehouseSpinnerArrayAdapter);
        warehouseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
//                warehouseSpinner.isSpinnerDialogOpen = false;

                Warehouse w = (Warehouse) adapterView.getItemAtPosition(pos);
                warehouse_id = w.warehouse_code;
                distributor_id = w.warehouse_name;
                try {
                    invoiceArrayList = primaryInvoiceTable.getPrimaryInvoiceByWarehouse(warehouse_id);
                    Log.d("StockReceipt-- ", "InvoiceArrayList-- " + invoiceArrayList.size());
                } catch (Exception e) {
                    Log.d("StockReceipt Error-- ", e.getMessage());
                }
//                invoiceSpinner.setTitle("Select");
                PrimaryInvoice pInvoice = new PrimaryInvoice();
                pInvoice.primaryInvoiceNumber = "Select ";
                Log.d("StockReceipt --", "WarehouseId-- " + warehouse_id);
                invoiceArrayList.add(0, pInvoice);
                invoiceSpinnerArrayAdapter = new ArrayAdapter<PrimaryInvoice>(mContext, android.R.layout.simple_spinner_item, invoiceArrayList);
                invoiceSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                invoiceSpinner.setAdapter(invoiceSpinnerArrayAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
//                warehouseSpinner.isSpinnerDialogOpen = false;
            }
        });

        //Invoice Spinner Initialization
        invoiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
//                invoiceSpinner.isSpinnerDialogOpen = false;

                PrimaryInvoice pi = (PrimaryInvoice) adapterView.getItemAtPosition(pos);
                invoice_id = pi.primaryInvoiceNumber;
                //Product Spinner Initialization
                invoiceProducts = primaryInvoiceProductTable.getPrimaryInvoiceByInvoiceNumber(invoice_id);
                updateProductsInAdapter();
                PrimaryInvoiceProduct p = new PrimaryInvoiceProduct();
                p.setPrimaryInvoiceProductName("All");
                p.setPrimaryInvoiceProductCode("-1");
                invoiceProducts.add(0, p);
                productSpinnerArrayAdapter = new ArrayAdapter<PrimaryInvoiceProduct>(mContext, android.R.layout.simple_spinner_item, invoiceProducts);
                productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);// The drop down view
                productSpinner.setPadding(0, 5, 0, 5);
                productSpinner.setAdapter(productSpinnerArrayAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
//                invoiceSpinner.isSpinnerDialogOpen = false;

            }
        });

        //Invoice Products recycler view
        warInvoiceProRecyclerView.setHasFixedSize(true);
        warInvoiceProductLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        warInvoiceProRecyclerView.setLayoutManager(warInvoiceProductLayoutManager);


        return rootView;
    }

    private void generateStockReceiptNumber() {
        random_num = dbHandler.get_receipt_random_num();

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("ddMMyy");
        Date myDate = new Date();
        String rec_seq_date = timeStampFormat.format(myDate);

        random_num = random_num + 1;

        if (random_num <= 9) {
            receipt_num.setText(tab_prefix + "SR" + rec_seq_date + "00" + random_num);

        } else if (random_num > 9 & random_num < 99) {
            receipt_num.setText(tab_prefix + "SR" + rec_seq_date + "0" + random_num);

        } else {
            receipt_num.setText(tab_prefix + "SR" + rec_seq_date + random_num);
        }

        str_receipt_id = receipt_num.getText().toString();
    }


    public void updateProductsInAdapter() {
        stockReceiptList = new ArrayList<StockReceipt>();
        for (PrimaryInvoiceProduct pip : invoiceProducts) {
            if (!pip.getPrimaryInvoiceProductCode().equals("-1")) {
                StockReceipt srwp = new StockReceipt();
                srwp.setProductId(pip.getPrimaryInvoiceProductCode());
                srwp.setProductName(pip.getPrimaryInvoiceProductName());
                srwp.setProductPrice(pip.getPrimaryInvoiceProductPrice());
                int i = (int) Float.parseFloat(pip.getPrimaryInvoiceProductQuantity());

                srwp.setProductQuantity(String.valueOf(i));
                srwp.setProductTotalPrice(pip.getPrimaryInvoiceProductTotalPrice());
                srwp.setProductUom(pip.getPrimaryInvoiceProductUom());
                srwp.setError_productName(pip.getError_primaryInvoiceProductName());
                srwp.setBatchDetailArrayList(pip.getBatchDetailArrayList());
                srwp.setBatchControlled(pip.getBatchControlled());
                stockReceiptList.add(srwp);
            }
        }
        warInvoiceProductAdapter = new StockReceiptWarehouseAdapter(stockReceiptList, listener);
        warInvoiceProRecyclerView.setAdapter(warInvoiceProductAdapter);
        warInvoiceProductAdapter.notifyDataSetChanged();
    }

    private void setRecieptDateTime() {
        String PATTERN = "dd MMM yy";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        PATTERN = "hh : mm";
        dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String time = dateFormat.format(Calendar.getInstance().getTime());
        //order_date.setText(date + "\n" + time);
        receipt_date.setText(date);

        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        str_receip_date = timeStampFormat.format(myDate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("DataCheck", "Entered Child Fragment" + requestCode);
        //super.onActivityResult(requestCode, resultCode, data); //comment this unless you want to pass your result to the activity.
        if (requestCode == 1) {
            Log.d("DataCheck", "Entered in to Check");
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                String product_id = (String) bundle.get("product_id");
                ArrayList<StockReceiptBatch> product_batch_details = (ArrayList<StockReceiptBatch>) bundle.get("product_batch_details");
                for (PrimaryInvoiceProduct pip : invoiceProducts) {
                    if (product_id.equals(pip.getPrimaryInvoiceProductCode())) {
                        Log.d("DataCheck", "Value Assigned in product");
                        pip.setBatchDetailArrayList(product_batch_details);
                        if (product_batch_details.size() != 0)
                            pip.setError_primaryInvoiceProductName(null);
                    }
                }
                for (StockReceipt srwp : stockReceiptList) {
                    if (product_id.equals(srwp.getProductId())) {
                        Log.d("DataCheck", "Value Assigned in product");
                        srwp.setBatchDetailArrayList(product_batch_details);
                        if (product_batch_details.size() != 0)
                            srwp.setError_productName(null);
                        Log.d("DataCheck", "Values" + srwp.getBatchDetailArrayList().size());
                    }
                }
                warInvoiceProductAdapter = new StockReceiptWarehouseAdapter(stockReceiptList, listener);
                warInvoiceProRecyclerView.setAdapter(warInvoiceProductAdapter);
                warInvoiceProductAdapter.notifyDataSetChanged();
            }
        }
    }
}






