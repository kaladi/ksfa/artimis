package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.model.CustomerStockProduct;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_DETAILS;
import static in.kumanti.emzor.utils.Constants.TABLE_CUSTOMER_STOCK_PRODUCT;
import static in.kumanti.emzor.utils.Constants.TABLE_ITEM_DETAILS;

public class CustomerStockProductTable extends SQLiteOpenHelper {

    //Columns used for Customer Stock Product Information table
    private static final String COLUMN_CI_TAB_ID = "tab_id";
    private static final String COLUMN_CI_STOCK_PRODUCT_ID = "Id";
    private static final String COLUMN_CI_STOCK_PRODUCT_NAME = "stock_product_name";
    private static final String COLUMN_CI_CUSTOMER_ID = "customer_id";
    private static final String COLUMN_CI_SALES_REP_ID = "sales_rep_id";
    private static final String COLUMN_CI_TRANSACTION_TYPE = "transaction_type";
    private static final String COLUMN_CI_TRANSACTION_NUMBER = "transaction_number";
    private static final String COLUMN_CI_TRANSACTION_DATE = "transaction_date";
    private static final String COLUMN_CI_PRODUCT_ID = "product_id";
    private static final String COLUMN_CI_STOCK_AVAILABILITY = "availability";
    private static final String COLUMN_CI_STOCK_VOLUME = "stock_volume";
    private static final String COLUMN_CI_NOTES = "notes";
    private static final String COLUMN_CI_CREATED_AT = "created_at";
    private static final String COLUMN_CI_UPDATED_AT = "updated_at";
    private static final String COLUMN_CI_IS_SYNC = "is_sync";
    private final static String COLUMN_CI_SEQUENCE_NO = "visitSeqNo";

    public Context context;
    private SQLiteDatabase sqlWrite;
    private CompetitorStockProductTable competitorStockProductTable;


    public CustomerStockProductTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER_STOCK_PRODUCT);

        String competitorStockDetailTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOMER_STOCK_PRODUCT + "(" +
                COLUMN_CI_STOCK_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CI_TAB_ID + " TEXT, " +
                COLUMN_CI_SALES_REP_ID + " TEXT, " +
                COLUMN_CI_CUSTOMER_ID + " TEXT, " +
                COLUMN_CI_TRANSACTION_TYPE + " TEXT, " +
                COLUMN_CI_TRANSACTION_NUMBER + " TEXT, " +
                COLUMN_CI_TRANSACTION_DATE + " TEXT, " +
                COLUMN_CI_PRODUCT_ID + " TEXT, " +
                COLUMN_CI_STOCK_PRODUCT_NAME + " TEXT, " +
                COLUMN_CI_STOCK_AVAILABILITY + " BOOLEAN, " +
                COLUMN_CI_STOCK_VOLUME + " TEXT, " +
                COLUMN_CI_NOTES + " TEXT, " +
                COLUMN_CI_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CI_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CI_IS_SYNC + " TEXT, " +
                COLUMN_CI_SEQUENCE_NO + " TEXT " +
                ");";

        sqlWrite.execSQL(competitorStockDetailTable);
        competitorStockProductTable = new CompetitorStockProductTable(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(CustomerStockProduct customerStockProduct) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CI_TAB_ID, Constants.TAB_CODE);
        values.put(COLUMN_CI_SALES_REP_ID, Constants.SR_CODE);

        values.put(COLUMN_CI_CUSTOMER_ID, customerStockProduct.getCustomer_id());

        values.put(COLUMN_CI_TRANSACTION_TYPE, customerStockProduct.getTransactionType());
        values.put(COLUMN_CI_TRANSACTION_NUMBER, customerStockProduct.getTransactionNumber());
        values.put(COLUMN_CI_TRANSACTION_DATE, customerStockProduct.getTransactionDate());

        values.put(COLUMN_CI_PRODUCT_ID, customerStockProduct.productCode);
        values.put(COLUMN_CI_STOCK_PRODUCT_NAME, customerStockProduct.stockProductName);
        values.put(COLUMN_CI_STOCK_AVAILABILITY, customerStockProduct.stockAvailability);
        values.put(COLUMN_CI_STOCK_VOLUME, customerStockProduct.stockVolume);
        values.put(COLUMN_CI_NOTES, customerStockProduct.notes);

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CUSTOMER_STOCK_PRODUCT, null, values);
        db.close();
        return id;
    }



  /*  public CustomerStockProduct getStockProductsById(String orderId, String invoiceId, String productId,String customerId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        Cursor cursor;
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMER_STOCK_PRODUCT + " WHERE "+ COLUMN_CI_ORDER_ID + "=? AND "+  COLUMN_CI_PRODUCT_ID+"=? AND "+COLUMN_CI_CUSTOMER_ID+"=?";
        if(orderId!=null){
            cursor = sqlWrite.rawQuery(selectQuery, new String[] {orderId, productId,customerId} );
        }
        else
        {
            selectQuery = "SELECT * FROM " + TABLE_CUSTOMER_STOCK_PRODUCT + " WHERE "+ COLUMN_CI_INVOICE_ID + "=? AND "+  COLUMN_CI_PRODUCT_ID+"=? AND "+COLUMN_CI_CUSTOMER_ID+"=?";
            cursor = sqlWrite.rawQuery(selectQuery, new String[] {invoiceId, productId,customerId} );
        }
        Log.d("Data","getCompetitorDetailsByProductId  " + productId);
        CustomerStockProduct customerStockProduct = null;
        int i = 0;
        while (cursor.moveToNext()) {
            customerStockProduct = new CustomerStockProduct();
            Log.d("MYDATA","Data  id" + cursor.getInt(0));
            customerStockProduct.setId(cursor.getInt(0));
            customerStockProduct.setTab_id(cursor.getString(1));
            customerStockProduct.setSales_rep_id(cursor.getString(2));
            customerStockProduct.setCustomer_id(cursor.getString(3));

            customerStockProduct.setTransactionType(cursor.getString(3));
            customerStockProduct.setTransactionNumber(cursor.getString(3));
            customerStockProduct.setTransactionDate(cursor.getString(3));


            customerStockProduct.setOrder_id(cursor.getString(4));
            customerStockProduct.setInvoice_id(cursor.getString(5));
            customerStockProduct.setProduct_id(cursor.getString(6));
            customerStockProduct.setStock_product_name(cursor.getString(7));
            customerStockProduct.setStock_availability(cursor.getInt(8) >0);
            customerStockProduct.setStock_volume(cursor.getInt(9));
            customerStockProduct.setNotes(cursor.getString(10));

            i++;
        }
        cursor.close();
        sqlWrite.close();
        return customerStockProduct;
    }
*/

    public CustomerStockProduct getStockProductsByTransactionType(String transactionNumber, String productId, String customerId) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        Cursor cursor;
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMER_STOCK_PRODUCT + " WHERE " + COLUMN_CI_TRANSACTION_NUMBER + "=? AND " + COLUMN_CI_PRODUCT_ID + "=? AND " + COLUMN_CI_CUSTOMER_ID + "=?";
        cursor = sqlWrite.rawQuery(selectQuery, new String[]{transactionNumber, productId, customerId});

        Log.d("Data", "getCompetitorDetailsByProductId  " + productId);
        CustomerStockProduct customerStockProduct = null;
        while (cursor.moveToNext()) {
            customerStockProduct = new CustomerStockProduct();
            Log.d("MYDATA", "Data  id" + cursor.getInt(0));
            customerStockProduct.setId(cursor.getInt(0));
            customerStockProduct.setTab_id(cursor.getString(1));
            customerStockProduct.setSales_rep_id(cursor.getString(2));
            customerStockProduct.setCustomer_id(cursor.getString(3));

            customerStockProduct.setTransactionType(cursor.getString(4));
            customerStockProduct.setTransactionNumber(cursor.getString(5));
            customerStockProduct.setTransactionDate(cursor.getString(6));

            customerStockProduct.setProduct_id(cursor.getString(7));
            customerStockProduct.setStock_product_name(cursor.getString(8));
            customerStockProduct.setStock_availability(cursor.getInt(9) > 0);
            customerStockProduct.setStock_volume(cursor.getInt(10));
            customerStockProduct.setNotes(cursor.getString(11));

        }
        cursor.close();
        sqlWrite.close();
        return customerStockProduct;
    }


    public void update(String id, CustomerStockProduct csd) {
        sqlWrite = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CI_STOCK_AVAILABILITY, csd.stockAvailability);
        values.put(COLUMN_CI_STOCK_VOLUME, String.valueOf(csd.stockVolume));
        values.put(COLUMN_CI_NOTES, csd.notes);
        sqlWrite.update(TABLE_CUSTOMER_STOCK_PRODUCT, values, COLUMN_CI_STOCK_PRODUCT_ID + "=" + id, null);
        sqlWrite.close();
    }


    public ArrayList<CustomerStockProduct> getCompetitorInfoHistory() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT sp.id,sp.tab_id,sp.sales_rep_id,sp.customer_id,sp.transaction_number,sp.product_id,item_det.product_name,sp.availability,sp.stock_volume,sp.notes,cust_det.customer_name,sp.created_at FROM " + TABLE_CUSTOMER_STOCK_PRODUCT + " as sp LEFT JOIN " + TABLE_ITEM_DETAILS + " as item_det ON sp.product_id = item_det.product_id LEFT JOIN " + TABLE_CUSTOMER_DETAILS + " as cust_det ON sp.customer_id = cust_det.customer_id";

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        Log.d("Data", "getCompetitorDetailsByProductId  ");
        ArrayList<CustomerStockProduct> customerStockProductArrayList = new ArrayList<>();

        while (cursor.moveToNext()) {
            CustomerStockProduct customerStockProduct = new CustomerStockProduct();
            customerStockProduct.setId(cursor.getInt(0));
            customerStockProduct.setTab_id(cursor.getString(1));
            customerStockProduct.setSales_rep_id(cursor.getString(2));
            customerStockProduct.setCustomer_id(cursor.getString(3));
            customerStockProduct.setTransactionNumber(cursor.getString(4));
            customerStockProduct.setProduct_id(cursor.getString(5));
            customerStockProduct.setStock_product_name(cursor.getString(6));
            customerStockProduct.setStock_availability(cursor.getInt(7) > 0);
            customerStockProduct.setStock_volume(cursor.getInt(8));
            customerStockProduct.setCompetitorStockProductArrayList(competitorStockProductTable.getCompetitorProductsByStockId(String.valueOf(cursor.getInt(0))));
            customerStockProduct.setNotes(cursor.getString(9));
            customerStockProduct.setCustomer_name(cursor.getString(10));
            String dateTime = cursor.getString(11);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd MMM yy");
            Date myDate = new java.util.Date(Long.parseLong(dateTime) * 1000L);
            String date = dateFormat.format(myDate);

            customerStockProduct.setDate(date);
            customerStockProductArrayList.add(customerStockProduct);
        }
        cursor.close();
        cursor.close();
        sqlWrite.close();
        return customerStockProductArrayList;
    }

    public ArrayList<CustomerStockProduct> getAllCustomerStockProductForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMER_STOCK_PRODUCT;

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});
        ArrayList<CustomerStockProduct> customerStockProductArrayList = new ArrayList<>();

        while (cursor.moveToNext()) {
            CustomerStockProduct customerStockProduct = new CustomerStockProduct();
            customerStockProduct.setId(cursor.getInt(0));
            customerStockProduct.setTab_id(Constants.TAB_CODE);
            customerStockProduct.setSales_rep_id(Constants.SR_CODE);
            customerStockProduct.setCompanyCode(Constants.COMPANY_CODE);
            customerStockProduct.setCustomer_id(cursor.getString(3));
            customerStockProduct.setTransactionType(cursor.getString(4));
            customerStockProduct.setTransactionNumber(cursor.getString(5));
            customerStockProduct.setTransactionDate(cursor.getString(6));
            customerStockProduct.setProduct_id(cursor.getString(7));
            customerStockProduct.setStock_product_name(cursor.getString(8));
            customerStockProduct.setStock_availability(cursor.getInt(9) > 0);
            customerStockProduct.setStock_volume(cursor.getInt(10));
            customerStockProduct.setNotes(cursor.getString(11));
            customerStockProduct.setCreatedAt(cursor.getString(12));
            customerStockProduct.setUpdatedAt(cursor.getString(13));
            customerStockProduct.setVisitSeqNumber(cursor.getString(15));

            customerStockProductArrayList.add(customerStockProduct);
        }
        cursor.close();
        sqlWrite.close();
        return customerStockProductArrayList;
    }


}
