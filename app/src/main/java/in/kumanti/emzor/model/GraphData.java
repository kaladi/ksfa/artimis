package in.kumanti.emzor.model;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class GraphData {
    public ArrayList<BarEntry> targetValues = new ArrayList<>();
    public ArrayList<BarEntry> achievementValues = new ArrayList<>();
    public String title;

    public ArrayList<BarEntry> getTargetValues() {
        return targetValues;
    }

    public void setTargetValues(ArrayList<BarEntry> targetValues) {
        this.targetValues = targetValues;
    }

    public ArrayList<BarEntry> getAchievementValues() {
        return achievementValues;
    }

    public void setAchievementValues(ArrayList<BarEntry> achievementValues) {
        this.achievementValues = achievementValues;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
