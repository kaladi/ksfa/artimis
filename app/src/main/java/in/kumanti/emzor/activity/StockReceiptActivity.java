package in.kumanti.emzor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.kumanti.emzor.R;
import in.kumanti.emzor.fragment.StockReceiptBDEReceiveFragment;
import in.kumanti.emzor.fragment.StockReceiptDistributorFragment;
import in.kumanti.emzor.fragment.StockReceiptWarehouseFragment;


public class StockReceiptActivity extends MainActivity {

    String login_id = "";
    ImageView actionbarBackButton, deviceInfo;
    StockReceiptWarehouseFragment warehouseFragment;
    StockReceiptDistributorFragment distributorFragment;
    StockReceiptBDEReceiveFragment bdeReceiptFragment;
    private TextView actionbarTitle, marqueeText;
    private TabLayout stockReceiptTabLayout;
    private ViewPager stockReceiptPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_stock_receipt);

        initializeViews();
        setFooterDateTime();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViewPager(stockReceiptPager);
        stockReceiptTabLayout.setupWithViewPager(stockReceiptPager);
        actionbarTitle.setText("Stock Receipt");

        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        actionbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });


    }

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private void setupViewPager(ViewPager viewPager) {
        // set StockReceipt WarehouseFragment Arguments
        Bundle bundle = new Bundle();
        bundle.putString("login_id", login_id);

        warehouseFragment = new StockReceiptWarehouseFragment();
        warehouseFragment.setArguments(bundle);

        distributorFragment = new StockReceiptDistributorFragment();
        distributorFragment.setArguments(bundle);

        bdeReceiptFragment = new StockReceiptBDEReceiveFragment();
        bdeReceiptFragment.setArguments(bundle);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(warehouseFragment, "WAREHOUSE");
        adapter.addFragment(distributorFragment, "DISTRIBUTOR");
        adapter.addFragment(bdeReceiptFragment, "BDE RECEIPT");
        viewPager.setAdapter(adapter);
    }

    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        stockReceiptPager = findViewById(R.id.stockReceiptViewpager);
        stockReceiptTabLayout = findViewById(R.id.stockReceiptTabs);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        Bundle bundle = getIntent().getExtras();
        login_id = bundle.getString("login_id");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("DataCheck", "Entered " + requestCode);
        if (requestCode == 1)
            warehouseFragment.onActivityResult(requestCode, resultCode, data);
        else if (requestCode == 2)
            distributorFragment.onActivityResult(requestCode, resultCode, data);

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
