package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.DoctorVisitInfo;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_DOCTOR_VISIT_INFO;

public class DoctorVisitInfoTable extends SQLiteOpenHelper {

    //Columns used for Pharmacy Information details based upon the type of pharmacy
    private static final String COLUMN_DOCTOR_VISIT_INFO_ID = "id";
    private static final String COLUMN_SR_CODE = "sr_code";
    private static final String COLUMN_DOCTOR_CODE = "doctor_code";
    private static final String COLUMN_DOCTOR_VISIT_SEQUENCE_NO = "visit_sequence_no";
    private static final String COLUMN_DOCTOR_VISIT_DATE = "visit_date";
    private static final String COLUMN_DOCTOR_VISIT_AVG_PATIENTS_VISITS = "avg_patients_visits";
    private static final String COLUMN_DOCTOR_VISIT_AVG_PRESCRIPTIONS_ISSUED = "avg_presc_issued";
    private static final String COLUMN_DOCTOR_VISIT_CREATED_AT = "created_at";
    private static final String COLUMN_DOCTOR_VISIT_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public DoctorVisitInfoTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCTOR_VISIT_INFO);

        String productTable = "CREATE TABLE IF NOT EXISTS " + TABLE_DOCTOR_VISIT_INFO + "(" +
                COLUMN_DOCTOR_VISIT_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SR_CODE + " TEXT, " +
                COLUMN_DOCTOR_CODE + " TEXT, " +
                COLUMN_DOCTOR_VISIT_SEQUENCE_NO + " TEXT, " +
                COLUMN_DOCTOR_VISIT_DATE + " TEXT, " +
                COLUMN_DOCTOR_VISIT_AVG_PATIENTS_VISITS + " TEXT, " +
                COLUMN_DOCTOR_VISIT_AVG_PRESCRIPTIONS_ISSUED + " TEXT, " +
                COLUMN_DOCTOR_VISIT_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_DOCTOR_VISIT_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";

        sqlWrite.execSQL(productTable);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long create(DoctorVisitInfo doctorVisitInfo) {
        boolean isExsit = false;
        long id = 0;
        DoctorVisitInfo dvi = getDoctorVisitByCode(doctorVisitInfo.getDoctorCode());
        if (dvi != null) {
            id = dvi.getId();
            isExsit = true;
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_SR_CODE, doctorVisitInfo.getSrCode());
        values.put(COLUMN_DOCTOR_CODE, doctorVisitInfo.getDoctorCode());
        values.put(COLUMN_DOCTOR_VISIT_SEQUENCE_NO, doctorVisitInfo.getVisitSeqNo());
        values.put(COLUMN_DOCTOR_VISIT_DATE, doctorVisitInfo.getVisitDate());
        values.put(COLUMN_DOCTOR_VISIT_AVG_PATIENTS_VISITS, doctorVisitInfo.getAvgVisitPatients());
        values.put(COLUMN_DOCTOR_VISIT_AVG_PRESCRIPTIONS_ISSUED, doctorVisitInfo.getAvgPrescriptionsIssued());
        SQLiteDatabase db = getWritableDatabase();
        if (isExsit) {
            db.update(TABLE_DOCTOR_VISIT_INFO, values,
                    COLUMN_DOCTOR_CODE + " = ?",
                    new String[]{doctorVisitInfo.getDoctorCode()});
            db.close();
        } else {
            id = db.insert(TABLE_DOCTOR_VISIT_INFO, null, values);
            db.close();
        }
        return id;
    }

    public DoctorVisitInfo getDoctorVisitByCode(String doctorCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DOCTOR_VISIT_INFO + " WHERE " + COLUMN_DOCTOR_CODE + "=? ORDER BY id asc";

        @SuppressLint("Recycle") Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{doctorCode});
        Log.d("Data", "Get Products  ");

        DoctorVisitInfo doctorVisitInfo = null;
        while (cursor.moveToNext()) {
            doctorVisitInfo = new DoctorVisitInfo();
            doctorVisitInfo.setId(cursor.getInt(0));
            doctorVisitInfo.setSrCode(cursor.getString(1));
            doctorVisitInfo.setDoctorCode(cursor.getString(2));
            doctorVisitInfo.setVisitSeqNo(cursor.getString(3));
            doctorVisitInfo.setVisitDate(cursor.getString(4));
            doctorVisitInfo.setAvgVisitPatients(cursor.getString(5));
            doctorVisitInfo.setAvgPrescriptionsIssued(cursor.getString(6));
        }
        return doctorVisitInfo;
    }

    public ArrayList<DoctorVisitInfo> getDoctorVisitInfo() {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_DOCTOR_VISIT_INFO;

        ArrayList<DoctorVisitInfo> doctorVisitInfoArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            DoctorVisitInfo doctorVisitInfo = new DoctorVisitInfo();
            doctorVisitInfo.setTabCode(Constants.TAB_CODE);
            doctorVisitInfo.setCompanyCode(Constants.COMPANY_CODE);
            doctorVisitInfo.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            doctorVisitInfo.setId(id);
            doctorVisitInfo.setDoctorCode(cursor.getString(2));
            doctorVisitInfo.setAvgVisitPatients(cursor.getString(5));
            doctorVisitInfo.setAvgPrescriptionsIssued(cursor.getString(6));
            doctorVisitInfo.setVisitDate(cursor.getString(4));
            doctorVisitInfo.setVisitSeqNo(cursor.getString(3));
            doctorVisitInfo.setCreatedAt(cursor.getString(7));
            doctorVisitInfo.setUpdatedAt(cursor.getString(8));
            doctorVisitInfoArrayList.add(doctorVisitInfo);
        }
        cursor.close();
        sqlWrite.close();
        return doctorVisitInfoArrayList;
    }

    /*public ArrayList<DoctorVisitInfo> getDoctorInfo(String pharmacyType, String customerCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DOCTOR_VISIT_INFO +" WHERE "+COLUMN_DOCTOR_VISIT_SEQUENCE_NO+"=? AND "+COLUMN_DOCTOR_CODE+"=? ORDER BY id asc" ;
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[] {pharmacyType, customerCode} );
        Log.d("Data","Get Products  " );

        ArrayList<DoctorVisitInfo> doctorVisitInfoArrayList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DoctorVisitInfo product = new DoctorVisitInfo();
            Log.d("PPPPP","Data  id" + cursor.getInt(0));
            product.setId(cursor.getInt(0));
            product.setSrCode(cursor.getString(1));
            product.setDoctorCode(cursor.getString(2));
            product.setVisitSeqNo(cursor.getString(3));
            product.setVisitDate(cursor.getString(4));
            product.setAvgVisitPatients(cursor.getString(5));
            product.setAvgPrescriptionsIssued(cursor.getString(6));
            doctorVisitInfoArrayList.add(product);
        }
        cursor.close();
        sqlWrite.close();
        return doctorVisitInfoArrayList;
    }*/


}
