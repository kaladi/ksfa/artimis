package in.kumanti.emzor.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.utils.SharedPreferenceManager;


public class
UserSetupActivity extends MainActivity implements View.OnClickListener {

    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;
    private static final String[] paths = {"Select Role", "Admin", "User"};
    MyDBHandler dbHandler;
    EditText password, confirm_password;
    ImageView btn_back;
    Button confirm_btn, uploadCSVBtn, stockAdjustmentButton;
    TextView browse_logo, email_id, mobile_number;
    Bitmap thumbnail;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    TextView emp_id;
    TextView first_name, last_name;
    String employee_fname, employee_id, login_signup, employee_lname;
    String role_id, role_name;
    DrawerLayout drawer;
    ImageView info;
    TextView actionbarTitle;
    SharedPreferenceManager sharedPreferenceManager;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    ImageView bdeImageView;
    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    private boolean hasImageChanged = false;
    private Spinner spinner;
    private TextView marqueeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_user_setup);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionbarTitle.setText("User Setup");
        marqueeText.setSelected(true);
        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
        }


        if ("true".equals(login_signup)) {
            //if(login_signup.equals("true")){
            toolbar.setNavigationIcon(null);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        } else {

        }

        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserSetupActivity.this,
                android.R.layout.simple_spinner_item, paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        getUserDetails();

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });


        confirm_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean cancel = false;
                View focusView = null;


                String password1 = password.getText().toString();
                String confirm_password1 = confirm_password.getText().toString();
                String mobile_number1 = mobile_number.getText().toString();

                if (TextUtils.isEmpty(password1)) {
                    password.setError(getString(R.string.error_field_required));
                    focusView = password;
                    cancel = true;
                } else if (password1.length() < 6) {
                    password.setError("Password should contains min 6 character");
                } else if (TextUtils.isEmpty(confirm_password1)) {
                    confirm_password.setError(getString(R.string.error_field_required));
                    focusView = confirm_password;
                    cancel = true;
                } else if (password1.length() < 6) {
                    password.setError("Password should contains min 6 character");
                } else if (!password1.equals(confirm_password1)) {
                    confirm_password.setError("Password does not match");
                } else if (TextUtils.isEmpty(mobile_number1)) {
                    mobile_number.setError(getString(R.string.error_field_required));
                    focusView = mobile_number;
                    cancel = true;
                } else if (mobile_number1.length() < 10) {
                    password.setError("Mobile Number should contains min 10 character");
                } else {
                    update_user_records();

                }

            }
        });


        uploadCSVBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    uploadCSVBtn.setEnabled(false);
                    dbHandler.uploadOpenStock(emp_id.getText().toString());
                    dbHandler.uploadOpenStockBatchProducts(emp_id.getText().toString());
                    Toast.makeText(getApplicationContext(), "Uploaded Successfully", Toast.LENGTH_SHORT).show();
                    editor = pref.edit();
                    editor.putBoolean("openStock", true); // Storing boolean - true/false
                    editor.commit();
                    uploadCSVBtn.setVisibility(View.INVISIBLE);

                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "Unable to Read File", Toast.LENGTH_SHORT).show();
                }
            }
        });


        stockAdjustmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dbHandler.uploadReconciliationProducts(emp_id.getText().toString());
                    dbHandler.uploadReconciliationBatchProducts(emp_id.getText().toString());
                    Toast.makeText(getApplicationContext(), "Uploaded Successfully", Toast.LENGTH_SHORT).show();


                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "Unable to Read File", Toast.LENGTH_SHORT).show();
                }
            }
        });


        // browse_logo.setOnClickListener(this);

     /*   if (ContextCompat.checkSelfPermission(UserSetupActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            bdeImageView.setEnabled(false);
            ActivityCompat.requestPermissions(UserSetupActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            bdeImageView.setEnabled(true);
        }*/

    }

    private void getUserDetails() {

        Cursor userDeatils = dbHandler.getUserDeatils();
        int count1 = userDeatils.getCount();
        if (count1 == 0) {
        } else {
            int i = 0;
            while (userDeatils.moveToNext()) {

                first_name.setText(userDeatils.getString(0));
                last_name.setText(userDeatils.getString(1));
                email_id.setText(userDeatils.getString(2));
                mobile_number.setText(userDeatils.getString(3));
                emp_id.setText(userDeatils.getString(4));
                password.setText(userDeatils.getString(5));
                confirm_password.setText(userDeatils.getString(6));


                Bitmap bdeImageBitmap = dbHandler.getBDELogoImage(emp_id.getText().toString());
                if (bdeImageBitmap != null) {
                    bdeImageView.setImageBitmap(bdeImageBitmap);
                }

            }
        }
    }

    private void initializeViews() {
        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);

        bdeImageView = findViewById(R.id.bdeImageView);

        email_id = findViewById(R.id.email_id);
        password = findViewById(R.id.password);
        confirm_password = findViewById(R.id.confirm_password);
        mobile_number = findViewById(R.id.mobile_number);
        emp_id = findViewById(R.id.employee_id);
        // browse_logo = (TextView) findViewById(R.id.browse_logo);
        btn_back = findViewById(R.id.back);
        actionbarTitle = findViewById(R.id.toolbar_title);

        // Load Products
        info = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);
        confirm_btn = findViewById(R.id.confirm_btn);
        uploadCSVBtn = findViewById(R.id.uploadCSVBtn);
        stockAdjustmentButton = findViewById(R.id.stockAdjustmentButton);

        uploadCSVBtn.setVisibility(View.INVISIBLE);
        pref = getApplicationContext().getSharedPreferences("mSalesStock", 0); // 0 - for private mode

    }

    private void populateHeaderDetails() {
        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //Extract the data…
            login_signup = bundle.getString("login_signup");
        }

    }

    public void update_user_records() {


        String password1 = password.getText().toString();
        String confirm_password1 = confirm_password.getText().toString();
        String mobile_number1 = mobile_number.getText().toString();

        dbHandler.update_user_setup(password1, confirm_password1, mobile_number1);
        Toast.makeText(getApplicationContext(), "User Record Updated Succesfully", Toast.LENGTH_SHORT).show();
        if (!pref.getBoolean("openStock", false))
            uploadCSVBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            /*case R.id.browse_logo:
                new MaterialDialog.Builder(this)
                        .title("Set your image")
                        .items(R.array.uploadImages)
                        .itemsIds(R.array.itemIds)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                switch (which){
                                    case 0:
                                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                        photoPickerIntent.setType("image/*");
                                        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                                        break;
                                    case 1:
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        startActivityForResult(intent, CAPTURE_PHOTO);
                                        break;
                                    case 2:
                                        company_logo.setImageResource(R.drawable.ic_account_circle_black);
                                        break;
                                }
                            }
                        })
                        .show();
                break;
*/

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // bdeImageView.setEnabled(true);
            }
        }
    }

    public void setProgressBar() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Please wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressBarStatus < 100) {
                    progressBarStatus += 30;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    progressBarbHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }
                if (progressBarStatus >= 100) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progressBar.dismiss();
                }

            }
        }).start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    //set Progress Bar
                    setProgressBar();
                    //set profile picture form gallery
                    //  bdeImageView.setImageBitmap(selectedImage);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAPTURE_PHOTO) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }
    }


    private void onCaptureImageResult(Intent data) {

        thumbnail = (Bitmap) data.getExtras().get("data");
        //set Progress Bar
        setProgressBar();
        //set profile picture form camera
        //  bdeImageView.setMaxWidth(200);
        // bdeImageView.setImageBitmap(thumbnail);

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

}
