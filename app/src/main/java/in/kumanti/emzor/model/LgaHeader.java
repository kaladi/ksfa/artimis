package in.kumanti.emzor.model;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class LgaHeader {

    public String lga;
    public String lga_id;


    public String getLga() {
        return lga;
    }

    public void setLga(String lga) {
        this.lga = lga;
    }

    public String getLga_id() {
        return lga_id;
    }

    public void setLga_id(String lga_id) {
        this.lga_id = lga_id;
    }

    @Override
    public String toString() {
        return this.lga;
    }
}

