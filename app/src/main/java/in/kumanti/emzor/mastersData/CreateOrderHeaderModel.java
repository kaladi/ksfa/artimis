package in.kumanti.emzor.mastersData;

/**
 * Created by Anbarivu.mi on 1/19/2018.
 */

public class CreateOrderHeaderModel {

    private String company_code;
    private String tab_code;
    private String salesrep_id;
    private String customer_id;
    private String order_id;
    private String order_num;
    private String date;
    private String value;
    private String line_items;
    private String lpo_number;
    private String remarks;
    private String delivery_date;
    private String ship_address1;
    private String ship_address2;
    private String product_id;
    private String product_name;
    private String product_uom;
    private String quantity;
    private String price;
    private String line_value;


    public CreateOrderHeaderModel(String company_code1,
                                  String tab_code1,
                                  String salesrep_id1,
                                  String customer_id1,
                                  String order_id1,
                                  String order_num1,
                                  String date1,
                                  String value1,
                                  String line_items1,
                                  String lpo_number1,
                                  String remarks1,
                                  String delivery_date1,
                                  String ship_address11,
                                  String ship_address21,
                                  String prod_id1,
                                  String product_name1,
                                  String product_uom1,
                                  String quantity1,
                                  String price1,
                                  String line_value1
    ) {

        date = date1;
        order_num = order_num1;
        line_items = line_items1;
        value = value1;
        order_id = order_id1;
        customer_id = customer_id1;
        salesrep_id = salesrep_id1;
        lpo_number = lpo_number1;
        remarks = remarks1;
        delivery_date = delivery_date1;
        ship_address1 = ship_address11;
        ship_address2 = ship_address21;
        product_id = prod_id1;
        product_name = product_name1;
        product_uom = product_uom1;
        quantity = quantity1;
        price = price1;
        line_value = line_value1;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code1) {
        company_code = company_code1;
    }

    public String getTab_code() {
        return tab_code;
    }

    public void setTab_code(String tab_code1) {
        tab_code = tab_code1;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date1) {
        date = date1;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num1) {
        order_num = order_num1;
    }

    public String getLine_items() {
        return line_items;
    }

    public void setLine_items(String line_items1) {
        line_items = line_items1;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value1) {
        value = value1;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id1) {
        order_id = order_id1;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id1) {
        customer_id = customer_id1;
    }

    public String getSalesrep_id() {
        return salesrep_id;
    }

    public void setSalesrep_id(String salesrep_id1) {
        customer_id = salesrep_id1;
    }

    public String getLpo_number() {
        return lpo_number;
    }

    public void setLpo_number(String lpo_number1) {
        lpo_number = lpo_number1;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks1) {
        remarks = remarks1;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date1) {
        delivery_date = delivery_date1;
    }

    public String getShip_address1() {
        return ship_address1;
    }

    public void setShip_address1(String ship_address11) {
        ship_address1 = ship_address11;
    }

    public String getShip_address2() {
        return ship_address2;
    }

    public void setShip_address2(String ship_address21) {
        ship_address2 = ship_address21;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id1) {
        product_id = product_id1;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name1) {
        product_name = product_name1;
    }

    public String getProduct_uom() {
        return product_uom;
    }

    public void setProduct_uom(String product_uom1) {
        product_uom = product_uom1;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity1) {
        quantity = quantity1;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price1) {
        price = price1;
    }

    public String getLine_value() {
        return line_value;
    }

    public void setLine_value(String line_value1) {
        line_value = line_value1;
    }


}

