package in.kumanti.emzor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import in.kumanti.emzor.R;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.utils.Globals;


public class ChangePasswordActivity extends MainActivity {

    String login_id = "";
    EditText old_password, new_password, confirm_password;
    MyDBHandler dbHandler;
    ImageView confirm_btn;
    ImageView info, btn_back;
    ImageView actionbarBackButton, deviceInfo;
    Globals globals;
    private TextView marqueeText, actionBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_change_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        marqueeText.setSelected(true);
        actionBarTitle.setText("Customer Report");

        // Load Products

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(2).setChecked(true);

        final String old_pwd = dbHandler.getOldPassword(login_id);


        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

            }
        });

        old_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    final String old_password1 = old_password.getText().toString().trim();
                    final String new_password1 = new_password.getText().toString().trim();
                    final String confirm_password1 = confirm_password.getText().toString().trim();

                    if (!old_pwd.equals(old_password1)) {
                        Toast.makeText(getApplicationContext(), "Old Password is Invalid", Toast.LENGTH_LONG).show();
                    } else if (!new_password1.equals(confirm_password1)) {
                        Toast.makeText(getApplicationContext(), "Password is not Matched", Toast.LENGTH_LONG).show();
                    }

                }

            }
        });


        confirm_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String old_password1 = old_password.getText().toString().trim();
                final String new_password1 = new_password.getText().toString().trim();
                final String confirm_password1 = confirm_password.getText().toString().trim();
                View focusView = null;
                boolean cancel = false;

                if (TextUtils.isEmpty(old_password1)) {
                    old_password.setError(getString(R.string.error_field_required));
                    focusView = old_password;
                    cancel = true;
                } else if (TextUtils.isEmpty(new_password1)) {
                    new_password.setError(getString(R.string.error_field_required));
                    focusView = new_password;
                    cancel = true;
                } else if (TextUtils.isEmpty(confirm_password1)) {
                    confirm_password.setError(getString(R.string.error_field_required));
                    focusView = confirm_password;
                    cancel = true;
                }

                if (cancel) {
                    focusView.requestFocus();
                } else if (!old_pwd.equals(old_password1)) {
                    Toast.makeText(getApplicationContext(), "Old Password is Invalid", Toast.LENGTH_LONG).show();
                } else if (!new_password1.equals(confirm_password1)) {
                    Toast.makeText(getApplicationContext(), "Password is not Matched", Toast.LENGTH_LONG).show();
                } else {

                    dbHandler.updatePassword(login_id, confirm_password1);
                    Toast.makeText(getApplicationContext(), "Password Changed Successfully", Toast.LENGTH_LONG).show();
                }

            }

        });


    }

    private void populateHeaderDetails() {

    }

    private void initializeViews() {
        globals = ((Globals) getApplicationContext());
        login_id = globals.getLogin_id();
        dbHandler = new MyDBHandler(this, null, null, 1);
        info = findViewById(R.id.img_info);
        marqueeText = findViewById(R.id.marquee);

        old_password = findViewById(R.id.old_password);
        new_password = findViewById(R.id.new_password);
        confirm_password = findViewById(R.id.confirm_password);

        confirm_btn = findViewById(R.id.confirm_btn);
        btn_back = findViewById(R.id.btn_back);


    }


    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();


    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

}
