package in.kumanti.emzor.model;

public class ResponseJson {

    public String msg;
    public String status;


    public ResponseJson(String msg, String status) {

        this.msg = msg;
        this.status = status;

    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
