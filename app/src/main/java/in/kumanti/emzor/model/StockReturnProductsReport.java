package in.kumanti.emzor.model;

public class StockReturnProductsReport {

    public int stockReturnProdId;
    public String stockReturnDate;
    public String stockReturnNumber;
    public String stockReturnType;
    public String productCode;
    public String productName;
    public String returnQuantity;
    public String returnPrice;
    public String returnValue;
    public String createdAt;
    public String updatedAt;


    public int getStockReturnProdId() {
        return stockReturnProdId;
    }

    public void setStockReturnProdId(int stockReturnProdId) {
        this.stockReturnProdId = stockReturnProdId;
    }

    public String getStockReturnDate() {
        return stockReturnDate;
    }

    public void setStockReturnDate(String stockReturnDate) {
        this.stockReturnDate = stockReturnDate;
    }

    public String getStockReturnNumber() {
        return stockReturnNumber;
    }

    public void setStockReturnNumber(String stockReturnNumber) {
        this.stockReturnNumber = stockReturnNumber;
    }

    public String getStockReturnType() {
        return stockReturnType;
    }

    public void setStockReturnType(String stockReturnType) {
        this.stockReturnType = stockReturnType;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
