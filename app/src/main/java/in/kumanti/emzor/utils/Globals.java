package in.kumanti.emzor.utils;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

public class Globals extends Application {

    /**
     * Keeps a reference of the application context
     */
    private static Context sContext;
    // Global variable
    private String trip_num;
    private String login_id;
    private String login_email;
    private String mileage = "";
    private Bitmap sr_image;
    private String sr_name;
    private String customer_type;
    private String user_role;
    private String trip_name;
    private String customer_id;

    /**
     * Returns the application context
     *
     * @return application context
     */
    public static Context getContext() {
        return sContext;
    }

    public String getTrip_num() {
        return trip_num;
    }

    public void setTrip_num(String trip_num1) {
        this.trip_num = trip_num1;
    }

    public String getLogin_id() {
        return login_id;
    }

    public void setLogin_id(String login_id1) {
        this.login_id = login_id1;
    }

    public String getLogin_email() {
        return login_email;
    }

    public void setLogin_email(String login_email1) {
        this.login_email = login_email1;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage1) {
        this.mileage = mileage1;
    }

    public String getSr_name() {
        return sr_name;
    }

    public void setSr_name(String sr_name1) {
        this.sr_name = sr_name1;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(String customer_type1) {
        this.customer_type = customer_type1;
    }

    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    public String getTrip_name() {
        return trip_name;
    }

    public void setTrip_name(String trip_name1) {
        this.trip_name = trip_name1;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String trip_name1) {
        this.customer_id = trip_name1;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();

    }

}

