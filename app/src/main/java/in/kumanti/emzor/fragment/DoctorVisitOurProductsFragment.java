package in.kumanti.emzor.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.StockReceiptBatchActivity;
import in.kumanti.emzor.adapter.DoctorVisitOurProdAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.eloquent.DoctorPrescribedProductsTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.model.DoctorPrescribedProducts;
import in.kumanti.emzor.model.Product;
import in.kumanti.emzor.utils.CustomSearchableSpinner;


public class DoctorVisitOurProductsFragment extends Fragment {

    public RecyclerView presOurProdRecyclerView;
    CustomSearchableSpinner productSpinner;
    ArrayAdapter<Product> productSpinnerArrayAdapter;
    ArrayList<Product> productArrayList;
    ArrayList<Product> newProductList;
    ProductTable productTable;
    ArrayList<DoctorPrescribedProducts> docVisitOurProdArrayList = new ArrayList<DoctorPrescribedProducts>();
    Context mContext;
    MyDBHandler dbHandler;
    String login_id = "", product_id = null, customer_id = "",visit_sequence="", product_type = "OurProduct";
    Button addRow;
    RecyclerViewItemListener listener, listener1;
    DoctorPrescribedProductsTable doctorPrescribedProductsTable;
    private RecyclerView.Adapter presOurProdAdapter;
    private RecyclerView.LayoutManager presOurProdLayoutManager;

    public SharedPreferences sharedPreferences;
    public DoctorVisitOurProductsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_doctor_visit_our_products, container, false);
        mContext = rootView.getContext();
        dbHandler = new MyDBHandler(mContext, null, null, 1);
        doctorPrescribedProductsTable = new DoctorPrescribedProductsTable(mContext);
        if (getArguments() != null) {
            login_id = getArguments().getString("login_id");
            customer_id = getArguments().getString("customer_id");
            visit_sequence = getArguments().getString("visit_sequence");
        }

        productTable = new ProductTable(mContext);

        productSpinner = rootView.findViewById(R.id.doctorVisitOurProdSpinner);
        presOurProdRecyclerView = rootView.findViewById(R.id.docVisitOurProdRecyclerView);

        addRow = rootView.findViewById(R.id.ourProdAddRowButton);


        //Product Spinner Initialization
        updateProductSpinner();
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pos, long id) {
                productSpinner.isSpinnerDialogOpen = false;
                Product p = (Product) adapterView.getItemAtPosition(pos);
                product_id = p.product_id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
                productSpinner.isSpinnerDialogOpen = false;
            }
        });


        presOurProdRecyclerView.setHasFixedSize(true);
        presOurProdLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        presOurProdRecyclerView.setLayoutManager(presOurProdLayoutManager);

        listener = (view, position) -> {
            updateProductSpinner();
        };
        listener1 = (view, position) -> {

            Intent intent = new Intent(getContext(), StockReceiptBatchActivity.class);
            intent.putExtra("product_id", docVisitOurProdArrayList.get(position).getProductCode());
            intent.putExtra("product_name", docVisitOurProdArrayList.get(position).getProductName());
            getActivity().startActivityForResult(intent, 2);
        };
        docVisitOurProdArrayList = doctorPrescribedProductsTable.getPharmacyInfoProducts(customer_id, product_type);
        presOurProdAdapter = new DoctorVisitOurProdAdapter(docVisitOurProdArrayList, listener, listener1);
        presOurProdRecyclerView.setAdapter(presOurProdAdapter);

        //Generate new row or item in opportunity details Recycler view
        addRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPrescribedProducts();
            }
        });


        return rootView;
    }


    //Saving the Distributor Stock Receipt
    private void saveStockReceipt() {


    }

    /**
     * Update the Product Spinner, based upon the addition and deletion of the products in Opportunity Product Details RecyclerView List
     */
    private void updateProductSpinner() {
        //To Set the Values to the Product Spinner
        productArrayList = productTable.getProducts();
        //Remove the already selected product
        int k = 0;
        newProductList = new ArrayList<Product>();
        for (Product product : productArrayList) {
            boolean flag = true;
            for (DoctorPrescribedProducts srd : docVisitOurProdArrayList) {
                if (product.product_id.equals(srd.getProductCode()))
                    flag = false;
            }
            if (flag)
                newProductList.add(product);
            k++;
        }

        Product p = new Product();
        p.setProduct_id("-1");
        p.setProduct_name("Select");
        newProductList.add(0, p);

        productSpinner.setTitle("Select a Product");
        productSpinner.setPositiveButton("OK");

        productSpinnerArrayAdapter = new ArrayAdapter<Product>(mContext, android.R.layout.simple_spinner_item, newProductList);
        productSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        productSpinner.setAdapter(productSpinnerArrayAdapter);
    }


    /**
     * Adding of Products Details in Opportunity Recycler View List
     */
    private void addPrescribedProducts() {
        if (productSpinner.getSelectedItemPosition() == 0) {
            Toast.makeText(mContext, "Please choose a product before adding to the Product List", Toast.LENGTH_SHORT).show();
            return;
        }
//        SharedPreferences preferences = this.getActivity().getSharedPreferences("VisitSequence",     Context.MODE_PRIVATE);
//        String visitSequence = preferences.getString("VisitSequence", "");
//        String visitSequence = this.getActivity().sharedPreferences.getString("VisitSequence", "");

        Product selectedProduct = newProductList.get(productSpinner.getSelectedItemPosition());
        DoctorPrescribedProducts srd = new DoctorPrescribedProducts();
        srd.setProductName(selectedProduct.product_name);
        srd.setProductCode(selectedProduct.product_code);
        srd.setDoctorCode(customer_id);
        srd.setProductType(product_type);
        srd.setVisitSeqNo(visit_sequence);
        System.out.println("visit_sequence = " + visit_sequence);
        docVisitOurProdArrayList.add(srd);
        presOurProdAdapter.notifyDataSetChanged();
        Log.d("Recycle", "Data Added: " + selectedProduct.product_name);
        updateProductSpinner();
    }

    public boolean savePresciptionProducts() {
        if (docVisitOurProdArrayList.size() == 0) {
            // Toast.makeText(mContext,"Please add Prescription Products",Toast.LENGTH_SHORT).show();
            return false;
        }
        boolean validateCheck = false;

        for (DoctorPrescribedProducts dpp : docVisitOurProdArrayList) {
            if (TextUtils.isEmpty(dpp.getPrescriptionsCount())) {
                //dpp.setErrorPrescriptionsCount("Please enter prescription count");
            } else{
                dpp.setErrorPrescriptionsCount(null);
                validateCheck = true;
            }

        }
        if (!validateCheck) {
            presOurProdAdapter.notifyDataSetChanged();
            return false;
        }
        doctorPrescribedProductsTable.deletePrescriptionProduct(customer_id, product_type);
        for (DoctorPrescribedProducts dpp : docVisitOurProdArrayList) {
            doctorPrescribedProductsTable.create(dpp);
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}






