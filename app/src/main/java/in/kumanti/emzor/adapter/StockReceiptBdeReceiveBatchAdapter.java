package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.BdeReceiveBatchProducts;

public class StockReceiptBdeReceiveBatchAdapter extends RecyclerView.Adapter<StockReceiptBdeReceiveBatchAdapter.ViewHolder> {

    public ArrayList<BdeReceiveBatchProducts> mDataset;
    public Context context;
    private RecyclerViewItemListener mListener;
    private int mYear, mMonth, mDay;

    public StockReceiptBdeReceiveBatchAdapter(ArrayList<BdeReceiveBatchProducts> myDataset, RecyclerViewItemListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    public void add(BdeReceiveBatchProducts item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(BdeReceiveBatchProducts item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockReceiptBdeReceiveBatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_receipt_bde_receive_batch, parent, false);


        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        try {
            String PATTERN = "dd MMM YY";
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(mDataset.get(position).getExpiryDate());
            dateFormat.applyPattern(PATTERN);
            viewHolder.expiryDate.setText(dateFormat.format(d));
        } catch (Exception e) {

        }

        viewHolder.expiryDate.setText(mDataset.get(position).getExpiryDate());
        viewHolder.batchNumber.setText(mDataset.get(position).getBatchNumber());
        viewHolder.batchQuantity.setText(String.valueOf(Integer.parseInt(mDataset.get(position).getIssueQuantity())));


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView expiryDate, batchNumber, batchQuantity;
        private RecyclerViewItemListener mListener;

        public ViewHolder(View v, RecyclerViewItemListener listener) {
            super(v);
            mListener = listener;

            expiryDate = v.findViewById(R.id.stockIssueExpiryDateTv);
            batchNumber = v.findViewById(R.id.stockIssueBatchNameTv);
            batchQuantity = v.findViewById(R.id.stockIssueBatchStockQtyTv);

        }
    }


}