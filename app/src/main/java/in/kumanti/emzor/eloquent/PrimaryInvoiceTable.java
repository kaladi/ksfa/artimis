package in.kumanti.emzor.eloquent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import in.kumanti.emzor.model.PrimaryInvoice;

import static in.kumanti.emzor.eloquent.MyDBHandler.TABLE_STOCK_RECIEPT;
import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_PRIMARY_INVOICE;

public class PrimaryInvoiceTable extends SQLiteOpenHelper {

    //Columns used for Primary Invoice details for a selected warehouse
    private static final String COLUMN_PRIMARY_INVOICE_ID = "id";
    private static final String COLUMN_WAREHOUSE_ID = "warehouse_id";
    private static final String COLUMN_PRIMARY_INVOICE_NUMBER = "primary_invoice_number";
    private static final String COLUMN_PRIMARY_INVOICE_CREATED_AT = "created_at";
    private static final String COLUMN_PRIMARY_INVOICE_UPDATED_AT = "updated_at";

    public Context context;
    private SQLiteDatabase sqlWrite;

    public PrimaryInvoiceTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        sqlWrite = this.getWritableDatabase();
        // sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_PRIMARY_INVOICE);

        String surveyTable = "CREATE TABLE IF NOT EXISTS " + TABLE_PRIMARY_INVOICE + "(" +
                COLUMN_PRIMARY_INVOICE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_WAREHOUSE_ID + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_NUMBER + " TEXT, " +
                COLUMN_PRIMARY_INVOICE_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_PRIMARY_INVOICE_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')) " +
                ");";
        sqlWrite.execSQL(surveyTable);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public ArrayList<PrimaryInvoice> getPrimaryInvoiceByWarehouse(String warehouse_id) {
        sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRIMARY_INVOICE + " WHERE " + COLUMN_WAREHOUSE_ID + "=? AND " + COLUMN_PRIMARY_INVOICE_NUMBER + " NOT IN (select invoice_id from " + TABLE_STOCK_RECIEPT + " where invoice_id!='' AND invoice_id NOT NULL) GROUP BY " + COLUMN_PRIMARY_INVOICE_NUMBER;

        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{warehouse_id});
        Log.d("StockReceipt --Db", "Get PrimaryInvoice Lists  " + cursor.toString());
        ArrayList<PrimaryInvoice> primaryInvoiceArrayList = new ArrayList<>();

        while (cursor.moveToNext()) {
            PrimaryInvoice primaryInvoice = new PrimaryInvoice();
            int id = cursor.getInt(0);
            primaryInvoice.setPrimary_invoice_id(id);
            primaryInvoice.setWarehouseId(cursor.getString(1));
            primaryInvoice.setPrimary_invoice_number(cursor.getString(2));
            primaryInvoiceArrayList.add(primaryInvoice);
        }

        Log.d("StockReceipt --Db", "Get Invoice Array Lists  " + primaryInvoiceArrayList.toString());

        cursor.close();
        sqlWrite.close();
        return primaryInvoiceArrayList;
    }

    public long create(PrimaryInvoice primaryInvoice) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_PRIMARY_INVOICE_NUMBER, primaryInvoice.getPrimary_invoice_number());
        values.put(COLUMN_WAREHOUSE_ID, primaryInvoice.getWarehouseId());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_PRIMARY_INVOICE, null, values);
        db.close();
        return id;
    }


    /*
     * Fetching the Primary Invoice Numbers Details List from the Master via Web Service
     */
    public void insertingPrimaryInvoicesMaster(ArrayList<PrimaryInvoice> primaryInvoiceList) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            if (primaryInvoiceList != null) {
                for (int i = 0; i < primaryInvoiceList.size(); i++) {
                    values.put(COLUMN_PRIMARY_INVOICE_NUMBER, primaryInvoiceList.get(i).getPrimary_invoice_number());
                    values.put(COLUMN_WAREHOUSE_ID, primaryInvoiceList.get(i).getWarehouseId());
                    db.insert(TABLE_PRIMARY_INVOICE, null, values);
                }
                db.setTransactionSuccessful();

            }

        } finally {
            db.endTransaction();
        }

    }


    public void deletePrimaryInvoice() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRIMARY_INVOICE, null, null);
        db.close();
    }


}
