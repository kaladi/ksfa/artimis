package in.kumanti.emzor.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import in.kumanti.emzor.R;
import in.kumanti.emzor.api.ApiInterface;
import in.kumanti.emzor.eloquent.AgeTable;
import in.kumanti.emzor.eloquent.ArtifactsTable;
import in.kumanti.emzor.eloquent.BDEListTable;
import in.kumanti.emzor.eloquent.CompanySetupTable;
import in.kumanti.emzor.eloquent.CompetitorProductTable;
import in.kumanti.emzor.eloquent.CompetitorStockProductTable;
import in.kumanti.emzor.eloquent.CrmColdCallTable;
import in.kumanti.emzor.eloquent.CrmDetailsTable;
import in.kumanti.emzor.eloquent.CrmKeyPersonsAlertTable;
import in.kumanti.emzor.eloquent.CrmKeyPersonsTable;
import in.kumanti.emzor.eloquent.CrmMeetingsTable;
import in.kumanti.emzor.eloquent.CrmNewProductsTable;
import in.kumanti.emzor.eloquent.CrmOpportunityProductsTable;
import in.kumanti.emzor.eloquent.CrmOpportunityTable;
import in.kumanti.emzor.eloquent.CrmOrderProductsTable;
import in.kumanti.emzor.eloquent.CrmOrderQuotationImagesTable;
import in.kumanti.emzor.eloquent.CrmOrderTable;
import in.kumanti.emzor.eloquent.CrmProductGroupTable;
import in.kumanti.emzor.eloquent.CrmProductsTable;
import in.kumanti.emzor.eloquent.CrmQuotationProductsTable;
import in.kumanti.emzor.eloquent.CrmStatusTable;
import in.kumanti.emzor.eloquent.CrmTable;
import in.kumanti.emzor.eloquent.CustomerReturnProductsTable;
import in.kumanti.emzor.eloquent.CustomerReturnTable;
import in.kumanti.emzor.eloquent.CustomerShippingAddressTable;
import in.kumanti.emzor.eloquent.CustomerStockProductTable;
import in.kumanti.emzor.eloquent.DistributorsTable;
import in.kumanti.emzor.eloquent.DoctorPrescribedProductsTable;
import in.kumanti.emzor.eloquent.DoctorVisitInfoTable;
import in.kumanti.emzor.eloquent.ExpenseTable;
import in.kumanti.emzor.eloquent.GeneralSettingsTable;
import in.kumanti.emzor.eloquent.LedgerTable;
import in.kumanti.emzor.eloquent.LogInOutHistoryTable;
import in.kumanti.emzor.eloquent.LpoImagesTable;
import in.kumanti.emzor.eloquent.MyDBHandler;
import in.kumanti.emzor.eloquent.NewJourneyPlanCustomerTable;
import in.kumanti.emzor.eloquent.NewJourneyPlanTable;
import in.kumanti.emzor.eloquent.NotesTable;
import in.kumanti.emzor.eloquent.OpportunityProductDetailsTable;
import in.kumanti.emzor.eloquent.PharmacyInfoProductsTable;
import in.kumanti.emzor.eloquent.PharmacyInfoTable;
import in.kumanti.emzor.eloquent.PrimaryInvoiceProductTable;
import in.kumanti.emzor.eloquent.PrimaryInvoiceTable;
import in.kumanti.emzor.eloquent.ProductTable;
import in.kumanti.emzor.eloquent.RDLSyncTable;
import in.kumanti.emzor.eloquent.StockIssueProductsTable;
import in.kumanti.emzor.eloquent.StockIssueTable;
import in.kumanti.emzor.eloquent.StockReturnProductsTable;
import in.kumanti.emzor.eloquent.StockReturnTable;
import in.kumanti.emzor.eloquent.SurveyQuestionOptionTable;
import in.kumanti.emzor.eloquent.SurveyQuestionsTable;
import in.kumanti.emzor.eloquent.SurveyResponseAnswerTable;
import in.kumanti.emzor.eloquent.SurveyResponseTable;
import in.kumanti.emzor.eloquent.SurveyTable;
import in.kumanti.emzor.eloquent.WarehouseStockProductsTable;
import in.kumanti.emzor.eloquent.WarehouseTable;
import in.kumanti.emzor.mastersData.ActivationFlag;
import in.kumanti.emzor.mastersData.CustomerMaster;
import in.kumanti.emzor.mastersData.FeedbackType;
import in.kumanti.emzor.mastersData.ItemMaster;
import in.kumanti.emzor.mastersData.JourneyPlanMaster;
import in.kumanti.emzor.mastersData.LocationMaster;
import in.kumanti.emzor.mastersData.MarqueeText;
import in.kumanti.emzor.mastersData.PriceListHeader;
import in.kumanti.emzor.mastersData.PriceListLines;
import in.kumanti.emzor.mastersData.SetupValidations;
import in.kumanti.emzor.model.Age;
import in.kumanti.emzor.model.Artifacts;
import in.kumanti.emzor.model.BDEList;
import in.kumanti.emzor.model.CompetitorProduct;
import in.kumanti.emzor.model.CrmColdCall;
import in.kumanti.emzor.model.CustomerShippingAddress;
import in.kumanti.emzor.model.Distributors;
import in.kumanti.emzor.model.GeneralSettings;
import in.kumanti.emzor.model.Ledger;
import in.kumanti.emzor.model.LogInOut;
import in.kumanti.emzor.model.MDLSync;
import in.kumanti.emzor.model.MasterResponseJson;
import in.kumanti.emzor.model.MetricsTarget;
import in.kumanti.emzor.model.Orders;
import in.kumanti.emzor.model.PrimaryInvoice;
import in.kumanti.emzor.model.PrimaryInvoiceProduct;
import in.kumanti.emzor.model.SRPriceList;
import in.kumanti.emzor.model.SurveyHeader;
import in.kumanti.emzor.model.SurveyLines;
import in.kumanti.emzor.model.ViewStockWarehouseProduct;
import in.kumanti.emzor.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static in.kumanti.emzor.activity.MainActivity.hideSoftKeyboard;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.FTP_HOST_NAME;
import static in.kumanti.emzor.utils.Constants.FTP_PASSWORD;
import static in.kumanti.emzor.utils.Constants.FTP_USER_NAME;

public class LoginActivity extends AppCompatActivity {

    public static final String USER_KEY = "User";
    public static final String PWD_KEY = "Password";
    public static final String PREF_NAME = "prefs";
    ImageView info;
    Button SignIN;
    static int sync_data = 0;
    static int activ_data1 = 0;
    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();

    EditText email_id, password;
    MyDBHandler dbHandler;
    TextView forgot_password, signup;
    String login_id, email;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String userrole = "";
    String login_type;
    ApiInterface api;
    String tab_code = "";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    CheckBox ckbremember;
    Boolean savelogin;
    TextView actionbarTitle, marqueeText;
    ImageView actionbarBackButton, deviceInfo, customerImage,headerImage,footerLogo;
    LinearLayout view;
    String logInDate, logInTime;
    LogInOutHistoryTable logInOutHistoryTable;
    Thread postingDbThread;
    private TextView marquee_txt;

    CustomerShippingAddressTable customerShippingAddressTable;
    CompetitorProductTable competitorProductTable;
    SurveyTable surveyTable;
    SurveyQuestionsTable surveyQuestionsTable;
    SurveyQuestionOptionTable surveyQuestionOptionTable;
    WarehouseStockProductsTable warehouseStockProductsTable;
    LedgerTable ledgerTable;
    AgeTable ageTable;
    WarehouseTable warehouseTable;
    PrimaryInvoiceTable primaryInvoiceTable;
    PrimaryInvoiceProductTable primaryInvoiceProductTable;
    DistributorsTable distributorsTable;
    GeneralSettingsTable generalSettingsTable;
    ArtifactsTable artifactsTable;
    BDEListTable bdeListTable;
    ExpenseTable expenseTable;
    TelephonyManager telephonyManager;
    ArrayList<CustomerMaster> customerMasterList;
    ArrayList<ItemMaster> itemMastersList;
    ArrayList<MetricsTarget> metricsTargetList;
    ArrayList<CompetitorProduct> competitor_productList;
    ArrayList<PriceListHeader> price_headerList;
    ArrayList<PriceListLines> price_lineList;
    ArrayList<CustomerShippingAddress> shipping_addressList;
    ArrayList<JourneyPlanMaster> journeyplanList;
    ArrayList<ViewStockWarehouseProduct> warehouseProductsList;
    ArrayList<Ledger> ledgerList;
    ArrayList<Age> ageList;
    ArrayList<FeedbackType> feedbackList;
    ArrayList<MarqueeText> marqueeTextList;
    ArrayList<LocationMaster> locationList;
    ArrayList<PrimaryInvoice> primaryInvoiceList;
    ArrayList<PrimaryInvoiceProduct> primaryInvoiceProductsList;
    ArrayList<CrmColdCall> coldCallArrayList;
    ArrayList<Distributors> distributorsArrayList;
    ArrayList<SRPriceList> srPriceListArrayList;
    ArrayList<GeneralSettings> settingsArrayList;
    ArrayList<Artifacts> artifactsArrayList;
    ArrayList<SurveyHeader> surveyArrayList;
    ArrayList<Orders> ordersArrayList;
    ArrayList<SurveyLines> surveyLinesArrayList;
    ArrayList<SurveyLines> surveyOptionsArrayList;
    ArrayList<BDEList> bdeListArrayList;

    int flag = 1;
    Gson gson;
    String company_code;
    Handler masterDownloadHandler;
    Thread thread1;
    String mdlSyncDate, mdlSyncTime;

    String imel_no="";
    int tabrowCount = 0;

    String imeiNumber  = "";
    String checkImei = "No";

    public EditText etImeiNumber;
    public TextView tvCancel, tvSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        File dbfile = new File(Constants.DATABASE_NAME);
        if (dbfile.exists() && !dbfile.isDirectory()) {
            MyDBHandler dbHandler1 = new MyDBHandler(this, null, null, DATABASE_VERSION);
            Constants.COMPANY_NAME = dbHandler1.get_company_name();
            company_code = dbHandler1.get_company_code();
        }


        initializeViews();
        customerType();
        populateHeaderDetails();
        setFooterDateTime();
        generateLogInDateTime();
        System.out.println("Constants.COMPANY_CODE = " + Constants.COMPANY_CODE);

        marqueeText.setSelected(true);
        //String marquee_txt = sharedPreferences.getString("MarqueeText","");
        //marqueeText.setText(marquee_txt);



       /* View overlay = findViewById(R.id.mylayout);

        overlay.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_FULLSCREEN); */
        //hideSystemUI();


        mSalesDatabaseCreation();
        dbHandler.execute_alter_script();

        /*int database_version = dbHandler.get_database_version();

        if(database_version== 0){

        } */


        sharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();



        savelogin = sharedPreferences.getBoolean("savelogin", true);

        if ("User".equals(login_type)) {

            if (savelogin) {

                ckbremember.setChecked(true);
                email_id.setText(sharedPreferences.getString(USER_KEY, null));
               // if (BuildConfig.DEBUG) password.setText("123456");
            }
        } else {

        }

       /* if (email_id.getText().toString().length() < 3 && BuildConfig.DEBUG) {
            email_id.setText("admin");
            password.setText("admin@123");
        }*/
        //Populate SalesRep Details

        Cursor setupDetails = dbHandler.getSetupDetails();
        int setupDetailsCount = setupDetails.getCount();
        if (setupDetailsCount == 0) {

        } else {
            int i = 0;
            while (setupDetails.moveToNext()) {
                tab_code = Constants.TAB_CODE = setupDetails.getString(0);
                Constants.SR_CODE = setupDetails.getString(1);
                Constants.COMPANY_NAME = setupDetails.getString(6);
            }
        }

        // Calling Main Page
        SignIN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
              /*  if (dbHandler.getTabDeatils().getCount()!=0) {
                    String emailIDD = email_id.getText().toString();
                    if(!emailIDD.equals("admin")){
                        activateApi();}}*/
                attemptLogin();
                //Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //startActivity(intent);
            }
        });

        deviceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
                startActivity(intent);

            }
        });

        forgot_password.setOnClickListener(new View.OnClickListener() {
            boolean cancel = false;
            View focusView = null;

            @Override
            public void onClick(View v) {
           /*     email_id.setError(null);
                email = email_id.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    email_id.setError(getString(R.string.error_field_required));
                    focusView = email_id;
                    cancel = true;
                } else if (!email.matches(emailPattern)) {

                    email_id.setError("Invalid Email Address");
                    focusView = email_id;
                    cancel = true;

                } else {*/

                  /*  Intent i = new Intent(getApplicationContext(), ForgetPasswordActivity.class);
                    i.putExtra("email", email);
                    startActivity(i);*/
//                }

                Cursor tabDetails = dbHandler.getTabDeatils();
                int tabrowCount = tabDetails.getCount();

                if (tabrowCount != 0) {
                    getForgetPasswordDialog();
                }else{
                    Toast.makeText(getApplicationContext(), "Please Contact Admin", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    void getForgetPasswordDialog() {



        View editingNotesToolView = getLayoutInflater().inflate(R.layout.activity_forget_password, null, false);

        EditText forget_password = editingNotesToolView.findViewById(R.id.forget_password);
        Button tvPwdSubmit = editingNotesToolView.findViewById(R.id.send);
        ImageButton tvPwdCancel = editingNotesToolView.findViewById(R.id.tvPwdCancel);


        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setView(editingNotesToolView);
        final android.app.AlertDialog alert = builder.create();
        alert.show();


        forget_password.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if(!s.equals("") ) {
                    //do your work here
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            public void afterTextChanged(Editable s) { }
        });

        tvPwdSubmit.setOnClickListener(v -> {
            hideSoftKeyboard(getApplicationContext(), v);
            forget_password.setError(null);
            String forget_password1 = forget_password.getText().toString();
            if (TextUtils.isEmpty(forget_password1)) {
                forget_password.setError(getString(R.string.error_field_required));
//                Toast.makeText(getApplicationContext(), "Please Enter Mail Id", Toast.LENGTH_LONG).show();
                return;
            }

            if (!forget_password1.matches(emailPattern)) {
                forget_password.setError("Invalid Email Address");
                return;
            }else{

                Cursor setupDetails = dbHandler.getSetupDetails();
                int setupDetailsCount = setupDetails.getCount();
                if (setupDetailsCount == 0) {

                } else {
                    int i = 0;
                    while (setupDetails.moveToNext()) {

                        Constants.COMPANY_CODE = setupDetails.getString(2);
                        Constants.COMPANY_NAME = setupDetails.getString(6);
                    }
                }

                String user_password = dbHandler.getPassword(email);
                String emailIdd= dbHandler.get_user_setup();
                Gson gson = new GsonBuilder().setLenient().create();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                ApiInterface api = retrofit.create(ApiInterface.class);
                Call<ResponseBody> call = api.postingForgetPassword(emailIdd,forget_password1,user_password,Constants.COMPANY_CODE);
                System.out.println("TTT::emailIdd = " + emailIdd);
                System.out.println("TTT::forget_password1 = " + forget_password1);
                System.out.println("TTT::user_password = " + user_password);
                System.out.println("TTT::Constants.COMPANY_CODE = " + Constants.COMPANY_CODE);
                Toast.makeText(getApplicationContext(), "Password Sent to your Mail Id", Toast.LENGTH_LONG).show();


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            Log.d("EMAIL SEND:", "Success----" + response.body().string());
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Log.i("EMAIL SEND: ", "failure------" + t);
                        Toast.makeText(getApplicationContext(), "Throwable" + t, Toast.LENGTH_LONG).show();

                    }

                });
            }

            alert.dismiss();

        });


        tvPwdCancel.setOnClickListener(v -> {
            alert.dismiss();
        });



    }
    private void initializeViews() {
        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);
        SignIN = findViewById(R.id.email_sign_in_button);

        email_id = findViewById(R.id.email_id);
        password = findViewById(R.id.password);
        forgot_password = findViewById(R.id.forgot_password);
        ckbremember = findViewById(R.id.ch_rememberme);

        headerImage  =  findViewById(R.id.headerImage);
        footerLogo = findViewById(R.id.footerLogo);
        view = findViewById(R.id.view);

        logInOutHistoryTable = new LogInOutHistoryTable(getApplicationContext());

    }

    private void customerType(){
        artimis();
        /*if (Constants.COMPANY_NAME==null){
            artimis();
        }
        else {
            if (Constants.COMPANY_NAME.equals("VISTA INTERNATIONAL LTD")){

                vista();
            }else {
                artimis();
            }
        }*/

    }

    private void  artimis(){
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.gps_progress_on));
        }

        view.setBackgroundColor(this.getResources().getColor(R.color.green_vista));
        Resources res = getResources();
        headerImage.setImageDrawable(res.getDrawable(R.drawable.artemis_logo));
        footerLogo.setImageDrawable(res.getDrawable(R.drawable.artemis_logo));
        SignIN.setBackgroundResource(R.drawable.btn_primary_green);
        ckbremember.setTextColor(this.getResources().getColor(R.color.green_vista));
        forgot_password.setTextColor(this.getResources().getColor(R.color.green_vista));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ckbremember.setButtonTintList(ColorStateList.valueOf(this.getResources().getColor(R.color.green_vista)));
        }

    }
    private void  vista(){
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.blue_vista));
        }
        view.setBackgroundColor(this.getResources().getColor(R.color.blue_vista));
        Resources res = getResources();
        headerImage.setImageDrawable(res.getDrawable(R.drawable.vista_header));
        footerLogo.setImageDrawable(res.getDrawable(R.drawable.vista_footer));
        SignIN.setBackgroundResource(R.drawable.btn_primary_blue);
        ckbremember.setTextColor(this.getResources().getColor(R.color.blue_vista));
        forgot_password.setTextColor(this.getResources().getColor(R.color.blue_vista));
       /* ContextThemeWrapper newContext = new ContextThemeWrapper(getApplicationContext(), R.style.MyCheckboxStyle1);
        ckbremember = new CheckBox(newContext);*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ckbremember.setButtonTintList(ColorStateList.valueOf(this.getResources().getColor(R.color.blue_vista)));
        }
    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            login_type = bundle.getString("login_type");
        }

    }

    private void mSalesDatabaseCreation() {

        dbHandler = new MyDBHandler(this, null, null, DATABASE_VERSION);
        AgeTable ageTable = new AgeTable(getApplicationContext());
        BDEListTable bDEListTable = new BDEListTable(getApplicationContext());
        CompetitorProductTable competitorProductTable = new CompetitorProductTable(getApplicationContext());
        CompanySetupTable companySetupTable = new CompanySetupTable(getApplicationContext());
        CustomerReturnTable customerReturnTable = new CustomerReturnTable(getApplicationContext());
        CustomerShippingAddressTable CustomerShippingAddressTable = new CustomerShippingAddressTable(getApplicationContext());
        CustomerReturnProductsTable customerReturnProductsTable = new CustomerReturnProductsTable(getApplicationContext());
        CustomerShippingAddressTable customerShippingAddressTable = new CustomerShippingAddressTable(getApplicationContext());
        CustomerStockProductTable customerStockProductTable = new CustomerStockProductTable(getApplicationContext());
        DistributorsTable distributorsTable = new DistributorsTable(getApplicationContext());
        DoctorPrescribedProductsTable doctorPrescribedProductsTable = new DoctorPrescribedProductsTable(getApplicationContext());
        DoctorVisitInfoTable doctorVisitInfoTable = new DoctorVisitInfoTable(getApplicationContext());
        LogInOutHistoryTable logInOutHistoryTable = new LogInOutHistoryTable(getApplicationContext());
        LpoImagesTable lpoImagesTable = new LpoImagesTable(getApplicationContext());
        LedgerTable ledgerTable = new LedgerTable(getApplicationContext());
        NewJourneyPlanTable NewJourneyPlanTable = new NewJourneyPlanTable(getApplicationContext());
        NewJourneyPlanCustomerTable NewJourneyPlanCustomerTable = new NewJourneyPlanCustomerTable(getApplicationContext());
        NotesTable notesTable = new NotesTable(getApplicationContext());
        OpportunityProductDetailsTable opportunityProductDetailsTable = new OpportunityProductDetailsTable(getApplicationContext());
        //OldOpportunityTable opportunityTable = new OldOpportunityTable(getApplicationContext());
        PharmacyInfoTable PharmacyInfoTable = new PharmacyInfoTable(getApplicationContext());
        PharmacyInfoProductsTable PharmacyInfoProductsTable = new PharmacyInfoProductsTable(getApplicationContext());
        PrimaryInvoiceProductTable primaryInvoiceProductTable = new PrimaryInvoiceProductTable(getApplicationContext());
        PrimaryInvoiceTable primaryInvoice = new PrimaryInvoiceTable(getApplicationContext());
        CompetitorStockProductTable competitorStockProductTable = new CompetitorStockProductTable(getApplicationContext());
        CrmProductGroupTable crmProductGroupTable = new CrmProductGroupTable(getApplicationContext());
        ProductTable productTable = new ProductTable(getApplicationContext());
        RDLSyncTable RDLSyncTable = new RDLSyncTable(getApplicationContext());
        StockIssueProductsTable StockIssueProductsTable = new StockIssueProductsTable(getApplicationContext());
        StockIssueTable StockIssueTable = new StockIssueTable(getApplicationContext());
        StockReturnTable StockReturnTable = new StockReturnTable(getApplicationContext());
        StockReturnProductsTable StockReturnProductsTable = new StockReturnProductsTable(getApplicationContext());
        SurveyQuestionOptionTable surveyQuestionOptionTable = new SurveyQuestionOptionTable(getApplicationContext());
        SurveyQuestionsTable surveyQuestionsTable = new SurveyQuestionsTable(getApplicationContext());
        SurveyResponseAnswerTable surveyResponseAnswerTable = new SurveyResponseAnswerTable(getApplicationContext());
        SurveyResponseTable surveyResponseTable = new SurveyResponseTable(getApplicationContext());
        SurveyTable surveyTable = new SurveyTable(getApplicationContext());
        WarehouseStockProductsTable warehouseStockProductsTable = new WarehouseStockProductsTable(getApplicationContext());
        WarehouseTable warehouseTable = new WarehouseTable(getApplicationContext());
        GeneralSettingsTable generalSettingsTable = new GeneralSettingsTable(getApplicationContext());
        ArtifactsTable artifactsTable = new ArtifactsTable(getApplicationContext());
        ExpenseTable ExpenseTable = new ExpenseTable(getApplicationContext());



        CrmColdCallTable CrmColdCallTable = new CrmColdCallTable(getApplicationContext());
        CrmDetailsTable CrmDetailsTable = new CrmDetailsTable(getApplicationContext());
        CrmKeyPersonsAlertTable CrmKeyPersonsAlertTable = new CrmKeyPersonsAlertTable(getApplicationContext());
        CrmKeyPersonsTable CrmKeyPersonsTable = new CrmKeyPersonsTable(getApplicationContext());
        CrmMeetingsTable CrmMeetingsTable = new CrmMeetingsTable(getApplicationContext());
        CrmNewProductsTable CrmNewProductsTable = new CrmNewProductsTable(getApplicationContext());
        CrmOpportunityProductsTable CrmOpportunityProductsTable = new CrmOpportunityProductsTable(getApplicationContext());
        CrmOpportunityTable CrmOpportunityTable = new CrmOpportunityTable(getApplicationContext());
        CrmOrderProductsTable CrmOrderProductsTable = new CrmOrderProductsTable(getApplicationContext());
        CrmOrderQuotationImagesTable CrmOrderQuotationImagesTable = new CrmOrderQuotationImagesTable(getApplicationContext());
        CrmOrderTable CrmOrderTable = new CrmOrderTable(getApplicationContext());
        CrmProductGroupTable CrmProductGroupTable = new CrmProductGroupTable(getApplicationContext());
        CrmProductsTable CrmProductsTable = new CrmProductsTable(getApplicationContext());
        CrmQuotationProductsTable CrmQuotationProductsTable = new CrmQuotationProductsTable(getApplicationContext());
        CrmStatusTable CrmStatusTable = new CrmStatusTable(getApplicationContext());
        CrmTable CrmTable = new CrmTable(getApplicationContext());

        //DoctorPrescribedProductsTable doctorPrescribedProductsTable = new DoctorPrescribedProductsTable(getApplicationContext());



    }

    //@Override
   /* protected void onStart() {
        super.onStart();
        // start lock task mode if it's not already active
        ActivityManager am = (ActivityManager) getSystemService(
                Context.ACTIVITY_SERVICE);
        // ActivityManager.getLockTaskModeState api is not available in pre-M.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (!am.isInLockTaskMode()) {
                startLockTask();
            }
        } else {
            if (am.getLockTaskModeState() ==
                    ActivityManager.LOCK_TASK_MODE_NONE) {
                startLockTask();
            }
        }


    } */

   void activateApi(){

       MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);


       int line_count = dbHandler.getmaster_count();

       if (line_count == 0) {

           boolean networkConnected = isNetworkConnected();
           boolean networking = isInternetAvailable();
           if (networkConnected == true) {

               Gson gson = new GsonBuilder()
                       .setLenient()
                       .create();
               OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();


               Retrofit retrofit = new Retrofit.Builder()
                       .baseUrl(ApiInterface.ACTIVATION_URL)
                       .client(client)
                       .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                       .build();

               api = retrofit.create(ApiInterface.class);

               Call<ActivationFlag> validatedLogin = api.getActivationFlag(company_code);

               System.out.println("validatedLogin1 = " + validatedLogin);
               validatedLogin.enqueue(new Callback<ActivationFlag>() {

                   @Override
                   public void onResponse(Call<ActivationFlag> validatedLogin, retrofit2.Response<ActivationFlag> response) {
                       System.out.println("validatedLogin2 = " + response);
                       ActivationFlag mLoginObject = response.body();
                       System.out.println("mLoginObject = " + response.body());
                       System.out.println("mLoginObject.status = " + mLoginObject.flag);
                       String returnedResponse = mLoginObject.flag;

                       if ("Y".equals(returnedResponse)) {
                           int flag1 = 0;
                           dbHandler.update_tab_flag(flag1);
                           flag = 0;
//                           activ_data1 =1;

                           System.out.println("flag4 = " + flag);
                       }
                    /*   else {
                           Toast.makeText(getApplicationContext(), "Please Contact Admin to Activate your login", Toast.LENGTH_SHORT).show();

                       }*/

                   }

                   @Override
                   public void onFailure(Call<ActivationFlag> call, Throwable t) {
                       System.out.println("validatedLogin4 = " + validatedLogin);
                       System.out.println("t11.getMessage() = " + t.getMessage());
                       Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                   }
               });

           } else {
               Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();

           }

       }
   }

    public void setProgressBar() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Please wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus = 0;

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressBarStatus < 100) {
                    progressBarStatus += 30;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    progressBarbHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }
                if (progressBarStatus >= 100) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progressBar.dismiss();
                }

            }
        }).start();

    }
    private void attemptLogin() {

        // Reset errors.
        email_id.setError(null);
        password.setError(null);

        // Store values at the time of the login attempt.
        email = email_id.getText().toString();
        String pwd = password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if ("admin".equals(email)) {
            login_type = "Admin";
        } else {
            login_type = "User";
        }

        if (TextUtils.isEmpty(email)) {
            email_id.setError(getString(R.string.error_field_required));
            focusView = email_id;
            cancel = true;
        } else if (TextUtils.isEmpty(pwd)) {
            password.setError(getString(R.string.error_field_required));
            focusView = password;
            cancel = true;
        }

        if ("Admin".equals(login_type)) {

        } else {
            if (!isEmailValid(email)) {
                email_id.setError(getString(R.string.error_invalid_email));
                focusView = email_id;
                cancel = true;
            }
        }


        if (cancel) {
            focusView.requestFocus();
        } else {
            if ("User".equals(login_type)) {
                if (ckbremember.isChecked()) {

                    editor.putBoolean("savelogin", true);
                    editor.putString(USER_KEY, email);
                    editor.putString(PWD_KEY, pwd);
                    editor.commit();

                }

            } else {

            }


            if ("User".equals(login_type)) {
                login_id = dbHandler.getLoginID(email, pwd);
                userrole = dbHandler.getRole(email);


                if (login_id.equals("0")) {
                    Toast.makeText(this.getApplicationContext(), "Invalid Login", Toast.LENGTH_LONG).show();

                }
                else {

//                    int flag2 = dbHandler.get_activate_flag();
//                    System.out.println("flag5 = " + flag2);
//                    System.out.println("flag6 = " + flag);
//                    if(activ_data1 == 1) {
//                        if (flag2 == 1) {

                    MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);


                    int line_count = dbHandler.getmaster_count();

                    if (line_count == 0) {

                        boolean networkConnected = isNetworkConnected();
                        boolean networking = isInternetAvailable();
                        if (networkConnected == true) {

                            Gson gson = new GsonBuilder()
                                    .setLenient()
                                    .create();
                            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();


                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(ApiInterface.ACTIVATION_URL)
                                    .client(client)
                                    .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                                    .build();

                            api = retrofit.create(ApiInterface.class);

                            Call<ActivationFlag> validatedLogin = api.getActivationFlag(company_code);

                            System.out.println("validatedLogin1 = " + validatedLogin);
                            validatedLogin.enqueue(new Callback<ActivationFlag>() {

                                @Override
                                public void onResponse(Call<ActivationFlag> validatedLogin, retrofit2.Response<ActivationFlag> response) {
                                    System.out.println("validatedLogin2 = " + response);
                                    ActivationFlag mLoginObject = response.body();
                                    System.out.println("mLoginObject = " + response.body());
                                    System.out.println("mLoginObject.status = " + mLoginObject.flag);
                                    String returnedResponse = mLoginObject.flag;

                                    if ("N".equals(returnedResponse)) {
                                        int flag1 = 0;
                                        dbHandler.update_tab_flag(flag1);
                                        flag = 0;

                                    }
                                     int active_flag1 = dbHandler.get_activate_flag();
                                    if(active_flag1 == 1) {
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.putExtra("login_id", login_id);
                                        intent.putExtra("email", email);
                                        intent.putExtra("login_type", login_type);
                                        startActivity(intent);

                                        Log.d("Testing---", "LogIn Date---" + logInDate);
                                        Log.d("Testing---", "LogIn Time---" + logInTime);


                                        LogInOut logInOut = new LogInOut();
                                        logInOut.setLogInDate(logInDate);
                                        logInOut.setLogInTime(logInTime);
                                        editor = sharedPreferences.edit();
                                        String logInOutId = String.valueOf(logInOutHistoryTable.create(logInOut));
                                        Log.d("logInOutId", "Login " + logInOutId);
                                        editor.putString("logInOutId", logInOutId);
                                        editor.putString("logInId", login_id);
                                        editor.commit();
                                        // Toast.makeText(getApplicationContext(),"LogIn Successful", Toast.LENGTH_LONG).show();

                                        backupDatabase();
                                    }else {
                                        Toast.makeText(getApplicationContext(), "Please Contact Admin to Activate your login", Toast.LENGTH_SHORT).show();

                                    }

                                }

                                @Override
                                public void onFailure(Call<ActivationFlag> call, Throwable t) {
                                    System.out.println("validatedLogin4 = " + validatedLogin);
                                    System.out.println("t11.getMessage() = " + t.getMessage());
                                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } else {
                            Toast.makeText(getApplicationContext(), "Network is not Connected", Toast.LENGTH_LONG).show();

                        }

                    } else{
                        int active_flag1 = dbHandler.get_activate_flag();
                        if(active_flag1 == 1) {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.putExtra("login_id", login_id);
                            intent.putExtra("email", email);
                            intent.putExtra("login_type", login_type);
                            startActivity(intent);

                            Log.d("Testing---", "LogIn Date---" + logInDate);
                            Log.d("Testing---", "LogIn Time---" + logInTime);


                            LogInOut logInOut = new LogInOut();
                            logInOut.setLogInDate(logInDate);
                            logInOut.setLogInTime(logInTime);
                            editor = sharedPreferences.edit();
                            String logInOutId = String.valueOf(logInOutHistoryTable.create(logInOut));
                            Log.d("logInOutId", "Login " + logInOutId);
                            editor.putString("logInOutId", logInOutId);
                            editor.putString("logInId", login_id);
                            editor.commit();
                            // Toast.makeText(getApplicationContext(),"LogIn Successful", Toast.LENGTH_LONG).show();

                            backupDatabase();
                        }else {
                            Toast.makeText(getApplicationContext(), "Please Contact Admin to Activate your login", Toast.LENGTH_SHORT).show();

                        }
                    }
                }

            } else {
                String emailIDD = email_id.getText().toString();
                if(!emailIDD.equals("admin"))
                {
                    System.out.println("flag0 = " + flag);
                    activateApi();
                    System.out.println("flag1 = " + flag);
                }


                if (flag == 1) {
                    System.out.println("flag2 = " + flag);
                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();
                    OkHttpClient client = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build();


                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ApiInterface.BASE_URL)
                            .client(client)
                            .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                            .build();

                    api = retrofit.create(ApiInterface.class);

                    Call<SetupValidations> validatedLogin = api.getValidatedLogin(email, pwd);

                    System.out.println("validatedLogin1 = " + validatedLogin);
                    validatedLogin.enqueue(new Callback<SetupValidations>() {

                        @Override
                        public void onResponse(Call<SetupValidations> validatedLogin, retrofit2.Response<SetupValidations> response) {
                            System.out.println("validatedLogin2 = " + response);
                            SetupValidations mLoginObject = response.body();
                            System.out.println("mLoginObject = " + response.body());
                            System.out.println("mLoginObject.status = " + mLoginObject.status);
                            String returnedResponse = mLoginObject.status;

                            if ("1".equals(returnedResponse)) {
                                Constants.IMEI_NUMBER = imel_no;
                                System.out.println("TTT::imel_no = " + imel_no);
                                System.out.println("validatedLogin3 = " + validatedLogin);
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra("login_id", login_id);
                                intent.putExtra("email", email);
                                intent.putExtra("login_type", login_type);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid Login", Toast.LENGTH_SHORT).show();

                            }

                        }

                        @Override
                        public void onFailure(Call<SetupValidations> call, Throwable t) {
                            System.out.println("validatedLogin4 = " + validatedLogin);
                            System.out.println("t.getMessage() = " + t.getMessage());
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(getApplicationContext(), "Please Contact Admin to Activate your login", Toast.LENGTH_SHORT).show();

                }
            }
        }

    }

    private void fetch_data() {

        MyDBHandler dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);
        int line_count = dbHandler.get_count_master_download();

        if (line_count == 0) {

            customerShippingAddressTable = new CustomerShippingAddressTable(getApplicationContext());
            competitorProductTable = new CompetitorProductTable(getApplicationContext());
            surveyTable = new SurveyTable(getApplicationContext());
            surveyQuestionsTable = new SurveyQuestionsTable(getApplicationContext());
            surveyQuestionOptionTable = new SurveyQuestionOptionTable(getApplicationContext());
            warehouseStockProductsTable = new WarehouseStockProductsTable(getApplicationContext());
            ledgerTable = new LedgerTable(getApplicationContext());
            ageTable = new AgeTable(getApplicationContext());
            warehouseTable = new WarehouseTable(getApplicationContext());
            primaryInvoiceTable = new PrimaryInvoiceTable(getApplicationContext());
            primaryInvoiceProductTable = new PrimaryInvoiceProductTable(getApplicationContext());
            distributorsTable = new DistributorsTable(getApplicationContext());
            artifactsTable = new ArtifactsTable(getApplicationContext());
            bdeListTable = new BDEListTable(getApplicationContext());

            gson = new GsonBuilder()
                    .setLenient()
                    .create();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(500000, TimeUnit.SECONDS)
                    .readTimeout(500000, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiInterface.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson)) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();

            ApiInterface api = retrofit.create(ApiInterface.class);

            Call<MasterResponseJson> customerMaster = api.getMasterData(login_id, company_code);


            customerMaster.enqueue(new Callback<MasterResponseJson>() {

                @Override
                public void onResponse(Call<MasterResponseJson> customerMaster, retrofit2.Response<MasterResponseJson> response) {

                    MasterResponseJson resultSet = response.body();

                    if (resultSet.status.equals("true")) {

                        customerMasterList = new Gson().fromJson(resultSet.customer, new TypeToken<ArrayList<CustomerMaster>>() {
                        }.getType());

                        itemMastersList = new Gson().fromJson(resultSet.Product, new TypeToken<ArrayList<ItemMaster>>() {
                        }.getType());

                        competitor_productList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CompetitorProduct>>() {
                        }.getType());

                        price_headerList = new Gson().fromJson(resultSet.price_header, new TypeToken<ArrayList<PriceListHeader>>() {
                        }.getType());

                        price_lineList = new Gson().fromJson(resultSet.price_line, new TypeToken<ArrayList<PriceListLines>>() {
                        }.getType());

                        shipping_addressList = new Gson().fromJson(resultSet.shipping_address, new TypeToken<ArrayList<CustomerShippingAddress>>() {
                        }.getType());

                        journeyplanList = new Gson().fromJson(resultSet.journey_plan, new TypeToken<ArrayList<JourneyPlanMaster>>() {
                        }.getType());

                        warehouseProductsList = new Gson().fromJson(resultSet.warehouse_stock, new TypeToken<ArrayList<ViewStockWarehouseProduct>>() {
                        }.getType());

                        ledgerList = new Gson().fromJson(resultSet.customer_ledgerr, new TypeToken<ArrayList<Ledger>>() {
                        }.getType());

                        ageList = new Gson().fromJson(resultSet.customer_aging, new TypeToken<ArrayList<Age>>() {
                        }.getType());

                        feedbackList = new Gson().fromJson(resultSet.feedback, new TypeToken<ArrayList<FeedbackType>>() {
                        }.getType());

                        marqueeTextList = new Gson().fromJson(resultSet.message, new TypeToken<ArrayList<MarqueeText>>() {
                        }.getType());

                        locationList = new Gson().fromJson(resultSet.location, new TypeToken<ArrayList<LocationMaster>>() {
                        }.getType());

                        primaryInvoiceList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoice>>() {
                        }.getType());

                        primaryInvoiceProductsList = new Gson().fromJson(resultSet.sr_invoice, new TypeToken<ArrayList<PrimaryInvoiceProduct>>() {
                        }.getType());


                        coldCallArrayList = new Gson().fromJson(resultSet.competitor_product, new TypeToken<ArrayList<CrmColdCall>>() {
                        }.getType());

                        distributorsArrayList = new Gson().fromJson(resultSet.distributors, new TypeToken<ArrayList<Distributors>>() {
                        }.getType());

                        srPriceListArrayList = new Gson().fromJson(resultSet.sr_pricelist, new TypeToken<ArrayList<SRPriceList>>() {
                        }.getType());

                        settingsArrayList = new Gson().fromJson(resultSet.settings, new TypeToken<ArrayList<GeneralSettings>>() {
                        }.getType());

                        artifactsArrayList = new Gson().fromJson(resultSet.artifact, new TypeToken<ArrayList<Artifacts>>() {
                        }.getType());

                        surveyArrayList = new Gson().fromJson(resultSet.survey_header, new TypeToken<ArrayList<SurveyHeader>>() {
                        }.getType());

                        surveyLinesArrayList = new Gson().fromJson(resultSet.survey_line, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        surveyOptionsArrayList = new Gson().fromJson(resultSet.survey_option, new TypeToken<ArrayList<SurveyLines>>() {
                        }.getType());

                        bdeListArrayList = new Gson().fromJson(resultSet.sr_master_bde, new TypeToken<ArrayList<BDEList>>() {
                        }.getType());

                        metricsTargetList = new Gson().fromJson(resultSet.target, new TypeToken<ArrayList<MetricsTarget>>() {
                        }.getType());

                        deletingMasterData();

                        Log.d("Multi Thread*****", "Message-----." + marqueeTextList.size());

                        masterDownloadHandler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                if (msg.what == 1) {//
                                    Toast.makeText(getApplicationContext(), "Master Download has been Completed", Toast.LENGTH_SHORT).show();
                                    // Log.d("Multi", "Start Stop setIsDownloaded After Downloaded--" +mdlSync.getIsDownloaded());

                                } else {
                                    Toast.makeText(getApplicationContext(), "Master Data is not downloaded", Toast.LENGTH_SHORT).show();

                                }

                            }
                        };

                        thread1 = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                Log.d("Multi Thread*****", "Starting T------1.");
                                insertFirstData();
                                insertSecondData();
                                insertThirdData();
                                insertFourthData();
                                insertFifthData();
                                insertSixthData();
                                Log.d("Multi Thread*****", "Finishing T------1.");

                                generateMDLSyncDateTime();
                                MDLSync mdlSync = new MDLSync();
                                mdlSync.setSyncDate(mdlSyncDate);
                                mdlSync.setSyncTime(mdlSyncTime);
                                dbHandler.createMasterDownload(mdlSync);

                                mdlSync.setIsDownloaded("Done");
                                dbHandler.createMasterDownload(mdlSync);

                                mdlSync.setIsDownloaded("Done");
                                dbHandler.createMasterDownload(mdlSync);
                                if (mdlSync.getIsDownloaded().equals("Done"))
                                    masterDownloadHandler.sendEmptyMessage(1);
                                else
                                    masterDownloadHandler.sendEmptyMessage(0);


                            }
                        });


                        thread1.start();


                        sync_data = 0;


                        Log.d("Multi Thread*****", "Data Downloaded-----.");

                    }
                }

                @Override
                public void onFailure(Call<MasterResponseJson> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    sync_data = 0;

                }
            });


//            postingOrderDetailsToWeb();

        } else {

        }
    }

    private void generateMDLSyncDateTime() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        mdlSyncDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        mdlSyncTime = sdf.format(outDate);
    }

    private void deletingMasterData() {

        dbHandler.deleteMasterData();

        //dbHandler.deleteCustomerMaster();
        //dbHandler.deleteItemMaster();
        competitorProductTable.deleteCompetitorProducts();
        //dbHandler.deletePriceList();
        //dbHandler.deleteJourneyplan();
        customerShippingAddressTable.deleteShippingAddress();
        warehouseStockProductsTable.deleteWarehouseStockProduct();
        warehouseTable.deleteWarehouse();
        ledgerTable.deleteLedger();
        ageTable.deleteAge();
        primaryInvoiceProductTable.deletePrimaryInvoiceProduct();
        primaryInvoiceTable.deletePrimaryInvoice();
        distributorsTable.deleteDistributors();
        generalSettingsTable.deleteGeneralSettings();
        artifactsTable.deleteArtifacts();
        surveyTable.deleteSurveyHeader();
        surveyQuestionsTable.deleteSurveyLines();
        surveyQuestionOptionTable.deleteSurveyOptions();
        bdeListTable.deleteBdeList();

    }

    public void insertFirstData() {

        generalSettingsTable.insertingSettingsMaster(settingsArrayList);

        dbHandler.insertingJourneyPlanMaster(journeyplanList);

        dbHandler.insertingCustomerMaster(customerMasterList);


        if (customerMasterList != null) {
            for (int i = 0; i < customerMasterList.size(); i++) {

                new MainActivity.AsyncTaskLoadImage().execute(customerMasterList.get(i).getImage_url(), customerMasterList.get(i).getCustomer_image());
            }
        }

        dbHandler.insertingMetricsTargetMaster(metricsTargetList);
    }

    public void insertSecondData() {

        dbHandler.insertingProductsMaster(itemMastersList);

        dbHandler.insertingPriceListsHeaderMaster(price_headerList);

        dbHandler.insertingPriceListLinesMaster(price_lineList);

        dbHandler.insertingBdePriceListsMaster(srPriceListArrayList);

    }


    public void insertThirdData() {

        warehouseTable.insertingWarehousesMaster(warehouseProductsList);

        primaryInvoiceTable.insertingPrimaryInvoicesMaster(primaryInvoiceList);

        primaryInvoiceProductTable.insertingPrimaryInvoiceProductsMaster(primaryInvoiceProductsList);

        distributorsTable.insertingDistributorsMaster(distributorsArrayList);

        warehouseStockProductsTable.insertingWarehouseStockMaster(warehouseProductsList);

    }

    public void insertFourthData() {


        customerShippingAddressTable.insertingCusShippingAddressMaster(shipping_addressList);

        competitorProductTable.insertingCompetitorProductsMaster(competitor_productList);

        dbHandler.insertingLocationsMaster(locationList);


    }

    public void insertFifthData() {

        ledgerTable.insertingLedgersMaster(ledgerList);

        ageTable.insertingAgeDetailsMaster(ageList);

        dbHandler.insertingFooterMarqueeMsgText(marqueeTextList);

        bdeListTable.insertingBDEListMaster(bdeListArrayList);


    }

    public void insertSixthData() {


        artifactsTable.insertingArtifactsMaster(artifactsArrayList);

        if (artifactsArrayList != null) {
            for (int i = 0; i < artifactsArrayList.size(); i++) {

                new MainActivity.AsyncTaskLoadPdf().execute(artifactsArrayList.get(i).getFileURL(), artifactsArrayList.get(i).getFileName());

            }
        }


        surveyTable.insertingSurveysMaster(surveyArrayList);

        surveyQuestionsTable.insertingSurveyQuestionsMaster(surveyLinesArrayList);

        if (surveyOptionsArrayList != null) {
            surveyQuestionOptionTable.insertingSurveyOptionsMaster(surveyOptionsArrayList);
        }

        dbHandler.insertingFeedbackTypeMaster(feedbackList);


    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public boolean isInternetAvailable() {

        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;

    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();
    }


    private void generateLogInDateTime() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        logInDate = timeStampFormat.format(myDate);

        long outDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        logInTime = sdf.format(outDate);
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SCREEN_STATE_OFF

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {

    }

    private void backupDatabase() {
        String sourcePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/ksfaDB.DB";
        File source = new File(sourcePath);

        File backupDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/KSFA/", "dbBackup");
        backupDir.mkdir();
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String date = timeStampFormat.format(myDate);
        String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/dbBackup/" + tab_code + "_" + login_id + "_" + date + "_";
        try {
            File f = new File(destinationPath + "ksfaDB.DB");
            if (f.exists() && !f.isDirectory()) {
                return;
            }
            File temp = File.createTempFile(destinationPath, "ksfaDB.DB");

            boolean exists = temp.exists();

            if (exists) {
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        destinationPath += "ksfaDB.DB";

        File copied = new File(destinationPath);
        try (
                InputStream in = new BufferedInputStream(
                        new FileInputStream(source));
                OutputStream out = new BufferedOutputStream(
                        new FileOutputStream(copied))) {

            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        postingDbThread = new Thread(new Runnable() {

            @Override
            public void run() {
                Log.d("Multi Thread DB*****", "Starting T------1.");
                postingDatabaseToFtp();
                Log.d("Multi Thread DB*****", "Finishing T-----1.");
            }
        });

        postingDbThread.start();
    }

    private void postingDatabaseToFtp() {

        FTPClient con = null;
        con = new FTPClient();

        try {
            con.connect(FTP_HOST_NAME);
            if (con.login(FTP_USER_NAME, FTP_PASSWORD)) {
                Log.d("FileUpload", "FilePath Login");
                con.enterLocalPassiveMode(); // important!
                con.setFileType(FTP.BINARY_FILE_TYPE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d("FileUpload", "FilePath Connected");
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String date = timeStampFormat.format(myDate);
        String destinationPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/KSFA/dbBackup/" + tab_code + "_" + login_id + "_" + date + "_";


        destinationPath += "ksfaDB.DB";

        String file_name = tab_code + "_" + login_id + "_" + date + "_" + "ksfaDB.DB";
        Log.d("FileUpload", "FileName--" + file_name);

        try {
            String data = Environment.getExternalStorageDirectory() + File.separator
                    + "KSFA" + File.separator + "ksfaDB.DB";
            Log.d("FileUpload", "FilePath " + data);

            FileInputStream in = new FileInputStream(new File(destinationPath));
            String serverPath = "/home/vistagftp/vistagftp/Db/" + file_name;
            boolean result = con.storeFile(serverPath, in);
            Log.d("FileUpload", "File Result--" + result);

            in.close();

            if (result) {
                Log.d("FileUpload", "Success");

            }


            con.logout();
            con.disconnect();

            Log.d("FileUpload", "File Connection Closed--");


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FileUpload", "Database Upload Error" + e.getMessage());
        }
    }


    void getImeiNumberDialog() {

        View editingNotesToolView = getLayoutInflater().inflate(R.layout.dialog_imei, null, false);

        etImeiNumber = editingNotesToolView.findViewById(R.id.etImeiNumber);
        tvSubmit = editingNotesToolView.findViewById(R.id.tvSubmit);
        tvCancel = editingNotesToolView.findViewById(R.id.tvCancel);


        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setView(editingNotesToolView);
        final android.app.AlertDialog alert = builder.create();
        alert.show();

        checkImei = "Yes";

        etImeiNumber.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if(!s.equals("") ) {
                    //do your work here
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            public void afterTextChanged(Editable s) { }
        });

        imeiNumber = etImeiNumber.getText().toString();
        tvSubmit.setOnClickListener(v -> {
            hideSoftKeyboard(getApplicationContext(), v);

            if (TextUtils.isEmpty(etImeiNumber.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Please enter IMEI", Toast.LENGTH_LONG).show();
                return;
            }

            if ((etImeiNumber.getText().toString()).length() > 5){
                imel_no=etImeiNumber.getText().toString();
                Constants.IMEI_NUMBER = imel_no;
                dbHandler.updateIMEINo(imel_no);
            }


            System.out.println("TTT::imel_no1 = " + imel_no);
            System.out.println("TTT::imeiNumber = " + imeiNumber);
            System.out.println("TTT::etImeiNumber. = " + etImeiNumber.getText().toString());
            alert.dismiss();

        });


        tvCancel.setOnClickListener(v -> {
//            hideSoftKeyboard(getApplicationContext(), v);
            alert.dismiss();
        });



    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Cursor tabDetails = dbHandler.getTabDeatils();
            tabrowCount = tabDetails.getCount();
            String getImei = dbHandler.getImeinumber();

            System.out.println("TT::getImei = " + getImei);
            System.out.println("TT::tabrowCount = " + tabrowCount);
            if (tabrowCount == 0) {
                if (imel_no.equals("") && checkImei.equals("No")) {
                    getImeiNumberDialog();
                }
            }

            if(tabrowCount!=0) {
                if(getImei.equals("")){
                    if (imel_no.equals("") && checkImei.equals("No")) {
                        getImeiNumberDialog();
                    }
                }else{
                    Constants.IMEI_NUMBER = getImei;
                    dbHandler.updateIMEINo(getImei);
                }
            }


        }else{
            telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }


            Constants.IMEI_NUMBER = telephonyManager.getDeviceId();
            String imel_no1 = telephonyManager.getDeviceId();

            Cursor tabDetails = dbHandler.getTabDeatils();
            tabrowCount = tabDetails.getCount();

            if(tabrowCount!=0) {
                dbHandler.updateIMEINo(imel_no1);
            }
        }

    }



}
