package in.kumanti.emzor.model;


import java.io.Serializable;

public class CrmStatus implements Serializable {

    private int crmStatusId;
    public String companyCode;
    public String tabCode;
    public String srCode;
    private String customerCode;
    private String crmNumber;
    private String crmStatusDate;
    private String crmStatus;
    private boolean isSync;
    private String createdAt;
    public String updatedAt;

    public int getCrmStatusId() {
        return crmStatusId;
    }

    public void setCrmStatusId(int crmStatusId) {
        this.crmStatusId = crmStatusId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getTabCode() {
        return tabCode;
    }

    public void setTabCode(String tabCode) {
        this.tabCode = tabCode;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCrmNumber() {
        return crmNumber;
    }

    public void setCrmNumber(String crmNumber) {
        this.crmNumber = crmNumber;
    }

    public String getCrmStatusDate() {
        return crmStatusDate;
    }

    public void setCrmStatusDate(String crmStatusDate) {
        this.crmStatusDate = crmStatusDate;
    }

    public String getCrmStatus() {
        return crmStatus;
    }

    public void setCrmStatus(String crmStatus) {
        this.crmStatus = crmStatus;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return crmNumber;
    }


}
