package in.kumanti.emzor.backgroundService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PostingDataReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.codepath.example.servicesdemo.alarm";

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context mContext, Intent intent) {
        Intent startServiceIntent = new Intent(mContext, MyService.class);
        mContext.startService(startServiceIntent);

        /*Intent i = new Intent(context, PostingDataService.class);
        PostingDataService.enqueueJobAction(context, "action.SOME_ACTION");*/
       /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(i);
        }
        else {
            context.startService(i);
        }*/
    }
}
