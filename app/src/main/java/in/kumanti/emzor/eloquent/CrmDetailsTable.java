package in.kumanti.emzor.eloquent;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.kumanti.emzor.model.Details;
import in.kumanti.emzor.utils.Constants;

import static in.kumanti.emzor.utils.Constants.DATABASE_NAME;
import static in.kumanti.emzor.utils.Constants.DATABASE_VERSION;
import static in.kumanti.emzor.utils.Constants.TABLE_CRM_DETAILS;

public class CrmDetailsTable extends SQLiteOpenHelper {

    private static final String COLUMN_CRM_DETAILS_ID = "detailsId";
    private static final String COLUMN_SALES_REP_CODE = "bdeCode";
    private static final String COLUMN_CUSTOMER_CODE = "customerCode";
    private static final String COLUMN_CRM_DETAILS_PRODUCT_CODE = "productCode";
    private static final String COLUMN_CRM_DETAILS_TEXT = "notesText";
    private static final String COLUMN_CRM_DETAILS_TYPE = "notesType";
    private static final String COLUMN_CRM_DETAILS_REFERENCE_NUMBER = "notesReferenceNo";
    private final static String COLUMN_CRM_DETAILS_SEQUENCE_NO = "visitSeqNo";
    private static final String COLUMN_CRM_DETAILS_CREATED_AT = "createdAt";
    private static final String COLUMN_CRM_DETAILS_UPDATED_AT = "updatedAt";

    public Context context;

    public CrmDetailsTable(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        //sqlWrite.execSQL("DROP TABLE IF EXISTS " + TABLE_CRM_DETAILS);

        String competitorInfoDetailTable = "CREATE TABLE IF NOT EXISTS " + TABLE_CRM_DETAILS + "(" +
                COLUMN_CRM_DETAILS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_SALES_REP_CODE + " TEXT, " +
                COLUMN_CUSTOMER_CODE + " TEXT, " +
                COLUMN_CRM_DETAILS_PRODUCT_CODE + " TEXT, " +
                COLUMN_CRM_DETAILS_TEXT + " TEXT, " +
                COLUMN_CRM_DETAILS_TYPE + " TEXT, " +
                COLUMN_CRM_DETAILS_REFERENCE_NUMBER + " BOOLEAN, " +
                COLUMN_CRM_DETAILS_CREATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CRM_DETAILS_UPDATED_AT + " INTEGER(4) NOT NULL DEFAULT (strftime('%s','now')), " +
                COLUMN_CRM_DETAILS_SEQUENCE_NO + " TEXT " +
                ");";

        sqlWrite.execSQL(competitorInfoDetailTable);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long create(Details details) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_SALES_REP_CODE, details.getSrCode());
        values.put(COLUMN_CUSTOMER_CODE, details.getCustomerCode());
        values.put(COLUMN_CRM_DETAILS_PRODUCT_CODE, details.getProductCode());
        values.put(COLUMN_CRM_DETAILS_TEXT, details.getNotesText());
        values.put(COLUMN_CRM_DETAILS_REFERENCE_NUMBER, details.getNotesReferenceNumber());
        values.put(COLUMN_CRM_DETAILS_TYPE, details.getNotesType());
        values.put(COLUMN_CRM_DETAILS_SEQUENCE_NO, details.getVisitSequenceNumber());

        SQLiteDatabase db = getWritableDatabase();
        long id = db.insert(TABLE_CRM_DETAILS, null, values);
        db.close();
        return id;
    }

    public long update(Details notes) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CUSTOMER_CODE, notes.getCustomerCode());
        values.put(COLUMN_CRM_DETAILS_PRODUCT_CODE, notes.getProductCode());
        values.put(COLUMN_CRM_DETAILS_TEXT, notes.getNotesText());
        values.put(COLUMN_CRM_DETAILS_UPDATED_AT, String.valueOf(System.currentTimeMillis() / 1000L));
        SQLiteDatabase db = getWritableDatabase();
        long id = db.update(TABLE_CRM_DETAILS, values, COLUMN_CRM_DETAILS_ID + " = ?", new String[]{String.valueOf(notes.getDetailsId())});
        db.close();
        return id;
    }

    public ArrayList<Details> getDetailsByCustomer(String customerCode, String productCode) {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CRM_DETAILS + " WHERE " + COLUMN_CUSTOMER_CODE + "=? AND " + COLUMN_CRM_DETAILS_PRODUCT_CODE + "=? ORDER BY " + COLUMN_CRM_DETAILS_UPDATED_AT + " desc";
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{customerCode, productCode});

        ArrayList<Details> notesArrayList = new ArrayList<>();
        int i = 0;
        while (cursor.moveToNext()) {
            Details note = new Details();
            note.setDetailsId(cursor.getInt(0));
            note.setSrCode(cursor.getString(1));
            note.setCustomerCode(cursor.getString(2));
            note.setProductCode(cursor.getString(3));
            note.setNotesText(cursor.getString(4));
            note.setNotesReferenceNumber(cursor.getString(6));

            String dateTime = cursor.getString(7);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd MMM yy hh:mm a");
            Date myDate = new Date(Long.parseLong(dateTime) * 1000L);
            String date = dateFormat.format(myDate);

            note.setTimeStamp(date);
            notesArrayList.add(i, note);
            i++;
        }
        cursor.close();
        sqlWrite.close();
        return notesArrayList;
    }

    public ArrayList<Details> getDetailsForSync() {
        SQLiteDatabase sqlWrite = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + TABLE_CRM_DETAILS + " WHERE DATE(datetime(createdAt,'unixepoch')) >= date('now','-5 Days')";

        ArrayList<Details> detailsArrayList = new ArrayList<>();
        Cursor cursor = sqlWrite.rawQuery(selectQuery, new String[]{});

        while (cursor.moveToNext()) {
            Details details = new Details();
            details.setTabCode(Constants.TAB_CODE);
            details.setCompanyCode(Constants.COMPANY_CODE);
            details.setSrCode(Constants.SR_CODE);

            int id = cursor.getInt(0);
            details.setDetailsId(id);
            details.setCustomerCode(cursor.getString(2));
            details.setProductCode(cursor.getString(3));
            details.setNotesText(cursor.getString(4));
            details.setNotesType(cursor.getString(5));
            details.setNotesReferenceNumber(cursor.getString(8));
            details.setCreatedAt(cursor.getString(7));
            details.setUpdatedAt(cursor.getString(6));

            detailsArrayList.add(details);
        }
        cursor.close();
        sqlWrite.close();
        return detailsArrayList;
    }



}
