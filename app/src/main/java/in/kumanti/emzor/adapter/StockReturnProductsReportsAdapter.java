package in.kumanti.emzor.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.model.StockReturnProductsReport;

public class StockReturnProductsReportsAdapter extends RecyclerView.Adapter<StockReturnProductsReportsAdapter.ViewHolder> {

    public ArrayList<StockReturnProductsReport> mDataset;
    public Context context;

    public StockReturnProductsReportsAdapter(ArrayList<StockReturnProductsReport> myDataset) {
        mDataset = myDataset;
    }

    public void add(StockReturnProductsReport item1, int position) {
        mDataset.add(position, item1);
        notifyItemInserted(position);
    }

    public void remove(StockReturnProductsReport item1) {
        int position = mDataset.indexOf(item1);
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public StockReturnProductsReportsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_return_products_reports, parent, false);

        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.returnDate.setText(mDataset.get(position).getStockReturnDate());
        viewHolder.returnNumber.setText(mDataset.get(position).getStockReturnNumber());
        viewHolder.returnType.setText(mDataset.get(position).getStockReturnType());
        viewHolder.productName.setText(mDataset.get(position).getProductName());
        viewHolder.returnQuantity.setText(mDataset.get(position).getReturnQuantity());
        viewHolder.returnPrice.setText(mDataset.get(position).getReturnPrice());
        viewHolder.returnValue.setText(mDataset.get(position).getReturnValue());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView returnDate, returnNumber, returnType, productName, returnQuantity, returnPrice, returnValue;

        public ViewHolder(View v) {
            super(v);
            returnType = v.findViewById(R.id.stockReturnTypeTv);
            returnDate = v.findViewById(R.id.stockReturnReportsDateTv);
            returnNumber = v.findViewById(R.id.stockReturnNumberDateTv);
            productName = v.findViewById(R.id.stockReturnProductNameTv);
            returnQuantity = v.findViewById(R.id.stockReturnReturnQtyTv);
            returnPrice = v.findViewById(R.id.stockReturnReturnPriceTv);
            returnValue = v.findViewById(R.id.stockReturnReturnValueTv);
        }
    }


}