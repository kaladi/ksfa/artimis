package in.kumanti.emzor.crm;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.DeviceInfoActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.adapter.DetailsAdapter;
import in.kumanti.emzor.adapter.RecyclerViewItemListener;
import in.kumanti.emzor.eloquent.CrmDetailsTable;
import in.kumanti.emzor.model.Details;
import in.kumanti.emzor.utils.GPSTracker;

public class CrmDetailsActivity extends MainActivity {

    ImageView actionbarBackButton, deviceInfo;
    private TextView actionbarTitle, marqueeText;

    public RecyclerView notesRecyclerView;
    RecyclerViewItemListener listener;
    private RecyclerView.Adapter notesAdapter;
    private RecyclerView.LayoutManager notesLayoutManager;

    public EditText notesText;
    public ImageButton saveNotesButton, cancelNotesButton;
    String login_id = "", checkin_time = "", customerCode = "", notesReferenceId = "", notesType = "PRODUCT", productCode = "", marqueeTextString;
    FloatingActionButton fabIcon;

    String[] notesReferenceIdList, notesTypeList;

    CrmDetailsTable crmDetailsTable;
    ArrayList<Details> detailsArrayList;
    Details note;

    GPSTracker gpsTracker;
    double latitudeDouble, longitudeDouble;
    String latitude, longitude, latitudeLongitude;
    DecimalFormat formatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeViews();
        populateHeaderDetails();
        getFooterDateTime();
//        getCustomerLocation();

        actionbarTitle.setText(R.string.notes_actionbar_title);

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        String marquee_txt = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marquee_txt);

        notesRecyclerView.setHasFixedSize(true);
        notesLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        notesRecyclerView.setLayoutManager(notesLayoutManager);

        note = new Details();

        setNotesData();

        fabIcon.setOnClickListener(view -> notesAdd(0, null));

        actionbarBackButton.setOnClickListener(v -> finish());

        deviceInfo.setOnClickListener(v -> {

            Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            startActivity(intent);


        });


    }

    private void getCustomerLocation() {

        gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {
            latitudeDouble = gpsTracker.getLatitude();
            longitudeDouble = gpsTracker.getLongitude();

            latitude = formatter.format(latitudeDouble);
            longitude = formatter.format(longitudeDouble);

            latitudeLongitude = latitude + "," + longitude;
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void setNotesData() {
        try {
            listener = (view, position) -> notesAdd(detailsArrayList.get(position).getDetailsId(), detailsArrayList.get(position).notesText);

            detailsArrayList = crmDetailsTable.getDetailsByCustomer(customerCode, productCode);
            Log.d("Crm--", "customer" + customerCode + " sdf " + detailsArrayList.size());

            notesAdapter = new DetailsAdapter(detailsArrayList, listener);
            notesRecyclerView.setAdapter(notesAdapter);
        } catch (Exception e) {
            Log.d("Crm--", "" + e.getMessage());
        }
    }

    public void notesAdd(int noteId, String noteData) {
        View editingNotesToolView = getLayoutInflater().inflate(R.layout.details_layout, null, false);

        notesText = editingNotesToolView.findViewById(R.id.detailsEditText);
        saveNotesButton = editingNotesToolView.findViewById(R.id.saveDetailsButton);
        cancelNotesButton = editingNotesToolView.findViewById(R.id.cancelDetailsButton);

        if (noteId == 0)
            notesText.setText("");
        else
            notesText.setText(noteData);
        Log.d("Crm--", "ShowNotesEditor: ");

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setView(editingNotesToolView);
        final android.app.AlertDialog alert = builder.create();
        Log.d("Crm--", "ShowNotesEditor: " + alert.toString());
        alert.show();

        saveNotesButton.setOnClickListener(v -> {
            hideSoftKeyboard(getApplicationContext(), v);

            if (TextUtils.isEmpty(notesText.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Please add the notes Text", Toast.LENGTH_LONG).show();
                return;
            }

            note.setDetailsId(noteId);
            note.setSrCode(login_id);
            note.setCustomerCode(customerCode);
            note.setProductCode(productCode);
            note.setNotesText(notesText.getText().toString());
            note.setNotesType(notesType);

            if (!TextUtils.isEmpty(notesReferenceId))
                note.setNotesReferenceNumber(notesReferenceId);
            String visitSequence = sharedPreferences.getString("VisitSequence", "");
            note.setVisitSequenceNumber(visitSequence);

            if (noteId == 0)
                crmDetailsTable.create(note);
            else
                crmDetailsTable.update(note);
            alert.dismiss();

            setNotesData();
        });


        cancelNotesButton.setOnClickListener(v -> {
            hideSoftKeyboard(getApplicationContext(), v);

            final Dialog alertBox = new Dialog(v.getRootView().getContext());

            LayoutInflater mInflater = (LayoutInflater) getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            final View alertLayout = mInflater.inflate(R.layout.activity_exit_notes_popup, null);

            alertBox.setCancelable(false);
            alertBox.setContentView(alertLayout);

            final Button Yes = alertLayout.findViewById(R.id.Yes);
            final Button No = alertLayout.findViewById(R.id.No);

            Yes.setOnClickListener(v1 -> {
                alertBox.dismiss();
                alert.cancel();
            });

            No.setOnClickListener(v12 -> alertBox.dismiss());

            alertBox.show();

        });
    }


    public void initializeViews() {

        actionbarTitle = findViewById(R.id.toolbar_title);
        actionbarBackButton = findViewById(R.id.back);

        marqueeText = findViewById(R.id.marquee);
        deviceInfo = findViewById(R.id.img_info);

        fabIcon = findViewById(R.id.createDetailsFab);

        crmDetailsTable = new CrmDetailsTable(getApplicationContext());
        notesRecyclerView = findViewById(R.id.detailsRecyclerView);
    }

    public void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            checkin_time = bundle.getString("checkin_time");
            customerCode = bundle.getString("customer_id");
            login_id = bundle.getString("login_id");
            notesType = bundle.getString("notesType");
            productCode = bundle.getString("productCode");

            Log.d("Crm--", "Details Product Code: " +productCode);


            if (bundle.containsKey("notesReferenceId")) {
                notesReferenceId = bundle.getString("notesReferenceId");
                notesReferenceIdList = bundle.getStringArray("notesReferenceIdList");
                notesTypeList = bundle.getStringArray("notesTypeList");
            }
        }
    }


    @Override
    protected void onResume() {
        Log.d("Crm-- ", "onResume ");

        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {

        super.onRestart();
    }

    @Override
    public void onBackPressed() {

    }
}
