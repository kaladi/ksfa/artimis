package in.kumanti.emzor.activity.reports;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import in.kumanti.emzor.R;
import in.kumanti.emzor.activity.EnquiryProductActivity;
import in.kumanti.emzor.activity.InvoiceProductActivity;
import in.kumanti.emzor.activity.MainActivity;
import in.kumanti.emzor.model.OrderDetailsList;
import in.kumanti.emzor.utils.Globals;


public class EnquiryReportsActivity extends MainActivity {

    ImageView actionBarBack;
    String login_id, checkin_time, marqueeTextString;
    TextView actionBarTitle, marqueeText;
    ArrayList<OrderDetailsList> enquiryDetailsListArrayList;
    OrderDetailsListAdapter adapter = null;
    DecimalFormat formatter;
    Globals globals;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_create_enquiry_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        initializeViews();
        populateHeaderDetails();
        setFooterDateTime();

        actionBarTitle.setText("Enquiry Report");

        marqueeTextString = sharedPreferences.getString("MarqueeText", "");
        marqueeText.setText(marqueeTextString);
        marqueeText.setSelected(true);

        enquiryDetailsListArrayList = new ArrayList<>();

        getInvoiceReportsDetails();


        actionBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void getInvoiceReportsDetails() {

        OrderDetailsList s1 = new OrderDetailsList("04-09-2020", "TAB006EN040920001", "General Hospital", "Lagos", "Kano", "2", "10580", "New Opportunity", "1", "CUS001");
        OrderDetailsList s2 = new OrderDetailsList("04-09-2020", "TAB006EN040920002", "ST Marry General", "Lagos", "Kano", "2", "10580", "Prospect", "1", "CUS001");
        enquiryDetailsListArrayList.add(s1);
        enquiryDetailsListArrayList.add(s2);

        System.out.println("TT::enquiryDetailsListArrayList = " + enquiryDetailsListArrayList);


        adapter = new OrderDetailsListAdapter(enquiryDetailsListArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);


    }

    private void populateHeaderDetails() {
        Bundle bundle = getIntent().getExtras();
        login_id = bundle.getString("login_id");

    }

    private void initializeViews() {

        actionBarTitle = findViewById(R.id.toolbar_title);
        actionBarBack = findViewById(R.id.back);
        recyclerView = findViewById(R.id.order_list);
        marqueeText = findViewById(R.id.marquee);

        globals = ((Globals) getApplicationContext());
        login_id = globals.getLogin_id();

        formatter = new DecimalFormat("#,###.00");

//        dbHandler = new MyDBHandler(getApplicationContext(), null, null, 1);

    }

    private void setFooterDateTime() {
        final TextView datetime = findViewById(R.id.datetime);
        String PATTERN = "dd MMM YY";
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern(PATTERN);
        String date = dateFormat.format(Calendar.getInstance().getTime());
        datetime.setText(date);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YY\nhh:mm a");
                                String dateString = sdf.format(date);
                                datetime.setText(dateString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();


    }

    public class OrderDetailsListAdapter extends RecyclerView.Adapter<OrderDetailsListAdapter.MyViewHolder> {

        private ArrayList<OrderDetailsList> users;

        public OrderDetailsListAdapter(ArrayList<OrderDetailsList> users) {
            this.users = users;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_enquiry_details, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            final OrderDetailsList movie = users.get(position);

            holder.date.setText(movie.getDate());
            holder.order_num.setText(movie.getOrder_num());
            holder.customer_name.setText(movie.getCustomer_name());
            holder.location.setText(movie.getLocation());
            holder.city.setText(movie.getCity());
            holder.line_items.setText(movie.getLine_items());
            holder.value.setText(movie.getValue());
            holder.status.setText(movie.getStatus());

            holder.vm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String id = movie.getOrder_id();
                    //  Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
                    Intent myintent = new Intent(getApplicationContext(), EnquiryProductActivity.class);

                    //Create the bundle
                    Bundle bundle = new Bundle();

                    //Add your data to bundle
                    bundle.putString("checkin_time", checkin_time);
                    bundle.putString("customer_id", movie.getCustomer_id());
                    bundle.putString("invoice_id", id);
                    bundle.putString("login_id", login_id);
                    bundle.putString("nav_type", "Reports");
                    bundle.putString("invoice_num", movie.getOrder_num());

                    //Add the bundle to the intent
                    myintent.putExtras(bundle);

                    //Fire that second activity
                    startActivity(myintent);

                }
            });

        }

        @Override
        public int getItemCount() {
            return users.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView date, order_num, customer_name, location, city, line_items, value, status;
            public View vm;


            public MyViewHolder(View view) {
                super(view);
                date = view.findViewById(R.id.date);
                order_num = view.findViewById(R.id.order_num);
                customer_name = view.findViewById(R.id.customer_name);
                location = view.findViewById(R.id.location);
                city = view.findViewById(R.id.city);
                line_items = view.findViewById(R.id.line_items);
                value = view.findViewById(R.id.value);
                status = view.findViewById(R.id.status);
                vm = view;

            }
        }

    }
}


